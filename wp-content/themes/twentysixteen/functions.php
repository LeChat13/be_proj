<?php
/**
 * Twenty Sixteen functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

/**
 * Twenty Sixteen only works in WordPress 4.4 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.4-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}

if ( ! function_exists( 'twentysixteen_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 *
	 * Create your own twentysixteen_setup() function to override in a child theme.
	 *
	 * @since Twenty Sixteen 1.0
	 */
	function twentysixteen_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/twentysixteen
		 * If you're building a theme based on Twenty Sixteen, use a find and replace
		 * to change 'twentysixteen' to the name of your theme in all the template files
		 */
		load_theme_textdomain( 'twentysixteen' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for custom logo.
		 *
		 *  @since Twenty Sixteen 1.2
		 */
		add_theme_support(
			'custom-logo', array(
				'height'      => 240,
				'width'       => 240,
				'flex-height' => true,
			)
		);

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
		 */
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 1200, 9999 );

		// This theme uses wp_nav_menu() in two locations.
		register_nav_menus(
			array(
				'primary' => __( 'Primary Menu', 'twentysixteen' ),
				'social'  => __( 'Social Links Menu', 'twentysixteen' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5', array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);

		/*
		 * Enable support for Post Formats.
		 *
		 * See: https://codex.wordpress.org/Post_Formats
		 */
		add_theme_support(
			'post-formats', array(
				'aside',
				'image',
				'video',
				'quote',
				'link',
				'gallery',
				'status',
				'audio',
				'chat',
			)
		);

		/*
		 * This theme styles the visual editor to resemble the theme style,
		 * specifically font, colors, icons, and column width.
		 */
		add_editor_style( array( 'css/editor-style.css', twentysixteen_fonts_url() ) );

		// Indicate widget sidebars can use selective refresh in the Customizer.
		add_theme_support( 'customize-selective-refresh-widgets' );
	}
endif; // twentysixteen_setup
add_action( 'after_setup_theme', 'twentysixteen_setup' );

/**
 * Sets the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'twentysixteen_content_width', 840 );
}
add_action( 'after_setup_theme', 'twentysixteen_content_width', 0 );

/**
 * Registers a widget area.
 *
 * @link https://developer.wordpress.org/reference/functions/register_sidebar/
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_widgets_init() {
	register_sidebar(
		array(
			'name'          => __( 'Sidebar', 'twentysixteen' ),
			'id'            => 'sidebar-1',
			'description'   => __( 'Add widgets here to appear in your sidebar.', 'twentysixteen' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

	register_sidebar(
		array(
			'name'          => __( 'Content Bottom 1', 'twentysixteen' ),
			'id'            => 'sidebar-2',
			'description'   => __( 'Appears at the bottom of the content on posts and pages.', 'twentysixteen' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

	register_sidebar(
		array(
			'name'          => __( 'Content Bottom 2', 'twentysixteen' ),
			'id'            => 'sidebar-3',
			'description'   => __( 'Appears at the bottom of the content on posts and pages.', 'twentysixteen' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'twentysixteen_widgets_init' );

if ( ! function_exists( 'twentysixteen_fonts_url' ) ) :
	/**
	 * Register Google fonts for Twenty Sixteen.
	 *
	 * Create your own twentysixteen_fonts_url() function to override in a child theme.
	 *
	 * @since Twenty Sixteen 1.0
	 *
	 * @return string Google fonts URL for the theme.
	 */
	function twentysixteen_fonts_url() {
		$fonts_url = '';
		$fonts     = array();
		$subsets   = 'latin,latin-ext';

		/* translators: If there are characters in your language that are not supported by Merriweather, translate this to 'off'. Do not translate into your own language. */
		if ( 'off' !== _x( 'on', 'Merriweather font: on or off', 'twentysixteen' ) ) {
			$fonts[] = 'Merriweather:400,700,900,400italic,700italic,900italic';
		}

		/* translators: If there are characters in your language that are not supported by Montserrat, translate this to 'off'. Do not translate into your own language. */
		if ( 'off' !== _x( 'on', 'Montserrat font: on or off', 'twentysixteen' ) ) {
			$fonts[] = 'Montserrat:400,700';
		}

		/* translators: If there are characters in your language that are not supported by Inconsolata, translate this to 'off'. Do not translate into your own language. */
		if ( 'off' !== _x( 'on', 'Inconsolata font: on or off', 'twentysixteen' ) ) {
			$fonts[] = 'Inconsolata:400';
		}

		if ( $fonts ) {
			$fonts_url = add_query_arg(
				array(
					'family' => urlencode( implode( '|', $fonts ) ),
					'subset' => urlencode( $subsets ),
				), 'https://fonts.googleapis.com/css'
			);
		}

		return $fonts_url;
	}
endif;

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'twentysixteen_javascript_detection', 0 );

/**
 * Enqueues scripts and styles.
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'twentysixteen-fonts', twentysixteen_fonts_url(), array(), null );

	// Add Genericons, used in the main stylesheet.
	wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.4.1' );

	// Theme stylesheet.
	wp_enqueue_style( 'twentysixteen-style', get_stylesheet_uri() );

	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'twentysixteen-ie', get_template_directory_uri() . '/css/ie.css', array( 'twentysixteen-style' ), '20160816' );
	wp_style_add_data( 'twentysixteen-ie', 'conditional', 'lt IE 10' );

	// Load the Internet Explorer 8 specific stylesheet.
	wp_enqueue_style( 'twentysixteen-ie8', get_template_directory_uri() . '/css/ie8.css', array( 'twentysixteen-style' ), '20160816' );
	wp_style_add_data( 'twentysixteen-ie8', 'conditional', 'lt IE 9' );

	// Load the Internet Explorer 7 specific stylesheet.
	wp_enqueue_style( 'twentysixteen-ie7', get_template_directory_uri() . '/css/ie7.css', array( 'twentysixteen-style' ), '20160816' );
	wp_enqueue_style( 'twentysixteen-style', get_template_directory_uri() . '/css/style.css', array( 'twentysixteen-style' ), '20160816' );
	wp_style_add_data( 'twentysixteen-ie7', 'conditional', 'lt IE 8' );

	// Load the html5 shiv.
	wp_enqueue_script( 'twentysixteen-html5', get_template_directory_uri() . '/js/html5.js', array(), '3.7.3' );
	wp_script_add_data( 'twentysixteen-html5', 'conditional', 'lt IE 9' );

	wp_enqueue_script( 'twentysixteen-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20160816', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'twentysixteen-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20160816' );
	}

	wp_enqueue_script( 'twentysixteen-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20160816', true );
    wp_enqueue_script( 'twentysixteen-script', get_template_directory_uri() . '/js/script.js', array( 'jquery' ), '20160816', true );

	wp_localize_script(
		'twentysixteen-script', 'screenReaderText', array(
			'expand'   => __( 'expand child menu', 'twentysixteen' ),
			'collapse' => __( 'collapse child menu', 'twentysixteen' ),
		)
	);
}
add_action( 'wp_enqueue_scripts', 'twentysixteen_scripts' );

/**
 * Adds custom classes to the array of body classes.
 *
 * @since Twenty Sixteen 1.0
 *
 * @param array $classes Classes for the body element.
 * @return array (Maybe) filtered body classes.
 */
function twentysixteen_body_classes( $classes ) {
	// Adds a class of custom-background-image to sites with a custom background image.
	if ( get_background_image() ) {
		$classes[] = 'custom-background-image';
	}

	// Adds a class of group-blog to sites with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of no-sidebar to sites without active sidebar.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'twentysixteen_body_classes' );

/**
 * Converts a HEX value to RGB.
 *
 * @since Twenty Sixteen 1.0
 *
 * @param string $color The original color, in 3- or 6-digit hexadecimal form.
 * @return array Array containing RGB (red, green, and blue) values for the given
 *               HEX code, empty array otherwise.
 */
function twentysixteen_hex2rgb( $color ) {
	$color = trim( $color, '#' );

	if ( strlen( $color ) === 3 ) {
		$r = hexdec( substr( $color, 0, 1 ) . substr( $color, 0, 1 ) );
		$g = hexdec( substr( $color, 1, 1 ) . substr( $color, 1, 1 ) );
		$b = hexdec( substr( $color, 2, 1 ) . substr( $color, 2, 1 ) );
	} elseif ( strlen( $color ) === 6 ) {
		$r = hexdec( substr( $color, 0, 2 ) );
		$g = hexdec( substr( $color, 2, 2 ) );
		$b = hexdec( substr( $color, 4, 2 ) );
	} else {
		return array();
	}

	return array(
		'red'   => $r,
		'green' => $g,
		'blue'  => $b,
	);
}

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for content images
 *
 * @since Twenty Sixteen 1.0
 *
 * @param string $sizes A source size value for use in a 'sizes' attribute.
 * @param array  $size  Image size. Accepts an array of width and height
 *                      values in pixels (in that order).
 * @return string A source size value for use in a content image 'sizes' attribute.
 */
function twentysixteen_content_image_sizes_attr( $sizes, $size ) {
	$width = $size[0];

	if ( 840 <= $width ) {
		$sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 62vw, 840px';
	}

	if ( 'page' === get_post_type() ) {
		if ( 840 > $width ) {
			$sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
		}
	} else {
		if ( 840 > $width && 600 <= $width ) {
			$sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 61vw, (max-width: 1362px) 45vw, 600px';
		} elseif ( 600 > $width ) {
			$sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
		}
	}

	return $sizes;
}
add_filter( 'wp_calculate_image_sizes', 'twentysixteen_content_image_sizes_attr', 10, 2 );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for post thumbnails
 *
 * @since Twenty Sixteen 1.0
 *
 * @param array $attr Attributes for the image markup.
 * @param int   $attachment Image attachment ID.
 * @param array $size Registered image size or flat array of height and width dimensions.
 * @return array The filtered attributes for the image markup.
 */
function twentysixteen_post_thumbnail_sizes_attr( $attr, $attachment, $size ) {
	if ( 'post-thumbnail' === $size ) {
		if ( is_active_sidebar( 'sidebar-1' ) ) {
			$attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 60vw, (max-width: 1362px) 62vw, 840px';
		} else {
			$attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 88vw, 1200px';
		}
	}
	return $attr;
}
add_filter( 'wp_get_attachment_image_attributes', 'twentysixteen_post_thumbnail_sizes_attr', 10, 3 );

/**
 * Modifies tag cloud widget arguments to display all tags in the same font size
 * and use list format for better accessibility.
 *
 * @since Twenty Sixteen 1.1
 *
 * @param array $args Arguments for tag cloud widget.
 * @return array The filtered arguments for tag cloud widget.
 */
function twentysixteen_widget_tag_cloud_args( $args ) {
	$args['largest']  = 1;
	$args['smallest'] = 1;
	$args['unit']     = 'em';
	$args['format']   = 'list';

	return $args;
}
add_filter( 'widget_tag_cloud_args', 'twentysixteen_widget_tag_cloud_args' );

function redirect_login_page() {  
    $login_page  = home_url( '//' );  
    $page_viewed = basename($_SERVER['REQUEST_URI']);  
  
    if( $page_viewed == "wp-login.php" && $_SERVER['REQUEST_METHOD'] == 'GET') {  
        wp_redirect($login_page);  
        exit;  
    }  
}  
add_action('init','redirect_login_page');

function login_failed() {  
    $login_page  = home_url( '//' );  
    wp_redirect( $login_page . '?login=failed' );  
    exit;  
}  
add_action( 'wp_login_failed', 'login_failed' );  
  
function verify_username_password( $user, $username, $password ) {  
    $login_page  = home_url( '//' );  
    if( $username == "" || $password == "" ) {  
        wp_redirect( $login_page . "?login=empty" );  
        exit;  
    }  
}  
add_filter( 'authenticate', 'verify_username_password', 1, 3);

function logout_page() {  
    $login_page  = home_url( '//' );  
    wp_redirect( $login_page );  
    exit;  
}  
add_action('wp_logout','logout_page');

function no_logined() {  
    $login_page  = home_url( '//' );  
    wp_redirect( $login_page );  
    exit;  
}  
add_action('wp_no_logined','no_logined');

add_action( 'wp_ajax_ajax', 'ajax_execute' ); // wp_ajax_{ЗНАЧЕНИЕ ПАРАМЕТРА ACTION!!}
//add_action( 'wp_ajax_nopriv_ajax', 'ajax_execute' );  // wp_ajax_nopriv_{ЗНАЧЕНИЕ ACTION!!}
// первый хук для авторизованных, второй для не авторизованных пользователей
 
function ajax_execute() {

	$task = $_POST['task'];
	$dbase = $_POST['dbase'];

	error_reporting(0);

# Подключение к MySQL
	$db_host = 'h807240122.mysql';
	$db_user = 'h807240122_u0';
	$db_password = '6OXbvGd/';#'WeV3C+xn';
	$db_name = 'h807240122_s0';

# Создание соединения
	$conn = new mysqli($db_host, $db_user, $db_password, $db_name);

# Проверка соединения
	if ($conn->connect_error) {
		die('<p style="color:red">'.$conn->connect_error.'</p>');
	}

# Установить кодировки
	$sql = "SET NAMES utf8";
	if (!($conn->query($sql) === TRUE)) {
		die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
	}

# Выбор варианта зададачи запроса
	switch ($task) {

		case 'loadNewFile': {

# Удалить старые временные таблицы
			$sql = "SHOW TABLES LIKE 'db\_".$dbase."\_xls\_for\_diagrams\_%'";
			if ($result = $conn->query($sql)) {
				$table_names = [];
				while ($row = $result->fetch_assoc()) {
					$table_names[] = $row['Tables_in_h807240122_s0 (db\_'.$dbase.'\_xls\_for\_diagrams\_%)'];
				}
				$sql = "DROP TABLE IF EXISTS ".implode($table_names, ', ');
				if (!($conn->query($sql) === TRUE)) {
					die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
				}
			}
				else {
					die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
				}

# Разбор исходных данных
			$data = $_POST['data'];
			$col_count = 0;
			$row_count = 0;
			$columns = [];
			$uniq_values = [];
			foreach ($data as $row) {
				$row_count++;
				foreach ($row as $column => $cell) {
					$f = 1;
					if (($column == 'Дата окончания процедуры') || ($column == 'Сумма Кон. Продаж в Нац. валюта')) {# || ($column == 'Сумма Кон. Продаж в EURO') || ($column == 'Сумма Упаковок')) {
						continue;
					}
					foreach ($columns as $name) {
						if ($column == $name) {
							$f = 0;
							break;
						}
					}
					if ($f) {
						$columns[] = $column;
						$col_count++;
						if ($row_count > 1) {
							$uniq_values[$column][] = 'null';
						}
						$uniq_values[$column][] = $cell;
					}
						else {
							$f = 1;
							foreach ($uniq_values[$column] as $value) {
								if ($cell == $value) {
									$f = 0;
									break;
								}
							}
							if ($f) {
								$uniq_values[$column][] = $cell;
							}
						}
				}
			}

# Формирование ответа и заполнение БД
			$col_ids = [];
			$uVal_ids = [];
			$sql = "
				CREATE TABLE `db_".$dbase."_xls_for_diagrams_columns` (
					`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
					`value` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'noinfo',
					`add_date` DATETIME NOT NULL DEFAULT NOW(),
					PRIMARY KEY (`id`),
					UNIQUE `i_value` (`value`)
				) ENGINE = InnoDB CHARSET = utf8 COLLATE utf8_general_ci
			";
			if (!($conn->query($sql) === TRUE)) {
				die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
			}
			$sql = "
				CREATE TABLE `db_".$dbase."_xls_for_diagrams_unique_values` (
					`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
					`column_id` INT(11) UNSIGNED NOT NULL,
					`value` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'noinfo',
					`add_date` DATETIME NOT NULL DEFAULT NOW(),
					PRIMARY KEY (`id`)
				) ENGINE = InnoDB CHARSET = utf8 COLLATE utf8_general_ci
			";
			if (!($conn->query($sql) === TRUE)) {
				die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
			}
			$queries = [];
			$t_cols = [];
			foreach ($columns as $column) {
				$sql = "INSERT INTO db_".$dbase."_xls_for_diagrams_columns (value) VALUES ('".$column."')";
				if (!($conn->query($sql) === TRUE)) {
					die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
				}
				$col_ids[$column] = $conn->insert_id;
				$queries[] = "`column_number_".$conn->insert_id."` INT(11) UNSIGNED NOT NULL";
				$t_cols[] = "column_number_".$conn->insert_id;
				$uVal_ids[$column] = [];
				foreach ($uniq_values[$column] as $value) {
					$sql = "INSERT INTO db_".$dbase."_xls_for_diagrams_unique_values (column_id, value) VALUES (".$col_ids[$column].", '".$value."')";
					if (!($conn->query($sql) === TRUE)) {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
					$uVal_ids[$column][$value] = $conn->insert_id;
				}
			}
			if (($col_list = implode(', ', $t_cols)) == '') {
				die('<p style="color:red">Ошибка: col_list пуст - в файле отсутствуют колонки для анализа, файл не подходит для обработки!</p>');
			}
			$sql = "
				CREATE TABLE `db_".$dbase."_xls_for_diagrams_table` (
					`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
					`sorting_date` DATE NOT NULL DEFAULT '2001-01-01',
					`amount` DECIMAL(15,2) UNSIGNED NOT NULL DEFAULT '0.00',
					`euros` DECIMAL(15,2) UNSIGNED NOT NULL DEFAULT '0.00',
					`count` DECIMAL(15,2) UNSIGNED NOT NULL DEFAULT '0.00',
					".implode(', ', $queries).",
					`add_date` DATETIME NOT NULL DEFAULT NOW(),
					PRIMARY KEY (`id`),
					INDEX `i_sorting_date` (`sorting_date`)
				) ENGINE = InnoDB CHARSET = utf8 COLLATE utf8_general_ci
			";
			if (!($conn->query($sql) === TRUE)) {
				die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
			}
			$row_count = 0;
			foreach ($data as $row) {
				$row_count++;
				$t_vals = [];
				foreach ($columns as $column) {
					if (!(isset($row[$column]))) {
						if (!(isset($uVal_ids[$column]['null']))) {
							$sql = "INSERT INTO db_".$dbase."_xls_for_diagrams_unique_values (column_id, value) VALUES (".$col_ids[$column].", 'null')";
							if (!($conn->query($sql) === TRUE)) {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
							$uVal_ids[$column]['null'] = $conn->insert_id;
						}
						$row[$column] = 'null';
					}
					$t_vals[] = $uVal_ids[$column][$row[$column]];
				}
				$date_pieces = explode('/', $row['Дата окончания процедуры']);
				if ($date_pieces[0] < 10) {
					$date_pieces[0] = '0'.(int)$date_pieces[0];
				};
				if ($date_pieces[1] < 10) {
					$date_pieces[1] = '0'.(int)$date_pieces[1];
				};
				if ($date_pieces[2] < 10) {
					$date_pieces[2] = '0'.(int)$date_pieces[2];
				};
				$date_pieces[2] = '20'.$date_pieces[2];
				$sorting_date = $date_pieces[2].'-'.$date_pieces[0].'-'.$date_pieces[1];
				$amount = str_replace(',', '', $row['Сумма Кон. Продаж в Нац. валюта']);
				$euros = str_replace(',', '', $row['Сумма Кон. Продаж в EURO']);
				$count = str_replace(',', '', $row['Сумма Упаковок']);
				$sql = "INSERT INTO db_".$dbase."_xls_for_diagrams_table (sorting_date, amount, euros, count, ".$col_list.") VALUES ('".$sorting_date."', ".$amount.", ".$euros.", ".$count.", ".implode(', ', $t_vals).")";
				if (!($conn->query($sql) === TRUE)) {
					die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
				}
			}

			echo '{"result":'.$row_count.'}';

			break;

		}

		case 'getColumns': {

			$sql = "SELECT id, value FROM db_".$dbase."_xls_for_diagrams_columns";
			if ($result = $conn->query($sql)) {
				$columns = [];
				while ($obj = $result->fetch_object()) {
					$columns[] = '{"id":'.$obj->id.',"value":"'.$obj->value.'"}';
				}
				echo '{"result":'.count($columns).',"data":['.implode(',', $columns).']}';
			}
				else {
					die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
				}

			break;

		}

		case 'maxStart': {

			$sql = "SELECT MAX(amount) AS max_price FROM db_".$dbase."_xls_for_diagrams_table";
			if ($result = $conn->query($sql)) {
				while ($obj = $result->fetch_object()) {
					$max_price = $obj->max_price;
				}
			}
				else {
					die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
				}

			echo '{"result":1,"max_price":'.$max_price.'}';

			break;

		}

		case 'getMain': {

			#	Определить название "сортера"
			$sql = "SELECT id FROM db_".$dbase."_xls_for_diagrams_columns WHERE value = 'Корпорация' OR value = 'Корпорация - map' ORDER BY value DESC";
			$colids = [];
			if ($result = $conn->query($sql)) {
				while ($obj = $result->fetch_object()) {
					$colids[] = $obj->id;
				}
			}
				else {
					die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
				}

			foreach ($colids as $col_id) {
				$sql = "SELECT id, value FROM db_".$dbase."_xls_for_diagrams_unique_values WHERE column_id  = ".$col_id;
				if ($result = $conn->query($sql)) {
					$values = [];
					$defaultId = 0;
					while ($obj = $result->fetch_object()) {
						$values[] = '{"id":'.$obj->id.',"value":"'.$obj->value.'"}';
						if (($obj->value == 'ФИЛИПС') || ($obj->value == 'PHILIPS')) {
							$defaultId = $obj->id;
						}
					}
					echo '{"result":'.count($values).',"defaultId":'.$defaultId.',"data":['.implode(',', $values).']}';
					if (count($values)) {
						# Закрыть подключение
						$conn->close();
						die;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}

			}

			echo '{"result":0,"defaultId":0,"data":[],"error":"emptyData"}';
			break;

		}

		case 'getSorterValues': {

			$column_id = $_POST['column'];
			$params = $_POST['params'];
			$sdate = $_POST['sdate'];
			$edate = $_POST['edate'];
			$mainCorp = $_POST['mainCorp'];

			if ($params['sP'] == 2) {
				$summing = 'count';
			}
				else if ($params['sP'] == 1) {
					$summing = 'euros';
				}
					else {
						$summing = 'amount';
					}
			$filters_queries = [];
			foreach ($params['fD'] as $key => $item) {
				if ($key != $_POST['filternum']) {
					if (count($item['vI'])) {
						$filters_queries[] = " column_number_".$item['cI']." IN (".implode(', ',$item['vI']).")";
					}
				}
			}
			if ($filters_queries) {
				$filters_query = implode(" AND", $filters_queries);
			}
				else {
					$filters_query = " 1";
				}

			#	Определить лидеров по "сортеру"
			$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item, value AS item_name, (SELECT value FROM db_".$dbase."_xls_for_diagrams_columns WHERE id = ".$params['sI'].") AS sorter_name FROM db_".$dbase."_xls_for_diagrams_table JOIN db_".$dbase."_xls_for_diagrams_unique_values ON (db_".$dbase."_xls_for_diagrams_unique_values.column_id = ".$params['sI']." AND db_".$dbase."_xls_for_diagrams_unique_values.id = db_".$dbase."_xls_for_diagrams_table.column_number_".$params['sI'].") WHERE".$filters_query." GROUP BY column_number_".$params['sI']." ORDER BY sorter_summ DESC";
			if ($result = $conn->query($sql)) {
				$values = [];
				$count = 0;
				while ($obj = $result->fetch_object()) {
					$count++;
					if ($count < $params['sCs']) {
						if (($params['sU']) && (( $obj->sorter_name == 'Корпорация') || ( $obj->sorter_name == 'Корпорация - map')) && ($obj->item_name == 'U.M.')) {
							$count--;
						}
							else {
								$values[] = '{"id":'.$obj->sorter_item.',"value":"'.$obj->item_name.'"}';
							}
					}
						else {
							if ((( $obj->sorter_name == 'Корпорация') && ($obj->sorter_item == $mainCorp)) || (( $obj->sorter_name == 'Корпорация - map') && ($obj->sorter_item == $mainCorp))) {
								$values[$params['sCs'] - 2] = '{"id":'.$obj->sorter_item.',"value":"'.$obj->item_name.'"}';
							}
						}
				}
				if (!(count($values))) {
					echo '{"result":0,"error":"Фильтры отсеяли все значения"}';
					break;
				}
				echo '{"result":'.count($values).',"data":['.implode(',', $values).']}';
			}
				else {
					die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
				}
			break;

		}

		case 'getColValues': {

			$column_id = $_POST['column'];
			$params = $_POST['params'];
			$sdate = $_POST['sdate'];
			$edate = $_POST['edate'];

			$filters_queries = [];
			foreach ($params['fD'] as $key => $item) {
				if ($key != $_POST['filternum']) {
					if (count($item['vI'])) {
						$filters_queries[] = " column_number_".$item['cI']." IN (".implode(', ',$item['vI']).")";
					}
				}
			}
			if ($filters_queries) {
				$filters_query = implode(" AND", $filters_queries);
			}
				else {
					$filters_query = " 1";
				}
				/*$sql = 
					"SELECT 
						SUM(".$summing.") AS sorter_summ, 
						column_number_".$params['sI']." AS sorter_item, 
						value AS item_name, 
						(SELECT value FROM db_".$dbase."_xls_for_diagrams_columns WHERE id = ".$params['sI'].") AS sorter_name 
					FROM 
						db_".$dbase."_xls_for_diagrams_table 
					JOIN 
						db_".$dbase."_xls_for_diagrams_unique_values 
					ON (
						db_".$dbase."_xls_for_diagrams_unique_values.column_id = ".$params['sI']." 
						AND db_".$dbase."_xls_for_diagrams_unique_values.id = db_".$dbase."_xls_for_diagrams_table.column_number_".$params['sI']."
					) 
					WHERE
						".$filters_query." 
					GROUP BY 
						column_number_".$params['sI']." 
					ORDER BY 
						sorter_summ DESC";
				$sql = 
					"SELECT 
						id, 
						value 
					FROM 
						db_".$dbase."_xls_for_diagrams_unique_values 
					WHERE 
						column_id = ".$column_id;*/
			$sql = "SELECT 
						id, 
						value 
					FROM 
						db_".$dbase."_xls_for_diagrams_unique_values 
					WHERE 
						id IN (
							SELECT DISTINCT 
								column_number_".$column_id." 
							FROM 
								db_".$dbase."_xls_for_diagrams_table 
							WHERE 
								".$filters_query."
						)";
			if ($result = $conn->query($sql)) {
				$values = [];
				while ($obj = $result->fetch_object()) {
					$values[] = '{"id":'.$obj->id.',"value":"'.$obj->value.'"}';
				}
				echo '{"result":'.count($values).',"data":['.implode(',', $values).']}';
			}
				else {
					die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
				}

			break;

		}

		case 'getSlideData': {
			$params = $_POST['params'];
			if ($params['ors']) {
			} else {
				$params['oos'] = 1;
			}
			$sdate = $_POST['sdate'];
			$edate = $_POST['edate'];
			$page = $_POST['page'];
			$mainCorp = $_POST['mainCorp'];
			$sqls = [];
			switch ($params['sP']) {
				case 0: {
					$cnt_string = 'RUB';
					switch ($params['sM']) {
						case 0: {
							$msscht1 = 100000000;// /100000000) / 10
							$msscht2 = 100000;// / 100000) / 10000
							$mst_string = 'Bln.'.$cnt_string;
							break;
						}
						case 1: {
							$msscht1 = 100000;// /100000000) / 10
							$msscht2 = 100;// / 100000) / 10000
							$mst_string = 'Mln.'.$cnt_string;
							break;
						}
						case 2: {
							$msscht1 = 100;// /100000000) / 10
							$msscht2 = 0.1;// / 100000) / 10000
							$mst_string = 'Tsd.'.$cnt_string;
							break;
						}
						default: {
							$msscht1 = 100000000;// /100000000) / 10
							$msscht2 = 100000;// / 100000) / 10000
							$mst_string = 'Bln.'.$cnt_string;
							break;
						}
					}
					break;
				}
				case 1: {
					$cnt_string = 'Euro';
					switch ($params['sM']) {
						case 0: {
							$msscht1 = 100000000;// /100000000) / 10
							$msscht2 = 100000;// / 100000) / 10000
							$mst_string = 'Bln.'.$cnt_string;
							break;
						}
						case 1: {
							$msscht1 = 100000;// /100000000) / 10
							$msscht2 = 100;// / 100000) / 10000
							$mst_string = 'Mln.'.$cnt_string;
							break;
						}
						case 2: {
							$msscht1 = 100;// /100000000) / 10
							$msscht2 = 0.1;// / 100000) / 10000
							$mst_string = 'Tsd.'.$cnt_string;
							break;
						}
						default: {
							$msscht1 = 100000000;// /100000000) / 10
							$msscht2 = 100000;// / 100000) / 10000
							$mst_string = 'Bln.'.$cnt_string;
							break;
						}
					}
					break;
				}
				case 2: {
					$mst_string = 'units';
					$msscht1 = 1;// /100000000) / 10	tmp
					$msscht2 = 1;// / 100000) / 10000	tmp
					break;
				}
				default: {
					$cnt_string = 'LC';
					switch ($params['sM']) {
						case 0: {
							$msscht1 = 100000000;// /100000000) / 10
							$msscht2 = 100000;// / 100000) / 10000
							$mst_string = 'Bln.'.$cnt_string;
							break;
						}
						case 1: {
							$msscht1 = 100000;// /100000000) / 10
							$msscht2 = 100;// / 100000) / 10000
							$mst_string = 'Mln.'.$cnt_string;
							break;
						}
						case 2: {
							$msscht1 = 100;// /100000000) / 10
							$msscht2 = 0.1;// / 100000) / 10000
							$mst_string = 'Tsd.'.$cnt_string;
							break;
						}
						default: {
							$msscht1 = 100000000;// /100000000) / 10
							$msscht2 = 100000;// / 100000) / 10000
							$mst_string = 'Bln.'.$cnt_string;
							break;
						}
			}
					break;
				}
			}
			$slide_legend = 'Тип страницы - '.$page.' Параметры: Основной параметр сортировки - ';
			#	Определить название "сортера"
			if ($params['sI']) {
				$sql = "SELECT value FROM db_".$dbase."_xls_for_diagrams_columns WHERE id = ".$params['sI'];
				$sqls[] = $sql;
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$slide_legend .= $obj->value.'';
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
			}
				else {
					$slide_legend .= 'Ценовые диапазоны'.'';
				}
			if (($page > 6) and ($page != 13) and ($page != 15)) {
				#	Определить название "второго сортера"
				$sql = "SELECT value FROM db_".$dbase."_xls_for_diagrams_columns WHERE id = ".$params['sCD'];
				$sqls[] = $sql;
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$slide_legend .= ' Второй параметр сортировки - '.$obj->value.'';
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
			}
			$slide_legend .= ' Анализируемые интервалы - ';
			if ($params['iI'] == 1) {
				$slide_legend .= 'Месяцы';
			}
				else if ($params['iI'] == 2) {
					$slide_legend .= 'Годы';
				}
					else {
						$slide_legend .= 'Кварталы';
					}
			if ($params['sP'] == 2) {
				$summing = 'count';
			}
				else if ($params['sP'] == 1) {
					$summing = 'euros';
				}
					else {
						$summing = 'amount';
					}
			$slide_legend .= '</br>Фильтры: ';
			if (count($params['fD'])) {
				foreach ($params['fD'] as $item) {
					#	Определить название фильтра
					$sql = "SELECT value FROM db_".$dbase."_xls_for_diagrams_columns WHERE id = ".$item['cI'];
				$sqls[] = $sql;
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							$slide_legend .= ' Фильтр по полю - '.$obj->value;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
					#	Определить название значения фильтра
					$sql = "SELECT value FROM db_".$dbase."_xls_for_diagrams_unique_values WHERE id IN (".implode(', ',$item['vI']).")";
				$sqls[] = $sql;
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							$slide_legend .= ' по значению - '.$obj->value.' ';
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
					
					$filters_queries[] = " column_number_".$item['cI']." IN (".implode(', ',$item['vI']).")";
				}
			}
				else {
					$slide_legend .= 'отсутствуют</br>';
				}
			if (($page < 3) or ($page == 5) or ($page == 6)) {
				$filters_queries = [];

				foreach ($params['fD'] as $item) {
					$filters_queries[] = " column_number_".$item['cI']." IN (".implode(', ',$item['vI']).")";
				}
				if ($filters_queries) {
					$filters_query = implode(" AND", $filters_queries);
					$main_filter = $filters_queries[0];
				}
					else {
						$filters_query = " 1";
						$main_filter = " 1";
					}

				$date_pieces = explode('-', $_POST['edate']);
				$year = $date_pieces[0];
				$month = $date_pieces[1];
				$day = $date_pieces[2];

				$sorters = [];
				$sorters_summ = [];
				$sorters_name = [];
				$other_summ = 0;
				if ($params['sI']) {
					#	Определить лидеров по "сортеру"
					$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item, value AS item_name, (SELECT value FROM db_".$dbase."_xls_for_diagrams_columns WHERE id = ".$params['sI'].") AS sorter_name FROM db_".$dbase."_xls_for_diagrams_table JOIN db_".$dbase."_xls_for_diagrams_unique_values ON (db_".$dbase."_xls_for_diagrams_unique_values.column_id = ".$params['sI']." AND db_".$dbase."_xls_for_diagrams_unique_values.id = db_".$dbase."_xls_for_diagrams_table.column_number_".$params['sI'].") WHERE".$filters_query." GROUP BY column_number_".$params['sI']." ORDER BY sorter_summ DESC";
				$sqls[] = $sql;
					if ($result = $conn->query($sql)) {
						if ($params['ors']) {
							$sorters = $params['osC'];
							foreach ($sorters as $item) {
								$sorters_summ[$item] = 0;
							}
							while ($obj = $result->fetch_object()) {
								if (in_array($obj->sorter_item, $sorters)) {
									$sorters_summ[$obj->sorter_item] = $obj->sorter_summ;
								}
									else {
										$other_summ += $obj->sorter_summ;
									}
							}
						}
							else {
								$count = 0;
								while ($obj = $result->fetch_object()) {
									$count++;
									if ($count < $params['sCs']) {
										if (($params['sU']) && (( $obj->sorter_name == 'Корпорация') || ( $obj->sorter_name == 'Корпорация - map')) && ($obj->item_name == 'U.M.')) {
											$count--;
											$other_summ += $obj->sorter_summ;
										}
											else {
												$sorters[] = $obj->sorter_item;
												$sorters_summ[$obj->sorter_item] = $obj->sorter_summ;
											}
									}
										else {
											if ((( $obj->sorter_name == 'Корпорация') && ($obj->sorter_item == $mainCorp)) || (( $obj->sorter_name === 'Корпорация - map') && ($obj->sorter_item == $mainCorp))) {
												$other_summ += $sorters_summ[$sorters[$params['sCs'] - 2]];
												$sorters[$params['sCs'] - 2] = $obj->sorter_item;
												$sorters_summ[$obj->sorter_item] = $obj->sorter_summ;
											}
												else {
													$other_summ += $obj->sorter_summ;
												}
										}
								}
							}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
				}
					else {
						foreach ($params['dD'] as $key => $item) {
							$sorters[] = $key;
							$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
				$sqls[] = $sql;
							$sorter_name = 'Ценовые диапазоны';
							if ($result = $conn->query($sql)) {
								while ($obj = $result->fetch_object()) {
									$sorters_summ[$key] = $obj->sorter_summ;
								}
							}
								else {
									die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
								}
							$sorters_name[$key] = $key;
						}
					}

				if (!(count($sorters))) {
					echo '{"result":0,"error":"Фильтры отсеяли все значения"}';
					break;
				}

				if ($params['sI']) {
					#	Определить название "сортера"
					$sql = "SELECT value FROM db_".$dbase."_xls_for_diagrams_columns WHERE id = ".$params['sI'];
				$sqls[] = $sql;
					$sorter_name = '';
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							$sorter_name = $obj->value;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}

					#	Определить названия лидеров "сортера"
					$sql = "SELECT id, value FROM db_".$dbase."_xls_for_diagrams_unique_values WHERE column_id = ".$params['sI']." AND id IN (".implode(', ', $sorters).")";
				$sqls[] = $sql;
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							if (((($sorter_name === 'Корпорация') && ($obj->id == $mainCorp)) || (($sorter_name === 'Корпорация - map') && ($obj->id == $mainCorp))) && (!($params['ors']))) {
								array_splice($sorters, array_search($obj->id, $sorters), 1);
								array_unshift($sorters, $obj->id);
							}
							$sorters_name[$obj->id] = $obj->value;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
				}

				$months = ['', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

				#	Определить данные для графиков MTM
				$monthToMonthData = [];
				$monthToMonthLabels = [
					((($month + 1) > 12) ? "Jan".substr(($year - 1), -2) : $months[(int)$month + 1]."".substr(($year - 2), -2))."-".$months[(int)$month]."".substr(($year - 1), -2),
					((($month + 1) > 12) ? "Jan".substr($year, -2) : $months[(int)$month + 1]."".substr(($year - 1), -2))."-".$months[(int)$month]."".substr($year, -2)
				];
				$titlemtm0 = $monthToMonthLabels[0];
				$titlemtm1 = $monthToMonthLabels[1];
				$monthToMonthData['currY'] = ['Other' => 0];
				$monthToMonthData['prevY'] = ['Other' => 0];
				$monthToMonthData['lastY'] = ['Other' => 0];
				$monthToMonthData['currYSumm'] = 0;
				$monthToMonthData['prevYSumm'] = 0;
				$monthToMonthData['lastYSumm'] = 0;
				$monthToMonthData['currYSummPT3'] = 0;
				$monthToMonthData['prevYSummPT3'] = 0;
				$monthToMonthData['lastYSummPT3'] = 0;
				$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? $year : ($year-1)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", $year, $month, "31");
				$sqls[] = "curr - ".$sql;
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$monthToMonthData['currYSummPT3'] = $obj->sorter_summ;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-1) : ($year-2)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-1), $month, "31");
				$sqls[] = "prev - ".$sql;
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$monthToMonthData['prevYSummPT3'] = $obj->sorter_summ;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-2) : ($year-3)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-2), $month, "31");
				$sqls[] = "last - ".$sql;
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$monthToMonthData['lastYSummPT3'] = $obj->sorter_summ;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				if ($params['sI']) {
					$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-0) : ($year-1)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-0), $month, "31")." GROUP BY column_number_".$params['sI'];
				$sqls[] = "curr - ".$sql;
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							if (array_key_exists($obj->sorter_item, $sorters_name)) {
								$monthToMonthData['currY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
								$monthToMonthData['currYSumm'] += $obj->sorter_summ;
							}
								else {
									$monthToMonthData['currY']['Other'] += $obj->sorter_summ;
									if ($params['oos']) {
										$monthToMonthData['currYSumm'] += $obj->sorter_summ;
									}
								}
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
					$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-1) : ($year-2)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-1), $month, "31")." GROUP BY column_number_".$params['sI'];
				$sqls[] = "prev - ".$sql;
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							if (array_key_exists($obj->sorter_item, $sorters_name)) {
								$monthToMonthData['prevY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
								$monthToMonthData['prevYSumm'] += $obj->sorter_summ;
							}
								else {
									$monthToMonthData['prevY']['Other'] += $obj->sorter_summ;
									if ($params['oos']) {
										$monthToMonthData['prevYSumm'] += $obj->sorter_summ;
									}
								}
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
					$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-2) : ($year-3)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-2), $month, "31")." GROUP BY column_number_".$params['sI'];
				$sqls[] = "last - ".$sql;
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							if (array_key_exists($obj->sorter_item, $sorters_name)) {
								$monthToMonthData['lastY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
								$monthToMonthData['lastYSumm'] += $obj->sorter_summ;
							}
								else {
									$monthToMonthData['lastY']['Other'] += $obj->sorter_summ;
									if ($params['oos']) {
										$monthToMonthData['lastYSumm'] += $obj->sorter_summ;
									}
								}
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
				}
					else {
						foreach ($params['dD'] as $key => $item) {
							$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-0) : ($year-1)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-0), $month, "31")." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
				$sqls[] = $sql;
							if ($result = $conn->query($sql)) {
								while ($obj = $result->fetch_object()) {
									$monthToMonthData['currY'][$key] = $obj->sorter_summ;
									$monthToMonthData['currYSumm'] += $obj->sorter_summ;
								}
							}
								else {
									die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
								}
							$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-1) : ($year-2)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-1), $month, "31")." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
				$sqls[] = $sql;
							if ($result = $conn->query($sql)) {
								while ($obj = $result->fetch_object()) {
									$monthToMonthData['prevY'][$key] = $obj->sorter_summ;
									$monthToMonthData['prevYSumm'] += $obj->sorter_summ;
								}
							}
								else {
									die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
								}
							$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-2) : ($year-3)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-2), $month, "31")." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
				$sqls[] = $sql;
							if ($result = $conn->query($sql)) {
								while ($obj = $result->fetch_object()) {
									$monthToMonthData['lastY'][$key] = $obj->sorter_summ;
									$monthToMonthData['lastYSumm'] += $obj->sorter_summ;
								}
							}
								else {
									die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
								}
						}
					}

				#	Определить данные для графиков YTD
				$yearToDayData = [];
				$yearToDayLabels = [
					"Jan".substr(($year - 1), -2)."-".$months[(int)$month]."".substr(($year - 1), -2),
					"Jan".substr($year, -2)."-".$months[(int)$month]."".substr($year, -2)
					#"YTD".substr(($year - 1), -2)." ".$months[$month],
					#"YTD".substr($year, -2)." ".$months[$month]
				];
				$titleytd0 = $yearToDayLabels[0];
				$titleytd1 = $yearToDayLabels[1];
				$yearToDayData['currY'] = ['Other' => 0];
				$yearToDayData['prevY'] = ['Other' => 0];
				$yearToDayData['lastY'] = ['Other' => 0];
				$yearToDayData['currYSumm'] = 0;
				$yearToDayData['prevYSumm'] = 0;
				$yearToDayData['lastYSumm'] = 0;
				$yearToDayData['currYSummPT3'] = 0;
				$yearToDayData['prevYSummPT3'] = 0;
				$yearToDayData['lastYSummPT3'] = 0;
				$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", $year, "01", "01")." AND ".sprintf("%04d%02d%02d", $year, $month, $day);
				$sqls[] = $sql;
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$yearToDayData['currYSummPT3'] = $obj->sorter_summ;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-1), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-1), $month, $day);
				$sqls[] = $sql;
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$yearToDayData['prevYSummPT3'] = $obj->sorter_summ;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-2), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-2), $month, $day);
				$sqls[] = $sql;
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$yearToDayData['lastYSummPT3'] = $obj->sorter_summ;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				if ($params['sI']) {
					$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-0), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-0), $month, $day)." GROUP BY column_number_".$params['sI'];
				$sqls[] = $sql;
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							if (array_key_exists($obj->sorter_item, $sorters_name)) {
								$yearToDayData['currY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
								$yearToDayData['currYSumm'] += $obj->sorter_summ;
							}
								else {
									$yearToDayData['currY']['Other'] += $obj->sorter_summ;
									if ($params['oos']) {
										$yearToDayData['currYSumm'] += $obj->sorter_summ;
									}
								}
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
					$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-1), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-1), $month, $day)." GROUP BY column_number_".$params['sI'];
				$sqls[] = $sql;
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							if (array_key_exists($obj->sorter_item, $sorters_name)) {
								$yearToDayData['prevY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
								$yearToDayData['prevYSumm'] += $obj->sorter_summ;
							}
								else {
									$yearToDayData['prevY']['Other'] += $obj->sorter_summ;
									if ($params['oos']) {
										$yearToDayData['prevYSumm'] += $obj->sorter_summ;
									}
								}
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
					$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-2), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-2), $month, $day)." GROUP BY column_number_".$params['sI'];
				$sqls[] = $sql;
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							if (array_key_exists($obj->sorter_item, $sorters_name)) {
								$yearToDayData['lastY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
								$yearToDayData['lastYSumm'] += $obj->sorter_summ;
							}
								else {
									$yearToDayData['lastY']['Other'] += $obj->sorter_summ;
									if ($params['oos']) {
										$yearToDayData['lastYSumm'] += $obj->sorter_summ;
									}
								}
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
				}
					else {
						foreach ($params['dD'] as $key => $item) {
							$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-0), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-0), $month, $day)." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
				$sqls[] = $sql;
							if ($result = $conn->query($sql)) {
								while ($obj = $result->fetch_object()) {
									$yearToDayData['currY'][$key] = $obj->sorter_summ;
									$yearToDayData['currYSumm'] += $obj->sorter_summ;
								}
							}
								else {
									die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
								}
							$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-1), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-1), $month, $day)." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
				$sqls[] = $sql;
							if ($result = $conn->query($sql)) {
								while ($obj = $result->fetch_object()) {
									$yearToDayData['prevY'][$key] = $obj->sorter_summ;
									$yearToDayData['prevYSumm'] += $obj->sorter_summ;
								}
							}
								else {
									die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
								}
							$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-2), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-2), $month, $day)." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
				$sqls[] = $sql;
							if ($result = $conn->query($sql)) {
								while ($obj = $result->fetch_object()) {
									$yearToDayData['lastY'][$key] = $obj->sorter_summ;
									$yearToDayData['lastYSumm'] += $obj->sorter_summ;
								}
							}
								else {
									die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
								}
						}
					}

				#	Определить данные для графиков intervals
				$intervalsData = [];
				$intervalsNames = [];
				if ($params['iI'] == 1) {
					$numberOfSeriee = -1;
					$countOfSeries = 14;
					for ($i = 0; $i < 26; $i++) {
						$numberOfSeriee++;
						$cMonth = $month - $i;
						$cYear = $year;
						while ($cMonth < 1) {
							$cMonth += 12;
							$cYear--;
						}
						if ($i) {
							$cDay = '31';
						}
							else {
								$cDay = $day;
								if ($cDay < 10) {
									$cDay = '0'.(int)$cDay;
								}
							}
						if ($cMonth < 10) {
							$cMonth = '0'.(int)$cMonth;
						}
						$intervalsNames[$numberOfSeriee] = $months[(int)$cMonth]."".substr($cYear, -2);
						$intervalsData[$intervalsNames[$numberOfSeriee]] = ['Other' => 0];
						$intervalsData[$intervalsNames[$numberOfSeriee].'Summ'] = 0;
						$intervalsData[$intervalsNames[$numberOfSeriee].'SummPT3'] = 0;
						$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", $cYear, $cMonth, "01")." AND ".sprintf("%04d%02d%02d", $cYear, $cMonth, $cDay);
				$sqls[] = $sql;
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								$intervalsData[$intervalsNames[$numberOfSeriee].'SummPT3'] = $obj->sorter_summ;
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
						if ($params['sI']) {
							$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", $cYear, $cMonth, "01")." AND ".sprintf("%04d%02d%02d", $cYear, $cMonth, $cDay)." GROUP BY column_number_".$params['sI'];
				$sqls[] = $sql;
							if ($result = $conn->query($sql)) {
								while ($obj = $result->fetch_object()) {
									if (array_key_exists($obj->sorter_item, $sorters_name)) {
										$intervalsData[$intervalsNames[$numberOfSeriee]][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
										$intervalsData[$intervalsNames[$numberOfSeriee].'Summ'] += $obj->sorter_summ;
									}
										else {
											$intervalsData[$intervalsNames[$numberOfSeriee]]['Other'] += $obj->sorter_summ;
											if ($params['oos']) {
												$intervalsData[$intervalsNames[$numberOfSeriee].'Summ'] += $obj->sorter_summ;
											}
										}
								}
							}
								else {
									die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
								}
						}
							else {
								foreach ($params['dD'] as $key => $item) {
									$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", $cYear, $cMonth, "01")." AND ".sprintf("%04d%02d%02d", $cYear, $cMonth, $cDay)." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
				$sqls[] = $sql;
									if ($result = $conn->query($sql)) {
										while ($obj = $result->fetch_object()) {
											$intervalsData[$intervalsNames[$numberOfSeriee]][$key] = $obj->sorter_summ;
											$intervalsData[$intervalsNames[$numberOfSeriee].'Summ'] += $obj->sorter_summ;
										}
									}
										else {
											die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
										}
								}
							}
					}
				}
					else if ($params['iI'] == 2) {
						$sql = "SELECT YEAR(sorting_date) AS min_year FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter." ORDER BY sorting_date ASC LIMIT 0, 1";
				$sqls[] = $sql;
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								$min_year = $obj->min_year;
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
						$sql = "SELECT YEAR(sorting_date) AS max_year FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter." ORDER BY sorting_date DESC LIMIT 0, 1";
				$sqls[] = $sql;
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								$max_year = $obj->max_year;
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
						$numberOfSeriee = -1;
						$countOfSeries = $max_year - $min_year + 1;
						for ($i = 0; $i < $countOfSeries; $i++) {
							$numberOfSeriee++;
							$cYear = ($max_year - $i);
							$intervalsNames[$numberOfSeriee] = (string)$cYear;
							$intervalsData[$intervalsNames[$numberOfSeriee]] = ['Other' => 0];
							$intervalsData[$intervalsNames[$numberOfSeriee].'Summ'] = 0;
							$intervalsData[$intervalsNames[$numberOfSeriee].'SummPT3'] = 0;
							$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", $cYear, "01", "01")." AND ".sprintf("%04d%02d%02d", $cYear, "12", "31");
				$sqls[] = $sql;
							if ($result = $conn->query($sql)) {
								while ($obj = $result->fetch_object()) {
									$intervalsData[$intervalsNames[$numberOfSeriee].'SummPT3'] = $obj->sorter_summ;
								}
							}
								else {
									die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
								}
							if ($params['sI']) {
								$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", $cYear, "01", "01")." AND ".sprintf("%04d%02d%02d", $cYear, "12", "31")." GROUP BY column_number_".$params['sI'];
				$sqls[] = $sql;
								if ($result = $conn->query($sql)) {
									while ($obj = $result->fetch_object()) {
										if (array_key_exists($obj->sorter_item, $sorters_name)) {
											$intervalsData[$intervalsNames[$numberOfSeriee]][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
											$intervalsData[$intervalsNames[$numberOfSeriee].'Summ'] += $obj->sorter_summ;
										}
											else {
												$intervalsData[$intervalsNames[$numberOfSeriee]]['Other'] += $obj->sorter_summ;
												if ($params['oos']) {
													$intervalsData[$intervalsNames[$numberOfSeriee].'Summ'] += $obj->sorter_summ;
												}
											}
									}
								}
									else {
										die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
									}
							}
								else {
									foreach ($params['dD'] as $key => $item) {
										$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", $cYear, "01", "01")." AND ".sprintf("%04d%02d%02d", $cYear, "12", "31")." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
				$sqls[] = $sql;
										if ($result = $conn->query($sql)) {
											while ($obj = $result->fetch_object()) {
												$intervalsData[$intervalsNames[$numberOfSeriee]][$key] = $obj->sorter_summ;
												$intervalsData[$intervalsNames[$numberOfSeriee].'Summ'] += $obj->sorter_summ;
											}
										}
											else {
												die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
											}
									}
								}
						}
					}
					else {
						$numberOfSeriee = -1;
						$cYear = $year;
						$monthDiv = floor(($month-1)/3);
						for ($i = 0; $i <= $monthDiv; $i++) {
							$numberOfSeriee++;
							$cMonthBegin = 1 + ($monthDiv - $i) * 3;
							if ($i) {
								$cDay = '31';
								$cMonthEnd = $cMonthBegin + 2;
							}
								else {
									$cMonthEnd = $month;
									$cDay = $day;
									if ($cDay < 10) {
										$cDay = '0'.(int)$cDay;
									}
								}
							if ($cMonthEnd < 10) {
								$cMonthBegin = '0'.(int)$cMonthBegin;
								$cMonthEnd = '0'.(int)$cMonthEnd;
							}
							$intervalsNames[$numberOfSeriee] = $cYear." Q".($monthDiv - $i + 1);
							$intervalsData[$intervalsNames[$numberOfSeriee]] = ['Other' => 0];
							$intervalsData[$intervalsNames[$numberOfSeriee].'Summ'] = 0;
							$intervalsData[$intervalsNames[$numberOfSeriee].'SummPT3'] = 0;
							$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", $cYear, $cMonthBegin, "01")." AND ".sprintf("%04d%02d%02d", $cYear, $cMonthEnd, $cDay);
				$sqls[] = $sql;
							if ($result = $conn->query($sql)) {
								while ($obj = $result->fetch_object()) {
									$intervalsData[$intervalsNames[$numberOfSeriee].'SummPT3'] = $obj->sorter_summ;
								}
							}
								else {
									die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
								}
							if ($params['sI']) {
								$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", $cYear, $cMonthBegin, "01")." AND ".sprintf("%04d%02d%02d", $cYear, $cMonthEnd, $cDay)." GROUP BY column_number_".$params['sI'];
				$sqls[] = $sql;
								if ($result = $conn->query($sql)) {
									while ($obj = $result->fetch_object()) {
										if (array_key_exists($obj->sorter_item, $sorters_name)) {
											$intervalsData[$intervalsNames[$numberOfSeriee]][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
											$intervalsData[$intervalsNames[$numberOfSeriee].'Summ'] += $obj->sorter_summ;
										}
											else {
												$intervalsData[$intervalsNames[$numberOfSeriee]]['Other'] += $obj->sorter_summ;
												if ($params['oos']) {
													$intervalsData[$intervalsNames[$numberOfSeriee].'Summ'] += $obj->sorter_summ;
												}
											}
									}
								}
									else {
										die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
									}
							}
								else {
									foreach ($params['dD'] as $key => $item) {
										$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", $cYear, $cMonthBegin, "01")." AND ".sprintf("%04d%02d%02d", $cYear, $cMonthEnd, $cDay)." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
				$sqls[] = $sql;
										if ($result = $conn->query($sql)) {
											while ($obj = $result->fetch_object()) {
												$intervalsData[$intervalsNames[$numberOfSeriee]][$key] = $obj->sorter_summ;
												$intervalsData[$intervalsNames[$numberOfSeriee].'Summ'] += $obj->sorter_summ;
											}
										}
											else {
												die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
											}
									}
								}
						}
						$countOfSeries = 5 + $numberOfSeriee;
						for ($i = 1; $i < 3; $i++) {
							$cYear--;
							for ($j = 0; $j < 4; $j++) {
								$numberOfSeriee++;
								$cMonthBegin = 10 - 3 * $j;
								$cMonthEnd = $cMonthBegin + 2;
								if ($cMonthBegin < 10) {
									$cMonthBegin = '0'.(int)$cMonthBegin;
									$cMonthEnd = '0'.(int)$cMonthEnd;
								}
								$intervalsNames[$numberOfSeriee] = $cYear." Q".(4 - $j);
								$intervalsData[$intervalsNames[$numberOfSeriee]] = ['Other' => 0];
								$intervalsData[$intervalsNames[$numberOfSeriee].'Summ'] = 0;
								$intervalsData[$intervalsNames[$numberOfSeriee].'SummPT3'] = 0;
								$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", $cYear, $cMonthBegin, "01")." AND ".sprintf("%04d%02d%02d", $cYear, $cMonthEnd, "01");
				$sqls[] = $sql;
								if ($result = $conn->query($sql)) {
									while ($obj = $result->fetch_object()) {
										$intervalsData[$intervalsNames[$numberOfSeriee].'SummPT3'] = $obj->sorter_summ;
									}
								}
									else {
										die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
									}
								if ($params['sI']) {
									$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", $cYear, $cMonthBegin, "01")." AND ".sprintf("%04d%02d%02d", $cYear, $cMonthEnd, "01")." GROUP BY column_number_".$params['sI'];
				$sqls[] = $sql;
									if ($result = $conn->query($sql)) {
										while ($obj = $result->fetch_object()) {
											if (array_key_exists($obj->sorter_item, $sorters_name)) {
												$intervalsData[$intervalsNames[$numberOfSeriee]][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
												$intervalsData[$intervalsNames[$numberOfSeriee].'Summ'] += $obj->sorter_summ;
											}
												else {
													$intervalsData[$intervalsNames[$numberOfSeriee]]['Other'] += $obj->sorter_summ;
													if ($params['oos']) {
														$intervalsData[$intervalsNames[$numberOfSeriee].'Summ'] += $obj->sorter_summ;
													}
												}
										}
									}
										else {
											die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
										}
								}
									else {
										foreach ($params['dD'] as $key => $item) {
											$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", $cYear, $cMonthBegin, "01")." AND ".sprintf("%04d%02d%02d", $cYear, $cMonthEnd, "01")." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
				$sqls[] = $sql;
											if ($result = $conn->query($sql)) {
												while ($obj = $result->fetch_object()) {
													$intervalsData[$intervalsNames[$numberOfSeriee]][$key] = $obj->sorter_summ;
													$intervalsData[$intervalsNames[$numberOfSeriee].'Summ'] += $obj->sorter_summ;
												}
											}
												else {
													die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
												}
										}
									}
							}
						}
					}
				if (($other_summ > 0) && ($params['oos'])) {
					$sorters[] = 0;
					$sorters_name[0] = 'Other';
				}
				#	Подсчет процентного выражения и приведение величин к масштабу миллиардов
				foreach ($sorters_name as $key) {
					#	Проценты
					$monthToMonthData['currYPerc'][$key] = (($monthToMonthData['currYSumm'] != 0) ? (round($monthToMonthData['currY'][$key] / $monthToMonthData['currYSumm'] * 1000) / 10) : 0.0);
					$monthToMonthData['prevYPerc'][$key] = (($monthToMonthData['prevYSumm'] != 0) ? (round($monthToMonthData['prevY'][$key] / $monthToMonthData['prevYSumm'] * 1000) / 10) : 0.0);
					$monthToMonthData['lastYPerc'][$key] = (($monthToMonthData['lastYSumm'] != 0) ? (round($monthToMonthData['lastY'][$key] / $monthToMonthData['lastYSumm'] * 1000) / 10) : 0.0);
					$yearToDayData['currYPerc'][$key] = (($yearToDayData['currYSumm'] != 0) ? (round($yearToDayData['currY'][$key] / $yearToDayData['currYSumm'] * 1000) / 10) : 0.0);
					$yearToDayData['prevYPerc'][$key] = (($yearToDayData['prevYSumm'] != 0) ? (round($yearToDayData['prevY'][$key] / $yearToDayData['prevYSumm'] * 1000) / 10) : 0.0);
					$yearToDayData['lastYPerc'][$key] = (($yearToDayData['lastYSumm'] != 0) ? (round($yearToDayData['lastY'][$key] / $yearToDayData['lastYSumm'] * 1000) / 10) : 0.0);
					$monthToMonthData['currYPercPT3'][$key] = (($monthToMonthData['currYSummPT3'] != 0) ? (round($monthToMonthData['currY'][$key] / $monthToMonthData['currYSummPT3'] * 1000) / 10) : 0.0);
					$monthToMonthData['prevYPercPT3'][$key] = (($monthToMonthData['prevYSummPT3'] != 0) ? (round($monthToMonthData['prevY'][$key] / $monthToMonthData['prevYSummPT3'] * 1000) / 10) : 0.0);
					$monthToMonthData['lastYPercPT3'][$key] = (($monthToMonthData['lastYSummPT3'] != 0) ? (round($monthToMonthData['lastY'][$key] / $monthToMonthData['lastYSummPT3'] * 1000) / 10) : 0.0);
					$yearToDayData['currYPercPT3'][$key] = (($yearToDayData['currYSummPT3'] != 0) ? (round($yearToDayData['currY'][$key] / $yearToDayData['currYSummPT3'] * 1000) / 10) : 0.0);
					$yearToDayData['prevYPercPT3'][$key] = (($yearToDayData['prevYSummPT3'] != 0) ? (round($yearToDayData['prevY'][$key] / $yearToDayData['prevYSummPT3'] * 1000) / 10) : 0.0);
					$yearToDayData['lastYPercPT3'][$key] = (($yearToDayData['lastYSummPT3'] != 0) ? (round($yearToDayData['lastY'][$key] / $yearToDayData['lastYSummPT3'] * 1000) / 10) : 0.0);
					#	Масштабирование
					if (!($params['sP'])) {
						$monthToMonthData['currY'][$key] = round($monthToMonthData['currY'][$key] / $msscht2) / 10000;
						$monthToMonthData['prevY'][$key] = round($monthToMonthData['prevY'][$key] / $msscht2) / 10000;
						$monthToMonthData['lastY'][$key] = round($monthToMonthData['lastY'][$key] / $msscht2) / 10000;
						$yearToDayData['currY'][$key] = round($yearToDayData['currY'][$key] / $msscht2) / 10000;
						$yearToDayData['prevY'][$key] = round($yearToDayData['prevY'][$key] / $msscht2) / 10000;
						$yearToDayData['lastY'][$key] = round($yearToDayData['lastY'][$key] / $msscht2) / 10000;
					} else if ($params['sP'] == 1) {
						$monthToMonthData['currY'][$key] = round($monthToMonthData['currY'][$key] / $msscht2) / 10000;
						$monthToMonthData['prevY'][$key] = round($monthToMonthData['prevY'][$key] / $msscht2) / 10000;
						$monthToMonthData['lastY'][$key] = round($monthToMonthData['lastY'][$key] / $msscht2) / 10000;
						$yearToDayData['currY'][$key] = round($yearToDayData['currY'][$key] / $msscht2) / 10000;
						$yearToDayData['prevY'][$key] = round($yearToDayData['prevY'][$key] / $msscht2) / 10000;
						$yearToDayData['lastY'][$key] = round($yearToDayData['lastY'][$key] / $msscht2) / 10000;
					}
					#	Проценты и масштабирование для интервального графика
					foreach ($intervalsNames as $ival) {
						$intervalsData[$ival.'Perc'][$key] = (($intervalsData[$ival.'Summ'] != 0) ? (round($intervalsData[$ival][$key] / $intervalsData[$ival.'Summ'] * 1000) / 10) : 0.0);
						$intervalsData[$ival.'PercPT3'][$key] = (($intervalsData[$ival.'SummPT3'] != 0) ? (round($intervalsData[$ival][$key] / $intervalsData[$ival.'SummPT3'] * 1000) / 10) : 0.0);
						if (!($params['sP'])) {
							$intervalsData[$ival][$key] = round($intervalsData[$ival][$key] / $msscht2) / 10000;
						} else if ($params['sP'] == 1) {
							$intervalsData[$ival][$key] = round($intervalsData[$ival][$key] / $msscht2) / 10000;
						}
					}
					
				}
				#	Масштабирование суммарных данных
				if (!($params['sP'])) {
					$monthToMonthData['currYSumm'] = round($monthToMonthData['currYSumm'] / $msscht1) / 10;
					$monthToMonthData['prevYSumm'] = round($monthToMonthData['prevYSumm'] / $msscht1) / 10;
					$monthToMonthData['lastYSumm'] = round($monthToMonthData['lastYSumm'] / $msscht1) / 10;
					$yearToDayData['currYSumm'] = round($yearToDayData['currYSumm'] / $msscht1) / 10;
					$yearToDayData['prevYSumm'] = round($yearToDayData['prevYSumm'] / $msscht1) / 10;
					$yearToDayData['lastYSumm'] = round($yearToDayData['lastYSumm'] / $msscht1) / 10;
					$monthToMonthData['currYSummPT3'] = round($monthToMonthData['currYSummPT3'] / $msscht1) / 10;
					$monthToMonthData['prevYSummPT3'] = round($monthToMonthData['prevYSummPT3'] / $msscht1) / 10;
					$monthToMonthData['lastYSummPT3'] = round($monthToMonthData['lastYSummPT3'] / $msscht1) / 10;
					$yearToDayData['currYSummPT3'] = round($yearToDayData['currYSummPT3'] / $msscht1) / 10;
					$yearToDayData['prevYSummPT3'] = round($yearToDayData['prevYSummPT3'] / $msscht1) / 10;
					$yearToDayData['lastYSummPT3'] = round($yearToDayData['lastYSummPT3'] / $msscht1) / 10;
					foreach ($intervalsNames as $ival) {
						$intervalsData[$ival.'Summ'] = round($intervalsData[$ival.'Summ'] / $msscht1) / 10;
						$intervalsData[$ival.'SummPT3'] = round($intervalsData[$ival.'SummPT3'] / $msscht1) / 10;
					}
				} else if ($params['sP'] == 1) {
					$monthToMonthData['currYSumm'] = round($monthToMonthData['currYSumm'] / $msscht1) / 10;
					$monthToMonthData['prevYSumm'] = round($monthToMonthData['prevYSumm'] / $msscht1) / 10;
					$monthToMonthData['lastYSumm'] = round($monthToMonthData['lastYSumm'] / $msscht1) / 10;
					$yearToDayData['currYSumm'] = round($yearToDayData['currYSumm'] / $msscht1) / 10;
					$yearToDayData['prevYSumm'] = round($yearToDayData['prevYSumm'] / $msscht1) / 10;
					$yearToDayData['lastYSumm'] = round($yearToDayData['lastYSumm'] / $msscht1) / 10;
					$monthToMonthData['currYSummPT3'] = round($monthToMonthData['currYSummPT3'] / $msscht1) / 10;
					$monthToMonthData['prevYSummPT3'] = round($monthToMonthData['prevYSummPT3'] / $msscht1) / 10;
					$monthToMonthData['lastYSummPT3'] = round($monthToMonthData['lastYSummPT3'] / $msscht1) / 10;
					$yearToDayData['currYSummPT3'] = round($yearToDayData['currYSummPT3'] / $msscht1) / 10;
					$yearToDayData['prevYSummPT3'] = round($yearToDayData['prevYSummPT3'] / $msscht1) / 10;
					$yearToDayData['lastYSummPT3'] = round($yearToDayData['lastYSummPT3'] / $msscht1) / 10;
					foreach ($intervalsNames as $ival) {
						$intervalsData[$ival.'Summ'] = round($intervalsData[$ival.'Summ'] / $msscht1) / 10;
						$intervalsData[$ival.'SummPT3'] = round($intervalsData[$ival.'SummPT3'] / $msscht1) / 10;
					}
				}
				#	Определение величин приращения (для легенды)/ формирование данных легенды
				$legends = [];
				if (0) {//($params['gT'] === '2') {//$params['gT'] === '2'	??? было iI
					$legends['up'] = [$monthToMonthData['prevYSummPT3'], $monthToMonthData['currYSummPT3'], $yearToDayData['prevYSummPT3'], $yearToDayData['currYSummPT3'], $mst_string];
					$legends['down'] = [
						(($monthToMonthData['lastYSummPT3'] != 0) ? (round((($monthToMonthData['prevYSummPT3'] - $monthToMonthData['lastYSummPT3']) / $monthToMonthData['lastYSummPT3']) * 1000) / 10) : 100.0),
						(($monthToMonthData['prevYSummPT3'] != 0) ? (round((($monthToMonthData['currYSummPT3'] - $monthToMonthData['prevYSummPT3']) / $monthToMonthData['prevYSummPT3']) * 1000) / 10) : 100.0),
						(($yearToDayData['lastYSummPT3'] != 0) ? (round((($yearToDayData['prevYSummPT3'] - $yearToDayData['lastYSummPT3']) / $yearToDayData['lastYSummPT3']) * 1000) / 10) : 100.0),
						(($yearToDayData['prevYSummPT3'] != 0) ? (round((($yearToDayData['currYSummPT3'] - $yearToDayData['prevYSummPT3']) / $yearToDayData['prevYSummPT3']) * 1000) / 10) : 100.0),
						'+/- % SPLY'
					];
					$legends['color'] = [
						(($monthToMonthData['prevYSummPT3'] < $monthToMonthData['lastYSummPT3']) ? 'Red' : 'Green'),
						(($monthToMonthData['currYSummPT3'] < $monthToMonthData['prevYSummPT3']) ? 'Red' : 'Green'),
						(($yearToDayData['prevYSummPT3'] < $yearToDayData['lastYSummPT3']) ? 'Red' : 'Green'),
						(($yearToDayData['currYSummPT3'] < $yearToDayData['prevYSummPT3']) ? 'Red' : 'Green'),
						'Black'
					];
					for ($i = ($countOfSeries - 1); $i > -1; $i--) {
						$legends['up'][] = $intervalsData[$intervalsNames[$i].'SummPT3'];
						if ($params['iI'] == 1) {
							$legends['down'][] = (($intervalsData[$intervalsNames[$i + 12].'SummPT3'] != 0) ? (round((($intervalsData[$intervalsNames[$i].'SummPT3'] - $intervalsData[$intervalsNames[$i + 12].'SummPT3']) / $intervalsData[$intervalsNames[$i + 12].'SummPT3']) * 1000) / 10) : 100.0);
							$legends['color'][] = (($intervalsData[$intervalsNames[$i].'SummPT3'] < $intervalsData[$intervalsNames[$i + 12].'SummPT3']) ? 'Red' : 'Green');
						} else if ($params['iI'] == 2) {
							if ($i < ($countOfSeries - 1)) {
								$legends['down'][] = (($intervalsData[$intervalsNames[$i + 1].'SummPT3'] != 0) ? (round((($intervalsData[$intervalsNames[$i].'SummPT3'] - $intervalsData[$intervalsNames[$i + 1].'SummPT3']) / $intervalsData[$intervalsNames[$i + 1].'SummPT3']) * 1000) / 10) : 100.0);
								$legends['color'][] = (($intervalsData[$intervalsNames[$i].'SummPT3'] < $intervalsData[$intervalsNames[$i + 1].'SummPT3']) ? 'Red' : 'Green');
							} else {
								$legends['down'][] = 100.0;
								$legends['color'][] = 'Green';
							}
						} else {
							$legends['down'][] = (($intervalsData[$intervalsNames[$i + 4].'SummPT3'] != 0) ? (round((($intervalsData[$intervalsNames[$i].'SummPT3'] - $intervalsData[$intervalsNames[$i + 4].'SummPT3']) / $intervalsData[$intervalsNames[$i + 4].'SummPT3']) * 1000) / 10) : 100.0);
							$legends['color'][] = (($intervalsData[$intervalsNames[$i].'SummPT3'] < $intervalsData[$intervalsNames[$i + 4].'SummPT3']) ? 'Red' : 'Green');
						}
					}
				}
				else {
					$legends['up'] = [$monthToMonthData['prevYSumm'], $monthToMonthData['currYSumm'], $yearToDayData['prevYSumm'], $yearToDayData['currYSumm'], $mst_string];
					$legends['down'] = [
						(($monthToMonthData['lastYSumm'] != 0) ? (round((($monthToMonthData['prevYSumm'] - $monthToMonthData['lastYSumm']) / $monthToMonthData['lastYSumm']) * 1000) / 10) : 100.0),
						(($monthToMonthData['prevYSumm'] != 0) ? (round((($monthToMonthData['currYSumm'] - $monthToMonthData['prevYSumm']) / $monthToMonthData['prevYSumm']) * 1000) / 10) : 100.0),
						(($yearToDayData['lastYSumm'] != 0) ? (round((($yearToDayData['prevYSumm'] - $yearToDayData['lastYSumm']) / $yearToDayData['lastYSumm']) * 1000) / 10) : 100.0),
						(($yearToDayData['prevYSumm'] != 0) ? (round((($yearToDayData['currYSumm'] - $yearToDayData['prevYSumm']) / $yearToDayData['prevYSumm']) * 1000) / 10) : 100.0),
						'+/- % SPLY'
					];
					$legends['color'] = [
						(($monthToMonthData['prevYSumm'] < $monthToMonthData['lastYSumm']) ? 'Red' : 'Green'),
						(($monthToMonthData['currYSumm'] < $monthToMonthData['prevYSumm']) ? 'Red' : 'Green'),
						(($yearToDayData['prevYSumm'] < $yearToDayData['lastYSumm']) ? 'Red' : 'Green'),
						(($yearToDayData['currYSumm'] < $yearToDayData['prevYSumm']) ? 'Red' : 'Green'),
						'Black'
					];
					for ($i = ($countOfSeries - 1); $i > -1; $i--) {
						$legends['up'][] = $intervalsData[$intervalsNames[$i].'Summ'];
						if ($params['iI'] == 1) {
							$legends['down'][] = (($intervalsData[$intervalsNames[$i + 12].'Summ'] != 0) ? (round((($intervalsData[$intervalsNames[$i].'Summ'] - $intervalsData[$intervalsNames[$i + 12].'Summ']) / $intervalsData[$intervalsNames[$i + 12].'Summ']) * 1000) / 10) : 100.0);
							$legends['color'][] = (($intervalsData[$intervalsNames[$i].'Summ'] < $intervalsData[$intervalsNames[$i + 12].'Summ']) ? 'Red' : 'Green');
						} else if ($params['iI'] == 2) {
							if ($i < ($countOfSeries - 1)) {
								$legends['down'][] = (($intervalsData[$intervalsNames[$i + 1].'Summ'] != 0) ? (round((($intervalsData[$intervalsNames[$i].'Summ'] - $intervalsData[$intervalsNames[$i + 1].'Summ']) / $intervalsData[$intervalsNames[$i + 1].'Summ']) * 1000) / 10) : 100.0);
								$legends['color'][] = (($intervalsData[$intervalsNames[$i].'Summ'] < $intervalsData[$intervalsNames[$i + 1].'Summ']) ? 'Red' : 'Green');
							} else {
								$legends['down'][] = 100.0;
								$legends['color'][] = 'Green';
							}
						} else {
							$legends['down'][] = (($intervalsData[$intervalsNames[$i + 4].'Summ'] != 0) ? (round((($intervalsData[$intervalsNames[$i].'Summ'] - $intervalsData[$intervalsNames[$i + 4].'Summ']) / $intervalsData[$intervalsNames[$i + 4].'Summ']) * 1000) / 10) : 100.0);
							$legends['color'][] = (($intervalsData[$intervalsNames[$i].'Summ'] < $intervalsData[$intervalsNames[$i + 4].'Summ']) ? 'Red' : 'Green');
						}
					}
				}
				#	Формирование итогового ответа
				$results = [];
				$names = [];
				foreach ($sorters as $key) {
					$result = '{"mtmcv":"'.$monthToMonthData['currY'][$sorters_name[$key]].'","mtmcp":"'.$monthToMonthData['currYPerc'][$sorters_name[$key]].'","mtmcppt3":"'.$monthToMonthData['currYPercPT3'][$sorters_name[$key]].'","mtmpv":"'.$monthToMonthData['prevY'][$sorters_name[$key]].'","mtmpp":"'.$monthToMonthData['prevYPerc'][$sorters_name[$key]].'","mtmpppt3":"'.$monthToMonthData['prevYPercPT3'][$sorters_name[$key]].'","mtmlv":"'.$monthToMonthData['lastY'][$sorters_name[$key]].'","mtmlp":"'.$monthToMonthData['lastYPerc'][$sorters_name[$key]].'","mtmlppt3":"'.$monthToMonthData['lastYPercPT3'][$sorters_name[$key]].'","ytdcv":"'.$yearToDayData['currY'][$sorters_name[$key]].'","ytdcp":"'.$yearToDayData['currYPerc'][$sorters_name[$key]].'","ytdcppt3":"'.$yearToDayData['currYPercPT3'][$sorters_name[$key]].'","ytdpv":"'.$yearToDayData['prevY'][$sorters_name[$key]].'","ytdpp":"'.$yearToDayData['prevYPerc'][$sorters_name[$key]].'","ytdpppt3":"'.$yearToDayData['prevYPercPT3'][$sorters_name[$key]].'","ytdlv":"'.$yearToDayData['lastY'][$sorters_name[$key]].'","ytdlp":"'.$yearToDayData['lastYPerc'][$sorters_name[$key]].'","ytdlppt3":"'.$yearToDayData['lastYPercPT3'][$sorters_name[$key]].'","iD":[';
					for ($i = 0; $i < ($countOfSeries - 1); $i++) {
						$result .= '{"idn":"'.$intervalsNames[$i].'","idv":"'.$intervalsData[$intervalsNames[$i]][$sorters_name[$key]].'","idp":"'.$intervalsData[$intervalsNames[$i].'Perc'][$sorters_name[$key]].'","idppt3":"'.$intervalsData[$intervalsNames[$i].'PercPT3'][$sorters_name[$key]].'"},';
					}
					$result .= '{"idn":"'.$intervalsNames[$countOfSeries - 1].'","idv":"'.$intervalsData[$intervalsNames[$countOfSeries - 1]][$sorters_name[$key]].'","idp":"'.$intervalsData[$intervalsNames[$countOfSeries - 1].'Perc'][$sorters_name[$key]].'","idppt3":"'.$intervalsData[$intervalsNames[$countOfSeries - 1].'PercPT3'][$sorters_name[$key]].'"}]}';
					$results[] = $result;
					$names[] = $sorters_name[$key];
				}
				
				#echo '{"result":0,"sqls":["'.implode('","',$sqls).'"],"params":'.json_encode($params).'}';
				echo '{"result":1,"slide_legend":"'.$slide_legend.'","data":{"titlemtm0":"'.$titlemtm0.'","titlemtm1":"'.$titlemtm1.'","titleytd0":"'.$titleytd0.'","titleytd1":"'.$titleytd1.'","ss":['.implode(',',$sorters).'],"sNs":["'.implode('","',$names).'"],"rs":['.implode(',',$results).'],"lUs":["'.implode('","',$legends['up']).'"],"lDs":["'.implode('","',$legends['down']).'"],"lCs":["'.implode('","',$legends['color']).'"],"lMs":["'.implode('","',$monthToMonthLabels).'"],"lYs":["'.implode('","',$yearToDayLabels).'"]},"sqls":["'.implode('","',$sqls).'"]}';
			}
			else if ($page == 3) {
				#	Определить столбец "ответственный" за количества единиц
				$sql = "SELECT id FROM db_".$dbase."_xls_for_diagrams_columns WHERE value = 'Сумма Упаковок'";
				$sum_upak_col_number = 0;
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$sum_upak_col_number = $obj->id;
						break;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}

				$filters_queries = [];

				foreach ($params['fD'] as $item) {
					$filters_queries[] = " t.column_number_".$item['cI']." IN (".implode(', ',$item['vI']).")";
				}
				if ($filters_queries) {
					$filters_query = implode(" AND", $filters_queries);
					$main_filter = $filters_queries[0];
				}
					else {
						$filters_query = " 1";
						$main_filter = " 1";
					}

				$date_pieces = explode('-', $_POST['edate']);
				$year = $date_pieces[0];
				$month = $date_pieces[1];
				$day = $date_pieces[2];

				#	Определить лидеров по "сортеру"
				$sql = "SELECT SUM(t.".$summing.") AS sorter_summ, t.column_number_".$params['sI']." AS sorter_item, value AS item_name, (SELECT value FROM db_".$dbase."_xls_for_diagrams_columns WHERE id = ".$params['sI'].") AS sorter_name FROM db_".$dbase."_xls_for_diagrams_table AS t JOIN db_".$dbase."_xls_for_diagrams_unique_values ON (db_".$dbase."_xls_for_diagrams_unique_values.column_id = ".$params['sI']." AND db_".$dbase."_xls_for_diagrams_unique_values.id = t.column_number_".$params['sI'].") WHERE".$filters_query." GROUP BY t.column_number_".$params['sI']." ORDER BY sorter_summ DESC";
				$sorters = [];
				$sorters_summ = [];
				if ($result = $conn->query($sql)) {
					$other_summ = 0;
					if ($params['ors']) {
						$sorters = $params['osC'];
						foreach ($sorters as $item) {
							$sorters_summ[$item] = 0;
						}
						while ($obj = $result->fetch_object()) {
							if (in_array($obj->sorter_item, $sorters)) {
								$sorters_summ[$obj->sorter_item] = $obj->sorter_summ;
							}
								else {
									$other_summ += $obj->sorter_summ;
								}
						}
					}
						else {
							$count = 0;
							while ($obj = $result->fetch_object()) {
								$count++;
								if ($count < $params['sCs']) {
									if (($params['sU']) && (( $obj->sorter_name === 'Корпорация') || ( $obj->sorter_name === 'Корпорация - map')) && ($obj->item_name === 'U.M.')) {
										$count--;
										$other_summ += $obj->sorter_summ;
									}
										else {
											$sorters[] = $obj->sorter_item;
											$sorters_summ[$obj->sorter_item] = $obj->sorter_summ;
										}
								}
									else {
										if ((( $obj->sorter_name === 'Корпорация') && ($obj->sorter_item == $mainCorp)) || (( $obj->sorter_name === 'Корпорация - map') && ($obj->sorter_item == $mainCorp))) {
											$other_summ += $sorters_summ[$sorters[$params['sCs'] - 2]];
											$sorters[$params['sCs'] - 2] = $obj->sorter_item;
											$sorters_summ[$obj->sorter_item] = $obj->sorter_summ;
										}
											else {
												$other_summ += $obj->sorter_summ;
											}
									}
							}
						}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				if (!(count($sorters))) {
					echo '{"result":0,"error":"Фильтры отсеяли все значения"}';
					break;
				}

				#	Определить название "сортера"
				$sql = "SELECT value FROM db_".$dbase."_xls_for_diagrams_columns WHERE id = ".$params['sI'];
				$sorter_name = '';
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$sorter_name = $obj->value;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}

				#	Определить названия лидеров "сортера"
				$sql = "SELECT id, value FROM db_".$dbase."_xls_for_diagrams_unique_values WHERE column_id = ".$params['sI']." AND id IN (".implode(', ', $sorters).")";
				$sorters_name = [];
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						if (((($sorter_name === 'Корпорация') && ($obj->id == $mainCorp)) || (($sorter_name === 'Корпорация - map') && ($obj->id == $mainCorp))) && (!($params['ors']))) {
							array_splice($sorters, array_search($obj->id, $sorters), 1);
							array_unshift($sorters, $obj->id);
						}
						$sorters_name[$obj->id] = $obj->value;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}

				#	Определить данные для первого графика
				$premierGraphData = [];
				$premierGraphData['currY'] = ['Other' => 0];
				$premierGraphData['prevY'] = ['Other' => 0];
				$sql = "SELECT SUM(u.value) AS sorter_summ, t.column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table AS t JOIN db_".$dbase."_xls_for_diagrams_unique_values AS u ON (u.id = t.column_number_".$sum_upak_col_number.") WHERE t.sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-0), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-0), $month, $day)." GROUP BY t.column_number_".$params['sI'];
				#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						if (array_key_exists($obj->sorter_item, $sorters_name)) {
							$premierGraphData['currY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
						}
							else {
								$premierGraphData['currY']['Other'] += $obj->sorter_summ;
							}
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				$sql = "SELECT SUM(u.value) AS sorter_summ, t.column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table AS t JOIN db_".$dbase."_xls_for_diagrams_unique_values AS u ON (u.id = t.column_number_".$sum_upak_col_number.") WHERE t.sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-1), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-1), $month, $day)." GROUP BY t.column_number_".$params['sI'];
				#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						if (array_key_exists($obj->sorter_item, $sorters_name)) {
							$premierGraphData['prevY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
						}
							else {
								$premierGraphData['prevY']['Other'] += $obj->sorter_summ;
							}
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}

				#	Определить данные для второго графика
				$secondGraphData = [];
				$secondGraphData['currY'] = ['Other' => 0];
				$secondGraphData['prevY'] = ['Other' => 0];
				$sql = "SELECT SUM(t.".$summing.") AS sorter_summ, t.column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table AS t WHERE".$filters_query." AND t.sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-0), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-0), $month, $day)." GROUP BY t.column_number_".$params['sI'];
				#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						if (array_key_exists($obj->sorter_item, $sorters_name)) {
							$secondGraphData['currY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
						}
							else {
								$secondGraphData['currY']['Other'] += $obj->sorter_summ;
							}
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				$sql = "SELECT SUM(t.".$summing.") AS sorter_summ, t.column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table AS t WHERE".$filters_query." AND t.sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-1), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-1), $month, $day)." GROUP BY t.column_number_".$params['sI'];
				#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						if (array_key_exists($obj->sorter_item, $sorters_name)) {
							$secondGraphData['prevY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
						}
							else {
								$secondGraphData['prevY']['Other'] += $obj->sorter_summ;
							}
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}

				if (($other_summ > 0) && ($params['oos'])) {
					$sorters[] = 0;
					$sorters_name[0] = 'Other';
				}
				#	Определить данные для третьего графика
				$thirdGraphData = [];
				$thirdGraphData['currY'] = [];
				$thirdGraphData['prevY'] = [];
				foreach ($sorters_name as $key) {
					$thirdGraphData['currY'][$key] = (($premierGraphData['currY'][$key] != 0) ? ($secondGraphData['currY'][$key] / $premierGraphData['currY'][$key]) : 0.0);
					$thirdGraphData['prevY'][$key] = (($premierGraphData['prevY'][$key] != 0) ? ($secondGraphData['prevY'][$key] / $premierGraphData['prevY'][$key]) : 0.0);
				}

				#	Подсчет процентного выражения
				foreach ($sorters_name as $key) {
					$premierGraphData['perc'][$key] = (($premierGraphData['prevY'][$key] != 0) ? (round(($premierGraphData['currY'][$key] - $premierGraphData['prevY'][$key]) / $premierGraphData['prevY'][$key] * 1000) / 10) : 0.0);
					$secondGraphData['perc'][$key] = (($secondGraphData['prevY'][$key] != 0) ? (round(($secondGraphData['currY'][$key] - $secondGraphData['prevY'][$key]) / $secondGraphData['prevY'][$key] * 1000) / 10) : 0.0);
					$thirdGraphData['perc'][$key] = (($thirdGraphData['prevY'][$key] != 0) ? (round(($thirdGraphData['currY'][$key] - $thirdGraphData['prevY'][$key]) / $thirdGraphData['prevY'][$key] * 1000) / 10) : 0.0);
				}

				#	Формирование итогового ответа
				$names = [];
				$data_0_m = [];
				$data_0_p = [];
				$data_1_m = [];
				$data_1_p = [];
				$data_2_m = [];
				$data_2_p = [];
				foreach ($sorters as $key) {
					if ($premierGraphData['perc'][$sorters_name[$key]] < 0) {
						$data_0_m[] = $premierGraphData['perc'][$sorters_name[$key]];
						$data_0_p[] = 0;
					} else if ($premierGraphData['perc'][$sorters_name[$key]] > 0) {
						$data_0_p[] = $premierGraphData['perc'][$sorters_name[$key]];
						$data_0_m[] = 0;
					} else {
						$data_0_m[] = 0;
						$data_0_p[] = 0.01;
					}
					if ($secondGraphData['perc'][$sorters_name[$key]] < 0) {
						$data_1_m[] = $secondGraphData['perc'][$sorters_name[$key]];
						$data_1_p[] = 0;
					} else if ($secondGraphData['perc'][$sorters_name[$key]] > 0) {
						$data_1_p[] = $secondGraphData['perc'][$sorters_name[$key]];
						$data_1_m[] = 0;
					} else {
						$data_1_m[] = 0;
						$data_1_p[] = 0.01;
					}
					if ($thirdGraphData['perc'][$sorters_name[$key]] < 0) {
						$data_2_m[] = $thirdGraphData['perc'][$sorters_name[$key]];
						$data_2_p[] = 0;
					} else if ($thirdGraphData['perc'][$sorters_name[$key]] > 0) {
						$data_2_p[] = $thirdGraphData['perc'][$sorters_name[$key]];
						$data_2_m[] = 0;
					} else {
						$data_2_m[] = 0;
						$data_2_p[] = 0.01;
					}
					$names[] = $sorters_name[$key];
				}
				echo '{"result":1,"slide_legend":"'.$slide_legend.'","data":{"ss":['.implode(',',$sorters).'],"sNs":["'.implode('","',$names).'"],"datasets_0":[{"backgroundColor":"rgba(6, 213, 10, 1)","data":['.implode(',',$data_0_p).']},{"backgroundColor":"rgba(255, 8, 0, 1)","data":['.implode(',',$data_0_m).']}],"datasets_1":[{"backgroundColor":"rgba(6, 213, 10, 1)","data":['.implode(',',$data_1_p).']},{"backgroundColor":"rgba(255, 8, 0, 1)","data":['.implode(',',$data_1_m).']}],"datasets_2":[{"backgroundColor":"rgba(6, 213, 10, 1)","data":['.implode(',',$data_2_p).']},{"backgroundColor":"rgba(255, 8, 0, 1)","data":['.implode(',',$data_2_m).']}]}}';
			}
			else if ($page == 4) {
				#	Определить столбец "ответственный" за количества единиц
				$sql = "SELECT id FROM db_".$dbase."_xls_for_diagrams_columns WHERE value = 'Сумма Упаковок'";
				$sum_upak_col_number = 0;
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$sum_upak_col_number = $obj->id;
						break;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}

				$filters_queries = [];

				foreach ($params['fD'] as $item) {
					$filters_queries[] = " t.column_number_".$item['cI']." IN (".implode(', ',$item['vI']).")";
				}
				if ($filters_queries) {
					$filters_query = implode(" AND", $filters_queries);
					$main_filter = $filters_queries[0];
				}
					else {
						$filters_query = " 1";
						$main_filter = " 1";
					}

				$date_pieces = explode('-', $_POST['edate']);
				$year = $date_pieces[0];
				$month = $date_pieces[1];
				$day = $date_pieces[2];

				#	Определить лидеров по "сортеру"
				$sql = "SELECT SUM(t.".$summing.") AS sorter_summ, t.column_number_".$params['sI']." AS sorter_item, value AS item_name, (SELECT value FROM db_".$dbase."_xls_for_diagrams_columns WHERE id = ".$params['sI'].") AS sorter_name FROM db_".$dbase."_xls_for_diagrams_table AS t JOIN db_".$dbase."_xls_for_diagrams_unique_values ON (db_".$dbase."_xls_for_diagrams_unique_values.column_id = ".$params['sI']." AND db_".$dbase."_xls_for_diagrams_unique_values.id = t.column_number_".$params['sI'].") WHERE".$filters_query." GROUP BY t.column_number_".$params['sI']." ORDER BY sorter_summ DESC";
				$sorters = [];
				$sorters_summ = [];
				if ($result = $conn->query($sql)) {
					$other_summ = 0;
					if ($params['ors']) {
						$sorters = $params['osC'];
						foreach ($sorters as $item) {
							$sorters_summ[$item] = 0;
						}
						while ($obj = $result->fetch_object()) {
							if (in_array($obj->sorter_item, $sorters)) {
								$sorters_summ[$obj->sorter_item] = $obj->sorter_summ;
							}
								else {
									$other_summ += $obj->sorter_summ;
								}
						}
					}
						else {
							$count = 0;
							while ($obj = $result->fetch_object()) {
								$count++;
								if ($count < $params['sCs']) {
									if (($params['sU']) && (( $obj->sorter_name === 'Корпорация') || ( $obj->sorter_name === 'Корпорация - map')) && ($obj->item_name === 'U.M.')) {
										$count--;
										$other_summ += $obj->sorter_summ;
									}
										else {
											$sorters[] = $obj->sorter_item;
											$sorters_summ[$obj->sorter_item] = $obj->sorter_summ;
										}
								}
									else {
										if ((( $obj->sorter_name === 'Корпорация') && ($obj->sorter_item == $mainCorp)) || (( $obj->sorter_name === 'Корпорация - map') && ($obj->sorter_item == $mainCorp))) {
											$other_summ += $sorters_summ[$sorters[$params['sCs'] - 2]];
											$sorters[$params['sCs'] - 2] = $obj->sorter_item;
											$sorters_summ[$obj->sorter_item] = $obj->sorter_summ;
										}
											else {
												$other_summ += $obj->sorter_summ;
											}
									}
							}
						}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				if (!(count($sorters))) {
					echo '{"result":0,"error":"Фильтры отсеяли все значения"}';
					break;
				}

				#	Определить название "сортера"
				$sql = "SELECT value FROM db_".$dbase."_xls_for_diagrams_columns WHERE id = ".$params['sI'];
				$sorter_name = '';
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$sorter_name = $obj->value;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}

				#	Определить названия лидеров "сортера"
				$sql = "SELECT id, value FROM db_".$dbase."_xls_for_diagrams_unique_values WHERE column_id = ".$params['sI']." AND id IN (".implode(', ', $sorters).")";
				$sorters_name = [];
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						if (((($sorter_name === 'Корпорация') && ($obj->id == $mainCorp)) || (($sorter_name === 'Корпорация - map') && ($obj->id == $mainCorp))) && (!($params['ors']))) {
							array_splice($sorters, array_search($obj->id, $sorters), 1);
							array_unshift($sorters, $obj->id);
						}
						$sorters_name[$obj->id] = $obj->value;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}

				#	Определить данные для первого графика
				$premierGraphData = [];
				$premierGraphData['currY'] = ['Other' => 0];
				$premierGraphData['prevY'] = ['Other' => 0];
				$sql = "SELECT SUM(u.value) AS sorter_summ, t.column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table AS t JOIN db_".$dbase."_xls_for_diagrams_unique_values AS u ON (u.id = t.column_number_".$sum_upak_col_number.") WHERE t.sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-0) : ($year-1)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-0), $month, "31")." GROUP BY t.column_number_".$params['sI'];
				#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						if (array_key_exists($obj->sorter_item, $sorters_name)) {
							$premierGraphData['currY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
						}
							else {
								$premierGraphData['currY']['Other'] += $obj->sorter_summ;
							}
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				$sql = "SELECT SUM(u.value) AS sorter_summ, t.column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table AS t JOIN db_".$dbase."_xls_for_diagrams_unique_values AS u ON (u.id = t.column_number_".$sum_upak_col_number.") WHERE t.sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-1) : ($year-2)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-1), $month, "31")." GROUP BY t.column_number_".$params['sI'];
				#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						if (array_key_exists($obj->sorter_item, $sorters_name)) {
							$premierGraphData['prevY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
						}
							else {
								$premierGraphData['prevY']['Other'] += $obj->sorter_summ;
							}
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}

				#	Определить данные для второго графика
				$secondGraphData = [];
				$secondGraphData['currY'] = ['Other' => 0];
				$secondGraphData['prevY'] = ['Other' => 0];
				$sql = "SELECT SUM(t.".$summing.") AS sorter_summ, t.column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table AS t WHERE".$filters_query." AND t.sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-0) : ($year-1)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-0), $month, "31")." GROUP BY t.column_number_".$params['sI'];
				#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						if (array_key_exists($obj->sorter_item, $sorters_name)) {
							$secondGraphData['currY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
						}
							else {
								$secondGraphData['currY']['Other'] += $obj->sorter_summ;
							}
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				$sql = "SELECT SUM(t.".$summing.") AS sorter_summ, t.column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table AS t WHERE".$filters_query." AND t.sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-1) : ($year-2)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-1), $month, "31")." GROUP BY t.column_number_".$params['sI'];
				#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						if (array_key_exists($obj->sorter_item, $sorters_name)) {
							$secondGraphData['prevY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
						}
							else {
								$secondGraphData['prevY']['Other'] += $obj->sorter_summ;
							}
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}

				if (($other_summ > 0) && ($params['oos'])) {
					$sorters[] = 0;
					$sorters_name[0] = 'Other';
				}
				#	Определить данные для третьего графика
				$thirdGraphData = [];
				$thirdGraphData['currY'] = [];
				$thirdGraphData['prevY'] = [];
				foreach ($sorters_name as $key) {
					$thirdGraphData['currY'][$key] = (($premierGraphData['currY'][$key] != 0) ? ($secondGraphData['currY'][$key] / $premierGraphData['currY'][$key]) : 0.0);
					$thirdGraphData['prevY'][$key] = (($premierGraphData['prevY'][$key] != 0) ? ($secondGraphData['prevY'][$key] / $premierGraphData['prevY'][$key]) : 0.0);
				}

				#	Подсчет процентного выражения
				foreach ($sorters_name as $key) {
					$premierGraphData['perc'][$key] = (($premierGraphData['prevY'][$key] != 0) ? (round(($premierGraphData['currY'][$key] - $premierGraphData['prevY'][$key]) / $premierGraphData['prevY'][$key] * 1000) / 10) : 0.0);
					$secondGraphData['perc'][$key] = (($secondGraphData['prevY'][$key] != 0) ? (round(($secondGraphData['currY'][$key] - $secondGraphData['prevY'][$key]) / $secondGraphData['prevY'][$key] * 1000) / 10) : 0.0);
					$thirdGraphData['perc'][$key] = (($thirdGraphData['prevY'][$key] != 0) ? (round(($thirdGraphData['currY'][$key] - $thirdGraphData['prevY'][$key]) / $thirdGraphData['prevY'][$key] * 1000) / 10) : 0.0);
				}

				#	Формирование итогового ответа
				$names = [];
				$data_0_m = [];
				$data_0_p = [];
				$data_1_m = [];
				$data_1_p = [];
				$data_2_m = [];
				$data_2_p = [];
				foreach ($sorters as $key) {
					if ($premierGraphData['perc'][$sorters_name[$key]] < 0) {
						$data_0_m[] = $premierGraphData['perc'][$sorters_name[$key]];
						$data_0_p[] = 0;
					} else if ($premierGraphData['perc'][$sorters_name[$key]] > 0) {
						$data_0_p[] = $premierGraphData['perc'][$sorters_name[$key]];
						$data_0_m[] = 0;
					} else {
						$data_0_m[] = 0;
						$data_0_p[] = 0.01;
					}
					if ($secondGraphData['perc'][$sorters_name[$key]] < 0) {
						$data_1_m[] = $secondGraphData['perc'][$sorters_name[$key]];
						$data_1_p[] = 0;
					} else if ($secondGraphData['perc'][$sorters_name[$key]] > 0) {
						$data_1_p[] = $secondGraphData['perc'][$sorters_name[$key]];
						$data_1_m[] = 0;
					} else {
						$data_1_m[] = 0;
						$data_1_p[] = 0.01;
					}
					if ($thirdGraphData['perc'][$sorters_name[$key]] < 0) {
						$data_2_m[] = $thirdGraphData['perc'][$sorters_name[$key]];
						$data_2_p[] = 0;
					} else if ($thirdGraphData['perc'][$sorters_name[$key]] > 0) {
						$data_2_p[] = $thirdGraphData['perc'][$sorters_name[$key]];
						$data_2_m[] = 0;
					} else {
						$data_2_m[] = 0;
						$data_2_p[] = 0.01;
					}
					$names[] = $sorters_name[$key];
				}
				echo '{"result":1,"slide_legend":"'.$slide_legend.'","data":{"ss":['.implode(',',$sorters).'],"sNs":["'.implode('","',$names).'"],"datasets_0":[{"backgroundColor":"rgba(6, 213, 10, 1)","data":['.implode(',',$data_0_p).']},{"backgroundColor":"rgba(255, 8, 0, 1)","data":['.implode(',',$data_0_m).']}],"datasets_1":[{"backgroundColor":"rgba(6, 213, 10, 1)","data":['.implode(',',$data_1_p).']},{"backgroundColor":"rgba(255, 8, 0, 1)","data":['.implode(',',$data_1_m).']}],"datasets_2":[{"backgroundColor":"rgba(6, 213, 10, 1)","data":['.implode(',',$data_2_p).']},{"backgroundColor":"rgba(255, 8, 0, 1)","data":['.implode(',',$data_2_m).']}]}}';
			}
			else if ($page == 7) {
				$filters_queries = [];

				foreach ($params['fD'] as $item) {
					$filters_queries[] = " column_number_".$item['cI']." IN (".implode(', ',$item['vI']).")";
				}
				if ($filters_queries) {
					$filters_query = implode(" AND", $filters_queries);
					$main_filter = $filters_queries[0];
				}
					else {
						$filters_query = " 1";
						$main_filter = " 1";
					}

				$date_pieces = explode('-', $_POST['edate']);
				$year = $date_pieces[0];
				$month = $date_pieces[1];
				$day = $date_pieces[2];

				$sorters = [];
				$sorters_summ = [];
				$sorters_name = [];
				$other_summ = 0;
				if ($params['sI']) {
					#	Определить лидеров по "сортеру"
					$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item, value AS item_name, (SELECT value FROM db_".$dbase."_xls_for_diagrams_columns WHERE id = ".$params['sI'].") AS sorter_name FROM db_".$dbase."_xls_for_diagrams_table JOIN db_".$dbase."_xls_for_diagrams_unique_values ON (db_".$dbase."_xls_for_diagrams_unique_values.column_id = ".$params['sI']." AND db_".$dbase."_xls_for_diagrams_unique_values.id = db_".$dbase."_xls_for_diagrams_table.column_number_".$params['sI'].") WHERE".$filters_query." GROUP BY column_number_".$params['sI']." ORDER BY sorter_summ DESC";
					if ($result = $conn->query($sql)) {
						if ($params['ors']) {
							$sorters = $params['osC'];
							foreach ($sorters as $item) {
								$sorters_summ[$item] = 0;
							}
							while ($obj = $result->fetch_object()) {
								if (in_array($obj->sorter_item, $sorters)) {
									$sorters_summ[$obj->sorter_item] = $obj->sorter_summ;
								}
									else {
										$other_summ += $obj->sorter_summ;
									}
							}
						}
							else {
								$count = 0;
								while ($obj = $result->fetch_object()) {
									$count++;
									if ($count < $params['sCs']) {
										if (($params['sU']) && (( $obj->sorter_name == 'Корпорация') || ( $obj->sorter_name == 'Корпорация - map')) && ($obj->item_name == 'U.M.')) {
											$count--;
											$other_summ += $obj->sorter_summ;
										}
											else {
												$sorters[] = $obj->sorter_item;
												$sorters_summ[$obj->sorter_item] = $obj->sorter_summ;
											}
									}
										else {
											if ((( $obj->sorter_name == 'Корпорация') && ($obj->sorter_item == $mainCorp)) || (( $obj->sorter_name === 'Корпорация - map') && ($obj->sorter_item == $mainCorp))) {
												$other_summ += $sorters_summ[$sorters[$params['sCs'] - 2]];
												$sorters[$params['sCs'] - 2] = $obj->sorter_item;
												$sorters_summ[$obj->sorter_item] = $obj->sorter_summ;
											}
												else {
													$other_summ += $obj->sorter_summ;
												}
										}
								}
							}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
				}
					else {
						foreach ($params['dD'] as $key => $item) {
							$sorters[] = $key;
							$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
							$sorter_name = 'Ценовые диапазоны';
							if ($result = $conn->query($sql)) {
								while ($obj = $result->fetch_object()) {
									$sorters_summ[$key] = $obj->sorter_summ;
								}
							}
								else {
									die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
								}
							$sorters_name[$key] = $key;
						}
					}

				if (!(count($sorters))) {
					echo '{"result":0,"error":"Фильтры отсеяли все значения"}';
					break;
				}

				if ($params['sI']) {
					#	Определить название "сортера"
					$sql = "SELECT value FROM db_".$dbase."_xls_for_diagrams_columns WHERE id = ".$params['sI'];
					$sorter_name = '';
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							$sorter_name = $obj->value;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}

					#	Определить названия лидеров "сортера"
					$sql = "SELECT id, value FROM db_".$dbase."_xls_for_diagrams_unique_values WHERE column_id = ".$params['sI']." AND id IN (".implode(', ', $sorters).")";
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							if (((($sorter_name === 'Корпорация') && ($obj->id == $mainCorp)) || (($sorter_name === 'Корпорация - map') && ($obj->id == $mainCorp))) && (!($params['ors']))) {
								array_splice($sorters, array_search($obj->id, $sorters), 1);
								array_unshift($sorters, $obj->id);
							}
							$sorters_name[$obj->id] = $obj->value;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
				}

				#	Определить название второго "сортера"
				$sql = "SELECT value FROM db_".$dbase."_xls_for_diagrams_columns WHERE id = ".$params['sCD'];
				$sub_sorters_name = [];
				$sub_sorters = [];
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$sub_sorters_name[0] = 'Total';
						$sub_sorters[] = 0;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}

				#	Определить названия категорий второго "сортера"
				
				#	тут добавить фильтр полученных ИТОГОВЫХ айди по выбранному списку - остальное сработает автоматически (!), по поводу упорядочивания - подумать над этим, вероятнее в пост обработке
				
				
				$sql = "SELECT id, value FROM db_".$dbase."_xls_for_diagrams_unique_values WHERE id IN (SELECT DISTINCT column_number_".$params['sCD']." FROM `db_".$dbase."_xls_for_diagrams_table` WHERE".$filters_query.") AND id IN (".implode(',',$params['ssC']).")";
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$sub_sorters_name[$obj->id] = $obj->value;
						#$sub_sorters[] = $obj->id;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				foreach ($params['ssC'] as $ss_id) {
					$sub_sorters[] = $ss_id;
				}

				$months = ['', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

				#	Определить данные для графиков YTD
				$yearToDayData = [];
				$ytdInd = 0;
				foreach ($sub_sorters as $ss_key) {
					$yearToDayData[$ss_key]['currY'] = ['Other' => 0];
					$yearToDayData[$ss_key]['prevY'] = ['Other' => 0];
					$yearToDayData[$ss_key]['lastY'] = ['Other' => 0];
					$yearToDayData[$ss_key]['currYSumm'] = 0;
					$yearToDayData[$ss_key]['prevYSumm'] = 0;
					$yearToDayData[$ss_key]['lastYSumm'] = 0;
					$yearToDayData[$ss_key]['currYSummPT3'] = 0;
					$yearToDayData[$ss_key]['prevYSummPT3'] = 0;
					$yearToDayData[$ss_key]['lastYSummPT3'] = 0;
					if ($ss_key) {
						$sorter_filter = " AND column_number_".$params['sCD']." = ".$ss_key;
						$ytdInd++;
						$yearToDayLabels[] = $ytdInd;
						$ytdInd++;
						$yearToDayLabels[] = $ytdInd;
					} else {
						$sorter_filter = " AND 1";
						$yearToDayLabels = [
							"Jan".substr(($year - 1), -2)."-".$months[(int)$month]."".substr(($year - 1), -2),
							"Jan".substr($year, -2)."-".$months[(int)$month]."".substr($year, -2),
							#"YTD".substr(($year - 1), -2)." ".$months[$month],
							#"YTD".substr($year, -2)." ".$months[$month],
							''
						];
					}
					$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-0), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-0), $month, $day);
					#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							$yearToDayData[$ss_key]['currYSummPT3'] = $obj->sorter_summ;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
					$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-1), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-1), $month, $day);
					#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							$yearToDayData[$ss_key]['prevYSummPT3'] = $obj->sorter_summ;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
					$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-2), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-2), $month, $day);
					#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							$yearToDayData[$ss_key]['lastYSummPT3'] = $obj->sorter_summ;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
					if ($params['sI']) {
						$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-0), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-0), $month, $day)." GROUP BY column_number_".$params['sI'];
						#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								if (array_key_exists($obj->sorter_item, $sorters_name)) {
									$yearToDayData[$ss_key]['currY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
									$yearToDayData[$ss_key]['currYSumm'] += $obj->sorter_summ;
								}
									else {
										$yearToDayData[$ss_key]['currY']['Other'] += $obj->sorter_summ;
										if ($params['oos']) {
											$yearToDayData[$ss_key]['currYSumm'] += $obj->sorter_summ;
										}
									}
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
						$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-1), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-1), $month, $day)." GROUP BY column_number_".$params['sI'];
						#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								if (array_key_exists($obj->sorter_item, $sorters_name)) {
									$yearToDayData[$ss_key]['prevY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
									$yearToDayData[$ss_key]['prevYSumm'] += $obj->sorter_summ;
								}
									else {
										$yearToDayData[$ss_key]['prevY']['Other'] += $obj->sorter_summ;
										if ($params['oos']) {
											$yearToDayData[$ss_key]['prevYSumm'] += $obj->sorter_summ;
										}
									}
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
						$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-2), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-2), $month, $day)." GROUP BY column_number_".$params['sI'];
						#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								if (array_key_exists($obj->sorter_item, $sorters_name)) {
									$yearToDayData[$ss_key]['lastY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
									$yearToDayData[$ss_key]['lastYSumm'] += $obj->sorter_summ;
								}
									else {
										$yearToDayData[$ss_key]['lastY']['Other'] += $obj->sorter_summ;
										if ($params['oos']) {
											$yearToDayData[$ss_key]['lastYSumm'] += $obj->sorter_summ;
										}
									}
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
					}
						else {
							foreach ($params['dD'] as $key => $item) {
								$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-0), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-0), $month, $day)." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
								#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
								if ($result = $conn->query($sql)) {
									while ($obj = $result->fetch_object()) {
										$yearToDayData[$ss_key]['currY'][$key] = $obj->sorter_summ;
										$yearToDayData[$ss_key]['currYSumm'] += $obj->sorter_summ;
									}
								}
									else {
										die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
									}
								$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-1), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-1), $month, $day)." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
								#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
								if ($result = $conn->query($sql)) {
									while ($obj = $result->fetch_object()) {
										$yearToDayData[$ss_key]['prevY'][$key] = $obj->sorter_summ;
										$yearToDayData[$ss_key]['prevYSumm'] += $obj->sorter_summ;
									}
								}
									else {
										die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
									}
								$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-2), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-2), $month, $day)." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
								#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
								if ($result = $conn->query($sql)) {
									while ($obj = $result->fetch_object()) {
										$yearToDayData[$ss_key]['lastY'][$key] = $obj->sorter_summ;
										$yearToDayData[$ss_key]['lastYSumm'] += $obj->sorter_summ;
									}
								}
									else {
										die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
									}
							}
						}

					if (($other_summ > 0) && ($sorters_name[0] != 'Other') && ($params['oos'])) {
						$sorters[] = 0;
						$sorters_name[0] = 'Other';
					}
					#	Подсчет процентного выражения и приведение величин к масштабу миллиардов
					foreach ($sorters_name as $key) {
						#	Проценты
						$yearToDayData[$ss_key]['currYPerc'][$key] = (($yearToDayData[$ss_key]['currYSumm'] != 0) ? (round($yearToDayData[$ss_key]['currY'][$key] / $yearToDayData[$ss_key]['currYSumm'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['prevYPerc'][$key] = (($yearToDayData[$ss_key]['prevYSumm'] != 0) ? (round($yearToDayData[$ss_key]['prevY'][$key] / $yearToDayData[$ss_key]['prevYSumm'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['lastYPerc'][$key] = (($yearToDayData[$ss_key]['lastYSumm'] != 0) ? (round($yearToDayData[$ss_key]['lastY'][$key] / $yearToDayData[$ss_key]['lastYSumm'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['currYPercPT3'][$key] = (($yearToDayData[$ss_key]['currYSummPT3'] != 0) ? (round($yearToDayData[$ss_key]['currY'][$key] / $yearToDayData[$ss_key]['currYSummPT3'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['prevYPercPT3'][$key] = (($yearToDayData[$ss_key]['prevYSummPT3'] != 0) ? (round($yearToDayData[$ss_key]['prevY'][$key] / $yearToDayData[$ss_key]['prevYSummPT3'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['lastYPercPT3'][$key] = (($yearToDayData[$ss_key]['lastYSummPT3'] != 0) ? (round($yearToDayData[$ss_key]['lastY'][$key] / $yearToDayData[$ss_key]['lastYSummPT3'] * 1000) / 10) : 0.0);
						if (!($params['sP'])) {
							#	Масштабирование
							$yearToDayData[$ss_key]['currY'][$key] = round($yearToDayData[$ss_key]['currY'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['prevY'][$key] = round($yearToDayData[$ss_key]['prevY'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['lastY'][$key] = round($yearToDayData[$ss_key]['lastY'][$key] / $msscht2) / 10000;
						} else if ($params['sP'] == 1) {
							#	Масштабирование
							$yearToDayData[$ss_key]['currY'][$key] = round($yearToDayData[$ss_key]['currY'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['prevY'][$key] = round($yearToDayData[$ss_key]['prevY'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['lastY'][$key] = round($yearToDayData[$ss_key]['lastY'][$key] / $msscht2) / 10000;
						}
					}
					#	Масштабирование суммарных данных
					/*if (!($params['sP'])) {
						$yearToDayData[$ss_key]['currYSumm'] = round($yearToDayData[$ss_key]['currYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['prevYSumm'] = round($yearToDayData[$ss_key]['prevYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['lastYSumm'] = round($yearToDayData[$ss_key]['lastYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['currYSummPT3'] = round($yearToDayData[$ss_key]['currYSummPT3'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['prevYSummPT3'] = round($yearToDayData[$ss_key]['prevYSummPT3'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['lastYSummPT3'] = round($yearToDayData[$ss_key]['lastYSummPT3'] / $msscht1) / 10;
					} else if ($params['sP'] == 1) {
						$yearToDayData[$ss_key]['currYSumm'] = round($yearToDayData[$ss_key]['currYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['prevYSumm'] = round($yearToDayData[$ss_key]['prevYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['lastYSumm'] = round($yearToDayData[$ss_key]['lastYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['currYSummPT3'] = round($yearToDayData[$ss_key]['currYSummPT3'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['prevYSummPT3'] = round($yearToDayData[$ss_key]['prevYSummPT3'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['lastYSummPT3'] = round($yearToDayData[$ss_key]['lastYSummPT3'] / $msscht1) / 10;
					}*/
					#	Определение величин приращения (для легенды)/ формирование данных легенды
				}

				switch ($params['sos']) {
					case 0: {
						break;
					}
					case 1: {
						for ($i = 1; $i < (count($sub_sorters) - 1); $i++) {
							$max = $yearToDayData[$sub_sorters[$i]]['prevYSumm'];
							$maxnum = $i;
							for ($j = ($i + 1); $j < count($sub_sorters); $j++) {
								if ($max < $yearToDayData[$sub_sorters[$j]]['prevYSumm']) {
									$max = $yearToDayData[$sub_sorters[$j]]['prevYSumm'];
									$maxnum = $j;
								}
							}
							if ($maxnum > $i) {
								$tmp = $sub_sorters[$i];
								$sub_sorters[$i] = $sub_sorters[$maxnum];
								$sub_sorters[$maxnum] = $tmp;
							}
						}
						break;
					}
					case 2: {
						for ($i = 1; $i < (count($sub_sorters) - 1); $i++) {
							$max = $yearToDayData[$sub_sorters[$i]]['currYSumm'];
							$maxnum = $i;
							for ($j = ($i + 1); $j < count($sub_sorters); $j++) {
								if ($max < $yearToDayData[$sub_sorters[$j]]['currYSumm']) {
									$max = $yearToDayData[$sub_sorters[$j]]['currYSumm'];
									$maxnum = $j;
								}
							}
							if ($maxnum > $i) {
								$tmp = $sub_sorters[$i];
								$sub_sorters[$i] = $sub_sorters[$maxnum];
								$sub_sorters[$maxnum] = $tmp;
							}
						}
						break;
					}
					case 3: {
						for ($i = 1; $i < (count($sub_sorters) - 1); $i++) {
							$max = $yearToDayData[$sub_sorters[$i]]['currYSumm'] + $yearToDayData[$sub_sorters[$i]]['prevYSumm'];
							$maxnum = $i;
							for ($j = ($i + 1); $j < count($sub_sorters); $j++) {
								if ($max < ($yearToDayData[$sub_sorters[$j]]['currYSumm'] + $yearToDayData[$sub_sorters[$j]]['prevYSumm'])) {
									$max = $yearToDayData[$sub_sorters[$j]]['currYSumm'] + $yearToDayData[$sub_sorters[$j]]['prevYSumm'];
									$maxnum = $j;
								}
							}
							if ($maxnum > $i) {
								$tmp = $sub_sorters[$i];
								$sub_sorters[$i] = $sub_sorters[$maxnum];
								$sub_sorters[$maxnum] = $tmp;
							}
						}
						break;
					}
				}

				$legends = [];
				$sub_names = [];
				foreach ($sub_sorters as $ss_key) {
					$sub_names[] = $sub_sorters_name[$ss_key];
					if ($params['gT'] === '2') {//$params['gT'] === '2'	??? было iI
						$legends['up'][] = $yearToDayData[$ss_key]['prevYSummPT3'];
						$legends['up'][] = $yearToDayData[$ss_key]['currYSummPT3'];
						$legends['down'][] = (($yearToDayData[$ss_key]['lastYSummPT3'] != 0) ? (round((($yearToDayData[$ss_key]['prevYSummPT3'] - $yearToDayData[$ss_key]['lastYSummPT3']) / $yearToDayData[$ss_key]['lastYSummPT3']) * 1000) / 10) : 0.0);
						$legends['down'][] = (($yearToDayData[$ss_key]['prevYSummPT3'] != 0) ? (round((($yearToDayData[$ss_key]['currYSummPT3'] - $yearToDayData[$ss_key]['prevYSummPT3']) / $yearToDayData[$ss_key]['prevYSummPT3']) * 1000) / 10) : 0.0);
						$legends['color'][] = (($yearToDayData[$ss_key]['prevYSummPT3'] < $yearToDayData[$ss_key]['lastYSummPT3']) ? 'Red' : 'Green');
						$legends['color'][] = (($yearToDayData[$ss_key]['currYSummPT3'] < $yearToDayData[$ss_key]['prevYSummPT3']) ? 'Red' : 'Green');
					} else {
						$legends['up'][] = $yearToDayData[$ss_key]['prevYSumm'];
						$legends['up'][] = $yearToDayData[$ss_key]['currYSumm'];
						$legends['down'][] = (($yearToDayData[$ss_key]['lastYSumm'] != 0) ? (round((($yearToDayData[$ss_key]['prevYSumm'] - $yearToDayData[$ss_key]['lastYSumm']) / $yearToDayData[$ss_key]['lastYSumm']) * 1000) / 10) : 0.0);
						$legends['down'][] = (($yearToDayData[$ss_key]['prevYSumm'] != 0) ? (round((($yearToDayData[$ss_key]['currYSumm'] - $yearToDayData[$ss_key]['prevYSumm']) / $yearToDayData[$ss_key]['prevYSumm']) * 1000) / 10) : 0.0);
						$legends['color'][] = (($yearToDayData[$ss_key]['prevYSumm'] < $yearToDayData[$ss_key]['lastYSumm']) ? 'Red' : 'Green');
						$legends['color'][] = (($yearToDayData[$ss_key]['currYSumm'] < $yearToDayData[$ss_key]['prevYSumm']) ? 'Red' : 'Green');
					}
					if (!($ss_key)) {
						$legends['up'][] = '';
						$legends['down'][] = '';
						$legends['color'][] = 'Black';
					}
				}
				#	Формирование итогового ответа
				$results = [];
				$names = [];
				foreach ($sorters as $key) {
					$sub_res = [];
					foreach ($sub_sorters as $ss_key) {
						$sub_res[] = '{"idv":"'.$yearToDayData[$ss_key]['prevY'][$sorters_name[$key]].'","idppt3":"'.$yearToDayData[$ss_key]['prevYPerc'][$sorters_name[$key]].'","lbl":"'.$sub_sorters_name[$ss_key].'"}';
						$sub_res[] = '{"idv":"'.$yearToDayData[$ss_key]['currY'][$sorters_name[$key]].'","idppt3":"'.$yearToDayData[$ss_key]['currYPerc'][$sorters_name[$key]].'","lbl":"'.$sub_sorters_name[$ss_key].'"}';
						if (!($ss_key)) {
							$sub_res[] =  '{"idv":"","idppt3":"","lbl":""}';
						}
					}
					$result = '{"iD":['.implode(',',$sub_res).']}';
					$results[] = $result;
					$names[] = $sorters_name[$key];
				}

				echo '{"result":1,"slide_legend":"'.$slide_legend.'","data":{"ss":['.implode(',',$sorters).'],"sBs":['.implode(',',$sub_sorters).'],"sNs":["'.implode('","',$names).'"],"sBNs":["'.implode('","',$sub_names).'"],"rs":['.implode(',',$results).'],"lUs":["'.implode('","',$legends['up']).'"],"lDs":["'.implode('","',$legends['down']).'"],"lCs":["'.implode('","',$legends['color']).'"],"lYs":["'.implode('","',$yearToDayLabels).'"]}}';
			}
			else if ($page == 8) {
				$filters_queries = [];

				foreach ($params['fD'] as $item) {
					$filters_queries[] = " column_number_".$item['cI']." IN (".implode(', ',$item['vI']).")";
				}
				if ($filters_queries) {
					$filters_query = implode(" AND", $filters_queries);
					$main_filter = $filters_queries[0];
				}
					else {
						$filters_query = " 1";
						$main_filter = " 1";
					}

				$date_pieces = explode('-', $_POST['edate']);
				$year = $date_pieces[0];
				$month = $date_pieces[1];
				$day = $date_pieces[2];

				$sorters = [];
				$sorters_summ = [];
				$sorters_name = [];
				$other_summ = 0;
				if ($params['sI']) {
					#	Определить лидеров по "сортеру"
					$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item, value AS item_name, (SELECT value FROM db_".$dbase."_xls_for_diagrams_columns WHERE id = ".$params['sI'].") AS sorter_name FROM db_".$dbase."_xls_for_diagrams_table JOIN db_".$dbase."_xls_for_diagrams_unique_values ON (db_".$dbase."_xls_for_diagrams_unique_values.column_id = ".$params['sI']." AND db_".$dbase."_xls_for_diagrams_unique_values.id = db_".$dbase."_xls_for_diagrams_table.column_number_".$params['sI'].") WHERE".$filters_query." GROUP BY column_number_".$params['sI']." ORDER BY sorter_summ DESC";
					if ($result = $conn->query($sql)) {
						if ($params['ors']) {
							$sorters = $params['osC'];
							foreach ($sorters as $item) {
								$sorters_summ[$item] = 0;
							}
							while ($obj = $result->fetch_object()) {
								if (in_array($obj->sorter_item, $sorters)) {
									$sorters_summ[$obj->sorter_item] = $obj->sorter_summ;
								}
									else {
										$other_summ += $obj->sorter_summ;
									}
							}
						}
							else {
								$count = 0;
								while ($obj = $result->fetch_object()) {
									$count++;
									if ($count < $params['sCs']) {
										if (($params['sU']) && (( $obj->sorter_name == 'Корпорация') || ( $obj->sorter_name == 'Корпорация - map')) && ($obj->item_name == 'U.M.')) {
											$count--;
											$other_summ += $obj->sorter_summ;
										}
											else {
												$sorters[] = $obj->sorter_item;
												$sorters_summ[$obj->sorter_item] = $obj->sorter_summ;
											}
									}
										else {
											if ((( $obj->sorter_name == 'Корпорация') && ($obj->sorter_item == $mainCorp)) || (( $obj->sorter_name === 'Корпорация - map') && ($obj->sorter_item == $mainCorp))) {
												$other_summ += $sorters_summ[$sorters[$params['sCs'] - 2]];
												$sorters[$params['sCs'] - 2] = $obj->sorter_item;
												$sorters_summ[$obj->sorter_item] = $obj->sorter_summ;
											}
												else {
													$other_summ += $obj->sorter_summ;
												}
										}
								}
							}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
				}
					else {
						foreach ($params['dD'] as $key => $item) {
							$sorters[] = $key;
							$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
							$sorter_name = 'Ценовые диапазоны';
							if ($result = $conn->query($sql)) {
								while ($obj = $result->fetch_object()) {
									$sorters_summ[$key] = $obj->sorter_summ;
								}
							}
								else {
									die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
								}
							$sorters_name[$key] = $key;
						}
					}

				if (!(count($sorters))) {
					echo '{"result":0,"error":"Фильтры отсеяли все значения"}';
					break;
				}

				if ($params['sI']) {
					#	Определить название "сортера"
					$sql = "SELECT value FROM db_".$dbase."_xls_for_diagrams_columns WHERE id = ".$params['sI'];
					$sorter_name = '';
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							$sorter_name = $obj->value;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}

					#	Определить названия лидеров "сортера"
					$sql = "SELECT id, value FROM db_".$dbase."_xls_for_diagrams_unique_values WHERE column_id = ".$params['sI']." AND id IN (".implode(', ', $sorters).")";
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							if (((($sorter_name === 'Корпорация') && ($obj->id == $mainCorp)) || (($sorter_name === 'Корпорация - map') && ($obj->id == $mainCorp))) && (!($params['ors']))) {
								array_splice($sorters, array_search($obj->id, $sorters), 1);
								array_unshift($sorters, $obj->id);
							}
							$sorters_name[$obj->id] = $obj->value;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
				}

				#	Определить название второго "сортера"
				$sql = "SELECT value FROM db_".$dbase."_xls_for_diagrams_columns WHERE id = ".$params['sCD'];
				$sub_sorters_name = [];
				$sub_sorters = [];
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$sub_sorters_name[0] = 'Total';
						$sub_sorters[] = 0;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}

				#	Определить названия категорий второго "сортера"
				
				#	тут добавить фильтр полученных ИТОГОВЫХ айди по выбранному списку - остальное сработает автоматически (!), по поводу упорядочивания - подумать над этим, вероятнее в пост обработке
				
				
				$sql = "SELECT id, value FROM db_".$dbase."_xls_for_diagrams_unique_values WHERE id IN (SELECT DISTINCT column_number_".$params['sCD']." FROM `db_".$dbase."_xls_for_diagrams_table` WHERE".$filters_query.") AND id IN (".implode(',',$params['ssC']).")";
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$sub_sorters_name[$obj->id] = $obj->value;
						#$sub_sorters[] = $obj->id;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				foreach ($params['ssC'] as $ss_id) {
					$sub_sorters[] = $ss_id;
				}

				$months = ['', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

				#	Определить данные для графиков YTD
				$yearToDayData = [];
				$ytdInd = 0;
				foreach ($sub_sorters as $ss_key) {
					$yearToDayData[$ss_key]['currY'] = ['Other' => 0];
					$yearToDayData[$ss_key]['prevY'] = ['Other' => 0];
					$yearToDayData[$ss_key]['lastY'] = ['Other' => 0];
					$yearToDayData[$ss_key]['currYSumm'] = 0;
					$yearToDayData[$ss_key]['prevYSumm'] = 0;
					$yearToDayData[$ss_key]['lastYSumm'] = 0;
					$yearToDayData[$ss_key]['currYSummPT3'] = 0;
					$yearToDayData[$ss_key]['prevYSummPT3'] = 0;
					$yearToDayData[$ss_key]['lastYSummPT3'] = 0;
					if ($ss_key) {
						$sorter_filter = " AND column_number_".$params['sCD']." = ".$ss_key;
						$ytdInd++;
						$yearToDayLabels[] = $ytdInd;
						$ytdInd++;
						$yearToDayLabels[] = $ytdInd;
					} else {
						$sorter_filter = " AND 1";
						$yearToDayLabels = [
							((($month + 1) > 12) ? "Jan".substr(($year - 1), -2) : $months[(int)$month + 1]."".substr(($year - 2), -2))."-".$months[(int)$month]."".substr(($year - 1), -2),
							((($month + 1) > 12) ? "Jan".substr($year, -2) : $months[(int)$month + 1]."".substr(($year - 1), -2))."-".$months[(int)$month]."".substr($year, -2),
							''
						];
					}
					$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-0) : ($year-1)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-0), $month, "31");
					#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							$yearToDayData[$ss_key]['currYSummPT3'] = $obj->sorter_summ;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
					$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-1) : ($year-2)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-1), $month, "31");
					#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							$yearToDayData[$ss_key]['prevYSummPT3'] = $obj->sorter_summ;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
					$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-2) : ($year-3)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-2), $month, "31");
					#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							$yearToDayData[$ss_key]['lastYSummPT3'] = $obj->sorter_summ;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
					if ($params['sI']) {
						$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-0) : ($year-1)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-0), $month, "31")." GROUP BY column_number_".$params['sI'];
						#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								if (array_key_exists($obj->sorter_item, $sorters_name)) {
									$yearToDayData[$ss_key]['currY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
									$yearToDayData[$ss_key]['currYSumm'] += $obj->sorter_summ;
								}
									else {
										$yearToDayData[$ss_key]['currY']['Other'] += $obj->sorter_summ;
										if ($params['oos']) {
											$yearToDayData[$ss_key]['currYSumm'] += $obj->sorter_summ;
										}
									}
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
						$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-1) : ($year-2)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-1), $month, "31")." GROUP BY column_number_".$params['sI'];
						#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								if (array_key_exists($obj->sorter_item, $sorters_name)) {
									$yearToDayData[$ss_key]['prevY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
									$yearToDayData[$ss_key]['prevYSumm'] += $obj->sorter_summ;
								}
									else {
										$yearToDayData[$ss_key]['prevY']['Other'] += $obj->sorter_summ;
										if ($params['oos']) {
											$yearToDayData[$ss_key]['prevYSumm'] += $obj->sorter_summ;
										}
									}
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
						$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-2) : ($year-3)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-2), $month, "31")." GROUP BY column_number_".$params['sI'];
						#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								if (array_key_exists($obj->sorter_item, $sorters_name)) {
									$yearToDayData[$ss_key]['lastY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
									$yearToDayData[$ss_key]['lastYSumm'] += $obj->sorter_summ;
								}
									else {
										$yearToDayData[$ss_key]['lastY']['Other'] += $obj->sorter_summ;
										if ($params['oos']) {
											$yearToDayData[$ss_key]['lastYSumm'] += $obj->sorter_summ;
										}
									}
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
					}
						else {
							foreach ($params['dD'] as $key => $item) {
								$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-0) : ($year-1)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-0), $month, "31")." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
								#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
								if ($result = $conn->query($sql)) {
									while ($obj = $result->fetch_object()) {
										$yearToDayData[$ss_key]['currY'][$key] = $obj->sorter_summ;
										$yearToDayData[$ss_key]['currYSumm'] += $obj->sorter_summ;
									}
								}
									else {
										die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
									}
								$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-1) : ($year-2)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-1), $month, "31")." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
								#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
								if ($result = $conn->query($sql)) {
									while ($obj = $result->fetch_object()) {
										$yearToDayData[$ss_key]['prevY'][$key] = $obj->sorter_summ;
										$yearToDayData[$ss_key]['prevYSumm'] += $obj->sorter_summ;
									}
								}
									else {
										die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
									}
								$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-2) : ($year-3)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-2), $month, "31")." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
								#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
								if ($result = $conn->query($sql)) {
									while ($obj = $result->fetch_object()) {
										$yearToDayData[$ss_key]['lastY'][$key] = $obj->sorter_summ;
										$yearToDayData[$ss_key]['lastYSumm'] += $obj->sorter_summ;
									}
								}
									else {
										die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
									}
							}
						}

					if (($other_summ > 0) && ($sorters_name[0] != 'Other') && ($params['oos'])) {
						$sorters[] = 0;
						$sorters_name[0] = 'Other';
					}
					#	Подсчет процентного выражения и приведение величин к масштабу миллиардов
					foreach ($sorters_name as $key) {
						#	Проценты
						$yearToDayData[$ss_key]['currYPerc'][$key] = (($yearToDayData[$ss_key]['currYSumm'] != 0) ? (round($yearToDayData[$ss_key]['currY'][$key] / $yearToDayData[$ss_key]['currYSumm'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['prevYPerc'][$key] = (($yearToDayData[$ss_key]['prevYSumm'] != 0) ? (round($yearToDayData[$ss_key]['prevY'][$key] / $yearToDayData[$ss_key]['prevYSumm'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['lastYPerc'][$key] = (($yearToDayData[$ss_key]['lastYSumm'] != 0) ? (round($yearToDayData[$ss_key]['lastY'][$key] / $yearToDayData[$ss_key]['lastYSumm'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['currYPercPT3'][$key] = (($yearToDayData[$ss_key]['currYSummPT3'] != 0) ? (round($yearToDayData[$ss_key]['currY'][$key] / $yearToDayData[$ss_key]['currYSummPT3'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['prevYPercPT3'][$key] = (($yearToDayData[$ss_key]['prevYSummPT3'] != 0) ? (round($yearToDayData[$ss_key]['prevY'][$key] / $yearToDayData[$ss_key]['prevYSummPT3'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['lastYPercPT3'][$key] = (($yearToDayData[$ss_key]['lastYSummPT3'] != 0) ? (round($yearToDayData[$ss_key]['lastY'][$key] / $yearToDayData[$ss_key]['lastYSummPT3'] * 1000) / 10) : 0.0);
						if (!($params['sP'])) {
							#	Масштабирование
							$yearToDayData[$ss_key]['currY'][$key] = round($yearToDayData[$ss_key]['currY'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['prevY'][$key] = round($yearToDayData[$ss_key]['prevY'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['lastY'][$key] = round($yearToDayData[$ss_key]['lastY'][$key] / $msscht2) / 10000;
						} else if ($params['sP'] == 1) {
							#	Масштабирование
							$yearToDayData[$ss_key]['currY'][$key] = round($yearToDayData[$ss_key]['currY'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['prevY'][$key] = round($yearToDayData[$ss_key]['prevY'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['lastY'][$key] = round($yearToDayData[$ss_key]['lastY'][$key] / $msscht2) / 10000;
						}
					}
					#	Масштабирование суммарных данных
					/*if (!($params['sP'])) {
						$yearToDayData[$ss_key]['currYSumm'] = round($yearToDayData[$ss_key]['currYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['prevYSumm'] = round($yearToDayData[$ss_key]['prevYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['lastYSumm'] = round($yearToDayData[$ss_key]['lastYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['currYSummPT3'] = round($yearToDayData[$ss_key]['currYSummPT3'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['prevYSummPT3'] = round($yearToDayData[$ss_key]['prevYSummPT3'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['lastYSummPT3'] = round($yearToDayData[$ss_key]['lastYSummPT3'] / $msscht1) / 10;
					} else if ($params['sP'] == 1) {
						$yearToDayData[$ss_key]['currYSumm'] = round($yearToDayData[$ss_key]['currYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['prevYSumm'] = round($yearToDayData[$ss_key]['prevYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['lastYSumm'] = round($yearToDayData[$ss_key]['lastYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['currYSummPT3'] = round($yearToDayData[$ss_key]['currYSummPT3'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['prevYSummPT3'] = round($yearToDayData[$ss_key]['prevYSummPT3'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['lastYSummPT3'] = round($yearToDayData[$ss_key]['lastYSummPT3'] / $msscht1) / 10;
					}*/
					#	Определение величин приращения (для легенды)/ формирование данных легенды
				}

				switch ($params['sos']) {
					case 0: {
						break;
					}
					case 1: {
						for ($i = 1; $i < (count($sub_sorters) - 1); $i++) {
							$max = $yearToDayData[$sub_sorters[$i]]['prevYSumm'];
							$maxnum = $i;
							for ($j = ($i + 1); $j < count($sub_sorters); $j++) {
								if ($max < $yearToDayData[$sub_sorters[$j]]['prevYSumm']) {
									$max = $yearToDayData[$sub_sorters[$j]]['prevYSumm'];
									$maxnum = $j;
								}
							}
							if ($maxnum > $i) {
								$tmp = $sub_sorters[$i];
								$sub_sorters[$i] = $sub_sorters[$maxnum];
								$sub_sorters[$maxnum] = $tmp;
							}
						}
						break;
					}
					case 2: {
						for ($i = 1; $i < (count($sub_sorters) - 1); $i++) {
							$max = $yearToDayData[$sub_sorters[$i]]['currYSumm'];
							$maxnum = $i;
							for ($j = ($i + 1); $j < count($sub_sorters); $j++) {
								if ($max < $yearToDayData[$sub_sorters[$j]]['currYSumm']) {
									$max = $yearToDayData[$sub_sorters[$j]]['currYSumm'];
									$maxnum = $j;
								}
							}
							if ($maxnum > $i) {
								$tmp = $sub_sorters[$i];
								$sub_sorters[$i] = $sub_sorters[$maxnum];
								$sub_sorters[$maxnum] = $tmp;
							}
						}
						break;
					}
					case 3: {
						for ($i = 1; $i < (count($sub_sorters) - 1); $i++) {
							$max = $yearToDayData[$sub_sorters[$i]]['currYSumm'] + $yearToDayData[$sub_sorters[$i]]['prevYSumm'];
							$maxnum = $i;
							for ($j = ($i + 1); $j < count($sub_sorters); $j++) {
								if ($max < ($yearToDayData[$sub_sorters[$j]]['currYSumm'] + $yearToDayData[$sub_sorters[$j]]['prevYSumm'])) {
									$max = $yearToDayData[$sub_sorters[$j]]['currYSumm'] + $yearToDayData[$sub_sorters[$j]]['prevYSumm'];
									$maxnum = $j;
								}
							}
							if ($maxnum > $i) {
								$tmp = $sub_sorters[$i];
								$sub_sorters[$i] = $sub_sorters[$maxnum];
								$sub_sorters[$maxnum] = $tmp;
							}
						}
						break;
					}
				}

				$legends = [];
				$sub_names = [];
				foreach ($sub_sorters as $ss_key) {
					$sub_names[] = $sub_sorters_name[$ss_key];
					if ($params['gT'] === '2') {//$params['gT'] === '2'	??? было iI
						$legends['up'][] = $yearToDayData[$ss_key]['prevYSummPT3'];
						$legends['up'][] = $yearToDayData[$ss_key]['currYSummPT3'];
						$legends['down'][] = (($yearToDayData[$ss_key]['lastYSummPT3'] != 0) ? (round((($yearToDayData[$ss_key]['prevYSummPT3'] - $yearToDayData[$ss_key]['lastYSummPT3']) / $yearToDayData[$ss_key]['lastYSummPT3']) * 1000) / 10) : 0.0);
						$legends['down'][] = (($yearToDayData[$ss_key]['prevYSummPT3'] != 0) ? (round((($yearToDayData[$ss_key]['currYSummPT3'] - $yearToDayData[$ss_key]['prevYSummPT3']) / $yearToDayData[$ss_key]['prevYSummPT3']) * 1000) / 10) : 0.0);
						$legends['color'][] = (($yearToDayData[$ss_key]['prevYSummPT3'] < $yearToDayData[$ss_key]['lastYSummPT3']) ? 'Red' : 'Green');
						$legends['color'][] = (($yearToDayData[$ss_key]['currYSummPT3'] < $yearToDayData[$ss_key]['prevYSummPT3']) ? 'Red' : 'Green');
					} else {
						$legends['up'][] = $yearToDayData[$ss_key]['prevYSumm'];
						$legends['up'][] = $yearToDayData[$ss_key]['currYSumm'];
						$legends['down'][] = (($yearToDayData[$ss_key]['lastYSumm'] != 0) ? (round((($yearToDayData[$ss_key]['prevYSumm'] - $yearToDayData[$ss_key]['lastYSumm']) / $yearToDayData[$ss_key]['lastYSumm']) * 1000) / 10) : 0.0);
						$legends['down'][] = (($yearToDayData[$ss_key]['prevYSumm'] != 0) ? (round((($yearToDayData[$ss_key]['currYSumm'] - $yearToDayData[$ss_key]['prevYSumm']) / $yearToDayData[$ss_key]['prevYSumm']) * 1000) / 10) : 0.0);
						$legends['color'][] = (($yearToDayData[$ss_key]['prevYSumm'] < $yearToDayData[$ss_key]['lastYSumm']) ? 'Red' : 'Green');
						$legends['color'][] = (($yearToDayData[$ss_key]['currYSumm'] < $yearToDayData[$ss_key]['prevYSumm']) ? 'Red' : 'Green');
					}
					if (!($ss_key)) {
						$legends['up'][] = '';
						$legends['down'][] = '';
						$legends['color'][] = 'Black';
					}
				}
				#	Формирование итогового ответа
				$results = [];
				$names = [];
				foreach ($sorters as $key) {
					$sub_res = [];
					foreach ($sub_sorters as $ss_key) {
						$sub_res[] = '{"idv":"'.$yearToDayData[$ss_key]['prevY'][$sorters_name[$key]].'","idppt3":"'.$yearToDayData[$ss_key]['prevYPerc'][$sorters_name[$key]].'","lbl":"'.$sub_sorters_name[$ss_key].'"}';
						$sub_res[] = '{"idv":"'.$yearToDayData[$ss_key]['currY'][$sorters_name[$key]].'","idppt3":"'.$yearToDayData[$ss_key]['currYPerc'][$sorters_name[$key]].'","lbl":"'.$sub_sorters_name[$ss_key].'"}';
						if (!($ss_key)) {
							$sub_res[] =  '{"idv":"","idppt3":"","lbl":""}';
						}
					}
					$result = '{"iD":['.implode(',',$sub_res).']}';
					$results[] = $result;
					$names[] = $sorters_name[$key];
				}

				echo '{"result":1,"slide_legend":"'.$slide_legend.'","data":{"ss":['.implode(',',$sorters).'],"sBs":['.implode(',',$sub_sorters).'],"sNs":["'.implode('","',$names).'"],"sBNs":["'.implode('","',$sub_names).'"],"rs":['.implode(',',$results).'],"lUs":["'.implode('","',$legends['up']).'"],"lDs":["'.implode('","',$legends['down']).'"],"lCs":["'.implode('","',$legends['color']).'"],"lYs":["'.implode('","',$yearToDayLabels).'"]}}';
			}
			else if ($page == 9) {
				$filters_queries = [];

				foreach ($params['fD'] as $item) {
					$filters_queries[] = " column_number_".$item['cI']." IN (".implode(', ',$item['vI']).")";
				}
				if ($filters_queries) {
					$filters_query = implode(" AND", $filters_queries);
					$main_filter = $filters_queries[0];
				}
					else {
						$filters_query = " 1";
						$main_filter = " 1";
					}

				$date_pieces = explode('-', $_POST['edate']);
				$year = $date_pieces[0];
				$month = $date_pieces[1];
				$day = $date_pieces[2];
				$date_pieces = explode('-', $_POST['sdate']);
				$syear = $date_pieces[0];
				$smonth = $date_pieces[1];
				$sday = $date_pieces[2];

				$sorters = [];
				$sorters_summ = [];
				$sorters_name = [];
				$other_summ = 0;
				if ($params['sI']) {
					#	Определить лидеров по "сортеру"
					$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item, value AS item_name, (SELECT value FROM db_".$dbase."_xls_for_diagrams_columns WHERE id = ".$params['sI'].") AS sorter_name FROM db_".$dbase."_xls_for_diagrams_table JOIN db_".$dbase."_xls_for_diagrams_unique_values ON (db_".$dbase."_xls_for_diagrams_unique_values.column_id = ".$params['sI']." AND db_".$dbase."_xls_for_diagrams_unique_values.id = db_".$dbase."_xls_for_diagrams_table.column_number_".$params['sI'].") WHERE".$filters_query." GROUP BY column_number_".$params['sI']." ORDER BY sorter_summ DESC";
					if ($result = $conn->query($sql)) {
						if ($params['ors']) {
							$sorters = $params['osC'];
							foreach ($sorters as $item) {
								$sorters_summ[$item] = 0;
							}
							while ($obj = $result->fetch_object()) {
								if (in_array($obj->sorter_item, $sorters)) {
									$sorters_summ[$obj->sorter_item] = $obj->sorter_summ;
								}
									else {
										$other_summ += $obj->sorter_summ;
									}
							}
						}
							else {
								$count = 0;
								while ($obj = $result->fetch_object()) {
									$count++;
									if ($count < $params['sCs']) {
										if (($params['sU']) && (( $obj->sorter_name == 'Корпорация') || ( $obj->sorter_name == 'Корпорация - map')) && ($obj->item_name == 'U.M.')) {
											$count--;
											$other_summ += $obj->sorter_summ;
										}
											else {
												$sorters[] = $obj->sorter_item;
												$sorters_summ[$obj->sorter_item] = $obj->sorter_summ;
											}
									}
										else {
											if ((( $obj->sorter_name == 'Корпорация') && ($obj->sorter_item == $mainCorp)) || (( $obj->sorter_name === 'Корпорация - map') && ($obj->sorter_item == $mainCorp))) {
												$other_summ += $sorters_summ[$sorters[$params['sCs'] - 2]];
												$sorters[$params['sCs'] - 2] = $obj->sorter_item;
												$sorters_summ[$obj->sorter_item] = $obj->sorter_summ;
											}
												else {
													$other_summ += $obj->sorter_summ;
												}
										}
								}
							}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
				}
					else {
						foreach ($params['dD'] as $key => $item) {
							$sorters[] = $key;
							$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
							$sorter_name = 'Ценовые диапазоны';
							if ($result = $conn->query($sql)) {
								while ($obj = $result->fetch_object()) {
									$sorters_summ[$key] = $obj->sorter_summ;
								}
							}
								else {
									die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
								}
							$sorters_name[$key] = $key;
						}
					}

				if (!(count($sorters))) {
					echo '{"result":0,"error":"Фильтры отсеяли все значения"}';
					break;
				}

				if ($params['sI']) {
					#	Определить название "сортера"
					$sql = "SELECT value FROM db_".$dbase."_xls_for_diagrams_columns WHERE id = ".$params['sI'];
					$sorter_name = '';
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							$sorter_name = $obj->value;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}

					#	Определить названия лидеров "сортера"
					$sql = "SELECT id, value FROM db_".$dbase."_xls_for_diagrams_unique_values WHERE column_id = ".$params['sI']." AND id IN (".implode(', ', $sorters).")";
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							if (((($sorter_name === 'Корпорация') && ($obj->id == $mainCorp)) || (($sorter_name === 'Корпорация - map') && ($obj->id == $mainCorp))) && (!($params['ors']))) {
								array_splice($sorters, array_search($obj->id, $sorters), 1);
								array_unshift($sorters, $obj->id);
							}
							$sorters_name[$obj->id] = $obj->value;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
				}

				#	Определить название второго "сортера"
				$sql = "SELECT value FROM db_".$dbase."_xls_for_diagrams_columns WHERE id = ".$params['sCD'];
				$sub_sorters_name = [];
				$sub_sorters = [];
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$sub_sorters_name[0] = 'Total';
						$sub_sorters[] = 0;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}

				#	Определить названия категорий второго "сортера"
				
				#	тут добавить фильтр полученных ИТОГОВЫХ айди по выбранному списку - остальное сработает автоматически (!), по поводу упорядочивания - подумать над этим, вероятнее в пост обработке
				
				
				$sql = "SELECT id, value FROM db_".$dbase."_xls_for_diagrams_unique_values WHERE id IN (SELECT DISTINCT column_number_".$params['sCD']." FROM `db_".$dbase."_xls_for_diagrams_table` WHERE".$filters_query.") AND id IN (".implode(',',$params['ssC']).")";
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$sub_sorters_name[$obj->id] = $obj->value;
						#$sub_sorters[] = $obj->id;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				foreach ($params['ssC'] as $ss_id) {
					$sub_sorters[] = $ss_id;
				}

				$months = ['', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

				#	Определить данные для графиков YTD
				$yearToDayData = [];
				$ytdInd = 0;
				foreach ($sub_sorters as $ss_key) {
					$yearToDayData[$ss_key]['currY'] = ['Other' => 0];
					$yearToDayData[$ss_key]['prevY'] = ['Other' => 0];
					$yearToDayData[$ss_key]['lastY'] = ['Other' => 0];
					$yearToDayData[$ss_key]['currYSumm'] = 0;
					$yearToDayData[$ss_key]['prevYSumm'] = 0;
					$yearToDayData[$ss_key]['lastYSumm'] = 0;
					$yearToDayData[$ss_key]['currYSummPT3'] = 0;
					$yearToDayData[$ss_key]['prevYSummPT3'] = 0;
					$yearToDayData[$ss_key]['lastYSummPT3'] = 0;
					if ($ss_key) {
						$sorter_filter = " AND column_number_".$params['sCD']." = ".$ss_key;
						$ytdInd++;
						$yearToDayLabels[] = $ytdInd;
						$ytdInd++;
						$yearToDayLabels[] = $ytdInd;
					} else {
						$sorter_filter = " AND 1";
						$yearToDayLabels = [
							$months[(int)$smonth].substr(($syear - 1), -2)."-".$months[(int)$month]."".substr(($year - 1), -2),
							$months[(int)$smonth].substr($syear, -2)."-".$months[(int)$month]."".substr($year, -2),
							#"YTD".substr(($year - 1), -2)." ".$months[$month],
							#"YTD".substr($year, -2)." ".$months[$month],
							''
						];
					}
					$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($syear-0), $smonth, $sday)." AND ".sprintf("%04d%02d%02d", ($year-0), $month, $day);
					#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							$yearToDayData[$ss_key]['currYSummPT3'] = $obj->sorter_summ;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
					$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($syear-1), $smonth, $sday)." AND ".sprintf("%04d%02d%02d", ($year-1), $month, $day);
					#BETWEEN ".sprintf("%04d%02d%02d", ($syear-1), $smonth, $sday)." AND ".sprintf("%04d%02d%02d", ($year-1), $month, $day)
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							$yearToDayData[$ss_key]['prevYSummPT3'] = $obj->sorter_summ;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
					$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($syear-2), $smonth, $sday)." AND ".sprintf("%04d%02d%02d", ($year-2), $month, $day);
					#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							$yearToDayData[$ss_key]['lastYSummPT3'] = $obj->sorter_summ;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
					if ($params['sI']) {
						$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($syear-0), $smonth, $sday)." AND ".sprintf("%04d%02d%02d", ($year-0), $month, $day)." GROUP BY column_number_".$params['sI'];
						#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								if (array_key_exists($obj->sorter_item, $sorters_name)) {
									$yearToDayData[$ss_key]['currY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
									$yearToDayData[$ss_key]['currYSumm'] += $obj->sorter_summ;
								}
									else {
										$yearToDayData[$ss_key]['currY']['Other'] += $obj->sorter_summ;
										if ($params['oos']) {
											$yearToDayData[$ss_key]['currYSumm'] += $obj->sorter_summ;
										}
									}
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
						$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($syear-1), $smonth, $sday)." AND ".sprintf("%04d%02d%02d", ($year-1), $month, $day)." GROUP BY column_number_".$params['sI'];
						#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								if (array_key_exists($obj->sorter_item, $sorters_name)) {
									$yearToDayData[$ss_key]['prevY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
									$yearToDayData[$ss_key]['prevYSumm'] += $obj->sorter_summ;
								}
									else {
										$yearToDayData[$ss_key]['prevY']['Other'] += $obj->sorter_summ;
										if ($params['oos']) {
											$yearToDayData[$ss_key]['prevYSumm'] += $obj->sorter_summ;
										}
									}
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
						$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($syear-2), $smonth, $sday)." AND ".sprintf("%04d%02d%02d", ($year-2), $month, $day)." GROUP BY column_number_".$params['sI'];
						#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								if (array_key_exists($obj->sorter_item, $sorters_name)) {
									$yearToDayData[$ss_key]['lastY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
									$yearToDayData[$ss_key]['lastYSumm'] += $obj->sorter_summ;
								}
									else {
										$yearToDayData[$ss_key]['lastY']['Other'] += $obj->sorter_summ;
										if ($params['oos']) {
											$yearToDayData[$ss_key]['lastYSumm'] += $obj->sorter_summ;
										}
									}
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
					}
						else {
							foreach ($params['dD'] as $key => $item) {
								$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($syear-0), $smonth, $sday)." AND ".sprintf("%04d%02d%02d", ($year-0), $month, $day)." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
								#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
								if ($result = $conn->query($sql)) {
									while ($obj = $result->fetch_object()) {
										$yearToDayData[$ss_key]['currY'][$key] = $obj->sorter_summ;
										$yearToDayData[$ss_key]['currYSumm'] += $obj->sorter_summ;
									}
								}
									else {
										die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
									}
								$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($syear-1), $smonth, $sday)." AND ".sprintf("%04d%02d%02d", ($year-1), $month, $day)." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
								#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
								if ($result = $conn->query($sql)) {
									while ($obj = $result->fetch_object()) {
										$yearToDayData[$ss_key]['prevY'][$key] = $obj->sorter_summ;
										$yearToDayData[$ss_key]['prevYSumm'] += $obj->sorter_summ;
									}
								}
									else {
										die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
									}
								$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($syear-2), $smonth, $sday)." AND ".sprintf("%04d%02d%02d", ($year-2), $month, $day)." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
								#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
								if ($result = $conn->query($sql)) {
									while ($obj = $result->fetch_object()) {
										$yearToDayData[$ss_key]['lastY'][$key] = $obj->sorter_summ;
										$yearToDayData[$ss_key]['lastYSumm'] += $obj->sorter_summ;
									}
								}
									else {
										die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
									}
							}
						}
					if (($other_summ > 0) && ($sorters_name[0] != 'Other') && ($params['oos'])) {
						$sorters[] = 0;
						$sorters_name[0] = 'Other';
					}
					#	Подсчет процентного выражения и приведение величин к масштабу миллиардов
					foreach ($sorters_name as $key) {
						#	Проценты
						$yearToDayData[$ss_key]['currYPerc'][$key] = (($yearToDayData[$ss_key]['currYSumm'] != 0) ? (round($yearToDayData[$ss_key]['currY'][$key] / $yearToDayData[$ss_key]['currYSumm'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['prevYPerc'][$key] = (($yearToDayData[$ss_key]['prevYSumm'] != 0) ? (round($yearToDayData[$ss_key]['prevY'][$key] / $yearToDayData[$ss_key]['prevYSumm'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['lastYPerc'][$key] = (($yearToDayData[$ss_key]['lastYSumm'] != 0) ? (round($yearToDayData[$ss_key]['lastY'][$key] / $yearToDayData[$ss_key]['lastYSumm'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['currYPercPT3'][$key] = (($yearToDayData[$ss_key]['currYSummPT3'] != 0) ? (round($yearToDayData[$ss_key]['currY'][$key] / $yearToDayData[$ss_key]['currYSummPT3'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['prevYPercPT3'][$key] = (($yearToDayData[$ss_key]['prevYSummPT3'] != 0) ? (round($yearToDayData[$ss_key]['prevY'][$key] / $yearToDayData[$ss_key]['prevYSummPT3'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['lastYPercPT3'][$key] = (($yearToDayData[$ss_key]['lastYSummPT3'] != 0) ? (round($yearToDayData[$ss_key]['lastY'][$key] / $yearToDayData[$ss_key]['lastYSummPT3'] * 1000) / 10) : 0.0);
						if (!($params['sP'])) {
							#	Масштабирование
							$yearToDayData[$ss_key]['currY'][$key] = round($yearToDayData[$ss_key]['currY'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['prevY'][$key] = round($yearToDayData[$ss_key]['prevY'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['lastY'][$key] = round($yearToDayData[$ss_key]['lastY'][$key] / $msscht2) / 10000;
						} else if ($params['sP'] == 1) {
							#	Масштабирование
							$yearToDayData[$ss_key]['currY'][$key] = round($yearToDayData[$ss_key]['currY'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['prevY'][$key] = round($yearToDayData[$ss_key]['prevY'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['lastY'][$key] = round($yearToDayData[$ss_key]['lastY'][$key] / $msscht2) / 10000;
						}
					}
					#	Масштабирование суммарных данных
					/*if (!($params['sP'])) {
						$yearToDayData[$ss_key]['currYSumm'] = round($yearToDayData[$ss_key]['currYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['prevYSumm'] = round($yearToDayData[$ss_key]['prevYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['lastYSumm'] = round($yearToDayData[$ss_key]['lastYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['currYSummPT3'] = round($yearToDayData[$ss_key]['currYSummPT3'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['prevYSummPT3'] = round($yearToDayData[$ss_key]['prevYSummPT3'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['lastYSummPT3'] = round($yearToDayData[$ss_key]['lastYSummPT3'] / $msscht1) / 10;
					} else if ($params['sP'] == 1) {
						$yearToDayData[$ss_key]['currYSumm'] = round($yearToDayData[$ss_key]['currYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['prevYSumm'] = round($yearToDayData[$ss_key]['prevYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['lastYSumm'] = round($yearToDayData[$ss_key]['lastYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['currYSummPT3'] = round($yearToDayData[$ss_key]['currYSummPT3'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['prevYSummPT3'] = round($yearToDayData[$ss_key]['prevYSummPT3'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['lastYSummPT3'] = round($yearToDayData[$ss_key]['lastYSummPT3'] / $msscht1) / 10;
					}*/
					#	Определение величин приращения (для легенды)/ формирование данных легенды
				}

				switch ($params['sos']) {
					case 0: {
						break;
					}
					case 1: {
						for ($i = 1; $i < (count($sub_sorters) - 1); $i++) {
							$max = $yearToDayData[$sub_sorters[$i]]['prevYSumm'];
							$maxnum = $i;
							for ($j = ($i + 1); $j < count($sub_sorters); $j++) {
								if ($max < $yearToDayData[$sub_sorters[$j]]['prevYSumm']) {
									$max = $yearToDayData[$sub_sorters[$j]]['prevYSumm'];
									$maxnum = $j;
								}
							}
							if ($maxnum > $i) {
								$tmp = $sub_sorters[$i];
								$sub_sorters[$i] = $sub_sorters[$maxnum];
								$sub_sorters[$maxnum] = $tmp;
							}
						}
						break;
					}
					case 2: {
						for ($i = 1; $i < (count($sub_sorters) - 1); $i++) {
							$max = $yearToDayData[$sub_sorters[$i]]['currYSumm'];
							$maxnum = $i;
							for ($j = ($i + 1); $j < count($sub_sorters); $j++) {
								if ($max < $yearToDayData[$sub_sorters[$j]]['currYSumm']) {
									$max = $yearToDayData[$sub_sorters[$j]]['currYSumm'];
									$maxnum = $j;
								}
							}
							if ($maxnum > $i) {
								$tmp = $sub_sorters[$i];
								$sub_sorters[$i] = $sub_sorters[$maxnum];
								$sub_sorters[$maxnum] = $tmp;
							}
						}
						break;
					}
					case 3: {
						for ($i = 1; $i < (count($sub_sorters) - 1); $i++) {
							$max = $yearToDayData[$sub_sorters[$i]]['currYSumm'] + $yearToDayData[$sub_sorters[$i]]['prevYSumm'];
							$maxnum = $i;
							for ($j = ($i + 1); $j < count($sub_sorters); $j++) {
								if ($max < ($yearToDayData[$sub_sorters[$j]]['currYSumm'] + $yearToDayData[$sub_sorters[$j]]['prevYSumm'])) {
									$max = $yearToDayData[$sub_sorters[$j]]['currYSumm'] + $yearToDayData[$sub_sorters[$j]]['prevYSumm'];
									$maxnum = $j;
								}
							}
							if ($maxnum > $i) {
								$tmp = $sub_sorters[$i];
								$sub_sorters[$i] = $sub_sorters[$maxnum];
								$sub_sorters[$maxnum] = $tmp;
							}
						}
						break;
					}
				}

				$legends = [];
				$sub_names = [];
				foreach ($sub_sorters as $ss_key) {
					$sub_names[] = $sub_sorters_name[$ss_key];
					if ($params['gT'] === '2') {//$params['gT'] === '2'	??? было iI
						$legends['up'][] = $yearToDayData[$ss_key]['prevYSummPT3'];
						$legends['up'][] = $yearToDayData[$ss_key]['currYSummPT3'];
						$legends['down'][] = (($yearToDayData[$ss_key]['lastYSummPT3'] != 0) ? (round((($yearToDayData[$ss_key]['prevYSummPT3'] - $yearToDayData[$ss_key]['lastYSummPT3']) / $yearToDayData[$ss_key]['lastYSummPT3']) * 1000) / 10) : 0.0);
						$legends['down'][] = (($yearToDayData[$ss_key]['prevYSummPT3'] != 0) ? (round((($yearToDayData[$ss_key]['currYSummPT3'] - $yearToDayData[$ss_key]['prevYSummPT3']) / $yearToDayData[$ss_key]['prevYSummPT3']) * 1000) / 10) : 0.0);
						$legends['color'][] = (($yearToDayData[$ss_key]['prevYSummPT3'] < $yearToDayData[$ss_key]['lastYSummPT3']) ? 'Red' : 'Green');
						$legends['color'][] = (($yearToDayData[$ss_key]['currYSummPT3'] < $yearToDayData[$ss_key]['prevYSummPT3']) ? 'Red' : 'Green');
					} else {
						$legends['up'][] = $yearToDayData[$ss_key]['prevYSumm'];
						$legends['up'][] = $yearToDayData[$ss_key]['currYSumm'];
						$legends['down'][] = (($yearToDayData[$ss_key]['lastYSumm'] != 0) ? (round((($yearToDayData[$ss_key]['prevYSumm'] - $yearToDayData[$ss_key]['lastYSumm']) / $yearToDayData[$ss_key]['lastYSumm']) * 1000) / 10) : 0.0);
						$legends['down'][] = (($yearToDayData[$ss_key]['prevYSumm'] != 0) ? (round((($yearToDayData[$ss_key]['currYSumm'] - $yearToDayData[$ss_key]['prevYSumm']) / $yearToDayData[$ss_key]['prevYSumm']) * 1000) / 10) : 0.0);
						$legends['color'][] = (($yearToDayData[$ss_key]['prevYSumm'] < $yearToDayData[$ss_key]['lastYSumm']) ? 'Red' : 'Green');
						$legends['color'][] = (($yearToDayData[$ss_key]['currYSumm'] < $yearToDayData[$ss_key]['prevYSumm']) ? 'Red' : 'Green');
					}
					if (!($ss_key)) {
						$legends['up'][] = '';
						$legends['down'][] = '';
						$legends['color'][] = 'Black';
					}
				}
				#	Формирование итогового ответа
				$results = [];
				$names = [];
				foreach ($sorters as $key) {
					$sub_res = [];
					foreach ($sub_sorters as $ss_key) {
						$sub_res[] = '{"idv":"'.$yearToDayData[$ss_key]['prevY'][$sorters_name[$key]].'","idppt3":"'.$yearToDayData[$ss_key]['prevYPerc'][$sorters_name[$key]].'","lbl":"'.$sub_sorters_name[$ss_key].'"}';
						$sub_res[] = '{"idv":"'.$yearToDayData[$ss_key]['currY'][$sorters_name[$key]].'","idppt3":"'.$yearToDayData[$ss_key]['currYPerc'][$sorters_name[$key]].'","lbl":"'.$sub_sorters_name[$ss_key].'"}';
						if (!($ss_key)) {
							$sub_res[] =  '{"idv":"","idppt3":"","lbl":""}';
						}
					}
					$result = '{"iD":['.implode(',',$sub_res).']}';
					$results[] = $result;
					$names[] = $sorters_name[$key];
				}

				echo '{"result":1,"slide_legend":"'.$slide_legend.'","data":{"ss":['.implode(',',$sorters).'],"sBs":['.implode(',',$sub_sorters).'],"sNs":["'.implode('","',$names).'"],"sBNs":["'.implode('","',$sub_names).'"],"rs":['.implode(',',$results).'],"lUs":["'.implode('","',$legends['up']).'"],"lDs":["'.implode('","',$legends['down']).'"],"lCs":["'.implode('","',$legends['color']).'"],"lYs":["'.implode('","',$yearToDayLabels).'"]}}';
			}
			else if ($page == 10) {
				$filters_queries = [];

				foreach ($params['fD'] as $item) {
					$filters_queries[] = " column_number_".$item['cI']." IN (".implode(', ',$item['vI']).")";
				}
				if ($filters_queries) {
					$filters_query = implode(" AND", $filters_queries);
					$main_filter = $filters_queries[0];
				}
					else {
						$filters_query = " 1";
						$main_filter = " 1";
					}

				$date_pieces = explode('-', $_POST['edate']);
				$year = $date_pieces[0];
				$month = $date_pieces[1];
				$day = $date_pieces[2];

				$sorters = [];
				$sorters_summ = [];
				$sorters_name = [];
				$other_summ = 0;
				if ($params['sI']) {
					#	Определить лидеров по "сортеру"
					$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item, value AS item_name, (SELECT value FROM db_".$dbase."_xls_for_diagrams_columns WHERE id = ".$params['sI'].") AS sorter_name FROM db_".$dbase."_xls_for_diagrams_table JOIN db_".$dbase."_xls_for_diagrams_unique_values ON (db_".$dbase."_xls_for_diagrams_unique_values.column_id = ".$params['sI']." AND db_".$dbase."_xls_for_diagrams_unique_values.id = db_".$dbase."_xls_for_diagrams_table.column_number_".$params['sI'].") WHERE".$filters_query." GROUP BY column_number_".$params['sI']." ORDER BY sorter_summ DESC";
					if ($result = $conn->query($sql)) {
						if ($params['ors']) {
							$sorters = $params['osC'];
							foreach ($sorters as $item) {
								$sorters_summ[$item] = 0;
							}
							while ($obj = $result->fetch_object()) {
								if (in_array($obj->sorter_item, $sorters)) {
									$sorters_summ[$obj->sorter_item] = $obj->sorter_summ;
								}
									else {
										$other_summ += $obj->sorter_summ;
									}
							}
						}
							else {
								$count = 0;
								while ($obj = $result->fetch_object()) {
									$count++;
									if ($count < $params['sCs']) {
										if (($params['sU']) && (( $obj->sorter_name == 'Корпорация') || ( $obj->sorter_name == 'Корпорация - map')) && ($obj->item_name == 'U.M.')) {
											$count--;
											$other_summ += $obj->sorter_summ;
										}
											else {
												$sorters[] = $obj->sorter_item;
												$sorters_summ[$obj->sorter_item] = $obj->sorter_summ;
											}
									}
										else {
											if ((( $obj->sorter_name == 'Корпорация') && ($obj->sorter_item == $mainCorp)) || (( $obj->sorter_name === 'Корпорация - map') && ($obj->sorter_item == $mainCorp))) {
												$other_summ += $sorters_summ[$sorters[$params['sCs'] - 2]];
												$sorters[$params['sCs'] - 2] = $obj->sorter_item;
												$sorters_summ[$obj->sorter_item] = $obj->sorter_summ;
											}
												else {
													$other_summ += $obj->sorter_summ;
												}
										}
								}
							}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
				}
					else {
						foreach ($params['dD'] as $key => $item) {
							$sorters[] = $key;
							$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
							$sorter_name = 'Ценовые диапазоны';
							if ($result = $conn->query($sql)) {
								while ($obj = $result->fetch_object()) {
									$sorters_summ[$key] = $obj->sorter_summ;
								}
							}
								else {
									die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
								}
							$sorters_name[$key] = $key;
						}
					}

				if (!(count($sorters))) {
					echo '{"result":0,"error":"Фильтры отсеяли все значения"}';
					break;
				}

				if ($params['sI']) {
					#	Определить название "сортера"
					$sql = "SELECT value FROM db_".$dbase."_xls_for_diagrams_columns WHERE id = ".$params['sI'];
					$sorter_name = '';
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							$sorter_name = $obj->value;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}

					#	Определить названия лидеров "сортера"
					$sql = "SELECT id, value FROM db_".$dbase."_xls_for_diagrams_unique_values WHERE column_id = ".$params['sI']." AND id IN (".implode(', ', $sorters).")";
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							if (((($sorter_name === 'Корпорация') && ($obj->id == $mainCorp)) || (($sorter_name === 'Корпорация - map') && ($obj->id == $mainCorp))) && (!($params['ors']))) {
								array_splice($sorters, array_search($obj->id, $sorters), 1);
								array_unshift($sorters, $obj->id);
							}
							$sorters_name[$obj->id] = $obj->value;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
				}

				#	Определить название второго "сортера"
				$sql = "SELECT value FROM db_".$dbase."_xls_for_diagrams_columns WHERE id = ".$params['sCD'];
				$sub_sorters_name = [];
				$sub_sorters = [];
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$sub_sorters_name[0] = 'Total';
						$sub_sorters[] = 0;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}

				#	Определить названия категорий второго "сортера"
				
				#	тут добавить фильтр полученных ИТОГОВЫХ айди по выбранному списку - остальное сработает автоматически (!), по поводу упорядочивания - подумать над этим, вероятнее в пост обработке
				
				
				$sql = "SELECT id, value FROM db_".$dbase."_xls_for_diagrams_unique_values WHERE id IN (SELECT DISTINCT column_number_".$params['sCD']." FROM `db_".$dbase."_xls_for_diagrams_table` WHERE".$filters_query.") AND id IN (".implode(',',$params['ssC']).")";
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$sub_sorters_name[$obj->id] = $obj->value;
						#$sub_sorters[] = $obj->id;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				foreach ($params['ssC'] as $ss_id) {
					$sub_sorters[] = $ss_id;
				}

				$months = ['', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

				#	Определить данные для графиков YTD
				$yearToDayData = [];
				$ytdInd = 0;
				foreach ($sub_sorters as $ss_key) {
					$yearToDayData[$ss_key]['currY'] = ['Other' => 0];
					$yearToDayData[$ss_key]['prevY'] = ['Other' => 0];
					$yearToDayData[$ss_key]['lastY'] = ['Other' => 0];
					$yearToDayData[$ss_key]['oldY'] = ['Other' => 0];
					$yearToDayData[$ss_key]['currYSumm'] = 0;
					$yearToDayData[$ss_key]['prevYSumm'] = 0;
					$yearToDayData[$ss_key]['lastYSumm'] = 0;
					$yearToDayData[$ss_key]['oldYSumm'] = 0;
					$yearToDayData[$ss_key]['currYSummPT3'] = 0;
					$yearToDayData[$ss_key]['prevYSummPT3'] = 0;
					$yearToDayData[$ss_key]['lastYSummPT3'] = 0;
					$yearToDayData[$ss_key]['oldYSummPT3'] = 0;
					if ($ss_key) {
						$sorter_filter = " AND column_number_".$params['sCD']." = ".$ss_key;
						$ytdInd++;
						$yearToDayLabels[] = $ytdInd;
						$ytdInd++;
						$yearToDayLabels[] = $ytdInd;
						$ytdInd++;
						$yearToDayLabels[] = $ytdInd;
					} else {
						$sorter_filter = " AND 1";
						$yearToDayLabels = [
							"Jan".substr(($year - 2), -2)."-".$months[(int)$month]."".substr(($year - 2), -2),
							"Jan".substr(($year - 1), -2)."-".$months[(int)$month]."".substr(($year - 1), -2),
							"Jan".substr($year, -2)."-".$months[(int)$month]."".substr($year, -2),
							#"YTD".substr(($year - 1), -2)." ".$months[$month],
							#"YTD".substr($year, -2)." ".$months[$month],
							''
						];
					}
					$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-0), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-0), $month, $day);
					#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							$yearToDayData[$ss_key]['currYSummPT3'] = $obj->sorter_summ;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
					$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-1), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-1), $month, $day);
					#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							$yearToDayData[$ss_key]['prevYSummPT3'] = $obj->sorter_summ;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
					$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-2), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-2), $month, $day);
					#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							$yearToDayData[$ss_key]['lastYSummPT3'] = $obj->sorter_summ;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
					$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter.$sorter_filter." AND sorting_date BETWEEN ".($year-3)."0101 AND ".($year-3).$month.$day;
					#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							$yearToDayData[$ss_key]['oldYSummPT3'] = $obj->sorter_summ;	#	lastYSummPT3	???
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
					if ($params['sI']) {
						$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-0), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-0), $month, $day)." GROUP BY column_number_".$params['sI'];
						#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								if (array_key_exists($obj->sorter_item, $sorters_name)) {
									$yearToDayData[$ss_key]['currY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
									$yearToDayData[$ss_key]['currYSumm'] += $obj->sorter_summ;
								}
									else {
										$yearToDayData[$ss_key]['currY']['Other'] += $obj->sorter_summ;
										if ($params['oos']) {
											$yearToDayData[$ss_key]['currYSumm'] += $obj->sorter_summ;
										}
									}
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
						$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-1), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-1), $month, $day)." GROUP BY column_number_".$params['sI'];
						#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								if (array_key_exists($obj->sorter_item, $sorters_name)) {
									$yearToDayData[$ss_key]['prevY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
									$yearToDayData[$ss_key]['prevYSumm'] += $obj->sorter_summ;
								}
									else {
										$yearToDayData[$ss_key]['prevY']['Other'] += $obj->sorter_summ;
										if ($params['oos']) {
											$yearToDayData[$ss_key]['prevYSumm'] += $obj->sorter_summ;
										}
									}
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
						$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-2), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-2), $month, $day)." GROUP BY column_number_".$params['sI'];
						#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								if (array_key_exists($obj->sorter_item, $sorters_name)) {
									$yearToDayData[$ss_key]['lastY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
									$yearToDayData[$ss_key]['lastYSumm'] += $obj->sorter_summ;
								}
									else {
										$yearToDayData[$ss_key]['lastY']['Other'] += $obj->sorter_summ;
										if ($params['oos']) {
											$yearToDayData[$ss_key]['lastYSumm'] += $obj->sorter_summ;
										}
									}
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
						$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".($year-3)."0101 AND ".($year-3).$month.$day." GROUP BY column_number_".$params['sI'];
						#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								if (array_key_exists($obj->sorter_item, $sorters_name)) {
									$yearToDayData[$ss_key]['oldY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
									$yearToDayData[$ss_key]['oldYSumm'] += $obj->sorter_summ;
								}
									else {
										$yearToDayData[$ss_key]['oldY']['Other'] += $obj->sorter_summ;
										if ($params['oos']) {
											$yearToDayData[$ss_key]['oldYSumm'] += $obj->sorter_summ;
										}
									}
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
					}
						else {
							foreach ($params['dD'] as $key => $item) {
								$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-0), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-0), $month, $day)." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
								#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
								if ($result = $conn->query($sql)) {
									while ($obj = $result->fetch_object()) {
										$yearToDayData[$ss_key]['currY'][$key] = $obj->sorter_summ;
										$yearToDayData[$ss_key]['currYSumm'] += $obj->sorter_summ;
									}
								}
									else {
										die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
									}
								$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-1), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-1), $month, $day)." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
								#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
								if ($result = $conn->query($sql)) {
									while ($obj = $result->fetch_object()) {
										$yearToDayData[$ss_key]['prevY'][$key] = $obj->sorter_summ;
										$yearToDayData[$ss_key]['prevYSumm'] += $obj->sorter_summ;
									}
								}
									else {
										die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
									}
								$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-2), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-2), $month, $day)." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
								#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
								if ($result = $conn->query($sql)) {
									while ($obj = $result->fetch_object()) {
										$yearToDayData[$ss_key]['lastY'][$key] = $obj->sorter_summ;
										$yearToDayData[$ss_key]['lastYSumm'] += $obj->sorter_summ;
									}
								}
									else {
										die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
									}
								$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".($year-3)."0101 AND ".($year-3).$month.$day." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
								#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
								if ($result = $conn->query($sql)) {
									while ($obj = $result->fetch_object()) {
										$yearToDayData[$ss_key]['oldY'][$key] = $obj->sorter_summ;
										$yearToDayData[$ss_key]['oldYSumm'] += $obj->sorter_summ;
									}
								}
									else {
										die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
									}
							}
						}
					if (($other_summ > 0) && ($sorters_name[0] != 'Other') && ($params['oos'])) {
						$sorters[] = 0;
						$sorters_name[0] = 'Other';
					}
					#	Подсчет процентного выражения и приведение величин к масштабу миллиардов
					foreach ($sorters_name as $key) {
						#	Проценты
						$yearToDayData[$ss_key]['currYPerc'][$key] = (($yearToDayData[$ss_key]['currYSumm'] != 0) ? (round($yearToDayData[$ss_key]['currY'][$key] / $yearToDayData[$ss_key]['currYSumm'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['prevYPerc'][$key] = (($yearToDayData[$ss_key]['prevYSumm'] != 0) ? (round($yearToDayData[$ss_key]['prevY'][$key] / $yearToDayData[$ss_key]['prevYSumm'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['lastYPerc'][$key] = (($yearToDayData[$ss_key]['lastYSumm'] != 0) ? (round($yearToDayData[$ss_key]['lastY'][$key] / $yearToDayData[$ss_key]['lastYSumm'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['oldYPerc'][$key] = (($yearToDayData[$ss_key]['oldYSumm'] != 0) ? (round($yearToDayData[$ss_key]['oldY'][$key] / $yearToDayData[$ss_key]['oldYSumm'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['currYPercPT3'][$key] = (($yearToDayData[$ss_key]['currYSummPT3'] != 0) ? (round($yearToDayData[$ss_key]['currY'][$key] / $yearToDayData[$ss_key]['currYSummPT3'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['prevYPercPT3'][$key] = (($yearToDayData[$ss_key]['prevYSummPT3'] != 0) ? (round($yearToDayData[$ss_key]['prevY'][$key] / $yearToDayData[$ss_key]['prevYSummPT3'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['lastYPercPT3'][$key] = (($yearToDayData[$ss_key]['lastYSummPT3'] != 0) ? (round($yearToDayData[$ss_key]['lastY'][$key] / $yearToDayData[$ss_key]['lastYSummPT3'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['oldYPercPT3'][$key] = (($yearToDayData[$ss_key]['oldYSummPT3'] != 0) ? (round($yearToDayData[$ss_key]['oldY'][$key] / $yearToDayData[$ss_key]['oldYSummPT3'] * 1000) / 10) : 0.0);
						if (!($params['sP'])) {
							#	Масштабирование
							$yearToDayData[$ss_key]['currY'][$key] = round($yearToDayData[$ss_key]['currY'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['prevY'][$key] = round($yearToDayData[$ss_key]['prevY'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['lastY'][$key] = round($yearToDayData[$ss_key]['lastY'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['oldY'][$key] = round($yearToDayData[$ss_key]['oldY'][$key] / $msscht2) / 10000;
						} else if ($params['sP'] == 1) {
							#	Масштабирование
							$yearToDayData[$ss_key]['currY'][$key] = round($yearToDayData[$ss_key]['currY'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['prevY'][$key] = round($yearToDayData[$ss_key]['prevY'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['lastY'][$key] = round($yearToDayData[$ss_key]['lastY'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['oldY'][$key] = round($yearToDayData[$ss_key]['oldY'][$key] / $msscht2) / 10000;
						}
					}
					#	Масштабирование суммарных данных
					/*if (!($params['sP'])) {
						$yearToDayData[$ss_key]['currYSumm'] = round($yearToDayData[$ss_key]['currYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['prevYSumm'] = round($yearToDayData[$ss_key]['prevYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['lastYSumm'] = round($yearToDayData[$ss_key]['lastYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['oldYSumm'] = round($yearToDayData[$ss_key]['oldYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['currYSummPT3'] = round($yearToDayData[$ss_key]['currYSummPT3'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['prevYSummPT3'] = round($yearToDayData[$ss_key]['prevYSummPT3'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['lastYSummPT3'] = round($yearToDayData[$ss_key]['lastYSummPT3'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['oldYSummPT3'] = round($yearToDayData[$ss_key]['oldYSummPT3'] / $msscht1) / 10;
					} else if ($params['sP'] == 1) {
						$yearToDayData[$ss_key]['currYSumm'] = round($yearToDayData[$ss_key]['currYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['prevYSumm'] = round($yearToDayData[$ss_key]['prevYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['lastYSumm'] = round($yearToDayData[$ss_key]['lastYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['oldYSumm'] = round($yearToDayData[$ss_key]['oldYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['currYSummPT3'] = round($yearToDayData[$ss_key]['currYSummPT3'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['prevYSummPT3'] = round($yearToDayData[$ss_key]['prevYSummPT3'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['lastYSummPT3'] = round($yearToDayData[$ss_key]['lastYSummPT3'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['oldYSummPT3'] = round($yearToDayData[$ss_key]['oldYSummPT3'] / $msscht1) / 10;
					}*/
					#	Определение величин приращения (для легенды)/ формирование данных легенды
				}

				switch ($params['sos']) {
					case 0: {
						break;
					}
					case 1: {
						for ($i = 1; $i < (count($sub_sorters) - 1); $i++) {
							$max = $yearToDayData[$sub_sorters[$i]]['lastYSumm'];
							$maxnum = $i;
							for ($j = ($i + 1); $j < count($sub_sorters); $j++) {
								if ($max < $yearToDayData[$sub_sorters[$j]]['lastYSumm']) {
									$max = $yearToDayData[$sub_sorters[$j]]['lastYSumm'];
									$maxnum = $j;
								}
							}
							if ($maxnum > $i) {
								$tmp = $sub_sorters[$i];
								$sub_sorters[$i] = $sub_sorters[$maxnum];
								$sub_sorters[$maxnum] = $tmp;
							}
						}
						break;
					}
					case 2: {
						for ($i = 1; $i < (count($sub_sorters) - 1); $i++) {
							$max = $yearToDayData[$sub_sorters[$i]]['prevYSumm'];
							$maxnum = $i;
							for ($j = ($i + 1); $j < count($sub_sorters); $j++) {
								if ($max < $yearToDayData[$sub_sorters[$j]]['prevYSumm']) {
									$max = $yearToDayData[$sub_sorters[$j]]['prevYSumm'];
									$maxnum = $j;
								}
							}
							if ($maxnum > $i) {
								$tmp = $sub_sorters[$i];
								$sub_sorters[$i] = $sub_sorters[$maxnum];
								$sub_sorters[$maxnum] = $tmp;
							}
						}
						break;
					}
					case 3: {
						for ($i = 1; $i < (count($sub_sorters) - 1); $i++) {
							$max = $yearToDayData[$sub_sorters[$i]]['currYSumm'];
							$maxnum = $i;
							for ($j = ($i + 1); $j < count($sub_sorters); $j++) {
								if ($max < $yearToDayData[$sub_sorters[$j]]['currYSumm']) {
									$max = $yearToDayData[$sub_sorters[$j]]['currYSumm'];
									$maxnum = $j;
								}
							}
							if ($maxnum > $i) {
								$tmp = $sub_sorters[$i];
								$sub_sorters[$i] = $sub_sorters[$maxnum];
								$sub_sorters[$maxnum] = $tmp;
							}
						}
						break;
					}
					case 4: {
						for ($i = 1; $i < (count($sub_sorters) - 1); $i++) {
							$max = $yearToDayData[$sub_sorters[$i]]['currYSumm'] + $yearToDayData[$sub_sorters[$i]]['prevYSumm'] + $yearToDayData[$sub_sorters[$i]]['lastYSumm'];
							$maxnum = $i;
							for ($j = ($i + 1); $j < count($sub_sorters); $j++) {
								if ($max < ($yearToDayData[$sub_sorters[$j]]['currYSumm'] + $yearToDayData[$sub_sorters[$j]]['prevYSumm'] + $yearToDayData[$sub_sorters[$j]]['lastYSumm'])) {
									$max = $yearToDayData[$sub_sorters[$j]]['currYSumm'] + $yearToDayData[$sub_sorters[$j]]['prevYSumm'] + $yearToDayData[$sub_sorters[$j]]['lastYSumm'];
									$maxnum = $j;
								}
							}
							if ($maxnum > $i) {
								$tmp = $sub_sorters[$i];
								$sub_sorters[$i] = $sub_sorters[$maxnum];
								$sub_sorters[$maxnum] = $tmp;
							}
						}
						break;
					}
				}

				$legends = [];
				$sub_names = [];
				foreach ($sub_sorters as $ss_key) {
					$sub_names[] = $sub_sorters_name[$ss_key];
					if ($params['gT'] === '2') {//$params['gT'] === '2'	??? было iI
						$legends['up'][] = $yearToDayData[$ss_key]['lastYSummPT3'];
						$legends['up'][] = $yearToDayData[$ss_key]['prevYSummPT3'];
						$legends['up'][] = $yearToDayData[$ss_key]['currYSummPT3'];
						$legends['down'][] = (($yearToDayData[$ss_key]['oldYSummPT3'] != 0) ? (round((($yearToDayData[$ss_key]['lastYSummPT3'] - $yearToDayData[$ss_key]['oldYSummPT3']) / $yearToDayData[$ss_key]['oldYSummPT3']) * 1000) / 10) : 0.0);
						$legends['down'][] = (($yearToDayData[$ss_key]['lastYSummPT3'] != 0) ? (round((($yearToDayData[$ss_key]['prevYSummPT3'] - $yearToDayData[$ss_key]['lastYSummPT3']) / $yearToDayData[$ss_key]['lastYSummPT3']) * 1000) / 10) : 0.0);
						$legends['down'][] = (($yearToDayData[$ss_key]['prevYSummPT3'] != 0) ? (round((($yearToDayData[$ss_key]['currYSummPT3'] - $yearToDayData[$ss_key]['prevYSummPT3']) / $yearToDayData[$ss_key]['prevYSummPT3']) * 1000) / 10) : 0.0);
						$legends['color'][] = (($yearToDayData[$ss_key]['lastYSummPT3'] < $yearToDayData[$ss_key]['oldYSummPT3']) ? 'Red' : 'Green');
						$legends['color'][] = (($yearToDayData[$ss_key]['prevYSummPT3'] < $yearToDayData[$ss_key]['lastYSummPT3']) ? 'Red' : 'Green');
						$legends['color'][] = (($yearToDayData[$ss_key]['currYSummPT3'] < $yearToDayData[$ss_key]['prevYSummPT3']) ? 'Red' : 'Green');
					} else {
						$legends['up'][] = $yearToDayData[$ss_key]['lastYSumm'];
						$legends['up'][] = $yearToDayData[$ss_key]['prevYSumm'];
						$legends['up'][] = $yearToDayData[$ss_key]['currYSumm'];
						$legends['down'][] = (($yearToDayData[$ss_key]['oldYSumm'] != 0) ? (round((($yearToDayData[$ss_key]['lastYSumm'] - $yearToDayData[$ss_key]['oldYSumm']) / $yearToDayData[$ss_key]['oldYSumm']) * 1000) / 10) : 0.0);
						$legends['down'][] = (($yearToDayData[$ss_key]['lastYSumm'] != 0) ? (round((($yearToDayData[$ss_key]['prevYSumm'] - $yearToDayData[$ss_key]['lastYSumm']) / $yearToDayData[$ss_key]['lastYSumm']) * 1000) / 10) : 0.0);
						$legends['down'][] = (($yearToDayData[$ss_key]['prevYSumm'] != 0) ? (round((($yearToDayData[$ss_key]['currYSumm'] - $yearToDayData[$ss_key]['prevYSumm']) / $yearToDayData[$ss_key]['prevYSumm']) * 1000) / 10) : 0.0);
						$legends['color'][] = (($yearToDayData[$ss_key]['lastYSumm'] < $yearToDayData[$ss_key]['oldYSumm']) ? 'Red' : 'Green');
						$legends['color'][] = (($yearToDayData[$ss_key]['prevYSumm'] < $yearToDayData[$ss_key]['lastYSumm']) ? 'Red' : 'Green');
						$legends['color'][] = (($yearToDayData[$ss_key]['currYSumm'] < $yearToDayData[$ss_key]['prevYSumm']) ? 'Red' : 'Green');
					}
					if (!($ss_key)) {
						$legends['up'][] = '';
						$legends['down'][] = '';
						$legends['color'][] = 'Black';
					}
				}
				#	Формирование итогового ответа
				$results = [];
				$names = [];
				foreach ($sorters as $key) {
					$sub_res = [];
					foreach ($sub_sorters as $ss_key) {
						$sub_res[] = '{"idv":"'.$yearToDayData[$ss_key]['lastY'][$sorters_name[$key]].'","idppt3":"'.$yearToDayData[$ss_key]['lastYPerc'][$sorters_name[$key]].'","lbl":"'.$sub_sorters_name[$ss_key].'"}';
						$sub_res[] = '{"idv":"'.$yearToDayData[$ss_key]['prevY'][$sorters_name[$key]].'","idppt3":"'.$yearToDayData[$ss_key]['prevYPerc'][$sorters_name[$key]].'","lbl":"'.$sub_sorters_name[$ss_key].'"}';
						$sub_res[] = '{"idv":"'.$yearToDayData[$ss_key]['currY'][$sorters_name[$key]].'","idppt3":"'.$yearToDayData[$ss_key]['currYPerc'][$sorters_name[$key]].'","lbl":"'.$sub_sorters_name[$ss_key].'"}';
						if (!($ss_key)) {
							$sub_res[] =  '{"idv":"","idppt3":"","lbl":""}';
						}
					}
					$result = '{"iD":['.implode(',',$sub_res).']}';
					$results[] = $result;
					$names[] = $sorters_name[$key];
				}

				echo '{"result":1,"slide_legend":"'.$slide_legend.'","data":{"ss":['.implode(',',$sorters).'],"sBs":['.implode(',',$sub_sorters).'],"sNs":["'.implode('","',$names).'"],"sBNs":["'.implode('","',$sub_names).'"],"rs":['.implode(',',$results).'],"lUs":["'.implode('","',$legends['up']).'"],"lDs":["'.implode('","',$legends['down']).'"],"lCs":["'.implode('","',$legends['color']).'"],"lYs":["'.implode('","',$yearToDayLabels).'"]}}';
			}
			else if ($page == 11) {
				$filters_queries = [];

				foreach ($params['fD'] as $item) {
					$filters_queries[] = " column_number_".$item['cI']." IN (".implode(', ',$item['vI']).")";
				}
				if ($filters_queries) {
					$filters_query = implode(" AND", $filters_queries);
					$main_filter = $filters_queries[0];
				}
					else {
						$filters_query = " 1";
						$main_filter = " 1";
					}

				$date_pieces = explode('-', $_POST['edate']);
				$year = $date_pieces[0];
				$month = $date_pieces[1];
				$day = $date_pieces[2];

				$sorters = [];
				$sorters_summ = [];
				$sorters_name = [];
				$other_summ = 0;
				if ($params['sI']) {
					#	Определить лидеров по "сортеру"
					$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item, value AS item_name, (SELECT value FROM db_".$dbase."_xls_for_diagrams_columns WHERE id = ".$params['sI'].") AS sorter_name FROM db_".$dbase."_xls_for_diagrams_table JOIN db_".$dbase."_xls_for_diagrams_unique_values ON (db_".$dbase."_xls_for_diagrams_unique_values.column_id = ".$params['sI']." AND db_".$dbase."_xls_for_diagrams_unique_values.id = db_".$dbase."_xls_for_diagrams_table.column_number_".$params['sI'].") WHERE".$filters_query." GROUP BY column_number_".$params['sI']." ORDER BY sorter_summ DESC";
					if ($result = $conn->query($sql)) {
						if ($params['ors']) {
							$sorters = $params['osC'];
							foreach ($sorters as $item) {
								$sorters_summ[$item] = 0;
							}
							while ($obj = $result->fetch_object()) {
								if (in_array($obj->sorter_item, $sorters)) {
									$sorters_summ[$obj->sorter_item] = $obj->sorter_summ;
								}
									else {
										$other_summ += $obj->sorter_summ;
									}
							}
						}
							else {
								$count = 0;
								while ($obj = $result->fetch_object()) {
									$count++;
									if ($count < $params['sCs']) {
										if (($params['sU']) && (( $obj->sorter_name == 'Корпорация') || ( $obj->sorter_name == 'Корпорация - map')) && ($obj->item_name == 'U.M.')) {
											$count--;
											$other_summ += $obj->sorter_summ;
										}
											else {
												$sorters[] = $obj->sorter_item;
												$sorters_summ[$obj->sorter_item] = $obj->sorter_summ;
											}
									}
										else {
											if ((( $obj->sorter_name == 'Корпорация') && ($obj->sorter_item == $mainCorp)) || (( $obj->sorter_name === 'Корпорация - map') && ($obj->sorter_item == $mainCorp))) {
												$other_summ += $sorters_summ[$sorters[$params['sCs'] - 2]];
												$sorters[$params['sCs'] - 2] = $obj->sorter_item;
												$sorters_summ[$obj->sorter_item] = $obj->sorter_summ;
											}
												else {
													$other_summ += $obj->sorter_summ;
												}
										}
								}
							}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
				}
					else {
						foreach ($params['dD'] as $key => $item) {
							$sorters[] = $key;
							$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
							$sorter_name = 'Ценовые диапазоны';
							if ($result = $conn->query($sql)) {
								while ($obj = $result->fetch_object()) {
									$sorters_summ[$key] = $obj->sorter_summ;
								}
							}
								else {
									die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
								}
							$sorters_name[$key] = $key;
						}
					}

				if (!(count($sorters))) {
					echo '{"result":0,"error":"Фильтры отсеяли все значения"}';
					break;
				}

				if ($params['sI']) {
					#	Определить название "сортера"
					$sql = "SELECT value FROM db_".$dbase."_xls_for_diagrams_columns WHERE id = ".$params['sI'];
					$sorter_name = '';
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							$sorter_name = $obj->value;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}

					#	Определить названия лидеров "сортера"
					$sql = "SELECT id, value FROM db_".$dbase."_xls_for_diagrams_unique_values WHERE column_id = ".$params['sI']." AND id IN (".implode(', ', $sorters).")";
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							if (((($sorter_name === 'Корпорация') && ($obj->id == $mainCorp)) || (($sorter_name === 'Корпорация - map') && ($obj->id == $mainCorp))) && (!($params['ors']))) {
								array_splice($sorters, array_search($obj->id, $sorters), 1);
								array_unshift($sorters, $obj->id);
							}
							$sorters_name[$obj->id] = $obj->value;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
				}

				#	Определить название второго "сортера"
				$sql = "SELECT value FROM db_".$dbase."_xls_for_diagrams_columns WHERE id = ".$params['sCD'];
				$sub_sorters_name = [];
				$sub_sorters = [];
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$sub_sorters_name[0] = 'Total';
						$sub_sorters[] = 0;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}

				#	Определить названия категорий второго "сортера"
				
				#	тут добавить фильтр полученных ИТОГОВЫХ айди по выбранному списку - остальное сработает автоматически (!), по поводу упорядочивания - подумать над этим, вероятнее в пост обработке
				
				
				$sql = "SELECT id, value FROM db_".$dbase."_xls_for_diagrams_unique_values WHERE id IN (SELECT DISTINCT column_number_".$params['sCD']." FROM `db_".$dbase."_xls_for_diagrams_table` WHERE".$filters_query.") AND id IN (".implode(',',$params['ssC']).")";
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$sub_sorters_name[$obj->id] = $obj->value;
						#$sub_sorters[] = $obj->id;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				foreach ($params['ssC'] as $ss_id) {
					$sub_sorters[] = $ss_id;
				}

				$months = ['', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

				#	Определить данные для графиков YTD
				$yearToDayData = [];
				$ytdInd = 0;
				foreach ($sub_sorters as $ss_key) {
					$yearToDayData[$ss_key]['currY'] = ['Other' => 0];
					$yearToDayData[$ss_key]['prevY'] = ['Other' => 0];
					$yearToDayData[$ss_key]['lastY'] = ['Other' => 0];
					$yearToDayData[$ss_key]['oldY'] = ['Other' => 0];
					$yearToDayData[$ss_key]['currYSumm'] = 0;
					$yearToDayData[$ss_key]['prevYSumm'] = 0;
					$yearToDayData[$ss_key]['lastYSumm'] = 0;
					$yearToDayData[$ss_key]['oldYSumm'] = 0;
					$yearToDayData[$ss_key]['currYSummPT3'] = 0;
					$yearToDayData[$ss_key]['prevYSummPT3'] = 0;
					$yearToDayData[$ss_key]['lastYSummPT3'] = 0;
					$yearToDayData[$ss_key]['oldYSummPT3'] = 0;
					if ($ss_key) {
						$sorter_filter = " AND column_number_".$params['sCD']." = ".$ss_key;
						$ytdInd++;
						$yearToDayLabels[] = $ytdInd;
						$ytdInd++;
						$yearToDayLabels[] = $ytdInd;
						$ytdInd++;
						$yearToDayLabels[] = $ytdInd;
					} else {
						$sorter_filter = " AND 1";
						$yearToDayLabels = [
							((($month + 1) > 12) ? "Jan".substr(($year - 2), -2) : $months[(int)$month + 1]."".substr(($year - 3), -2))."-".$months[(int)$month]."".substr(($year - 2), -2),
							((($month + 1) > 12) ? "Jan".substr(($year - 1), -2) : $months[(int)$month + 1]."".substr(($year - 2), -2))."-".$months[(int)$month]."".substr(($year - 1), -2),
							((($month + 1) > 12) ? "Jan".substr($year, -2) : $months[(int)$month + 1]."".substr(($year - 1), -2))."-".$months[(int)$month]."".substr($year, -2),
							''
						];
					}
					$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-0) : ($year-1)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-0), $month, "31");
					#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							$yearToDayData[$ss_key]['currYSummPT3'] = $obj->sorter_summ;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
					$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-1) : ($year-2)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-1), $month, "31");
					#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							$yearToDayData[$ss_key]['prevYSummPT3'] = $obj->sorter_summ;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
					$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-2) : ($year-3)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-2), $month, "31");
					#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							$yearToDayData[$ss_key]['lastYSummPT3'] = $obj->sorter_summ;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
					$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-3) : ($year-4)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-3), $month, "31");
					#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							$yearToDayData[$ss_key]['oldYSummPT3'] = $obj->sorter_summ;	#	lastYSummPT3	???
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
					if ($params['sI']) {
						$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-0) : ($year-1)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-0), $month, "31")." GROUP BY column_number_".$params['sI'];
						#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								if (array_key_exists($obj->sorter_item, $sorters_name)) {
									$yearToDayData[$ss_key]['currY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
									$yearToDayData[$ss_key]['currYSumm'] += $obj->sorter_summ;
								}
									else {
										$yearToDayData[$ss_key]['currY']['Other'] += $obj->sorter_summ;
										if ($params['oos']) {
											$yearToDayData[$ss_key]['currYSumm'] += $obj->sorter_summ;
										}
									}
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
						$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-1) : ($year-2)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-1), $month, "31")." GROUP BY column_number_".$params['sI'];
						#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								if (array_key_exists($obj->sorter_item, $sorters_name)) {
									$yearToDayData[$ss_key]['prevY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
									$yearToDayData[$ss_key]['prevYSumm'] += $obj->sorter_summ;
								}
									else {
										$yearToDayData[$ss_key]['prevY']['Other'] += $obj->sorter_summ;
										if ($params['oos']) {
											$yearToDayData[$ss_key]['prevYSumm'] += $obj->sorter_summ;
										}
									}
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
						$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-2) : ($year-3)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-2), $month, "31")." GROUP BY column_number_".$params['sI'];
						#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								if (array_key_exists($obj->sorter_item, $sorters_name)) {
									$yearToDayData[$ss_key]['lastY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
									$yearToDayData[$ss_key]['lastYSumm'] += $obj->sorter_summ;
								}
									else {
										$yearToDayData[$ss_key]['lastY']['Other'] += $obj->sorter_summ;
										if ($params['oos']) {
											$yearToDayData[$ss_key]['lastYSumm'] += $obj->sorter_summ;
										}
									}
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
						$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-3) : ($year-4)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-3), $month, "31")." GROUP BY column_number_".$params['sI'];
						#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								if (array_key_exists($obj->sorter_item, $sorters_name)) {
									$yearToDayData[$ss_key]['oldY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
									$yearToDayData[$ss_key]['oldYSumm'] += $obj->sorter_summ;
								}
									else {
										$yearToDayData[$ss_key]['oldY']['Other'] += $obj->sorter_summ;
										if ($params['oos']) {
											$yearToDayData[$ss_key]['oldYSumm'] += $obj->sorter_summ;
										}
									}
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
					}
						else {
							foreach ($params['dD'] as $key => $item) {
								$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-0) : ($year-1)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-0), $month, "31")." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
								#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
								if ($result = $conn->query($sql)) {
									while ($obj = $result->fetch_object()) {
										$yearToDayData[$ss_key]['currY'][$key] = $obj->sorter_summ;
										$yearToDayData[$ss_key]['currYSumm'] += $obj->sorter_summ;
									}
								}
									else {
										die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
									}
								$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-1) : ($year-2)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-1), $month, "31")." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
								#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
								if ($result = $conn->query($sql)) {
									while ($obj = $result->fetch_object()) {
										$yearToDayData[$ss_key]['prevY'][$key] = $obj->sorter_summ;
										$yearToDayData[$ss_key]['prevYSumm'] += $obj->sorter_summ;
									}
								}
									else {
										die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
									}
								$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-2) : ($year-3)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-2), $month, "31")." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
								#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
								if ($result = $conn->query($sql)) {
									while ($obj = $result->fetch_object()) {
										$yearToDayData[$ss_key]['lastY'][$key] = $obj->sorter_summ;
										$yearToDayData[$ss_key]['lastYSumm'] += $obj->sorter_summ;
									}
								}
									else {
										die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
									}
								$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-3) : ($year-4)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-3), $month, "31")." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
								#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
								if ($result = $conn->query($sql)) {
									while ($obj = $result->fetch_object()) {
										$yearToDayData[$ss_key]['oldY'][$key] = $obj->sorter_summ;
										$yearToDayData[$ss_key]['oldYSumm'] += $obj->sorter_summ;
									}
								}
									else {
										die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
									}
							}
						}
					if (($other_summ > 0) && ($sorters_name[0] != 'Other') && ($params['oos'])) {
						$sorters[] = 0;
						$sorters_name[0] = 'Other';
					}
					#	Подсчет процентного выражения и приведение величин к масштабу миллиардов
					foreach ($sorters_name as $key) {
						#	Проценты
						$yearToDayData[$ss_key]['currYPerc'][$key] = (($yearToDayData[$ss_key]['currYSumm'] != 0) ? (round($yearToDayData[$ss_key]['currY'][$key] / $yearToDayData[$ss_key]['currYSumm'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['prevYPerc'][$key] = (($yearToDayData[$ss_key]['prevYSumm'] != 0) ? (round($yearToDayData[$ss_key]['prevY'][$key] / $yearToDayData[$ss_key]['prevYSumm'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['lastYPerc'][$key] = (($yearToDayData[$ss_key]['lastYSumm'] != 0) ? (round($yearToDayData[$ss_key]['lastY'][$key] / $yearToDayData[$ss_key]['lastYSumm'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['oldYPerc'][$key] = (($yearToDayData[$ss_key]['oldYSumm'] != 0) ? (round($yearToDayData[$ss_key]['oldY'][$key] / $yearToDayData[$ss_key]['oldYSumm'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['currYPercPT3'][$key] = (($yearToDayData[$ss_key]['currYSummPT3'] != 0) ? (round($yearToDayData[$ss_key]['currY'][$key] / $yearToDayData[$ss_key]['currYSummPT3'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['prevYPercPT3'][$key] = (($yearToDayData[$ss_key]['prevYSummPT3'] != 0) ? (round($yearToDayData[$ss_key]['prevY'][$key] / $yearToDayData[$ss_key]['prevYSummPT3'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['lastYPercPT3'][$key] = (($yearToDayData[$ss_key]['lastYSummPT3'] != 0) ? (round($yearToDayData[$ss_key]['lastY'][$key] / $yearToDayData[$ss_key]['lastYSummPT3'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['oldYPercPT3'][$key] = (($yearToDayData[$ss_key]['oldYSummPT3'] != 0) ? (round($yearToDayData[$ss_key]['oldY'][$key] / $yearToDayData[$ss_key]['oldYSummPT3'] * 1000) / 10) : 0.0);
						if (!($params['sP'])) {
							#	Масштабирование
							$yearToDayData[$ss_key]['currY'][$key] = round($yearToDayData[$ss_key]['currY'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['prevY'][$key] = round($yearToDayData[$ss_key]['prevY'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['lastY'][$key] = round($yearToDayData[$ss_key]['lastY'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['oldY'][$key] = round($yearToDayData[$ss_key]['oldY'][$key] / $msscht2) / 10000;
						} else if ($params['sP'] == 1) {
							#	Масштабирование
							$yearToDayData[$ss_key]['currY'][$key] = round($yearToDayData[$ss_key]['currY'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['prevY'][$key] = round($yearToDayData[$ss_key]['prevY'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['lastY'][$key] = round($yearToDayData[$ss_key]['lastY'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['oldY'][$key] = round($yearToDayData[$ss_key]['oldY'][$key] / $msscht2) / 10000;
						}
					}
					#	Масштабирование суммарных данных
					/*if (!($params['sP'])) {
						$yearToDayData[$ss_key]['currYSumm'] = round($yearToDayData[$ss_key]['currYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['prevYSumm'] = round($yearToDayData[$ss_key]['prevYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['lastYSumm'] = round($yearToDayData[$ss_key]['lastYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['oldYSumm'] = round($yearToDayData[$ss_key]['oldYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['currYSummPT3'] = round($yearToDayData[$ss_key]['currYSummPT3'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['prevYSummPT3'] = round($yearToDayData[$ss_key]['prevYSummPT3'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['lastYSummPT3'] = round($yearToDayData[$ss_key]['lastYSummPT3'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['oldYSummPT3'] = round($yearToDayData[$ss_key]['oldYSummPT3'] / $msscht1) / 10;
					} else if ($params['sP'] == 1) {
						$yearToDayData[$ss_key]['currYSumm'] = round($yearToDayData[$ss_key]['currYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['prevYSumm'] = round($yearToDayData[$ss_key]['prevYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['lastYSumm'] = round($yearToDayData[$ss_key]['lastYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['oldYSumm'] = round($yearToDayData[$ss_key]['oldYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['currYSummPT3'] = round($yearToDayData[$ss_key]['currYSummPT3'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['prevYSummPT3'] = round($yearToDayData[$ss_key]['prevYSummPT3'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['lastYSummPT3'] = round($yearToDayData[$ss_key]['lastYSummPT3'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['oldYSummPT3'] = round($yearToDayData[$ss_key]['oldYSummPT3'] / $msscht1) / 10;
					}*/
					#	Определение величин приращения (для легенды)/ формирование данных легенды
				}

				switch ($params['sos']) {
					case 0: {
						break;
					}
					case 1: {
						for ($i = 1; $i < (count($sub_sorters) - 1); $i++) {
							$max = $yearToDayData[$sub_sorters[$i]]['lastYSumm'];
							$maxnum = $i;
							for ($j = ($i + 1); $j < count($sub_sorters); $j++) {
								if ($max < $yearToDayData[$sub_sorters[$j]]['lastYSumm']) {
									$max = $yearToDayData[$sub_sorters[$j]]['lastYSumm'];
									$maxnum = $j;
								}
							}
							if ($maxnum > $i) {
								$tmp = $sub_sorters[$i];
								$sub_sorters[$i] = $sub_sorters[$maxnum];
								$sub_sorters[$maxnum] = $tmp;
							}
						}
						break;
					}
					case 2: {
						for ($i = 1; $i < (count($sub_sorters) - 1); $i++) {
							$max = $yearToDayData[$sub_sorters[$i]]['prevYSumm'];
							$maxnum = $i;
							for ($j = ($i + 1); $j < count($sub_sorters); $j++) {
								if ($max < $yearToDayData[$sub_sorters[$j]]['prevYSumm']) {
									$max = $yearToDayData[$sub_sorters[$j]]['prevYSumm'];
									$maxnum = $j;
								}
							}
							if ($maxnum > $i) {
								$tmp = $sub_sorters[$i];
								$sub_sorters[$i] = $sub_sorters[$maxnum];
								$sub_sorters[$maxnum] = $tmp;
							}
						}
						break;
					}
					case 3: {
						for ($i = 1; $i < (count($sub_sorters) - 1); $i++) {
							$max = $yearToDayData[$sub_sorters[$i]]['currYSumm'];
							$maxnum = $i;
							for ($j = ($i + 1); $j < count($sub_sorters); $j++) {
								if ($max < $yearToDayData[$sub_sorters[$j]]['currYSumm']) {
									$max = $yearToDayData[$sub_sorters[$j]]['currYSumm'];
									$maxnum = $j;
								}
							}
							if ($maxnum > $i) {
								$tmp = $sub_sorters[$i];
								$sub_sorters[$i] = $sub_sorters[$maxnum];
								$sub_sorters[$maxnum] = $tmp;
							}
						}
						break;
					}
					case 4: {
						for ($i = 1; $i < (count($sub_sorters) - 1); $i++) {
							$max = $yearToDayData[$sub_sorters[$i]]['currYSumm'] + $yearToDayData[$sub_sorters[$i]]['prevYSumm'] + $yearToDayData[$sub_sorters[$i]]['lastYSumm'];
							$maxnum = $i;
							for ($j = ($i + 1); $j < count($sub_sorters); $j++) {
								if ($max < ($yearToDayData[$sub_sorters[$j]]['currYSumm'] + $yearToDayData[$sub_sorters[$j]]['prevYSumm'] + $yearToDayData[$sub_sorters[$j]]['lastYSumm'])) {
									$max = $yearToDayData[$sub_sorters[$j]]['currYSumm'] + $yearToDayData[$sub_sorters[$j]]['prevYSumm'] + $yearToDayData[$sub_sorters[$j]]['lastYSumm'];
									$maxnum = $j;
								}
							}
							if ($maxnum > $i) {
								$tmp = $sub_sorters[$i];
								$sub_sorters[$i] = $sub_sorters[$maxnum];
								$sub_sorters[$maxnum] = $tmp;
							}
						}
						break;
					}
				}

				$legends = [];
				$sub_names = [];
				foreach ($sub_sorters as $ss_key) {
					$sub_names[] = $sub_sorters_name[$ss_key];
					if ($params['gT'] === '2') {//$params['gT'] === '2'	??? было iI
						$legends['up'][] = $yearToDayData[$ss_key]['lastYSummPT3'];
						$legends['up'][] = $yearToDayData[$ss_key]['prevYSummPT3'];
						$legends['up'][] = $yearToDayData[$ss_key]['currYSummPT3'];
						$legends['down'][] = (($yearToDayData[$ss_key]['oldYSummPT3'] != 0) ? (round((($yearToDayData[$ss_key]['lastYSummPT3'] - $yearToDayData[$ss_key]['oldYSummPT3']) / $yearToDayData[$ss_key]['oldYSummPT3']) * 1000) / 10) : 0.0);
						$legends['down'][] = (($yearToDayData[$ss_key]['lastYSummPT3'] != 0) ? (round((($yearToDayData[$ss_key]['prevYSummPT3'] - $yearToDayData[$ss_key]['lastYSummPT3']) / $yearToDayData[$ss_key]['lastYSummPT3']) * 1000) / 10) : 0.0);
						$legends['down'][] = (($yearToDayData[$ss_key]['prevYSummPT3'] != 0) ? (round((($yearToDayData[$ss_key]['currYSummPT3'] - $yearToDayData[$ss_key]['prevYSummPT3']) / $yearToDayData[$ss_key]['prevYSummPT3']) * 1000) / 10) : 0.0);
						$legends['color'][] = (($yearToDayData[$ss_key]['lastYSummPT3'] < $yearToDayData[$ss_key]['oldYSummPT3']) ? 'Red' : 'Green');
						$legends['color'][] = (($yearToDayData[$ss_key]['prevYSummPT3'] < $yearToDayData[$ss_key]['lastYSummPT3']) ? 'Red' : 'Green');
						$legends['color'][] = (($yearToDayData[$ss_key]['currYSummPT3'] < $yearToDayData[$ss_key]['prevYSummPT3']) ? 'Red' : 'Green');
					} else {
						$legends['up'][] = $yearToDayData[$ss_key]['lastYSumm'];
						$legends['up'][] = $yearToDayData[$ss_key]['prevYSumm'];
						$legends['up'][] = $yearToDayData[$ss_key]['currYSumm'];
						$legends['down'][] = (($yearToDayData[$ss_key]['oldYSumm'] != 0) ? (round((($yearToDayData[$ss_key]['lastYSumm'] - $yearToDayData[$ss_key]['oldYSumm']) / $yearToDayData[$ss_key]['oldYSumm']) * 1000) / 10) : 0.0);
						$legends['down'][] = (($yearToDayData[$ss_key]['lastYSumm'] != 0) ? (round((($yearToDayData[$ss_key]['prevYSumm'] - $yearToDayData[$ss_key]['lastYSumm']) / $yearToDayData[$ss_key]['lastYSumm']) * 1000) / 10) : 0.0);
						$legends['down'][] = (($yearToDayData[$ss_key]['prevYSumm'] != 0) ? (round((($yearToDayData[$ss_key]['currYSumm'] - $yearToDayData[$ss_key]['prevYSumm']) / $yearToDayData[$ss_key]['prevYSumm']) * 1000) / 10) : 0.0);
						$legends['color'][] = (($yearToDayData[$ss_key]['lastYSumm'] < $yearToDayData[$ss_key]['oldYSumm']) ? 'Red' : 'Green');
						$legends['color'][] = (($yearToDayData[$ss_key]['prevYSumm'] < $yearToDayData[$ss_key]['lastYSumm']) ? 'Red' : 'Green');
						$legends['color'][] = (($yearToDayData[$ss_key]['currYSumm'] < $yearToDayData[$ss_key]['prevYSumm']) ? 'Red' : 'Green');
					}
					if (!($ss_key)) {
						$legends['up'][] = '';
						$legends['down'][] = '';
						$legends['color'][] = 'Black';
					}
				}
				#	Формирование итогового ответа
				$results = [];
				$names = [];
				foreach ($sorters as $key) {
					$sub_res = [];
					foreach ($sub_sorters as $ss_key) {
						$sub_res[] = '{"idv":"'.$yearToDayData[$ss_key]['lastY'][$sorters_name[$key]].'","idppt3":"'.$yearToDayData[$ss_key]['lastYPerc'][$sorters_name[$key]].'","lbl":"'.$sub_sorters_name[$ss_key].'"}';
						$sub_res[] = '{"idv":"'.$yearToDayData[$ss_key]['prevY'][$sorters_name[$key]].'","idppt3":"'.$yearToDayData[$ss_key]['prevYPerc'][$sorters_name[$key]].'","lbl":"'.$sub_sorters_name[$ss_key].'"}';
						$sub_res[] = '{"idv":"'.$yearToDayData[$ss_key]['currY'][$sorters_name[$key]].'","idppt3":"'.$yearToDayData[$ss_key]['currYPerc'][$sorters_name[$key]].'","lbl":"'.$sub_sorters_name[$ss_key].'"}';
						if (!($ss_key)) {
							$sub_res[] =  '{"idv":"","idppt3":"","lbl":""}';
						}
					}
					$result = '{"iD":['.implode(',',$sub_res).']}';
					$results[] = $result;
					$names[] = $sorters_name[$key];
				}

				echo '{"result":1,"slide_legend":"'.$slide_legend.'","data":{"ss":['.implode(',',$sorters).'],"sBs":['.implode(',',$sub_sorters).'],"sNs":["'.implode('","',$names).'"],"sBNs":["'.implode('","',$sub_names).'"],"rs":['.implode(',',$results).'],"lUs":["'.implode('","',$legends['up']).'"],"lDs":["'.implode('","',$legends['down']).'"],"lCs":["'.implode('","',$legends['color']).'"],"lYs":["'.implode('","',$yearToDayLabels).'"]}}';
			}
			else if ($page == 12) {
				$filters_queries = [];

				foreach ($params['fD'] as $item) {
					$filters_queries[] = " column_number_".$item['cI']." IN (".implode(', ',$item['vI']).")";
				}
				if ($filters_queries) {
					$filters_query = implode(" AND", $filters_queries);
					$main_filter = $filters_queries[0];
				}
					else {
						$filters_query = " 1";
						$main_filter = " 1";
					}

				$date_pieces = explode('-', $_POST['edate']);
				$year = $date_pieces[0];
				$month = $date_pieces[1];
				$day = $date_pieces[2];
				$date_pieces = explode('-', $_POST['sdate']);
				$syear = $date_pieces[0];
				$smonth = $date_pieces[1];
				$sday = $date_pieces[2];

				$sorters = [];
				$sorters_summ = [];
				$sorters_name = [];
				$other_summ = 0;
				if ($params['sI']) {
					#	Определить лидеров по "сортеру"
					$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item, value AS item_name, (SELECT value FROM db_".$dbase."_xls_for_diagrams_columns WHERE id = ".$params['sI'].") AS sorter_name FROM db_".$dbase."_xls_for_diagrams_table JOIN db_".$dbase."_xls_for_diagrams_unique_values ON (db_".$dbase."_xls_for_diagrams_unique_values.column_id = ".$params['sI']." AND db_".$dbase."_xls_for_diagrams_unique_values.id = db_".$dbase."_xls_for_diagrams_table.column_number_".$params['sI'].") WHERE".$filters_query." GROUP BY column_number_".$params['sI']." ORDER BY sorter_summ DESC";
					if ($result = $conn->query($sql)) {
						if ($params['ors']) {
							$sorters = $params['osC'];
							foreach ($sorters as $item) {
								$sorters_summ[$item] = 0;
							}
							while ($obj = $result->fetch_object()) {
								if (in_array($obj->sorter_item, $sorters)) {
									$sorters_summ[$obj->sorter_item] = $obj->sorter_summ;
								}
									else {
										$other_summ += $obj->sorter_summ;
									}
							}
						}
							else {
								$count = 0;
								while ($obj = $result->fetch_object()) {
									$count++;
									if ($count < $params['sCs']) {
										if (($params['sU']) && (( $obj->sorter_name == 'Корпорация') || ( $obj->sorter_name == 'Корпорация - map')) && ($obj->item_name == 'U.M.')) {
											$count--;
											$other_summ += $obj->sorter_summ;
										}
											else {
												$sorters[] = $obj->sorter_item;
												$sorters_summ[$obj->sorter_item] = $obj->sorter_summ;
											}
									}
										else {
											if ((( $obj->sorter_name == 'Корпорация') && ($obj->sorter_item == $mainCorp)) || (( $obj->sorter_name === 'Корпорация - map') && ($obj->sorter_item == $mainCorp))) {
												$other_summ += $sorters_summ[$sorters[$params['sCs'] - 2]];
												$sorters[$params['sCs'] - 2] = $obj->sorter_item;
												$sorters_summ[$obj->sorter_item] = $obj->sorter_summ;
											}
												else {
													$other_summ += $obj->sorter_summ;
												}
										}
								}
							}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
				}
					else {
						foreach ($params['dD'] as $key => $item) {
							$sorters[] = $key;
							$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
							$sorter_name = 'Ценовые диапазоны';
							if ($result = $conn->query($sql)) {
								while ($obj = $result->fetch_object()) {
									$sorters_summ[$key] = $obj->sorter_summ;
								}
							}
								else {
									die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
								}
							$sorters_name[$key] = $key;
						}
					}

				if (!(count($sorters))) {
					echo '{"result":0,"error":"Фильтры отсеяли все значения"}';
					break;
				}

				if ($params['sI']) {
					#	Определить название "сортера"
					$sql = "SELECT value FROM db_".$dbase."_xls_for_diagrams_columns WHERE id = ".$params['sI'];
					$sorter_name = '';
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							$sorter_name = $obj->value;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}

					#	Определить названия лидеров "сортера"
					$sql = "SELECT id, value FROM db_".$dbase."_xls_for_diagrams_unique_values WHERE column_id = ".$params['sI']." AND id IN (".implode(', ', $sorters).")";
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							if (((($sorter_name === 'Корпорация') && ($obj->id == $mainCorp)) || (($sorter_name === 'Корпорация - map') && ($obj->id == $mainCorp))) && (!($params['ors']))) {
								array_splice($sorters, array_search($obj->id, $sorters), 1);
								array_unshift($sorters, $obj->id);
							}
							$sorters_name[$obj->id] = $obj->value;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
				}

				#	Определить название второго "сортера"
				$sql = "SELECT value FROM db_".$dbase."_xls_for_diagrams_columns WHERE id = ".$params['sCD'];
				$sub_sorters_name = [];
				$sub_sorters = [];
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$sub_sorters_name[0] = 'Total';
						$sub_sorters[] = 0;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}

				#	Определить названия категорий второго "сортера"
				
				#	тут добавить фильтр полученных ИТОГОВЫХ айди по выбранному списку - остальное сработает автоматически (!), по поводу упорядочивания - подумать над этим, вероятнее в пост обработке
				
				
				$sql = "SELECT id, value FROM db_".$dbase."_xls_for_diagrams_unique_values WHERE id IN (SELECT DISTINCT column_number_".$params['sCD']." FROM `db_".$dbase."_xls_for_diagrams_table` WHERE".$filters_query.") AND id IN (".implode(',',$params['ssC']).")";
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$sub_sorters_name[$obj->id] = $obj->value;
						#$sub_sorters[] = $obj->id;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				foreach ($params['ssC'] as $ss_id) {
					$sub_sorters[] = $ss_id;
				}

				$months = ['', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

				#	Определить данные для графиков YTD
				$yearToDayData = [];
				$ytdInd = 0;
				foreach ($sub_sorters as $ss_key) {
					$yearToDayData[$ss_key]['currY'] = ['Other' => 0];
					$yearToDayData[$ss_key]['prevY'] = ['Other' => 0];
					$yearToDayData[$ss_key]['lastY'] = ['Other' => 0];
					$yearToDayData[$ss_key]['oldY'] = ['Other' => 0];
					$yearToDayData[$ss_key]['currYSumm'] = 0;
					$yearToDayData[$ss_key]['prevYSumm'] = 0;
					$yearToDayData[$ss_key]['lastYSumm'] = 0;
					$yearToDayData[$ss_key]['oldYSumm'] = 0;
					$yearToDayData[$ss_key]['currYSummPT3'] = 0;
					$yearToDayData[$ss_key]['prevYSummPT3'] = 0;
					$yearToDayData[$ss_key]['lastYSummPT3'] = 0;
					$yearToDayData[$ss_key]['oldYSummPT3'] = 0;
					if ($ss_key) {
						$sorter_filter = " AND column_number_".$params['sCD']." = ".$ss_key;
						$ytdInd++;
						$yearToDayLabels[] = $ytdInd;
						$ytdInd++;
						$yearToDayLabels[] = $ytdInd;
						$ytdInd++;
						$yearToDayLabels[] = $ytdInd;
					} else {
						$sorter_filter = " AND 1";
						$yearToDayLabels = [
							$months[(int)$smonth].substr(($syear - 2), -2)."-".$months[(int)$month]."".substr(($year - 2), -2),
							$months[(int)$smonth].substr(($syear - 1), -2)."-".$months[(int)$month]."".substr(($year - 1), -2),
							$months[(int)$smonth].substr($syear, -2)."-".$months[(int)$month]."".substr($year, -2),
							#"YTD".substr(($year - 1), -2)." ".$months[$month],
							#"YTD".substr($year, -2)." ".$months[$month],
							''
						];
					}
					$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($syear-0), $smonth, $sday)." AND ".sprintf("%04d%02d%02d", ($year-0), $month, $day);
					#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							$yearToDayData[$ss_key]['currYSummPT3'] = $obj->sorter_summ;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
					$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($syear-1), $smonth, $sday)." AND ".sprintf("%04d%02d%02d", ($year-1), $month, $day);
					#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							$yearToDayData[$ss_key]['prevYSummPT3'] = $obj->sorter_summ;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
					$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($syear-2), $smonth, $sday)." AND ".sprintf("%04d%02d%02d", ($year-2), $month, $day);
					#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							$yearToDayData[$ss_key]['lastYSummPT3'] = $obj->sorter_summ;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
					$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($syear-3), $smonth, $sday)." AND ".sprintf("%04d%02d%02d", ($year-3), $month, $day);
					#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							$yearToDayData[$ss_key]['oldYSummPT3'] = $obj->sorter_summ;	#	lastYSummPT3	???
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
					if ($params['sI']) {
						$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($syear-0), $smonth, $sday)." AND ".sprintf("%04d%02d%02d", ($year-0), $month, $day)." GROUP BY column_number_".$params['sI'];
						#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								if (array_key_exists($obj->sorter_item, $sorters_name)) {
									$yearToDayData[$ss_key]['currY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
									$yearToDayData[$ss_key]['currYSumm'] += $obj->sorter_summ;
								}
									else {
										$yearToDayData[$ss_key]['currY']['Other'] += $obj->sorter_summ;
										if ($params['oos']) {
											$yearToDayData[$ss_key]['currYSumm'] += $obj->sorter_summ;
										}
									}
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
						$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($syear-1), $smonth, $sday)." AND ".sprintf("%04d%02d%02d", ($year-1), $month, $day)." GROUP BY column_number_".$params['sI'];
						#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								if (array_key_exists($obj->sorter_item, $sorters_name)) {
									$yearToDayData[$ss_key]['prevY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
									$yearToDayData[$ss_key]['prevYSumm'] += $obj->sorter_summ;
								}
									else {
										$yearToDayData[$ss_key]['prevY']['Other'] += $obj->sorter_summ;
										if ($params['oos']) {
											$yearToDayData[$ss_key]['prevYSumm'] += $obj->sorter_summ;
										}
									}
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
						$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($syear-2), $smonth, $sday)." AND ".sprintf("%04d%02d%02d", ($year-2), $month, $day)." GROUP BY column_number_".$params['sI'];
						#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								if (array_key_exists($obj->sorter_item, $sorters_name)) {
									$yearToDayData[$ss_key]['lastY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
									$yearToDayData[$ss_key]['lastYSumm'] += $obj->sorter_summ;
								}
									else {
										$yearToDayData[$ss_key]['lastY']['Other'] += $obj->sorter_summ;
										if ($params['oos']) {
											$yearToDayData[$ss_key]['lastYSumm'] += $obj->sorter_summ;
										}
									}
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
						$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($syear-3), $smonth, $sday)." AND ".sprintf("%04d%02d%02d", ($year-3), $month, $day)." GROUP BY column_number_".$params['sI'];
						#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								if (array_key_exists($obj->sorter_item, $sorters_name)) {
									$yearToDayData[$ss_key]['oldY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
									$yearToDayData[$ss_key]['oldYSumm'] += $obj->sorter_summ;
								}
									else {
										$yearToDayData[$ss_key]['oldY']['Other'] += $obj->sorter_summ;
										if ($params['oos']) {
											$yearToDayData[$ss_key]['oldYSumm'] += $obj->sorter_summ;
										}
									}
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
					}
						else {
							foreach ($params['dD'] as $key => $item) {
								$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($syear-0), $smonth, $sday)." AND ".sprintf("%04d%02d%02d", ($year-0), $month, $day)." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
								#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
								if ($result = $conn->query($sql)) {
									while ($obj = $result->fetch_object()) {
										$yearToDayData[$ss_key]['currY'][$key] = $obj->sorter_summ;
										$yearToDayData[$ss_key]['currYSumm'] += $obj->sorter_summ;
									}
								}
									else {
										die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
									}
								$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($syear-1), $smonth, $sday)." AND ".sprintf("%04d%02d%02d", ($year-1), $month, $day)." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
								#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
								if ($result = $conn->query($sql)) {
									while ($obj = $result->fetch_object()) {
										$yearToDayData[$ss_key]['prevY'][$key] = $obj->sorter_summ;
										$yearToDayData[$ss_key]['prevYSumm'] += $obj->sorter_summ;
									}
								}
									else {
										die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
									}
								$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($syear-2), $smonth, $sday)." AND ".sprintf("%04d%02d%02d", ($year-2), $month, $day)." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
								#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
								if ($result = $conn->query($sql)) {
									while ($obj = $result->fetch_object()) {
										$yearToDayData[$ss_key]['lastY'][$key] = $obj->sorter_summ;
										$yearToDayData[$ss_key]['lastYSumm'] += $obj->sorter_summ;
									}
								}
									else {
										die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
									}
								$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($syear-3), $smonth, $sday)." AND ".sprintf("%04d%02d%02d", ($year-3), $month, $day)." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
								#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
								if ($result = $conn->query($sql)) {
									while ($obj = $result->fetch_object()) {
										$yearToDayData[$ss_key]['oldY'][$key] = $obj->sorter_summ;
										$yearToDayData[$ss_key]['oldYSumm'] += $obj->sorter_summ;
									}
								}
									else {
										die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
									}
							}
						}
					if (($other_summ > 0) && ($sorters_name[0] != 'Other') && ($params['oos'])) {
						$sorters[] = 0;
						$sorters_name[0] = 'Other';
					}
					#	Подсчет процентного выражения и приведение величин к масштабу миллиардов
					foreach ($sorters_name as $key) {
						#	Проценты
						$yearToDayData[$ss_key]['currYPerc'][$key] = (($yearToDayData[$ss_key]['currYSumm'] != 0) ? (round($yearToDayData[$ss_key]['currY'][$key] / $yearToDayData[$ss_key]['currYSumm'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['prevYPerc'][$key] = (($yearToDayData[$ss_key]['prevYSumm'] != 0) ? (round($yearToDayData[$ss_key]['prevY'][$key] / $yearToDayData[$ss_key]['prevYSumm'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['lastYPerc'][$key] = (($yearToDayData[$ss_key]['lastYSumm'] != 0) ? (round($yearToDayData[$ss_key]['lastY'][$key] / $yearToDayData[$ss_key]['lastYSumm'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['oldYPerc'][$key] = (($yearToDayData[$ss_key]['oldYSumm'] != 0) ? (round($yearToDayData[$ss_key]['oldY'][$key] / $yearToDayData[$ss_key]['oldYSumm'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['currYPercPT3'][$key] = (($yearToDayData[$ss_key]['currYSummPT3'] != 0) ? (round($yearToDayData[$ss_key]['currY'][$key] / $yearToDayData[$ss_key]['currYSummPT3'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['prevYPercPT3'][$key] = (($yearToDayData[$ss_key]['prevYSummPT3'] != 0) ? (round($yearToDayData[$ss_key]['prevY'][$key] / $yearToDayData[$ss_key]['prevYSummPT3'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['lastYPercPT3'][$key] = (($yearToDayData[$ss_key]['lastYSummPT3'] != 0) ? (round($yearToDayData[$ss_key]['lastY'][$key] / $yearToDayData[$ss_key]['lastYSummPT3'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['oldYPercPT3'][$key] = (($yearToDayData[$ss_key]['oldYSummPT3'] != 0) ? (round($yearToDayData[$ss_key]['oldY'][$key] / $yearToDayData[$ss_key]['oldYSummPT3'] * 1000) / 10) : 0.0);
						if (!($params['sP'])) {
							#	Масштабирование
							$yearToDayData[$ss_key]['currY'][$key] = round($yearToDayData[$ss_key]['currY'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['prevY'][$key] = round($yearToDayData[$ss_key]['prevY'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['lastY'][$key] = round($yearToDayData[$ss_key]['lastY'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['oldY'][$key] = round($yearToDayData[$ss_key]['oldY'][$key] / $msscht2) / 10000;
						} else if ($params['sP'] == 1) {
							#	Масштабирование
							$yearToDayData[$ss_key]['currY'][$key] = round($yearToDayData[$ss_key]['currY'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['prevY'][$key] = round($yearToDayData[$ss_key]['prevY'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['lastY'][$key] = round($yearToDayData[$ss_key]['lastY'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['oldY'][$key] = round($yearToDayData[$ss_key]['oldY'][$key] / $msscht2) / 10000;
						}
					}
					#	Масштабирование суммарных данных
					/*if (!($params['sP'])) {
						$yearToDayData[$ss_key]['currYSumm'] = round($yearToDayData[$ss_key]['currYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['prevYSumm'] = round($yearToDayData[$ss_key]['prevYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['lastYSumm'] = round($yearToDayData[$ss_key]['lastYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['oldYSumm'] = round($yearToDayData[$ss_key]['oldYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['currYSummPT3'] = round($yearToDayData[$ss_key]['currYSummPT3'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['prevYSummPT3'] = round($yearToDayData[$ss_key]['prevYSummPT3'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['lastYSummPT3'] = round($yearToDayData[$ss_key]['lastYSummPT3'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['oldYSummPT3'] = round($yearToDayData[$ss_key]['oldYSummPT3'] / $msscht1) / 10;
					} else if ($params['sP'] == 1) {
						$yearToDayData[$ss_key]['currYSumm'] = round($yearToDayData[$ss_key]['currYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['prevYSumm'] = round($yearToDayData[$ss_key]['prevYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['lastYSumm'] = round($yearToDayData[$ss_key]['lastYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['oldYSumm'] = round($yearToDayData[$ss_key]['oldYSumm'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['currYSummPT3'] = round($yearToDayData[$ss_key]['currYSummPT3'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['prevYSummPT3'] = round($yearToDayData[$ss_key]['prevYSummPT3'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['lastYSummPT3'] = round($yearToDayData[$ss_key]['lastYSummPT3'] / $msscht1) / 10;
						$yearToDayData[$ss_key]['oldYSummPT3'] = round($yearToDayData[$ss_key]['oldYSummPT3'] / $msscht1) / 10;
					}*/
					#	Определение величин приращения (для легенды)/ формирование данных легенды
				}

				switch ($params['sos']) {
					case 0: {
						break;
					}
					case 1: {
						for ($i = 1; $i < (count($sub_sorters) - 1); $i++) {
							$max = $yearToDayData[$sub_sorters[$i]]['lastYSumm'];
							$maxnum = $i;
							for ($j = ($i + 1); $j < count($sub_sorters); $j++) {
								if ($max < $yearToDayData[$sub_sorters[$j]]['lastYSumm']) {
									$max = $yearToDayData[$sub_sorters[$j]]['lastYSumm'];
									$maxnum = $j;
								}
							}
							if ($maxnum > $i) {
								$tmp = $sub_sorters[$i];
								$sub_sorters[$i] = $sub_sorters[$maxnum];
								$sub_sorters[$maxnum] = $tmp;
							}
						}
						break;
					}
					case 2: {
						for ($i = 1; $i < (count($sub_sorters) - 1); $i++) {
							$max = $yearToDayData[$sub_sorters[$i]]['prevYSumm'];
							$maxnum = $i;
							for ($j = ($i + 1); $j < count($sub_sorters); $j++) {
								if ($max < $yearToDayData[$sub_sorters[$j]]['prevYSumm']) {
									$max = $yearToDayData[$sub_sorters[$j]]['prevYSumm'];
									$maxnum = $j;
								}
							}
							if ($maxnum > $i) {
								$tmp = $sub_sorters[$i];
								$sub_sorters[$i] = $sub_sorters[$maxnum];
								$sub_sorters[$maxnum] = $tmp;
							}
						}
						break;
					}
					case 3: {
						for ($i = 1; $i < (count($sub_sorters) - 1); $i++) {
							$max = $yearToDayData[$sub_sorters[$i]]['currYSumm'];
							$maxnum = $i;
							for ($j = ($i + 1); $j < count($sub_sorters); $j++) {
								if ($max < $yearToDayData[$sub_sorters[$j]]['currYSumm']) {
									$max = $yearToDayData[$sub_sorters[$j]]['currYSumm'];
									$maxnum = $j;
								}
							}
							if ($maxnum > $i) {
								$tmp = $sub_sorters[$i];
								$sub_sorters[$i] = $sub_sorters[$maxnum];
								$sub_sorters[$maxnum] = $tmp;
							}
						}
						break;
					}
					case 4: {
						for ($i = 1; $i < (count($sub_sorters) - 1); $i++) {
							$max = $yearToDayData[$sub_sorters[$i]]['currYSumm'] + $yearToDayData[$sub_sorters[$i]]['prevYSumm'] + $yearToDayData[$sub_sorters[$i]]['lastYSumm'];
							$maxnum = $i;
							for ($j = ($i + 1); $j < count($sub_sorters); $j++) {
								if ($max < ($yearToDayData[$sub_sorters[$j]]['currYSumm'] + $yearToDayData[$sub_sorters[$j]]['prevYSumm'] + $yearToDayData[$sub_sorters[$j]]['lastYSumm'])) {
									$max = $yearToDayData[$sub_sorters[$j]]['currYSumm'] + $yearToDayData[$sub_sorters[$j]]['prevYSumm'] + $yearToDayData[$sub_sorters[$j]]['lastYSumm'];
									$maxnum = $j;
								}
							}
							if ($maxnum > $i) {
								$tmp = $sub_sorters[$i];
								$sub_sorters[$i] = $sub_sorters[$maxnum];
								$sub_sorters[$maxnum] = $tmp;
							}
						}
						break;
					}
				}

				$legends = [];
				$sub_names = [];
				foreach ($sub_sorters as $ss_key) {
					$sub_names[] = $sub_sorters_name[$ss_key];
					if ($params['gT'] === '2') {//$params['gT'] === '2'	??? было iI
						$legends['up'][] = $yearToDayData[$ss_key]['lastYSummPT3'];
						$legends['up'][] = $yearToDayData[$ss_key]['prevYSummPT3'];
						$legends['up'][] = $yearToDayData[$ss_key]['currYSummPT3'];
						$legends['down'][] = (($yearToDayData[$ss_key]['oldYSummPT3'] != 0) ? (round((($yearToDayData[$ss_key]['lastYSummPT3'] - $yearToDayData[$ss_key]['oldYSummPT3']) / $yearToDayData[$ss_key]['oldYSummPT3']) * 1000) / 10) : 0.0);
						$legends['down'][] = (($yearToDayData[$ss_key]['lastYSummPT3'] != 0) ? (round((($yearToDayData[$ss_key]['prevYSummPT3'] - $yearToDayData[$ss_key]['lastYSummPT3']) / $yearToDayData[$ss_key]['lastYSummPT3']) * 1000) / 10) : 0.0);
						$legends['down'][] = (($yearToDayData[$ss_key]['prevYSummPT3'] != 0) ? (round((($yearToDayData[$ss_key]['currYSummPT3'] - $yearToDayData[$ss_key]['prevYSummPT3']) / $yearToDayData[$ss_key]['prevYSummPT3']) * 1000) / 10) : 0.0);
						$legends['color'][] = (($yearToDayData[$ss_key]['lastYSummPT3'] < $yearToDayData[$ss_key]['oldYSummPT3']) ? 'Red' : 'Green');
						$legends['color'][] = (($yearToDayData[$ss_key]['prevYSummPT3'] < $yearToDayData[$ss_key]['lastYSummPT3']) ? 'Red' : 'Green');
						$legends['color'][] = (($yearToDayData[$ss_key]['currYSummPT3'] < $yearToDayData[$ss_key]['prevYSummPT3']) ? 'Red' : 'Green');
					} else {
						$legends['up'][] = $yearToDayData[$ss_key]['lastYSumm'];
						$legends['up'][] = $yearToDayData[$ss_key]['prevYSumm'];
						$legends['up'][] = $yearToDayData[$ss_key]['currYSumm'];
						$legends['down'][] = (($yearToDayData[$ss_key]['oldYSumm'] != 0) ? (round((($yearToDayData[$ss_key]['lastYSumm'] - $yearToDayData[$ss_key]['oldYSumm']) / $yearToDayData[$ss_key]['oldYSumm']) * 1000) / 10) : 0.0);
						$legends['down'][] = (($yearToDayData[$ss_key]['lastYSumm'] != 0) ? (round((($yearToDayData[$ss_key]['prevYSumm'] - $yearToDayData[$ss_key]['lastYSumm']) / $yearToDayData[$ss_key]['lastYSumm']) * 1000) / 10) : 0.0);
						$legends['down'][] = (($yearToDayData[$ss_key]['prevYSumm'] != 0) ? (round((($yearToDayData[$ss_key]['currYSumm'] - $yearToDayData[$ss_key]['prevYSumm']) / $yearToDayData[$ss_key]['prevYSumm']) * 1000) / 10) : 0.0);
						$legends['color'][] = (($yearToDayData[$ss_key]['lastYSumm'] < $yearToDayData[$ss_key]['oldYSumm']) ? 'Red' : 'Green');
						$legends['color'][] = (($yearToDayData[$ss_key]['prevYSumm'] < $yearToDayData[$ss_key]['lastYSumm']) ? 'Red' : 'Green');
						$legends['color'][] = (($yearToDayData[$ss_key]['currYSumm'] < $yearToDayData[$ss_key]['prevYSumm']) ? 'Red' : 'Green');
					}
					if (!($ss_key)) {
						$legends['up'][] = '';
						$legends['down'][] = '';
						$legends['color'][] = 'Black';
					}
				}
				#	Формирование итогового ответа
				$results = [];
				$names = [];
				foreach ($sorters as $key) {
					$sub_res = [];
					foreach ($sub_sorters as $ss_key) {
						$sub_res[] = '{"idv":"'.$yearToDayData[$ss_key]['lastY'][$sorters_name[$key]].'","idppt3":"'.$yearToDayData[$ss_key]['lastYPerc'][$sorters_name[$key]].'","lbl":"'.$sub_sorters_name[$ss_key].'"}';
						$sub_res[] = '{"idv":"'.$yearToDayData[$ss_key]['prevY'][$sorters_name[$key]].'","idppt3":"'.$yearToDayData[$ss_key]['prevYPerc'][$sorters_name[$key]].'","lbl":"'.$sub_sorters_name[$ss_key].'"}';
						$sub_res[] = '{"idv":"'.$yearToDayData[$ss_key]['currY'][$sorters_name[$key]].'","idppt3":"'.$yearToDayData[$ss_key]['currYPerc'][$sorters_name[$key]].'","lbl":"'.$sub_sorters_name[$ss_key].'"}';
						if (!($ss_key)) {
							$sub_res[] =  '{"idv":"","idppt3":"","lbl":""}';
						}
					}
					$result = '{"iD":['.implode(',',$sub_res).']}';
					$results[] = $result;
					$names[] = $sorters_name[$key];
				}

				echo '{"result":1,"slide_legend":"'.$slide_legend.'","data":{"ss":['.implode(',',$sorters).'],"sBs":['.implode(',',$sub_sorters).'],"sNs":["'.implode('","',$names).'"],"sBNs":["'.implode('","',$sub_names).'"],"rs":['.implode(',',$results).'],"lUs":["'.implode('","',$legends['up']).'"],"lDs":["'.implode('","',$legends['down']).'"],"lCs":["'.implode('","',$legends['color']).'"],"lYs":["'.implode('","',$yearToDayLabels).'"]}}';
			}
			else if ($page == 13) {
				$filters_queries = [];

				foreach ($params['fD'] as $item) {
					$filters_queries[] = " column_number_".$item['cI']." IN (".implode(', ',$item['vI']).")";
				}
				if ($filters_queries) {
					$filters_query = implode(" AND", $filters_queries);
					$main_filter = $filters_queries[0];
				}
					else {
						$filters_query = " 1";
						$main_filter = " 1";
					}

				foreach ($params['ssC'] as $ss_id) {
					$sub_sorters[] = $ss_id;
				}
				$sorter_filter = " AND column_number_".$params['sCD']." IN (".implode(', ',$sub_sorters).")";

				$date_pieces = explode('-', $_POST['edate']);
				$year = $date_pieces[0];
				$month = $date_pieces[1];
				$day = $date_pieces[2];

				#	Определить лидеров по "сортеру" с учетом "второго сортера"
				$sql = "SELECT IF(SUM(count) > 0, (SUM(".$summing.")/SUM(count)), 0) AS sorter_summ, column_number_".$params['sI']." AS sorter_item, value AS item_name, (SELECT value FROM db_".$dbase."_xls_for_diagrams_columns WHERE id = ".$params['sI'].") AS sorter_name FROM db_".$dbase."_xls_for_diagrams_table JOIN db_".$dbase."_xls_for_diagrams_unique_values ON (db_".$dbase."_xls_for_diagrams_unique_values.column_id = ".$params['sI']." AND db_".$dbase."_xls_for_diagrams_unique_values.id = db_".$dbase."_xls_for_diagrams_table.column_number_".$params['sI'].") WHERE".$filters_query.$sorter_filter." GROUP BY column_number_".$params['sI']." ORDER BY sorter_summ DESC";
				$sorters = [];
				$sorters_summ = [];
				if ($result = $conn->query($sql)) {
					if ($params['ors']) {
						$sorters = $params['osC'];
						foreach ($sorters as $item) {
							$sorters_summ[$item] = 0;
						}
						while ($obj = $result->fetch_object()) {
							if (in_array($obj->sorter_item, $sorters)) {
								$sorters_summ[$obj->sorter_item] = $obj->sorter_summ;
							}
						}
					}
						else {
							$count = 0;
							while ($obj = $result->fetch_object()) {
								$count++;
								$sorters[] = $obj->sorter_item;
								$sorters_summ[$obj->sorter_item] = $obj->sorter_summ;
							}
						}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}

				/*
				#	Определить лидеров по "сортеру"
				$sql = "SELECT IF(SUM(count) > 0, (SUM(".$summing.")/SUM(count)), 0) AS sorter_summ, column_number_".$params['sI']." AS sorter_item, value AS item_name, (SELECT value FROM db_".$dbase."_xls_for_diagrams_columns WHERE id = ".$params['sI'].") AS sorter_name FROM db_".$dbase."_xls_for_diagrams_table JOIN db_".$dbase."_xls_for_diagrams_unique_values ON (db_".$dbase."_xls_for_diagrams_unique_values.column_id = ".$params['sI']." AND db_".$dbase."_xls_for_diagrams_unique_values.id = db_".$dbase."_xls_for_diagrams_table.column_number_".$params['sI'].") WHERE".$filters_query." GROUP BY column_number_".$params['sI']." ORDER BY sorter_summ DESC";
				$sorters = [];
				$sorters_summ = [];
				if ($result = $conn->query($sql)) {
					$count = 0;
					$other_summ = 0;
					while ($obj = $result->fetch_object()) {
						$count++;
						if ($count <= $params['sCs']) {
							if (($params['sU']) && (( $obj->sorter_name === 'Корпорация') || ( $obj->sorter_name === 'Корпорация - map')) && ($obj->item_name === 'U.M.')) {
								$count--;
								$other_summ += $obj->sorter_summ;
							}
								else {
									$sorters[] = $obj->sorter_item;
									$sorters_summ[$obj->sorter_item] = $obj->sorter_summ;
								}
						}
							else {
								if ((( $obj->sorter_name === 'Корпорация') && ($obj->sorter_item == $mainCorp)) || (( $obj->sorter_name === 'Корпорация - map') && ($obj->sorter_item == $mainCorp))) {
									$other_summ += $sorters_summ[$sorters[$params['sCs'] - 1]];
									$sorters[$params['sCs'] - 1] = $obj->sorter_item;
									$sorters_summ[$obj->sorter_item] = $obj->sorter_summ;
								}
									else {
										$other_summ += $obj->sorter_summ;
									}
							}
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				*/

				if (!(count($sorters))) {
					echo '{"result":0,"error":"Фильтры отсеяли все значения"}';
					break;
				}

				#	Определить название "сортера"
				$sql = "SELECT value FROM db_".$dbase."_xls_for_diagrams_columns WHERE id = ".$params['sI'];
				$sorter_name = '';
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$sorter_name = $obj->value;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}

				#	Определить названия лидеров "сортера"
				$sql = "SELECT id, value FROM db_".$dbase."_xls_for_diagrams_unique_values WHERE column_id = ".$params['sI']." AND id IN (".implode(', ', $sorters).")";
				$sorters_name = [];
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						if (((($sorter_name === 'Корпорация') && ($obj->id == $mainCorp)) || (($sorter_name === 'Корпорация - map') && ($obj->id == $mainCorp))) && (!($params['ors']))) {
							array_splice($sorters, array_search($obj->id, $sorters), 1);
							array_unshift($sorters, $obj->id);
						}
						$sorters_name[$obj->id] = $obj->value;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}

				$months = ['', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

				#	Определить данные для графиков MTM
				$monthToMonthData = [];
				$monthToMonthLabels = [
					((($month + 1) > 12) ? "Jan".substr(($year - 1), -2) : $months[(int)$month + 1]."".substr(($year - 2), -2))."-".$months[(int)$month]."".substr(($year - 1), -2),
					((($month + 1) > 12) ? "Jan".substr($year, -2) : $months[(int)$month + 1]."".substr(($year - 1), -2))."-".$months[(int)$month]."".substr($year, -2)
				];
				$titlemtm0 = $monthToMonthLabels[0];
				$titlemtm1 = $monthToMonthLabels[1];
				$monthToMonthData['currY'] = ['Other' => 0];
				$monthToMonthData['prevY'] = ['Other' => 0];
				$monthToMonthData['lastY'] = ['Other' => 0];
				$monthToMonthData['currYSumm'] = 0;
				$monthToMonthData['prevYSumm'] = 0;
				$monthToMonthData['lastYSumm'] = 0;
				$monthToMonthData['currYSummPT3'] = 0;
				$monthToMonthData['prevYSummPT3'] = 0;
				$monthToMonthData['lastYSummPT3'] = 0;
				$sql = "SELECT IF(SUM(count) > 0, (SUM(".$summing.")/SUM(count)), 0) AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-0) : ($year-1)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-0), $month, "31");
				#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$monthToMonthData['currYSummPT3'] = $obj->sorter_summ;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				$sql = "SELECT IF(SUM(count) > 0, (SUM(".$summing.")/SUM(count)), 0) AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-0) : ($year-1)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-0), $month, "31")." GROUP BY column_number_".$params['sI'];
				#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						if (array_key_exists($obj->sorter_item, $sorters_name)) {
							$monthToMonthData['currY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
							$monthToMonthData['currYSumm'] += $obj->sorter_summ;
						}
							else {
								$monthToMonthData['currY']['Other'] += $obj->sorter_summ;
								if ($params['oos']) {
									$monthToMonthData['currYSumm'] += $obj->sorter_summ;
								}
							}
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				$sql = "SELECT IF(SUM(count) > 0, (SUM(".$summing.")/SUM(count)), 0) AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-1) : ($year-2)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-1), $month, "31");
				#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$monthToMonthData['prevYSummPT3'] = $obj->sorter_summ;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				$sql = "SELECT IF(SUM(count) > 0, (SUM(".$summing.")/SUM(count)), 0) AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-1) : ($year-2)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-1), $month, "31")." GROUP BY column_number_".$params['sI'];
				#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						if (array_key_exists($obj->sorter_item, $sorters_name)) {
							$monthToMonthData['prevY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
							$monthToMonthData['prevYSumm'] += $obj->sorter_summ;
						}
							else {
								$monthToMonthData['prevY']['Other'] += $obj->sorter_summ;
								if ($params['oos']) {
									$monthToMonthData['prevYSumm'] += $obj->sorter_summ;
								}
							}
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				$sql = "SELECT IF(SUM(count) > 0, (SUM(".$summing.")/SUM(count)), 0) AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-2) : ($year-3)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-2), $month, "31");
				#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$monthToMonthData['lastYSummPT3'] = $obj->sorter_summ;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				$sql = "SELECT IF(SUM(count) > 0, (SUM(".$summing.")/SUM(count)), 0) AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-2) : ($year-3)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-2), $month, "31")." GROUP BY column_number_".$params['sI'];
				#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						if (array_key_exists($obj->sorter_item, $sorters_name)) {
							$monthToMonthData['lastY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
							$monthToMonthData['lastYSumm'] += $obj->sorter_summ;
						}
							else {
								$monthToMonthData['lastY']['Other'] += $obj->sorter_summ;
								if ($params['oos']) {
									$monthToMonthData['lastYSumm'] += $obj->sorter_summ;
								}
							}
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}

				#	Определить данные для графиков YTD
				$yearToDayData = [];
				$yearToDayLabels = [
					"Jan".substr(($year - 1), -2)."-".$months[(int)$month]."".substr(($year - 1), -2),
					"Jan".substr($year, -2)."-".$months[(int)$month]."".substr($year, -2)
					#"YTD".substr(($year - 1), -2)." ".$months[$month],
					#"YTD".substr($year, -2)." ".$months[$month]
				];
				$titleytd0 = $yearToDayLabels[0];
				$titleytd1 = $yearToDayLabels[1];
				$yearToDayData['currY'] = ['Other' => 0];
				$yearToDayData['prevY'] = ['Other' => 0];
				$yearToDayData['lastY'] = ['Other' => 0];
				$yearToDayData['currYSumm'] = 0;
				$yearToDayData['prevYSumm'] = 0;
				$yearToDayData['lastYSumm'] = 0;
				$yearToDayData['currYSummPT3'] = 0;
				$yearToDayData['prevYSummPT3'] = 0;
				$yearToDayData['lastYSummPT3'] = 0;
				$sql = "SELECT IF(SUM(count) > 0, (SUM(".$summing.")/SUM(count)), 0) AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-0), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-0), $month, $day);
				#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$yearToDayData['currYSummPT3'] = $obj->sorter_summ;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				$sql = "SELECT IF(SUM(count) > 0, (SUM(".$summing.")/SUM(count)), 0) AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-0), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-0), $month, $day)." GROUP BY column_number_".$params['sI'];
				#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						if (array_key_exists($obj->sorter_item, $sorters_name)) {
							$yearToDayData['currY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
							$yearToDayData['currYSumm'] += $obj->sorter_summ;
						}
							else {
								$yearToDayData['currY']['Other'] += $obj->sorter_summ;
								if ($params['oos']) {
									$yearToDayData['currYSumm'] += $obj->sorter_summ;
								}
							}
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				$sql = "SELECT IF(SUM(count) > 0, (SUM(".$summing.")/SUM(count)), 0) AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-1), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-1), $month, $day);
				#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$yearToDayData['prevYSummPT3'] = $obj->sorter_summ;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				$sql = "SELECT IF(SUM(count) > 0, (SUM(".$summing.")/SUM(count)), 0) AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-1), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-1), $month, $day)." GROUP BY column_number_".$params['sI'];
				#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						if (array_key_exists($obj->sorter_item, $sorters_name)) {
							$yearToDayData['prevY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
							$yearToDayData['prevYSumm'] += $obj->sorter_summ;
						}
							else {
								$yearToDayData['prevY']['Other'] += $obj->sorter_summ;
								if ($params['oos']) {
									$yearToDayData['prevYSumm'] += $obj->sorter_summ;
								}
							}
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				$sql = "SELECT IF(SUM(count) > 0, (SUM(".$summing.")/SUM(count)), 0) AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-2), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-2), $month, $day);
				#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$yearToDayData['lastYSummPT3'] = $obj->sorter_summ;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				$sql = "SELECT IF(SUM(count) > 0, (SUM(".$summing.")/SUM(count)), 0) AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-2), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-2), $month, $day)." GROUP BY column_number_".$params['sI'];
				#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						if (array_key_exists($obj->sorter_item, $sorters_name)) {
							$yearToDayData['lastY'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
							$yearToDayData['lastYSumm'] += $obj->sorter_summ;
						}
							else {
								$yearToDayData['lastY']['Other'] += $obj->sorter_summ;
								if ($params['oos']) {
									$yearToDayData['lastYSumm'] += $obj->sorter_summ;
								}
							}
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}

				#	Определить данные для графиков intervals
				$intervalsData = [];
				$intervalsNames = [];
				if ($params['iI'] == 1) {
					$numberOfSeriee = -1;
					$countOfSeries = 14;
					for ($i = 0; $i < 26; $i++) {
						$numberOfSeriee++;
						$cMonth = $month - $i;
						$cYear = $year;
						while ($cMonth < 1) {
							$cMonth += 12;
							$cYear--;
						}
						if ($i) {
							$cDay = '31';
						}
							else {
								$cDay = $day;
								if ($cDay < 10) {
									$cDay = '0'.(int)$cDay;
								}
							}
						if ($cMonth < 10) {
							$cMonth = '0'.(int)$cMonth;
						}
						$intervalsNames[$numberOfSeriee] = $months[(int)$cMonth]."".substr($cYear, -2);
						$intervalsData[$intervalsNames[$numberOfSeriee]] = ['Other' => 0];
						$intervalsData[$intervalsNames[$numberOfSeriee].'Summ'] = 0;
						$intervalsData[$intervalsNames[$numberOfSeriee].'SummPT3'] = 0;
						$sql = "SELECT IF(SUM(count) > 0, (SUM(".$summing.")/SUM(count)), 0) AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($cYear-0), $cMonth, "01")." AND ".sprintf("%04d%02d%02d", ($cYear-0), $cMonth, $cDay);
						#BETWEEN ".sprintf("%04d%02d%02d", $cYear, $cMonth, "01")." AND ".sprintf("%04d%02d%02d", $cYear, $cMonth, $cDay)
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								$intervalsData[$intervalsNames[$numberOfSeriee].'SummPT3'] = $obj->sorter_summ;
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
						$sql = "SELECT IF(SUM(count) > 0, (SUM(".$summing.")/SUM(count)), 0) AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($cYear-0), $cMonth, "01")." AND ".sprintf("%04d%02d%02d", ($cYear-0), $cMonth, $cDay)." GROUP BY column_number_".$params['sI'];
						#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								if (array_key_exists($obj->sorter_item, $sorters_name)) {
									$intervalsData[$intervalsNames[$numberOfSeriee]][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
									$intervalsData[$intervalsNames[$numberOfSeriee].'Summ'] += $obj->sorter_summ;
								}
									else {
										$intervalsData[$intervalsNames[$numberOfSeriee]]['Other'] += $obj->sorter_summ;
										if ($params['oos']) {
											$intervalsData[$intervalsNames[$numberOfSeriee].'Summ'] += $obj->sorter_summ;
										}
									}
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
					}
				}
					else if ($params['iI'] == 2) {
						$sql = "SELECT YEAR(sorting_date) AS min_year FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter." ORDER BY sorting_date ASC LIMIT 0, 1";
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								$min_year = $obj->min_year;
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
						$sql = "SELECT YEAR(sorting_date) AS max_year FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter." ORDER BY sorting_date DESC LIMIT 0, 1";
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								$max_year = $obj->max_year;
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
						$numberOfSeriee = -1;
						$countOfSeries = $max_year - $min_year + 1;
						for ($i = 0; $i < $countOfSeries; $i++) {
							$numberOfSeriee++;
							$cYear = ($max_year - $i);
							$intervalsNames[$numberOfSeriee] = (string)$cYear;
							$intervalsData[$intervalsNames[$numberOfSeriee]] = ['Other' => 0];
							$intervalsData[$intervalsNames[$numberOfSeriee].'Summ'] = 0;
							$intervalsData[$intervalsNames[$numberOfSeriee].'SummPT3'] = 0;
							$sql = "SELECT IF(SUM(count) > 0, (SUM(".$summing.")/SUM(count)), 0) AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($cYear-0), "01", "01")." AND ".sprintf("%04d%02d%02d", ($cYear-0), "12", "31");
							#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
							if ($result = $conn->query($sql)) {
								while ($obj = $result->fetch_object()) {
									$intervalsData[$intervalsNames[$numberOfSeriee].'SummPT3'] = $obj->sorter_summ;
								}
							}
								else {
									die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
								}
							$sql = "SELECT IF(SUM(count) > 0, (SUM(".$summing.")/SUM(count)), 0) AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($cYear-0), "01", "01")." AND ".sprintf("%04d%02d%02d", ($cYear-0), "12", "31")." GROUP BY column_number_".$params['sI'];
							#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
							if ($result = $conn->query($sql)) {
								while ($obj = $result->fetch_object()) {
									if (array_key_exists($obj->sorter_item, $sorters_name)) {
										$intervalsData[$intervalsNames[$numberOfSeriee]][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
										$intervalsData[$intervalsNames[$numberOfSeriee].'Summ'] += $obj->sorter_summ;
									}
										else {
											$intervalsData[$intervalsNames[$numberOfSeriee]]['Other'] += $obj->sorter_summ;
											if ($params['oos']) {
												$intervalsData[$intervalsNames[$numberOfSeriee].'Summ'] += $obj->sorter_summ;
											}
										}
								}
							}
								else {
									die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
								}
						}
					}
					else {
						$numberOfSeriee = -1;
						$cYear = $year;
						$monthDiv = floor(($month-1)/3);
						for ($i = 0; $i <= $monthDiv; $i++) {
							$numberOfSeriee++;
							$cMonthBegin = 1 + ($monthDiv - $i) * 3;
							if ($i) {
								$cDay = '31';
								$cMonthEnd = $cMonthBegin + 2;
							}
								else {
									$cMonthEnd = $month;
									$cDay = $day;
									if ($cDay < 10) {
										$cDay = '0'.(int)$cDay;
									}
								}
							if ($cMonthEnd < 10) {
								$cMonthBegin = '0'.(int)$cMonthBegin;
								$cMonthEnd = '0'.(int)$cMonthEnd;
							}
							$intervalsNames[$numberOfSeriee] = $cYear." Q".($monthDiv - $i + 1);
							$intervalsData[$intervalsNames[$numberOfSeriee]] = ['Other' => 0];
							$intervalsData[$intervalsNames[$numberOfSeriee].'Summ'] = 0;
							$intervalsData[$intervalsNames[$numberOfSeriee].'SummPT3'] = 0;
							$sql = "SELECT IF(SUM(count) > 0, (SUM(".$summing.")/SUM(count)), 0) AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($cYear-0), $cMonthBegin, "01")." AND ".sprintf("%04d%02d%02d", ($cYear-0), $cMonthEnd, $cDay);
							#BETWEEN ".sprintf("%04d%02d%02d", ($cYear-0), $cMonthBegin, "01")." AND ".sprintf("%04d%02d%02d", ($cYear-0), $cMonthEnd, $cDay)
							if ($result = $conn->query($sql)) {
								while ($obj = $result->fetch_object()) {
									$intervalsData[$intervalsNames[$numberOfSeriee].'SummPT3'] = $obj->sorter_summ;
								}
							}
								else {
									die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
								}
							$sql = "SELECT IF(SUM(count) > 0, (SUM(".$summing.")/SUM(count)), 0) AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($cYear-0), $cMonthBegin, "01")." AND ".sprintf("%04d%02d%02d", ($cYear-0), $cMonthEnd, $cDay)." GROUP BY column_number_".$params['sI'];
							#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
							if ($result = $conn->query($sql)) {
								while ($obj = $result->fetch_object()) {
									if (array_key_exists($obj->sorter_item, $sorters_name)) {
										$intervalsData[$intervalsNames[$numberOfSeriee]][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
										$intervalsData[$intervalsNames[$numberOfSeriee].'Summ'] += $obj->sorter_summ;
									}
										else {
											$intervalsData[$intervalsNames[$numberOfSeriee]]['Other'] += $obj->sorter_summ;
											if ($params['oos']) {
												$intervalsData[$intervalsNames[$numberOfSeriee].'Summ'] += $obj->sorter_summ;
											}
										}
								}
							}
								else {
									die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
								}
						}
						$countOfSeries = 5 + $numberOfSeriee;
						for ($i = 1; $i < 3; $i++) {
							$cYear--;
							for ($j = 0; $j < 4; $j++) {
								$numberOfSeriee++;
								$cMonthBegin = 10 - 3 * $j;
								$cMonthEnd = $cMonthBegin + 2;
								if ($cMonthBegin < 10) {
									$cMonthBegin = '0'.(int)$cMonthBegin;
									$cMonthEnd = '0'.(int)$cMonthEnd;
								}
								$intervalsNames[$numberOfSeriee] = $cYear." Q".(4 - $j);
								$intervalsData[$intervalsNames[$numberOfSeriee]] = ['Other' => 0];
								$intervalsData[$intervalsNames[$numberOfSeriee].'Summ'] = 0;
								$intervalsData[$intervalsNames[$numberOfSeriee].'SummPT3'] = 0;
								$sql = "SELECT IF(SUM(count) > 0, (SUM(".$summing.")/SUM(count)), 0) AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($cYear-0), $cMonthBegin, "01")." AND ".sprintf("%04d%02d%02d", ($cYear-0), $cMonthEnd, "31");
								#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
								if ($result = $conn->query($sql)) {
									while ($obj = $result->fetch_object()) {
										$intervalsData[$intervalsNames[$numberOfSeriee].'SummPT3'] = $obj->sorter_summ;
									}
								}
									else {
										die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
									}
								$sql = "SELECT IF(SUM(count) > 0, (SUM(".$summing.")/SUM(count)), 0) AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($cYear-0), $cMonthBegin, "01")." AND ".sprintf("%04d%02d%02d", ($cYear-0), $cMonthEnd, "31")." GROUP BY column_number_".$params['sI'];
								#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
								if ($result = $conn->query($sql)) {
									while ($obj = $result->fetch_object()) {
										if (array_key_exists($obj->sorter_item, $sorters_name)) {
											$intervalsData[$intervalsNames[$numberOfSeriee]][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
											$intervalsData[$intervalsNames[$numberOfSeriee].'Summ'] += $obj->sorter_summ;
										}
											else {
												$intervalsData[$intervalsNames[$numberOfSeriee]]['Other'] += $obj->sorter_summ;
												if ($params['oos']) {
													$intervalsData[$intervalsNames[$numberOfSeriee].'Summ'] += $obj->sorter_summ;
												}
											}
									}
								}
									else {
										die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
									}
							}
						}
					}
				/*	Для 8го графика подсчет "азерс" иной - переделать запросы и далее подсчеты все - пока проще без "азерс" (соответственно на единицу изменяется коичество лидеров сортера)
					if (($other_summ > 0) && ($params['oos'])) {
					$sorters[] = 0;
					$sorters_name[0] = 'Other';
				}
				*/
				#	Подсчет процентного выражения и приведение величин к масштабу миллиардов
				foreach ($sorters_name as $key) {
					#	Проценты
					$monthToMonthData['currYPerc'][$key] = (($monthToMonthData['currYSumm'] != 0) ? (round($monthToMonthData['currY'][$key] / $monthToMonthData['currYSumm'] * 1000) / 10) : 0.0);
					$monthToMonthData['prevYPerc'][$key] = (($monthToMonthData['prevYSumm'] != 0) ? (round($monthToMonthData['prevY'][$key] / $monthToMonthData['prevYSumm'] * 1000) / 10) : 0.0);
					$monthToMonthData['lastYPerc'][$key] = (($monthToMonthData['lastYSumm'] != 0) ? (round($monthToMonthData['lastY'][$key] / $monthToMonthData['lastYSumm'] * 1000) / 10) : 0.0);
					$yearToDayData['currYPerc'][$key] = (($yearToDayData['currYSumm'] != 0) ? (round($yearToDayData['currY'][$key] / $yearToDayData['currYSumm'] * 1000) / 10) : 0.0);
					$yearToDayData['prevYPerc'][$key] = (($yearToDayData['prevYSumm'] != 0) ? (round($yearToDayData['prevY'][$key] / $yearToDayData['prevYSumm'] * 1000) / 10) : 0.0);
					$yearToDayData['lastYPerc'][$key] = (($yearToDayData['lastYSumm'] != 0) ? (round($yearToDayData['lastY'][$key] / $yearToDayData['lastYSumm'] * 1000) / 10) : 0.0);
					$monthToMonthData['currYPercPT3'][$key] = (($monthToMonthData['currYSummPT3'] != 0) ? (round($monthToMonthData['currY'][$key] / $monthToMonthData['currYSummPT3'] * 1000) / 10) : 0.0);
					$monthToMonthData['prevYPercPT3'][$key] = (($monthToMonthData['prevYSummPT3'] != 0) ? (round($monthToMonthData['prevY'][$key] / $monthToMonthData['prevYSummPT3'] * 1000) / 10) : 0.0);
					$monthToMonthData['lastYPercPT3'][$key] = (($monthToMonthData['lastYSummPT3'] != 0) ? (round($monthToMonthData['lastY'][$key] / $monthToMonthData['lastYSummPT3'] * 1000) / 10) : 0.0);
					$yearToDayData['currYPercPT3'][$key] = (($yearToDayData['currYSummPT3'] != 0) ? (round($yearToDayData['currY'][$key] / $yearToDayData['currYSummPT3'] * 1000) / 10) : 0.0);
					$yearToDayData['prevYPercPT3'][$key] = (($yearToDayData['prevYSummPT3'] != 0) ? (round($yearToDayData['prevY'][$key] / $yearToDayData['prevYSummPT3'] * 1000) / 10) : 0.0);
					$yearToDayData['lastYPercPT3'][$key] = (($yearToDayData['lastYSummPT3'] != 0) ? (round($yearToDayData['lastY'][$key] / $yearToDayData['lastYSummPT3'] * 1000) / 10) : 0.0);
					#	Масштабирование
					if (!($params['sP'])) {
						$monthToMonthData['currY'][$key] = round($monthToMonthData['currY'][$key], 2);
						$monthToMonthData['prevY'][$key] = round($monthToMonthData['prevY'][$key], 2);
						$monthToMonthData['lastY'][$key] = round($monthToMonthData['lastY'][$key], 2);
						$yearToDayData['currY'][$key] = round($yearToDayData['currY'][$key], 2);
						$yearToDayData['prevY'][$key] = round($yearToDayData['prevY'][$key], 2);
						$yearToDayData['lastY'][$key] = round($yearToDayData['lastY'][$key], 2);
					} else if ($params['sP'] == 1) {
						$monthToMonthData['currY'][$key] = round($monthToMonthData['currY'][$key], 2);
						$monthToMonthData['prevY'][$key] = round($monthToMonthData['prevY'][$key], 2);
						$monthToMonthData['lastY'][$key] = round($monthToMonthData['lastY'][$key], 2);
						$yearToDayData['currY'][$key] = round($yearToDayData['currY'][$key], 2);
						$yearToDayData['prevY'][$key] = round($yearToDayData['prevY'][$key], 2);
						$yearToDayData['lastY'][$key] = round($yearToDayData['lastY'][$key], 2);
					}
					#	Проценты и масштабирование для интервального графика
					foreach ($intervalsNames as $ival) {
						$intervalsData[$ival.'Perc'][$key] = (($intervalsData[$ival.'Summ'] != 0) ? (round($intervalsData[$ival][$key] / $intervalsData[$ival.'Summ'] * 1000) / 10) : 0.0);
						$intervalsData[$ival.'PercPT3'][$key] = (($intervalsData[$ival.'SummPT3'] != 0) ? (round($intervalsData[$ival][$key] / $intervalsData[$ival.'SummPT3'] * 1000) / 10) : 0.0);
						if (!($params['sP'])) {
							$intervalsData[$ival][$key] = round($intervalsData[$ival][$key], 2);
						} else if ($params['sP'] == 1) {
							$intervalsData[$ival][$key] = round($intervalsData[$ival][$key], 2);
						}
					}
					
				}
				#	Масштабирование суммарных данных
				if (!($params['sP'])) {
					$monthToMonthData['currYSumm'] = round($monthToMonthData['currYSumm'], 2);
					$monthToMonthData['prevYSumm'] = round($monthToMonthData['prevYSumm'], 2);
					$monthToMonthData['lastYSumm'] = round($monthToMonthData['lastYSumm'], 2);
					$yearToDayData['currYSumm'] = round($yearToDayData['currYSumm'], 2);
					$yearToDayData['prevYSumm'] = round($yearToDayData['prevYSumm'], 2);
					$yearToDayData['lastYSumm'] = round($yearToDayData['lastYSumm'], 2);
					$monthToMonthData['currYSummPT3'] = round($monthToMonthData['currYSummPT3'], 2);
					$monthToMonthData['prevYSummPT3'] = round($monthToMonthData['prevYSummPT3'], 2);
					$monthToMonthData['lastYSummPT3'] = round($monthToMonthData['lastYSummPT3'], 2);
					$yearToDayData['currYSummPT3'] = round($yearToDayData['currYSummPT3'], 2);
					$yearToDayData['prevYSummPT3'] = round($yearToDayData['prevYSummPT3'], 2);
					$yearToDayData['lastYSummPT3'] = round($yearToDayData['lastYSummPT3'], 2);
					foreach ($intervalsNames as $ival) {
						$intervalsData[$ival.'Summ'] = round($intervalsData[$ival.'Summ'], 2);
						$intervalsData[$ival.'SummPT3'] = round($intervalsData[$ival.'SummPT3'], 2);
					}
				} else if ($params['sP'] == 1) {
					$monthToMonthData['currYSumm'] = round($monthToMonthData['currYSumm'], 2);
					$monthToMonthData['prevYSumm'] = round($monthToMonthData['prevYSumm'], 2);
					$monthToMonthData['lastYSumm'] = round($monthToMonthData['lastYSumm'], 2);
					$yearToDayData['currYSumm'] = round($yearToDayData['currYSumm'], 2);
					$yearToDayData['prevYSumm'] = round($yearToDayData['prevYSumm'], 2);
					$yearToDayData['lastYSumm'] = round($yearToDayData['lastYSumm'], 2);
					$monthToMonthData['currYSummPT3'] = round($monthToMonthData['currYSummPT3'], 2);
					$monthToMonthData['prevYSummPT3'] = round($monthToMonthData['prevYSummPT3'], 2);
					$monthToMonthData['lastYSummPT3'] = round($monthToMonthData['lastYSummPT3'], 2);
					$yearToDayData['currYSummPT3'] = round($yearToDayData['currYSummPT3'], 2);
					$yearToDayData['prevYSummPT3'] = round($yearToDayData['prevYSummPT3'], 2);
					$yearToDayData['lastYSummPT3'] = round($yearToDayData['lastYSummPT3'], 2);
					foreach ($intervalsNames as $ival) {
						$intervalsData[$ival.'Summ'] = round($intervalsData[$ival.'Summ'], 2);
						$intervalsData[$ival.'SummPT3'] = round($intervalsData[$ival.'SummPT3'], 2);
					}
				}
				#	Определение величин приращения (для легенды)/ формирование данных легенды
				$legends = [];
				if ($params['gT'] === '2') {//$params['gT'] === '2'	??? было iI
					$legends['up'] = [$monthToMonthData['prevYSummPT3'], $monthToMonthData['currYSummPT3'], $yearToDayData['prevYSummPT3'], $yearToDayData['currYSummPT3'], $mst_string];
					$legends['down'] = [
						(($monthToMonthData['lastYSummPT3'] != 0) ? (round((($monthToMonthData['prevYSummPT3'] - $monthToMonthData['lastYSummPT3']) / $monthToMonthData['lastYSummPT3']) * 1000) / 10) : 100.0),
						(($monthToMonthData['prevYSummPT3'] != 0) ? (round((($monthToMonthData['currYSummPT3'] - $monthToMonthData['prevYSummPT3']) / $monthToMonthData['prevYSummPT3']) * 1000) / 10) : 100.0),
						(($yearToDayData['lastYSummPT3'] != 0) ? (round((($yearToDayData['prevYSummPT3'] - $yearToDayData['lastYSummPT3']) / $yearToDayData['lastYSummPT3']) * 1000) / 10) : 100.0),
						(($yearToDayData['prevYSummPT3'] != 0) ? (round((($yearToDayData['currYSummPT3'] - $yearToDayData['prevYSummPT3']) / $yearToDayData['prevYSummPT3']) * 1000) / 10) : 100.0),
						'+/- % SPLY'
					];
					$legends['color'] = [
						(($monthToMonthData['prevYSummPT3'] < $monthToMonthData['lastYSummPT3']) ? 'Red' : 'Green'),
						(($monthToMonthData['currYSummPT3'] < $monthToMonthData['prevYSummPT3']) ? 'Red' : 'Green'),
						(($yearToDayData['prevYSummPT3'] < $yearToDayData['lastYSummPT3']) ? 'Red' : 'Green'),
						(($yearToDayData['currYSummPT3'] < $yearToDayData['prevYSummPT3']) ? 'Red' : 'Green'),
						'Black'
					];
					for ($i = ($countOfSeries - 1); $i > -1; $i--) {
						$legends['up'][] = $intervalsData[$intervalsNames[$i].'SummPT3'];
						if ($params['iI'] == 1) {
							$legends['down'][] = (($intervalsData[$intervalsNames[$i + 12].'SummPT3'] != 0) ? (round((($intervalsData[$intervalsNames[$i].'SummPT3'] - $intervalsData[$intervalsNames[$i + 12].'SummPT3']) / $intervalsData[$intervalsNames[$i + 12].'SummPT3']) * 1000) / 10) : 100.0);
							$legends['color'][] = (($intervalsData[$intervalsNames[$i].'SummPT3'] < $intervalsData[$intervalsNames[$i + 12].'SummPT3']) ? 'Red' : 'Green');
						} else if ($params['iI'] == 2) {
							if ($i < ($countOfSeries - 1)) {
								$legends['down'][] = (($intervalsData[$intervalsNames[$i + 1].'SummPT3'] != 0) ? (round((($intervalsData[$intervalsNames[$i].'SummPT3'] - $intervalsData[$intervalsNames[$i + 1].'SummPT3']) / $intervalsData[$intervalsNames[$i + 1].'SummPT3']) * 1000) / 10) : 100.0);
								$legends['color'][] = (($intervalsData[$intervalsNames[$i].'SummPT3'] < $intervalsData[$intervalsNames[$i + 1].'SummPT3']) ? 'Red' : 'Green');
							} else {
								$legends['down'][] = 100.0;
								$legends['color'][] = 'Green';
							}
						} else {
							$legends['down'][] = (($intervalsData[$intervalsNames[$i + 4].'SummPT3'] != 0) ? (round((($intervalsData[$intervalsNames[$i].'SummPT3'] - $intervalsData[$intervalsNames[$i + 4].'SummPT3']) / $intervalsData[$intervalsNames[$i + 4].'SummPT3']) * 1000) / 10) : 100.0);
							$legends['color'][] = (($intervalsData[$intervalsNames[$i].'SummPT3'] < $intervalsData[$intervalsNames[$i + 4].'SummPT3']) ? 'Red' : 'Green');
						}
					}
				} else {
					$legends['up'] = [$monthToMonthData['prevYSumm'], $monthToMonthData['currYSumm'], $yearToDayData['prevYSumm'], $yearToDayData['currYSumm'], $mst_string];
					$legends['down'] = [
						(($monthToMonthData['lastYSumm'] != 0) ? (round((($monthToMonthData['prevYSumm'] - $monthToMonthData['lastYSumm']) / $monthToMonthData['lastYSumm']) * 1000) / 10) : 100.0),
						(($monthToMonthData['prevYSumm'] != 0) ? (round((($monthToMonthData['currYSumm'] - $monthToMonthData['prevYSumm']) / $monthToMonthData['prevYSumm']) * 1000) / 10) : 100.0),
						(($yearToDayData['lastYSumm'] != 0) ? (round((($yearToDayData['prevYSumm'] - $yearToDayData['lastYSumm']) / $yearToDayData['lastYSumm']) * 1000) / 10) : 100.0),
						(($yearToDayData['prevYSumm'] != 0) ? (round((($yearToDayData['currYSumm'] - $yearToDayData['prevYSumm']) / $yearToDayData['prevYSumm']) * 1000) / 10) : 100.0),
						'+/- % SPLY'
					];
					$legends['color'] = [
						(($monthToMonthData['prevYSumm'] < $monthToMonthData['lastYSumm']) ? 'Red' : 'Green'),
						(($monthToMonthData['currYSumm'] < $monthToMonthData['prevYSumm']) ? 'Red' : 'Green'),
						(($yearToDayData['prevYSumm'] < $yearToDayData['lastYSumm']) ? 'Red' : 'Green'),
						(($yearToDayData['currYSumm'] < $yearToDayData['prevYSumm']) ? 'Red' : 'Green'),
						'Black'
					];
					for ($i = ($countOfSeries - 1); $i > -1; $i--) {
						$legends['up'][] = $intervalsData[$intervalsNames[$i].'Summ'];
						if ($params['iI'] == 1) {
							$legends['down'][] = (($intervalsData[$intervalsNames[$i + 12].'Summ'] != 0) ? (round((($intervalsData[$intervalsNames[$i].'Summ'] - $intervalsData[$intervalsNames[$i + 12].'Summ']) / $intervalsData[$intervalsNames[$i + 12].'Summ']) * 1000) / 10) : 100.0);
							$legends['color'][] = (($intervalsData[$intervalsNames[$i].'Summ'] < $intervalsData[$intervalsNames[$i + 12].'Summ']) ? 'Red' : 'Green');
						} else if ($params['iI'] == 2) {
							if ($i < ($countOfSeries - 1)) {
								$legends['down'][] = (($intervalsData[$intervalsNames[$i + 1].'Summ'] != 0) ? (round((($intervalsData[$intervalsNames[$i].'Summ'] - $intervalsData[$intervalsNames[$i + 1].'Summ']) / $intervalsData[$intervalsNames[$i + 1].'Summ']) * 1000) / 10) : 100.0);
								$legends['color'][] = (($intervalsData[$intervalsNames[$i].'Summ'] < $intervalsData[$intervalsNames[$i + 1].'Summ']) ? 'Red' : 'Green');
							} else {
								$legends['down'][] = 100.0;
								$legends['color'][] = 'Green';
							}
						} else {
							$legends['down'][] = (($intervalsData[$intervalsNames[$i + 4].'Summ'] != 0) ? (round((($intervalsData[$intervalsNames[$i].'Summ'] - $intervalsData[$intervalsNames[$i + 4].'Summ']) / $intervalsData[$intervalsNames[$i + 4].'Summ']) * 1000) / 10) : 100.0);
							$legends['color'][] = (($intervalsData[$intervalsNames[$i].'Summ'] < $intervalsData[$intervalsNames[$i + 4].'Summ']) ? 'Red' : 'Green');
						}
					}
				}
				#	Формирование итогового ответа
				$results = [];
				$names = [];
				foreach ($sorters as $key) {
					$result = '{"mtmcv":"'.$monthToMonthData['currY'][$sorters_name[$key]].'","mtmcp":"'.$monthToMonthData['currY'][$sorters_name[$key]].'","mtmcppt3":"'.$monthToMonthData['currY'][$sorters_name[$key]].'","mtmpv":"'.$monthToMonthData['prevY'][$sorters_name[$key]].'","mtmpp":"'.$monthToMonthData['prevY'][$sorters_name[$key]].'","mtmpppt3":"'.$monthToMonthData['prevY'][$sorters_name[$key]].'","mtmlv":"'.$monthToMonthData['lastY'][$sorters_name[$key]].'","mtmlp":"'.$monthToMonthData['lastY'][$sorters_name[$key]].'","mtmlppt3":"'.$monthToMonthData['lastY'][$sorters_name[$key]].'","ytdcv":"'.$yearToDayData['currY'][$sorters_name[$key]].'","ytdcp":"'.$yearToDayData['currY'][$sorters_name[$key]].'","ytdcppt3":"'.$yearToDayData['currY'][$sorters_name[$key]].'","ytdpv":"'.$yearToDayData['prevY'][$sorters_name[$key]].'","ytdpp":"'.$yearToDayData['prevY'][$sorters_name[$key]].'","ytdpppt3":"'.$yearToDayData['prevY'][$sorters_name[$key]].'","ytdlv":"'.$yearToDayData['lastY'][$sorters_name[$key]].'","ytdlp":"'.$yearToDayData['lastY'][$sorters_name[$key]].'","ytdlppt3":"'.$yearToDayData['lastY'][$sorters_name[$key]].'","iD":[';
					for ($i = 0; $i < ($countOfSeries - 1); $i++) {
						$result .= '{"idn":"'.$intervalsNames[$i].'","idv":"'.$intervalsData[$intervalsNames[$i]][$sorters_name[$key]].'","idp":"'.$intervalsData[$intervalsNames[$i].''][$sorters_name[$key]].'","idppt3":"'.$intervalsData[$intervalsNames[$i].''][$sorters_name[$key]].'"},';
					}
					$result .= '{"idn":"'.$intervalsNames[$countOfSeries - 1].'","idv":"'.$intervalsData[$intervalsNames[$countOfSeries - 1]][$sorters_name[$key]].'","idp":"'.$intervalsData[$intervalsNames[$countOfSeries - 1].''][$sorters_name[$key]].'","idppt3":"'.$intervalsData[$intervalsNames[$countOfSeries - 1].''][$sorters_name[$key]].'"}]}';
					$results[] = $result;
					$names[] = $sorters_name[$key];
				}
				echo '{"result":1,"slide_legend":"'.$slide_legend.'","data":{"titlemtm0":"'.$titlemtm0.'","titlemtm1":"'.$titlemtm1.'","titleytd0":"'.$titleytd0.'","titleytd1":"'.$titleytd1.'","ss":['.implode(',',$sorters).'],"sNs":["'.implode('","',$names).'"],"rs":['.implode(',',$results).'],"lUs":["'.implode('","',$legends['up']).'"],"lDs":["'.implode('","',$legends['down']).'"],"lCs":["'.implode('","',$legends['color']).'"],"lMs":["'.implode('","',$monthToMonthLabels).'"],"lYs":["'.implode('","',$yearToDayLabels).'"]}}';

				/* Prev var
				foreach ($sorters as $key) {
					$result = '{"mtmcv":"'.$monthToMonthData['currY'][$sorters_name[$key]].'","mtmcp":"'.$monthToMonthData['currYPerc'][$sorters_name[$key]].'","mtmcppt3":"'.$monthToMonthData['currYPercPT3'][$sorters_name[$key]].'","mtmpv":"'.$monthToMonthData['prevY'][$sorters_name[$key]].'","mtmpp":"'.$monthToMonthData['prevYPerc'][$sorters_name[$key]].'","mtmpppt3":"'.$monthToMonthData['prevYPercPT3'][$sorters_name[$key]].'","mtmlv":"'.$monthToMonthData['lastY'][$sorters_name[$key]].'","mtmlp":"'.$monthToMonthData['lastYPerc'][$sorters_name[$key]].'","mtmlppt3":"'.$monthToMonthData['lastYPercPT3'][$sorters_name[$key]].'","ytdcv":"'.$yearToDayData['currY'][$sorters_name[$key]].'","ytdcp":"'.$yearToDayData['currYPerc'][$sorters_name[$key]].'","ytdcppt3":"'.$yearToDayData['currYPercPT3'][$sorters_name[$key]].'","ytdpv":"'.$yearToDayData['prevY'][$sorters_name[$key]].'","ytdpp":"'.$yearToDayData['prevYPerc'][$sorters_name[$key]].'","ytdpppt3":"'.$yearToDayData['prevYPercPT3'][$sorters_name[$key]].'","ytdlv":"'.$yearToDayData['lastY'][$sorters_name[$key]].'","ytdlp":"'.$yearToDayData['lastYPerc'][$sorters_name[$key]].'","ytdlppt3":"'.$yearToDayData['lastYPercPT3'][$sorters_name[$key]].'","iD":[';
					for ($i = 0; $i < ($countOfSeries - 1); $i++) {
						$result .= '{"idn":"'.$intervalsNames[$i].'","idv":"'.$intervalsData[$intervalsNames[$i]][$sorters_name[$key]].'","idp":"'.$intervalsData[$intervalsNames[$i].'Perc'][$sorters_name[$key]].'","idppt3":"'.$intervalsData[$intervalsNames[$i].'PercPT3'][$sorters_name[$key]].'"},';
					}
					$result .= '{"idn":"'.$intervalsNames[$countOfSeries - 1].'","idv":"'.$intervalsData[$intervalsNames[$countOfSeries - 1]][$sorters_name[$key]].'","idp":"'.$intervalsData[$intervalsNames[$countOfSeries - 1].'Perc'][$sorters_name[$key]].'","idppt3":"'.$intervalsData[$intervalsNames[$countOfSeries - 1].'PercPT3'][$sorters_name[$key]].'"}]}';
					$results[] = $result;
					$names[] = $sorters_name[$key];
				}
				echo '{"result":1,"slide_legend":"'.$slide_legend.'","data":{"titlemtm0":"'.$titlemtm0.'","titlemtm1":"'.$titlemtm1.'","titleytd0":"'.$titleytd0.'","titleytd1":"'.$titleytd1.'","ss":['.implode(',',$sorters).'],"sNs":["'.implode('","',$names).'"],"rs":['.implode(',',$results).'],"lUs":["'.implode('","',$legends['up']).'"],"lDs":["'.implode('","',$legends['down']).'"],"lCs":["'.implode('","',$legends['color']).'"],"lMs":["'.implode('","',$monthToMonthLabels).'"],"lYs":["'.implode('","',$yearToDayLabels).'"]}}';
				*/

			}
			else if ($page == 14) {
				$filters_queries = [];

				foreach ($params['fD'] as $item) {
					$filters_queries[] = " column_number_".$item['cI']." IN (".implode(', ',$item['vI']).")";
				}
				if ($filters_queries) {
					$filters_query = implode(" AND", $filters_queries);
					$main_filter = $filters_queries[0];
				}
					else {
						$filters_query = " 1";
						$main_filter = " 1";
					}

				$date_pieces = explode('-', $_POST['edate']);
				$year = $date_pieces[0];
				$month = $date_pieces[1];
				$day = $date_pieces[2];
				$sqldate = $year.'-'.$month.'-'.$day;
				#	Определить названия кварталов
				$sql = "SELECT IF((QUARTER(DATE_ADD(t.sqldate, INTERVAL 1 DAY))=QUARTER(t.sqldate)),0,1) AS lastday, QUARTER(t.sqldate) AS quart FROM (SELECT '".$sqldate."' AS sqldate) as t";
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						if ($obj->lastday) {
							switch ($obj->quart) {
								case 1: {
									$yearToDayLabels = [($year - 1).' Q2', ($year - 1).' Q3', ($year - 1).' Q4', ($year).' Q1'];
									$diapazons = [
										($year - 2).'0401 AND '.($year - 2).'0630',
										($year - 2).'0701 AND '.($year - 2).'0930',
										($year - 2).'1001 AND '.($year - 2).'1231',
										($year - 1).'0101 AND '.($year - 1).'0331',
										($year - 1).'0401 AND '.($year - 1).'0630',
										($year - 1).'0701 AND '.($year - 1).'0930',
										($year - 1).'1001 AND '.($year - 1).'1231',
										($year).'0101 AND '.($year).'0331'
									];
									break;
								}
								case 2: {
									$yearToDayLabels = [($year - 1).' Q3', ($year - 1).' Q4', ($year).' Q1', ($year).' Q2'];
									$diapazons = [
										($year - 2).'0701 AND '.($year - 2).'0930',
										($year - 2).'1001 AND '.($year - 2).'1231',
										($year - 1).'0101 AND '.($year - 1).'0331',
										($year - 1).'0401 AND '.($year - 1).'0630',
										($year - 1).'0701 AND '.($year - 1).'0930',
										($year - 1).'1001 AND '.($year - 1).'1231',
										($year).'0101 AND '.($year).'0331',
										($year).'0401 AND '.($year).'0630'
									];
									break;
								}
								case 3: {
									$yearToDayLabels = [($year - 1).' Q4', ($year).' Q1', ($year).' Q2', ($year).' Q3'];
									$diapazons = [
										($year - 2).'1001 AND '.($year - 2).'1231',
										($year - 1).'0101 AND '.($year - 1).'0331',
										($year - 1).'0401 AND '.($year - 1).'0630',
										($year - 1).'0701 AND '.($year - 1).'0930',
										($year - 1).'1001 AND '.($year - 1).'1231',
										($year).'0101 AND '.($year).'0331',
										($year).'0401 AND '.($year).'0630',
										($year).'0701 AND '.($year).'0930'
									];
									break;
								}
								case 4: {
									$yearToDayLabels = [($year).' Q1', ($year).' Q2', ($year).' Q3', ($year).' Q4'];
									$diapazons = [
										($year - 1).'0101 AND '.($year - 1).'0331',
										($year - 1).'0401 AND '.($year - 1).'0630',
										($year - 1).'0701 AND '.($year - 1).'0930',
										($year - 1).'1001 AND '.($year - 1).'1231',
										($year).'0101 AND '.($year).'0331',
										($year).'0401 AND '.($year).'0630',
										($year).'0701 AND '.($year).'0930',
										($year).'1001 AND '.($year).'1231'
									];
									break;
								}
							}
						} else {
							switch ($obj->quart) {
								case 1: {
									$yearToDayLabels = [($year - 1).' Q1', ($year - 1).' Q2', ($year - 1).' Q3', ($year - 1).' Q4'];
									$diapazons = [
										($year - 2).'0101 AND '.($year - 2).'0331',
										($year - 2).'0401 AND '.($year - 2).'0630',
										($year - 2).'0701 AND '.($year - 2).'0930',
										($year - 2).'1001 AND '.($year - 2).'1231',
										($year - 1).'0101 AND '.($year - 1).'0331',
										($year - 1).'0401 AND '.($year - 1).'0630',
										($year - 1).'0701 AND '.($year - 1).'0930',
										($year - 1).'1001 AND '.($year - 1).'1231'
									];
									break;
								}
								case 2: {
									$yearToDayLabels = [($year - 1).' Q2', ($year - 1).' Q3', ($year - 1).' Q4', ($year).' Q1'];
									$diapazons = [
										($year - 2).'0401 AND '.($year - 2).'0630',
										($year - 2).'0701 AND '.($year - 2).'0930',
										($year - 2).'1001 AND '.($year - 2).'1231',
										($year - 1).'0101 AND '.($year - 1).'0331',
										($year - 1).'0401 AND '.($year - 1).'0630',
										($year - 1).'0701 AND '.($year - 1).'0930',
										($year - 1).'1001 AND '.($year - 1).'1231',
										($year).'0101 AND '.($year).'0331'
									];
									break;
								}
								case 3: {
									$yearToDayLabels = [($year - 1).' Q3', ($year - 1).' Q4', ($year).' Q1', ($year).' Q2'];
									$diapazons = [
										($year - 2).'0701 AND '.($year - 2).'0930',
										($year - 2).'1001 AND '.($year - 2).'1231',
										($year - 1).'0101 AND '.($year - 1).'0331',
										($year - 1).'0401 AND '.($year - 1).'0630',
										($year - 1).'0701 AND '.($year - 1).'0930',
										($year - 1).'1001 AND '.($year - 1).'1231',
										($year).'0101 AND '.($year).'0331',
										($year).'0401 AND '.($year).'0630'
									];
									break;
								}
								case 4: {
									$yearToDayLabels = [($year - 1).' Q4', ($year).' Q1', ($year).' Q2', ($year).' Q3'];
									$diapazons = [
										($year - 2).'1001 AND '.($year - 2).'1231',
										($year - 1).'0101 AND '.($year - 1).'0331',
										($year - 1).'0401 AND '.($year - 1).'0630',
										($year - 1).'0701 AND '.($year - 1).'0930',
										($year - 1).'1001 AND '.($year - 1).'1231',
										($year).'0101 AND '.($year).'0331',
										($year).'0401 AND '.($year).'0630',
										($year).'0701 AND '.($year).'0930'
									];
									break;
								}
							}
						}
						break;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
					
				$sql = "SELECT CONCAT(YEAR(t.sqldate),' Q',IF((QUARTER(DATE_ADD(t.sqldate, INTERVAL 1 DAY))=QUARTER(t.sqldate)),QUARTER(DATE_SUB(t.sqldate, INTERVAL 3 MONTH)),QUARTER(t.sqldate))) AS quart FROM (SELECT '".$sqldate."' AS sqldate) as t";
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$yearToDayLabels[] = $obj->quart;
						break;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}



				$sorters = [];
				$sorters_summ = [];
				$sorters_name = [];
				$other_summ = 0;
				if ($params['sI']) {
					#	Определить лидеров по "сортеру"
					$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item, value AS item_name, (SELECT value FROM db_".$dbase."_xls_for_diagrams_columns WHERE id = ".$params['sI'].") AS sorter_name FROM db_".$dbase."_xls_for_diagrams_table JOIN db_".$dbase."_xls_for_diagrams_unique_values ON (db_".$dbase."_xls_for_diagrams_unique_values.column_id = ".$params['sI']." AND db_".$dbase."_xls_for_diagrams_unique_values.id = db_".$dbase."_xls_for_diagrams_table.column_number_".$params['sI'].") WHERE".$filters_query." GROUP BY column_number_".$params['sI']." ORDER BY sorter_summ DESC";
					if ($result = $conn->query($sql)) {
						if ($params['ors']) {
							$sorters = $params['osC'];
							foreach ($sorters as $item) {
								$sorters_summ[$item] = 0;
							}
							while ($obj = $result->fetch_object()) {
								if (in_array($obj->sorter_item, $sorters)) {
									$sorters_summ[$obj->sorter_item] = $obj->sorter_summ;
								}
									else {
										$other_summ += $obj->sorter_summ;
									}
							}
						}
							else {
								$count = 0;
								while ($obj = $result->fetch_object()) {
									$count++;
									if ($count < $params['sCs']) {
										if (($params['sU']) && (( $obj->sorter_name == 'Корпорация') || ( $obj->sorter_name == 'Корпорация - map')) && ($obj->item_name == 'U.M.')) {
											$count--;
											$other_summ += $obj->sorter_summ;
										}
											else {
												$sorters[] = $obj->sorter_item;
												$sorters_summ[$obj->sorter_item] = $obj->sorter_summ;
											}
									}
										else {
											if ((( $obj->sorter_name == 'Корпорация') && ($obj->sorter_item == $mainCorp)) || (( $obj->sorter_name === 'Корпорация - map') && ($obj->sorter_item == $mainCorp))) {
												$other_summ += $sorters_summ[$sorters[$params['sCs'] - 2]];
												$sorters[$params['sCs'] - 2] = $obj->sorter_item;
												$sorters_summ[$obj->sorter_item] = $obj->sorter_summ;
											}
												else {
													$other_summ += $obj->sorter_summ;
												}
										}
								}
							}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
				}
					else {
						foreach ($params['dD'] as $key => $item) {
							$sorters[] = $key;
							$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
							$sorter_name = 'Ценовые диапазоны';
							if ($result = $conn->query($sql)) {
								while ($obj = $result->fetch_object()) {
									$sorters_summ[$key] = $obj->sorter_summ;
								}
							}
								else {
									die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
								}
							$sorters_name[$key] = $key;
						}
					}

				if (!(count($sorters))) {
					echo '{"result":0,"error":"Фильтры отсеяли все значения"}';
					break;
				}

				if ($params['sI']) {
					#	Определить название "сортера"
					$sql = "SELECT value FROM db_".$dbase."_xls_for_diagrams_columns WHERE id = ".$params['sI'];
					$sorter_name = '';
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							$sorter_name = $obj->value;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}

					#	Определить названия лидеров "сортера"
					$sql = "SELECT id, value FROM db_".$dbase."_xls_for_diagrams_unique_values WHERE column_id = ".$params['sI']." AND id IN (".implode(', ', $sorters).")";
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							if (((($sorter_name === 'Корпорация') && ($obj->id == $mainCorp)) || (($sorter_name === 'Корпорация - map') && ($obj->id == $mainCorp))) && (!($params['ors']))) {
								array_splice($sorters, array_search($obj->id, $sorters), 1);
								array_unshift($sorters, $obj->id);
							}
							$sorters_name[$obj->id] = $obj->value;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
				}

				#	Определить название второго "сортера"
				$sql = "SELECT value FROM db_".$dbase."_xls_for_diagrams_columns WHERE id = ".$params['sCD'];
				$sub_sorters_name = [];
				$sub_sorters = [];
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$sub_sorters_name[0] = 'Total';
						$sub_sorters[] = 0;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}

				#	Определить названия категорий второго "сортера"
				
				#	тут добавить фильтр полученных ИТОГОВЫХ айди по выбранному списку - остальное сработает автоматически (!), по поводу упорядочивания - подумать над этим, вероятнее в пост обработке
				
				
				$sql = "SELECT id, value FROM db_".$dbase."_xls_for_diagrams_unique_values WHERE id IN (SELECT DISTINCT column_number_".$params['sCD']." FROM `db_".$dbase."_xls_for_diagrams_table` WHERE".$filters_query.") AND id IN (".implode(',',$params['ssC']).")";
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$sub_sorters_name[$obj->id] = $obj->value;
						#$sub_sorters[] = $obj->id;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				foreach ($params['ssC'] as $ss_id) {
					$sub_sorters[] = $ss_id;
				}

				$months = ['', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

				#	Определить данные для графиков YTD
				$yearToDayData = [];
				$ytdInd = 0;
				foreach ($sub_sorters as $ss_key) {
					$yearToDayData[$ss_key]['Q1'] = ['Other' => 0];
					$yearToDayData[$ss_key]['Q2'] = ['Other' => 0];
					$yearToDayData[$ss_key]['Q3'] = ['Other' => 0];
					$yearToDayData[$ss_key]['Q4'] = ['Other' => 0];
					$yearToDayData[$ss_key]['Q1O'] = ['Other' => 0];
					$yearToDayData[$ss_key]['Q2O'] = ['Other' => 0];
					$yearToDayData[$ss_key]['Q3O'] = ['Other' => 0];
					$yearToDayData[$ss_key]['Q4O'] = ['Other' => 0];
					$yearToDayData[$ss_key]['Q1Summ'] = 0;
					$yearToDayData[$ss_key]['Q2Summ'] = 0;
					$yearToDayData[$ss_key]['Q3Summ'] = 0;
					$yearToDayData[$ss_key]['Q4Summ'] = 0;
					$yearToDayData[$ss_key]['Q1OSumm'] = 0;
					$yearToDayData[$ss_key]['Q2OSumm'] = 0;
					$yearToDayData[$ss_key]['Q3OSumm'] = 0;
					$yearToDayData[$ss_key]['Q4OSumm'] = 0;
					$yearToDayData[$ss_key]['Q1SummPT3'] = 0;
					$yearToDayData[$ss_key]['Q2SummPT3'] = 0;
					$yearToDayData[$ss_key]['Q3SummPT3'] = 0;
					$yearToDayData[$ss_key]['Q4SummPT3'] = 0;
					$yearToDayData[$ss_key]['Q1OSummPT3'] = 0;
					$yearToDayData[$ss_key]['Q2OSummPT3'] = 0;
					$yearToDayData[$ss_key]['Q3OSummPT3'] = 0;
					$yearToDayData[$ss_key]['Q4OSummPT3'] = 0;
					if ($ss_key) {
						$sorter_filter = " AND column_number_".$params['sCD']." = ".$ss_key;
						$ytdInd++;
						$yearToDayLabels[] = $ytdInd;
						$ytdInd++;
						$yearToDayLabels[] = $ytdInd;
						$ytdInd++;
						$yearToDayLabels[] = $ytdInd;
						$ytdInd++;
						$yearToDayLabels[] = $ytdInd;
					} else {
						if ($params['sos']) {
							$sorter_filter = " AND column_number_".$params['sCD']." IN (".implode(',',$sub_sorters).")";
						} else {
							$sorter_filter = " AND 1";
						}
					}

					$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter.$sorter_filter." AND sorting_date BETWEEN ".$diapazons[7];
					#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							$yearToDayData[$ss_key]['Q4SummPT3'] = $obj->sorter_summ;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
					$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter.$sorter_filter." AND sorting_date BETWEEN ".$diapazons[6];
					#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							$yearToDayData[$ss_key]['Q3SummPT3'] = $obj->sorter_summ;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
					$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter.$sorter_filter." AND sorting_date BETWEEN ".$diapazons[5];
					#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							$yearToDayData[$ss_key]['Q2SummPT3'] = $obj->sorter_summ;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
					$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter.$sorter_filter." AND sorting_date BETWEEN ".$diapazons[4];
					#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							$yearToDayData[$ss_key]['Q1SummPT3'] = $obj->sorter_summ;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
					$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter.$sorter_filter." AND sorting_date BETWEEN ".$diapazons[3];
					#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							$yearToDayData[$ss_key]['Q4OSummPT3'] = $obj->sorter_summ;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
					$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter.$sorter_filter." AND sorting_date BETWEEN ".$diapazons[2];
					#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							$yearToDayData[$ss_key]['Q3OSummPT3'] = $obj->sorter_summ;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
					$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter.$sorter_filter." AND sorting_date BETWEEN ".$diapazons[1];
					#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							$yearToDayData[$ss_key]['Q2OSummPT3'] = $obj->sorter_summ;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
					$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter.$sorter_filter." AND sorting_date BETWEEN ".$diapazons[0];
					#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
					if ($result = $conn->query($sql)) {
						while ($obj = $result->fetch_object()) {
							$yearToDayData[$ss_key]['Q1OSummPT3'] = $obj->sorter_summ;
						}
					}
						else {
							die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
						}
					if ($params['sI']) {
						$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".$diapazons[7]." GROUP BY column_number_".$params['sI'];
						#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								if (array_key_exists($obj->sorter_item, $sorters_name)) {
									$yearToDayData[$ss_key]['Q4'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
									$yearToDayData[$ss_key]['Q4Summ'] += $obj->sorter_summ;
								}
									else {
										$yearToDayData[$ss_key]['Q4']['Other'] += $obj->sorter_summ;
										if ($params['oos']) {
											$yearToDayData[$ss_key]['Q4Summ'] += $obj->sorter_summ;
										}
									}
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
						$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".$diapazons[6]." GROUP BY column_number_".$params['sI'];
						#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								if (array_key_exists($obj->sorter_item, $sorters_name)) {
									$yearToDayData[$ss_key]['Q3'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
									$yearToDayData[$ss_key]['Q3Summ'] += $obj->sorter_summ;
								}
									else {
										$yearToDayData[$ss_key]['Q3']['Other'] += $obj->sorter_summ;
										if ($params['oos']) {
											$yearToDayData[$ss_key]['Q3Summ'] += $obj->sorter_summ;
										}
									}
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
						$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".$diapazons[5]." GROUP BY column_number_".$params['sI'];
						#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								if (array_key_exists($obj->sorter_item, $sorters_name)) {
									$yearToDayData[$ss_key]['Q2'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
									$yearToDayData[$ss_key]['Q2Summ'] += $obj->sorter_summ;
								}
									else {
										$yearToDayData[$ss_key]['Q2']['Other'] += $obj->sorter_summ;
										if ($params['oos']) {
											$yearToDayData[$ss_key]['Q2Summ'] += $obj->sorter_summ;
										}
									}
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
						$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".$diapazons[4]." GROUP BY column_number_".$params['sI'];
						#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								if (array_key_exists($obj->sorter_item, $sorters_name)) {
									$yearToDayData[$ss_key]['Q1'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
									$yearToDayData[$ss_key]['Q1Summ'] += $obj->sorter_summ;
								}
									else {
										$yearToDayData[$ss_key]['Q1']['Other'] += $obj->sorter_summ;
										if ($params['oos']) {
											$yearToDayData[$ss_key]['Q1Summ'] += $obj->sorter_summ;
										}
									}
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}

						$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".$diapazons[3]." GROUP BY column_number_".$params['sI'];
						#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								if (array_key_exists($obj->sorter_item, $sorters_name)) {
									$yearToDayData[$ss_key]['Q4O'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
									$yearToDayData[$ss_key]['Q4OSumm'] += $obj->sorter_summ;
								}
									else {
										$yearToDayData[$ss_key]['Q4O']['Other'] += $obj->sorter_summ;
										if ($params['oos']) {
											$yearToDayData[$ss_key]['Q4OSumm'] += $obj->sorter_summ;
										}
									}
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
						$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".$diapazons[2]." GROUP BY column_number_".$params['sI'];
						#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								if (array_key_exists($obj->sorter_item, $sorters_name)) {
									$yearToDayData[$ss_key]['Q3O'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
									$yearToDayData[$ss_key]['Q3OSumm'] += $obj->sorter_summ;
								}
									else {
										$yearToDayData[$ss_key]['Q3O']['Other'] += $obj->sorter_summ;
										if ($params['oos']) {
											$yearToDayData[$ss_key]['Q3OSumm'] += $obj->sorter_summ;
										}
									}
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
						$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".$diapazons[1]." GROUP BY column_number_".$params['sI'];
						#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								if (array_key_exists($obj->sorter_item, $sorters_name)) {
									$yearToDayData[$ss_key]['Q2O'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
									$yearToDayData[$ss_key]['Q2OSumm'] += $obj->sorter_summ;
								}
									else {
										$yearToDayData[$ss_key]['Q2O']['Other'] += $obj->sorter_summ;
										if ($params['oos']) {
											$yearToDayData[$ss_key]['Q2OSumm'] += $obj->sorter_summ;
										}
									}
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
						$sql = "SELECT SUM(".$summing.") AS sorter_summ, column_number_".$params['sI']." AS sorter_item FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".$diapazons[0]." GROUP BY column_number_".$params['sI'];
						#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								if (array_key_exists($obj->sorter_item, $sorters_name)) {
									$yearToDayData[$ss_key]['Q1O'][$sorters_name[$obj->sorter_item]] = $obj->sorter_summ;
									$yearToDayData[$ss_key]['Q1OSumm'] += $obj->sorter_summ;
								}
									else {
										$yearToDayData[$ss_key]['Q1O']['Other'] += $obj->sorter_summ;
										if ($params['oos']) {
											$yearToDayData[$ss_key]['Q1OSumm'] += $obj->sorter_summ;
										}
									}
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
					}
						else {
							foreach ($params['dD'] as $key => $item) {
								$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".$diapazons[7]." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
								#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
								if ($result = $conn->query($sql)) {
									while ($obj = $result->fetch_object()) {
										$yearToDayData[$ss_key]['Q4'][$key] = $obj->sorter_summ;
										$yearToDayData[$ss_key]['Q4Summ'] += $obj->sorter_summ;
									}
								}
									else {
										die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
									}
								$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".$diapazons[6]." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
								#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
								if ($result = $conn->query($sql)) {
									while ($obj = $result->fetch_object()) {
										$yearToDayData[$ss_key]['Q3'][$key] = $obj->sorter_summ;
										$yearToDayData[$ss_key]['Q3Summ'] += $obj->sorter_summ;
									}
								}
									else {
										die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
									}
								$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".$diapazons[5]." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
								#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
								if ($result = $conn->query($sql)) {
									while ($obj = $result->fetch_object()) {
										$yearToDayData[$ss_key]['Q2'][$key] = $obj->sorter_summ;
										$yearToDayData[$ss_key]['Q2Summ'] += $obj->sorter_summ;
									}
								}
									else {
										die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
									}
								$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".$diapazons[4]." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
								#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
								if ($result = $conn->query($sql)) {
									while ($obj = $result->fetch_object()) {
										$yearToDayData[$ss_key]['Q1'][$key] = $obj->sorter_summ;
										$yearToDayData[$ss_key]['Q1Summ'] += $obj->sorter_summ;
									}
								}
									else {
										die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
									}

								$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".$diapazons[3]." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
								#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
								if ($result = $conn->query($sql)) {
									while ($obj = $result->fetch_object()) {
										$yearToDayData[$ss_key]['Q4O'][$key] = $obj->sorter_summ;
										$yearToDayData[$ss_key]['Q4OSumm'] += $obj->sorter_summ;
									}
								}
									else {
										die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
									}
								$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".$diapazons[2]." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
								#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
								if ($result = $conn->query($sql)) {
									while ($obj = $result->fetch_object()) {
										$yearToDayData[$ss_key]['Q3O'][$key] = $obj->sorter_summ;
										$yearToDayData[$ss_key]['Q3OSumm'] += $obj->sorter_summ;
									}
								}
									else {
										die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
									}
								$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".$diapazons[1]." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
								#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
								if ($result = $conn->query($sql)) {
									while ($obj = $result->fetch_object()) {
										$yearToDayData[$ss_key]['Q2O'][$key] = $obj->sorter_summ;
										$yearToDayData[$ss_key]['Q2OSumm'] += $obj->sorter_summ;
									}
								}
									else {
										die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
									}
								$sql = "SELECT SUM(".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table WHERE".$filters_query.$sorter_filter." AND sorting_date BETWEEN ".$diapazons[0]." AND (IF(count > 0, (".$summing."/count), 0) >= ".$item['sS'].") AND (IF(count > 0, (".$summing."/count), 0) <= ".$item['eS'].")";
								#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
								if ($result = $conn->query($sql)) {
									while ($obj = $result->fetch_object()) {
										$yearToDayData[$ss_key]['Q1O'][$key] = $obj->sorter_summ;
										$yearToDayData[$ss_key]['Q1OSumm'] += $obj->sorter_summ;
									}
								}
									else {
										die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
									}
							}
						}


					if (($other_summ > 0) && ($sorters_name[0] != 'Other') && ($params['oos'])) {
						$sorters[] = 0;
						$sorters_name[0] = 'Other';
					}
					#	Подсчет процентного выражения и приведение величин к масштабу миллиардов
					foreach ($sorters_name as $key) {
						#	Проценты
						$yearToDayData[$ss_key]['Q1Perc'][$key] = (($yearToDayData[$ss_key]['Q1Summ'] != 0) ? (round($yearToDayData[$ss_key]['Q1'][$key] / $yearToDayData[$ss_key]['Q1Summ'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['Q2Perc'][$key] = (($yearToDayData[$ss_key]['Q2Summ'] != 0) ? (round($yearToDayData[$ss_key]['Q2'][$key] / $yearToDayData[$ss_key]['Q2Summ'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['Q3Perc'][$key] = (($yearToDayData[$ss_key]['Q3Summ'] != 0) ? (round($yearToDayData[$ss_key]['Q3'][$key] / $yearToDayData[$ss_key]['Q3Summ'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['Q4Perc'][$key] = (($yearToDayData[$ss_key]['Q4Summ'] != 0) ? (round($yearToDayData[$ss_key]['Q4'][$key] / $yearToDayData[$ss_key]['Q4Summ'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['Q1OPerc'][$key] = (($yearToDayData[$ss_key]['Q1OSumm'] != 0) ? (round($yearToDayData[$ss_key]['Q1O'][$key] / $yearToDayData[$ss_key]['Q1OSumm'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['Q2OPerc'][$key] = (($yearToDayData[$ss_key]['Q2OSumm'] != 0) ? (round($yearToDayData[$ss_key]['Q2O'][$key] / $yearToDayData[$ss_key]['Q2OSumm'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['Q3OPerc'][$key] = (($yearToDayData[$ss_key]['Q3OSumm'] != 0) ? (round($yearToDayData[$ss_key]['Q3O'][$key] / $yearToDayData[$ss_key]['Q3OSumm'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['Q4OPerc'][$key] = (($yearToDayData[$ss_key]['Q4OSumm'] != 0) ? (round($yearToDayData[$ss_key]['Q4O'][$key] / $yearToDayData[$ss_key]['Q4OSumm'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['Q1PercPT3'][$key] = (($yearToDayData[$ss_key]['Q1SummPT3'] != 0) ? (round($yearToDayData[$ss_key]['Q1'][$key] / $yearToDayData[$ss_key]['Q1SummPT3'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['Q2PercPT3'][$key] = (($yearToDayData[$ss_key]['Q2SummPT3'] != 0) ? (round($yearToDayData[$ss_key]['Q2'][$key] / $yearToDayData[$ss_key]['Q2SummPT3'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['Q3PercPT3'][$key] = (($yearToDayData[$ss_key]['Q3SummPT3'] != 0) ? (round($yearToDayData[$ss_key]['Q3'][$key] / $yearToDayData[$ss_key]['Q3SummPT3'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['Q4PercPT3'][$key] = (($yearToDayData[$ss_key]['Q4SummPT3'] != 0) ? (round($yearToDayData[$ss_key]['Q4'][$key] / $yearToDayData[$ss_key]['Q4SummPT3'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['Q1OPercPT3'][$key] = (($yearToDayData[$ss_key]['Q1OSummPT3'] != 0) ? (round($yearToDayData[$ss_key]['Q1O'][$key] / $yearToDayData[$ss_key]['Q1OSummPT3'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['Q2OPercPT3'][$key] = (($yearToDayData[$ss_key]['Q2OSummPT3'] != 0) ? (round($yearToDayData[$ss_key]['Q2O'][$key] / $yearToDayData[$ss_key]['Q2OSummPT3'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['Q3OPercPT3'][$key] = (($yearToDayData[$ss_key]['Q3OSummPT3'] != 0) ? (round($yearToDayData[$ss_key]['Q3O'][$key] / $yearToDayData[$ss_key]['Q3OSummPT3'] * 1000) / 10) : 0.0);
						$yearToDayData[$ss_key]['Q4OPercPT3'][$key] = (($yearToDayData[$ss_key]['Q4OSummPT3'] != 0) ? (round($yearToDayData[$ss_key]['Q4O'][$key] / $yearToDayData[$ss_key]['Q4OSummPT3'] * 1000) / 10) : 0.0);
						if (!($params['sP'])) {
							#	Масштабирование
							$yearToDayData[$ss_key]['Q1'][$key] = round($yearToDayData[$ss_key]['Q1'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['Q2'][$key] = round($yearToDayData[$ss_key]['Q2'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['Q3'][$key] = round($yearToDayData[$ss_key]['Q3'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['Q4'][$key] = round($yearToDayData[$ss_key]['Q4'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['Q1O'][$key] = round($yearToDayData[$ss_key]['Q1O'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['Q2O'][$key] = round($yearToDayData[$ss_key]['Q2O'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['Q3O'][$key] = round($yearToDayData[$ss_key]['Q3O'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['Q4O'][$key] = round($yearToDayData[$ss_key]['Q4O'][$key] / $msscht2) / 10000;
						} else if ($params['sP'] == 1) {
							#	Масштабирование
							$yearToDayData[$ss_key]['Q1'][$key] = round($yearToDayData[$ss_key]['Q1'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['Q2'][$key] = round($yearToDayData[$ss_key]['Q2'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['Q3'][$key] = round($yearToDayData[$ss_key]['Q3'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['Q4'][$key] = round($yearToDayData[$ss_key]['Q4'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['Q1O'][$key] = round($yearToDayData[$ss_key]['Q1O'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['Q2O'][$key] = round($yearToDayData[$ss_key]['Q2O'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['Q3O'][$key] = round($yearToDayData[$ss_key]['Q3O'][$key] / $msscht2) / 10000;
							$yearToDayData[$ss_key]['Q4O'][$key] = round($yearToDayData[$ss_key]['Q4O'][$key] / $msscht2) / 10000;
						}
					}
				}
				$legends = [];
				$sub_names = [];
				foreach ($sub_sorters as $ss_key) {
					$sub_names[] = $sub_sorters_name[$ss_key];
					if ($params['gT'] === '2') {//$params['gT'] === '2'	??? было iI
						$legends['up'][] = $yearToDayData[$ss_key]['Q1SummPT3'];
						$legends['up'][] = $yearToDayData[$ss_key]['Q2SummPT3'];
						$legends['up'][] = $yearToDayData[$ss_key]['Q3SummPT3'];
						$legends['up'][] = $yearToDayData[$ss_key]['Q4SummPT3'];
						$legends['down'][] = (($yearToDayData[$ss_key]['Q1OSummPT3'] != 0) ? (round((($yearToDayData[$ss_key]['Q1SummPT3'] - $yearToDayData[$ss_key]['Q1OSummPT3']) / $yearToDayData[$ss_key]['Q1OSummPT3']) * 1000) / 10) : 0.0);
						$legends['down'][] = (($yearToDayData[$ss_key]['Q2OSummPT3'] != 0) ? (round((($yearToDayData[$ss_key]['Q2SummPT3'] - $yearToDayData[$ss_key]['Q2OSummPT3']) / $yearToDayData[$ss_key]['Q2OSummPT3']) * 1000) / 10) : 0.0);
						$legends['down'][] = (($yearToDayData[$ss_key]['Q3OSummPT3'] != 0) ? (round((($yearToDayData[$ss_key]['Q3SummPT3'] - $yearToDayData[$ss_key]['Q3OSummPT3']) / $yearToDayData[$ss_key]['Q3OSummPT3']) * 1000) / 10) : 0.0);
						$legends['down'][] = (($yearToDayData[$ss_key]['Q4OSummPT3'] != 0) ? (round((($yearToDayData[$ss_key]['Q4SummPT3'] - $yearToDayData[$ss_key]['Q4OSummPT3']) / $yearToDayData[$ss_key]['Q4OSummPT3']) * 1000) / 10) : 0.0);
						$legends['color'][] = (($yearToDayData[$ss_key]['Q1SummPT3'] < $yearToDayData[$ss_key]['Q1OSummPT3']) ? 'Red' : 'Green');
						$legends['color'][] = (($yearToDayData[$ss_key]['Q2SummPT3'] < $yearToDayData[$ss_key]['Q2OSummPT3']) ? 'Red' : 'Green');
						$legends['color'][] = (($yearToDayData[$ss_key]['Q3SummPT3'] < $yearToDayData[$ss_key]['Q3OSummPT3']) ? 'Red' : 'Green');
						$legends['color'][] = (($yearToDayData[$ss_key]['Q4SummPT3'] < $yearToDayData[$ss_key]['Q4OSummPT3']) ? 'Red' : 'Green');
					} else {
						$legends['up'][] = $yearToDayData[$ss_key]['Q1Summ'];
						$legends['up'][] = $yearToDayData[$ss_key]['Q2Summ'];
						$legends['up'][] = $yearToDayData[$ss_key]['Q3Summ'];
						$legends['up'][] = $yearToDayData[$ss_key]['Q4Summ'];
						$legends['down'][] = (($yearToDayData[$ss_key]['Q1OSumm'] != 0) ? (round((($yearToDayData[$ss_key]['Q1Summ'] - $yearToDayData[$ss_key]['Q1OSumm']) / $yearToDayData[$ss_key]['Q1OSumm']) * 1000) / 10) : 0.0);
						$legends['down'][] = (($yearToDayData[$ss_key]['Q2OSumm'] != 0) ? (round((($yearToDayData[$ss_key]['Q2Summ'] - $yearToDayData[$ss_key]['Q2OSumm']) / $yearToDayData[$ss_key]['Q2OSumm']) * 1000) / 10) : 0.0);
						$legends['down'][] = (($yearToDayData[$ss_key]['Q3OSumm'] != 0) ? (round((($yearToDayData[$ss_key]['Q3Summ'] - $yearToDayData[$ss_key]['Q3OSumm']) / $yearToDayData[$ss_key]['Q3OSumm']) * 1000) / 10) : 0.0);
						$legends['down'][] = (($yearToDayData[$ss_key]['Q4OSumm'] != 0) ? (round((($yearToDayData[$ss_key]['Q4Summ'] - $yearToDayData[$ss_key]['Q4OSumm']) / $yearToDayData[$ss_key]['Q4OSumm']) * 1000) / 10) : 0.0);
						$legends['color'][] = (($yearToDayData[$ss_key]['Q1Summ'] < $yearToDayData[$ss_key]['Q1OSumm']) ? 'Red' : 'Green');
						$legends['color'][] = (($yearToDayData[$ss_key]['Q2Summ'] < $yearToDayData[$ss_key]['Q2OSumm']) ? 'Red' : 'Green');
						$legends['color'][] = (($yearToDayData[$ss_key]['Q3Summ'] < $yearToDayData[$ss_key]['Q3OSumm']) ? 'Red' : 'Green');
						$legends['color'][] = (($yearToDayData[$ss_key]['Q4Summ'] < $yearToDayData[$ss_key]['Q4OSumm']) ? 'Red' : 'Green');
					}
					if (!($ss_key)) {
						$legends['up'][] = '';
						$legends['down'][] = '';
						$legends['color'][] = 'Black';
					}
				}
				#	Формирование итогового ответа
				$results = [];
				$names = [];
				foreach ($sorters as $key) {
					$sub_res = [];
					foreach ($sub_sorters as $ss_key) {
						$sub_res[] = '{"idv":"'.$yearToDayData[$ss_key]['Q1'][$sorters_name[$key]].'","idppt3":"'.$yearToDayData[$ss_key]['Q1Perc'][$sorters_name[$key]].'","lbl":"'.$sub_sorters_name[$ss_key].'"}';
						$sub_res[] = '{"idv":"'.$yearToDayData[$ss_key]['Q2'][$sorters_name[$key]].'","idppt3":"'.$yearToDayData[$ss_key]['Q2Perc'][$sorters_name[$key]].'","lbl":"'.$sub_sorters_name[$ss_key].'"}';
						$sub_res[] = '{"idv":"'.$yearToDayData[$ss_key]['Q3'][$sorters_name[$key]].'","idppt3":"'.$yearToDayData[$ss_key]['Q3Perc'][$sorters_name[$key]].'","lbl":"'.$sub_sorters_name[$ss_key].'"}';
						$sub_res[] = '{"idv":"'.$yearToDayData[$ss_key]['Q4'][$sorters_name[$key]].'","idppt3":"'.$yearToDayData[$ss_key]['Q4Perc'][$sorters_name[$key]].'","lbl":"'.$sub_sorters_name[$ss_key].'"}';
						if (!($ss_key)) {
							$sub_res[] =  '{"idv":"","idppt3":"","lbl":""}';
						}
					}
					$result = '{"iD":['.implode(',',$sub_res).']}';
					$results[] = $result;
					$names[] = $sorters_name[$key];
				}

				echo '{"result":1,"slide_legend":"'.$slide_legend.'","data":{"ss":['.implode(',',$sorters).'],"sBs":['.implode(',',$sub_sorters).'],"sNs":["'.implode('","',$names).'"],"sBNs":["'.implode('","',$sub_names).'"],"rs":['.implode(',',$results).'],"lUs":["'.implode('","',$legends['up']).'"],"lDs":["'.implode('","',$legends['down']).'"],"lCs":["'.implode('","',$legends['color']).'"],"lYs":["'.implode('","',$yearToDayLabels).'"]}}';
			}
			else if ($page == 15) {
				#	Определить столбец "ответственный" за количества единиц
				$sql = "SELECT id FROM db_".$dbase."_xls_for_diagrams_columns WHERE value = 'Сумма Упаковок'";
				$sum_upak_col_number = 0;
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$sum_upak_col_number = $obj->id;
						break;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}

				$filters_queries = [];

				foreach ($params['fD'] as $item) {
					$filters_queries[] = " t.column_number_".$item['cI']." IN (".implode(', ',$item['vI']).")";
				}
				if ($filters_queries) {
					$filters_query = implode(" AND", $filters_queries);
					$main_filter = $filters_queries[0];
				}
					else {
						$filters_query = " 1";
						$main_filter = " 1";
					}

				$date_pieces = explode('-', $_POST['edate']);
				$year = $date_pieces[0];
				$month = $date_pieces[1];
				$day = $date_pieces[2];

				$months = ['', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

				#	Определить данные для графиков MTM
				$monthToMonthData = [];
				$monthToMonthLabels = [
					((($month + 1) > 12) ? "Jan".substr(($year - 1), -2) : $months[(int)$month + 1]."".substr(($year - 2), -2))."-".$months[(int)$month]."".substr(($year - 1), -2),
					((($month + 1) > 12) ? "Jan".substr($year, -2) : $months[(int)$month + 1]."".substr(($year - 1), -2))."-".$months[(int)$month]."".substr($year, -2)
				];
				$titlemtm0 = $monthToMonthLabels[0];
				$titlemtm1 = $monthToMonthLabels[1];
				$sql = "SELECT SUM(t.".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table AS t  WHERE".$filters_query." AND t.sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-0) : ($year-1)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-0), $month, "31");
				#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$monthToMonthData['currYSum'] = $obj->sorter_summ;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				$sql = "SELECT SUM(t.".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table AS t WHERE".$filters_query." AND t.sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-1) : ($year-2)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-1), $month, "31");
				#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$monthToMonthData['prevYSum'] = $obj->sorter_summ;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				$sql = "SELECT SUM(t.".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table AS t WHERE".$filters_query." AND t.sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-2) : ($year-3)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-2), $month, "31");
				#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$monthToMonthData['lastYSum'] = $obj->sorter_summ;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}

				$sql = "SELECT SUM(u.value) AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table AS t JOIN db_".$dbase."_xls_for_diagrams_unique_values AS u ON (u.id = t.column_number_".$sum_upak_col_number.") WHERE".$filters_query." AND t.sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-0) : ($year-1)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-0), $month, "31");
				#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$monthToMonthData['currYQty'] = $obj->sorter_summ;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				$sql = "SELECT SUM(u.value) AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table AS t JOIN db_".$dbase."_xls_for_diagrams_unique_values AS u ON (u.id = t.column_number_".$sum_upak_col_number.") WHERE".$filters_query." AND t.sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-1) : ($year-2)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-1), $month, "31");
				#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$monthToMonthData['prevYQty'] = $obj->sorter_summ;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				$sql = "SELECT SUM(u.value) AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table AS t JOIN db_".$dbase."_xls_for_diagrams_unique_values AS u ON (u.id = t.column_number_".$sum_upak_col_number.") WHERE".$filters_query." AND t.sorting_date BETWEEN ".sprintf("%04d%02d%02d", ((($month+1) > 12) ? ($year-2) : ($year-3)), ((($month+1) > 12) ? "01" : ($month+1)), "01")." AND ".sprintf("%04d%02d%02d", ($year-2), $month, "31");
				#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$monthToMonthData['lastYQty'] = $obj->sorter_summ;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				$monthToMonthData['currYPrc'] = (($monthToMonthData['currYQty'] != 0) ? (round($monthToMonthData['currYSum'] / $monthToMonthData['currYQty'] * 100) / 100) : 0.0);
				$monthToMonthData['prevYPrc'] = (($monthToMonthData['prevYQty'] != 0) ? (round($monthToMonthData['prevYSum'] / $monthToMonthData['prevYQty'] * 100) / 100) : 0.0);
				$monthToMonthData['lastYPrc'] = (($monthToMonthData['lastYQty'] != 0) ? (round($monthToMonthData['lastYSum'] / $monthToMonthData['lastYQty'] * 100) / 100) : 0.0);

				#	Определить данные для графиков YTD
				$yearToDayData = [];
				$yearToDayLabels = [
					"Jan".substr(($year - 1), -2)."-".$months[(int)$month]."".substr(($year - 1), -2),
					"Jan".substr($year, -2)."-".$months[(int)$month]."".substr($year, -2)
					#"YTD".substr(($year - 1), -2)." ".$months[$month],
					#"YTD".substr($year, -2)." ".$months[$month]
				];
				$titleytd0 = $yearToDayLabels[0];
				$titleytd1 = $yearToDayLabels[1];
				$sql = "SELECT SUM(t.".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table AS t WHERE".$filters_query." AND t.sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-0), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-0), $month, $day);
				#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$yearToDayData['currYSum'] = $obj->sorter_summ;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				$sql = "SELECT SUM(t.".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table AS t WHERE".$filters_query." AND t.sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-1), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-1), $month, $day);
				#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$yearToDayData['prevYSum'] = $obj->sorter_summ;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				$sql = "SELECT SUM(t.".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table AS t WHERE".$filters_query." AND t.sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-2), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-2), $month, $day);
				#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$yearToDayData['lastYSum'] = $obj->sorter_summ;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}

				$sql = "SELECT SUM(u.value) AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table AS t JOIN db_".$dbase."_xls_for_diagrams_unique_values AS u ON (u.id = t.column_number_".$sum_upak_col_number.") WHERE".$filters_query." AND t.sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-0), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-0), $month, $day);
				#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$yearToDayData['currYQty'] = $obj->sorter_summ;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				$sql = "SELECT SUM(u.value) AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table AS t JOIN db_".$dbase."_xls_for_diagrams_unique_values AS u ON (u.id = t.column_number_".$sum_upak_col_number.") WHERE".$filters_query." AND t.sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-1), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-1), $month, $day);
				#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$yearToDayData['prevYQty'] = $obj->sorter_summ;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				$sql = "SELECT SUM(u.value) AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table AS t JOIN db_".$dbase."_xls_for_diagrams_unique_values AS u ON (u.id = t.column_number_".$sum_upak_col_number.") WHERE".$filters_query." AND t.sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($year-2), "01", "01")." AND ".sprintf("%04d%02d%02d", ($year-2), $month, $day);
				#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
				if ($result = $conn->query($sql)) {
					while ($obj = $result->fetch_object()) {
						$yearToDayData['lastYQty'] = $obj->sorter_summ;
					}
				}
					else {
						die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
					}
				$yearToDayData['currYPrc'] = (($yearToDayData['currYQty'] != 0) ? (round($yearToDayData['currYSum'] / $yearToDayData['currYQty'] * 100) / 100) : 0.0);
				$yearToDayData['prevYPrc'] = (($yearToDayData['prevYQty'] != 0) ? (round($yearToDayData['prevYSum'] / $yearToDayData['prevYQty'] * 100) / 100) : 0.0);
				$yearToDayData['lastYPrc'] = (($yearToDayData['lastYQty'] != 0) ? (round($yearToDayData['lastYSum'] / $yearToDayData['lastYQty'] * 100) / 100) : 0.0);

				#	Определить данные для графиков intervals
				$intervalsData = [];
				$intervalsNames = [];
				if ($params['iI'] == 1) {
					$numberOfSeriee = -1;
					$countOfSeries = 14;
					for ($i = 0; $i < 26; $i++) {
						$numberOfSeriee++;
						$cMonth = $month - $i;
						$cYear = $year;
						while ($cMonth < 1) {
							$cMonth += 12;
							$cYear--;
						}
						if ($i) {
							$cDay = '31';
						}
							else {
								$cDay = $day;
								if ($cDay < 10) {
									$cDay = '0'.(int)$cDay;
								}
							}
						if ($cMonth < 10) {
							$cMonth = '0'.(int)$cMonth;
						}
						$intervalsNames[$numberOfSeriee] = $months[(int)$cMonth]."".substr($cYear, -2);
						$sql = "SELECT SUM(t.".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table AS t WHERE".$filters_query." AND t.sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($cYear-0), $cMonth, "01")." AND ".sprintf("%04d%02d%02d", ($cYear-0), $cMonth, $cDay);
						#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								$intervalsData[$intervalsNames[$numberOfSeriee].'Sum'] = $obj->sorter_summ;
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
						$sql = "SELECT SUM(u.value) AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table AS t JOIN db_".$dbase."_xls_for_diagrams_unique_values AS u ON (u.id = t.column_number_".$sum_upak_col_number.") WHERE".$filters_query." AND t.sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($cYear-0), $cMonth, "01")." AND ".sprintf("%04d%02d%02d", ($cYear-0), $cMonth, $cDay);
						#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								$intervalsData[$intervalsNames[$numberOfSeriee].'Qty'] = $obj->sorter_summ;
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
						$intervalsData[$intervalsNames[$numberOfSeriee].'Prc'] = (($intervalsData[$intervalsNames[$numberOfSeriee].'Qty'] != 0) ? (round($intervalsData[$intervalsNames[$numberOfSeriee].'Sum'] / $intervalsData[$intervalsNames[$numberOfSeriee].'Qty'] * 100) / 100) : 0.0);
					}
				}
					else if ($params['iI'] == 2) {
						$sql = "SELECT YEAR(sorting_date) AS min_year FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter." ORDER BY sorting_date ASC LIMIT 0, 1";
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								$min_year = $obj->min_year;
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
						$sql = "SELECT YEAR(sorting_date) AS max_year FROM db_".$dbase."_xls_for_diagrams_table WHERE".$main_filter." ORDER BY sorting_date DESC LIMIT 0, 1";
						if ($result = $conn->query($sql)) {
							while ($obj = $result->fetch_object()) {
								$max_year = $obj->max_year;
							}
						}
							else {
								die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
							}
						$numberOfSeriee = -1;
						$countOfSeries = $max_year - $min_year + 1;
						for ($i = 0; $i < $countOfSeries; $i++) {
							$numberOfSeriee++;
							$cYear = ($max_year - $i);
							$intervalsNames[$numberOfSeriee] = (string)$cYear;
							$sql = "SELECT SUM(t.".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table AS t WHERE".$filters_query." AND t.sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($cYear-0), "01", "01")." AND ".sprintf("%04d%02d%02d", ($cYear-0), "12", "31");
							#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
							if ($result = $conn->query($sql)) {
								while ($obj = $result->fetch_object()) {
									$intervalsData[$intervalsNames[$numberOfSeriee].'Sum'] = $obj->sorter_summ;
								}
							}
								else {
									die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
								}
							$sql = "SELECT SUM(u.value) AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table AS t JOIN db_".$dbase."_xls_for_diagrams_unique_values AS u ON (u.id = t.column_number_".$sum_upak_col_number.") WHERE".$filters_query." AND t.sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($cYear-0), "01", "01")." AND ".sprintf("%04d%02d%02d", ($cYear-0), "12", "31");
							#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
							if ($result = $conn->query($sql)) {
								while ($obj = $result->fetch_object()) {
									$intervalsData[$intervalsNames[$numberOfSeriee].'Qty'] = $obj->sorter_summ;
								}
							}
								else {
									die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
								}
							$intervalsData[$intervalsNames[$numberOfSeriee].'Prc'] = (($intervalsData[$intervalsNames[$numberOfSeriee].'Qty'] != 0) ? (round($intervalsData[$intervalsNames[$numberOfSeriee].'Sum'] / $intervalsData[$intervalsNames[$numberOfSeriee].'Qty'] * 100) / 100) : 0.0);
						}
					}
					else {
						$numberOfSeriee = -1;
						$cYear = $year;
						$monthDiv = floor(($month-1)/3);
						for ($i = 0; $i <= $monthDiv; $i++) {
							$numberOfSeriee++;
							$cMonthBegin = 1 + ($monthDiv - $i) * 3;
							if ($i) {
								$cDay = '31';
								$cMonthEnd = $cMonthBegin + 2;
							}
								else {
									$cMonthEnd = $month;
									$cDay = $day;
									if ($cDay < 10) {
										$cDay = '0'.(int)$cDay;
									}
								}
							if ($cMonthEnd < 10) {
								$cMonthBegin = '0'.(int)$cMonthBegin;
								$cMonthEnd = '0'.(int)$cMonthEnd;
							}
							$intervalsNames[$numberOfSeriee] = $cYear." Q".($monthDiv - $i + 1);
							$sql = "SELECT SUM(t.".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table AS t WHERE".$filters_query." AND t.sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($cYear-0), $cMonthBegin, "01")." AND ".sprintf("%04d%02d%02d", ($cYear-0), $cMonthEnd, $cDay);
							#BETWEEN ".sprintf("%04d%02d%02d", ($cYear-0), $cMonthBegin, "01")." AND ".sprintf("%04d%02d%02d", ($cYear-0), $cMonthEnd, $cDay)
							if ($result = $conn->query($sql)) {
								while ($obj = $result->fetch_object()) {
									$intervalsData[$intervalsNames[$numberOfSeriee].'Sum'] = $obj->sorter_summ;
								}
							}
								else {
									die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
								}
							$sql = "SELECT SUM(u.value) AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table AS t JOIN db_".$dbase."_xls_for_diagrams_unique_values AS u ON (u.id = t.column_number_".$sum_upak_col_number.") WHERE".$filters_query." AND t.sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($cYear-0), $cMonthBegin, "01")." AND ".sprintf("%04d%02d%02d", ($cYear-0), $cMonthEnd, $cDay);
							#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
							if ($result = $conn->query($sql)) {
								while ($obj = $result->fetch_object()) {
									$intervalsData[$intervalsNames[$numberOfSeriee].'Qty'] = $obj->sorter_summ;
								}
							}
								else {
									die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
								}
							$intervalsData[$intervalsNames[$numberOfSeriee].'Prc'] = (($intervalsData[$intervalsNames[$numberOfSeriee].'Qty'] != 0) ? (round($intervalsData[$intervalsNames[$numberOfSeriee].'Sum'] / $intervalsData[$intervalsNames[$numberOfSeriee].'Qty'] * 100) / 100) : 0.0);
						}
						$countOfSeries = 5 + $numberOfSeriee;
						for ($i = 1; $i < 3; $i++) {
							$cYear--;
							for ($j = 0; $j < 4; $j++) {
								$numberOfSeriee++;
								$cMonthBegin = 10 - 3 * $j;
								$cMonthEnd = $cMonthBegin + 2;
								if ($cMonthBegin < 10) {
									$cMonthBegin = '0'.(int)$cMonthBegin;
									$cMonthEnd = '0'.(int)$cMonthEnd;
								}
								$intervalsNames[$numberOfSeriee] = $cYear." Q".(4 - $j);
								$sql = "SELECT SUM(t.".$summing.") AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table AS t WHERE".$filters_query." AND t.sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($cYear-0), $cMonthBegin, "01")." AND ".sprintf("%04d%02d%02d", ($cYear-0), $cMonthEnd, "31");
								#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
								if ($result = $conn->query($sql)) {
									while ($obj = $result->fetch_object()) {
										$intervalsData[$intervalsNames[$numberOfSeriee].'Sum'] = $obj->sorter_summ;
									}
								}
									else {
										die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
									}
								$sql = "SELECT SUM(u.value) AS sorter_summ FROM db_".$dbase."_xls_for_diagrams_table AS t JOIN db_".$dbase."_xls_for_diagrams_unique_values AS u ON (u.id = t.column_number_".$sum_upak_col_number.") WHERE".$filters_query." AND t.sorting_date BETWEEN ".sprintf("%04d%02d%02d", ($cYear-0), $cMonthBegin, "01")." AND ".sprintf("%04d%02d%02d", ($cYear-0), $cMonthEnd, "31");
								#BETWEEN ".sprintf("%04d%02d%02d", $year, $month, $day)." AND ".sprintf("%04d%02d%02d", $year, $month, $day)
								if ($result = $conn->query($sql)) {
									while ($obj = $result->fetch_object()) {
										$intervalsData[$intervalsNames[$numberOfSeriee].'Qty'] = $obj->sorter_summ;
									}
								}
									else {
										die('<p style="color:red">Ошибка: '.$sql.'</br>'.$conn->error.'</p>');
									}
								$intervalsData[$intervalsNames[$numberOfSeriee].'Prc'] = (($intervalsData[$intervalsNames[$numberOfSeriee].'Qty'] != 0) ? (round($intervalsData[$intervalsNames[$numberOfSeriee].'Sum'] / $intervalsData[$intervalsNames[$numberOfSeriee].'Qty'] * 100) / 100) : 0.0);
							}
						}
					}

				#	Подсчет процентного выражения
				$monthToMonthData['currYSumPerc'] = (($monthToMonthData['prevYSum'] != 0) ? (round(($monthToMonthData['currYSum'] - $monthToMonthData['prevYSum']) / $monthToMonthData['prevYSum'] * 1000) / 10) : 0.0);
				$monthToMonthData['currYQtyPerc'] = (($monthToMonthData['prevYQty'] != 0) ? (round(($monthToMonthData['currYQty'] - $monthToMonthData['prevYQty']) / $monthToMonthData['prevYQty'] * 1000) / 10) : 0.0);
				$monthToMonthData['currYPrcPerc'] = (($monthToMonthData['prevYPrc'] != 0) ? (round(($monthToMonthData['currYPrc'] - $monthToMonthData['prevYPrc']) / $monthToMonthData['prevYPrc'] * 1000) / 10) : 0.0);
				$monthToMonthData['prevYSumPerc'] = (($monthToMonthData['lastYSum'] != 0) ? (round(($monthToMonthData['prevYSum'] - $monthToMonthData['lastYSum']) / $monthToMonthData['lastYSum'] * 1000) / 10) : 0.0);
				$monthToMonthData['prevYQtyPerc'] = (($monthToMonthData['lastYQty'] != 0) ? (round(($monthToMonthData['prevYQty'] - $monthToMonthData['lastYQty']) / $monthToMonthData['lastYQty'] * 1000) / 10) : 0.0);
				$monthToMonthData['prevYPrcPerc'] = (($monthToMonthData['lastYPrc'] != 0) ? (round(($monthToMonthData['prevYPrc'] - $monthToMonthData['lastYPrc']) / $monthToMonthData['lastYPrc'] * 1000) / 10) : 0.0);
				$yearToDayData['currYSumPerc'] = (($yearToDayData['prevYSum'] != 0) ? (round(($yearToDayData['currYSum'] - $yearToDayData['prevYSum']) / $yearToDayData['prevYSum'] * 1000) / 10) : 0.0);
				$yearToDayData['currYQtyPerc'] = (($yearToDayData['prevYQty'] != 0) ? (round(($yearToDayData['currYQty'] - $yearToDayData['prevYQty']) / $yearToDayData['prevYQty'] * 1000) / 10) : 0.0);
				$yearToDayData['currYPrcPerc'] = (($yearToDayData['prevYPrc'] != 0) ? (round(($yearToDayData['currYPrc'] - $yearToDayData['prevYPrc']) / $yearToDayData['prevYPrc'] * 1000) / 10) : 0.0);
				$yearToDayData['prevYSumPerc'] = (($yearToDayData['lastYSum'] != 0) ? (round(($yearToDayData['prevYSum'] - $yearToDayData['lastYSum']) / $yearToDayData['lastYSum'] * 1000) / 10) : 0.0);
				$yearToDayData['prevYQtyPerc'] = (($yearToDayData['lastYQty'] != 0) ? (round(($yearToDayData['prevYQty'] - $yearToDayData['lastYQty']) / $yearToDayData['lastYQty'] * 1000) / 10) : 0.0);
				$yearToDayData['prevYPrcPerc'] = (($yearToDayData['lastYPrc'] != 0) ? (round(($yearToDayData['prevYPrc'] - $yearToDayData['lastYPrc']) / $yearToDayData['lastYPrc'] * 1000) / 10) : 0.0);
				for ($i = ($countOfSeries - 1); $i > -1; $i--) {
					if ($params['iI'] == 1) {
						$intervalsData[$intervalsNames[$i].'SumPerc'] = (($intervalsData[$intervalsNames[$i + 12].'Sum'] != 0) ? (round((($intervalsData[$intervalsNames[$i].'Sum'] - $intervalsData[$intervalsNames[$i + 12].'Sum']) / $intervalsData[$intervalsNames[$i + 12].'Sum']) * 1000) / 10) : 100.0);
						$intervalsData[$intervalsNames[$i].'QtyPerc'] = (($intervalsData[$intervalsNames[$i + 12].'Qty'] != 0) ? (round((($intervalsData[$intervalsNames[$i].'Qty'] - $intervalsData[$intervalsNames[$i + 12].'Qty']) / $intervalsData[$intervalsNames[$i + 12].'Qty']) * 1000) / 10) : 100.0);
						$intervalsData[$intervalsNames[$i].'PrcPerc'] = (($intervalsData[$intervalsNames[$i + 12].'Prc'] != 0) ? (round((($intervalsData[$intervalsNames[$i].'Prc'] - $intervalsData[$intervalsNames[$i + 12].'Prc']) / $intervalsData[$intervalsNames[$i + 12].'Prc']) * 1000) / 10) : 100.0);
					} else if ($params['iI'] == 2) {
						if ($i < ($countOfSeries - 1)) {
							$intervalsData[$intervalsNames[$i].'SumPerc'] = (($intervalsData[$intervalsNames[$i + 1].'Sum'] != 0) ? (round((($intervalsData[$intervalsNames[$i].'Sum'] - $intervalsData[$intervalsNames[$i + 1].'Sum']) / $intervalsData[$intervalsNames[$i + 1].'Sum']) * 1000) / 10) : 100.0);
							$intervalsData[$intervalsNames[$i].'QtyPerc'] = (($intervalsData[$intervalsNames[$i + 1].'Qty'] != 0) ? (round((($intervalsData[$intervalsNames[$i].'Qty'] - $intervalsData[$intervalsNames[$i + 1].'Qty']) / $intervalsData[$intervalsNames[$i + 1].'Qty']) * 1000) / 10) : 100.0);
							$intervalsData[$intervalsNames[$i].'PrcPerc'] = (($intervalsData[$intervalsNames[$i + 1].'Prc'] != 0) ? (round((($intervalsData[$intervalsNames[$i].'Prc'] - $intervalsData[$intervalsNames[$i + 1].'Prc']) / $intervalsData[$intervalsNames[$i + 1].'Prc']) * 1000) / 10) : 100.0);
						} else {
							$intervalsData[$intervalsNames[$i].'SumPerc'] = 100.0;
							$intervalsData[$intervalsNames[$i].'QtyPerc'] = 100.0;
							$intervalsData[$intervalsNames[$i].'PrcPerc'] = 100.0;
						}
					} else {
						$intervalsData[$intervalsNames[$i].'SumPerc'] = (($intervalsData[$intervalsNames[$i + 4].'Sum'] != 0) ? (round((($intervalsData[$intervalsNames[$i].'Sum'] - $intervalsData[$intervalsNames[$i + 4].'Sum']) / $intervalsData[$intervalsNames[$i + 4].'Sum']) * 1000) / 10) : 100.0);
						$intervalsData[$intervalsNames[$i].'QtyPerc'] = (($intervalsData[$intervalsNames[$i + 4].'Qty'] != 0) ? (round((($intervalsData[$intervalsNames[$i].'Qty'] - $intervalsData[$intervalsNames[$i + 4].'Qty']) / $intervalsData[$intervalsNames[$i + 4].'Qty']) * 1000) / 10) : 100.0);
						$intervalsData[$intervalsNames[$i].'PrcPerc'] = (($intervalsData[$intervalsNames[$i + 4].'Prc'] != 0) ? (round((($intervalsData[$intervalsNames[$i].'Prc'] - $intervalsData[$intervalsNames[$i + 4].'Prc']) / $intervalsData[$intervalsNames[$i + 4].'Prc']) * 1000) / 10) : 100.0);
					}
				}

				#	Формирование итогового ответа
				$data_0_m = [];
				$data_0_p = [];
				$data_1_m = [];
				$data_1_p = [];
				$data_2_m = [];
				$data_2_p = [];
				$sorters = [0,0,0,0,0];
				$names = ['',$monthToMonthLabels[0],$monthToMonthLabels[1],$yearToDayLabels[0],$yearToDayLabels[1],''];
				if ($monthToMonthData['prevYSumPerc'] < 0) {
					$data_0_m[] = $monthToMonthData['prevYSumPerc'];
					$data_0_p[] = 0;
				} else if ($monthToMonthData['prevYSumPerc'] > 0) {
					$data_0_p[] = $monthToMonthData['prevYSumPerc'];
					$data_0_m[] = 0;
				} else {
					$data_0_m[] = 0;
					$data_0_p[] = 0.01;
				}
				if ($monthToMonthData['currYSumPerc'] < 0) {
					$data_0_m[] = $monthToMonthData['currYSumPerc'];
					$data_0_p[] = 0;
				} else if ($monthToMonthData['currYSumPerc'] > 0) {
					$data_0_p[] = $monthToMonthData['currYSumPerc'];
					$data_0_m[] = 0;
				} else {
					$data_0_m[] = 0;
					$data_0_p[] = 0.01;
				}
				if ($monthToMonthData['prevYQtyPerc'] < 0) {
					$data_1_m[] = $monthToMonthData['prevYQtyPerc'];
					$data_1_p[] = 0;
				} else if ($monthToMonthData['prevYQtyPerc'] > 0) {
					$data_1_p[] = $monthToMonthData['prevYQtyPerc'];
					$data_1_m[] = 0;
				} else {
					$data_1_m[] = 0;
					$data_1_p[] = 0.01;
				}
				if ($monthToMonthData['currYQtyPerc'] < 0) {
					$data_1_m[] = $monthToMonthData['currYQtyPerc'];
					$data_1_p[] = 0;
				} else if ($monthToMonthData['currYQtyPerc'] > 0) {
					$data_1_p[] = $monthToMonthData['currYQtyPerc'];
					$data_1_m[] = 0;
				} else {
					$data_1_m[] = 0;
					$data_1_p[] = 0.01;
				}
				if ($monthToMonthData['prevYPrcPerc'] < 0) {
					$data_2_m[] = $monthToMonthData['prevYPrcPerc'];
					$data_2_p[] = 0;
				} else if ($monthToMonthData['prevYPrcPerc'] > 0) {
					$data_2_p[] = $monthToMonthData['prevYPrcPerc'];
					$data_2_m[] = 0;
				} else {
					$data_2_m[] = 0;
					$data_2_p[] = 0.01;
				}
				if ($monthToMonthData['currYPrcPerc'] < 0) {
					$data_2_m[] = $monthToMonthData['currYPrcPerc'];
					$data_2_p[] = 0;
				} else if ($monthToMonthData['currYPrcPerc'] > 0) {
					$data_2_p[] = $monthToMonthData['currYPrcPerc'];
					$data_2_m[] = 0;
				} else {
					$data_2_m[] = 0;
					$data_2_p[] = 0.01;
				}
				if ($yearToDayData['prevYSumPerc'] < 0) {
					$data_0_m[] = $yearToDayData['prevYSumPerc'];
					$data_0_p[] = 0;
				} else if ($yearToDayData['prevYSumPerc'] > 0) {
					$data_0_p[] = $yearToDayData['prevYSumPerc'];
					$data_0_m[] = 0;
				} else {
					$data_0_m[] = 0;
					$data_0_p[] = 0.01;
				}
				if ($yearToDayData['currYSumPerc'] < 0) {
					$data_0_m[] = $yearToDayData['currYSumPerc'];
					$data_0_p[] = 0;
				} else if ($yearToDayData['currYSumPerc'] > 0) {
					$data_0_p[] = $yearToDayData['currYSumPerc'];
					$data_0_m[] = 0;
				} else {
					$data_0_m[] = 0;
					$data_0_p[] = 0.01;
				}
				$data_0_m[] = 0;
				$data_0_p[] = 0;
				if ($yearToDayData['prevYQtyPerc'] < 0) {
					$data_1_m[] = $yearToDayData['prevYQtyPerc'];
					$data_1_p[] = 0;
				} else if ($yearToDayData['prevYQtyPerc'] > 0) {
					$data_1_p[] = $yearToDayData['prevYQtyPerc'];
					$data_1_m[] = 0;
				} else {
					$data_1_m[] = 0;
					$data_1_p[] = 0.01;
				}
				if ($yearToDayData['currYQtyPerc'] < 0) {
					$data_1_m[] = $yearToDayData['currYQtyPerc'];
					$data_1_p[] = 0;
				} else if ($yearToDayData['currYQtyPerc'] > 0) {
					$data_1_p[] = $yearToDayData['currYQtyPerc'];
					$data_1_m[] = 0;
				} else {
					$data_1_m[] = 0;
					$data_1_p[] = 0.01;
				}
				$data_1_m[] = 0;
				$data_1_p[] = 0;
				if ($yearToDayData['prevYPrcPerc'] < 0) {
					$data_2_m[] = $yearToDayData['prevYPrcPerc'];
					$data_2_p[] = 0;
				} else if ($yearToDayData['prevYPrcPerc'] > 0) {
					$data_2_p[] = $yearToDayData['prevYPrcPerc'];
					$data_2_m[] = 0;
				} else {
					$data_2_m[] = 0;
					$data_2_p[] = 0.01;
				}
				if ($yearToDayData['currYPrcPerc'] < 0) {
					$data_2_m[] = $yearToDayData['currYPrcPerc'];
					$data_2_p[] = 0;
				} else if ($yearToDayData['currYPrcPerc'] > 0) {
					$data_2_p[] = $yearToDayData['currYPrcPerc'];
					$data_2_m[] = 0;
				} else {
					$data_2_m[] = 0;
					$data_2_p[] = 0.01;
				}
				$data_2_m[] = 0;
				$data_2_p[] = 0;

				#	Масштабирование суммарных данных
				if ($params['sP'] < 2) {
					$monthToMonthData['currYSum'] = round($monthToMonthData['currYSum'] / $msscht1 * 10) / 100;
					$monthToMonthData['prevYSum'] = round($monthToMonthData['prevYSum'] / $msscht1 * 10) / 100;
					$yearToDayData['currYSum'] = round($yearToDayData['currYSum'] / $msscht1 * 10) / 100;
					$yearToDayData['prevYSum'] = round($yearToDayData['prevYSum'] / $msscht1 * 10) / 100;
					$monthToMonthData['currYPrc'] = round($monthToMonthData['currYPrc'] / $msscht1 * 10) / 100;
					$monthToMonthData['prevYPrc'] = round($monthToMonthData['prevYPrc'] / $msscht1 * 10) / 100;
					$yearToDayData['currYPrc'] = round($yearToDayData['currYPrc'] / $msscht1 * 10) / 100;
					$yearToDayData['prevYPrc'] = round($yearToDayData['prevYPrc'] / $msscht1 * 10) / 100;
					foreach ($intervalsNames as $ival) {
						$intervalsData[$ival.'Sum'] = round($intervalsData[$ival.'Sum'] / $msscht1 * 10) / 100;
						$intervalsData[$ival.'Prc'] = round($intervalsData[$ival.'Prc'] / $msscht1 * 10) / 100;
					}
				}

				$legends = [];
				$legends['up'] = [$mst_string, $monthToMonthData['prevYSum'], $monthToMonthData['currYSum'], $yearToDayData['prevYSum'], $yearToDayData['currYSum'], ''];
				$legends['middle'] = ['Units', $monthToMonthData['prevYQty'], $monthToMonthData['currYQty'], $yearToDayData['prevYQty'], $yearToDayData['currYQty'], ''];
				$legends['down'] = [$mst_string, $monthToMonthData['prevYPrc'], $monthToMonthData['currYPrc'], $yearToDayData['prevYPrc'], $yearToDayData['currYPrc'], ''];
				for ($i = ($countOfSeries - 1); $i > -1; $i--) {
					$legends['up'][] = $intervalsData[$intervalsNames[$i].'Sum'];
					$legends['middle'][] = $intervalsData[$intervalsNames[$i].'Qty'];
					$legends['down'][] = $intervalsData[$intervalsNames[$i].'Prc'];
					$sorters[] = 0;
					$names[] = $intervalsNames[$i];
					if ($intervalsData[$intervalsNames[$i].'SumPerc'] < 0) {
						$data_0_m[] = $intervalsData[$intervalsNames[$i].'SumPerc'];
						$data_0_p[] = 0;
					} else if ($intervalsData[$intervalsNames[$i].'SumPerc'] > 0) {
						$data_0_p[] = $intervalsData[$intervalsNames[$i].'SumPerc'];
						$data_0_m[] = 0;
					} else {
						$data_0_m[] = 0;
						$data_0_p[] = 0.01;
					}
					if ($intervalsData[$intervalsNames[$i].'QtyPerc'] < 0) {
						$data_1_m[] = $intervalsData[$intervalsNames[$i].'QtyPerc'];
						$data_1_p[] = 0;
					} else if ($intervalsData[$intervalsNames[$i].'QtyPerc'] > 0) {
						$data_1_p[] = $intervalsData[$intervalsNames[$i].'QtyPerc'];
						$data_1_m[] = 0;
					} else {
						$data_1_m[] = 0;
						$data_1_p[] = 0.01;
					}
					if ($intervalsData[$intervalsNames[$i].'PrcPerc'] < 0) {
						$data_2_m[] = $intervalsData[$intervalsNames[$i].'PrcPerc'];
						$data_2_p[] = 0;
					} else if ($intervalsData[$intervalsNames[$i].'PrcPerc'] > 0) {
						$data_2_p[] = $intervalsData[$intervalsNames[$i].'PrcPerc'];
						$data_2_m[] = 0;
					} else {
						$data_2_m[] = 0;
						$data_2_p[] = 0.01;
					}
				}
				echo '{"result":1,"currency":"'.$cnt_string.'","slide_legend":"'.$slide_legend.'","data":{"ss":['.implode(',',$sorters).'],"sNs":["'.implode('","',$names).'"],"datasets_0":[{"backgroundColor":"rgba(6, 213, 10, 1)","data":['.implode(',',$data_0_p).']},{"backgroundColor":"rgba(255, 8, 0, 1)","data":['.implode(',',$data_0_m).']}],"datasets_1":[{"backgroundColor":"rgba(6, 213, 10, 1)","data":['.implode(',',$data_1_p).']},{"backgroundColor":"rgba(255, 8, 0, 1)","data":['.implode(',',$data_1_m).']}],"datasets_2":[{"backgroundColor":"rgba(6, 213, 10, 1)","data":['.implode(',',$data_2_p).']},{"backgroundColor":"rgba(255, 8, 0, 1)","data":['.implode(',',$data_2_m).']}],"lUs":["'.implode('","',$legends['up']).'"],"lMs":["'.implode('","',$legends['middle']).'"],"lDs":["'.implode('","',$legends['down']).'"]}}';
			}
			else {
				echo '{"result":0}';
			}

			break;

		}

		case 'getUserReqs': {
			$cur_user_id = get_current_user_id();
			$sql = sprintf("SELECT id, name FROM user_reqs WHERE user_id=%d ORDER by `name` ASC", 
						  	$cur_user_id
						  );
			if ($result = $conn->query($sql)) {
				$columns = [];
				while ($obj = $result->fetch_object()) {
					$columns[] = '{"id":'.$obj->id.',"name":"'.$obj->name.'"}';
				}
				echo '{"result":'.count($columns).',"data":['.implode(',', $columns).']}';
			}
				else {
					die('{"result":0,"error":"'.$sql.'</br>'.$conn->error.'"}');
				}
			break;
		}

		case 'loadUserReqs': {
			$cur_user_id = get_current_user_id();
			$req_id = strip_tags($_POST['req_id']);
			if(empty($req_id)) {
				die('{"result":0,"error":"Empty ID"}');
			}
			$sql = sprintf("SELECT `data` FROM user_reqs WHERE user_id=%d AND id=%d", 
						  	$cur_user_id,
						    $req_id
						  );
			//die('{"result":0,"error":"'.$sql.'"}');
				if ($result = $conn->query($sql)) {
					$obj = $result->fetch_object();
					//die('{"result":0,"error":'.unserialize(stripslashes($obj)).'}');
					$data = json_encode(unserialize(stripslashes($obj->data)));
					echo '{"result":1,"data":'.$data.'}';
				}
			break;
		}

		case 'delUserReqs': {
			$cur_user_id = get_current_user_id();
			$req_id = strip_tags($_POST['req_id']);
			if(empty($req_id)) {
				die('{"result":0,"error":"Empty ID"}');
			}
			
			$sql = sprintf("DELETE FROM user_reqs WHERE user_id=%d AND id=%d", 
						  	$cur_user_id,
						    $req_id
						  );
				if ($result = $conn->query($sql)) {
					echo '{"result":1,"message":"Template deleted!"}';
				}
				else {
					die('{"result":0,"error":"'.$sql.'</br>'.$conn->error.'"}');
				}
			break;
		}

		case 'addUserReqs': {
			$cur_user_id = get_current_user_id();
			$req_name = strip_tags($_POST['req_name']);
			$req_data = serialize($_POST['req_data']);
			if(empty($req_name)) {
				die('{"result":0,"error":"Name empty"}');
			}			
			$sql = sprintf("SELECT COUNT(id) as cnt FROM user_reqs WHERE user_id=%d AND name='%s'", 
						  	$cur_user_id,
						    addslashes($req_name)
						  );
			if ($result = $conn->query($sql)) {
			$obj = $result->fetch_object();				
				if($obj->cnt > 0) {
					die('{"result":0,"error":"Name exists"}');
				}
			}

			/*$postform = array();
				if(is_array($req_data)) {
				$len = sizeof($req_data);
					for($i=0; $i<sizeof($req_data); $i++) {
						$postform[$req_data['q'][$i]['name']] = $req_data['q'][$i]['value'];
					}
				unset($len);
				}*/
			
			$sql = sprintf("INSERT INTO user_reqs (user_id, name, data) VALUES (%d, '%s', '%s')", 
						  	$cur_user_id,
						    addslashes($req_name),
						    addslashes($req_data)
						  );
				//die('{"result":0,"error":"'.$sql.'"}');			
				if (!($conn->query($sql) === TRUE)) {
					die('{"result":0,"error":"'.$sql.'</br>'.$conn->error.'"}');
				}
				$insert_id = $conn->insert_id;
				echo '{"result":1,"message":"Template added!"}';
			break;
		}

		case 'saveUserReqs': {
			$cur_user_id = get_current_user_id();
			$req_id = strip_tags($_POST['req_id']);
			if(empty($req_id)) {
				die('{"result":0,"error":"Empty ID"}');
			}
			$req_data = serialize($_POST['req_data']);
			
			$sql = sprintf("UPDATE user_reqs SET data='%s' WHERE user_id=%d AND id=%d", 
						    addslashes($req_data),
						  	$cur_user_id,
							$req_id
						  );
			//die('{"result":0,"error":"'.$sql.'"}');
			if ($result = $conn->query($sql)) {
				echo '{"result":1,"message":"Template saved!"}';
			}
			else {
				die('{"result":0,"error":"'.$sql.'</br>'.$conn->error.'"}');
			}
			
			break;
		}

		case 'editUserReqs': {
			$cur_user_id = get_current_user_id();
			$req_id = strip_tags($_POST['req_id']);
			if(empty($req_id)) {
				die('{"result":0,"error":"Empty ID"}');
			}
			$req_name = strip_tags($_POST['req_name']);
			$req_data = strip_tags($_POST['req_data']);
			if(empty($req_name)) {
				die('{"result":0,"error":"Name empty"}');
			}
			$sql = sprintf("SELECT FROM user_reqs WHERE user_id=%d AND name='%s'", 
						  	$cur_user_id,
						    addslashes($req_name)
						  );
				if ($result = $conn->query($sql)) {
				$obj = $result->fetch_object();				
					if($obj->cnt > 0) {
						die('{"result":0,"error":"Name exists"}');
					}
				}
			
			$sql = sprintf("UPDATE user_reqs SET name='%s', data='%s' WHERE user_id=%d AND id=%d", 
						    addslashes($req_name),
						    addslashes($req_data),
						  	$cur_user_id,
							$req_id
						  );
			if ($result = $conn->query($sql)) {
				echo '{"result":1,"message":"Template saved!"}';
			}
			else {
				die('{"result":0,"error":"'.$sql.'</br>'.$conn->error.'"}');
			}
			
			break;
		}

		case 'getName2Colors': {
			$sql = "SELECT id, name_ru, shortname_ru, name_en, shortname_en, rgbcolor FROM name2colors ORDER by `name_en` ASC"; 
			if ($result = $conn->query($sql)) {
				$columns = [];
				while ($obj = $result->fetch_object()) {
					/*$columns[] = array(
						'id'			=> $obj->id,
						'name_ru' 		=> stripslashes($obj->name_ru),
						'shortname_ru' 	=> stripslashes($obj->shortname_ru),
						'name_en' 		=> stripslashes($obj->name_en),
						'shortname_en' 	=> stripslashes($obj->shortname_en),
						'color' 		=> stripslashes($obj->rgbcolor)
						);*/
					$columns[stripslashes($obj->name_ru)] = array(
						'name' 		=> !(empty($obj->shortname_ru))?stripslashes($obj->shortname_ru):stripslashes($obj->name_ru),
						'color' 	=> stripslashes($obj->rgbcolor)
						);
					$columns[stripslashes($obj->name_en)] = array(
						'name' 		=> !(empty($obj->shortname_en))?stripslashes($obj->shortname_en):stripslashes($obj->name_en),
						'color' 	=> stripslashes($obj->rgbcolor)
						);
				}				
				$res_arr = array(
					'result' => count($columns),
					'data'	 => $columns
					);
				echo json_encode($res_arr);
			}
			else {
				die('{"result":0,"error":"'.$sql.'</br>'.$conn->error.'"}');
			}
			break;
		}			
											
		case '<caseValue>': {

			break;

		}

		default: {

			echo '{"result":0,"error":"emptyTask"}';

			break;

		}

	}

# Закрыть подключение
	$conn->close();

	die;
}