<?php

/*
 * Example PHP implementation used for the index.html example
 */

// DataTables PHP library
include("../lib/DataTables.php" );
	
use
    DataTables\Editor,
    DataTables\Editor\Field,
    DataTables\Editor\Format,
    DataTables\Editor\Mjoin,
    DataTables\Editor\Options,
    DataTables\Editor\Upload,
    DataTables\Editor\Validate,
    DataTables\Editor\ValidateOptions;
 
// Build our Editor instance and process the data coming from _POST
$db->sql("SET names 'utf8'");
Editor::inst( $db, 'name2colors' )
    ->fields(
        Field::inst( 'name_en' ),
		Field::inst( 'shortname_en' ),
        Field::inst( 'name_ru' ),
        Field::inst( 'shortname_ru' ),
		Field::inst( 'rgbcolor' )
    )
    ->process( $_POST )
    ->json();

?>