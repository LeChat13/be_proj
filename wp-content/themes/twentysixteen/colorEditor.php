<?php
	/*
		Template Name: colorEditor
	*/
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
<!--	<link rel="shortcut icon" type="image/ico" href="http://www.datatables.net/favicon.ico">-->
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, user-scalable=no">
	<title>Color Editor</title>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.0/css/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.0.0/css/buttons.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.3.3/css/select.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/datetime/1.1.1/css/dataTables.dateTime.min.css">
	<link rel="stylesheet" type="text/css" href="<?=(get_template_directory_uri() . "/DTables/css/editor.dataTables.min.css")?>">
	<link rel="stylesheet" type="text/css" href="<?=(get_template_directory_uri() . "/DTables/examples/resources/demo.css")?>">

	<link href="<?=(get_template_directory_uri() . "/css/spectrum.css")?>" rel="stylesheet" />
	<style type="text/css" class="init">
.colorSquare {
      width: 20px;
      height: 20px;
      margin:auto;
      border: 1px solid rgba(0, 0, 0, .2);
}
table.dataTable tbody th.dt-body-center,
table.dataTable tbody td.dt-body-center {
  text-align: center;
}
	</style>

	<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.11.0/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/2.0.0/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/select/1.3.3/js/dataTables.select.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/datetime/1.1.1/js/dataTables.dateTime.min.js"></script>
	<script type="text/javascript" language="javascript" src="<?=(get_template_directory_uri() . "/DTables/js/dataTables.editor.min.js")?>"></script>

	<script src="<?=(get_template_directory_uri() . "/js/spectrum.js")?>"></script>
	
	<script type="text/javascript" language="javascript" class="init">
		
//plugin
// colorpicker field type plug-in code
(function ($, DataTable) {
 
if ( ! DataTable.ext.editorFields ) {
    DataTable.ext.editorFields = {};
}
 
var Editor = DataTable.Editor;
var _fieldTypes = DataTable.ext.editorFields;
 
_fieldTypes.colorpicker = {
    create: function ( conf ) {
        var that = this;
 
        conf._enabled = true;
 
        // Create the elements to use for the input
        conf._input = $(
            '<div id="' + Editor.safeId(conf.id) + '">' +
                '<input type="text" class="basic" id="spectrum"/>' +
                '<em id="basic-log"></em>' +
                '</div>');
 
        // Use the fact that we are called in the Editor instance's scope to call
        // input.ClassName
        $("input.basic", conf._input).spectrum({
            //color: "#f00",
            change: function (color) {
                $("#basic-log").text(color.toRgbString());
            }
        });
        return conf._input;
    },
 
    get: function (conf) {
        var val = $("input.basic", conf._input).spectrum("get").toRgbString();
        return val;
    },
 
    set: function (conf, val) {
        $("input.basic", conf._input).spectrum({
            color: val,
			showAlpha: true,
			showInput: true,
    		allowEmpty:true,
			preferredFormat: "rgb",
            change: function (color) {
               // $("#basic-log").text("change called: " + color.toHexString());
                $("#basic-log").text(color.toRgbString());
			}
        });          
    },
 
    enable: function ( conf ) {
        conf._enabled = true;
        $(conf._input).removeClass( 'disabled' );
    },
 
    disable: function ( conf ) {
        conf._enabled = false;
        $(conf._input).addClass( 'disabled' );
    }
};
})(jQuery, jQuery.fn.dataTable);
		
var editor; // use a global for the submit and return data rendering in the examples
	</script>
</head>
	
<body class="dt-example php">
	
	<div class="container">

<?
	global $user_ID, $user_identity, $user_email;	
	get_currentuserinfo();
	if (!$user_ID):
?>
	<div>
		<?$temp = str_replace('Войти', 'Авторизация', $temp);?>
			<form name="loginform" id="autoriz" action="<?=get_settings('siteurl'); ?>/wp-login.php" method="post">
				<div>
					<p>Логин:  <input type="text" name="log" placeholder="Логин" value="wpadm" id="login" /></p>
				</div>
				<div>
					<p>Пароль: <input type="password" name="pwd" placeholder="Пароль" value="" id="password" /></p>
					<input type="submit" name="submit" value="Войти" id="enter" /><br/>
				</div>
				<div>
					<input type="hidden" name="redirect_to" value="<?=$_SERVER['REQUEST_URI']; ?>" />
				</div>
			</form> 
	</div>
<?else:?>
	
<script type="text/javascript" language="javascript" class="init">
$(document).ready(function() {
	editor = new $.fn.dataTable.Editor( {
		ajax: "<?=(get_template_directory_uri() . "/DTables/controllers/staff.php")?>",
		table: "#name2color",
		fields: [ {
				label: "En name:",
				name: "name_en"
			},
			{
				label: "En shortname:",
				name: "shortname_en"
			},
			{
				label: "Ru name:",
				name: "name_ru"
			},
			{
				label: "Ru shortname:",
				name: "shortname_ru"
			},
			{
				type: "colorpicker",
				label: "Color:",
				name: "rgbcolor"
			}
		]
	} );

	// Activate an inline edit on click of a table cell
	$('#name2color').on( 'click', 'tbody td:not(:first-child)', function (e) {
//	$('#name2color').on( 'click', 'tbody td:not(:last-child)', function (e) {
//	$('#name2color').on( 'click', 'tbody td', function (e) {
		editor.inline( this, {
			buttons: { label: '&gt;', fn: function () { this.submit(); } }
		} );
	} );

	$('#name2color').removeAttr('width').DataTable( {
		dom: "Bfrtip",
		"pageLength": 20,
		ajax: "<?=(get_template_directory_uri() . "/DTables/controllers/staff.php")?>",
		columns: [
			{
				data: null,
				defaultContent: '',
				className: 'select-checkbox',
				orderable: false
			},
			{ data: "name_en" },
			{ data: "shortname_en" },
			{ data: "name_ru" },
			{ data: "shortname_ru" },
			{
			 /* data option isn't necessary unless your dataset is named "color"
				if not, you can remove it */
			  data: "rgbcolor",
			  render: function (data, type, row) {
				if (type === 'display' &&
					data != null &&
					data.match(/rgb/i)) {
					//data.match(/#([a-f0-9]{3}){1,2}\b/i)) {
				  return '<div class="colorSquare" style="background-color:' + data + ';"></div>';
				}
				return data;
			  },
			  /* className isn't necessary either unless you want
				 parent-level formatting for the cell's contents */
			  className: "dt-body-center"
			}
		],		
		order: [ 1, 'asc' ],
		select: {
			style:    'os',
			selector: 'td:first-child'
		},
 		columnDefs: [
            { width: '15%', targets: 5 }
        ],
        fixedColumns: true,		
		buttons: [
			{ extend: "create", editor: editor },
			{ extend: "edit",   editor: editor },
			{ extend: "remove", editor: editor }
		]
	} );
} );
</script>		
		<section>
			<!--<h1>Editor</h1>-->
			<div class="demo-html">
				<table id="name2color" class="display" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th></th>
							<th>En name</th>
							<th>En shortname</th>
							<th>Ru name</th>
							<th>Ru shortname</th>
							<th>Color</th>
						</tr>
					</thead>
				</table>
			</div>
		</section>
<?endif;?> 
	</div>
</body>
</html>