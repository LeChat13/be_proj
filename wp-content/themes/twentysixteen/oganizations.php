<?php
	/*
		Template Name: organizations2
	*/
?>
<meta http-equiv="Content-Type" name="viewport" content="width=device-width, initial-scale=1">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script>
	function openTab(evt, tabName) {
		var i, tabcontent, tablinks;

		tabcontent = document.getElementsByClassName("tabcontent");
		for (i = 0; i < tabcontent.length; i++) {
			tabcontent[i].style.display = "none";
		}

		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}

		document.getElementById(tabName).style.display = "block";
		evt.currentTarget.className += " active";
	}
</script>
<style>
	body,
	html {
		margin : 0px;
		padding : 0px;
		text-align : center;
	}
	.all {
		margin-left: auto;
		margin-right: auto;
	}
	.all_2 {
		margin-left: auto;
		margin-right: auto;
		width : 20%;
	}
	.left {
		width : 50%;
		/*background-color : #C0C000;*/
		float : left;
	}
	.right {
		width : 50%;
		/*background-color : #00C0C0;*/
		float : right;
	}
	.bottom {
		/*background-color : #FFC0FF;*/
		clear : both;
		margin-top : 50px;
		padding-top : 20px;
	}
	.col {
		/*background-color : #FFC0FF;*/
		padding-top : 10px;
	}
	.tab {
		overflow: hidden;
		border: 1px solid #4CAF50;
		background-color: #C8E6C9;
	}
	.tab button {
		background-color: inherit;
		float: left;
		border: none;
		outline: none;
		cursor: pointer;
		padding: 14px 16px;
		transition: 0.3s;
	}
	.tab button:hover {
		background-color: #FFEB3B;
	}
	.tab button.active {
		background-color: #4CAF50;
	  color: #fff;
	}
	.tabcontent {
		display: none;
		padding: 6px 12px;
		border: 1px solid #4CAF50;
		border-top: none;
	}
</style>

<div>
<?
	global $user_ID, $user_identity, $user_email;	
	get_currentuserinfo();
	if (!$user_ID):
?>
	<div>
		<?$temp = str_replace('Войти', 'Авторизация', $temp);?>
			<form name="loginform" id="autoriz" action="<?=get_settings('siteurl'); ?>/wp-login.php" method="post">
				<div>
					<p>Логин:  <input type="text" name="log" placeholder="Логин" value="wpadm" id="login" /></p>
				</div>
				<div>
					<p>Пароль: <input type="password" name="pwd" placeholder="Пароль" value="" id="password" /></p>
					<input type="submit" name="submit" value="Войти" id="enter" /><br/>
				</div>
				<div>
					<input type="hidden" name="redirect_to" value="<?=$_SERVER['REQUEST_URI']; ?>" />
				</div>
			</form> 
	</div>
<?else:?>
		
	<div>
		<a href="/" style="color:green">Назад на главную</a> 
		<p/>
		<form name="add_org" id="add_org" action="/orgadd" method="post">
			<input type="submit" name="added" value="Добавить организацию" id="added"><br/>
		</form>
		<br/>
		<div class="tab">
			<button class="tablinks<?if (!$_POST['search1']):?> active<?endif;?>" onclick="openTab(event, 'Diffirent')">Поиск в одной строке</button>
			<button class="tablinks<?if ($_POST['search1']):?> active<?endif;?>" onclick="openTab(event, 'All_in_one')">Поиск по отдельным полям</button>
		</div>
		<div id="All_in_one" class="tabcontent" style="display: <?if ($_POST['search1']):?>block<?else:?>none<?endif;?>;">
			<form name="search_org1" id="search_org1" action="" method="post" enctype="multipart/form-data">
				<?require_once 'db_org.php';?>
				<div class="left">
					<label for="org_recipient_type">Выберите тип покупателя:</label><br/>
					<select name="org_recipient_type" style="width: 90%" onchange="$('#description').text(value);">
						<?foreach ($recipient_types_data as $dropd):?>
							<?if ($dropd['id'] == $_POST['org_recipient_type']):?>
								<option id="<?=$dropd['id']?>" value="<?=$dropd['id']?>" selected><?=$dropd['named']?></option>
							<?else:?>
								<option id="<?=$dropd['id']?>" value="<?=$dropd['id']?>"><?=$dropd['named']?></option>
							<?endif;?>
						<?endforeach?>
					</select>
				</div>
				<div class="right">
					<label for="org_e_u_type">Выберите E-U Type:</label><br/>
					<select name="org_e_u_type" style="width: 90%" onchange="$('#description').text(value);">
						<?foreach ($e_u_types_data as $dropd):?>
							<?if ($dropd['id'] == $_POST['org_e_u_type']):?>
								<option id="<?=$dropd['id']?>" value="<?=$dropd['id']?>" selected><?=$dropd['named']?></option>
							<?else:?>
								<option id="<?=$dropd['id']?>" value="<?=$dropd['id']?>"><?=$dropd['named']?></option>
							<?endif;?>
						<?endforeach?>
					</select>
				</div>
				<div class="left">
					<label for="org_inn">Поиск в ИНН:</label><br/>
					<input type="text" style="width: 90%" id="org_inn" name="org_inn" value="<?if ($_POST['org_inn']):?><?=$_POST['org_inn']?><?endif;?>"><br/>
				</div>
				<div class="right">
					<label for="org_kpp">Поиск в КПП:</label><br/>
					<input type="text" style="width: 90%" id="org_kpp" name="org_kpp" value="<?if ($_POST['org_kpp']):?><?=$_POST['org_kpp']?><?endif;?>"><br/>
				</div>
				<div class="left">
					<label for="org_name">Поиск в Наименовании:</label><br/>
					<input type="text" style="width: 90%" id="org_name" name="org_name" value="<?if ($_POST['org_name']):?><?=$_POST['org_name']?><?endif;?>"><br/>
				</div>
				<div class="right">
					<label for="org_address">Поиск в Адресе:</label><br/>
					<input type="text" style="width: 90%" id="org_address" name="org_address" value="<?if ($_POST['org_address']):?><?=$_POST['org_address']?><?endif;?>"><br/>
				</div>
				<div class="bottom">
					<input type="submit" name="search1" value="Найти" id="search1"><br/>
				</div>
			</form>
		</div>
		<div id="Diffirent" class="tabcontent" style="display: <?if ($_POST['search1']):?>none<?else:?>block<?endif;?>;">
			<form name="search_org2" id="search_org2" action="" method="post" enctype="multipart/form-data">
				<?require_once 'db_org.php';?>
				<div class="all">
					<label for="org_search">Введите критерии поиска:</label>
					<input type="text" style="width: 90%" id="org_search" name="org_search" value="<?if ($_POST['org_search']):?><?=$_POST['org_search']?><?endif;?>"><br/>
				</div>
				<div class="all_2">
					<div class="left">
						<label>Искать в:</label>
					</div>
					<div class="right">
						<label><input type="checkbox" id="org_search_inn" name="org_search_inn" <?if (($_POST['org_search_inn']) || (!$_POST['search2'])):?>checked<?endif;?>>ИНН</label><br/>
						<label><input type="checkbox" id="org_search_kpp" name="org_search_kpp" <?if (($_POST['org_search_kpp']) || (!$_POST['search2'])):?>checked<?endif;?>>КПП</label><br/>
						<label><input type="checkbox" id="org_search_named" name="org_search_named" <?if (($_POST['org_search_named']) || (!$_POST['search2'])):?>checked<?endif;?>>Наименование</label><br/>
						<label><input type="checkbox" id="org_search_address" name="org_search_address" <?if (($_POST['org_search_address']) || (!$_POST['search2'])):?>checked<?endif;?>>Адрес</label><br/>
					</div>
				</div>
				<div class="bottom">
					<input type="submit" name="search2" value="Найти" id="search2"><br/>
				</div>
			</form>
		</div>
		<div>
			<label>Таблица организаций:</label>
		</div>
		<p/>
		<div>
			<table border="1" style="width: 100%;">
				<thead align="center">
					<tr>
						<td style="width: 3%;">
							№ пп
						</td>
						<td style="width: 27%;">
							Наименование
						</td>
						<td style="width: 7%;">
							ИНН
						</td>
						<td style="width: 7%;">
							КПП
						</td>
						<td style="width: 27%;">
							Адрес
						</td>
						<td style="width: 14%;">
							Тип получателя
						</td>
						<td style="width: 8%;">
							E-U Type
						</td>
						<td style="width: 7%;">
							Изменить запись
						</td>
					</tr>
				</thead>
				<tbody>
					<?
					$num = 0;
					foreach ($organizations_data as $dropd):
						$num++;
						if ($_POST['search1']) {
							$replace = '$1<b style="color:#FF0000; background:#FFFF00;">$2</b>';
							if ($_POST['org_inn']) {
								$dropd['org_inn'] = preg_replace("/((?:^|>)[^<]*)(".$_POST['org_inn'].")/siu", $replace, $dropd['org_inn']);
							}
							if ($_POST['org_kpp']) {
								$dropd['org_kpp'] = preg_replace("/((?:^|>)[^<]*)(".$_POST['org_kpp'].")/siu", $replace, $dropd['org_kpp']);
							}
							if ($_POST['org_name']) {
								$dropd['org_name'] = preg_replace("/((?:^|>)[^<]*)(".$_POST['org_name'].")/siu", $replace, $dropd['org_name']);
							}
							if ($_POST['org_address']) {
								$dropd['org_address'] = preg_replace("/((?:^|>)[^<]*)(".$_POST['org_address'].")/siu", $replace, $dropd['org_address']);
							}
							if ($_POST['org_recipient_type'] > 1) {
								$dropd['recipient_name'] = '<b style="color:#FF0000; background:#FFFF00;">'.$dropd['recipient_name'].'</b>';
							}
							if ($_POST['org_e_u_type'] > 1) {
								$dropd['e_u_type_name'] = '<b style="color:#FF0000; background:#FFFF00;">'.$dropd['e_u_type_name'].'</b>';
							}
						}
						if ($_POST['search2']) {
							$replace = '$1<b style="color:#FF0000; background:#FFFF00;">$2</b>';
							foreach ($search_words as $search_word) {
								$pattern = "/((?:^|>)[^<]*)(".$search_word.")/siu";
								if ($_POST['org_search_inn']) {
									$dropd['org_inn'] = preg_replace($pattern, $replace, $dropd['org_inn']);
								}
								if ($_POST['org_search_kpp']) {
									$dropd['org_kpp'] = preg_replace($pattern, $replace, $dropd['org_kpp']);
								}
								if ($_POST['org_search_named']) {
									$dropd['org_name'] = preg_replace($pattern, $replace, $dropd['org_name']);
								}
								if ($_POST['org_search_address']) {
									$dropd['org_address'] = preg_replace($pattern, $replace, $dropd['org_address']);
								}
							}
						}
					?>
						<tr>
							<td align="center">
								<?=$num?>
							</td>
							<td align="left">
								<?=$dropd['org_name']?>
							</td>
							<td align="left">
								<?=$dropd['org_inn']?>
							</td>
							<td align="left">
								<?=$dropd['org_kpp']?>
							</td>
							<td align="left">
								<?=$dropd['org_address']?>
							</td>
							<td align="left">
								<?=$dropd['recipient_name']?>
							</td>
							<td align="left">
								<?=$dropd['e_u_type_name']?>
							</td>
							<td align="center" class="col">
								<form name="edit<?=$num?>" id="edit<?=$num?>" action="/orgedit" method="post">
									<div hidden>
										<input type="text" id="org_id" name="org_id" value="<?=$dropd['org_id']?>">
									</div>
									<div hidden>
										<input type="text" id="recipient_type_id" name="recipient_type_id" value="<?=$dropd['recipient_type_id']?>">
									</div>
									<div hidden>
										<input type="text" id="e_u_type_id" name="e_u_type_id" value="<?=$dropd['e_u_type_id']?>">
									</div>
									<input type="submit" name="paste<?=$num?>" value="Отрдактировать" id="paste<?=$num?>"><br/>
								</form>
							</td>
						</tr>
					<?endforeach?>
				</tbody>
			</table>
		</div>
	</div>
	<?endif;?> 
</div>

<!--		.main {
			min-width : 600px;
			width : expression((document.compatMode && document.compatMode == 'CSS1Compat')?(document.documentElement.clientWidth < 600?"600px":"auto"):(document.body.clientWidth < 600?"600px":"auto"));
			margin : 0 auto;
			text-align : left;
		} */ -->
