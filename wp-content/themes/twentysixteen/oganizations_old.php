<?php
	/*
		Template Name: organizations
	*/
?>

<meta http-equiv="Content-Type" name="viewport" content="width=device-width, initial-scale=1, text/html, charset=utf-8">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<link href="style.css" rel="stylesheet" type="text/css">
<style>
	body,
	html {
		margin : 0px;
		padding : 0px;
		text-align : center;
	}
	.main {
		min-width : 600px;
		width : expression((document.compatMode && document.compatMode == 'CSS1Compat')?(document.documentElement.clientWidth < 600?"600px":"auto"):(document.body.clientWidth < 600?"600px":"auto"));
		margin : 0 auto;
		text-align : left;
	}
	.left {
		width : 50%;
		/*background-color : #C0C000;*/
		float : left;
	}
	.right {
		width : 50%;
		/*background-color : #00C0C0;*/
		float : right;
	}
	.bottom {
		/*background-color : #FFC0FF;*/
		clear : both;
		margin-top : 50px;
		padding-top : 20px;
	}
	.col {
		/*background-color : #FFC0FF;*/
		padding-top : 10px;
	}
</style>
<div>
<?
	global $user_ID, $user_identity, $user_email;
	get_currentuserinfo();
	if (!$user_ID):
?>
	<div>
		<?$temp = str_replace('Войти', 'Авторизация', $temp);?>
			<form name="loginform" id="autoriz" action="<?=get_settings('siteurl'); ?>/wp-login.php" method="post">
				<div>
					<p>Логин:  <input type="text" name="log" placeholder="Логин" value="wpadm" id="login" /></p>
				</div>
				<div>
					<p>Пароль: <input type="password" name="pwd" placeholder="Пароль" value="" id="password" /></p>
					<input type="submit" name="submit" value="Войти" id="enter" /><br/>
				</div>
				<div>
					<input type="hidden" name="redirect_to" value="<?=$_SERVER['REQUEST_URI']; ?>" />
				</div>
			</form> 
	</div>
<!--	<?#elseif ($user_identity <> 'be_admin'):?>
	<div>
		<label style="color:red">Ваших прав доступа недостаточно для просмотра данной страницы</label>
	</div>
-->	<?else:?>
	<div>
		<a href="/" style="color:green">Назад на главную</a>
		<p/>
		<div id="maket">
			<form name="add_org" id="add_org" action="/orgadd" method="post">
				<input type="submit" name="added" value="Добавить организацию" id="added"><br/>
			</form>
			<br/>
			<form name="search_org" id="search_org" action="" method="post" enctype="multipart/form-data">
				<?require_once 'db_org.php';?>
				<div class="left">
					<label for="org_recipient_type">Выберите тип покупателя:</label>
					<select name="org_recipient_type" style="width: 90%" onchange="$('#description').text(value);">
						<?foreach ($recipient_types_data as $dropd):?>
							<option id="<?=$dropd['id']?>" value="<?=$dropd['id']?>"><?=$dropd['named']?></option>
						<?endforeach?>
					</select>
				</div>
				<div class="right">
					<label for="org_e_u_type">Выберите E-U Type:</label>
					<select name="org_e_u_type" style="width: 90%" onchange="$('#description').text(value);">
						<?foreach ($e_u_types_data as $dropd):?>
							<option id="<?=$dropd['id']?>" value="<?=$dropd['id']?>"><?=$dropd['named']?></option>
						<?endforeach?>
					</select>
				</div>
				<div class="left">
					<label for="org_inn">Поиск в ИНН:</label>
					<input type="text" style="width: 90%" id="org_inn" name="org_inn" value=""><br/>
				</div>
				<div class="right">
					<label for="org_kpp">Поиск в КПП:</label>
					<input type="text" style="width: 90%" id="org_kpp" name="org_kpp" value=""><br/>
				</div>
				<div class="left">
					<label for="org_name">Поиск в Наименовании:</label>
					<input type="text" style="width: 90%" id="org_name" name="org_name" value=""><br/>
				</div>
				<div class="right">
					<label for="org_address">Поиск в Адресе:</label>
					<input type="text" style="width: 90%" id="org_address" name="org_address" value=""><br/>
				</div>
				<div class="bottom">
					<input type="submit" name="paste" value="Найти" id="paste"><br/>
				</div>
			</form>
		</div>
	<?php
		ini_set('upload_max_filesize', '1M'); //ограничение в 1 мб
		if (($_SERVER['REQUEST_METHOD'] == "POST") && ($_POST['paste']))
		{
			require_once 'db_org.php';
		}
	?>

		<div>
			<label>Таблица организаций:</label>
		</div>
		<p/>
		<div>
			<table border="1" style="width: 100%;">
				<thead align="center">
					<tr>
						<td style="width: 3%;">
							№ пп
						</td>
						<td style="width: 27%;">
							Наименование
						</td>
						<td style="width: 7%;">
							ИНН
						</td>
						<td style="width: 7%;">
							КПП
						</td>
						<td style="width: 27%;">
							Адрес
						</td>
						<td style="width: 14%;">
							Тип получателя
						</td>
						<td style="width: 8%;">
							E-U Type
						</td>
						<td style="width: 7%;">
							Изменить запись
						</td>
					</tr>
				</thead>
				<tbody>
					<?
					$num = 0;
					foreach ($organizations_data as $dropd):
						$num++;
						fclose ($file);
					?>
						<tr>
							<td align="center">
								<?=$num?>
							</td>
							<td align="left">
								<?=$dropd['org_name']?>
							</td>
							<td align="left">
								<?=$dropd['org_inn']?>
							</td>
							<td align="left">
								<?=$dropd['org_kpp']?>
							</td>
							<td align="left">
								<?=$dropd['org_address']?>
							</td>
							<td align="left">
								<?=$dropd['recipient_name']?>
							</td>
							<td align="left">
								<?=$dropd['e_u_type_name']?>
							</td>
							<td align="center" class="col">
								<form name="edit<?=$num?>" id="edit<?=$num?>" action="/orgedit" method="post">
									<div hidden>
										<input type="text" id="org_id" name="org_id" value="<?=$dropd['org_id']?>">
									</div>
									<div hidden>
										<input type="text" id="recipient_type_id" name="recipient_type_id" value="<?=$dropd['recipient_type_id']?>">
									</div>
									<div hidden>
										<input type="text" id="e_u_type_id" name="e_u_type_id" value="<?=$dropd['e_u_type_id']?>">
									</div>
									<input type="submit" name="paste<?=$num?>" value="Отрдактировать" id="paste<?=$num?>"><br/>
								</form>
							</td>
						</tr>
					<?endforeach?>
				</tbody>
			</table>
		</div>
	</div>
	<?endif;?> 
</div>


