<?php
	/*
    		Template Name: MailAdd
	*/
?>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<div>
<?
	global $user_ID, $user_identity, $user_email;
	get_currentuserinfo();
#	echo $user_email;
	if (!$user_ID):
?>
	<div>
		<?$temp = str_replace('Войти', 'Авторизация', $temp);?>
			<form name="loginform" id="autoriz" action="<?=get_settings('siteurl'); ?>/wp-login.php" method="post">
				<div class="left">
					<p>Логин:  <input type="text" name="log" placeholder="Логин" value="wpadm" id="login" /></p>
				</div>
				<div>
					<p>Пароль: <input type="password" name="pwd" placeholder="Пароль" value="" id="password" /></p>
					<input type="submit" name="submit" value="Войти" id="enter" /><br/>
				</div>
				<div>
					<input type="hidden" name="redirect_to" value="<?=$_SERVER['REQUEST_URI']; ?>" />
				</div>
			</form> 
	</div>
	<?else:?>
	<div>
		<?if (($_SERVER['REQUEST_METHOD'] == "POST") && ($_POST['added'])):?>
			<div>
				<form name="addmail" id="addmail" action="" method="post" enctype="multipart/form-data">
					<?require_once 'get_mail_data.php';?>
					<div>
						<label for="email">Введите E-Mail получателя:</label>
						<p/>
						<input type="text" style="width: 613px;" id="email" name="email" value="" required><br/>
						<label id='mailerr' style="color:red; display:none">Необходимо обязательно выбрать email адрес</label>
					</div>
					<div>
					</div>
					<div>
						<label for="script_id">Выберите обрабатывающий скрипт:</label>
						<p/>
						<select name="script_id" style="width: 613px;" onchange="$('#description').text(value);">
							<?foreach ($xlsx_scripts_data as $dropd):?>
								<option id="<?=$dropd['id']?>" value="<?=$dropd['id']?>"><?=$dropd['script_info']?> - <?=$dropd['script_name']?></option>
							<?endforeach?>
						</select>
						<label id='scripterr' style="color:red; display:none">Необходимо обязательно выбрать скрипт</label>
					</div>
					<div>
					</div>
					<div>
						<label for="attach_flag">Присоединять файл к письму? - </label>
						<p/>
						<select name="attach_flag" style="width: 613px;" onchange="$('#description').text(value);">
							<option id="true" value="true">Да</option>
							<option selected id="false" value="false">Нет</option>
						</select>
						<label id='attach_flagerr' style="color:red; display:none">Необходимо обязательно выбрать вариант</label>
					</div>
					<div>
					</div>
					<div>
						<label for="group_flag">Введите метку группировки:</label>
						<p/>
						<input type="text" style="width: 613px;" id="group_flag" name="group_flag" value="" required><br/>
						<label id='group_flag_err' style="color:red; display:none">Необходимо обязательно ввести метку группировки</label>
					</div>
					<div>
					</div>
					<div>
						<input type="submit" name="added" value="Добавить" id="added"><br/>
                    	<?if (($_SERVER['REQUEST_METHOD'] == "POST" ) && ($_POST['attach_flag']) && ($_POST['script_id']) && ($_POST['email']) && ($_POST['group_flag']) && ($_POST['added'])):?>
                    	    <?require_once 'add_mail_data.php';?>
                 			<script>
                				{
                					window.location.href = "/maileslist";
                				}
                			</script>
                    	<?endif;?>
					</div>
				</form>
			</div>
			<br/>
			<a href="/maileslist">Вернуться без редактирования</a>
    	<?else:?>
			<script>
				{
					window.location.href = "/";
				}
			</script>
		<?endif;?> 
	</div>
	<?endif;?> 
</div>