<?php
	/*
			Template Name: OrgEdit
	*/
?>
<meta http-equiv="Content-Type" name="viewport" content="width=device-width, initial-scale=1, text/html, charset=utf-8">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<link href="style.css" rel="stylesheet" type="text/css">
<style>
	body,
	html {
		margin : 0px;
		padding : 0px;
		text-align : center;
	}
	.main {
		min-width : 600px;
		width : expression((document.compatMode && document.compatMode == 'CSS1Compat')?(document.documentElement.clientWidth < 600?"600px":"auto"):(document.body.clientWidth < 600?"600px":"auto"));
		margin : 0 auto;
		text-align : left;
	}
	.left {
		width : 50%;
		/*background-color : #C0C000;*/
		float : left;
	}
	.right {
		width : 50%;
		/*background-color : #00C0C0;*/
		float : right;
	}
	.bottom {
		/*background-color : #FFC0FF;*/
		clear : both;
		margin-top : 50px;
		padding-top : 20px;
	}
	.col {
		/*background-color : #FFC0FF;*/
		padding-top : 10px;
	}
</style>
<div>
<?
	global $user_ID, $user_identity, $user_email;
	get_currentuserinfo();
	if (!$user_ID):
?>
	<div>
		<?$temp = str_replace('Войти', 'Авторизация', $temp);?>
			<form name="loginform" id="autoriz" action="<?=get_settings('siteurl'); ?>/wp-login.php" method="post">
				<div class="left">
					<p>Логин:  <input type="text" name="log" placeholder="Логин" value="wpadm" id="login" /></p>
				</div>
				<div>
					<p>Пароль: <input type="password" name="pwd" placeholder="Пароль" value="" id="password" /></p>
					<input type="submit" name="submit" value="Войти" id="enter" /><br/>
				</div>
				<div>
					<input type="hidden" name="redirect_to" value="<?=$_SERVER['REQUEST_URI']; ?>" />
				</div>
			</form> 
	</div>
	<?else:?>
	<div>
		<?if ($_SERVER['REQUEST_METHOD'] == "POST"):?>
			<div id="maket">
				<form name="editorg" id="editorg" action="" method="post" enctype="multipart/form-data">
					<?require_once 'get_org_data.php';?>

					<div class="left">
						<label for="recipient_type_id">Выберите тип покупателя:</label>
						<select name="recipient_type_id" style="width: 90%" onchange="$('#description').text(value);">
							<?foreach ($recipient_types_data as $dropd):?>
								<?if ($dropd['id'] == $_POST['recipient_type_id']):?>
									<option selected id="sel<?=$dropd['id']?>" value="<?=$dropd['id']?>"><?=$dropd['named']?></option>
								<?else:?>
									<option id="<?=$dropd['id']?>" value="<?=$dropd['id']?>"><?=$dropd['named']?></option>
								<?endif;?>
							<?endforeach?>
						</select>
					</div>
					<div class="right">
						<label for="e_u_type_id">Выберите E-U Type:</label>
						<select name="e_u_type_id" style="width: 90%" onchange="$('#description').text(value);">
							<?foreach ($e_u_types_data as $dropd):?>
								<?if ($dropd['id'] == $_POST['e_u_type_id']):?>
									<option selected id="sel<?=$dropd['id']?>" value="<?=$dropd['id']?>"><?=$dropd['named']?></option>
								<?else:?>
									<option id="<?=$dropd['id']?>" value="<?=$dropd['id']?>"><?=$dropd['named']?></option>
								<?endif;?>
							<?endforeach?>
						</select>
					</div>
					<div class="left">
						<label for="org_inn">Введите значение ИНН:</label>
						<input type="text" style="width: 90%" id="org_inn" name="org_inn" value="<?=$org_data[0]['org_inn']?>">
					</div>
					<div class="right">
						<label for="org_kpp">Введите значение КПП:</label>
						<input type="text" style="width: 90%" id="org_kpp" name="org_kpp" value="<?=$org_data[0]['org_kpp']?>">
					</div>
					<div class="left">
						<label for="org_name">Введите назване организации:</label>
						<input type="text" style="width: 90%" id="org_name" name="org_name" value="<?=$org_data[0]['org_name']?>">
					</div>
					<div class="right">
						<label for="org_address">Введите адрес организации:</label>
						<input type="text" style="width: 90%" id="org_address" name="org_address" value="<?=$org_data[0]['org_address']?>">
					</div>
					<div hidden class="bottom">
						<input type="text" id="org_id" name="org_id" value="<?=$_POST['org_id']?>">
					</div>
					<div class="bottom">
						<input type="submit" name="orgedit" value="Отредактировать" id="orgedit">
						<?if (($_SERVER['REQUEST_METHOD'] == "POST" ) && ($_POST['orgedit']) && ($_POST['org_id'])):?>
							<?require_once 'edit_org_data.php';?>
				 			<script>
								{
									window.location.href = "/organizations";
								}
							</script>
						<?endif;?>
					</div>
				</form>
			</div>
			<br/>
			<a href="/organizations">Вернуться без редактирования</a>
			<p/>
			<div>
				<form name="delorg" id="delorg" action="" method="post" enctype="multipart/form-data">
					<div hidden>
						<input type="text" id="org_id" name="org_id" value="<?=$_POST['org_id']?>">
					</div>
					<div>
						<input type="submit" name="orgdel" value="Удалить запись" id="orgdel"><br/>
						<?if (($_SERVER['REQUEST_METHOD'] == "POST" ) && ($_POST['orgdel']) && ($_POST['org_id'])):?>
							<?require_once 'del_org_data.php';?>
							<script>
								{
									window.location.href = "/organizations";
								}
							</script>
						<?endif;?>
					</div>
				</form>
			</div>
		<?else:?>
			<script>
				{
					window.location.href = "/";
				}
			</script>
		<?endif;?> 
	</div>
	<?endif;?> 
</div>