(function($) {
	var helper,
		helper_err,
		visible,
		timeout1,
		timeout2;

	$.fn.message = function(message) {
		message = $.trim(message || this.text());
		if (!message) {
			return;
		}
		clearTimeout(timeout1);
		clearTimeout(timeout2);

		initHelper();
		helper.find("p").html(message);
		helper.show().animate({ opacity: $.message.defaults.opacity}, $.message.defaults.fadeInDuration);
		visible = true;
		active = false;
		timeout1 = setTimeout(function() {
			visible = false;
		}, $.message.defaults.minDuration + $.message.defaults.displayDurationPerCharacter * Math.sqrt(message.length));
		timeout2 = setTimeout(fadeOutHelper, $.message.defaults.totalTimeout);
	};

	$.fn.message_err = function(message) {
		//alert($.message_err.defaults.displayDurationPerCharacter);
		message = $.trim(message || this.text());
		if (!message) {
			return;
		}
		clearTimeout(timeout1);
		clearTimeout(timeout2);

		initHelper_err();
		helper_err.find("p").html(message);
		helper_err.show().animate({ opacity: $.message_err.defaults.opacity}, $.message_err.defaults.fadeInDuration);
		visible = true;
		active = false;
		timeout1 = setTimeout(function() {
			visible = false;
		}, $.message_err.defaults.minDuration + $.message_err.defaults.displayDurationPerCharacter * Math.sqrt(message.length));
		timeout2 = setTimeout(fadeOutHelper_err, $.message_err.defaults.totalTimeout);
	};

	function initHelper() {
		if (!helper) {
			helper = $($.message.defaults.template).appendTo(document.body);
			$(window).bind("mousemove click keypress", fadeOutHelper);
		}
	}

	function fadeOutHelper() {
		if (helper.is(":visible") && !helper.is(":animated") && !visible) {
			helper.animate({ opacity: 0 }, $.message.defaults.fadeOutDuration, function() { $(this).hide() })
		}
	}

	function initHelper_err() {
			if (!helper_err) {
				helper_err = $($.message_err.defaults.template).appendTo(document.body);
				$(window).bind("mousemove click keypress", fadeOutHelper_err);
			}
		}

	function fadeOutHelper_err() {
		if (helper_err.is(":visible") && !helper_err.is(":animated") && !visible) {
			helper_err.animate({ opacity: 0 }, $.message_err.defaults.fadeOutDuration, function() { $(this).hide() })
		}
	}

	$.message_err = {};
	$.message_err.defaults = {
		opacity: 1,
		fadeOutDuration: 500,
		fadeInDuration: 200,
		displayDurationPerCharacter: 100,  //150
		minDuration: 1000,                 //2500
		totalTimeout: 5000,                //10000
		template: '<div class="jquery-message-err"><p></p></div>'
	}
	$.message = {};
	$.message.defaults = {
		opacity: 1,
		fadeOutDuration: 500,
		fadeInDuration: 200,
		displayDurationPerCharacter: 300,   //500
		minDuration: 1000,                  //2500
		totalTimeout: 6000,
		template: '<div class="jquery-message"><p></p></div>'
	}

  // Apply defaults
/*
  $.message_err = {
    defaults : function(userDefaults) {
      $.each(userDefaults, function(i, val) {
        if (typeof val == 'object' && defaults[i]) {
          $.extend(defaults[i], val);
        }
        else defaults[i] = val;
      });
    }
  };
*/
})(jQuery);
