<?php
	/*
		Template Name: Mapping
	*/
?>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<style>
	.tabs {
		border: 1px solid #e0e0e0;
	}

	.tabs__nav {
		display: flex;
		flex-wrap: wrap;
		list-style-type: none;
		background: #fafafa;
		margin: 0;
		border-bottom: 1px solid #e0e0e0;
	}

	.tabs__btn {
		padding: 0.5rem 0.75rem;
		text-decoration: none;
		color: black;
		text-align: center;
		flex-shrink: 0;
		flex-grow: 1;
		border: 1px solid transparent;
		cursor: pointer;
	}

	.tabs__btn_active {
		background: #e0e0e0;
		cursor: default;
	}

	.tabs__btn:not(.tabs__btn_active):hover,
	.tabs__btn:not(.tabs__btn_active):focus {
		background-color: #eee;
	}

	.tabs__content {
		padding: 1rem;
	}

	.tabs__pane {
		display: none;
	}

	.tabs__pane_show {
		display: block;
	}
</style>

<div>
<?
	global $user_ID, $user_identity, $user_email;
	get_currentuserinfo();
#	echo $user_email;
	if (!$user_ID):
?>
	<div>
		<?$temp = str_replace('Войти', 'Авторизация', $temp);?>
			<form name="loginform" id="autoriz" action="<?=get_settings('siteurl'); ?>/wp-login.php" method="post">
				<div class="left">
					<p>Логин:<input type="text" name="log" placeholder="Логин" value="wpadm" id="login" /></p>
				</div>
				<div>
					<p>Пароль: <input type="password" name="pwd" placeholder="Пароль" value="" id="password" /></p>
					<input type="submit" name="submit" value="Войти" id="enter" /><br/>
				</div>
				<div>
					<input type="hidden" name="redirect_to" value="<?=$_SERVER['REQUEST_URI']; ?>" />
				</div>
			</form> 
	</div>
	<?else:?>
		<?if ($user_identity <> 'be_admin'):?>
		<?else:?>
			<div class="mapping_main">
				<div class="mapping_title">
					<p>Редактируем БД</p>
				</div>
				<div class="mapping_db">
					<label for="select_m_db">Редактируемая БД:</label>
					<select class="select_m_db" name="select_m_db" id="select_m_db" onchange="$('#description').text(value);">
						<!--<option selected="selected" id="0" value="0">Россия</option>
						<option id="1" value="1">Беларусь</option>
						<option id="2" value="2">Казахстан</option>
						<option id="3" value="3">Тестовая</option>-->
					</select>
					<input type="button" class="button_m_db_add" name="button_m_db_add" id="button_m_db_add" value="Дозаписать" />
					<input type="button" class="button_m_db_rewrite" name="button_m_db_rewrite" id="button_m_db_rewrite" value="Перезаписать" />
					<input type="button" class="button_m_db_clear" name="button_m_db_clear" id="button_m_db_clear" value="Очистить" />
					<input type="button" class="button_m_db_del" name="button_m_db_del" id="button_m_db_del" value="Удалить" />
					<input type="button" class="button_m_db_new" name="button_m_db_new" id="button_m_db_new" value="Создать" />
				</div>
				<div class="mapping_activity">
					<div class="tabs" id="tabs">
						<div class="tabs__nav">
							<button class="tabs__btn tabs__btn_active">Вкладка 1</button>
							<button class="tabs__btn">Вкладка 2</button>
							<button class="tabs__btn">Вкладка 3</button>
						</div>
						<div class="tabs__content">
							<div id="tab1-1" class="tabs__pane tabs__pane_show">
							<p>Содержимое 1...</p>
							</div>
							<div id="tab1-2" class="tabs__pane">
							<p>Содержимое 2...</p>
							</div>
							<div id="tab1-3" class="tabs__pane">
							<p>Содержимое 3...</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?endif;?> 
	<?endif;?> 
</div>

<!--

Табы

<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<style>
			.tabs {
				border: 1px solid #e0e0e0;
			}

			.tabs__nav {
				display: flex;
				flex-wrap: wrap;
				list-style-type: none;
				background: #fafafa;
				margin: 0;
				border-bottom: 1px solid #e0e0e0;
			}

			.tabs__btn {
				padding: 0.5rem 0.75rem;
				text-decoration: none;
				color: black;
				text-align: center;
				flex-shrink: 0;
				flex-grow: 1;
				border: 1px solid transparent;
				cursor: pointer;
			}

			.tabs__btn_active {
				background: #e0e0e0;
				cursor: default;
			}

			.tabs__btn:not(.tabs__btn_active):hover,
			.tabs__btn:not(.tabs__btn_active):focus {
				background-color: #eee;
			}

			.tabs__content {
				padding: 1rem;
			}

			.tabs__pane {
				display: none;
			}

			.tabs__pane_show {
				display: block;
			}
		</style>
	</head>
	<body>
		<div id="tabs" class="tabs">
			<div class="tabs__nav">
				<button class="tabs__btn tabs__btn_active">Вкладка 1</button>
				<button class="tabs__btn">Вкладка 2</button>
				<button class="tabs__btn">Вкладка 3</button>
			</div>
			<div class="tabs__content">
				<div id="tab1-1" class="tabs__pane tabs__pane_show">
					<p>Содержимое 1...</p>
				</div>
				<div id="tab1-2" class="tabs__pane">
					<p>Содержимое 2...</p>
				</div>
				<div id="tab1-3" class="tabs__pane">
					<p>Содержимое 3...</p>
				</div>
			</div>
		</div>

		<script>
			class ItcTabs {
				constructor(target, config) {
					const defaultConfig = {};
					this._config = Object.assign(defaultConfig, config);
					this._elTabs = typeof target === 'string' ? document.querySelector(target) : target;
					this._elButtons = [];
					this._elPanes = [];
					[...this._elTabs.children].forEach(el => {
						[...el.children].forEach(el => {
						if (el.classList.contains('tabs__btn')) {
							this._elButtons.push(el);
						} else if (el.classList.contains('tabs__pane')) {
							this._elPanes.push(el);
						}
						})
					});
					this._eventShow = new Event('tab.itc.change');
					this._init();
					this._events();
				}
				_init() {
					this._elTabs.setAttribute('role', 'tablist');
					this._elButtons.forEach((el, index) => {
						el.dataset.index = index;
						el.setAttribute('role', 'tab');
						this._elPanes[index].setAttribute('role', 'tabpanel');
					});
				}
				show(elLinkTarget) {
					const elPaneTarget = this._elPanes[this._elButtons.indexOf(elLinkTarget)];
					let elLinkActive = null;
					let elPaneShow = null;
					this._elButtons.forEach(el => {
						if (el.classList.contains('tabs__btn_active')) {
						elLinkActive = el;
						}
					})
					this._elPanes.forEach(el => {
						if (el.classList.contains('tabs__pane_show')) {
						elPaneShow = el;
						}
					})
					if (elLinkTarget === elLinkActive) {
						return;
					}
					elLinkActive ? elLinkActive.classList.remove('tabs__btn_active') : null;
					elPaneShow ? elPaneShow.classList.remove('tabs__pane_show') : null;
					elLinkTarget.classList.add('tabs__btn_active');
					elPaneTarget.classList.add('tabs__pane_show');
					this._elTabs.dispatchEvent(this._eventShow);
					elLinkTarget.focus();
				}
				showByIndex(index) {
					const elLinkTarget = this._elButtons[index];
					elLinkTarget ? this.show(elLinkTarget) : null;
				};
				_events() {
					this._elTabs.addEventListener('click', (e) => {
						const target = e.target.closest('.tabs__btn');
						if (this._elButtons.includes(target)) {
						e.preventDefault();
						this.show(target);
						}
					});
				}
			}

			// получаем элемент
			const elTabs = document.querySelector('#tabs');
			// инициализируем элемент как ItcTabs
			const tabs = new ItcTabs(elTabs);

			const data = {
				'tabs1-1': [
					{ title: 'Вкладка 1.1', content: 'Содержимое 1.1...' },
					{ title: 'Вкладка 1.2', content: 'Содержимое 1.2...' },
					{ title: 'Вкладка 1.3', content: 'Содержимое 1.3...' },
					{ title: 'Вкладка 1.4', content: 'Содержимое 1.4...' },
				],
				'tabs1-2': [
					{ title: 'Вкладка 2.1', content: 'Содержимое 2.1...' },
					{ title: 'Вкладка 2.2', content: 'Содержимое 2.2...' },
					{ title: 'Вкладка 2.3', content: 'Содержимое 2.3...' },
					{ title: 'Вкладка 2.4', content: 'Содержимое 2.4...' },
				],
				'tabs1-3': [
					{ title: 'Вкладка 3.1', content: 'Содержимое 3.1...' },
					{ title: 'Вкладка 3.2', content: 'Содержимое 3.2...' },
					{ title: 'Вкладка 3.3', content: 'Содержимое 3.3...' },
					{ title: 'Вкладка 3.4', content: 'Содержимое 3.4...' },
				]
			}

			const addTabs = (id, data, target) => {
				let output = `<div {{id}} class="tabs">
					<div class="tabs__nav">{{buttons}}</div>
					<div class="tabs__content">{{content}}</div>
				</div>`;
				output = output.replace('{{id}}', `id="${id}"`);
				let buttons = '';
				let content = '';
				data.forEach(item => {
					buttons += `<button class="tabs__btn">${item['title']}</button>`;
					content += `<div class="tabs__pane">${item['content']}</div>`;
				})
				output = output.replace('{{buttons}}', buttons);
				output = output.replace('{{content}}', content);
				if (!target.querySelector(`#${id}`)) {
					target.insertAdjacentHTML('beforeend', output);
					const tab = new ItcTabs(`#${id}`);
					tab.showByIndex(0);
				}
			}

			if (elTabs.querySelector('.tabs__pane_show')) {
				elTabs.querySelectorAll('.tabs__pane_show').forEach(el => {
					const id = el.id.replace('tab', 'tabs');
					if (data[id]) {
						addTabs(id, data[id], el);
					}
				});
			}

			elTabs.addEventListener('tab.itc.change', () => {
				elTabs.querySelectorAll('.tabs__pane_show').forEach(el => {
					const id = el.id.replace('tab', 'tabs');
					if (data[id]) {
						addTabs(id, data[id], el);
					}
				});
			})
		</script>
	</body>
</html>
-->