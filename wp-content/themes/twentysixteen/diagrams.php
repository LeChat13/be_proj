<?php
	/*
		Template Name: Diagrams
	*/
?>
<html data-wf-domain="sergs-dandy-project-ed4856.webflow.io" data-wf-page="5ff5f07a32cce7ce2ea915ee" data-wf-site="5ff5f07a32cce789a5a915ed" data-wf-status="1" class=" w-mod-js">

<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>Main</title>
    <meta content="Main" property="og:title">
    <meta content="Main" property="twitter:title">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <script type="text/javascript">
        ! function(o, c) {
            var n = c.documentElement,
                t = " w-mod-";
            n.className += t + "js", ("ontouchstart" in o || o.DocumentTouch && c instanceof DocumentTouch) && (n.className += t + "touch")
        }(window, document);
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.7.0"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.13.5/xlsx.full.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript">
        'use strict';

        //window.BUCodes = ['US', 'ABA ', 'ALV', 'CHMV CHECK ', 'CRS', 'CT', 'CV', 'DECG', 'DXR', 'FLUORO', 'FM', 'HOLTER', 'ICCA', 'INFANT RESUSCITATION SYSTEMS', 'IT', 'MAMMO', 'MR', 'NAVI', 'NEONATAL INCUBATOR', 'NEONATAL PHOTOTHERAPY', 'PM', 'SPECT', 'SPECT/CT', 'SURGERY', 'TP', ];
        //window.BUNames = ['Ultrasound', 'ABA ', 'ALV', 'CHMV CHECK ', 'CRS', 'CT', 'CV', 'DECG', 'DXR', 'FLUORO', 'FM', 'HOLTER', 'ICCA', 'INFANT RESUSCITATION SYSTEMS', 'IT', 'MAMMO', 'MR', 'NAVI', 'NEONATAL INCUBATOR', 'NEONATAL PHOTOTHERAPY', 'PM', 'SPECT', 'SPECT/CT', 'SURGERY', 'TP', ];
        window.BUCodes = ['US', 'ALV', 'CRS', 'CT', 'CV', 'DECG', 'DXR', 'FM', 'HOLTER', 'ICCA', 'IT', 'MR', 'PM', 'SPECT', 'SPECT/CT', 'SURGERY', 'TP', ];
        window.BUNames = ['Ultrasound', 'ALV', 'CRS', 'CT', 'CV', 'DECG', 'DXR', 'FM', 'HOLTER', 'ICCA', 'IT', 'MR', 'PM', 'SPECT', 'SPECT/CT', 'SURGERY', 'TP', ];
        window.MarkCodes = ['САМСУНГ МЕДИСОН КО.; ЛТД.', ];
        window.MarkNames = ['SAMSUNG', ];
        window.MonthsToNumbers = {
            'Январь': '0',
            'Февраль': '1',
            'Март': '2',
            'Апрель': '3',
            'Май': '4',
            'Июнь': '5',
            'Июль': '6',
            'Август': '7',
            'Сентябрь': '8',
            'Октябрь': '9',
            'Ноябрь': '10',
            'Декабрь': '11',
        };
        window.MonthsForLegend = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        window.ProdSegData = {
            'Premium cart 8M+': {
                name: 'Premium cart 8M+',
                color: 'rgba(7, 93, 144, 1)',
            },
            'High-end cart 4-8M': {
                name: 'High-end cart 4-8M',
                color: 'rgba(112, 173, 70, 1)',
            },
            'Low-end cart 0-4M': {
                name: 'Low-end cart 0-4M',
                color: 'rgba(68, 115, 197, 1)',
            },
            'High-end compact 4M+': {
                name: 'High-end compact 4M+',
                color: 'rgba(255, 192, 0, 1)',
            },
            'Low-end compact 0-4M': {
                name: 'Low-end compact 0-4M',
                color: 'rgba(165, 165, 165, 1)',
            },
            'Handheld': {
                name: 'Handheld',
                color: 'rgba(238, 125, 49, 1)',
            },
            'U.M.': {
                name: 'U.M.',
                color: 'rgba(255, 0, 0, 1)',
            },
            'null': {
                name: '',
                color: 'rgba(255, 0, 255, 1)',
            },
            '~': {
                name: '~',
                color: 'rgba(90, 155, 213, 1)',
            },
            'High': {
                name: 'High',
                color: 'rgba(112, 173, 70, 1)',
            },
            'Mid High': {
                name: 'Mid High',
                color: 'rgba(68, 115, 197, 1)',
            },
            'Mid': {
                name: 'Mid',
                color: 'rgba(255, 192, 0, 1)',
            },
            'Mid Low': {
                name: 'Mid Low',
                color: 'rgba(165, 165, 165, 1)',
            },
            'Low': {
                name: 'Low',
                color: 'rgba(238, 125, 49, 1)',
            },
            'SURGERY': {
                name: 'SURGERY',
                color: 'rgba(238, 125, 49, 1)',
            },
            'RTS': {
                name: 'RTS',
                color: 'rgba(238, 125, 49, 1)',
            },
            'МОБИЛЬНЫЙ': {
                name: 'МОБИЛЬНЫЙ',
                color: 'rgba(165, 165, 165, 1)',
            },
            'МРТ-СОВМЕСТИМЫЙ': {
                name: 'МРТ-СОВМЕСТИМЫЙ',
                color: 'rgba(238, 125, 49, 1)',
            },
            'AUTOMATED CRS': {
                name: 'AUTOMATED CRS',
                color: 'rgba(165, 165, 165, 1)',
            },
            'CRS MONITOR': {
                name: 'CRS MONITOR',
                color: 'rgba(238, 125, 49, 1)',
            },
            'BIPLAN': {
                name: 'BIPLAN',
                color: 'rgba(255, 192, 0, 1)',
            },
            'HYBRID': {
                name: 'HYBRID',
                color: 'rgba(165, 165, 165, 1)',
            },
            'SINGLE-PLAN': {
                name: 'SINGLE-PLAN',
                color: 'rgba(238, 125, 49, 1)',
            },
            '12 CHANNELS': {
                name: '12 CHANNELS',
                color: 'rgba(255, 192, 0, 1)',
            },
            '3-6 CHANNELS': {
                name: '3-6 CHANNELS',
                color: 'rgba(165, 165, 165, 1)',
            },
            '1 CHANNEL': {
                name: '1 CHANNEL',
                color: 'rgba(238, 125, 49, 1)',
            },
            'ALV': {
                name: 'ALV',
                color: 'rgba(68, 115, 197, 1)',
            },
            'PORTABLE': {
                name: 'PORTABLE',
                color: 'rgba(255, 192, 0, 1)',
            },
            'НЕОНАТАЛЬНЫЙ': {
                name: 'НЕОНАТАЛЬНЫЙ',
                color: 'rgba(165, 165, 165, 1)',
            },
            'ТРАНСПОРТНЫЙ': {
                name: 'ТРАНСПОРТНЫЙ',
                color: 'rgba(238, 125, 49, 1)',
            },
            'transport': {
                name: 'transport',
                color: 'rgba(112, 173, 70, 1)',
            },
            'hospital': {
                name: 'hospital',
                color: 'rgba(7, 93, 144, 1)',
            },
            'home/hospital/transport': {
                name: 'home/hospital/transport',
                color: 'rgba(0, 255, 255, 1)',
            },
            'hospital/transport': {
                name: 'hospital/transport',
                color: 'rgba(0, 255, 0, 1)',
            },
            'ЭКМО': {
                name: 'ЭКМО',
                color: 'rgba(127, 127, 127, 1)',
            },
            'home/hospital': {
                name: 'home/hospital',
                color: 'rgba(0, 127, 127, 1)',
            },
            'home': {
                name: 'home',
                color: 'rgba(127, 127, 0, 1)',
            },
            'не производят ивл': {
                name: 'не производят ивл',
                color: 'rgba(255, 255, 0, 1)',
            },
            'наркозный аппарат': {
                name: 'наркозный аппарат',
                color: 'rgba(0, 0, 255, 1)',
            },
            'not for patient use': {
                name: 'not for patient use',
                color: 'rgba(197, 255, 10, 1)',
            },
            //rgba(000, 255, 000, 0.4)
            'АППАРАТ ДЛЯ ОЧИЩЕНИЯ ДЫХАТЕЛЬНЫХ ПУТЕЙ': {
                name: 'АППАРАТ ДЛЯ ОЧИЩЕНИЯ ДЫХАТЕЛЬНЫХ ПУТЕЙ',
                color: 'rgba(128, 000, 255, 0.7)',
            },
            'это не ивл': {
                name: 'это не ивл',
                color: 'rgba(205, 197, 10, 1)',
            },
            'MAMMO': {
                name: 'MAMMO',
                color: 'rgba(68, 115, 197, 1)',
            },
            'MAMMO + CR': {
                name: 'MAMMO + CR',
                color: 'rgba(255, 192, 0, 1)',
            },
            'MAMMO ANALOG': {
                name: 'MAMMO ANALOG',
                color: 'rgba(165, 165, 165, 1)',
            },
            'MAMMO DIGITAL': {
                name: 'MAMMO DIGITAL',
                color: 'rgba(238, 125, 49, 1)',
            },
            'MOBILE.POWER >38': {
                name: 'MOBILE.POWER >38',
                color: 'rgba(0, 255, 0, 1)',
            },
            'MOBILE.POWER 20-38': {
                name: 'MOBILE.POWER 20-38',
                color: 'rgba(0, 255, 255, 1)',
            },
            'MOBILE.POWER <20': {
                name: 'MOBILE.POWER <20',
                color: 'rgba(7, 93, 144, 1)',
            },
            'MOBILE.POWER ~': {
                name: 'MOBILE.POWER ~',
                color: 'rgba(112, 173, 70, 1)',
            },
            'FLUORO.ANALOG': {
                name: 'FLUORO.ANALOG',
                color: 'rgba(68, 115, 197, 1)',
            },
            'FLUORO.DIGITAL': {
                name: 'FLUORO.DIGITAL',
                color: 'rgba(255, 192, 0, 1)',
            },
            'RAD ANALOG': {
                name: 'RAD ANALOG',
                color: 'rgba(165, 165, 165, 1)',
            },
            'RAD DIGITAL': {
                name: 'RAD DIGITAL',
                color: 'rgba(238, 125, 49, 1)',
            },
            '7T': {
                name: '7T',
                color: 'rgba(127, 127, 127, 1)',
            },
            '3.0T': {
                name: '3.0T',
                color: 'rgba(0, 255, 0, 1)',
            },
            '1.5T': {
                name: '1.5T',
                color: 'rgba(0, 255, 255, 1)',
            },
            '1.0T': {
                name: '1.0T',
                color: 'rgba(7, 93, 144, 1)',
            },
            '0.4T': {
                name: '0.4T',
                color: 'rgba(112, 173, 70, 1)',
            },
            '0.35Т': {
                name: '0.35Т',
                color: 'rgba(68, 115, 197, 1)',
            },
            '0.32T': {
                name: '0.32T',
                color: 'rgba(255, 192, 0, 1)',
            },
            '0.3Т': {
                name: '0.3Т',
                color: 'rgba(165, 165, 165, 1)',
            },
            '0.25Т': {
                name: '0.25Т',
                color: 'rgba(238, 125, 49, 1)',
            },
            '640 SLICE': {
                name: '640 SLICE',
                color: 'rgba(0, 0, 255, 1)',
            },
            '512 SLICE': {
                name: '512 SLICE',
                color: 'rgba(255, 255, 0, 1)',
            },
            '384 SLICE': {
                name: '384 SLICE',
                color: 'rgba(127, 127, 0, 1)',
            },
            '256 SLICE': {
                name: '256 SLICE',
                color: 'rgba(0, 127, 127, 1)',
            },
            '192 SLICE': {
                name: '192 SLICE',
                color: 'rgba(127, 127, 127, 1)',
            },
            '160 SLICE': {
                name: '160 SLICE',
                color: 'rgba(0, 255, 0, 1)',
            },
            '128 SLICE': {
                name: '128 SLICE',
                color: 'rgba(0, 255, 255, 1)',
            },
            '80 SLICE': {
                name: '80 SLICE',
                color: 'rgba(7, 93, 144, 1)',
            },
            '64 SLICE': {
                name: '64 SLICE',
                color: 'rgba(112, 173, 70, 1)',
            },
            '60 SLICE': {
                name: '60 SLICE',
                color: 'rgba(68, 115, 197, 1)',
            },
            '32 SLICE': {
                name: '32 SLICE',
                color: 'rgba(255, 192, 0, 1)',
            },
            '16 SLICE': {
                name: '16 SLICE',
                color: 'rgba(165, 165, 165, 1)',
            },
            '6 SLICE': {
                name: '6 SLICE',
                color: 'rgba(238, 125, 49, 1)',
            },
        };
        window.DirIndirData = {
            'Direct': {
                name: 'Direct',
                color: 'rgba(84, 150, 211, 1)',
            },
            'Indirect': {
                name: 'Indirect',
                color: 'rgba(237, 125, 49, 1)',
            },
        };
        window.BrandsData = {
            '3DISC': {
                name: '3DISC',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ALARIS MEDICAL SYSTEMS': {
                name: 'ALARIS MEDICAL SYSTEMS',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'APPLIED BIOMEDICAL SYSTEMS': {
                name: 'APPLIED BIOMEDICAL SYSTEMS',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ARMSTRONG MEDICAL LIMITED': {
                name: 'ARMSTRONG MEDICAL LIMITED',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ATCOR MEDICAL PTY. LTD.': {
                name: 'ATCOR MEDICAL PTY. LTD.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'BCF TECHNOLOGY': {
                name: 'BCF TECHNOLOGY',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'BEIJING ETERNITY ELECTRONIC TECHNOLOGY CO., LTD': {
                name: 'BEIJING ETERNITY ELECTRONIC TECHNOLOGY CO., LTD',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'BEIJING M&B ELECTRONIC INSTRUMENTS': {
                name: 'BEIJING M&B ELECTRONIC INSTRUMENTS',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'BEIJING READ EAGLE TECHNOLOGY CO': {
                name: 'BEIJING READ EAGLE TECHNOLOGY CO',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'BENFORD SCIENTIDIC LTD': {
                name: 'BENFORD SCIENTIDIC LTD',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'BIOMED DEVICES INC.': {
                name: 'BIOMED DEVICES INC.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'BIOSPACE CO., LTD': {
                name: 'BIOSPACE CO., LTD',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'BIOTRONIK SE & Co. KG': {
                name: 'BIOTRONIK SE & Co. KG',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'BITMOS GMBH': {
                name: 'BITMOS GMBH',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'BMC MEDICAL': {
                name: 'BMC MEDICAL',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'COMPUMEDICS GERMANY GMBH': {
                name: 'COMPUMEDICS GERMANY GMBH',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'COSMED S.R.L.': {
                name: 'COSMED S.R.L.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'CWE INC': {
                name: 'CWE INC',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'DATASCOPE': {
                name: 'DATASCOPE',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'DEPO': {
                name: 'DEPO',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'DRAMINSKI': {
                name: 'DRAMINSKI',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'DRTECH': {
                name: 'DRTECH',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'E.I. MEDICAL IMAGING': {
                name: 'E.I. MEDICAL IMAGING',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ERICH JAEGER': {
                name: 'ERICH JAEGER',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'FANEM': {
                name: 'FANEM',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'FIAGON GMBH': {
                name: 'FIAGON GMBH',
                color: 'rgba(255, 000, 255, 0.4)',
            },
            'FLEXICARE MEDICAL LTD.': {
                name: 'FLEXICARE MEDICAL LTD.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'GEMSS MEDICAL SYSTEMS': {
                name: 'GEMSS MEDICAL SYSTEMS',
                color: 'rgba(255, 000, 128, 0.4)',
            },
            'GMM': {
                name: 'GMM',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'HEACO': {
                name: 'HEACO',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'HEALCERION CO., LTD': {
                name: 'HEALCERION CO., LTD',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'HUGO SACHS': {
                name: 'HUGO SACHS',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'HYUN DAI MEDICAL X-RAY CO., LTD.': {
                name: 'HYUN DAI MEDICAL X-RAY CO., LTD.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'IMAGE INFORMETION SYSTEMS EUROPE GMBH': {
                name: 'IMAGE INFORMETION SYSTEMS EUROPE GMBH',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'INTRASENSE SAS': {
                name: 'INTRASENSE SAS',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'JOLIFE AB': {
                name: 'JOLIFE AB',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'KAIXIN INDUSTRIAL CO., LTD.': {
                name: 'KAIXIN INDUSTRIAL CO., LTD.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'KONICA MINOLTA MEDICAL & GRAPHIC': {
                name: 'KONICA MINOLTA MEDICAL & GRAPHIC',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'LISTEM': {
                name: 'LISTEM',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'MAGNAMED TECNOLOGIA MÉDICA S/A': {
                name: 'MAGNAMED TECNOLOGIA MÉDICA S/A',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'MAKE AG': {
                name: 'MAKE AG',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'MASIMO': {
                name: 'MASIMO',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'MEDECO': {
                name: 'MEDECO',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'MEDIGATE INC': {
                name: 'MEDIGATE INC',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'MEDIXANT': {
                name: 'MEDIXANT',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'MED-RAY': {
                name: 'MED-RAY',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'MOBIUS IMAGING. LLC': {
                name: 'MOBIUS IMAGING. LLC',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'MODUL GRUP MUHENDISLIK LTD': {
                name: 'MODUL GRUP MUHENDISLIK LTD',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'MOELLER MEDICAL': {
                name: 'MOELLER MEDICAL',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'MR SOLUTIONS': {
                name: 'MR SOLUTIONS',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'MULTIMAGE S.R.L': {
                name: 'MULTIMAGE S.R.L',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'MULTISCAN': {
                name: 'MULTISCAN',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'NATUS': {
                name: 'NATUS',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'NEPHTYS INTERNATIONAL CDRH': {
                name: 'NEPHTYS INTERNATIONAL CDRH',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'NORDICNEUROLAB AS': {
                name: 'NORDICNEUROLAB AS',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'NOVADAQ TECHNOLOGIES INC.': {
                name: 'NOVADAQ TECHNOLOGIES INC.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'OLYMPUS': {
                name: 'OLYMPUS',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'OOO ТЕХНОМАРКЕТ': {
                name: 'OOO ТЕХНОМАРКЕТ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'PANLAB': {
                name: 'PANLAB',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'PERKINELMER': {
                name: 'PERKINELMER',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'PHASITRON': {
                name: 'PHASITRON',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'PIOWAY': {
                name: 'PIOWAY',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'POSCOM': {
                name: 'POSCOM',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'RAMSEY MEDICAL': {
                name: 'RAMSEY MEDICAL',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'RESVENT MEDICAL TECHNOLOGY CO., LTD': {
                name: 'RESVENT MEDICAL TECHNOLOGY CO., LTD',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'RIMED': {
                name: 'RIMED',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ROESYS GMBH': {
                name: 'ROESYS GMBH',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'SEFAM': {
                name: 'SEFAM',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'SEOIL PACIFIC': {
                name: 'SEOIL PACIFIC',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'SHENZHEN ANKE HIGH-TECH CO. LTD': {
                name: 'SHENZHEN ANKE HIGH-TECH CO. LTD',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'SHENZHEN CHENWEI ELECTRONIC CO., LTD': {
                name: 'SHENZHEN CHENWEI ELECTRONIC CO., LTD',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'SHENZHEN CREATIVE INDUSTRY': {
                name: 'SHENZHEN CREATIVE INDUSTRY',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'SHENZHEN HEXIN ZONDAN MEDICAL EQUIPMENT': {
                name: 'SHENZHEN HEXIN ZONDAN MEDICAL EQUIPMENT',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'SOFIE BIOSCIENCE': {
                name: 'SOFIE BIOSCIENCE',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'SOMNOMEDICS': {
                name: 'SOMNOMEDICS',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'SOMO INTERNATIONAL CO': {
                name: 'SOMO INTERNATIONAL CO',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'SONOWIN SCI-TECH INC': {
                name: 'SONOWIN SCI-TECH INC',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'SPEKTROMED': {
                name: 'SPEKTROMED',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'SUN MICROSYSTEMS': {
                name: 'SUN MICROSYSTEMS',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'TECME S.A.': {
                name: 'TECME S.A.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'U.M.': {
                name: 'U.M.',
                color: 'rgba(197, 255, 10, 1)',
            },
            'UGO BASILE': {
                name: 'UGO BASILE',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ULTRASONIX MEDICAL CORPORATION': {
                name: 'ULTRASONIX MEDICAL CORPORATION',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'VIEWORKS CO., LTD': {
                name: 'VIEWORKS CO., LTD',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'VOTEM': {
                name: 'VOTEM',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'WESTMED': {
                name: 'WESTMED',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'WUHAN ZONCARE BIO-MEDICAL ELECTRONICS CO., LTD': {
                name: 'WUHAN ZONCARE BIO-MEDICAL ELECTRONICS CO., LTD',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'XUZHOU KERNEL MEDICAL EQUIPMENT CO., LTD': {
                name: 'XUZHOU KERNEL MEDICAL EQUIPMENT CO., LTD',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'YUWELL EUROPE': {
                name: 'YUWELL EUROPE',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ZOOMED': {
                name: 'ZOOMED',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'АГФА ХЭЛСКЕА Н.В.': {
                name: 'АГФА ХЭЛСКЕА Н.В.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'АДАНИ': {
                name: 'АДАНИ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'АКСИОН-ХОЛДИНГ ИЖЕВСКИЙ МОТОЗАВОД; ОАО': {
                name: 'АКСИОН-ХОЛДИНГ ИЖЕВСКИЙ МОТОЗАВОД; ОАО',
                color: 'rgba(205, 197, 10, 1)',
                //                color: 'rgba(255, 255, 000, 0.1)',
            },
            'АКТЮБРЕНТГЕН АО': {
                name: 'АКТЮБРЕНТГЕН АО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'АКУТРОНИК МЕДИКАЛ СИСТЕМС АГ': {
                name: 'АКУТРОНИК МЕДИКАЛ СИСТЕМС АГ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'АЛПИНИОН МЕДИКАЛ СИСТЕМЗ КО.; ЛТД.': {
                name: 'АЛПИНИОН МЕДИКАЛ СИСТЕМЗ КО.; ЛТД.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'АЛЬТОНИКА ООО': {
                name: 'АЛЬТОНИКА ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'АМИКО; ЗАО': {
                name: 'АМИКО; ЗАО',
                color: 'rgba(000, 255, 000, 0.4)',
            },
            'АНМЕДИК АБ': {
                name: 'АНМЕДИК АБ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'АО КАМПО': {
                name: 'АО КАМПО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'АО НПО МОБИЛЬНЫЕ КЛИНИКИ': {
                name: 'АО НПО МОБИЛЬНЫЕ КЛИНИКИ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'АО ЦЕНТРАЛЬНОЕ КОНСТРУКТОРСКОЕ БЮРО ИНФОРМАЦИОННО-УПРАВЛЯЮЩИХ СИСТЕМ': {
                name: 'АО ЦЕНТРАЛЬНОЕ КОНСТРУКТОРСКОЕ БЮРО ИНФОРМАЦИОННО-УПРАВЛЯЮЩИХ СИСТЕМ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'АОКАЙ МЕДИКАЛ ЭКВИПМЕНТ КО; ЛТД': {
                name: 'АОКАЙ МЕДИКАЛ ЭКВИПМЕНТ КО; ЛТД',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'АПЕКСМЕД ИНТЕРНЭШНЛ': {
                name: 'АПЕКСМЕД ИНТЕРНЭШНЛ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'АРДО МЕДИКАЛ АГ': {
                name: 'АРДО МЕДИКАЛ АГ',
                color: 'rgba(000, 000, 255, 0.4)',
            },
            'АРМЕД/ДЖАНГСУ ЮЮ МЕДИКАЛ ЭКВИПМЕНТ ЭНД САППЛАЙ КО.; ЛТД.': {
                name: 'АРМЕД/ДЖАНГСУ ЮЮ МЕДИКАЛ ЭКВИПМЕНТ ЭНД САППЛАЙ КО.; ЛТД.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'АСК ООО': {
                name: 'АСК ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'АСПЕЛ С.А.': {
                name: 'АСПЕЛ С.А.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'АССОЦИАЦИЯ МЕДИЦИНСКИХ ФИЗИКОВ': {
                name: 'АССОЦИАЦИЯ МЕДИЦИНСКИХ ФИЗИКОВ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'АТЕС МЕДИКА ДЕВАЙС': {
                name: 'АТЕС МЕДИКА ДЕВАЙС',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'АТОМ МЕДИКЭЛ КО': {
                name: 'АТОМ МЕДИКЭЛ КО',
                color: 'rgba(000, 000, 255, 0.1)',
            },
            'АЭЛИТА ООО': {
                name: 'АЭЛИТА ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'АЭРОМЕД; ООО': {
                name: 'АЭРОМЕД; ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'АЭРОТЕЛЬ МЕДИКАЛ СИСТЕМС; ЛТД': {
                name: 'АЭРОТЕЛЬ МЕДИКАЛ СИСТЕМС; ЛТД',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'БАРД': {
                name: 'БАРД',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'БЕЙДЖИНГ ЭМ ЭНД БИ ЭЛЕКТРОНИК ИНСТРУМЕНТС КО.; ЛТД.': {
                name: 'БЕЙДЖИНГ ЭМ ЭНД БИ ЭЛЕКТРОНИК ИНСТРУМЕНТС КО.; ЛТД.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'БЕКСЕН': {
                name: 'БЕКСЕН',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'БЕМЕМС КО.; ЛТД.': {
                name: 'БЕМЕМС КО.; ЛТД.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'БИОКЕА': {
                name: 'БИОКЕА',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'БИОНЕТ ЛТД.': {
                name: 'БИОНЕТ ЛТД.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'БИОПРИБОР': {
                name: 'БИОПРИБОР',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'БИСТОС КО; ЛТД.': {
                name: 'БИСТОС КО; ЛТД.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'БК МЕДИКАЛ АПС': {
                name: 'БК МЕДИКАЛ АПС',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'БМИ БИОМЕДИКАЛ ИНТЕРНЕШНЛ; С.Р.Л.': {
                name: 'БМИ БИОМЕДИКАЛ ИНТЕРНЕШНЛ; С.Р.Л.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'БМС МЕДИКАЛ КО ЛТД': {
                name: 'БМС МЕДИКАЛ КО ЛТД',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'БРАЙНЛАБ АГ': {
                name: 'БРАЙНЛАБ АГ',
                color: 'rgba(128, 255, 000, 0.1)',
            },
            'БРЭС МЕДИКАЛ АБ': {
                name: 'БРЭС МЕДИКАЛ АБ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'БТЛ ИНДАСТРИЕС ЛТД.': {
                name: 'БТЛ ИНДАСТРИЕС ЛТД.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ВАЙНМАН МЕДИКЭЛ ТЕХНОЛОДЖИ': {
                name: 'ВАЙНМАН МЕДИКЭЛ ТЕХНОЛОДЖИ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ВАРИАН МЕДИКАЛ СИСТЕМС ИНТЕРНЕЙШНЛ АГ': {
                name: 'ВАРИАН МЕДИКАЛ СИСТЕМС ИНТЕРНЕЙШНЛ АГ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ВЕЙЕР ГМБХ': {
                name: 'ВЕЙЕР ГМБХ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ВИЛЛА СИСТЕМИ МЕДИКАЛИ С.П.А': {
                name: 'ВИЛЛА СИСТЕМИ МЕДИКАЛИ С.П.А',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ВНИИМП-ВИТА; ЗАО': {
                name: 'ВНИИМП-ВИТА; ЗАО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ВОТЕМ': {
                name: 'ВОТЕМ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ВУХАН ЗОНКАРЕ БИО-МЕДИКАЛ ЭЛЕКТРОНИКС ЛТД': {
                name: 'ВУХАН ЗОНКАРЕ БИО-МЕДИКАЛ ЭЛЕКТРОНИКС ЛТД',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ВЭБ БИОТЕКНОЛОДЖИ ПТЕ. ЛТД': {
                name: 'ВЭБ БИОТЕКНОЛОДЖИ ПТЕ. ЛТД',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ВЭЛИТ НПП ООО': {
                name: 'ВЭЛИТ НПП ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ГАМИЛЬТОН МЕДИКАЛ АГ': {
                name: 'ГАМИЛЬТОН МЕДИКАЛ АГ',
                color: 'rgba(255, 128, 000, 0.7)',
            },
            'ГК ГАММАМЕД': {
                name: 'ГК ГАММАМЕД',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ГУАНГДОНГ БИОЛАЙТ МЕДИТЕК КО.; ЛТД.': {
                name: 'ГУАНГДОНГ БИОЛАЙТ МЕДИТЕК КО.; ЛТД.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ГУАНЧЖОУ САНРЕЙ МЕДИКАЛ АППАРАТУС КО ЛТД': {
                name: 'ГУАНЧЖОУ САНРЕЙ МЕДИКАЛ АППАРАТУС КО ЛТД',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ДЖЕМИС МЕДИЦИНСКИЕ СИСТЕМЫ': {
                name: 'ДЖЕМИС МЕДИЦИНСКИЕ СИСТЕМЫ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ДЖЕНЕРАЛ ЭЛЕКТРИК': {
                name: 'GE',
                color: 'rgba(0, 156, 73, 1)',
            },
            'ДЖЕНОРЕЙ КО.; ЛТД.': {
                name: 'ДЖЕНОРЕЙ КО.; ЛТД.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ДЖИНЕВРИ С.Р.Л.': {
                name: 'ДЖИНЕВРИ С.Р.Л.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ДИАМАНТ; ЗАО': {
                name: 'ДИАМАНТ; ЗАО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ДИКСИОН': {
                name: 'ДИКСИОН',
                color: 'rgba(255, 000, 000, 0.1)',
            },
            'ДМС ПЕРЕДОВЫЕ ТЕХНОЛОГИИ ООО': {
                name: 'ДМС ПЕРЕДОВЫЕ ТЕХНОЛОГИИ ООО',
                color: 'rgba(000, 128, 255, 0.4)',
            },
            'ДНК И К': {
                name: 'ДНК И К',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ДОНГМУН КО.; ЛТД': {
                name: 'ДОНГМУН КО.; ЛТД',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ДОСЧАТИНСКИЙ ЗАВОД МЕДИЦИНСКОГО ОБОРУДОВАНИЯ; ОАО': {
                name: 'ДОСЧАТИНСКИЙ ЗАВОД МЕДИЦИНСКОГО ОБОРУДОВАНИЯ; ОАО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ДРЕГЕР МЕДИКЭЛ АГ ЭНД КО.КГ': {
                name: 'ДРЕГЕР МЕДИКЭЛ АГ ЭНД КО.КГ',
                color: 'rgba(000, 255, 255, 0.7)',
            },
            'ЕВРОКОЛАМБУС С.Р.Л.': {
                name: 'ЕВРОКОЛАМБУС С.Р.Л.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ЕВРОЛИНИЯ': {
                name: 'ЕВРОЛИНИЯ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ЕЗОНО АГ': {
                name: 'ЕЗОНО АГ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ЗАВОД ГОРНОСПАСАТЕЛЬНОЙ ТЕХНИКИ ГОРИЗОНТ ПАО': {
                name: 'ЗАВОД ГОРНОСПАСАТЕЛЬНОЙ ТЕХНИКИ ГОРИЗОНТ ПАО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ЗАВОД МЕДИЦИНСКОГО ОБОРУДОВАНИЯ ТАНДЕМ; ООО': {
                name: 'ЗАВОД МЕДИЦИНСКОГО ОБОРУДОВАНИЯ ТАНДЕМ; ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ЗАО БЕРНЕР РОСС МЕДИКАЛ': {
                name: 'ЗАО БЕРНЕР РОСС МЕДИКАЛ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ЗАО ДИАМАНТ': {
                name: 'ЗАО ДИАМАНТ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ЗАО НТФ ПЕРУН': {
                name: 'ЗАО НТФ ПЕРУН',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ЗЕЛЕНОГРАДСКИЙ ИННОВАЦИОННО-ТЕХНОЛОГИЧЕСКИЙ ЦЕНТР МЕДИЦИНСКОЙ ТЕХНИКИ ЗАО': {
                name: 'ЗЕЛЕНОГРАДСКИЙ ИННОВАЦИОННО-ТЕХНОЛОГИЧЕСКИЙ ЦЕНТР МЕДИЦИНСКОЙ ТЕХНИКИ ЗАО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ЗИМ ИМАГИНГ ГМБХ': {
                name: 'ЗИМ ИМАГИНГ ГМБХ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ЗОЛЛ МЕДИКАЛ КОРПОРЕЙШН': {
                name: 'ЗОЛЛ МЕДИКАЛ КОРПОРЕЙШН',
                color: 'rgba(255, 255, 000, 0.4)',
            },
            'ЗОНАРЕ МЕДИКАЛ СИСТЕМЗ; ИНК.': {
                name: 'ЗОНАРЕ МЕДИКАЛ СИСТЕМЗ; ИНК.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ИБП РАН': {
                name: 'ИБП РАН',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ИВЕНТ МЕДИКЭЛ ЛИМИТЕД': {
                name: 'ИВЕНТ МЕДИКЭЛ ЛИМИТЕД',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ИМС (ИНТЕРНАЦИОНАЛЕ МЕДИКО СЕНТИФИКА) С.Р.Л.': {
                name: 'ИМС (ИНТЕРНАЦИОНАЛЕ МЕДИКО СЕНТИФИКА) С.Р.Л.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ИНКАРТ ЗАО': {
                name: 'ИНКАРТ ЗАО',
                color: 'rgba(000, 128, 255, 0.1)',
            },
            'ИННОМЕД МЕДИКАЛ; ИНК.': {
                name: 'ИННОМЕД МЕДИКАЛ; ИНК.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ИНТЕГРА ЛАЙФСАЙНС': {
                name: 'ИНТЕГРА ЛАЙФСАЙНС',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ИНТЕРМЕД ЭКВИПАМЕНТО МЕДИКО ХОСПИТАЛАР; ЛТД.': {
                name: 'ИНТЕРМЕД ЭКВИПАМЕНТО МЕДИКО ХОСПИТАЛАР; ЛТД.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ИНТЕРНЕШНЛ БИОМЕДИКАЛ; ЛТД': {
                name: 'ИНТЕРНЕШНЛ БИОМЕДИКАЛ; ЛТД',
                color: 'rgba(255, 000, 128, 1.0)',
            },
            'ИТАЛРЭЙ (ИБИС); С.Р.Л.': {
                name: 'ИТАЛРЭЙ (ИБИС); С.Р.Л.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'КАДЕН ЯСЕН МЕДИКАЛ ЭЛЕКТРОНИКС КО.; ЛТД.': {
                name: 'КАДЕН ЯСЕН МЕДИКАЛ ЭЛЕКТРОНИКС КО.; ЛТД.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'КАНМЕД А.Б.': {
                name: 'КАНМЕД А.Б.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'КАРДЕКС ООО': {
                name: 'КАРДЕКС ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'КАРДИАК САЙНС ИНК.': {
                name: 'КАРДИАК САЙНС ИНК.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'КАРДИЕТТЕ (ЭЙЧ АНД СИ МЕДИКАЛ ДЕВАЙСЕЗ)': {
                name: 'КАРДИЕТТЕ (ЭЙЧ АНД СИ МЕДИКАЛ ДЕВАЙСЕЗ)',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'КАРДИО ИНК': {
                name: 'КАРДИО ИНК',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'КАРДИОЛАЙН': {
                name: 'КАРДИОЛАЙН',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'КАРЛ РЕЙНЕР; ГМБХ': {
                name: 'КАРЛ РЕЙНЕР; ГМБХ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'КЕРНЕЛ МЕДИКАЛ ЭКВИПМЕНТ КО.; ЛТД.': {
                name: 'КЕРНЕЛ МЕДИКАЛ ЭКВИПМЕНТ КО.; ЛТД.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'КОБАМС С.Р.Л': {
                name: 'КОБАМС С.Р.Л',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'КОВИДИЕН': {
                name: 'КОВИДИЕН',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'КОМПАНИЯ АЛАЙФ-МЕД; ООО': {
                name: 'КОМПАНИЯ АЛАЙФ-МЕД; ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'КОМПАНИЯ НЕО; ООО': {
                name: 'КОМПАНИЯ НЕО; ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'КРАСНОГВАРДЕЕЦ; ОАО': {
                name: 'КРАСНОГВАРДЕЕЦ; ОАО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'КСИМЕД ООО': {
                name: 'КСИМЕД ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'КУСТО МЕД ГМБХ': {
                name: 'КУСТО МЕД ГМБХ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'КЭАРСТРИМ ХЭЛС; ИНК.': {
                name: 'КЭАРСТРИМ ХЭЛС; ИНК.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'КЭЯФЬЮЖЕН': {
                name: 'КЭЯФЬЮЖЕН',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ЛАНАМЕДИКА ЗАО': {
                name: 'ЛАНАМЕДИКА ЗАО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'МАГНАМЕД ТЕХНОЛОГИЯ МЕДИКА': {
                name: 'МАГНАМЕД ТЕХНОЛОГИЯ МЕДИКА',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'МАКЕ': {
                name: 'МАКЕ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'МДОЛОРИС МЕДИКАЛ СИСТЕМС': {
                name: 'МДОЛОРИС МЕДИКАЛ СИСТЕМС',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'МЕДАППАРАТУРА КПО': {
                name: 'МЕДАППАРАТУРА КПО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'МЕДЕЛА АГ': {
                name: 'МЕДЕЛА АГ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'МЕДИАНА ИНК. КО.; ЛТД.': {
                name: 'МЕДИАНА ИНК. КО.; ЛТД.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'МЕДИЕН ИНТЕРНЕЙШНЛ': {
                name: 'МЕДИЕН ИНТЕРНЕЙШНЛ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'МЕДИКАЛ ДЕВАЙС МЕНЕДЖМЕНТ ЛТД': {
                name: 'МЕДИКАЛ ДЕВАЙС МЕНЕДЖМЕНТ ЛТД',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'МЕДИКАЛ ЭКОНЕТ ГМБХ': {
                name: 'МЕДИКАЛ ЭКОНЕТ ГМБХ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'МЕДИКОМ; ЗАО': {
                name: 'МЕДИКОМ; ЗАО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'МЕДИКОР ЭЛЕКТРОНИКА ЗРТ': {
                name: 'МЕДИКОР ЭЛЕКТРОНИКА ЗРТ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'МЕДИН МЕДИКЭЛ ИННОВЭЙШЕН ГМБХ': {
                name: 'МЕДИН МЕДИКЭЛ ИННОВЭЙШЕН ГМБХ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'МЕДИПРЕМА С.А.': {
                name: 'МЕДИПРЕМА С.А.',
                color: 'rgba(128, 128, 128, 0.7)',
            },
            'МЕДИТЕК ЗНАМЯ ТРУДА ЗАО': {
                name: 'МЕДИТЕК ЗНАМЯ ТРУДА ЗАО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'МЕДИТЕХ ЛТД.': {
                name: 'МЕДИТЕХ ЛТД.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'МЕДИЦИНСКИЕ СИСТЕМЫ ООО': {
                name: 'МЕДИЦИНСКИЕ СИСТЕМЫ ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'МЕДИЦИНСКИЕ ТЕЛЕМЕТРИЧЕСКИЕ СИСТЕМЫ ООО': {
                name: 'МЕДИЦИНСКИЕ ТЕЛЕМЕТРИЧЕСКИЕ СИСТЕМЫ ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'МЕДИЦИНСКИЕ ТЕХНОЛОГИИ ЛТД. (МТЛ); ЗАО': {
                name: 'МЕДИЦИНСКИЕ ТЕХНОЛОГИИ ЛТД. (МТЛ); ЗАО',
                color: 'rgba(000, 255, 128, 0.7)',
            },
            'МЕДКАР НПО ЗАО': {
                name: 'МЕДКАР НПО ЗАО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'МЕДПЛАНТ ООО': {
                name: 'МЕДПЛАНТ ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'МЕДПРОМ НПО': {
                name: 'МЕДПРОМ НПО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'МЕДПРОМИНЖИНИРИНГ': {
                name: 'МЕДПРОМИНЖИНИРИНГ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'МЕДРЕНТЕХ; СКБ (З-Д МОСРЕНТГЕН)': {
                name: 'МЕДРЕНТЕХ; СКБ (З-Д МОСРЕНТГЕН)',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'МЕДТЕХСЕРВИС ООО': {
                name: 'МЕДТЕХСЕРВИС ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'МЕДТРОНИК': {
                name: 'МЕДТРОНИК',
                color: 'rgba(128, 255, 000, 0.4)',
            },
            'МЕДЭКСПЕРТ НПО ООО': {
                name: 'МЕДЭКСПЕРТ НПО ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'МЕК ИНТЕНСИВ КЕЭ СИСТЕМ': {
                name: 'МЕК ИНТЕНСИВ КЕЭ СИСТЕМ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'МЕРКЬЮРИ МЕДИКАЛ': {
                name: 'МЕРКЬЮРИ МЕДИКАЛ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'МЕТРАКС ГМБХ': {
                name: 'МЕТРАКС ГМБХ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'МИКАРД-ЛАНА ЗАО': {
                name: 'МИКАРД-ЛАНА ЗАО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'МИКРОЛЮКС; ООО': {
                name: 'МИКРОЛЮКС; ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'МИКТО-ИНТЕХ': {
                name: 'МИКТО-ИНТЕХ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'МИНДРЕЙ': {
                name: 'Mindray',
                color: 'rgba(167, 122, 215, 1)',
            },
            'МИТК-М; ООО': {
                name: 'МИТК-М; ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'МКС (МЕДИЦИНСКИЕ КОМПЬЮТЕРНЫЕ СИСТЕМЫ); ООО': {
                name: 'МКС (МЕДИЦИНСКИЕ КОМПЬЮТЕРНЫЕ СИСТЕМЫ); ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'МОБИЛЬНЫЕ КЛИНИНИКИ НПО': {
                name: 'МОБИЛЬНЫЕ КЛИНИНИКИ НПО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'МОНИКА ХЕЛФКЕА; ЛТД': {
                name: 'МОНИКА ХЕЛФКЕА; ЛТД',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'МОНИТОР НПП ООО': {
                name: 'МОНИТОР НПП ООО',
                color: 'rgba(128, 255, 000, 1.0)',
            },
            'МОРТАРА ИНСТРУМЕНТ; ИНК.': {
                name: 'МОРТАРА ИНСТРУМЕНТ; ИНК.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'МОСРЕНТГЕНПРОМ; ООО': {
                name: 'МОСРЕНТГЕНПРОМ; ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'МП НПФ ГАММАМЕД-П; ООО': {
                name: 'МП НПФ ГАММАМЕД-П; ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'МС ВЕСТФАЛИЯ ГМБХ': {
                name: 'МС ВЕСТФАЛИЯ ГМБХ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'НАТУС МЕДИКАЛ ИНК.': {
                name: 'НАТУС МЕДИКАЛ ИНК.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'НАУЧПРИБОР; ЗАО': {
                name: 'НАУЧПРИБОР; ЗАО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'НЕВРОЛОДЖИКА': {
                name: 'НЕВРОЛОДЖИКА',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'НЕЙРОСОФТ ООО': {
                name: 'НЕЙРОСОФТ ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'НЕОВЕНТА МЕДИКАЛ АБ': {
                name: 'НЕОВЕНТА МЕДИКАЛ АБ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'НИКСИ': {
                name: 'НИКСИ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'НИМП ЕСН; ООО': {
                name: 'НИМП ЕСН; ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'НИНГБО ДЭВИД МЕДИКЛ ДЕВАЙС КО.; ЛТД.': {
                name: 'НИНГБО ДЭВИД МЕДИКЛ ДЕВАЙС КО.; ЛТД.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'НИХОН КОДЕН': {
                name: 'НИХОН КОДЕН',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'НМФ НЕЙРОТЕХ': {
                name: 'НМФ НЕЙРОТЕХ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'НОВОС МЕДИКАЛ СИСТЕМС': {
                name: 'НОВОС МЕДИКАЛ СИСТЕМС',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'НОРДАВИНД': {
                name: 'НОРДАВИНД',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'НПП МЕДИЦИНСКАЯ ТЕХНИКА; ООО': {
                name: 'НПП МЕДИЦИНСКАЯ ТЕХНИКА; ООО',
                color: 'rgba(128, 128, 000, 0.4)',
            },
            'НПФ БИОСС; ООО': {
                name: 'НПФ БИОСС; ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'НПФ ДИНАМИКА': {
                name: 'НПФ ДИНАМИКА',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'НПЦ АВТОМАТИКИ И ПРИБОРОСТРОЕНИЯ ИМ. ПИЛЮГИНА ФГУП': {
                name: 'НПЦ АВТОМАТИКИ И ПРИБОРОСТРОЕНИЯ ИМ. ПИЛЮГИНА ФГУП',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'НЬЮПОРТ МЕДИКЭЛ ИНСТРУМЕНТС': {
                name: 'НЬЮПОРТ МЕДИКЭЛ ИНСТРУМЕНТС',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ОКАМЕДИК ООО': {
                name: 'ОКАМЕДИК ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ОКСФОРД ИНСТРУМЕНТ МЕДИКАЛ ЛТД./ХАНТЛЕЙ ХЭЛСКЭА ЛТД.': {
                name: 'ОКСФОРД ИНСТРУМЕНТ МЕДИКАЛ ЛТД./ХАНТЛЕЙ ХЭЛСКЭА ЛТД.',
                color: 'rgba(207, 100, 48, 0.7)',
                //                color: 'rgba(211, 83, 57, 0.7)',
                //                color: 'rgba(000, 255, 128, 1.0)',
            },
            'ОКУЛЮС 2000; ООО': {
                name: 'ОКУЛЮС 2000; ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ООО АНГИОСКАН - ЭЛЕКТРОНИКС': {
                name: 'ООО АНГИОСКАН - ЭЛЕКТРОНИКС',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ООО БИОРС': {
                name: 'ООО БИОРС',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ООО БРМ': {
                name: 'ООО БРМ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ООО ДАНКО': {
                name: 'ООО ДАНКО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ООО ИВНМТ РАМЕНА': {
                name: 'ООО ИВНМТ РАМЕНА',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ООО ИКТ КОНСАЛТИНГ': {
                name: 'ООО ИКТ КОНСАЛТИНГ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ООО ИМЭТ': {
                name: 'ООО ИМЭТ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ООО ИНФЕРУМ': {
                name: 'ООО ИНФЕРУМ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ООО КАРДИОМЕД': {
                name: 'ООО КАРДИОМЕД',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ООО КОМЕТА': {
                name: 'ООО КОМЕТА',
                color: 'rgba(128, 000, 255, 0.1)',
            },
            'ООО КОМПАНИЯ МАКСИМА': {
                name: 'ООО КОМПАНИЯ МАКСИМА',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ООО КОМПАНИЯ ЮВЕНТ': {
                name: 'ООО КОМПАНИЯ ЮВЕНТ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ООО ЛИНС': {
                name: 'ООО ЛИНС',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ООО МИТК-М': {
                name: 'ООО МИТК-М',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ООО НЕЙРОБИОТИКС': {
                name: 'ООО НЕЙРОБИОТИКС',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ООО ПАРТНЕР-ЮГ': {
                name: 'ООО ПАРТНЕР-ЮГ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ООО ПЕРСОНАЛЬНЫЕ КАРДИОСИСТЕМЫ': {
                name: 'ООО ПЕРСОНАЛЬНЫЕ КАРДИОСИСТЕМЫ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ООО ПМП ПРОТОН': {
                name: 'ООО ПМП ПРОТОН',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ООО РЕИННМЕД': {
                name: 'ООО РЕИННМЕД',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ООО РЕНТГЕН-КОМПЛЕКТ': {
                name: 'ООО РЕНТГЕН-КОМПЛЕКТ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ООО СИМТ': {
                name: 'ООО СИМТ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ООО СММ': {
                name: 'ООО СММ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ООО СП МИНИМАКС': {
                name: 'ООО СП МИНИМАКС',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ООО ТЕЛЕОПТИК': {
                name: 'ООО ТЕЛЕОПТИК',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ООО ТПК МЕДИКО': {
                name: 'ООО ТПК МЕДИКО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ООО Ф-МАРКЕТ': {
                name: 'ООО Ф-МАРКЕТ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ООО ЭЛЕКТРОПУЛЬС': {
                name: 'ООО ЭЛЕКТРОПУЛЬС',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ООО ЭЛИТРОНИКА': {
                name: 'ООО ЭЛИТРОНИКА',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ОРЕНМЕД ООО': {
                name: 'ОРЕНМЕД ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ОРИКА ИНК': {
                name: 'ОРИКА ИНК',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ОРИОН МЕДИКАЛ ЛТД': {
                name: 'ОРИОН МЕДИКАЛ ЛТД',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ОРИЧ МЕДИКАЛ ЭКВИПМЕНТ': {
                name: 'ОРИЧ МЕДИКАЛ ЭКВИПМЕНТ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ПАО РОСТЕЛЕКОМ': {
                name: 'ПАО РОСТЕЛЕКОМ',
                color: 'rgba(000, 000, 255, 1.0)',
            },
            'ПАРИТЕТ-РЕНТГЕН; ООО': {
                name: 'ПАРИТЕТ-РЕНТГЕН; ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ПЕНЛОН ЛИМИТЕД': {
                name: 'ПЕНЛОН ЛИМИТЕД',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ПЕРКУСИОНЭЙР КОРПОРЭЙШЕН': {
                name: 'ПЕРКУСИОНЭЙР КОРПОРЭЙШЕН',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ПЕТР ТЕЛЕГИН; ООО': {
                name: 'ПЕТР ТЕЛЕГИН; ООО',
                color: 'rgba(215, 245, 0, 1)',
                //                color: 'rgba(205, 205, 30, 0.4)',
                //                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ПК РЕСПЕКТ-ПЛЮС; ООО': {
                name: 'ПК РЕСПЕКТ-ПЛЮС; ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ПЛАНМЕД ОУ': {
                name: 'ПЛАНМЕД ОУ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ПМП ПРОТОН; ООО': {
                name: 'ПМП ПРОТОН; ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ПНЕВМОПРИБОР ООО': {
                name: 'ПНЕВМОПРИБОР ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ПО ЗАРНИЦА': {
                name: 'ПО ЗАРНИЦА',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ПО УОМЗ; ОАО': {
                name: 'ПО УОМЗ; ОАО',
                color: 'rgba(128, 128, 000, 0.7)',
            },
            'ПО УРАЛЬСКИЙ ОПТИКО-МЕХАНИЧЕСКИЙ ЗАВОД; ОАО': {
                name: 'ПО УР��ЛЬСКИЙ ОПТИКО-МЕХАНИЧЕСКИЙ ЗАВОД; ОАО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ПОНИ; ЗАО': {
                name: 'ПОНИ; ЗАО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ПОСКОМ КО; ЛТД': {
                name: 'ПОСКОМ КО; ЛТД',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ПРОДЖЕТТИ С.Р.Л.': {
                name: 'ПРОДЖЕТТИ С.Р.Л.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ПУЛЬСИОН МЕДИКАЛ СИСТЕМС АГ': {
                name: 'ПУЛЬСИОН МЕДИКАЛ СИСТЕМС АГ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'РАДИАН ЗАО': {
                name: 'РАДИАН ЗАО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'РАДМИР ДП АО НИИР ФИРМА': {
                name: 'РАДМИР ДП АО НИИР ФИРМА',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'РАТЕКС НПП; ООО': {
                name: 'РАТЕКС НПП; ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'РАУМЕДИК АГ': {
                name: 'РАУМЕДИК АГ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'РЕН ИНН МЕД; ООО': {
                name: 'РЕН ИНН МЕД; ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'РЕНМЕДПРОМ ООО': {
                name: 'РЕНМЕДПРОМ ООО',
                color: 'rgba(128, 000, 255, 0.4)',
            },
            'РЕСМЕД': {
                name: 'РЕСМЕД',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'РИМКУС МЕ��ИЦИНТЕХНИК': {
                name: 'РИМКУС МЕДИЦИНТЕХНИК',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'РОЗИНН ЭЛЕКТРОНИКС; ИНК.': {
                name: 'РОЗИНН ЭЛЕКТРОНИКС; ИНК.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'РОССИЙСКАЯ КОМПАНИЯ': {
                name: 'РОССИЙСКАЯ КОМПАНИЯ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'РУДОЛЬФ РИСТЕР ГМБХ': {
                name: 'РУДОЛЬФ РИСТЕР ГМБХ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'С.П. ГЕЛПИК; ООО': {
                name: 'С.П. ГЕЛПИК; ООО',
                color: 'rgba(000, 255, 000, 0.7)',
            },
            'САМСУНГ МЕДИСОН КО.; ЛТД.': {
                name: 'SAMSUNG',
                color: 'rgba(240, 66, 121, 1)',
            },
            'САНТ ДЖУД МЕДИКАЛ': {
                name: 'САНТ ДЖУД МЕДИКАЛ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'САНТЕК МЕДИКАЛ; ИНК.': {
                name: 'САНТЕК МЕДИКАЛ; ИНК.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'СВИССРЕЙ МЕДИКАЛ АГ': {
                name: 'СВИССРЕЙ МЕДИКАЛ АГ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'СЕВКАВРЕНТГЕН-Д; ООО': {
                name: 'СЕВКАВРЕНТГЕН-Д; ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'СЕНТЕК АГ': {
                name: 'СЕНТЕК АГ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'СИАРЕ ИНЖЕНИРИНГ ИНТЕРНАЦИОНАЛЬ ГРУП СРЛ': {
                name: 'СИАРЕ ИНЖЕНИРИНГ ИНТЕРНАЦИОНАЛЬ ГРУП СРЛ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'СИАС; С.П.А.': {
                name: 'СИАС; С.П.А.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'СИКРИТ ИНДАСТРИЕС; ИНК.': {
                name: 'СИКРИТ ИНДАСТРИЕС; ИНК.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'СИМЕНС АГ': {
                name: 'Siemens',
                color: 'rgba(255, 127, 0, 1)',
            },
            'СИНО-ХИРО (ШЕНЖЕН) БИО-МЕДИКАЛ ЭЛЕКТРОНИКС КО.; ЛТД.': {
                name: 'СИНО-ХИРО (ШЕНЖЕН) БИО-МЕДИКАЛ ЭЛЕКТРОНИКС КО.; ЛТД.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'СКАНЕР НПО АО': {
                name: 'СКАНЕР НПО АО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'СЛЕ; ООО': {
                name: 'СЛЕ; ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'СМАРТ РЕЙ ЗАО': {
                name: 'СМАРТ РЕЙ ЗАО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'СОНОСКЕЙП КО; ЛТД': {
                name: 'Sonoscape',
                color: 'rgba(92, 185, 187, 1)',
            },
            'СОРС-РЭЙ ИНК': {
                name: 'СОРС-РЭЙ ИНК',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'СОСЕНСКИЙ ПРИБОРОСТРОИТЕЛЬНЫЙ ЗАВОД': {
                name: 'СОСЕНСКИЙ ПРИБОРОСТРОИТЕЛЬНЫЙ ЗАВОД',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'СПЕЙСЛАБС ХЕЛФКЕА; ЛТД.': {
                name: 'СПЕЙСЛАБС ХЕЛФКЕА; ЛТД.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'СПЕКТРАП; ООО': {
                name: 'СПЕКТРАП; ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'СТРАЙКЕР': {
                name: 'СТРАЙКЕР',
                color: 'rgba(128, 000, 255, 1.0)',
            },
            'СУЗУКЕН КЕНЗ КАРДИКО КО.; ЛТД.': {
                name: 'СУЗУКЕН КЕНЗ КАРДИКО КО.; ЛТД.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'СУПЕРСОНИК ИМЭДЖИН СА': {
                name: 'СУПЕРСОНИК ИМЭДЖИН СА',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ТЕЛЕМЕД ЛТД.': {
                name: 'ТЕЛЕМЕД ЛТД.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ТЕРАТЕХ КОРПОРЕШН': {
                name: 'ТЕРАТЕХ КОРПОРЕШН',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ТЕХНОМЕДИКА НПП': {
                name: 'ТЕХНОМЕДИКА НПП',
                color: 'rgba(255, 128, 000, 0.4)',
            },
            'ТИЭСИ СПОЛ; С Р.О.': {
                name: 'ТИЭСИ СПОЛ; С Р.О.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ТМО НИИЭМ': {
                name: 'ТМО НИИЭМ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ТМТ ООО': {
                name: 'ТМТ ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ТОО КПФ ИНВАРИАНТ': {
                name: 'ТОО КПФ ИНВАРИАНТ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ТОРГОВЫЙ ДОМ ВОРСМА; ООО': {
                name: 'ТОРГОВЫЙ ДОМ ВОРСМА; ООО',
                color: 'rgba(000, 255, 128, 0.4)',
            },
            'ТОСАН КO. ЛТД.': {
                name: 'ТОСАН КO. ЛТД.',
                color: 'rgba(128, 128, 128, 0.4)',
            },
            'ТОШИБА МЕДИКАЛ СИСТЕМС': {
                name: 'ТОШИБА МЕДИКАЛ СИСТЕМС',
                color: 'rgba(167, 122, 215, 1)',
            },
            'ТРИМА; ООО': {
                name: 'ТРИМА; ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ТРИММ МЕДИЦИНА; ООО': {
                name: 'ТРИММ МЕДИЦИНА; ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ТРИСМЕД КО.; ЛТД.': {
                name: 'ТРИСМЕД КО.; ЛТД.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ТРОЛЛЬ ГК': {
                name: 'ТРОЛЛЬ ГК',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'УНИКОС РПТ; ООО': {
                name: 'УНИКОС РПТ; ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'УНИКХЕЛЗ ООО': {
                name: 'УНИКХЕЛЗ ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'УРАЛРЕНТГЕН ЗАО': {
                name: 'УРАЛРЕНТГЕН ЗАО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'УРАЛЬСКИЙ ПРИБОРОСТРОИТЕЛЬНЫЙ ЗАВОД ОАО': {
                name: 'УРАЛЬСКИЙ ПРИБОРОСТРОИТЕЛЬНЫЙ ЗАВОД ОАО',
                color: 'rgba(215, 215, 009, 0.7)',
            },
            'УСКОМ ЛТД': {
                name: 'УСКОМ ЛТД',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ФАКТОРМЕДТЕХНИКА; ООО': {
                name: 'ФАКТОРМЕДТЕХНИКА; ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ФАНЕМ': {
                name: 'ФАНЕМ',
                color: 'rgba(000, 128, 255, 1.0)',
            },
            'ФГУП НПО АВРОРА': {
                name: 'ФГУП НПО АВРОРА',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ФЕНИКС МЕДИКАЛ СИСТЕМС ПРАЙВЕТ ЛИМИТЕД': {
                name: 'ФЕНИКС МЕДИКАЛ СИСТЕМС ПРАЙВЕТ ЛИМИТЕД',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ФИЛИПС': {
                name: 'ФИЛИПС',
                color: 'rgba(0, 114, 218, 1)',
            },
            'ФИРМА АСТЕЛ; ООО': {
                name: 'ФИРМА АСТЕЛ; ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ФИРМА ТРИТОН-ЭЛЕКТРОНИКС OOO': {
                name: 'ФИРМА ТРИТОН-ЭЛЕКТРОНИКС OOO',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ФИШЕР И ПЭЙКЕЛЬ ХЕЛСКЕЭ': {
                name: 'ФИШЕР И ПЭЙКЕЛЬ ХЕЛСКЕЭ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ФЛАЙТ МЕДИКЭЛ': {
                name: 'ФЛАЙТ МЕДИКЭЛ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ФРИЦ СТЕФАН ГМБХ': {
                name: 'ФРИЦ СТЕФАН ГМБХ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ФУДЖИФИЛМ КОРПОРЕЙШН': {
                name: 'ФУДЖИФИЛМ КОРПОРЕЙШН',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ФУДЖИФИЛМ СОНОСАЙТ': {
                name: 'ФУДЖИФИЛМ СОНОСАЙТ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ФУКУДА ДЕНШИ': {
                name: 'ФУКУДА ДЕНШИ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ХАИ-МЕДИКА; НПЦ': {
                name: 'ХАИ-МЕДИКА; НПЦ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ХАЙНЕН + ЛЁВЕНШТАЙН ГМБХ': {
                name: 'ХАЙНЕН + ЛЁВЕНШТАЙН ГМБХ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ХИЛСЕРИОН КО ЛТД': {
                name: 'ХИЛСЕРИОН КО ЛТД',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ХИРАНА': {
                name: 'ХИРАНА',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ХИТАЧИ АЛОКА МЕДИКАЛ': {
                name: 'Hitachi',
                color: 'rgba(255, 255, 0, 1)',
            },
            'ХОЛОДЖИК ИНК.': {
                name: 'ХОЛОДЖИК ИНК.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ХОНДА ЭЛЕКТРОНИКС КО.; ЛТД.': {
                name: 'ХОНДА ЭЛЕКТРОНИКС КО.; ЛТД.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ЧЕЛЯБИНСКОЕ ЭЛЕКТРОТЕХНИЧЕСКОЕ ПРЕДПРИЯТИЕ ОАО': {
                name: 'ЧЕЛЯБИНСКОЕ ЭЛЕКТРОТЕХНИЧЕСКОЕ ПРЕДПРИЯТИЕ ОАО',
                color: 'rgba(000, 255, 255, 0.4)',
            },
            'ЧЕНГЖОУ ДАЙСОН ИНСТРУМЕНТ ЭНД МЕТЕР КО; ЛТД': {
                name: 'ЧЕНГЖОУ ДАЙСОН ИНСТРУМЕНТ ЭНД МЕТЕР КО; ЛТД',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ЧИСОН МЕДИКАЛ ИМИДЖИНГ КО.; ЛТД.': {
                name: 'ЧИСОН МЕДИКАЛ ИМИДЖИНГ КО.; ЛТД.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ЧИТА МЕДИКАЛ': {
                name: 'ЧИТА МЕДИКАЛ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ЧУНГВЭЙ МЕДИКАЛ КОРПОРЕЙШН': {
                name: 'ЧУНГВЭЙ МЕДИКАЛ КОРПОРЕЙШН',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ШЕНЖЕН АНКЕ ХАЙ-ТЕЧ КО.; ЛТД.': {
                name: 'ШЕНЖЕН АНКЕ ХАЙ-ТЕЧ КО.; ЛТД.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ШЕНЖЕН БЕСТМЕН ИНСТРУМЕНТ КО.; ЛТД.': {
                name: 'ШЕНЖЕН БЕСТМЕН ИНСТРУМЕНТ КО.; ЛТД.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ШЕНЖЕН ДЖЕНЕРАЛ МЕДИТЕК; ИНК.': {
                name: 'ШЕНЖЕН ДЖЕНЕРАЛ МЕДИТЕК; ИНК.',
                color: 'rgba(255, 000, 000, 0.7)',
            },
            'ШЕНЖЕН КОМЕН МЕДИКАЛ ИНСТРУМЕНТ КО; ЛТД.': {
                name: 'ШЕНЖЕН КОМЕН МЕДИКАЛ ИНСТРУМЕНТ КО; ЛТД.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ШЕНЖЕН ЛЭНДВИНД ИНДАСТРИ КО; ЛТД': {
                name: 'ШЕНЖЕН ЛЭНДВИНД ИНДАСТРИ КО; ЛТД',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ШЕНЖЕН ПРОБ САЙНС ЭНД ТЕХНОЛОДЖИ КО.; ЛТД.': {
                name: 'ШЕНЖЕН ПРОБ САЙНС ЭНД ТЕХНОЛОДЖИ КО.; ЛТД.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ШЕНЖЕН ЮНИКЭЯ ЭЛЕКТРОНИК': {
                name: 'ШЕНЖЕН ЮНИКЭЯ ЭЛЕКТРОНИК',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ШЕНЖЕНЬ КЭЯВЕЛЛ ЭЛЕКТРОНИКС КО ЛТД': {
                name: 'ШЕНЖЕНЬ КЭЯВЕЛЛ ЭЛЕКТРОНИКС КО ЛТД',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ШЕНЬЖЕНЬ ВЭЛЛ-ДИ МЕДИКАЛ ЭЛЕКТРОНИКС КО; ЛТД.': {
                name: 'ШЕНЬЖЕНЬ ВЭЛЛ-ДИ МЕДИКАЛ ЭЛЕКТРОНИКС КО; ЛТД.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ШЕНЬЖЕНЬ КЭЯВЕЛЛ ЭЛЕКТРОНИКС': {
                name: 'ШЕНЬЖЕНЬ КЭЯВЕЛЛ ЭЛЕКТРОНИКС',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ШЕНЬЖЕНЬ ЭМПЕРОР ЭЛЕКТРОНИК ТЕХНОЛОДЖИ КО.; ЛТД.': {
                name: 'ШЕНЬЖЕНЬ ЭМПЕРОР ЭЛЕКТРОНИК ТЕХНОЛОДЖИ КО.; ЛТД.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ШИЛЛЕР АГ': {
                name: 'ШИЛЛЕР АГ',
                color: 'rgba(000, 128, 255, 0.7)',
            },
            'ШИМАДЗУ': {
                name: 'ШИМАДЗУ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ЭДАН ИНСТРУМЕНТС ИНК': {
                name: 'ЭДАН ИНСТРУМЕНТС ИНК',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ЭДВАНСТ ХЭЛСКЕА ТЕХНОЛОДЖИ; ЛТД': {
                name: 'ЭДВАНСТ ХЭЛСКЕА ТЕХНОЛОДЖИ; ЛТД',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ЭЗАОТЕ С.П.А.': {
                name: 'Esaote',
                color: 'rgba(127, 127, 0, 1)',
            },
            'ЭЙ ЭНД ДИ КАМПАНИ ЛТД.': {
                name: 'ЭЙ ЭНД ДИ КАМПАНИ ЛТД.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ЭККЕРТ И ЦИГЛЕР БЕБИГ ГМБХ': {
                name: 'ЭККЕРТ И ЦИГЛЕР БЕБИГ ГМБХ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ЭКОРЕЙ КО.; ЛТД.': {
                name: 'ЭКОРЕЙ КО.; ЛТД.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ЭКОТРОН КО.; ЛТД.': {
                name: 'ЭКОТРОН КО.; ЛТД.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ЭКСАМИОН ГМБХ': {
                name: 'ЭКСАМИОН ГМБХ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ЭЛЕКТА ЛТД': {
                name: 'ЭЛЕКТА ЛТД',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ЭЛЕКТРОМЕДОБОРУДОВАНИЕ': {
                name: 'ЭЛЕКТРОМЕДОБОРУДОВАНИЕ',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ЭЛЕКТРОН НИПК': {
                name: 'ЭЛЕКТРОН НИПК',
                color: 'rgba(128, 255, 000, 0.7)',
            },
            'ЭЛЕКТРОННЫЕ МЕДИЦИНСКИЕ СИСТЕМЫ; ООО': {
                name: 'ЭЛЕКТРОННЫЕ МЕДИЦИНСКИЕ СИСТЕМЫ; ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ЭР ЛИКИД МЕДИКАЛ СИСТЕМС (ТАЭМА)': {
                name: 'ЭР ЛИКИД МЕДИКАЛ СИСТЕМС (ТАЭМА)',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ЭС АЙ Ю АЙ; КО.; ЛТД.': {
                name: 'ЭС АЙ Ю АЙ; КО.; ЛТД.',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ЭСДЖИ ХЭЛСКЕЯ КО ЛТД': {
                name: 'ЭСДЖИ ХЭЛСКЕЯ КО ЛТД',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ЭХОСЕНС': {
                name: 'ЭХОСЕНС',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ЮНАЙТЕД ИМЕДЖИНГ ХЭЛСКЕЯ ГРУП': {
                name: 'ЮНАЙТЕД ИМЕДЖИНГ ХЭЛСКЕЯ ГРУП',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'ЮТАС; ООО': {
                name: 'ЮТАС; ООО',
                color: 'rgba(255, 99, 132, 0.9)',
            },
            'Other': {
                name: 'Other',
                color: 'rgba(127, 127, 127, 1)',
            },
        };

        window.AddDatesOptions = function() {
            document.getElementById("datefield").max = document.getElementById("datefield").value = window.Year + '-' + window.Month + '-' + window.Day;
        };

        window.AddBUSelectsOptions = function() {
            let select_object = '';
            for (let i = 0; i < window.BUCodes.length; i++) {
                select_object += '<option id="' + i + '" value="' + i + '">' + window.BUCodes[i] + ' - ' + window.BUNames[i] + '</option>';
            };
            document.getElementById("buselector").innerHTML = select_object;
        };

        window.AddMarkSelectsOptions = function() {
            let select_object = '';
            for (let i = 0; i < window.MarkNames.length; i++) {
                select_object += '<option id="' + i + '" value="' + window.MarkNames[i] + '">' + window.MarkNames[i] + '</option>';
            };
            document.getElementById("markselector").innerHTML = select_object;
        };

        window.FifthPageRecalculate = function(xlsxData) {
            if (xlsxData) {
                if (document.getElementById("modeselector").selectedIndex === 0) {
                    let mydata1_5 = [];
                    let mydata2_5 = [];
                    let mydata4_5 = [];
                    window.forFifthPage.segments.forEach(function(item) {
                        mydata1_5.unshift({
                            label: window.ProdSegData[item]['name'],
                            backgroundColor: window.ProdSegData[item]['color'],
                            data: [
                                window.forFifthPage.summssegments[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['mtm1'],
                                window.forFifthPage.summssegments[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['mtm'],
                            ],
                            tooltipsValues: [
                                window.forFifthPage.summssegments_t[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['mtm1'],
                                window.forFifthPage.summssegments_t[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['mtm'],
                            ],
                        });
                        mydata2_5.unshift({
                            label: window.ProdSegData[item]['name'],
                            backgroundColor: window.ProdSegData[item]['color'],
                            data: [
                                window.forFifthPage.summssegments[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['ytd1'],
                                window.forFifthPage.summssegments[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['ytd'],
                            ],
                            tooltipsValues: [
                                window.forFifthPage.summssegments_t[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['ytd1'],
                                window.forFifthPage.summssegments_t[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['ytd'],
                            ],
                        });
                        mydata4_5.unshift({
                            label: window.ProdSegData[item]['name'],
                            backgroundColor: window.ProdSegData[item]['color'],
                            data: [
                                window.forFifthPage.summssegments[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q1_1'],
                                window.forFifthPage.summssegments[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q2_1'],
                                window.forFifthPage.summssegments[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q3_1'],
                                window.forFifthPage.summssegments[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q4_1'],
                                window.forFifthPage.summssegments[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q1'],
                            ],
                            tooltipsValues: [
                                window.forFifthPage.summssegments_t[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q1_1'],
                                window.forFifthPage.summssegments_t[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q2_1'],
                                window.forFifthPage.summssegments_t[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q3_1'],
                                window.forFifthPage.summssegments_t[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q4_1'],
                                window.forFifthPage.summssegments_t[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q1'],
                            ],
                        });
                        if (window.Month > '03') {
                            mydata4_5[0].data.push(window.forFifthPage.summssegments[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q2']);
                            mydata4_5[0].tooltipsValues.push(window.forFifthPage.summssegments_t[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q2']);
                        };
                        if (window.Month > '06') {
                            mydata4_5[0].data.push(window.forFifthPage.summssegments[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q3']);
                            mydata4_5[0].tooltipsValues.push(window.forFifthPage.summssegments_t[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q3']);
                        };
                        if (window.Month > '09') {
                            mydata4_5[0].data.push(window.forFifthPage.summssegments[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q4']);
                            mydata4_5[0].tooltipsValues.push(window.forFifthPage.summssegments_t[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q4']);
                        };
                    });
                    window.ChartData1[4].datasets = mydata1_5;
                    window.ChartData2[4].datasets = mydata2_5;
                    window.ChartData4[4].datasets = mydata4_5;
                    window.SummaryList[4] = [
                        '<p style="font-size: 11px; color: #FF0000">MS drivers for ' + window.MarkNames[document.getElementById("markselector").selectedIndex] + ': High-end Cart / Low-end Compact</p>',
                        '<p style="font-size: 11px; color: #FF0000">High-end Cart: extended portfolio (6 models)</p>',
                        '<p style="font-size: 11px; color: #FF0000">Low-end Compact: Covid / Emergency demand with decreased lead-times</p>',
                    ];
                    window.Legends[4] = [
                        'Bln.RUB<br>+/- % SPLY',
                        String(Math.floor(window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q1_1'] / 100000000) / 10) + '<br><font color="#' + ((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q1_1'] < window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q1_2']) ? 'c000' : '008') + '000">' + String(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q1_2'] > 0) ? Math.floor(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q1_1'] - window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q1_2']) / window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q1_2']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q2_1'] / 100000000) / 10) + '<br><font color="#' + ((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q2_1'] < window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q2_2']) ? 'c000' : '008') + '000">' + String(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q2_2'] > 0) ? Math.floor(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q2_1'] - window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q2_2']) / window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q2_2']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q3_1'] / 100000000) / 10) + '<br><font color="#' + ((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q3_1'] < window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q3_2']) ? 'c000' : '008') + '000">' + String(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q3_2'] > 0) ? Math.floor(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q3_1'] - window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q3_2']) / window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q3_2']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q4_1'] / 100000000) / 10) + '<br><font color="#' + ((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q4_1'] < window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q4_2']) ? 'c000' : '008') + '000">' + String(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q4_2'] > 0) ? Math.floor(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q4_1'] - window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q4_2']) / window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q4_2']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q1'] / 100000000) / 10) + '<br><font color="#' + ((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q1'] < window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q1_1']) ? 'c000' : '008') + '000">' + String(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q1_1'] > 0) ? Math.floor(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q1'] - window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q1_1']) / window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q1_1']) * 1000) / 10 : 100.0)) + '%</font>',
                    ];
                    window.Legends[4].unshift(String(Math.floor(window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['ytd'] / 100000000) / 10) + '<br><font color="#' + ((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['ytd'] < window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['ytd1']) ? 'c000' : '008') + '000">' + String(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['ytd1'] > 0) ? Math.floor(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['ytd'] - window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['ytd1']) / window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['ytd1']) * 1000) / 10 : 100.0)) + '%</font>');
                    window.Legends[4].unshift(String(Math.floor(window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['ytd1'] / 100000000) / 10) + '<br><font color="#' + ((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['ytd1'] < window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['ytd2']) ? 'c000' : '008') + '000">' + String(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['ytd2'] > 0) ? Math.floor(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['ytd1'] - window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['ytd2']) / window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['ytd2']) * 1000) / 10 : 100.0)) + '%</font>');
                    window.Legends[4].unshift(String(Math.floor(window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['mtm'] / 100000000) / 10) + '<br><font color="#' + ((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['mtm'] < window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['mtm1']) ? 'c000' : '008') + '000">' + String(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['mtm1'] > 0) ? Math.floor(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['mtm'] - window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['mtm1']) / window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['mtm1']) * 1000) / 10 : 100.0)) + '%</font>');
                    window.Legends[4].unshift(String(Math.floor(window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['mtm1'] / 100000000) / 10) + '<br><font color="#' + ((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['mtm1'] < window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['mtm2']) ? 'c000' : '008') + '000">' + String(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['mtm2'] > 0) ? Math.floor(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['mtm1'] - window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['mtm2']) / window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['mtm2']) * 1000) / 10 : 100.0)) + '%</font>');

                    if (window.Month > '03') {
                        window.Legends[4].push(String(Math.floor(window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q2'] / 100000000) / 10) + '<br><font color="#' + ((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q2'] < window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q2_1']) ? 'c000' : '008') + '000">' + String(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q2_1'] > 0) ? Math.floor(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q2'] - window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q2_1']) / window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q2_1']) * 1000) / 10 : 100.0)) + '%</font>');
                    };
                    if (window.Month > '06') {
                        window.Legends[4].push(String(Math.floor(window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q3'] / 100000000) / 10) + '<br><font color="#' + ((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q3'] < window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q3_1']) ? 'c000' : '008') + '000">' + String(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q3_1'] > 0) ? Math.floor(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q3'] - window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q3_1']) / window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q3_1']) * 1000) / 10 : 100.0)) + '%</font>');
                    };
                    if (window.Month > '09') {
                        window.Legends[4].push(String(Math.floor(window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q4'] / 100000000) / 10) + '<br><font color="#' + ((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q4'] < window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q4_1']) ? 'c000' : '008') + '000">' + String(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q4_1'] > 0) ? Math.floor(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q4'] - window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q4_1']) / window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q4_1']) * 1000) / 10 : 100.0)) + '%</font>');
                    };
                    window.myBar1_5.data = window.ChartData1[4];
                    window.myBar2_5.data = window.ChartData2[4];
                    window.myBar4_5.data = window.ChartData4[4];
                    let markIndex = window.ChartData1[2].datasets.findIndex(el => el.label === window.MarkNames[document.getElementById("markselector").selectedIndex]);
                    if (markIndex > -1) {
                        window.myBar1_5.options.scales.yAxes[0].ticks.max = Math.max.apply(null, window.ChartData1[2].datasets[markIndex].data) * 1.05;
                        window.myBar2_5.options.scales.yAxes[0].ticks.max = Math.max.apply(null, window.ChartData2[2].datasets[markIndex].data) * 1.05;
                        window.myBar4_5.options.scales.yAxes[0].ticks.max = Math.max.apply(null, window.ChartData4[2].datasets[markIndex].data) * 1.05;
                    }
                    window.myBar1_5.update();
                    window.myBar2_5.update();
                    window.myBar4_5.update();
                    document.getElementsByClassName('FifthPageMark')[0].innerHTML = window.MarkNames[document.getElementById("markselector").selectedIndex] + ': Indirect Channel';
                    let out_arr = document.getElementsByClassName('paragraph_1 4');
                    for (let i = 0; i < 5; i++) {
                        if (window.Legends[4][i] !== undefined) out_arr[i].innerHTML = window.Legends[4][i];
                    };
                    out_arr = '';
                    for (let i = 5; i < window.Legends[4].length; i++) {
                        out_arr += '<div class="div-block-12" style="width: 100%; height: 100%"><p class="paragraph_1 4">' + window.Legends[4][i] + '</p></div>';
                    };
                    document.getElementsByClassName('div-block-10')[4].innerHTML = out_arr;
                    out_arr = '';
                    for (let i = 0; i < window.SummaryList[4].length; i++) {
                        out_arr += '<li><p class="list_paragraph">' + window.SummaryList[4][i] + '</p></li>';
                    }
                    document.getElementsByClassName('list')[4].innerHTML = out_arr;
                } else {
                    let mydata1_5 = [];
                    let mydata2_5 = [];
                    let mydata4_5 = [];
                    window.forFifthPage.segments.forEach(function(item) {
                        mydata1_5.unshift({
                            label: window.ProdSegData[item]['name'],
                            backgroundColor: window.ProdSegData[item]['color'],
                            data: [
                                window.forFifthPage.summssegments[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['mtm1'],
                                window.forFifthPage.summssegments[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['mtm'],
                            ],
                            tooltipsValues: [
                                window.forFifthPage.summssegments_t[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['mtm1'],
                                window.forFifthPage.summssegments_t[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['mtm'],
                            ],
                        });
                        mydata2_5.unshift({
                            label: window.ProdSegData[item]['name'],
                            backgroundColor: window.ProdSegData[item]['color'],
                            data: [
                                window.forFifthPage.summssegments[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['ytd1'],
                                window.forFifthPage.summssegments[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['ytd'],
                            ],
                            tooltipsValues: [
                                window.forFifthPage.summssegments_t[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['ytd1'],
                                window.forFifthPage.summssegments_t[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['ytd'],
                            ],
                        });
                        mydata4_5.unshift({
                            label: window.ProdSegData[item]['name'],
                            backgroundColor: window.ProdSegData[item]['color'],
                            data: [
                                window.forFifthPage.summssegments[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q13'],
                                window.forFifthPage.summssegments[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q12'],
                                window.forFifthPage.summssegments[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q11'],
                                window.forFifthPage.summssegments[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q10'],
                                window.forFifthPage.summssegments[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q9'],
                                window.forFifthPage.summssegments[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q8'],
                                window.forFifthPage.summssegments[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q7'],
                                window.forFifthPage.summssegments[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q6'],
                                window.forFifthPage.summssegments[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q5'],
                                window.forFifthPage.summssegments[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q4'],
                                window.forFifthPage.summssegments[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q3'],
                                window.forFifthPage.summssegments[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q2'],
                                window.forFifthPage.summssegments[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q1'],
                                window.forFifthPage.summssegments[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q0'],
                            ],
                            tooltipsValues: [
                                window.forFifthPage.summssegments_t[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q13'],
                                window.forFifthPage.summssegments_t[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q12'],
                                window.forFifthPage.summssegments_t[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q11'],
                                window.forFifthPage.summssegments_t[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q10'],
                                window.forFifthPage.summssegments_t[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q9'],
                                window.forFifthPage.summssegments_t[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q8'],
                                window.forFifthPage.summssegments_t[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q7'],
                                window.forFifthPage.summssegments_t[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q6'],
                                window.forFifthPage.summssegments_t[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q5'],
                                window.forFifthPage.summssegments_t[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q4'],
                                window.forFifthPage.summssegments_t[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q3'],
                                window.forFifthPage.summssegments_t[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q2'],
                                window.forFifthPage.summssegments_t[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q1'],
                                window.forFifthPage.summssegments_t[item + window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q0'],
                            ],
                        });
                    });
                    window.ChartData1[4].datasets = mydata1_5;
                    window.ChartData2[4].datasets = mydata2_5;
                    window.ChartData4[4].datasets = mydata4_5;
                    window.SummaryList[4] = [
                        '<p style="font-size: 11px; color: #FF0000">MS drivers for ' + window.MarkNames[document.getElementById("markselector").selectedIndex] + ': High-end Cart / Low-end Compact</p>',
                        '<p style="font-size: 11px; color: #FF0000">High-end Cart: extended portfolio (6 models)</p>',
                        '<p style="font-size: 11px; color: #FF0000">Low-end Compact: Covid / Emergency demand with decreased lead-times</p>',
                    ];
                    window.Legends[4] = [
                        String(Math.floor(window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['mtm1'] / 100000000) / 10) + '<br><font color="#' + ((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['mtm1'] < window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['mtm2']) ? 'c000' : '008') + '000">' + String(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['mtm2'] > 0) ? Math.floor(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['mtm1'] - window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['mtm2']) / window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['mtm2']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['mtm'] / 100000000) / 10) + '<br><font color="#' + ((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['mtm'] < window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['mtm1']) ? 'c000' : '008') + '000">' + String(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['mtm1'] > 0) ? Math.floor(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['mtm'] - window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['mtm1']) / window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['mtm1']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['ytd1'] / 100000000) / 10) + '<br><font color="#' + ((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['ytd1'] < window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['ytd2']) ? 'c000' : '008') + '000">' + String(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['ytd2'] > 0) ? Math.floor(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['ytd1'] - window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['ytd2']) / window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['ytd2']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['ytd'] / 100000000) / 10) + '<br><font color="#' + ((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['ytd'] < window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['ytd1']) ? 'c000' : '008') + '000">' + String(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['ytd1'] > 0) ? Math.floor(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['ytd'] - window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['ytd1']) / window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['ytd1']) * 1000) / 10 : 100.0)) + '%</font>',
                        'Bln.RUB<br>+/- % SPLY',
                        String(Math.floor(window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q13'] / 100000000) / 10) + '<br><font color="#' + ((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q13'] < window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q25']) ? 'c000' : '008') + '000">' + String(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q25'] > 0) ? Math.floor(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q13'] - window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q25']) / window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q25']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q12'] / 100000000) / 10) + '<br><font color="#' + ((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q12'] < window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q24']) ? 'c000' : '008') + '000">' + String(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q24'] > 0) ? Math.floor(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q12'] - window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q24']) / window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q24']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q11'] / 100000000) / 10) + '<br><font color="#' + ((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q11'] < window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q23']) ? 'c000' : '008') + '000">' + String(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q23'] > 0) ? Math.floor(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q11'] - window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q23']) / window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q23']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q10'] / 100000000) / 10) + '<br><font color="#' + ((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q10'] < window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q22']) ? 'c000' : '008') + '000">' + String(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q22'] > 0) ? Math.floor(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q10'] - window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q22']) / window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q22']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q9'] / 100000000) / 10) + '<br><font color="#' + ((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q9'] < window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q21']) ? 'c000' : '008') + '000">' + String(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q21'] > 0) ? Math.floor(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q9'] - window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q21']) / window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q21']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q8'] / 100000000) / 10) + '<br><font color="#' + ((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q8'] < window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q20']) ? 'c000' : '008') + '000">' + String(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q20'] > 0) ? Math.floor(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q8'] - window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q20']) / window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q20']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q7'] / 100000000) / 10) + '<br><font color="#' + ((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q7'] < window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q19']) ? 'c000' : '008') + '000">' + String(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q19'] > 0) ? Math.floor(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q7'] - window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q19']) / window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q19']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q6'] / 100000000) / 10) + '<br><font color="#' + ((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q6'] < window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q18']) ? 'c000' : '008') + '000">' + String(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q18'] > 0) ? Math.floor(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q6'] - window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q18']) / window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q18']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q5'] / 100000000) / 10) + '<br><font color="#' + ((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q5'] < window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q17']) ? 'c000' : '008') + '000">' + String(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q17'] > 0) ? Math.floor(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q5'] - window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q17']) / window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q17']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q4'] / 100000000) / 10) + '<br><font color="#' + ((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q4'] < window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q16']) ? 'c000' : '008') + '000">' + String(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q16'] > 0) ? Math.floor(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q4'] - window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q16']) / window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q16']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q3'] / 100000000) / 10) + '<br><font color="#' + ((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q3'] < window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q15']) ? 'c000' : '008') + '000">' + String(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q15'] > 0) ? Math.floor(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q3'] - window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q15']) / window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q15']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q2'] / 100000000) / 10) + '<br><font color="#' + ((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q2'] < window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q14']) ? 'c000' : '008') + '000">' + String(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q14'] > 0) ? Math.floor(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q2'] - window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q14']) / window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q14']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q1'] / 100000000) / 10) + '<br><font color="#' + ((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q1'] < window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q13']) ? 'c000' : '008') + '000">' + String(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q13'] > 0) ? Math.floor(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q1'] - window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q13']) / window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q13']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q0'] / 100000000) / 10) + '<br><font color="#' + ((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q0'] < window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q12']) ? 'c000' : '008') + '000">' + String(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q12'] > 0) ? Math.floor(((window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q0'] - window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q12']) / window.forFifthPage.summsbrands5[window.MarkCodes[document.getElementById("markselector").selectedIndex]]['q12']) * 1000) / 10 : 100.0)) + '%</font>',
                    ];
                    window.myBar1_5.data = window.ChartData1[4];
                    window.myBar2_5.data = window.ChartData2[4];
                    window.myBar4_5.data = window.ChartData4[4];
                    let markIndex = window.ChartData1[2].datasets.findIndex(el => el.label === window.MarkNames[document.getElementById("markselector").selectedIndex]);
                    if (markIndex > -1) {
                        window.myBar1_5.options.scales.yAxes[0].ticks.max = Math.max.apply(null, window.ChartData1[2].datasets[markIndex].data) * 1.05;
                        window.myBar2_5.options.scales.yAxes[0].ticks.max = Math.max.apply(null, window.ChartData2[2].datasets[markIndex].data) * 1.05;
                        window.myBar4_5.options.scales.yAxes[0].ticks.max = Math.max.apply(null, window.ChartData4[2].datasets[markIndex].data) * 1.05;
                    }
                    window.myBar1_5.update();
                    window.myBar2_5.update();
                    window.myBar4_5.update();
                    document.getElementsByClassName('FifthPageMark')[0].innerHTML = window.MarkNames[document.getElementById("markselector").selectedIndex] + ': Indirect Channel';
                    let out_arr = document.getElementsByClassName('paragraph_1 4');
                    for (let i = 0; i < 5; i++) {
                        if (window.Legends[4][i] !== undefined) out_arr[i].innerHTML = window.Legends[4][i];
                    };
                    out_arr = '';
                    for (let i = 5; i < window.Legends[4].length; i++) {
                        out_arr += '<div class="div-block-12" style="width: 100%; height: 100%"><p class="paragraph_1 4">' + window.Legends[4][i] + '</p></div>';
                    };
                    document.getElementsByClassName('div-block-10')[4].innerHTML = out_arr;
                    out_arr = '';
                    for (let i = 0; i < window.SummaryList[4].length; i++) {
                        out_arr += '<li><p class="list_paragraph">' + window.SummaryList[4][i] + '</p></li>';
                    }
                    document.getElementsByClassName('list')[4].innerHTML = out_arr;
                }
            }
        }

        window.CalculateGraphData = function(xlsxData) {
            window.SelectedBU = [];
            window.SummaryList = [];
            window.ChartData1 = [];
            window.ChartData2 = [];
            window.ChartData3 = [];
            window.ChartData4 = [];
            window.Legends = [];
            if (xlsxData) {
                let f_date = 0;
                let f_bu = 0;
                let f_pseg = 0;
                let f_corp = 0;
                let f_dirindir = 0;
                let f_summ = 0;
                let f_strings = {};
                for (let i = 0; i < xlsxData.length; i++) {
                    if ((!(xlsxData[i]['Корпорация'])) || (xlsxData[i]['Корпорация'] === '')) {
                        alert('Пустая корпорация');
                        exit;
                    };
                    if ((!(xlsxData[i]['Product Segment 1'])) || (xlsxData[i]['Product Segment 1'] === '')) {
                        xlsxData[i]['Product Segment 1'] = 'null';
                    };
                    if ((!(xlsxData[i]['Business Units'])) || (xlsxData[i]['Business Units'] === '')) {
                        alert('Пустая корпорация');
                        exit;
                    };
                    if (!(/\d{1,2}\/\d{1,2}\/\d{1,2}/.test(xlsxData[i]['Дата окончания процедуры']))) {
                        if (f_date) {
                            f_strings['date'] += ', ' + String(i + 2);
                        } else {
                            f_date = 1;
                            f_strings['date'] = 'Ошибка формата ячейки "Дата окончания процедуры" в строках: ' + String(i + 2);
                        }
                    };
                    if (!((xlsxData[i]['Прямая закупка'] === 'Direct') || (xlsxData[i]['Прямая закупка'] === 'Indirect'))) {
                        if (f_dirindir) {
                            f_strings['dirindir'] += ', ' + String(i + 2);
                        } else {
                            f_dirindir = 1;
                            f_strings['dirindir'] = 'Неизвестные данные в ячейке "Прямая закупка" в строках: ' + String(i + 2);
                        }
                    };
                    let f_tmp = 1;
                    for (let key in window.BrandsData) {
                        if ((xlsxData[i]['Корпорация'] === key) && (xlsxData[i]['Корпорация'] != '')) {
                            f_tmp = 0;
                            break;
                        };
                    };
                    if (f_tmp) {
                        window.BrandsData[xlsxData[i]['Корпорация']] = {
                            name: xlsxData[i]['Корпорация'],
                            color: 'rgba(255, 99, 132, 0.9)',
                        };
                        if (f_corp) {
                            f_strings['corp'] += ', ' + String(i + 2);
                        } else {
                            f_corp = 1;
                            f_strings['corp'] = 'Неизвестные данные в ячейке "Корпорация" в строках: ' + String(i + 2);
                        };
                    };
                    f_tmp = 1;
                    for (let key in window.ProdSegData) {
                        if ((xlsxData[i]['Product Segment 1'] === key) && (xlsxData[i]['Product Segment 1'] != '')) {
                            f_tmp = 0;
                            break;
                        };
                    };
                    if (f_tmp) {
                        window.ProdSegData[xlsxData[i]['Product Segment 1']] = {
                            name: xlsxData[i]['Product Segment 1'],
                            color: 'rgba(255, 99, 132, 0.9)',
                        };
                        if (f_pseg) {
                            f_strings['pseg'] += ', ' + String(i + 2);
                        } else {
                            f_pseg = 1;
                            f_strings['pseg'] = 'Неизвестные данные в ячейке "Product Segment 1" в строках: ' + String(i + 2);
                        };
                    };
                    f_tmp = 1;
                    window.BUCodes.forEach(function(item) {
                        if ((xlsxData[i]['Business Units'] === item) && (xlsxData[i]['Business Units'] != '')) {
                            f_tmp = 0;
                        };
                    });
                    if (f_tmp) {
                        window.BUCodes.push(xlsxData[i]['Business Units']);
                        window.BUNames.push(xlsxData[i]['Business Units']);
                        if (f_bu) {
                            f_strings['bu'] += ', ' + String(i + 2);
                        } else {
                            f_bu = 1;
                            f_strings['bu'] = 'Неизвестные данные в ячейке "Business Units" в строках: ' + String(i + 2);
                        };
                    };
                };
                if (f_date) {
                    alert(f_strings['date']);
                };
                if (f_bu) {
                    alert(f_strings['bu']);
                };
                if (f_pseg) {
                    alert(f_strings['pseg']);
                };
                if (f_corp) {
                    alert(f_strings['corp']);
                };
                if (f_dirindir) {
                    alert(f_strings['dirindir']);
                };
                if (document.getElementById("modeselector").selectedIndex === 0) {
                    let selectedDate = document.getElementById("datefield").value.split('-');
                    window.Year = selectedDate[0];
                    window.Month = selectedDate[1];
                    window.Day = selectedDate[2];
                    if (window.Month === '12') {
                        window.Year1 = window.Year;
                        window.Year2 = window.Year3 = window.Year - 1;
                        window.Month1 = '01';
                    } else {
                        window.Year1 = window.Year2 = window.Year - 1;
                        window.Year3 = window.Year - 2;
                        window.Month1 = Number(window.Month) + 1;
                        if (window.Month1 < 10) {
                            window.Month1 = '0' + String(window.Month1);
                        }
                    };
                    window.CurDate = Number(String(window.Year) + window.Month + window.Day);
                    window.CurDate1 = Number(String(window.Year - 1) + window.Month + window.Day);
                    window.CurDate2 = Number(String(window.Year - 2) + window.Month + window.Day);
                    window.CurDate3 = Number(String(window.Year - 3) + window.Month + window.Day);
                    window.CurDate4 = Number(String(window.Year - 4) + window.Month + window.Day);
                    window.YearDate = Number(String(window.Year) + '0101');
                    window.YearDate1 = Number(String(window.Year - 1) + '0101');
                    window.YearDate2 = Number(String(window.Year - 2) + '0101');
                    window.YearDate3 = Number(String(window.Year - 3) + '0101');
                    window.YearDate4 = Number(String(window.Year - 4) + '0101');
                    window.CurMonth = Number(String(window.Year) + window.Month);
                    window.CurMonth1 = Number(String(window.Year - 1) + window.Month);
                    window.CurMonth2 = Number(String(window.Year - 2) + window.Month);
                    window.CurMonth3 = Number(String(window.Year - 3) + window.Month);
                    window.CurMonth4 = Number(String(window.Year - 4) + window.Month);
                    let brands = [];
                    let brandsInd = [];
                    let segments = [];
                    let summsall = 0;
                    let summsallInd = 0;
                    let summsallIndbrands = [];
                    let summsbrands = {};
                    let summsbrands_t = {};
                    let summsbrands5 = {};
                    let summssegments = {};
                    let summssegments_t = {};
                    summsbrands['Direct'] = {
                        'all': 0,
                        'mtm': 0,
                        'mtm1': 0,
                        'mtm2': 0,
                        'mtm3': 0,
                        'ytd': 0,
                        'ytd1': 0,
                        'ytd2': 0,
                        'ytd3': 0,
                        'q1': 0,
                        'q1_1': 0,
                        'q1_2': 0,
                        'q1_3': 0,
                        'q2': 0,
                        'q2_1': 0,
                        'q2_2': 0,
                        'q2_3': 0,
                        'q3': 0,
                        'q3_1': 0,
                        'q3_2': 0,
                        'q3_3': 0,
                        'q4': 0,
                        'q4_1': 0,
                        'q4_2': 0,
                        'q4_3': 0,
                    };
                    summsbrands['Indirect'] = {
                        'all': 0,
                        'mtm': 0,
                        'mtm1': 0,
                        'mtm2': 0,
                        'mtm3': 0,
                        'ytd': 0,
                        'ytd1': 0,
                        'ytd2': 0,
                        'ytd3': 0,
                        'q1': 0,
                        'q1_1': 0,
                        'q1_2': 0,
                        'q1_3': 0,
                        'q2': 0,
                        'q2_1': 0,
                        'q2_2': 0,
                        'q2_3': 0,
                        'q3': 0,
                        'q3_1': 0,
                        'q3_2': 0,
                        'q3_3': 0,
                        'q4': 0,
                        'q4_1': 0,
                        'q4_2': 0,
                        'q4_3': 0,
                    };
                    let summsMTM = {
                        'cur': 0,
                        'cur_1': 0,
                        'cur_2': 0,
                        'cur_3': 0,
                        'curi': 0,
                        'curi_1': 0,
                        'curi_2': 0,
                        'curi_3': 0,
                    };
                    let summsYTD = {
                        'cur': 0,
                        'cur_1': 0,
                        'cur_2': 0,
                        'cur_3': 0,
                        'curi': 0,
                        'curi_1': 0,
                        'curi_2': 0,
                        'curi_3': 0,
                    };
                    let summsQ1 = {
                        'cur': 0,
                        'cur_1': 0,
                        'cur_2': 0,
                        'cur_3': 0,
                        'curi': 0,
                        'curi_1': 0,
                        'curi_2': 0,
                        'curi_3': 0,
                    };
                    let summsQ2 = {
                        'cur': 0,
                        'cur_1': 0,
                        'cur_2': 0,
                        'cur_3': 0,
                        'curi': 0,
                        'curi_1': 0,
                        'curi_2': 0,
                        'curi_3': 0,
                    };
                    let summsQ3 = {
                        'cur': 0,
                        'cur_1': 0,
                        'cur_2': 0,
                        'cur_3': 0,
                        'curi': 0,
                        'curi_1': 0,
                        'curi_2': 0,
                        'curi_3': 0,
                    };
                    let summsQ4 = {
                        'cur': 0,
                        'cur_1': 0,
                        'cur_2': 0,
                        'cur_3': 0,
                        'curi': 0,
                        'curi_1': 0,
                        'curi_2': 0,
                        'curi_3': 0,
                    };
                    for (let i = 0; i < xlsxData.length; i++) {
                        let vDates = xlsxData[i]['Дата окончания процедуры'].split('/');
                        for (let j = 0; j < vDates.length; j++) {
                            if (vDates[j] < 10) {
                                vDates[j] = '0' + String(vDates[j]);
                            };
                        };
                        vDates[2] = '20' + vDates[2];
                        let vDate = Number(vDates[2] + vDates[0] + vDates[1]);
                        let vMonth = Number(vDates[2] + vDates[0]);
                        if (xlsxData[i]['Business Units'] === window.BUCodes[document.getElementById("buselector").selectedIndex]) {
                            xlsxData[i]['Сумма Кон. Продаж в Нац. валюта'] = xlsxData[i]['Сумма Кон. Продаж в Нац. валюта'].replace(/,/g, '');
                            if ((!(xlsxData[i]['Product Segment 1'])) || (xlsxData[i]['Product Segment 1'] === '')) {
                                xlsxData[i]['Product Segment 1'] = 'null';
                            };
                            let f = true;
                            for (let j = 0; j < brands.length; j++) {
                                if ((xlsxData[i]['Корпорация']) === brands[j]) {
                                    f = false;
                                    break;
                                }
                            };
                            if (f) {
                                brands.push(xlsxData[i]['Корпорация']);
                                summsallIndbrands[xlsxData[i]['Корпорация']] = 0;
                                summsbrands[xlsxData[i]['Корпорация']] = {
                                    'all': 0,
                                    'mtm': 0,
                                    'mtm1': 0,
                                    'mtm2': 0,
                                    'mtm3': 0,
                                    'ytd': 0,
                                    'ytd1': 0,
                                    'ytd2': 0,
                                    'ytd3': 0,
                                    'q1': 0,
                                    'q1_1': 0,
                                    'q1_2': 0,
                                    'q1_3': 0,
                                    'q2': 0,
                                    'q2_1': 0,
                                    'q2_2': 0,
                                    'q2_3': 0,
                                    'q3': 0,
                                    'q3_1': 0,
                                    'q3_2': 0,
                                    'q3_3': 0,
                                    'q4': 0,
                                    'q4_1': 0,
                                    'q4_2': 0,
                                    'q4_3': 0,
                                };
                                summsbrands[xlsxData[i]['Корпорация'] + 'Indirect'] = {
                                    'all': 0,
                                    'mtm': 0,
                                    'mtm1': 0,
                                    'mtm2': 0,
                                    'mtm3': 0,
                                    'ytd': 0,
                                    'ytd1': 0,
                                    'ytd2': 0,
                                    'ytd3': 0,
                                    'q1': 0,
                                    'q1_1': 0,
                                    'q1_2': 0,
                                    'q1_3': 0,
                                    'q2': 0,
                                    'q2_1': 0,
                                    'q2_2': 0,
                                    'q2_3': 0,
                                    'q3': 0,
                                    'q3_1': 0,
                                    'q3_2': 0,
                                    'q3_3': 0,
                                    'q4': 0,
                                    'q4_1': 0,
                                    'q4_2': 0,
                                    'q4_3': 0,
                                };
                                summsbrands[xlsxData[i]['Корпорация'] + 'Direct'] = {
                                    'all': 0,
                                    'mtm': 0,
                                    'mtm1': 0,
                                    'mtm2': 0,
                                    'mtm3': 0,
                                    'ytd': 0,
                                    'ytd1': 0,
                                    'ytd2': 0,
                                    'ytd3': 0,
                                    'q1': 0,
                                    'q1_1': 0,
                                    'q1_2': 0,
                                    'q1_3': 0,
                                    'q2': 0,
                                    'q2_1': 0,
                                    'q2_2': 0,
                                    'q2_3': 0,
                                    'q3': 0,
                                    'q3_1': 0,
                                    'q3_2': 0,
                                    'q3_3': 0,
                                    'q4': 0,
                                    'q4_1': 0,
                                    'q4_2': 0,
                                    'q4_3': 0,
                                };
                            }
                            if (xlsxData[i]['Прямая закупка'] === 'Indirect') {
                                f = true;
                                for (let j = 0; j < brandsInd.length; j++) {
                                    if ((xlsxData[i]['Корпорация']) === brandsInd[j]) {
                                        f = false;
                                        break;
                                    }
                                };
                                if (f) {
                                    brandsInd.push(xlsxData[i]['Корпорация']);
                                    for (let j = 0; j < segments.length; j++) {
                                        summssegments[segments[j] + xlsxData[i]['Корпорация']] = {
                                            'all': 0,
                                            'mtm': 0,
                                            'mtm1': 0,
                                            'mtm2': 0,
                                            'mtm3': 0,
                                            'ytd': 0,
                                            'ytd1': 0,
                                            'ytd2': 0,
                                            'ytd3': 0,
                                            'q1': 0,
                                            'q1_1': 0,
                                            'q1_2': 0,
                                            'q1_3': 0,
                                            'q2': 0,
                                            'q2_1': 0,
                                            'q2_2': 0,
                                            'q2_3': 0,
                                            'q3': 0,
                                            'q3_1': 0,
                                            'q3_2': 0,
                                            'q3_3': 0,
                                            'q4': 0,
                                            'q4_1': 0,
                                            'q4_2': 0,
                                            'q4_3': 0,
                                        };
                                    };
                                };
                                f = true;
                                for (let j = 0; j < segments.length; j++) {
                                    if ((xlsxData[i]['Product Segment 1']) === segments[j]) {
                                        f = false;
                                        break;
                                    }
                                };
                                if (f) {
                                    segments.push(xlsxData[i]['Product Segment 1']);
                                    summssegments[xlsxData[i]['Product Segment 1']] = {
                                        'all': 0,
                                        'mtm': 0,
                                        'mtm1': 0,
                                        'mtm2': 0,
                                        'mtm3': 0,
                                        'ytd': 0,
                                        'ytd1': 0,
                                        'ytd2': 0,
                                        'ytd3': 0,
                                        'q1': 0,
                                        'q1_1': 0,
                                        'q1_2': 0,
                                        'q1_3': 0,
                                        'q2': 0,
                                        'q2_1': 0,
                                        'q2_2': 0,
                                        'q2_3': 0,
                                        'q3': 0,
                                        'q3_1': 0,
                                        'q3_2': 0,
                                        'q3_3': 0,
                                        'q4': 0,
                                        'q4_1': 0,
                                        'q4_2': 0,
                                        'q4_3': 0,
                                    };
                                    for (let j = 0; j < brandsInd.length; j++) {
                                        summssegments[xlsxData[i]['Product Segment 1'] + brandsInd[j]] = {
                                            'all': 0,
                                            'mtm': 0,
                                            'mtm1': 0,
                                            'mtm2': 0,
                                            'mtm3': 0,
                                            'ytd': 0,
                                            'ytd1': 0,
                                            'ytd2': 0,
                                            'ytd3': 0,
                                            'q1': 0,
                                            'q1_1': 0,
                                            'q1_2': 0,
                                            'q1_3': 0,
                                            'q2': 0,
                                            'q2_1': 0,
                                            'q2_2': 0,
                                            'q2_3': 0,
                                            'q3': 0,
                                            'q3_1': 0,
                                            'q3_2': 0,
                                            'q3_3': 0,
                                            'q4': 0,
                                            'q4_1': 0,
                                            'q4_2': 0,
                                            'q4_3': 0,
                                        };
                                    };
                                };
                            }
                            if (vMonth > window.CurMonth) {
                                continue;
                            } else if (vMonth > window.CurMonth1) {
                                summsMTM['cur'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                if (xlsxData[i]['Прямая закупка'] === 'Indirect') {
                                    summsMTM['curi'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summssegments[xlsxData[i]['Product Segment 1']]['mtm'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summssegments[xlsxData[i]['Product Segment 1'] + xlsxData[i]['Корпорация']]['mtm'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                }
                                summsbrands[xlsxData[i]['Корпорация']]['mtm'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                summsbrands[xlsxData[i]['Корпорация'] + xlsxData[i]['Прямая закупка']]['mtm'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                summsbrands[xlsxData[i]['Прямая закупка']]['mtm'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                            } else if (vMonth > window.CurMonth2) {
                                summsMTM['cur_1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                if (xlsxData[i]['Прямая закупка'] === 'Indirect') {
                                    summsMTM['curi_1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summssegments[xlsxData[i]['Product Segment 1']]['mtm1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summssegments[xlsxData[i]['Product Segment 1'] + xlsxData[i]['Корпорация']]['mtm1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                }
                                summsbrands[xlsxData[i]['Корпорация']]['mtm1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                summsbrands[xlsxData[i]['Корпорация'] + xlsxData[i]['Прямая закупка']]['mtm1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                summsbrands[xlsxData[i]['Прямая закупка']]['mtm1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                            } else if (vMonth > window.CurMonth3) {
                                summsMTM['cur_2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                if (xlsxData[i]['Прямая закупка'] === 'Indirect') {
                                    summsMTM['curi_2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summssegments[xlsxData[i]['Product Segment 1']]['mtm2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summssegments[xlsxData[i]['Product Segment 1'] + xlsxData[i]['Корпорация']]['mtm2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                }
                                summsbrands[xlsxData[i]['Корпорация']]['mtm2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                summsbrands[xlsxData[i]['Корпорация'] + xlsxData[i]['Прямая закупка']]['mtm2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                summsbrands[xlsxData[i]['Прямая закупка']]['mtm2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                            } else if (vMonth > window.CurMonth4) {
                                summsMTM['cur_3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                if (xlsxData[i]['Прямая закупка'] === 'Indirect') {
                                    summsMTM['curi_3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summssegments[xlsxData[i]['Product Segment 1']]['mtm3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summssegments[xlsxData[i]['Product Segment 1'] + xlsxData[i]['Корпорация']]['mtm3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                }
                                summsbrands[xlsxData[i]['Корпорация']]['mtm3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                summsbrands[xlsxData[i]['Корпорация'] + xlsxData[i]['Прямая закупка']]['mtm3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                summsbrands[xlsxData[i]['Прямая закупка']]['mtm3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                            } else {
                                continue;
                            };
                            if ((vDate >= window.YearDate) && (vDate <= window.CurDate)) {
                                summsYTD['cur'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                if (xlsxData[i]['Прямая закупка'] === 'Indirect') {
                                    summsYTD['curi'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summssegments[xlsxData[i]['Product Segment 1']]['ytd'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summssegments[xlsxData[i]['Product Segment 1'] + xlsxData[i]['Корпорация']]['ytd'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                }
                                summsbrands[xlsxData[i]['Корпорация']]['ytd'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                summsbrands[xlsxData[i]['Корпорация'] + xlsxData[i]['Прямая закупка']]['ytd'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                summsbrands[xlsxData[i]['Прямая закупка']]['ytd'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                            } else if ((vDate >= window.YearDate1) && (vDate <= window.CurDate1)) {
                                summsYTD['cur_1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                if (xlsxData[i]['Прямая закупка'] === 'Indirect') {
                                    summsYTD['curi_1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summssegments[xlsxData[i]['Product Segment 1']]['ytd1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summssegments[xlsxData[i]['Product Segment 1'] + xlsxData[i]['Корпорация']]['ytd1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                }
                                summsbrands[xlsxData[i]['Корпорация']]['ytd1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                summsbrands[xlsxData[i]['Корпорация'] + xlsxData[i]['Прямая закупка']]['ytd1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                summsbrands[xlsxData[i]['Прямая закупка']]['ytd1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                            } else if ((vDate >= window.YearDate2) && (vDate <= window.CurDate2)) {
                                summsYTD['cur_2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                if (xlsxData[i]['Прямая закупка'] === 'Indirect') {
                                    summsYTD['curi_2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summssegments[xlsxData[i]['Product Segment 1']]['ytd2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summssegments[xlsxData[i]['Product Segment 1'] + xlsxData[i]['Корпорация']]['ytd2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                }
                                summsbrands[xlsxData[i]['Корпорация']]['ytd2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                summsbrands[xlsxData[i]['Корпорация'] + xlsxData[i]['Прямая закупка']]['ytd2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                summsbrands[xlsxData[i]['Прямая закупка']]['ytd2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                            } else if ((vDate >= window.YearDate3) && (vDate <= window.CurDate3)) {
                                summsYTD['cur_3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                if (xlsxData[i]['Прямая закупка'] === 'Indirect') {
                                    summsYTD['curi_3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summssegments[xlsxData[i]['Product Segment 1']]['ytd3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summssegments[xlsxData[i]['Product Segment 1'] + xlsxData[i]['Корпорация']]['ytd3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                }
                                summsbrands[xlsxData[i]['Корпорация']]['ytd3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                summsbrands[xlsxData[i]['Корпорация'] + xlsxData[i]['Прямая закупка']]['ytd3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                summsbrands[xlsxData[i]['Прямая закупка']]['ytd3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                            };
                            if (vDates[0] < '04') {
                                if (Number(vDates[2]) == window.Year) {
                                    summsQ1['cur'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    if (xlsxData[i]['Прямая закупка'] === 'Indirect') {
                                        summsQ1['curi'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                        summssegments[xlsxData[i]['Product Segment 1']]['q1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                        summssegments[xlsxData[i]['Product Segment 1'] + xlsxData[i]['Корпорация']]['q1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    }
                                    summsbrands[xlsxData[i]['Корпорация']]['q1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summsbrands[xlsxData[i]['Корпорация'] + xlsxData[i]['Прямая закупка']]['q1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summsbrands[xlsxData[i]['Прямая закупка']]['q1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                } else if (Number(vDates[2]) == (window.Year - 1)) {
                                    summsQ1['cur_1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    if (xlsxData[i]['Прямая закупка'] === 'Indirect') {
                                        summsQ1['curi_1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                        summssegments[xlsxData[i]['Product Segment 1']]['q1_1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                        summssegments[xlsxData[i]['Product Segment 1'] + xlsxData[i]['Корпорация']]['q1_1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    }
                                    summsbrands[xlsxData[i]['Корпорация']]['q1_1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summsbrands[xlsxData[i]['Корпорация'] + xlsxData[i]['Прямая закупка']]['q1_1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summsbrands[xlsxData[i]['Прямая закупка']]['q1_1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                } else if (Number(vDates[2]) == (window.Year - 2)) {
                                    summsQ1['cur_2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    if (xlsxData[i]['Прямая закупка'] === 'Indirect') {
                                        summsQ1['curi_2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                        summssegments[xlsxData[i]['Product Segment 1']]['q1_2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                        summssegments[xlsxData[i]['Product Segment 1'] + xlsxData[i]['Корпорация']]['q1_2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    }
                                    summsbrands[xlsxData[i]['Корпорация']]['q1_2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summsbrands[xlsxData[i]['Корпорация'] + xlsxData[i]['Прямая закупка']]['q1_2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summsbrands[xlsxData[i]['Прямая закупка']]['q1_2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                } else if (Number(vDates[2]) == (window.Year - 3)) {
                                    summsQ1['cur_3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    if (xlsxData[i]['Прямая закупка'] === 'Indirect') {
                                        summsQ1['curi_3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                        summssegments[xlsxData[i]['Product Segment 1']]['q1_3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                        summssegments[xlsxData[i]['Product Segment 1'] + xlsxData[i]['Корпорация']]['q1_3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    }
                                    summsbrands[xlsxData[i]['Корпорация']]['q1_3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summsbrands[xlsxData[i]['Корпорация'] + xlsxData[i]['Прямая закупка']]['q1_3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summsbrands[xlsxData[i]['Прямая закупка']]['q1_3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                }
                            } else if (vDates[0] < '07') {
                                if (Number(vDates[2]) == window.Year) {
                                    summsQ2['cur'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    if (xlsxData[i]['Прямая закупка'] === 'Indirect') {
                                        summsQ2['curi'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                        summssegments[xlsxData[i]['Product Segment 1']]['q2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                        summssegments[xlsxData[i]['Product Segment 1'] + xlsxData[i]['Корпорация']]['q2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    }
                                    summsbrands[xlsxData[i]['Корпорация']]['q2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summsbrands[xlsxData[i]['Корпорация'] + xlsxData[i]['Прямая закупка']]['q2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summsbrands[xlsxData[i]['Прямая закупка']]['q2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                } else if (Number(vDates[2]) == (window.Year - 1)) {
                                    summsQ2['cur_1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    if (xlsxData[i]['Прямая закупка'] === 'Indirect') {
                                        summsQ2['curi_1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                        summssegments[xlsxData[i]['Product Segment 1']]['q2_1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                        summssegments[xlsxData[i]['Product Segment 1'] + xlsxData[i]['Корпорация']]['q2_1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    }
                                    summsbrands[xlsxData[i]['Корпорация']]['q2_1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summsbrands[xlsxData[i]['Корпорация'] + xlsxData[i]['Прямая закупка']]['q2_1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summsbrands[xlsxData[i]['Прямая закупка']]['q2_1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                } else if (Number(vDates[2]) == (window.Year - 2)) {
                                    summsQ2['cur_2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    if (xlsxData[i]['Прямая закупка'] === 'Indirect') {
                                        summsQ2['curi_2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                        summssegments[xlsxData[i]['Product Segment 1']]['q2_2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                        summssegments[xlsxData[i]['Product Segment 1'] + xlsxData[i]['Корпорация']]['q2_2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    }
                                    summsbrands[xlsxData[i]['Корпорация']]['q2_2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summsbrands[xlsxData[i]['Корпорация'] + xlsxData[i]['Прямая закупка']]['q2_2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summsbrands[xlsxData[i]['Прямая закупка']]['q2_2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                } else if (Number(vDates[2]) == (window.Year - 3)) {
                                    summsQ2['cur_3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    if (xlsxData[i]['Прямая закупка'] === 'Indirect') {
                                        summsQ2['curi_3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                        summssegments[xlsxData[i]['Product Segment 1']]['q2_3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                        summssegments[xlsxData[i]['Product Segment 1'] + xlsxData[i]['Корпорация']]['q2_3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    }
                                    summsbrands[xlsxData[i]['Корпорация']]['q2_3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summsbrands[xlsxData[i]['Корпорация'] + xlsxData[i]['Прямая закупка']]['q2_3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summsbrands[xlsxData[i]['Прямая закупка']]['q2_3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                }
                            } else if (vDates[0] < '10') {
                                if (Number(vDates[2]) == window.Year) {
                                    summsQ3['cur'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    if (xlsxData[i]['Прямая закупка'] === 'Indirect') {
                                        summsQ3['curi'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                        summssegments[xlsxData[i]['Product Segment 1']]['q3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                        summssegments[xlsxData[i]['Product Segment 1'] + xlsxData[i]['Корпорация']]['q3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    }
                                    summsbrands[xlsxData[i]['Корпорация']]['q3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summsbrands[xlsxData[i]['Корпорация'] + xlsxData[i]['Прямая закупка']]['q3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summsbrands[xlsxData[i]['Прямая закупка']]['q3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                } else if (Number(vDates[2]) == (window.Year - 1)) {
                                    summsQ3['cur_1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    if (xlsxData[i]['Прямая закупка'] === 'Indirect') {
                                        summsQ3['curi_1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                        summssegments[xlsxData[i]['Product Segment 1']]['q3_1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                        summssegments[xlsxData[i]['Product Segment 1'] + xlsxData[i]['Корпорация']]['q3_1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    }
                                    summsbrands[xlsxData[i]['Корпорация']]['q3_1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summsbrands[xlsxData[i]['Корпорация'] + xlsxData[i]['Прямая закупка']]['q3_1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summsbrands[xlsxData[i]['Прямая закупка']]['q3_1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                } else if (Number(vDates[2]) == (window.Year - 2)) {
                                    summsQ3['cur_2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    if (xlsxData[i]['Прямая закупка'] === 'Indirect') {
                                        summsQ3['curi_2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                        summssegments[xlsxData[i]['Product Segment 1']]['q3_2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                        summssegments[xlsxData[i]['Product Segment 1'] + xlsxData[i]['Корпорация']]['q3_2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    }
                                    summsbrands[xlsxData[i]['Корпорация']]['q3_2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summsbrands[xlsxData[i]['Корпорация'] + xlsxData[i]['Прямая закупка']]['q3_2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summsbrands[xlsxData[i]['Прямая закупка']]['q3_2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                } else if (Number(vDates[2]) == (window.Year - 3)) {
                                    summsQ3['cur_3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    if (xlsxData[i]['Прямая закупка'] === 'Indirect') {
                                        summsQ3['curi_3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                        summssegments[xlsxData[i]['Product Segment 1']]['q3_3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                        summssegments[xlsxData[i]['Product Segment 1'] + xlsxData[i]['Корпорация']]['q3_3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    }
                                    summsbrands[xlsxData[i]['Корпорация']]['q3_3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summsbrands[xlsxData[i]['Корпорация'] + xlsxData[i]['Прямая закупка']]['q3_3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summsbrands[xlsxData[i]['Прямая закупка']]['q3_3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                }
                            } else {
                                if (Number(vDates[2]) == window.Year) {
                                    summsQ4['cur'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    if (xlsxData[i]['Прямая закупка'] === 'Indirect') {
                                        summsQ4['curi'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                        summssegments[xlsxData[i]['Product Segment 1']]['q4'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                        summssegments[xlsxData[i]['Product Segment 1'] + xlsxData[i]['Корпорация']]['q4'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    }
                                    summsbrands[xlsxData[i]['Корпорация']]['q4'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summsbrands[xlsxData[i]['Корпорация'] + xlsxData[i]['Прямая закупка']]['q4'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summsbrands[xlsxData[i]['Прямая закупка']]['q4'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                } else if (Number(vDates[2]) == (window.Year - 1)) {
                                    summsQ4['cur_1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    if (xlsxData[i]['Прямая закупка'] === 'Indirect') {
                                        summsQ4['curi_1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                        summssegments[xlsxData[i]['Product Segment 1']]['q4_1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                        summssegments[xlsxData[i]['Product Segment 1'] + xlsxData[i]['Корпорация']]['q4_1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    }
                                    summsbrands[xlsxData[i]['Корпорация']]['q4_1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summsbrands[xlsxData[i]['Корпорация'] + xlsxData[i]['Прямая закупка']]['q4_1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summsbrands[xlsxData[i]['Прямая закупка']]['q4_1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                } else if (Number(vDates[2]) == (window.Year - 2)) {
                                    summsQ4['cur_2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    if (xlsxData[i]['Прямая закупка'] === 'Indirect') {
                                        summsQ4['curi_2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                        summssegments[xlsxData[i]['Product Segment 1']]['q4_2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                        summssegments[xlsxData[i]['Product Segment 1'] + xlsxData[i]['Корпорация']]['q4_2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    }
                                    summsbrands[xlsxData[i]['Корпорация'] + xlsxData[i]['Прямая закупка']]['q4_2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summsbrands[xlsxData[i]['Корпорация']]['q4_2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summsbrands[xlsxData[i]['Прямая закупка']]['q4_2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                } else if (Number(vDates[2]) == (window.Year - 3)) {
                                    summsQ4['cur_3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    if (xlsxData[i]['Прямая закупка'] === 'Indirect') {
                                        summsQ4['curi_3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                        summssegments[xlsxData[i]['Product Segment 1']]['q4_3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                        summssegments[xlsxData[i]['Product Segment 1'] + xlsxData[i]['Корпорация']]['q4_3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    }
                                    summsbrands[xlsxData[i]['Корпорация'] + xlsxData[i]['Прямая закупка']]['q4_3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summsbrands[xlsxData[i]['Корпорация']]['q4_3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summsbrands[xlsxData[i]['Прямая закупка']]['q4_3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                }
                            };
                            summsbrands[xlsxData[i]['Корпорация']]['all'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                            summsbrands[xlsxData[i]['Корпорация'] + xlsxData[i]['Прямая закупка']]['all'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                            summsbrands[xlsxData[i]['Прямая закупка']]['all'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                            summsall += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                            if (xlsxData[i]['Прямая закупка'] === 'Indirect') {
                                summssegments[xlsxData[i]['Product Segment 1']]['all'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                summsallInd += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                summssegments[xlsxData[i]['Product Segment 1'] + xlsxData[i]['Корпорация']]['all'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                summsallIndbrands[xlsxData[i]['Корпорация']] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                            }
                        };
                    };
                    if (brands.length < 1) {
                        alert('Нет информации по выбранному BU коду за выбранные диапазоны дат!');
                        window.MarkCodes = ['САМСУНГ МЕДИСОН КО.; ЛТД.', ];
                        window.MarkNames = ['SAMSUNG', ];
                        window.AddMarkSelectsOptions();
                        document.getElementsByClassName('FifthPageMark')[0].innerHTML = window.MarkNames[0] + ': Indirect Channel';
                        return;
                    };
                    let maxbrands = [brands[0], ];
                    let maxsumms = [summsbrands[brands[0]]['all'], ];
                    for (let i = 1; i < brands.length; i++) {
                        for (let j = 0; j < maxbrands.length; j++) {
                            if (summsbrands[brands[i]]['all'] > maxsumms[j]) {
                                for (let k = maxbrands.length; k > j; k--) {
                                    maxsumms[k] = maxsumms[k - 1];
                                    maxbrands[k] = maxbrands[k - 1];
                                };
                                maxsumms[j] = summsbrands[brands[i]]['all'];
                                maxbrands[j] = brands[i];
                                break;
                            } else if (j == (maxbrands.length - 1)) {
                                maxsumms[j + 1] = summsbrands[brands[i]]['all'];
                                maxbrands[j + 1] = brands[i];
                                break;
                            };
                        };
                    };
                    let maxbrandsInd = [brandsInd[0], ];
                    let maxsummsInd = [summsbrands[brandsInd[0] + 'Indirect']['all'], ];
                    for (let i = 1; i < brandsInd.length; i++) {
                        for (let j = 0; j < maxbrandsInd.length; j++) {
                            if (summsbrands[brandsInd[i] + 'Indirect']['all'] > maxsummsInd[j]) {
                                for (let k = maxbrandsInd.length; k > j; k--) {
                                    maxsummsInd[k] = maxsummsInd[k - 1];
                                    maxbrandsInd[k] = maxbrandsInd[k - 1];
                                };
                                maxsummsInd[j] = summsbrands[brandsInd[i] + 'Indirect']['all'];
                                maxbrandsInd[j] = brandsInd[i];
                                break;
                            } else if (j == (maxbrandsInd.length - 1)) {
                                maxsummsInd[j + 1] = summsbrands[brandsInd[i] + 'Indirect']['all'];
                                maxbrandsInd[j + 1] = brandsInd[i];
                                break;
                            };
                        };
                    };
                    brands.push('Other');
                    summsbrands['Other'] = {
                        'all': 0,
                        'mtm': 0,
                        'mtm1': 0,
                        'mtm2': 0,
                        'mtm3': 0,
                        'ytd': 0,
                        'ytd1': 0,
                        'ytd2': 0,
                        'ytd3': 0,
                        'q1': 0,
                        'q1_1': 0,
                        'q1_2': 0,
                        'q1_3': 0,
                        'q2': 0,
                        'q2_1': 0,
                        'q2_2': 0,
                        'q2_3': 0,
                        'q3': 0,
                        'q3_1': 0,
                        'q3_2': 0,
                        'q3_3': 0,
                        'q4': 0,
                        'q4_1': 0,
                        'q4_2': 0,
                        'q4_3': 0,
                    };
                    summsbrands['OtherIndirect'] = {
                        'all': 0,
                        'mtm': 0,
                        'mtm1': 0,
                        'mtm2': 0,
                        'mtm3': 0,
                        'ytd': 0,
                        'ytd1': 0,
                        'ytd2': 0,
                        'ytd3': 0,
                        'q1': 0,
                        'q1_1': 0,
                        'q1_2': 0,
                        'q1_3': 0,
                        'q2': 0,
                        'q2_1': 0,
                        'q2_2': 0,
                        'q2_3': 0,
                        'q3': 0,
                        'q3_1': 0,
                        'q3_2': 0,
                        'q3_3': 0,
                        'q4': 0,
                        'q4_1': 0,
                        'q4_2': 0,
                        'q4_3': 0,
                    };
                    segments.forEach(function(item) {
                        summssegments[item + 'Other'] = {};
                        summssegments[item + 'Other']['all'] = 0;
                        summssegments[item + 'Other']['mtm'] = 0;
                        summssegments[item + 'Other']['mtm1'] = 0;
                        summssegments[item + 'Other']['mtm2'] = 0;
                        summssegments[item + 'Other']['mtm3'] = 0;
                        summssegments[item + 'Other']['ytd'] = 0;
                        summssegments[item + 'Other']['ytd1'] = 0;
                        summssegments[item + 'Other']['ytd2'] = 0;
                        summssegments[item + 'Other']['ytd3'] = 0;
                        summssegments[item + 'Other']['q1'] = 0;
                        summssegments[item + 'Other']['q1_1'] = 0;
                        summssegments[item + 'Other']['q1_2'] = 0;
                        summssegments[item + 'Other']['q1_3'] = 0;
                        summssegments[item + 'Other']['q2'] = 0;
                        summssegments[item + 'Other']['q2_1'] = 0;
                        summssegments[item + 'Other']['q2_2'] = 0;
                        summssegments[item + 'Other']['q2_3'] = 0;
                        summssegments[item + 'Other']['q3'] = 0;
                        summssegments[item + 'Other']['q3_1'] = 0;
                        summssegments[item + 'Other']['q3_2'] = 0;
                        summssegments[item + 'Other']['q3_3'] = 0;
                        summssegments[item + 'Other']['q4'] = 0;
                        summssegments[item + 'Other']['q4_1'] = 0;
                        summssegments[item + 'Other']['q4_2'] = 0;
                        summssegments[item + 'Other']['q4_3'] = 0;
                    });
                    for (let i = 0; i < (brands.length - 1); i++) {
                        let f = false;
                        for (let j = 0; j < 6; j++) {
                            if (brands[i] === maxbrands[j]) {
                                f = true;
                                break;
                            };
                        };
                        if (f) {
                            continue;
                        };
                        summsbrands['Other']['all'] += summsbrands[brands[i]]['all'];
                        summsbrands['Other']['mtm'] += summsbrands[brands[i]]['mtm'];
                        summsbrands['Other']['mtm1'] += summsbrands[brands[i]]['mtm1'];
                        summsbrands['Other']['mtm2'] += summsbrands[brands[i]]['mtm2'];
                        summsbrands['Other']['mtm3'] += summsbrands[brands[i]]['mtm3'];
                        summsbrands['Other']['ytd'] += summsbrands[brands[i]]['ytd'];
                        summsbrands['Other']['ytd1'] += summsbrands[brands[i]]['ytd1'];
                        summsbrands['Other']['ytd2'] += summsbrands[brands[i]]['ytd2'];
                        summsbrands['Other']['ytd3'] += summsbrands[brands[i]]['ytd3'];
                        summsbrands['Other']['q1'] += summsbrands[brands[i]]['q1'];
                        summsbrands['Other']['q1_1'] += summsbrands[brands[i]]['q1_1'];
                        summsbrands['Other']['q1_2'] += summsbrands[brands[i]]['q1_2'];
                        summsbrands['Other']['q1_3'] += summsbrands[brands[i]]['q1_3'];
                        summsbrands['Other']['q2'] += summsbrands[brands[i]]['q2'];
                        summsbrands['Other']['q2_1'] += summsbrands[brands[i]]['q2_1'];
                        summsbrands['Other']['q2_2'] += summsbrands[brands[i]]['q2_2'];
                        summsbrands['Other']['q2_3'] += summsbrands[brands[i]]['q2_3'];
                        summsbrands['Other']['q3'] += summsbrands[brands[i]]['q3'];
                        summsbrands['Other']['q3_1'] += summsbrands[brands[i]]['q3_1'];
                        summsbrands['Other']['q3_2'] += summsbrands[brands[i]]['q3_2'];
                        summsbrands['Other']['q3_3'] += summsbrands[brands[i]]['q3_3'];
                        summsbrands['Other']['q4'] += summsbrands[brands[i]]['q4'];
                        summsbrands['Other']['q4_1'] += summsbrands[brands[i]]['q4_1'];
                        summsbrands['Other']['q4_2'] += summsbrands[brands[i]]['q4_2'];
                        summsbrands['Other']['q4_3'] += summsbrands[brands[i]]['q4_3'];
                    };
                    maxsumms[maxbrands.length] = summsbrands['Other']['all'];
                    maxbrands.push('Other');
                    brandsInd.push('Other');
                    for (let i = 0; i < (brandsInd.length - 1); i++) {
                        let f = false;
                        for (let j = 0; j < 6; j++) {
                            if (brandsInd[i] === maxbrandsInd[j]) {
                                f = true;
                                break;
                            };
                        };
                        if (f) {
                            continue;
                        };
                        summsbrands['OtherIndirect']['all'] += summsbrands[brandsInd[i] + 'Indirect']['all'];
                        summsbrands['OtherIndirect']['mtm'] += summsbrands[brandsInd[i] + 'Indirect']['mtm'];
                        summsbrands['OtherIndirect']['mtm1'] += summsbrands[brandsInd[i] + 'Indirect']['mtm1'];
                        summsbrands['OtherIndirect']['mtm2'] += summsbrands[brandsInd[i] + 'Indirect']['mtm2'];
                        summsbrands['OtherIndirect']['mtm3'] += summsbrands[brandsInd[i] + 'Indirect']['mtm3'];
                        summsbrands['OtherIndirect']['ytd'] += summsbrands[brandsInd[i] + 'Indirect']['ytd'];
                        summsbrands['OtherIndirect']['ytd1'] += summsbrands[brandsInd[i] + 'Indirect']['ytd1'];
                        summsbrands['OtherIndirect']['ytd2'] += summsbrands[brandsInd[i] + 'Indirect']['ytd2'];
                        summsbrands['OtherIndirect']['ytd3'] += summsbrands[brandsInd[i] + 'Indirect']['ytd3'];
                        summsbrands['OtherIndirect']['q1'] += summsbrands[brandsInd[i] + 'Indirect']['q1'];
                        summsbrands['OtherIndirect']['q1_1'] += summsbrands[brandsInd[i] + 'Indirect']['q1_1'];
                        summsbrands['OtherIndirect']['q1_2'] += summsbrands[brandsInd[i] + 'Indirect']['q1_2'];
                        summsbrands['OtherIndirect']['q1_3'] += summsbrands[brandsInd[i] + 'Indirect']['q1_3'];
                        summsbrands['OtherIndirect']['q2'] += summsbrands[brandsInd[i] + 'Indirect']['q2'];
                        summsbrands['OtherIndirect']['q2_1'] += summsbrands[brandsInd[i] + 'Indirect']['q2_1'];
                        summsbrands['OtherIndirect']['q2_2'] += summsbrands[brandsInd[i] + 'Indirect']['q2_2'];
                        summsbrands['OtherIndirect']['q2_3'] += summsbrands[brandsInd[i] + 'Indirect']['q2_3'];
                        summsbrands['OtherIndirect']['q3'] += summsbrands[brandsInd[i] + 'Indirect']['q3'];
                        summsbrands['OtherIndirect']['q3_1'] += summsbrands[brandsInd[i] + 'Indirect']['q3_1'];
                        summsbrands['OtherIndirect']['q3_2'] += summsbrands[brandsInd[i] + 'Indirect']['q3_2'];
                        summsbrands['OtherIndirect']['q3_3'] += summsbrands[brandsInd[i] + 'Indirect']['q3_3'];
                        summsbrands['OtherIndirect']['q4'] += summsbrands[brandsInd[i] + 'Indirect']['q4'];
                        summsbrands['OtherIndirect']['q4_1'] += summsbrands[brandsInd[i] + 'Indirect']['q4_1'];
                        summsbrands['OtherIndirect']['q4_2'] += summsbrands[brandsInd[i] + 'Indirect']['q4_2'];
                        summsbrands['OtherIndirect']['q4_3'] += summsbrands[brandsInd[i] + 'Indirect']['q4_3'];
                        segments.forEach(function(item) {
                            summssegments[item + 'Other']['all'] += summssegments[item + brandsInd[i]]['all'];
                            summssegments[item + 'Other']['mtm'] += summssegments[item + brandsInd[i]]['mtm'];
                            summssegments[item + 'Other']['mtm1'] += summssegments[item + brandsInd[i]]['mtm1'];
                            summssegments[item + 'Other']['mtm2'] += summssegments[item + brandsInd[i]]['mtm2'];
                            summssegments[item + 'Other']['mtm3'] += summssegments[item + brandsInd[i]]['mtm3'];
                            summssegments[item + 'Other']['ytd'] += summssegments[item + brandsInd[i]]['ytd'];
                            summssegments[item + 'Other']['ytd1'] += summssegments[item + brandsInd[i]]['ytd1'];
                            summssegments[item + 'Other']['ytd2'] += summssegments[item + brandsInd[i]]['ytd2'];
                            summssegments[item + 'Other']['ytd3'] += summssegments[item + brandsInd[i]]['ytd3'];
                            summssegments[item + 'Other']['q1'] += summssegments[item + brandsInd[i]]['q1'];
                            summssegments[item + 'Other']['q1_1'] += summssegments[item + brandsInd[i]]['q1_1'];
                            summssegments[item + 'Other']['q1_2'] += summssegments[item + brandsInd[i]]['q1_2'];
                            summssegments[item + 'Other']['q1_3'] += summssegments[item + brandsInd[i]]['q1_3'];
                            summssegments[item + 'Other']['q2'] += summssegments[item + brandsInd[i]]['q2'];
                            summssegments[item + 'Other']['q2_1'] += summssegments[item + brandsInd[i]]['q2_1'];
                            summssegments[item + 'Other']['q2_2'] += summssegments[item + brandsInd[i]]['q2_2'];
                            summssegments[item + 'Other']['q2_3'] += summssegments[item + brandsInd[i]]['q2_3'];
                            summssegments[item + 'Other']['q3'] += summssegments[item + brandsInd[i]]['q3'];
                            summssegments[item + 'Other']['q3_1'] += summssegments[item + brandsInd[i]]['q3_1'];
                            summssegments[item + 'Other']['q3_2'] += summssegments[item + brandsInd[i]]['q3_2'];
                            summssegments[item + 'Other']['q3_3'] += summssegments[item + brandsInd[i]]['q3_3'];
                            summssegments[item + 'Other']['q4'] += summssegments[item + brandsInd[i]]['q4'];
                            summssegments[item + 'Other']['q4_1'] += summssegments[item + brandsInd[i]]['q4_1'];
                            summssegments[item + 'Other']['q4_2'] += summssegments[item + brandsInd[i]]['q4_2'];
                            summssegments[item + 'Other']['q4_3'] += summssegments[item + brandsInd[i]]['q4_3'];
                        });
                    };
                    maxsummsInd[maxbrandsInd.length] = summsbrands['OtherIndirect']['all'];
                    maxbrandsInd.push('Other');
                    brandsInd.forEach(function(item) {
                        summsbrands5[item] = {};
                        summsbrands5[item]['all'] = summsbrands[item + 'Indirect']['all'];
                        summsbrands5[item]['mtm'] = summsbrands[item + 'Indirect']['mtm'];
                        summsbrands5[item]['mtm1'] = summsbrands[item + 'Indirect']['mtm1'];
                        summsbrands5[item]['mtm2'] = summsbrands[item + 'Indirect']['mtm2'];
                        summsbrands5[item]['mtm3'] = summsbrands[item + 'Indirect']['mtm3'];
                        summsbrands5[item]['ytd'] = summsbrands[item + 'Indirect']['ytd'];
                        summsbrands5[item]['ytd1'] = summsbrands[item + 'Indirect']['ytd1'];
                        summsbrands5[item]['ytd2'] = summsbrands[item + 'Indirect']['ytd2'];
                        summsbrands5[item]['ytd3'] = summsbrands[item + 'Indirect']['ytd3'];
                        summsbrands5[item]['q1'] = summsbrands[item + 'Indirect']['q1'];
                        summsbrands5[item]['q1_1'] = summsbrands[item + 'Indirect']['q1_1'];
                        summsbrands5[item]['q1_2'] = summsbrands[item + 'Indirect']['q1_2'];
                        summsbrands5[item]['q1_3'] = summsbrands[item + 'Indirect']['q1_3'];
                        summsbrands5[item]['q2'] = summsbrands[item + 'Indirect']['q2'];
                        summsbrands5[item]['q2_1'] = summsbrands[item + 'Indirect']['q2_1'];
                        summsbrands5[item]['q2_2'] = summsbrands[item + 'Indirect']['q2_2'];
                        summsbrands5[item]['q2_3'] = summsbrands[item + 'Indirect']['q2_3'];
                        summsbrands5[item]['q3'] = summsbrands[item + 'Indirect']['q3'];
                        summsbrands5[item]['q3_1'] = summsbrands[item + 'Indirect']['q3_1'];
                        summsbrands5[item]['q3_2'] = summsbrands[item + 'Indirect']['q3_2'];
                        summsbrands5[item]['q3_3'] = summsbrands[item + 'Indirect']['q3_3'];
                        summsbrands5[item]['q4'] = summsbrands[item + 'Indirect']['q4'];
                        summsbrands5[item]['q4_1'] = summsbrands[item + 'Indirect']['q4_1'];
                        summsbrands5[item]['q4_2'] = summsbrands[item + 'Indirect']['q4_2'];
                        summsbrands5[item]['q4_3'] = summsbrands[item + 'Indirect']['q4_3'];
                    });
                    summsbrands_t['Indirect'] = {};
                    summsbrands_t['Direct'] = {};
                    for (let key in summsbrands['Indirect']) {
                        summsbrands_t['Indirect'][key] = Math.floor(summsbrands['Indirect'][key] / 100000) / 10000;
                        summsbrands_t['Direct'][key] = Math.floor(summsbrands['Direct'][key] / 100000) / 10000;
                    };
                    brands.forEach(function(item) {
                        summsbrands_t[item] = {};
                        summsbrands_t[item + 'Indirect'] = {};
                        for (let key in summsbrands[item]) {
                            summsbrands_t[item][key] = summsbrands[item][key];
                            summsbrands_t[item + 'Indirect'][key] = summsbrands[item + 'Indirect'][key];
                        };
                        summsbrands[item]['all'] = ((summsall > 0) ? summsbrands[item]['all'] / summsall : 0.0);
                        summsbrands[item]['mtm'] = ((summsMTM['cur'] > 0) ? summsbrands[item]['mtm'] / summsMTM['cur'] : 0.0);
                        summsbrands[item]['mtm1'] = ((summsMTM['cur_1'] > 0) ? summsbrands[item]['mtm1'] / summsMTM['cur_1'] : 0.0);
                        summsbrands[item]['mtm2'] = ((summsMTM['cur_2'] > 0) ? summsbrands[item]['mtm2'] / summsMTM['cur_2'] : 0.0);
                        summsbrands[item]['mtm3'] = ((summsMTM['cur_3'] > 0) ? summsbrands[item]['mtm3'] / summsMTM['cur_3'] : 0.0);
                        summsbrands[item]['ytd'] = ((summsYTD['cur'] > 0) ? summsbrands[item]['ytd'] / summsYTD['cur'] : 0.0);
                        summsbrands[item]['ytd1'] = ((summsYTD['cur_1'] > 0) ? summsbrands[item]['ytd1'] / summsYTD['cur_1'] : 0.0);
                        summsbrands[item]['ytd2'] = ((summsYTD['cur_2'] > 0) ? summsbrands[item]['ytd2'] / summsYTD['cur_2'] : 0.0);
                        summsbrands[item]['ytd3'] = ((summsYTD['cur_3'] > 0) ? summsbrands[item]['ytd3'] / summsYTD['cur_3'] : 0.0);
                        summsbrands[item]['q1'] = ((summsQ1['cur'] > 0) ? summsbrands[item]['q1'] / summsQ1['cur'] : 0.0);
                        summsbrands[item]['q1_1'] = ((summsQ1['cur_1'] > 0) ? summsbrands[item]['q1_1'] / summsQ1['cur_1'] : 0.0);
                        summsbrands[item]['q1_2'] = ((summsQ1['cur_2'] > 0) ? summsbrands[item]['q1_2'] / summsQ1['cur_2'] : 0.0);
                        summsbrands[item]['q1_3'] = ((summsQ1['cur_3'] > 0) ? summsbrands[item]['q1_3'] / summsQ1['cur_3'] : 0.0);
                        summsbrands[item]['q2'] = ((summsQ2['cur'] > 0) ? summsbrands[item]['q2'] / summsQ2['cur'] : 0.0);
                        summsbrands[item]['q2_1'] = ((summsQ2['cur_1'] > 0) ? summsbrands[item]['q2_1'] / summsQ2['cur_1'] : 0.0);
                        summsbrands[item]['q2_2'] = ((summsQ2['cur_2'] > 0) ? summsbrands[item]['q2_2'] / summsQ2['cur_2'] : 0.0);
                        summsbrands[item]['q2_3'] = ((summsQ2['cur_3'] > 0) ? summsbrands[item]['q2_3'] / summsQ2['cur_3'] : 0.0);
                        summsbrands[item]['q3'] = ((summsQ3['cur'] > 0) ? summsbrands[item]['q3'] / summsQ3['cur'] : 0.0);
                        summsbrands[item]['q3_1'] = ((summsQ3['cur_1'] > 0) ? summsbrands[item]['q3_1'] / summsQ3['cur_1'] : 0.0);
                        summsbrands[item]['q3_2'] = ((summsQ3['cur_2'] > 0) ? summsbrands[item]['q3_2'] / summsQ3['cur_2'] : 0.0);
                        summsbrands[item]['q3_3'] = ((summsQ3['cur_3'] > 0) ? summsbrands[item]['q3_3'] / summsQ3['cur_3'] : 0.0);
                        summsbrands[item]['q4'] = ((summsQ4['cur'] > 0) ? summsbrands[item]['q4'] / summsQ4['cur'] : 0.0);
                        summsbrands[item]['q4_1'] = ((summsQ4['cur_1'] > 0) ? summsbrands[item]['q4_1'] / summsQ4['cur_1'] : 0.0);
                        summsbrands[item]['q4_2'] = ((summsQ4['cur_2'] > 0) ? summsbrands[item]['q4_2'] / summsQ4['cur_2'] : 0.0);
                        summsbrands[item]['q4_3'] = ((summsQ4['cur_3'] > 0) ? summsbrands[item]['q4_3'] / summsQ4['cur_3'] : 0.0);
                        summsbrands[item + 'Indirect']['all'] = ((summsallInd > 0) ? summsbrands[item + 'Indirect']['all'] / summsallInd : 0.0);
                        summsbrands[item + 'Indirect']['mtm'] = ((summsMTM['curi'] > 0) ? summsbrands[item + 'Indirect']['mtm'] / summsMTM['curi'] : 0.0);
                        summsbrands[item + 'Indirect']['mtm1'] = ((summsMTM['curi_1'] > 0) ? summsbrands[item + 'Indirect']['mtm1'] / summsMTM['curi_1'] : 0.0);
                        summsbrands[item + 'Indirect']['mtm2'] = ((summsMTM['curi_2'] > 0) ? summsbrands[item + 'Indirect']['mtm2'] / summsMTM['curi_2'] : 0.0);
                        summsbrands[item + 'Indirect']['mtm3'] = ((summsMTM['curi_3'] > 0) ? summsbrands[item + 'Indirect']['mtm3'] / summsMTM['curi_3'] : 0.0);
                        summsbrands[item + 'Indirect']['ytd'] = ((summsYTD['curi'] > 0) ? summsbrands[item + 'Indirect']['ytd'] / summsYTD['curi'] : 0.0);
                        summsbrands[item + 'Indirect']['ytd1'] = ((summsYTD['curi_1'] > 0) ? summsbrands[item + 'Indirect']['ytd1'] / summsYTD['curi_1'] : 0.0);
                        summsbrands[item + 'Indirect']['ytd2'] = ((summsYTD['curi_2'] > 0) ? summsbrands[item + 'Indirect']['ytd2'] / summsYTD['curi_2'] : 0.0);
                        summsbrands[item + 'Indirect']['ytd3'] = ((summsYTD['curi_3'] > 0) ? summsbrands[item + 'Indirect']['ytd3'] / summsYTD['curi_3'] : 0.0);
                        summsbrands[item + 'Indirect']['q1'] = ((summsQ1['curi'] > 0) ? summsbrands[item + 'Indirect']['q1'] / summsQ1['curi'] : 0.0);
                        summsbrands[item + 'Indirect']['q1_1'] = ((summsQ1['curi_1'] > 0) ? summsbrands[item + 'Indirect']['q1_1'] / summsQ1['curi_1'] : 0.0);
                        summsbrands[item + 'Indirect']['q1_2'] = ((summsQ1['curi_2'] > 0) ? summsbrands[item + 'Indirect']['q1_2'] / summsQ1['curi_2'] : 0.0);
                        summsbrands[item + 'Indirect']['q1_3'] = ((summsQ1['curi_3'] > 0) ? summsbrands[item + 'Indirect']['q1_3'] / summsQ1['curi_3'] : 0.0);
                        summsbrands[item + 'Indirect']['q2'] = ((summsQ2['curi'] > 0) ? summsbrands[item + 'Indirect']['q2'] / summsQ2['curi'] : 0.0);
                        summsbrands[item + 'Indirect']['q2_1'] = ((summsQ2['curi_1'] > 0) ? summsbrands[item + 'Indirect']['q2_1'] / summsQ2['curi_1'] : 0.0);
                        summsbrands[item + 'Indirect']['q2_2'] = ((summsQ2['curi_2'] > 0) ? summsbrands[item + 'Indirect']['q2_2'] / summsQ2['curi_2'] : 0.0);
                        summsbrands[item + 'Indirect']['q2_3'] = ((summsQ2['curi_3'] > 0) ? summsbrands[item + 'Indirect']['q2_3'] / summsQ2['curi_3'] : 0.0);
                        summsbrands[item + 'Indirect']['q3'] = ((summsQ3['curi'] > 0) ? summsbrands[item + 'Indirect']['q3'] / summsQ3['curi'] : 0.0);
                        summsbrands[item + 'Indirect']['q3_1'] = ((summsQ3['curi_1'] > 0) ? summsbrands[item + 'Indirect']['q3_1'] / summsQ3['curi_1'] : 0.0);
                        summsbrands[item + 'Indirect']['q3_2'] = ((summsQ3['curi_2'] > 0) ? summsbrands[item + 'Indirect']['q3_2'] / summsQ3['curi_2'] : 0.0);
                        summsbrands[item + 'Indirect']['q3_3'] = ((summsQ3['curi_3'] > 0) ? summsbrands[item + 'Indirect']['q3_3'] / summsQ3['curi_3'] : 0.0);
                        summsbrands[item + 'Indirect']['q4'] = ((summsQ4['curi'] > 0) ? summsbrands[item + 'Indirect']['q4'] / summsQ4['curi'] : 0.0);
                        summsbrands[item + 'Indirect']['q4_1'] = ((summsQ4['curi_1'] > 0) ? summsbrands[item + 'Indirect']['q4_1'] / summsQ4['curi_1'] : 0.0);
                        summsbrands[item + 'Indirect']['q4_2'] = ((summsQ4['curi_2'] > 0) ? summsbrands[item + 'Indirect']['q4_2'] / summsQ4['curi_2'] : 0.0);
                        summsbrands[item + 'Indirect']['q4_3'] = ((summsQ4['curi_3'] > 0) ? summsbrands[item + 'Indirect']['q4_3'] / summsQ4['curi_3'] : 0.0);
                    });
                    segments.forEach(function(item) {
                        summssegments_t[item] = {};
                        for (let key in summssegments[item]) {
                            summssegments_t[item][key] = summssegments[item][key];
                        };
                        summssegments[item]['all'] = ((summsallInd > 0) ? summssegments[item]['all'] / summsallInd : 0.0);
                        summssegments[item]['mtm'] = ((summsMTM['curi'] > 0) ? summssegments[item]['mtm'] / summsMTM['curi'] : 0.0);
                        summssegments[item]['mtm1'] = ((summsMTM['curi_1'] > 0) ? summssegments[item]['mtm1'] / summsMTM['curi_1'] : 0.0);
                        summssegments[item]['mtm2'] = ((summsMTM['curi_2'] > 0) ? summssegments[item]['mtm2'] / summsMTM['curi_2'] : 0.0);
                        summssegments[item]['mtm3'] = ((summsMTM['curi_3'] > 0) ? summssegments[item]['mtm3'] / summsMTM['curi_3'] : 0.0);
                        summssegments[item]['ytd'] = ((summsYTD['curi'] > 0) ? summssegments[item]['ytd'] / summsYTD['curi'] : 0.0);
                        summssegments[item]['ytd1'] = ((summsYTD['curi_1'] > 0) ? summssegments[item]['ytd1'] / summsYTD['curi_1'] : 0.0);
                        summssegments[item]['ytd2'] = ((summsYTD['curi_2'] > 0) ? summssegments[item]['ytd2'] / summsYTD['curi_2'] : 0.0);
                        summssegments[item]['ytd3'] = ((summsYTD['curi_3'] > 0) ? summssegments[item]['ytd3'] / summsYTD['curi_3'] : 0.0);
                        summssegments[item]['q1'] = ((summsQ1['curi'] > 0) ? summssegments[item]['q1'] / summsQ1['curi'] : 0.0);
                        summssegments[item]['q1_1'] = ((summsQ1['curi_1'] > 0) ? summssegments[item]['q1_1'] / summsQ1['curi_1'] : 0.0);
                        summssegments[item]['q1_2'] = ((summsQ1['curi_2'] > 0) ? summssegments[item]['q1_2'] / summsQ1['curi_2'] : 0.0);
                        summssegments[item]['q1_3'] = ((summsQ1['curi_3'] > 0) ? summssegments[item]['q1_3'] / summsQ1['curi_3'] : 0.0);
                        summssegments[item]['q2'] = ((summsQ2['curi'] > 0) ? summssegments[item]['q2'] / summsQ2['curi'] : 0.0);
                        summssegments[item]['q2_1'] = ((summsQ2['curi_1'] > 0) ? summssegments[item]['q2_1'] / summsQ2['curi_1'] : 0.0);
                        summssegments[item]['q2_2'] = ((summsQ2['curi_2'] > 0) ? summssegments[item]['q2_2'] / summsQ2['curi_2'] : 0.0);
                        summssegments[item]['q2_3'] = ((summsQ2['curi_3'] > 0) ? summssegments[item]['q2_3'] / summsQ2['curi_3'] : 0.0);
                        summssegments[item]['q3'] = ((summsQ3['curi'] > 0) ? summssegments[item]['q3'] / summsQ3['curi'] : 0.0);
                        summssegments[item]['q3_1'] = ((summsQ3['curi_1'] > 0) ? summssegments[item]['q3_1'] / summsQ3['curi_1'] : 0.0);
                        summssegments[item]['q3_2'] = ((summsQ3['curi_2'] > 0) ? summssegments[item]['q3_2'] / summsQ3['curi_2'] : 0.0);
                        summssegments[item]['q3_3'] = ((summsQ3['curi_3'] > 0) ? summssegments[item]['q3_3'] / summsQ3['curi_3'] : 0.0);
                        summssegments[item]['q4'] = ((summsQ4['curi'] > 0) ? summssegments[item]['q4'] / summsQ4['curi'] : 0.0);
                        summssegments[item]['q4_1'] = ((summsQ4['curi_1'] > 0) ? summssegments[item]['q4_1'] / summsQ4['curi_1'] : 0.0);
                        summssegments[item]['q4_2'] = ((summsQ4['curi_2'] > 0) ? summssegments[item]['q4_2'] / summsQ4['curi_2'] : 0.0);
                        summssegments[item]['q4_3'] = ((summsQ4['curi_3'] > 0) ? summssegments[item]['q4_3'] / summsQ4['curi_3'] : 0.0);
                        brandsInd.forEach(function(item2) {
                            summssegments_t[item + item2] = {};
                            for (let key in summssegments[item + item2]) {
                                summssegments_t[item + item2][key] = summssegments[item + item2][key];
                            };
                            summssegments[item + item2]['all'] = ((summsallInd > 0) ? summssegments[item + item2]['all'] / summsallInd : 0.0);
                            summssegments[item + item2]['mtm'] = ((summsMTM['curi'] > 0) ? summssegments[item + item2]['mtm'] / summsMTM['curi'] : 0.0);
                            summssegments[item + item2]['mtm1'] = ((summsMTM['curi_1'] > 0) ? summssegments[item + item2]['mtm1'] / summsMTM['curi_1'] : 0.0);
                            summssegments[item + item2]['mtm2'] = ((summsMTM['curi_2'] > 0) ? summssegments[item + item2]['mtm2'] / summsMTM['curi_2'] : 0.0);
                            summssegments[item + item2]['mtm3'] = ((summsMTM['curi_3'] > 0) ? summssegments[item + item2]['mtm3'] / summsMTM['curi_3'] : 0.0);
                            summssegments[item + item2]['ytd'] = ((summsYTD['curi'] > 0) ? summssegments[item + item2]['ytd'] / summsYTD['curi'] : 0.0);
                            summssegments[item + item2]['ytd1'] = ((summsYTD['curi_1'] > 0) ? summssegments[item + item2]['ytd1'] / summsYTD['curi_1'] : 0.0);
                            summssegments[item + item2]['ytd2'] = ((summsYTD['curi_2'] > 0) ? summssegments[item + item2]['ytd2'] / summsYTD['curi_2'] : 0.0);
                            summssegments[item + item2]['ytd3'] = ((summsYTD['curi_3'] > 0) ? summssegments[item + item2]['ytd3'] / summsYTD['curi_3'] : 0.0);
                            summssegments[item + item2]['q1'] = ((summsQ1['curi'] > 0) ? summssegments[item + item2]['q1'] / summsQ1['curi'] : 0.0);
                            summssegments[item + item2]['q1_1'] = ((summsQ1['curi_1'] > 0) ? summssegments[item + item2]['q1_1'] / summsQ1['curi_1'] : 0.0);
                            summssegments[item + item2]['q1_2'] = ((summsQ1['curi_2'] > 0) ? summssegments[item + item2]['q1_2'] / summsQ1['curi_2'] : 0.0);
                            summssegments[item + item2]['q1_3'] = ((summsQ1['curi_3'] > 0) ? summssegments[item + item2]['q1_3'] / summsQ1['curi_3'] : 0.0);
                            summssegments[item + item2]['q2'] = ((summsQ2['curi'] > 0) ? summssegments[item + item2]['q2'] / summsQ2['curi'] : 0.0);
                            summssegments[item + item2]['q2_1'] = ((summsQ2['curi_1'] > 0) ? summssegments[item + item2]['q2_1'] / summsQ2['curi_1'] : 0.0);
                            summssegments[item + item2]['q2_2'] = ((summsQ2['curi_2'] > 0) ? summssegments[item + item2]['q2_2'] / summsQ2['curi_2'] : 0.0);
                            summssegments[item + item2]['q2_3'] = ((summsQ2['curi_3'] > 0) ? summssegments[item + item2]['q2_3'] / summsQ2['curi_3'] : 0.0);
                            summssegments[item + item2]['q3'] = ((summsQ3['curi'] > 0) ? summssegments[item + item2]['q3'] / summsQ3['curi'] : 0.0);
                            summssegments[item + item2]['q3_1'] = ((summsQ3['curi_1'] > 0) ? summssegments[item + item2]['q3_1'] / summsQ3['curi_1'] : 0.0);
                            summssegments[item + item2]['q3_2'] = ((summsQ3['curi_2'] > 0) ? summssegments[item + item2]['q3_2'] / summsQ3['curi_2'] : 0.0);
                            summssegments[item + item2]['q3_3'] = ((summsQ3['curi_3'] > 0) ? summssegments[item + item2]['q3_3'] / summsQ3['curi_3'] : 0.0);
                            summssegments[item + item2]['q4'] = ((summsQ4['curi'] > 0) ? summssegments[item + item2]['q4'] / summsQ4['curi'] : 0.0);
                            summssegments[item + item2]['q4_1'] = ((summsQ4['curi_1'] > 0) ? summssegments[item + item2]['q4_1'] / summsQ4['curi_1'] : 0.0);
                            summssegments[item + item2]['q4_2'] = ((summsQ4['curi_2'] > 0) ? summssegments[item + item2]['q4_2'] / summsQ4['curi_2'] : 0.0);
                            summssegments[item + item2]['q4_3'] = ((summsQ4['curi_3'] > 0) ? summssegments[item + item2]['q4_3'] / summsQ4['curi_3'] : 0.0);
                            summssegments[item + item2]['mtm1'] = Math.floor(summssegments[item + item2]['mtm1'] * 1000) / 10;
                            summssegments[item + item2]['mtm'] = Math.floor(summssegments[item + item2]['mtm'] * 1000) / 10;
                            summssegments[item + item2]['ytd1'] = Math.floor(summssegments[item + item2]['ytd1'] * 1000) / 10;
                            summssegments[item + item2]['ytd'] = Math.floor(summssegments[item + item2]['ytd'] * 1000) / 10;
                            summssegments[item + item2]['q1_1'] = Math.floor(summssegments[item + item2]['q1_1'] * 1000) / 10;
                            summssegments[item + item2]['q2_1'] = Math.floor(summssegments[item + item2]['q2_1'] * 1000) / 10;
                            summssegments[item + item2]['q3_1'] = Math.floor(summssegments[item + item2]['q3_1'] * 1000) / 10;
                            summssegments[item + item2]['q4_1'] = Math.floor(summssegments[item + item2]['q4_1'] * 1000) / 10;
                            summssegments[item + item2]['q1'] = Math.floor(summssegments[item + item2]['q1'] * 1000) / 10;
                            summssegments[item + item2]['q2'] = Math.floor(summssegments[item + item2]['q2'] * 1000) / 10;
                            summssegments[item + item2]['q3'] = Math.floor(summssegments[item + item2]['q3'] * 1000) / 10;
                            summssegments[item + item2]['q4'] = Math.floor(summssegments[item + item2]['q4'] * 1000) / 10;
                            summssegments_t[item + item2]['mtm1'] = Math.floor(summssegments_t[item + item2]['mtm1'] / 100000) / 10000;
                            summssegments_t[item + item2]['mtm'] = Math.floor(summssegments_t[item + item2]['mtm'] / 100000) / 10000;
                            summssegments_t[item + item2]['ytd1'] = Math.floor(summssegments_t[item + item2]['ytd1'] / 100000) / 10000;
                            summssegments_t[item + item2]['ytd'] = Math.floor(summssegments_t[item + item2]['ytd'] / 100000) / 10000;
                            summssegments_t[item + item2]['q1_1'] = Math.floor(summssegments_t[item + item2]['q1_1'] / 100000) / 10000;
                            summssegments_t[item + item2]['q2_1'] = Math.floor(summssegments_t[item + item2]['q2_1'] / 100000) / 10000;
                            summssegments_t[item + item2]['q3_1'] = Math.floor(summssegments_t[item + item2]['q3_1'] / 100000) / 10000;
                            summssegments_t[item + item2]['q4_1'] = Math.floor(summssegments_t[item + item2]['q4_1'] / 100000) / 10000;
                            summssegments_t[item + item2]['q1'] = Math.floor(summssegments_t[item + item2]['q1'] / 100000) / 10000;
                            summssegments_t[item + item2]['q2'] = Math.floor(summssegments_t[item + item2]['q2'] / 100000) / 10000;
                            summssegments_t[item + item2]['q3'] = Math.floor(summssegments_t[item + item2]['q3'] / 100000) / 10000;
                            summssegments_t[item + item2]['q4'] = Math.floor(summssegments_t[item + item2]['q4'] / 100000) / 10000;
                        });
                    });
                    let grapgbrands = [];
                    let count = brands.length - 1;
                    if (count > 6) {
                        count = 6;
                    };
                    let elnum = maxbrands.indexOf('ФИЛИПС');
                    if ((elnum != -1) && (elnum < count)) {
                        grapgbrands.push('ФИЛИПС');
                        maxbrands.splice(elnum, 1);
                        count--;
                    };
                    elnum = maxbrands.indexOf('ДЖЕНЕРАЛ ЭЛЕКТРИК');
                    if ((elnum != -1) && (elnum < count)) {
                        grapgbrands.push('ДЖЕНЕРАЛ ЭЛЕКТРИК');
                        maxbrands.splice(elnum, 1);
                        count--;
                    };
                    elnum = maxbrands.indexOf('САМСУНГ МЕДИСОН КО.; ЛТД.');
                    if ((elnum != -1) && (elnum < count)) {
                        grapgbrands.push('САМСУНГ МЕДИСОН КО.; ЛТД.');
                        maxbrands.splice(elnum, 1);
                        count--;
                    };
                    elnum = maxbrands.indexOf('СИМЕНС АГ');
                    if ((elnum != -1) && (elnum < count)) {
                        grapgbrands.push('СИМЕНС АГ');
                        maxbrands.splice(elnum, 1);
                        count--;
                    };
                    elnum = maxbrands.indexOf('МИНДРЕЙ');
                    if ((elnum != -1) && (elnum < count)) {
                        grapgbrands.push('МИНДРЕЙ');
                        maxbrands.splice(elnum, 1);
                        count--;
                    };
                    elnum = maxbrands.indexOf('ХИТАЧИ АЛОКА МЕДИКАЛ');
                    if ((elnum != -1) && (elnum < count)) {
                        grapgbrands.push('ХИТАЧИ АЛОКА МЕДИКАЛ');
                        maxbrands.splice(elnum, 1);
                        count--;
                    };
                    for (let i = 0; i < count; i++) {
                        grapgbrands.push(maxbrands[i]);
                    };
                    grapgbrands.push('Other');
                    let grapgbrandsind = [];
                    count = brandsInd.length - 1;
                    if (count > 6) {
                        count = 6;
                    };
                    elnum = maxbrandsInd.indexOf('ФИЛИПС');
                    if ((elnum != -1) && (elnum < count)) {
                        grapgbrandsind.push('ФИЛИПС');
                        maxbrandsInd.splice(elnum, 1);
                        count--;
                    };
                    elnum = maxbrandsInd.indexOf('ДЖЕНЕРАЛ ЭЛЕКТРИК');
                    if ((elnum != -1) && (elnum < count)) {
                        grapgbrandsind.push('ДЖЕНЕРАЛ ЭЛЕКТРИК');
                        maxbrandsInd.splice(elnum, 1);
                        count--;
                    };
                    elnum = maxbrandsInd.indexOf('САМСУНГ МЕДИСОН КО.; ЛТД.');
                    if ((elnum != -1) && (elnum < count)) {
                        grapgbrandsind.push('САМСУНГ МЕДИСОН КО.; ЛТД.');
                        maxbrandsInd.splice(elnum, 1);
                        count--;
                    };
                    elnum = maxbrandsInd.indexOf('СИМЕНС АГ');
                    if ((elnum != -1) && (elnum < count)) {
                        grapgbrandsind.push('СИМЕНС АГ');
                        maxbrandsInd.splice(elnum, 1);
                        count--;
                    };
                    elnum = maxbrandsInd.indexOf('МИНДРЕЙ');
                    if ((elnum != -1) && (elnum < count)) {
                        grapgbrandsind.push('МИНДРЕЙ');
                        maxbrandsInd.splice(elnum, 1);
                        count--;
                    };
                    elnum = maxbrandsInd.indexOf('ХИТАЧИ АЛОКА МЕДИКАЛ');
                    if ((elnum != -1) && (elnum < count)) {
                        grapgbrandsind.push('ХИТАЧИ АЛОКА МЕДИКАЛ');
                        maxbrandsInd.splice(elnum, 1);
                        count--;
                    };
                    for (let i = 0; i < count; i++) {
                        grapgbrandsind.push(maxbrandsInd[i]);
                    };
                    grapgbrandsind.push('Other');
                    let mydata1_1 = [];
                    let mydata2_1 = [];
                    let mydata4_1 = [];
                    let mydata1_2 = [];
                    let mydata2_2 = [];
                    let mydata4_2 = [];
                    let mydata1_3 = [];
                    let mydata2_3 = [];
                    let mydata4_3 = [];
                    let mydata1_4 = [];
                    let mydata2_4 = [];
                    let mydata4_4 = [];
                    let mydata1_5 = [];
                    let mydata2_5 = [];
                    let mydata4_5 = [];
                    window.ChartData3 = [
                        [],
                        [{
                            label: 'Direct',
                            backgroundColor: 'rgba(84, 150, 211, 1)',
                        }, {
                            label: 'Indirect',
                            backgroundColor: 'rgba(237, 125, 49, 1)',
                        }, ],
                        [],
                        [],
                        [],
                    ];
                    let diff = {};
                    let diffInd = {};
                    window.PercData1 = [
                        [
                            ((summsMTM['cur_1'] > 0) ? Math.floor(summsbrands['Indirect']['mtm1'] / summsMTM['cur_1'] * 1000) / 10 : 100.0),
                            ((summsMTM['cur'] > 0) ? Math.floor(summsbrands['Indirect']['mtm'] / summsMTM['cur'] * 1000) / 10 : 100.0),
                        ],
                        [
                            ((summsMTM['cur_1'] > 0) ? Math.floor(summsbrands['Direct']['mtm1'] / summsMTM['cur_1'] * 1000) / 10 : 100.0),
                            ((summsMTM['cur'] > 0) ? Math.floor(summsbrands['Direct']['mtm'] / summsMTM['cur'] * 1000) / 10 : 100.0),
                        ],
                    ];
                    window.PercData2 = [
                        [
                            ((summsYTD['cur_1'] > 0) ? Math.floor(summsbrands['Indirect']['ytd1'] / summsYTD['cur_1'] * 1000) / 10 : 100.0),
                            ((summsYTD['cur'] > 0) ? Math.floor(summsbrands['Indirect']['ytd'] / summsYTD['cur'] * 1000) / 10 : 100.0),
                        ],
                        [
                            ((summsYTD['cur_1'] > 0) ? Math.floor(summsbrands['Direct']['ytd1'] / summsYTD['cur_1'] * 1000) / 10 : 100.0),
                            ((summsYTD['cur'] > 0) ? Math.floor(summsbrands['Direct']['ytd'] / summsYTD['cur'] * 1000) / 10 : 100.0),
                        ],
                    ];
                    window.PercData4 = [
                        [
                            ((summsQ1['cur_1'] > 0) ? Math.floor(summsbrands['Indirect']['q1_1'] / summsQ1['cur_1'] * 1000) / 10 : 100.0),
                            ((summsQ2['cur_1'] > 0) ? Math.floor(summsbrands['Indirect']['q2_1'] / summsQ2['cur_1'] * 1000) / 10 : 100.0),
                            ((summsQ3['cur_1'] > 0) ? Math.floor(summsbrands['Indirect']['q3_1'] / summsQ3['cur_1'] * 1000) / 10 : 100.0),
                            ((summsQ4['cur_1'] > 0) ? Math.floor(summsbrands['Indirect']['q4_1'] / summsQ4['cur_1'] * 1000) / 10 : 100.0),
                            ((summsQ1['cur'] > 0) ? Math.floor(summsbrands['Indirect']['q1'] / summsQ1['cur'] * 1000) / 10 : 100.0),
                            ((summsQ2['cur'] > 0) ? Math.floor(summsbrands['Indirect']['q2'] / summsQ2['cur'] * 1000) / 10 : 100.0),
                            ((summsQ3['cur'] > 0) ? Math.floor(summsbrands['Indirect']['q3'] / summsQ3['cur'] * 1000) / 10 : 100.0),
                            ((summsQ4['cur'] > 0) ? Math.floor(summsbrands['Indirect']['q4'] / summsQ4['cur'] * 1000) / 10 : 100.0),
                        ],
                        [
                            ((summsQ1['cur_1'] > 0) ? Math.floor(summsbrands['Direct']['q1_1'] / summsQ1['cur_1'] * 1000) / 10 : 100.0),
                            ((summsQ2['cur_1'] > 0) ? Math.floor(summsbrands['Direct']['q2_1'] / summsQ2['cur_1'] * 1000) / 10 : 100.0),
                            ((summsQ3['cur_1'] > 0) ? Math.floor(summsbrands['Direct']['q3_1'] / summsQ3['cur_1'] * 1000) / 10 : 100.0),
                            ((summsQ4['cur_1'] > 0) ? Math.floor(summsbrands['Direct']['q4_1'] / summsQ4['cur_1'] * 1000) / 10 : 100.0),
                            ((summsQ1['cur'] > 0) ? Math.floor(summsbrands['Direct']['q1'] / summsQ1['cur'] * 1000) / 10 : 100.0),
                            ((summsQ2['cur'] > 0) ? Math.floor(summsbrands['Direct']['q2'] / summsQ2['cur'] * 1000) / 10 : 100.0),
                            ((summsQ3['cur'] > 0) ? Math.floor(summsbrands['Direct']['q3'] / summsQ3['cur'] * 1000) / 10 : 100.0),
                            ((summsQ4['cur'] > 0) ? Math.floor(summsbrands['Direct']['q4'] / summsQ4['cur'] * 1000) / 10 : 100.0),
                        ],
                    ];
                    mydata1_2.push({
                        label: window.DirIndirData['Indirect']['name'],
                        backgroundColor: window.DirIndirData['Indirect']['color'],
                        data: [
                            summsbrands['Indirect']['mtm1'],
                            summsbrands['Indirect']['mtm'],
                        ],
                        tooltipsValues: [
                            summsbrands_t['Indirect']['mtm1'],
                            summsbrands_t['Indirect']['mtm'],
                        ],
                    });
                    mydata2_2.push({
                        label: window.DirIndirData['Indirect']['name'],
                        backgroundColor: window.DirIndirData['Indirect']['color'],
                        data: [
                            summsbrands['Indirect']['ytd1'],
                            summsbrands['Indirect']['ytd'],
                        ],
                        tooltipsValues: [
                            summsbrands_t['Indirect']['ytd1'],
                            summsbrands_t['Indirect']['ytd'],
                        ],
                    });
                    mydata4_2.push({
                        label: window.DirIndirData['Indirect']['name'],
                        backgroundColor: window.DirIndirData['Indirect']['color'],
                        data: [
                            summsbrands['Indirect']['q1_1'],
                            summsbrands['Indirect']['q2_1'],
                            summsbrands['Indirect']['q3_1'],
                            summsbrands['Indirect']['q4_1'],
                            summsbrands['Indirect']['q1'],
                        ],
                        tooltipsValues: [
                            summsbrands_t['Indirect']['q1_1'],
                            summsbrands_t['Indirect']['q2_1'],
                            summsbrands_t['Indirect']['q3_1'],
                            summsbrands_t['Indirect']['q4_1'],
                            summsbrands_t['Indirect']['q1'],
                        ],
                    });
                    if (window.Month > '03') {
                        mydata4_2[0].data.push(summsbrands['Indirect']['q2']);
                        mydata4_2[0].tooltipsValues.push(summsbrands_t['Indirect']['q2']);
                    };
                    if (window.Month > '06') {
                        mydata4_2[0].data.push(summsbrands['Indirect']['q3']);
                        mydata4_2[0].tooltipsValues.push(summsbrands_t['Indirect']['q3']);
                    };
                    if (window.Month > '09') {
                        mydata4_2[0].data.push(summsbrands['Indirect']['q4']);
                        mydata4_2[0].tooltipsValues.push(summsbrands_t['Indirect']['q4']);
                    };
                    mydata1_2.push({
                        label: window.DirIndirData['Direct']['name'],
                        backgroundColor: window.DirIndirData['Direct']['color'],
                        data: [
                            summsbrands['Direct']['mtm1'],
                            summsbrands['Direct']['mtm'],
                        ],
                        tooltipsValues: [
                            summsbrands_t['Direct']['mtm1'],
                            summsbrands_t['Direct']['mtm'],
                        ],
                    });
                    mydata2_2.push({
                        label: window.DirIndirData['Direct']['name'],
                        backgroundColor: window.DirIndirData['Direct']['color'],
                        data: [
                            summsbrands['Direct']['ytd1'],
                            summsbrands['Direct']['ytd'],
                        ],
                        tooltipsValues: [
                            summsbrands_t['Direct']['ytd1'],
                            summsbrands_t['Direct']['ytd'],
                        ],
                    });
                    mydata4_2.push({
                        label: window.DirIndirData['Direct']['name'],
                        backgroundColor: window.DirIndirData['Direct']['color'],
                        data: [
                            summsbrands['Direct']['q1_1'],
                            summsbrands['Direct']['q2_1'],
                            summsbrands['Direct']['q3_1'],
                            summsbrands['Direct']['q4_1'],
                            summsbrands['Direct']['q1'],
                        ],
                        tooltipsValues: [
                            summsbrands_t['Direct']['q1_1'],
                            summsbrands_t['Direct']['q2_1'],
                            summsbrands_t['Direct']['q3_1'],
                            summsbrands_t['Direct']['q4_1'],
                            summsbrands_t['Direct']['q1'],
                        ],
                    });
                    if (window.Month > '03') {
                        mydata4_2[1].data.push(summsbrands['Direct']['q2']);
                        mydata4_2[1].tooltipsValues.push(summsbrands_t['Direct']['q2']);
                    };
                    if (window.Month > '06') {
                        mydata4_2[1].data.push(summsbrands['Direct']['q3']);
                        mydata4_2[1].tooltipsValues.push(summsbrands_t['Direct']['q3']);
                    };
                    if (window.Month > '09') {
                        mydata4_2[1].data.push(summsbrands['Direct']['q4']);
                        mydata4_2[1].tooltipsValues.push(summsbrands_t['Direct']['q4']);
                    };
                    grapgbrands.forEach(function(item) {
                        summsbrands[item]['mtm1'] = Math.floor(summsbrands[item]['mtm1'] * 1000) / 10;
                        summsbrands[item]['mtm'] = Math.floor(summsbrands[item]['mtm'] * 1000) / 10;
                        summsbrands[item]['ytd1'] = Math.floor(summsbrands[item]['ytd1'] * 1000) / 10;
                        summsbrands[item]['ytd'] = Math.floor(summsbrands[item]['ytd'] * 1000) / 10;
                        summsbrands[item]['q1_1'] = Math.floor(summsbrands[item]['q1_1'] * 1000) / 10;
                        summsbrands[item]['q2_1'] = Math.floor(summsbrands[item]['q2_1'] * 1000) / 10;
                        summsbrands[item]['q3_1'] = Math.floor(summsbrands[item]['q3_1'] * 1000) / 10;
                        summsbrands[item]['q4_1'] = Math.floor(summsbrands[item]['q4_1'] * 1000) / 10;
                        summsbrands[item]['q1'] = Math.floor(summsbrands[item]['q1'] * 1000) / 10;
                        summsbrands[item]['q2'] = Math.floor(summsbrands[item]['q2'] * 1000) / 10;
                        summsbrands[item]['q3'] = Math.floor(summsbrands[item]['q3'] * 1000) / 10;
                        summsbrands[item]['q4'] = Math.floor(summsbrands[item]['q4'] * 1000) / 10;
                        summsbrands_t[item]['mtm1'] = Math.floor(summsbrands_t[item]['mtm1'] / 100000) / 10000;
                        summsbrands_t[item]['mtm'] = Math.floor(summsbrands_t[item]['mtm'] / 100000) / 10000;
                        summsbrands_t[item]['ytd1'] = Math.floor(summsbrands_t[item]['ytd1'] / 100000) / 10000;
                        summsbrands_t[item]['ytd'] = Math.floor(summsbrands_t[item]['ytd'] / 100000) / 10000;
                        summsbrands_t[item]['q1_1'] = Math.floor(summsbrands_t[item]['q1_1'] / 100000) / 10000;
                        summsbrands_t[item]['q2_1'] = Math.floor(summsbrands_t[item]['q2_1'] / 100000) / 10000;
                        summsbrands_t[item]['q3_1'] = Math.floor(summsbrands_t[item]['q3_1'] / 100000) / 10000;
                        summsbrands_t[item]['q4_1'] = Math.floor(summsbrands_t[item]['q4_1'] / 100000) / 10000;
                        summsbrands_t[item]['q1'] = Math.floor(summsbrands_t[item]['q1'] / 100000) / 10000;
                        summsbrands_t[item]['q2'] = Math.floor(summsbrands_t[item]['q2'] / 100000) / 10000;
                        summsbrands_t[item]['q3'] = Math.floor(summsbrands_t[item]['q3'] / 100000) / 10000;
                        summsbrands_t[item]['q4'] = Math.floor(summsbrands_t[item]['q4'] / 100000) / 10000;
                        diff[item] = Math.floor((summsbrands[item]['ytd'] - summsbrands[item]['ytd1']) * 10) / 10;
                        mydata1_1.unshift({
                            label: window.BrandsData[item]['name'],
                            backgroundColor: window.BrandsData[item]['color'],
                            data: [
                                summsbrands[item]['mtm1'],
                                summsbrands[item]['mtm'],
                            ],
                            tooltipsValues: [
                                summsbrands_t[item]['mtm1'],
                                summsbrands_t[item]['mtm'],
                            ],
                        });
                        mydata2_1.unshift({
                            label: window.BrandsData[item]['name'],
                            backgroundColor: window.BrandsData[item]['color'],
                            data: [
                                summsbrands[item]['ytd1'],
                                summsbrands[item]['ytd'],
                            ],
                            tooltipsValues: [
                                summsbrands_t[item]['ytd1'],
                                summsbrands_t[item]['ytd'],
                            ],
                        });
                        window.ChartData3[0].push({
                            label: window.BrandsData[item]['name'],
                            backgroundColor: window.BrandsData[item]['color'],
                        });
                        mydata4_1.unshift({
                            label: window.BrandsData[item]['name'],
                            backgroundColor: window.BrandsData[item]['color'],
                            data: [
                                summsbrands[item]['q1_1'],
                                summsbrands[item]['q2_1'],
                                summsbrands[item]['q3_1'],
                                summsbrands[item]['q4_1'],
                                summsbrands[item]['q1'],
                            ],
                            tooltipsValues: [
                                summsbrands_t[item]['q1_1'],
                                summsbrands_t[item]['q2_1'],
                                summsbrands_t[item]['q3_1'],
                                summsbrands_t[item]['q4_1'],
                                summsbrands_t[item]['q1'],
                            ],
                        });
                        if (window.Month > '03') {
                            mydata4_1[0].data.push(summsbrands[item]['q2']);
                            mydata4_1[0].tooltipsValues.push(summsbrands_t[item]['q2']);
                        };
                        if (window.Month > '06') {
                            mydata4_1[0].data.push(summsbrands[item]['q3']);
                            mydata4_1[0].tooltipsValues.push(summsbrands_t[item]['q3']);
                        };
                        if (window.Month > '09') {
                            mydata4_1[0].data.push(summsbrands[item]['q4']);
                            mydata4_1[0].tooltipsValues.push(summsbrands_t[item]['q4']);
                        };
                    });
                    window.MarkCodes = [];
                    window.MarkNames = [];
                    grapgbrandsind.forEach(function(item) {
                        window.MarkCodes.push(item);
                        window.MarkNames.push(window.BrandsData[item].name);
                        summsbrands[item + 'Indirect']['mtm1'] = Math.floor(summsbrands[item + 'Indirect']['mtm1'] * 1000) / 10;
                        summsbrands[item + 'Indirect']['mtm'] = Math.floor(summsbrands[item + 'Indirect']['mtm'] * 1000) / 10;
                        summsbrands[item + 'Indirect']['ytd1'] = Math.floor(summsbrands[item + 'Indirect']['ytd1'] * 1000) / 10;
                        summsbrands[item + 'Indirect']['ytd'] = Math.floor(summsbrands[item + 'Indirect']['ytd'] * 1000) / 10;
                        summsbrands[item + 'Indirect']['q1_1'] = Math.floor(summsbrands[item + 'Indirect']['q1_1'] * 1000) / 10;
                        summsbrands[item + 'Indirect']['q2_1'] = Math.floor(summsbrands[item + 'Indirect']['q2_1'] * 1000) / 10;
                        summsbrands[item + 'Indirect']['q3_1'] = Math.floor(summsbrands[item + 'Indirect']['q3_1'] * 1000) / 10;
                        summsbrands[item + 'Indirect']['q4_1'] = Math.floor(summsbrands[item + 'Indirect']['q4_1'] * 1000) / 10;
                        summsbrands[item + 'Indirect']['q1'] = Math.floor(summsbrands[item + 'Indirect']['q1'] * 1000) / 10;
                        summsbrands[item + 'Indirect']['q2'] = Math.floor(summsbrands[item + 'Indirect']['q2'] * 1000) / 10;
                        summsbrands[item + 'Indirect']['q3'] = Math.floor(summsbrands[item + 'Indirect']['q3'] * 1000) / 10;
                        summsbrands[item + 'Indirect']['q4'] = Math.floor(summsbrands[item + 'Indirect']['q4'] * 1000) / 10;
                        summsbrands_t[item + 'Indirect']['mtm1'] = Math.floor(summsbrands_t[item + 'Indirect']['mtm1'] / 100000) / 10000;
                        summsbrands_t[item + 'Indirect']['mtm'] = Math.floor(summsbrands_t[item + 'Indirect']['mtm'] / 100000) / 10000;
                        summsbrands_t[item + 'Indirect']['ytd1'] = Math.floor(summsbrands_t[item + 'Indirect']['ytd1'] / 100000) / 10000;
                        summsbrands_t[item + 'Indirect']['ytd'] = Math.floor(summsbrands_t[item + 'Indirect']['ytd'] / 100000) / 10000;
                        summsbrands_t[item + 'Indirect']['q1_1'] = Math.floor(summsbrands_t[item + 'Indirect']['q1_1'] / 100000) / 10000;
                        summsbrands_t[item + 'Indirect']['q2_1'] = Math.floor(summsbrands_t[item + 'Indirect']['q2_1'] / 100000) / 10000;
                        summsbrands_t[item + 'Indirect']['q3_1'] = Math.floor(summsbrands_t[item + 'Indirect']['q3_1'] / 100000) / 10000;
                        summsbrands_t[item + 'Indirect']['q4_1'] = Math.floor(summsbrands_t[item + 'Indirect']['q4_1'] / 100000) / 10000;
                        summsbrands_t[item + 'Indirect']['q1'] = Math.floor(summsbrands_t[item + 'Indirect']['q1'] / 100000) / 10000;
                        summsbrands_t[item + 'Indirect']['q2'] = Math.floor(summsbrands_t[item + 'Indirect']['q2'] / 100000) / 10000;
                        summsbrands_t[item + 'Indirect']['q3'] = Math.floor(summsbrands_t[item + 'Indirect']['q3'] / 100000) / 10000;
                        summsbrands_t[item + 'Indirect']['q4'] = Math.floor(summsbrands_t[item + 'Indirect']['q4'] / 100000) / 10000;
                        diffInd[item] = Math.floor((summsbrands[item + 'Indirect']['ytd'] - summsbrands[item + 'Indirect']['ytd1']) * 10) / 10;
                        mydata1_3.unshift({
                            label: window.BrandsData[item]['name'],
                            backgroundColor: window.BrandsData[item]['color'],
                            data: [
                                summsbrands[item + 'Indirect']['mtm1'],
                                summsbrands[item + 'Indirect']['mtm'],
                            ],
                            tooltipsValues: [
                                summsbrands_t[item + 'Indirect']['mtm1'],
                                summsbrands_t[item + 'Indirect']['mtm'],
                            ],
                        });
                        mydata2_3.unshift({
                            label: window.BrandsData[item]['name'],
                            backgroundColor: window.BrandsData[item]['color'],
                            data: [
                                summsbrands[item + 'Indirect']['ytd1'],
                                summsbrands[item + 'Indirect']['ytd'],
                            ],
                            tooltipsValues: [
                                summsbrands_t[item + 'Indirect']['ytd1'],
                                summsbrands_t[item + 'Indirect']['ytd'],
                            ],
                        });
                        window.ChartData3[2].push({
                            label: window.BrandsData[item]['name'],
                            backgroundColor: window.BrandsData[item]['color'],
                        });
                        mydata4_3.unshift({
                            label: window.BrandsData[item]['name'],
                            backgroundColor: window.BrandsData[item]['color'],
                            data: [
                                summsbrands[item + 'Indirect']['q1_1'],
                                summsbrands[item + 'Indirect']['q2_1'],
                                summsbrands[item + 'Indirect']['q3_1'],
                                summsbrands[item + 'Indirect']['q4_1'],
                                summsbrands[item + 'Indirect']['q1'],
                            ],
                            tooltipsValues: [
                                summsbrands_t[item + 'Indirect']['q1_1'],
                                summsbrands_t[item + 'Indirect']['q2_1'],
                                summsbrands_t[item + 'Indirect']['q3_1'],
                                summsbrands_t[item + 'Indirect']['q4_1'],
                                summsbrands_t[item + 'Indirect']['q1'],
                            ],
                        });
                        if (window.Month > '03') {
                            mydata4_3[0].data.push(summsbrands[item + 'Indirect']['q2']);
                            mydata4_3[0].tooltipsValues.push(summsbrands_t[item + 'Indirect']['q2']);
                        };
                        if (window.Month > '06') {
                            mydata4_3[0].data.push(summsbrands[item + 'Indirect']['q3']);
                            mydata4_3[0].tooltipsValues.push(summsbrands_t[item + 'Indirect']['q3']);
                        };
                        if (window.Month > '09') {
                            mydata4_3[0].data.push(summsbrands[item + 'Indirect']['q4']);
                            mydata4_3[0].tooltipsValues.push(summsbrands_t[item + 'Indirect']['q4']);
                        };
                    });
                    if (window.MarkCodes.length < 1) {
                        window.MarkCodes = ['САМСУНГ МЕДИСОН КО.; ЛТД.', ];
                        window.MarkNames = ['SAMSUNG', ];
                    }
                    segments.forEach(function(item) {
                        summssegments[item]['mtm1'] = Math.floor(summssegments[item]['mtm1'] * 1000) / 10;
                        summssegments[item]['mtm'] = Math.floor(summssegments[item]['mtm'] * 1000) / 10;
                        summssegments[item]['ytd1'] = Math.floor(summssegments[item]['ytd1'] * 1000) / 10;
                        summssegments[item]['ytd'] = Math.floor(summssegments[item]['ytd'] * 1000) / 10;
                        summssegments[item]['q1_1'] = Math.floor(summssegments[item]['q1_1'] * 1000) / 10;
                        summssegments[item]['q2_1'] = Math.floor(summssegments[item]['q2_1'] * 1000) / 10;
                        summssegments[item]['q3_1'] = Math.floor(summssegments[item]['q3_1'] * 1000) / 10;
                        summssegments[item]['q4_1'] = Math.floor(summssegments[item]['q4_1'] * 1000) / 10;
                        summssegments[item]['q1'] = Math.floor(summssegments[item]['q1'] * 1000) / 10;
                        summssegments[item]['q2'] = Math.floor(summssegments[item]['q2'] * 1000) / 10;
                        summssegments[item]['q3'] = Math.floor(summssegments[item]['q3'] * 1000) / 10;
                        summssegments[item]['q4'] = Math.floor(summssegments[item]['q4'] * 1000) / 10;
                        summssegments_t[item]['mtm1'] = Math.floor(summssegments_t[item]['mtm1'] / 100000) / 10000;
                        summssegments_t[item]['mtm'] = Math.floor(summssegments_t[item]['mtm'] / 100000) / 10000;
                        summssegments_t[item]['ytd1'] = Math.floor(summssegments_t[item]['ytd1'] / 100000) / 10000;
                        summssegments_t[item]['ytd'] = Math.floor(summssegments_t[item]['ytd'] / 100000) / 10000;
                        summssegments_t[item]['q1_1'] = Math.floor(summssegments_t[item]['q1_1'] / 100000) / 10000;
                        summssegments_t[item]['q2_1'] = Math.floor(summssegments_t[item]['q2_1'] / 100000) / 10000;
                        summssegments_t[item]['q3_1'] = Math.floor(summssegments_t[item]['q3_1'] / 100000) / 10000;
                        summssegments_t[item]['q4_1'] = Math.floor(summssegments_t[item]['q4_1'] / 100000) / 10000;
                        summssegments_t[item]['q1'] = Math.floor(summssegments_t[item]['q1'] / 100000) / 10000;
                        summssegments_t[item]['q2'] = Math.floor(summssegments_t[item]['q2'] / 100000) / 10000;
                        summssegments_t[item]['q3'] = Math.floor(summssegments_t[item]['q3'] / 100000) / 10000;
                        summssegments_t[item]['q4'] = Math.floor(summssegments_t[item]['q4'] / 100000) / 10000;
                    });
                    window.forFifthPage = {};
                    window.forFifthPage.segments = segments;
                    window.forFifthPage.summssegments = summssegments;
                    window.forFifthPage.summsbrands5 = summsbrands5;
                    window.forFifthPage.summssegments_t = summssegments_t;
                    segments.forEach(function(item) {
                        mydata1_4.unshift({
                            label: window.ProdSegData[item]['name'],
                            backgroundColor: window.ProdSegData[item]['color'],
                            data: [
                                summssegments[item]['mtm1'],
                                summssegments[item]['mtm'],
                            ],
                            tooltipsValues: [
                                summssegments_t[item]['mtm1'],
                                summssegments_t[item]['mtm'],
                            ],
                        });
                        mydata2_4.unshift({
                            label: window.ProdSegData[item]['name'],
                            backgroundColor: window.ProdSegData[item]['color'],
                            data: [
                                summssegments[item]['ytd1'],
                                summssegments[item]['ytd'],
                            ],
                            tooltipsValues: [
                                summssegments_t[item]['ytd1'],
                                summssegments_t[item]['ytd'],
                            ],
                        });
                        window.ChartData3[3].push({
                            label: window.ProdSegData[item]['name'],
                            backgroundColor: window.ProdSegData[item]['color'],
                        });
                        mydata4_4.unshift({
                            label: window.ProdSegData[item]['name'],
                            backgroundColor: window.ProdSegData[item]['color'],
                            data: [
                                summssegments[item]['q1_1'],
                                summssegments[item]['q2_1'],
                                summssegments[item]['q3_1'],
                                summssegments[item]['q4_1'],
                                summssegments[item]['q1'],
                            ],
                            tooltipsValues: [
                                summssegments_t[item]['q1_1'],
                                summssegments_t[item]['q2_1'],
                                summssegments_t[item]['q3_1'],
                                summssegments_t[item]['q4_1'],
                                summssegments_t[item]['q1'],
                            ],
                        });
                        if (window.Month > '03') {
                            mydata4_4[0].data.push(summssegments[item]['q2']);
                            mydata4_4[0].tooltipsValues.push(summssegments_t[item]['q2']);
                        };
                        if (window.Month > '06') {
                            mydata4_4[0].data.push(summssegments[item]['q3']);
                            mydata4_4[0].tooltipsValues.push(summssegments_t[item]['q3']);
                        };
                        if (window.Month > '09') {
                            mydata4_4[0].data.push(summssegments[item]['q4']);
                            mydata4_4[0].tooltipsValues.push(summssegments_t[item]['q4']);
                        };
                        mydata1_5.unshift({
                            label: window.ProdSegData[item]['name'],
                            backgroundColor: window.ProdSegData[item]['color'],
                            data: [
                                summssegments[item + window.MarkCodes[0]]['mtm1'],
                                summssegments[item + window.MarkCodes[0]]['mtm'],
                            ],
                            tooltipsValues: [
                                summssegments_t[item + window.MarkCodes[0]]['mtm1'],
                                summssegments_t[item + window.MarkCodes[0]]['mtm'],
                            ],
                        });
                        mydata2_5.unshift({
                            label: window.ProdSegData[item]['name'],
                            backgroundColor: window.ProdSegData[item]['color'],
                            data: [
                                summssegments[item + window.MarkCodes[0]]['ytd1'],
                                summssegments[item + window.MarkCodes[0]]['ytd'],
                            ],
                            tooltipsValues: [
                                summssegments_t[item + window.MarkCodes[0]]['ytd1'],
                                summssegments_t[item + window.MarkCodes[0]]['ytd'],
                            ],
                        });
                        window.ChartData3[4].push({
                            label: window.ProdSegData[item]['name'],
                            backgroundColor: window.ProdSegData[item]['color'],
                        });
                        mydata4_5.unshift({
                            label: window.ProdSegData[item]['name'],
                            backgroundColor: window.ProdSegData[item]['color'],
                            data: [
                                summssegments[item + window.MarkCodes[0]]['q1_1'],
                                summssegments[item + window.MarkCodes[0]]['q2_1'],
                                summssegments[item + window.MarkCodes[0]]['q3_1'],
                                summssegments[item + window.MarkCodes[0]]['q4_1'],
                                summssegments[item + window.MarkCodes[0]]['q1'],
                            ],
                            tooltipsValues: [
                                summssegments_t[item + window.MarkCodes[0]]['q1_1'],
                                summssegments_t[item + window.MarkCodes[0]]['q2_1'],
                                summssegments_t[item + window.MarkCodes[0]]['q3_1'],
                                summssegments_t[item + window.MarkCodes[0]]['q4_1'],
                                summssegments_t[item + window.MarkCodes[0]]['q1'],
                            ],
                        });
                        if (window.Month > '03') {
                            mydata4_5[0].data.push(summssegments[item + window.MarkCodes[0]]['q2']);
                            mydata4_5[0].tooltipsValues.push(summssegments_t[item + window.MarkCodes[0]]['q2']);
                        };
                        if (window.Month > '06') {
                            mydata4_5[0].data.push(summssegments[item + window.MarkCodes[0]]['q3']);
                            mydata4_5[0].tooltipsValues.push(summssegments_t[item + window.MarkCodes[0]]['q3']);
                        };
                        if (window.Month > '09') {
                            mydata4_5[0].data.push(summssegments[item + window.MarkCodes[0]]['q4']);
                            mydata4_5[0].tooltipsValues.push(summssegments_t[item + window.MarkCodes[0]]['q4']);
                        };
                    });
                    window.ChartData1 = [{
                        labels: [
                            window.MonthsForLegend[(Number(window.Month1) - 1)] + '’' + String(window.Year3 - 2000) + '-' + window.MonthsForLegend[(Number(window.Month) - 1)] + '’' + String(window.Year2 - 2000),
                            window.MonthsForLegend[(Number(window.Month1) - 1)] + '’' + String(window.Year1 - 2000) + '-' + window.MonthsForLegend[(Number(window.Month) - 1)] + '’' + String(window.Year - 2000),
                        ],
                        datasets: mydata1_1,
                    }, {
                        labels: [
                            window.MonthsForLegend[(Number(window.Month1) - 1)] + '’' + String(window.Year3 - 2000) + '-' + window.MonthsForLegend[(Number(window.Month) - 1)] + '’' + String(window.Year2 - 2000),
                            window.MonthsForLegend[(Number(window.Month1) - 1)] + '’' + String(window.Year1 - 2000) + '-' + window.MonthsForLegend[(Number(window.Month) - 1)] + '’' + String(window.Year - 2000),
                        ],
                        datasets: mydata1_2,
                    }, {
                        labels: [
                            window.MonthsForLegend[(Number(window.Month1) - 1)] + '’' + String(window.Year3 - 2000) + '-' + window.MonthsForLegend[(Number(window.Month) - 1)] + '’' + String(window.Year2 - 2000),
                            window.MonthsForLegend[(Number(window.Month1) - 1)] + '’' + String(window.Year1 - 2000) + '-' + window.MonthsForLegend[(Number(window.Month) - 1)] + '’' + String(window.Year - 2000),
                        ],
                        datasets: mydata1_3,
                    }, {
                        labels: [
                            window.MonthsForLegend[(Number(window.Month1) - 1)] + '’' + String(window.Year3 - 2000) + '-' + window.MonthsForLegend[(Number(window.Month) - 1)] + '’' + String(window.Year2 - 2000),
                            window.MonthsForLegend[(Number(window.Month1) - 1)] + '’' + String(window.Year1 - 2000) + '-' + window.MonthsForLegend[(Number(window.Month) - 1)] + '’' + String(window.Year - 2000),
                        ],
                        datasets: mydata1_4,
                    }, {
                        labels: [
                            window.MonthsForLegend[(Number(window.Month1) - 1)] + '’' + String(window.Year3 - 2000) + '-' + window.MonthsForLegend[(Number(window.Month) - 1)] + '’' + String(window.Year2 - 2000),
                            window.MonthsForLegend[(Number(window.Month1) - 1)] + '’' + String(window.Year1 - 2000) + '-' + window.MonthsForLegend[(Number(window.Month) - 1)] + '’' + String(window.Year - 2000),
                        ],
                        datasets: mydata1_5,
                    }, ];
                    window.ChartData2 = [{
                        labels: [
                            'YTD’' + String(window.Year2 - 2000) + ' ' + window.MonthsForLegend[(Number(window.Month) - 1)],
                            'YTD’' + String(window.Year - 2000) + ' ' + window.MonthsForLegend[(Number(window.Month) - 1)],
                        ],
                        datasets: mydata2_1,
                    }, {
                        labels: [
                            'YTD’' + String(window.Year2 - 2000) + ' ' + window.MonthsForLegend[(Number(window.Month) - 1)],
                            'YTD’' + String(window.Year - 2000) + ' ' + window.MonthsForLegend[(Number(window.Month) - 1)],
                        ],
                        datasets: mydata2_2,
                    }, {
                        labels: [
                            'YTD’' + String(window.Year2 - 2000) + ' ' + window.MonthsForLegend[(Number(window.Month) - 1)],
                            'YTD’' + String(window.Year - 2000) + ' ' + window.MonthsForLegend[(Number(window.Month) - 1)],
                        ],
                        datasets: mydata2_3,
                    }, {
                        labels: [
                            'YTD’' + String(window.Year2 - 2000) + ' ' + window.MonthsForLegend[(Number(window.Month) - 1)],
                            'YTD’' + String(window.Year - 2000) + ' ' + window.MonthsForLegend[(Number(window.Month) - 1)],
                        ],
                        datasets: mydata2_4,
                    }, {
                        labels: [
                            'YTD’' + String(window.Year2 - 2000) + ' ' + window.MonthsForLegend[(Number(window.Month) - 1)],
                            'YTD’' + String(window.Year - 2000) + ' ' + window.MonthsForLegend[(Number(window.Month) - 1)],
                        ],
                        datasets: mydata2_5,
                    }, ];
                    window.ChartData4 = [{
                        labels: [
                            String(window.Year - 1) + ' Q1',
                            String(window.Year - 1) + ' Q2',
                            String(window.Year - 1) + ' Q3',
                            String(window.Year - 1) + ' Q4',
                            String(window.Year) + ' Q1',
                        ],
                        datasets: mydata4_1,
                    }, {
                        labels: [
                            String(window.Year - 1) + ' Q1',
                            String(window.Year - 1) + ' Q2',
                            String(window.Year - 1) + ' Q3',
                            String(window.Year - 1) + ' Q4',
                            String(window.Year) + ' Q1',
                        ],
                        datasets: mydata4_2,
                    }, {
                        labels: [
                            String(window.Year - 1) + ' Q1',
                            String(window.Year - 1) + ' Q2',
                            String(window.Year - 1) + ' Q3',
                            String(window.Year - 1) + ' Q4',
                            String(window.Year) + ' Q1',
                        ],
                        datasets: mydata4_3,
                    }, {
                        labels: [
                            String(window.Year - 1) + ' Q1',
                            String(window.Year - 1) + ' Q2',
                            String(window.Year - 1) + ' Q3',
                            String(window.Year - 1) + ' Q4',
                            String(window.Year) + ' Q1',
                        ],
                        datasets: mydata4_4,
                    }, {
                        labels: [
                            String(window.Year - 1) + ' Q1',
                            String(window.Year - 1) + ' Q2',
                            String(window.Year - 1) + ' Q3',
                            String(window.Year - 1) + ' Q4',
                            String(window.Year) + ' Q1',
                        ],
                        datasets: mydata4_5,
                    }, ];
                    window.SelectedBU = [
                        window.BUNames[document.getElementById("buselector").selectedIndex],
                        window.BUNames[document.getElementById("buselector").selectedIndex],
                        window.BUNames[document.getElementById("buselector").selectedIndex],
                        window.BUNames[document.getElementById("buselector").selectedIndex],
                        window.BUNames[document.getElementById("buselector").selectedIndex],
                    ];
                    let keysSorted = Object.keys(diff).sort(function(a, b) {
                        return diff[a] - diff[b]
                    });
                    window.SummaryList[0] = ['YTD MS% gainers: ', 'YTD MS% losers: '];
                    for (let i = 0; i < keysSorted.length; i++) {
                        if (diff[keysSorted[i]] < 0) {
                            window.SummaryList[0][1] += window.BrandsData[keysSorted[i]]['name'] + ' ' + diff[keysSorted[i]] + '%,';
                        } else {
                            break;
                        };
                    };
                    for (let i = (keysSorted.length - 1); i > -1; i--) {
                        if (diff[keysSorted[i]] > 0) {
                            window.SummaryList[0][0] += window.BrandsData[keysSorted[i]]['name'] + ' +' + diff[keysSorted[i]] + '%,';
                        } else {
                            break;
                        };
                    };
                    window.SummaryList[0][0].slice(0, window.SummaryList[0][0].length - 1);
                    window.SummaryList[0][1].slice(0, window.SummaryList[0][1].length - 1);
                    window.SummaryList[1] = [
                        'Market growth ' + String(((summsYTD['cur_1'] > 0) ? Math.floor(((summsYTD['cur'] - summsYTD['cur_1']) / summsYTD['cur_1']) * 1000) / 10 : 100.0)) + '% YTD driven by Direct / MTS',
                        'Direct channel (2020 YTD - MTS Moscow only) impacted TTL market dramatically in Q2 / Q3',
                    ];
                    keysSorted = Object.keys(diffInd).sort(function(a, b) {
                        return diffInd[a] - diffInd[b]
                    });
                    window.SummaryList[2] = [
                        'Indirect channel +/- flat ' + window.MonthsForLegend[(Number(window.Month) - 1)] + '’' + String(window.Year - 2000) + ' YTD, with strong growth Q1 / Q2 reversing to sharp decline in Q3',
                        'YTD MS% gainers: ',
                        'YTD MS% losers: ',
                    ];
                    for (let i = 0; i < keysSorted.length; i++) {
                        if (diffInd[keysSorted[i]] < 0) {
                            window.SummaryList[2][2] += window.BrandsData[keysSorted[i]]['name'] + ' ' + diffInd[keysSorted[i]] + '%,';
                        } else {
                            break;
                        };
                    };
                    for (let i = (keysSorted.length - 1); i > -1; i--) {
                        if (diffInd[keysSorted[i]] > 0) {
                            window.SummaryList[2][1] += window.BrandsData[keysSorted[i]]['name'] + ' +' + diffInd[keysSorted[i]] + '%,';
                        } else {
                            break;
                        };
                    };
                    window.SummaryList[2][1].slice(0, window.SummaryList[2][1].length - 1);
                    window.SummaryList[2][2].slice(0, window.SummaryList[2][2].length - 1);
                    window.SummaryList[3] = [
                        '<p style="font-size: 11px; color: #FF0000">Indirect channel +/- flat ' + window.MonthsForLegend[(Number(window.Month) - 1)] + '’' + String(window.Year - 2000) + ' YTD, with strong growth Q1 / Q2 reversing to sharp decline in Q3</p>',
                        '<p style="font-size: 11px; color: #FF0000">Major segments are +/- flat except Low-end compact boosted by Covid / emergency orders in Q2’20</p>',
                        '<p style="font-size: 11px; color: #FF0000">Handheld (Lumify etc.) remains marginal segment</p>',
                    ];
                    window.SummaryList[4] = [
                        '<p style="font-size: 11px; color: #FF0000">MS drivers for ' + window.MarkNames[0] + ': High-end Cart / Low-end Compact</p>',
                        '<p style="font-size: 11px; color: #FF0000">High-end Cart: extended portfolio (6 models)</p>',
                        '<p style="font-size: 11px; color: #FF0000">Low-end Compact: Covid / Emergency demand with decreased lead-times</p>',
                    ];
                    window.Legends[0] = [
                        'Bln.RUB<br>+/- % SPLY',
                        String(Math.floor(summsQ1['cur_1'] / 100000000) / 10) + '<br><font color="#' + ((summsQ1['cur_1'] < summsQ1['cur_2']) ? 'c000' : '008') + '000">' + String(((summsQ1['cur_2'] > 0) ? Math.floor(((summsQ1['cur_1'] - summsQ1['cur_2']) / summsQ1['cur_2']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsQ2['cur_1'] / 100000000) / 10) + '<br><font color="#' + ((summsQ2['cur_1'] < summsQ2['cur_2']) ? 'c000' : '008') + '000">' + String(((summsQ2['cur_2'] > 0) ? Math.floor(((summsQ2['cur_1'] - summsQ2['cur_2']) / summsQ2['cur_2']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsQ3['cur_1'] / 100000000) / 10) + '<br><font color="#' + ((summsQ3['cur_1'] < summsQ3['cur_2']) ? 'c000' : '008') + '000">' + String(((summsQ3['cur_2'] > 0) ? Math.floor(((summsQ3['cur_1'] - summsQ3['cur_2']) / summsQ3['cur_2']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsQ4['cur_1'] / 100000000) / 10) + '<br><font color="#' + ((summsQ4['cur_1'] < summsQ4['cur_2']) ? 'c000' : '008') + '000">' + String(((summsQ4['cur_2'] > 0) ? Math.floor(((summsQ4['cur_1'] - summsQ4['cur_2']) / summsQ4['cur_2']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsQ1['cur'] / 100000000) / 10) + '<br><font color="#' + ((summsQ1['cur'] < summsQ1['cur_1']) ? 'c000' : '008') + '000">' + String(((summsQ1['cur_1'] > 0) ? Math.floor(((summsQ1['cur'] - summsQ1['cur_1']) / summsQ1['cur_1']) * 1000) / 10 : 100.0)) + '%</font>',
                    ];
                    window.Legends[0].unshift(String(Math.floor(summsYTD['cur'] / 100000000) / 10) + '<br><font color="#' + ((summsYTD['cur'] < summsYTD['cur_1']) ? 'c000' : '008') + '000">' + String(((summsYTD['cur_1'] > 0) ? Math.floor(((summsYTD['cur'] - summsYTD['cur_1']) / summsYTD['cur_1']) * 1000) / 10 : 100.0)) + '%</font>');
                    window.Legends[0].unshift(String(Math.floor(summsYTD['cur_1'] / 100000000) / 10) + '<br><font color="#' + ((summsYTD['cur_1'] < summsYTD['cur_2']) ? 'c000' : '008') + '000">' + String(((summsYTD['cur_2'] > 0) ? Math.floor(((summsYTD['cur_1'] - summsYTD['cur_2']) / summsYTD['cur_2']) * 1000) / 10 : 100.0)) + '%</font>');
                    window.Legends[0].unshift(String(Math.floor(summsMTM['cur'] / 100000000) / 10) + '<br><font color="#' + ((summsMTM['cur'] < summsMTM['cur_1']) ? 'c000' : '008') + '000">' + String(((summsMTM['cur_1'] > 0) ? Math.floor(((summsMTM['cur'] - summsMTM['cur_1']) / summsMTM['cur_1']) * 1000) / 10 : 100.0)) + '%</font>');
                    window.Legends[0].unshift(String(Math.floor(summsMTM['cur_1'] / 100000000) / 10) + '<br><font color="#' + ((summsMTM['cur_1'] < summsMTM['cur_2']) ? 'c000' : '008') + '000">' + String(((summsMTM['cur_2'] > 0) ? Math.floor(((summsMTM['cur_1'] - summsMTM['cur_2']) / summsMTM['cur_2']) * 1000) / 10 : 100.0)) + '%</font>');
                    window.Legends[2] = [
                        'Bln.RUB<br>+/- % SPLY',
                        String(Math.floor(summsQ1['curi_1'] / 100000000) / 10) + '<br><font color="#' + ((summsQ1['curi_1'] < summsQ1['curi_2']) ? 'c000' : '008') + '000">' + String(((summsQ1['curi_2'] > 0) ? Math.floor(((summsQ1['curi_1'] - summsQ1['curi_2']) / summsQ1['curi_2']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsQ2['curi_1'] / 100000000) / 10) + '<br><font color="#' + ((summsQ2['curi_1'] < summsQ2['curi_2']) ? 'c000' : '008') + '000">' + String(((summsQ2['curi_2'] > 0) ? Math.floor(((summsQ2['curi_1'] - summsQ2['curi_2']) / summsQ2['curi_2']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsQ3['curi_1'] / 100000000) / 10) + '<br><font color="#' + ((summsQ3['curi_1'] < summsQ3['curi_2']) ? 'c000' : '008') + '000">' + String(((summsQ3['curi_2'] > 0) ? Math.floor(((summsQ3['curi_1'] - summsQ3['curi_2']) / summsQ3['curi_2']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsQ4['curi_1'] / 100000000) / 10) + '<br><font color="#' + ((summsQ4['curi_1'] < summsQ4['curi_2']) ? 'c000' : '008') + '000">' + String(((summsQ4['curi_2'] > 0) ? Math.floor(((summsQ4['curi_1'] - summsQ4['curi_2']) / summsQ4['curi_2']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsQ1['curi'] / 100000000) / 10) + '<br><font color="#' + ((summsQ1['curi'] < summsQ1['curi_1']) ? 'c000' : '008') + '000">' + String(((summsQ1['curi_1'] > 0) ? Math.floor(((summsQ1['curi'] - summsQ1['curi_1']) / summsQ1['curi_1']) * 1000) / 10 : 100.0)) + '%</font>',
                    ];
                    window.Legends[2].unshift(String(Math.floor(summsYTD['curi'] / 100000000) / 10) + '<br><font color="#' + ((summsYTD['curi'] < summsYTD['curi_1']) ? 'c000' : '008') + '000">' + String(((summsYTD['curi_1'] > 0) ? Math.floor(((summsYTD['curi'] - summsYTD['curi_1']) / summsYTD['curi_1']) * 1000) / 10 : 100.0)) + '%</font>');
                    window.Legends[2].unshift(String(Math.floor(summsYTD['curi_1'] / 100000000) / 10) + '<br><font color="#' + ((summsYTD['curi_1'] < summsYTD['curi_2']) ? 'c000' : '008') + '000">' + String(((summsYTD['curi_2'] > 0) ? Math.floor(((summsYTD['curi_1'] - summsYTD['curi_2']) / summsYTD['curi_2']) * 1000) / 10 : 100.0)) + '%</font>');
                    window.Legends[2].unshift(String(Math.floor(summsMTM['curi'] / 100000000) / 10) + '<br><font color="#' + ((summsMTM['curi'] < summsMTM['curi_1']) ? 'c000' : '008') + '000">' + String(((summsMTM['curi_1'] > 0) ? Math.floor(((summsMTM['curi'] - summsMTM['curi_1']) / summsMTM['curi_1']) * 1000) / 10 : 100.0)) + '%</font>');
                    window.Legends[2].unshift(String(Math.floor(summsMTM['curi_1'] / 100000000) / 10) + '<br><font color="#' + ((summsMTM['curi_1'] < summsMTM['curi_2']) ? 'c000' : '008') + '000">' + String(((summsMTM['curi_2'] > 0) ? Math.floor(((summsMTM['curi_1'] - summsMTM['curi_2']) / summsMTM['curi_2']) * 1000) / 10 : 100.0)) + '%</font>');
                    window.Legends[4] = [
                        'Bln.RUB<br>+/- % SPLY',
                        String(Math.floor(summsbrands5[window.MarkCodes[0]]['q1_1'] / 100000000) / 10) + '<br><font color="#' + ((summsbrands5[window.MarkCodes[0]]['q1_1'] < summsbrands5[window.MarkCodes[0]]['q1_2']) ? 'c000' : '008') + '000">' + String(((summsbrands5[window.MarkCodes[0]]['q1_2'] > 0) ? Math.floor(((summsbrands5[window.MarkCodes[0]]['q1_1'] - summsbrands5[window.MarkCodes[0]]['q1_2']) / summsbrands5[window.MarkCodes[0]]['q1_2']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsbrands5[window.MarkCodes[0]]['q2_1'] / 100000000) / 10) + '<br><font color="#' + ((summsbrands5[window.MarkCodes[0]]['q2_1'] < summsbrands5[window.MarkCodes[0]]['q2_2']) ? 'c000' : '008') + '000">' + String(((summsbrands5[window.MarkCodes[0]]['q2_2'] > 0) ? Math.floor(((summsbrands5[window.MarkCodes[0]]['q2_1'] - summsbrands5[window.MarkCodes[0]]['q2_2']) / summsbrands5[window.MarkCodes[0]]['q2_2']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsbrands5[window.MarkCodes[0]]['q3_1'] / 100000000) / 10) + '<br><font color="#' + ((summsbrands5[window.MarkCodes[0]]['q3_1'] < summsbrands5[window.MarkCodes[0]]['q3_2']) ? 'c000' : '008') + '000">' + String(((summsbrands5[window.MarkCodes[0]]['q3_2'] > 0) ? Math.floor(((summsbrands5[window.MarkCodes[0]]['q3_1'] - summsbrands5[window.MarkCodes[0]]['q3_2']) / summsbrands5[window.MarkCodes[0]]['q3_2']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsbrands5[window.MarkCodes[0]]['q4_1'] / 100000000) / 10) + '<br><font color="#' + ((summsbrands5[window.MarkCodes[0]]['q4_1'] < summsbrands5[window.MarkCodes[0]]['q4_2']) ? 'c000' : '008') + '000">' + String(((summsbrands5[window.MarkCodes[0]]['q4_2'] > 0) ? Math.floor(((summsbrands5[window.MarkCodes[0]]['q4_1'] - summsbrands5[window.MarkCodes[0]]['q4_2']) / summsbrands5[window.MarkCodes[0]]['q4_2']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsbrands5[window.MarkCodes[0]]['q1'] / 100000000) / 10) + '<br><font color="#' + ((summsbrands5[window.MarkCodes[0]]['q1'] < summsbrands5[window.MarkCodes[0]]['q1_1']) ? 'c000' : '008') + '000">' + String(((summsbrands5[window.MarkCodes[0]]['q1_1'] > 0) ? Math.floor(((summsbrands5[window.MarkCodes[0]]['q1'] - summsbrands5[window.MarkCodes[0]]['q1_1']) / summsbrands5[window.MarkCodes[0]]['q1_1']) * 1000) / 10 : 100.0)) + '%</font>',
                    ];
                    window.Legends[4].unshift(String(Math.floor(summsbrands5[window.MarkCodes[0]]['ytd'] / 100000000) / 10) + '<br><font color="#' + ((summsbrands5[window.MarkCodes[0]]['ytd'] < summsbrands5[window.MarkCodes[0]]['ytd1']) ? 'c000' : '008') + '000">' + String(((summsbrands5[window.MarkCodes[0]]['ytd1'] > 0) ? Math.floor(((summsbrands5[window.MarkCodes[0]]['ytd'] - summsbrands5[window.MarkCodes[0]]['ytd1']) / summsbrands5[window.MarkCodes[0]]['ytd1']) * 1000) / 10 : 100.0)) + '%</font>');
                    window.Legends[4].unshift(String(Math.floor(summsbrands5[window.MarkCodes[0]]['ytd1'] / 100000000) / 10) + '<br><font color="#' + ((summsbrands5[window.MarkCodes[0]]['ytd1'] < summsbrands5[window.MarkCodes[0]]['ytd2']) ? 'c000' : '008') + '000">' + String(((summsbrands5[window.MarkCodes[0]]['ytd2'] > 0) ? Math.floor(((summsbrands5[window.MarkCodes[0]]['ytd1'] - summsbrands5[window.MarkCodes[0]]['ytd2']) / summsbrands5[window.MarkCodes[0]]['ytd2']) * 1000) / 10 : 100.0)) + '%</font>');
                    window.Legends[4].unshift(String(Math.floor(summsbrands5[window.MarkCodes[0]]['mtm'] / 100000000) / 10) + '<br><font color="#' + ((summsbrands5[window.MarkCodes[0]]['mtm'] < summsbrands5[window.MarkCodes[0]]['mtm1']) ? 'c000' : '008') + '000">' + String(((summsbrands5[window.MarkCodes[0]]['mtm1'] > 0) ? Math.floor(((summsbrands5[window.MarkCodes[0]]['mtm'] - summsbrands5[window.MarkCodes[0]]['mtm1']) / summsbrands5[window.MarkCodes[0]]['mtm1']) * 1000) / 10 : 100.0)) + '%</font>');
                    window.Legends[4].unshift(String(Math.floor(summsbrands5[window.MarkCodes[0]]['mtm1'] / 100000000) / 10) + '<br><font color="#' + ((summsbrands5[window.MarkCodes[0]]['mtm1'] < summsbrands5[window.MarkCodes[0]]['mtm2']) ? 'c000' : '008') + '000">' + String(((summsbrands5[window.MarkCodes[0]]['mtm2'] > 0) ? Math.floor(((summsbrands5[window.MarkCodes[0]]['mtm1'] - summsbrands5[window.MarkCodes[0]]['mtm2']) / summsbrands5[window.MarkCodes[0]]['mtm2']) * 1000) / 10 : 100.0)) + '%</font>');

                    if (window.Month > '03') {
                        window.ChartData4[0].labels.push(String(window.Year) + ' Q2');
                        window.ChartData4[1].labels.push(String(window.Year) + ' Q2');
                        window.ChartData4[2].labels.push(String(window.Year) + ' Q2');
                        window.ChartData4[3].labels.push(String(window.Year) + ' Q2');
                        window.ChartData4[4].labels.push(String(window.Year) + ' Q2');
                        window.Legends[0].push(String(Math.floor(summsQ2['cur'] / 100000000) / 10) + '<br><font color="#' + ((summsQ2['cur'] < summsQ2['cur_1']) ? 'c000' : '008') + '000">' + String(((summsQ2['cur_1'] > 0) ? Math.floor(((summsQ2['cur'] - summsQ2['cur_1']) / summsQ2['cur_1']) * 1000) / 10 : 100.0)) + '%</font>');
                        window.Legends[2].push(String(Math.floor(summsQ2['curi'] / 100000000) / 10) + '<br><font color="#' + ((summsQ2['curi'] < summsQ2['curi_1']) ? 'c000' : '008') + '000">' + String(((summsQ2['curi_1'] > 0) ? Math.floor(((summsQ2['curi'] - summsQ2['curi_1']) / summsQ2['curi_1']) * 1000) / 10 : 100.0)) + '%</font>');
                        window.Legends[4].push(String(Math.floor(summsbrands5[window.MarkCodes[0]]['q2'] / 100000000) / 10) + '<br><font color="#' + ((summsbrands5[window.MarkCodes[0]]['q2'] < summsbrands5[window.MarkCodes[0]]['q2_1']) ? 'c000' : '008') + '000">' + String(((summsbrands5[window.MarkCodes[0]]['q2_1'] > 0) ? Math.floor(((summsbrands5[window.MarkCodes[0]]['q2'] - summsbrands5[window.MarkCodes[0]]['q2_1']) / summsbrands5[window.MarkCodes[0]]['q2_1']) * 1000) / 10 : 100.0)) + '%</font>');
                    };
                    if (window.Month > '06') {
                        window.ChartData4[0].labels.push(String(window.Year) + ' Q3');
                        window.ChartData4[1].labels.push(String(window.Year) + ' Q3');
                        window.ChartData4[2].labels.push(String(window.Year) + ' Q3');
                        window.ChartData4[3].labels.push(String(window.Year) + ' Q3');
                        window.ChartData4[4].labels.push(String(window.Year) + ' Q3');
                        window.Legends[0].push(String(Math.floor(summsQ3['cur'] / 100000000) / 10) + '<br><font color="#' + ((summsQ3['cur'] < summsQ3['cur_1']) ? 'c000' : '008') + '000">' + String(((summsQ3['cur_1'] > 0) ? Math.floor(((summsQ3['cur'] - summsQ3['cur_1']) / summsQ3['cur_1']) * 1000) / 10 : 100.0)) + '%</font>');
                        window.Legends[2].push(String(Math.floor(summsQ3['curi'] / 100000000) / 10) + '<br><font color="#' + ((summsQ3['curi'] < summsQ3['curi_1']) ? 'c000' : '008') + '000">' + String(((summsQ3['curi_1'] > 0) ? Math.floor(((summsQ3['curi'] - summsQ3['curi_1']) / summsQ3['curi_1']) * 1000) / 10 : 100.0)) + '%</font>');
                        window.Legends[4].push(String(Math.floor(summsbrands5[window.MarkCodes[0]]['q3'] / 100000000) / 10) + '<br><font color="#' + ((summsbrands5[window.MarkCodes[0]]['q3'] < summsbrands5[window.MarkCodes[0]]['q3_1']) ? 'c000' : '008') + '000">' + String(((summsbrands5[window.MarkCodes[0]]['q3_1'] > 0) ? Math.floor(((summsbrands5[window.MarkCodes[0]]['q3'] - summsbrands5[window.MarkCodes[0]]['q3_1']) / summsbrands5[window.MarkCodes[0]]['q3_1']) * 1000) / 10 : 100.0)) + '%</font>');
                    };
                    if (window.Month > '09') {
                        window.ChartData4[0].labels.push(String(window.Year) + ' Q4');
                        window.ChartData4[1].labels.push(String(window.Year) + ' Q4');
                        window.ChartData4[2].labels.push(String(window.Year) + ' Q4');
                        window.ChartData4[3].labels.push(String(window.Year) + ' Q4');
                        window.ChartData4[4].labels.push(String(window.Year) + ' Q4');
                        window.Legends[0].push(String(Math.floor(summsQ4['cur'] / 100000000) / 10) + '<br><font color="#' + ((summsQ4['cur'] < summsQ4['cur_1']) ? 'c000' : '008') + '000">' + String(((summsQ4['cur_1'] > 0) ? Math.floor(((summsQ4['cur'] - summsQ4['cur_1']) / summsQ4['cur_1']) * 1000) / 10 : 100.0)) + '%</font>');
                        window.Legends[2].push(String(Math.floor(summsQ4['curi'] / 100000000) / 10) + '<br><font color="#' + ((summsQ4['curi'] < summsQ4['curi_1']) ? 'c000' : '008') + '000">' + String(((summsQ4['curi_1'] > 0) ? Math.floor(((summsQ4['curi'] - summsQ4['curi_1']) / summsQ4['curi_1']) * 1000) / 10 : 100.0)) + '%</font>');
                        window.Legends[4].push(String(Math.floor(summsbrands5[window.MarkCodes[0]]['q4'] / 100000000) / 10) + '<br><font color="#' + ((summsbrands5[window.MarkCodes[0]]['q4'] < summsbrands5[window.MarkCodes[0]]['q4_1']) ? 'c000' : '008') + '000">' + String(((summsbrands5[window.MarkCodes[0]]['q4_1'] > 0) ? Math.floor(((summsbrands5[window.MarkCodes[0]]['q4'] - summsbrands5[window.MarkCodes[0]]['q4_1']) / summsbrands5[window.MarkCodes[0]]['q4_1']) * 1000) / 10 : 100.0)) + '%</font>');
                    };
                    window.Legends[1] = window.Legends[0];
                    window.Legends[3] = window.Legends[2];
                } else {
                    let selectedDate = document.getElementById("datefield").value.split('-');
                    window.Year = selectedDate[0];
                    window.Month = selectedDate[1];
                    window.Day = selectedDate[2];
                    if (window.Month === '12') {
                        window.Year1 = window.Year;
                        window.Year2 = window.Year3 = window.Year - 1;
                        window.Month1 = '01';
                    } else {
                        window.Year1 = window.Year2 = window.Year - 1;
                        window.Year3 = window.Year - 2;
                        window.Month1 = Number(window.Month) + 1;
                        if (window.Month1 < 10) {
                            window.Month1 = '0' + String(window.Month1);
                        }
                    };
                    window.CurDate = Number(String(window.Year) + window.Month + window.Day);
                    window.CurDate1 = Number(String(window.Year - 1) + window.Month + window.Day);
                    window.CurDate2 = Number(String(window.Year - 2) + window.Month + window.Day);
                    window.CurDate3 = Number(String(window.Year - 3) + window.Month + window.Day);
                    window.CurDate4 = Number(String(window.Year - 4) + window.Month + window.Day);
                    window.YearDate = Number(String(window.Year) + '0101');
                    window.YearDate1 = Number(String(window.Year - 1) + '0101');
                    window.YearDate2 = Number(String(window.Year - 2) + '0101');
                    window.YearDate3 = Number(String(window.Year - 3) + '0101');
                    window.YearDate4 = Number(String(window.Year - 4) + '0101');
                    window.CurMonth = Number(String(window.Year) + window.Month);
                    window.CurMonth1 = Number(String(window.Year - 1) + window.Month);
                    window.CurMonth2 = Number(String(window.Year - 2) + window.Month);
                    window.CurMonth3 = Number(String(window.Year - 3) + window.Month);
                    window.CurMonth4 = Number(String(window.Year - 4) + window.Month);
                    let brands = [];
                    let brandsInd = [];
                    let segments = [];
                    let summsall = 0;
                    let summsallInd = 0;
                    let summsallIndbrands = [];
                    let summsbrands = {};
                    let summsbrands_t = {};
                    let summsbrands5 = {};
                    let summssegments = {};
                    let summssegments_t = {};
                    summsbrands['Direct'] = {
                        'all': 0,
                        'mtm': 0,
                        'mtm1': 0,
                        'mtm2': 0,
                        'mtm3': 0,
                        'ytd': 0,
                        'ytd1': 0,
                        'ytd2': 0,
                        'ytd3': 0,
                        'q0': 0,
                        'q1': 0,
                        'q2': 0,
                        'q3': 0,
                        'q4': 0,
                        'q5': 0,
                        'q6': 0,
                        'q7': 0,
                        'q8': 0,
                        'q9': 0,
                        'q10': 0,
                        'q11': 0,
                        'q12': 0,
                        'q13': 0,
                    };
                    summsbrands['Indirect'] = {
                        'all': 0,
                        'mtm': 0,
                        'mtm1': 0,
                        'mtm2': 0,
                        'mtm3': 0,
                        'ytd': 0,
                        'ytd1': 0,
                        'ytd2': 0,
                        'ytd3': 0,
                        'q0': 0,
                        'q1': 0,
                        'q2': 0,
                        'q3': 0,
                        'q4': 0,
                        'q5': 0,
                        'q6': 0,
                        'q7': 0,
                        'q8': 0,
                        'q9': 0,
                        'q10': 0,
                        'q11': 0,
                        'q12': 0,
                        'q13': 0,
                    };
                    summsbrands['Other'] = {
                        'all': 0,
                        'mtm': 0,
                        'mtm1': 0,
                        'mtm2': 0,
                        'mtm3': 0,
                        'ytd': 0,
                        'ytd1': 0,
                        'ytd2': 0,
                        'ytd3': 0,
                        'q0': 0,
                        'q1': 0,
                        'q2': 0,
                        'q3': 0,
                        'q4': 0,
                        'q5': 0,
                        'q6': 0,
                        'q7': 0,
                        'q8': 0,
                        'q9': 0,
                        'q10': 0,
                        'q11': 0,
                        'q12': 0,
                        'q13': 0,
                        'q14': 0,
                        'q15': 0,
                        'q16': 0,
                        'q17': 0,
                        'q18': 0,
                        'q19': 0,
                        'q20': 0,
                        'q21': 0,
                        'q22': 0,
                        'q23': 0,
                        'q24': 0,
                        'q25': 0,
                    };
                    summsbrands['OtherIndirect'] = {
                        'all': 0,
                        'mtm': 0,
                        'mtm1': 0,
                        'mtm2': 0,
                        'mtm3': 0,
                        'ytd': 0,
                        'ytd1': 0,
                        'ytd2': 0,
                        'ytd3': 0,
                        'q0': 0,
                        'q1': 0,
                        'q2': 0,
                        'q3': 0,
                        'q4': 0,
                        'q5': 0,
                        'q6': 0,
                        'q7': 0,
                        'q8': 0,
                        'q9': 0,
                        'q10': 0,
                        'q11': 0,
                        'q12': 0,
                        'q13': 0,
                        'q14': 0,
                        'q15': 0,
                        'q16': 0,
                        'q17': 0,
                        'q18': 0,
                        'q19': 0,
                        'q20': 0,
                        'q21': 0,
                        'q22': 0,
                        'q23': 0,
                        'q24': 0,
                        'q25': 0,
                    };
                    let summsMTM = {
                        'cur': 0,
                        'cur_1': 0,
                        'cur_2': 0,
                        'cur_3': 0,
                        'curi': 0,
                        'curi_1': 0,
                        'curi_2': 0,
                        'curi_3': 0,
                    };
                    let summsYTD = {
                        'cur': 0,
                        'cur_1': 0,
                        'cur_2': 0,
                        'cur_3': 0,
                        'curi': 0,
                        'curi_1': 0,
                        'curi_2': 0,
                        'curi_3': 0,
                    };
                    let summsM = [];
                    for (let i = 0; i < 26; i++) {
                        summsM[i] = {
                            'cur': 0,
                            'curi': 0,
                        };
                    };
                    for (let i = 0; i < xlsxData.length; i++) {
                        let vDates = xlsxData[i]['Дата окончания процедуры'].split('/');
                        for (let j = 0; j < vDates.length; j++) {
                            if (vDates[j] < 10) {
                                vDates[j] = '0' + String(vDates[j]);
                            };
                        };
                        vDates[2] = '20' + vDates[2];
                        let vDate = Number(vDates[2] + vDates[0] + vDates[1]);
                        let vMonth = Number(vDates[2] + vDates[0]);
                        if (xlsxData[i]['Business Units'] === window.BUCodes[document.getElementById("buselector").selectedIndex]) {
                            xlsxData[i]['Сумма Кон. Продаж в Нац. валюта'] = xlsxData[i]['Сумма Кон. Продаж в Нац. валюта'].replace(/,/g, '');
                            if ((!(xlsxData[i]['Product Segment 1'])) || (xlsxData[i]['Product Segment 1'] === '')) {
                                xlsxData[i]['Product Segment 1'] = 'null';
                            };
                            let f = true;
                            for (let j = 0; j < brands.length; j++) {
                                if ((xlsxData[i]['Корпорация']) === brands[j]) {
                                    f = false;
                                    break;
                                }
                            };
                            if (f) {
                                brands.push(xlsxData[i]['Корпорация']);
                                summsallIndbrands[xlsxData[i]['Корпорация']] = 0;
                                summsbrands[xlsxData[i]['Корпорация']] = {
                                    'all': 0,
                                    'mtm': 0,
                                    'mtm1': 0,
                                    'mtm2': 0,
                                    'mtm3': 0,
                                    'ytd': 0,
                                    'ytd1': 0,
                                    'ytd2': 0,
                                    'ytd3': 0,
                                    'q0': 0,
                                    'q1': 0,
                                    'q2': 0,
                                    'q3': 0,
                                    'q4': 0,
                                    'q5': 0,
                                    'q6': 0,
                                    'q7': 0,
                                    'q8': 0,
                                    'q9': 0,
                                    'q10': 0,
                                    'q11': 0,
                                    'q12': 0,
                                    'q13': 0,
                                };
                                summsbrands[xlsxData[i]['Корпорация'] + 'Indirect'] = {
                                    'all': 0,
                                    'mtm': 0,
                                    'mtm1': 0,
                                    'mtm2': 0,
                                    'mtm3': 0,
                                    'ytd': 0,
                                    'ytd1': 0,
                                    'ytd2': 0,
                                    'ytd3': 0,
                                    'q0': 0,
                                    'q1': 0,
                                    'q2': 0,
                                    'q3': 0,
                                    'q4': 0,
                                    'q5': 0,
                                    'q6': 0,
                                    'q7': 0,
                                    'q8': 0,
                                    'q9': 0,
                                    'q10': 0,
                                    'q11': 0,
                                    'q12': 0,
                                    'q13': 0,
                                    'q14': 0,
                                    'q15': 0,
                                    'q16': 0,
                                    'q17': 0,
                                    'q18': 0,
                                    'q19': 0,
                                    'q20': 0,
                                    'q21': 0,
                                    'q22': 0,
                                    'q23': 0,
                                    'q24': 0,
                                    'q25': 0,
                                };
                                summsbrands[xlsxData[i]['Корпорация'] + 'Direct'] = {
                                    'all': 0,
                                    'mtm': 0,
                                    'mtm1': 0,
                                    'mtm2': 0,
                                    'mtm3': 0,
                                    'ytd': 0,
                                    'ytd1': 0,
                                    'ytd2': 0,
                                    'ytd3': 0,
                                    'q0': 0,
                                    'q1': 0,
                                    'q2': 0,
                                    'q3': 0,
                                    'q4': 0,
                                    'q5': 0,
                                    'q6': 0,
                                    'q7': 0,
                                    'q8': 0,
                                    'q9': 0,
                                    'q10': 0,
                                    'q11': 0,
                                    'q12': 0,
                                    'q13': 0,
                                    'q14': 0,
                                    'q15': 0,
                                    'q16': 0,
                                    'q17': 0,
                                    'q18': 0,
                                    'q19': 0,
                                    'q20': 0,
                                    'q21': 0,
                                    'q22': 0,
                                    'q23': 0,
                                    'q24': 0,
                                    'q25': 0,
                                };
                            }
                            if (xlsxData[i]['Прямая закупка'] === 'Indirect') {
                                f = true;
                                for (let j = 0; j < brandsInd.length; j++) {
                                    if ((xlsxData[i]['Корпорация']) === brandsInd[j]) {
                                        f = false;
                                        break;
                                    }
                                };
                                if (f) {
                                    brandsInd.push(xlsxData[i]['Корпорация']);
                                    for (let j = 0; j < segments.length; j++) {
                                        summssegments[segments[j] + xlsxData[i]['Корпорация']] = {
                                            'all': 0,
                                            'mtm': 0,
                                            'mtm1': 0,
                                            'mtm2': 0,
                                            'mtm3': 0,
                                            'ytd': 0,
                                            'ytd1': 0,
                                            'ytd2': 0,
                                            'ytd3': 0,
                                            'q0': 0,
                                            'q1': 0,
                                            'q2': 0,
                                            'q3': 0,
                                            'q4': 0,
                                            'q5': 0,
                                            'q6': 0,
                                            'q7': 0,
                                            'q8': 0,
                                            'q9': 0,
                                            'q10': 0,
                                            'q11': 0,
                                            'q12': 0,
                                            'q13': 0,
                                        };
                                    };
                                };
                                f = true;
                                for (let j = 0; j < segments.length; j++) {
                                    if ((xlsxData[i]['Product Segment 1']) === segments[j]) {
                                        f = false;
                                        break;
                                    }
                                };
                                if (f) {
                                    segments.push(xlsxData[i]['Product Segment 1']);
                                    summssegments[xlsxData[i]['Product Segment 1']] = {
                                        'all': 0,
                                        'mtm': 0,
                                        'mtm1': 0,
                                        'mtm2': 0,
                                        'mtm3': 0,
                                        'ytd': 0,
                                        'ytd1': 0,
                                        'ytd2': 0,
                                        'ytd3': 0,
                                        'q0': 0,
                                        'q1': 0,
                                        'q2': 0,
                                        'q3': 0,
                                        'q4': 0,
                                        'q5': 0,
                                        'q6': 0,
                                        'q7': 0,
                                        'q8': 0,
                                        'q9': 0,
                                        'q10': 0,
                                        'q11': 0,
                                        'q12': 0,
                                        'q13': 0,
                                    };
                                    for (let j = 0; j < brandsInd.length; j++) {
                                        summssegments[xlsxData[i]['Product Segment 1'] + brandsInd[j]] = {
                                            'all': 0,
                                            'mtm': 0,
                                            'mtm1': 0,
                                            'mtm2': 0,
                                            'mtm3': 0,
                                            'ytd': 0,
                                            'ytd1': 0,
                                            'ytd2': 0,
                                            'ytd3': 0,
                                            'q0': 0,
                                            'q1': 0,
                                            'q2': 0,
                                            'q3': 0,
                                            'q4': 0,
                                            'q5': 0,
                                            'q6': 0,
                                            'q7': 0,
                                            'q8': 0,
                                            'q9': 0,
                                            'q10': 0,
                                            'q11': 0,
                                            'q12': 0,
                                            'q13': 0,
                                        };
                                    };
                                };
                            }
                            if (vMonth > window.CurMonth) {
                                continue;
                            } else if (vMonth > window.CurMonth1) {
                                summsMTM['cur'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                if (xlsxData[i]['Прямая закупка'] === 'Indirect') {
                                    summsMTM['curi'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summssegments[xlsxData[i]['Product Segment 1']]['mtm'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summssegments[xlsxData[i]['Product Segment 1'] + xlsxData[i]['Корпорация']]['mtm'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                }
                                summsbrands[xlsxData[i]['Корпорация']]['mtm'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                summsbrands[xlsxData[i]['Корпорация'] + xlsxData[i]['Прямая закупка']]['mtm'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                summsbrands[xlsxData[i]['Прямая закупка']]['mtm'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                            } else if (vMonth > window.CurMonth2) {
                                summsMTM['cur_1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                if (xlsxData[i]['Прямая закупка'] === 'Indirect') {
                                    summsMTM['curi_1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summssegments[xlsxData[i]['Product Segment 1']]['mtm1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summssegments[xlsxData[i]['Product Segment 1'] + xlsxData[i]['Корпорация']]['mtm1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                }
                                summsbrands[xlsxData[i]['Корпорация']]['mtm1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                summsbrands[xlsxData[i]['Корпорация'] + xlsxData[i]['Прямая закупка']]['mtm1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                summsbrands[xlsxData[i]['Прямая закупка']]['mtm1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                            } else if (vMonth > window.CurMonth3) {
                                summsMTM['cur_2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                if (xlsxData[i]['Прямая закупка'] === 'Indirect') {
                                    summsMTM['curi_2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summssegments[xlsxData[i]['Product Segment 1']]['mtm2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summssegments[xlsxData[i]['Product Segment 1'] + xlsxData[i]['Корпорация']]['mtm2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                }
                                summsbrands[xlsxData[i]['Корпорация']]['mtm2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                summsbrands[xlsxData[i]['Корпорация'] + xlsxData[i]['Прямая закупка']]['mtm2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                summsbrands[xlsxData[i]['Прямая закупка']]['mtm2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                            } else if (vMonth > window.CurMonth4) {
                                summsMTM['cur_3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                if (xlsxData[i]['Прямая закупка'] === 'Indirect') {
                                    summsMTM['curi_3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summssegments[xlsxData[i]['Product Segment 1']]['mtm3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summssegments[xlsxData[i]['Product Segment 1'] + xlsxData[i]['Корпорация']]['mtm3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                }
                                summsbrands[xlsxData[i]['Корпорация']]['mtm3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                summsbrands[xlsxData[i]['Корпорация'] + xlsxData[i]['Прямая закупка']]['mtm3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                summsbrands[xlsxData[i]['Прямая закупка']]['mtm3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                            } else {
                                continue;
                            };
                            if ((vDate >= window.YearDate) && (vDate <= window.CurDate)) {
                                summsYTD['cur'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                if (xlsxData[i]['Прямая закупка'] === 'Indirect') {
                                    summsYTD['curi'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summssegments[xlsxData[i]['Product Segment 1']]['ytd'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summssegments[xlsxData[i]['Product Segment 1'] + xlsxData[i]['Корпорация']]['ytd'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                }
                                summsbrands[xlsxData[i]['Корпорация']]['ytd'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                summsbrands[xlsxData[i]['Корпорация'] + xlsxData[i]['Прямая закупка']]['ytd'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                summsbrands[xlsxData[i]['Прямая закупка']]['ytd'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                            } else if ((vDate >= window.YearDate1) && (vDate <= window.CurDate1)) {
                                summsYTD['cur_1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                if (xlsxData[i]['Прямая закупка'] === 'Indirect') {
                                    summsYTD['curi_1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summssegments[xlsxData[i]['Product Segment 1']]['ytd1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summssegments[xlsxData[i]['Product Segment 1'] + xlsxData[i]['Корпорация']]['ytd1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                }
                                summsbrands[xlsxData[i]['Корпорация']]['ytd1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                summsbrands[xlsxData[i]['Корпорация'] + xlsxData[i]['Прямая закупка']]['ytd1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                summsbrands[xlsxData[i]['Прямая закупка']]['ytd1'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                            } else if ((vDate >= window.YearDate2) && (vDate <= window.CurDate2)) {
                                summsYTD['cur_2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                if (xlsxData[i]['Прямая закупка'] === 'Indirect') {
                                    summsYTD['curi_2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summssegments[xlsxData[i]['Product Segment 1']]['ytd2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summssegments[xlsxData[i]['Product Segment 1'] + xlsxData[i]['Корпорация']]['ytd2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                }
                                summsbrands[xlsxData[i]['Корпорация']]['ytd2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                summsbrands[xlsxData[i]['Корпорация'] + xlsxData[i]['Прямая закупка']]['ytd2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                summsbrands[xlsxData[i]['Прямая закупка']]['ytd2'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                            } else if ((vDate >= window.YearDate3) && (vDate <= window.CurDate3)) {
                                summsYTD['cur_3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                if (xlsxData[i]['Прямая закупка'] === 'Indirect') {
                                    summsYTD['curi_3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summssegments[xlsxData[i]['Product Segment 1']]['ytd3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summssegments[xlsxData[i]['Product Segment 1'] + xlsxData[i]['Корпорация']]['ytd3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                }
                                summsbrands[xlsxData[i]['Корпорация']]['ytd3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                summsbrands[xlsxData[i]['Корпорация'] + xlsxData[i]['Прямая закупка']]['ytd3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                summsbrands[xlsxData[i]['Прямая закупка']]['ytd3'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                            };
                            if (vMonth > 202001) {
                                //    debugger;
                            }
                            let monthIndex = Number(window.Month) - Number(vDates[0]) + 12 * (Number(window.Year) - Number(vDates[2]));
                            if ((monthIndex > -1) && (monthIndex < 26)) {
                                summsM[monthIndex]['cur'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                if (xlsxData[i]['Прямая закупка'] === 'Indirect') {
                                    summsM[monthIndex]['curi'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    if (monthIndex < 14) {
                                        summssegments[xlsxData[i]['Product Segment 1']]['q' + String(monthIndex)] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                        summssegments[xlsxData[i]['Product Segment 1'] + xlsxData[i]['Корпорация']]['q' + String(monthIndex)] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    }
                                }
                                if (monthIndex < 14) {
                                    summsbrands[xlsxData[i]['Корпорация']]['q' + String(monthIndex)] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                    summsbrands[xlsxData[i]['Прямая закупка']]['q' + String(monthIndex)] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                }
                                summsbrands[xlsxData[i]['Корпорация'] + xlsxData[i]['Прямая закупка']]['q' + String(monthIndex)] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                            }
                            summsbrands[xlsxData[i]['Корпорация']]['all'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                            summsbrands[xlsxData[i]['Корпорация'] + xlsxData[i]['Прямая закупка']]['all'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                            summsbrands[xlsxData[i]['Прямая закупка']]['all'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                            summsall += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                            if (xlsxData[i]['Прямая закупка'] === 'Indirect') {
                                summssegments[xlsxData[i]['Product Segment 1']]['all'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                summsallInd += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                summssegments[xlsxData[i]['Product Segment 1'] + xlsxData[i]['Корпорация']]['all'] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                                summsallIndbrands[xlsxData[i]['Корпорация']] += Number(xlsxData[i]['Сумма Кон. Продаж в Нац. валюта']);
                            }
                        };
                    };
                    if (brands.length < 1) {
                        alert('Нет информации по выбранному BU коду за выбранные диапазоны дат!');
                        window.MarkCodes = ['САМСУНГ МЕДИСОН КО.; ЛТД.', ];
                        window.MarkNames = ['SAMSUNG', ];
                        window.AddMarkSelectsOptions();
                        document.getElementsByClassName('FifthPageMark')[0].innerHTML = window.MarkNames[0] + ': Indirect Channel';
                        return;
                    };
                    let maxbrands = [brands[0], ];
                    let maxsumms = [summsbrands[brands[0]]['all'], ];
                    for (let i = 1; i < brands.length; i++) {
                        for (let j = 0; j < maxbrands.length; j++) {
                            if (summsbrands[brands[i]]['all'] > maxsumms[j]) {
                                for (let k = maxbrands.length; k > j; k--) {
                                    maxsumms[k] = maxsumms[k - 1];
                                    maxbrands[k] = maxbrands[k - 1];
                                };
                                maxsumms[j] = summsbrands[brands[i]]['all'];
                                maxbrands[j] = brands[i];
                                break;
                            } else if (j == (maxbrands.length - 1)) {
                                maxsumms[j + 1] = summsbrands[brands[i]]['all'];
                                maxbrands[j + 1] = brands[i];
                                break;
                            };
                        };
                    };
                    let maxbrandsInd = [brandsInd[0], ];
                    let maxsummsInd = [summsbrands[brandsInd[0] + 'Indirect']['all'], ];
                    for (let i = 1; i < brandsInd.length; i++) {
                        for (let j = 0; j < maxbrandsInd.length; j++) {
                            if (summsbrands[brandsInd[i] + 'Indirect']['all'] > maxsummsInd[j]) {
                                for (let k = maxbrandsInd.length; k > j; k--) {
                                    maxsummsInd[k] = maxsummsInd[k - 1];
                                    maxbrandsInd[k] = maxbrandsInd[k - 1];
                                };
                                maxsummsInd[j] = summsbrands[brandsInd[i] + 'Indirect']['all'];
                                maxbrandsInd[j] = brandsInd[i];
                                break;
                            } else if (j == (maxbrandsInd.length - 1)) {
                                maxsummsInd[j + 1] = summsbrands[brandsInd[i] + 'Indirect']['all'];
                                maxbrandsInd[j + 1] = brandsInd[i];
                                break;
                            };
                        };
                    };
                    brands.push('Other');
                    segments.forEach(function(item) {
                        summssegments[item + 'Other'] = {};
                        summssegments[item + 'Other']['all'] = 0;
                        summssegments[item + 'Other']['mtm'] = 0;
                        summssegments[item + 'Other']['mtm1'] = 0;
                        summssegments[item + 'Other']['mtm2'] = 0;
                        summssegments[item + 'Other']['mtm3'] = 0;
                        summssegments[item + 'Other']['ytd'] = 0;
                        summssegments[item + 'Other']['ytd1'] = 0;
                        summssegments[item + 'Other']['ytd2'] = 0;
                        summssegments[item + 'Other']['ytd3'] = 0;
                        summssegments[item + 'Other']['q0'] = 0;
                        summssegments[item + 'Other']['q1'] = 0;
                        summssegments[item + 'Other']['q2'] = 0;
                        summssegments[item + 'Other']['q3'] = 0;
                        summssegments[item + 'Other']['q4'] = 0;
                        summssegments[item + 'Other']['q5'] = 0;
                        summssegments[item + 'Other']['q6'] = 0;
                        summssegments[item + 'Other']['q7'] = 0;
                        summssegments[item + 'Other']['q8'] = 0;
                        summssegments[item + 'Other']['q9'] = 0;
                        summssegments[item + 'Other']['q10'] = 0;
                        summssegments[item + 'Other']['q11'] = 0;
                        summssegments[item + 'Other']['q12'] = 0;
                        summssegments[item + 'Other']['q13'] = 0;
                    });
                    for (let i = 0; i < (brands.length - 1); i++) {
                        let f = false;
                        for (let j = 0; j < 6; j++) {
                            if (brands[i] === maxbrands[j]) {
                                f = true;
                                break;
                            };
                        };
                        if (f) {
                            continue;
                        };
                        summsbrands['Other']['all'] += summsbrands[brands[i]]['all'];
                        summsbrands['Other']['mtm'] += summsbrands[brands[i]]['mtm'];
                        summsbrands['Other']['mtm1'] += summsbrands[brands[i]]['mtm1'];
                        summsbrands['Other']['mtm2'] += summsbrands[brands[i]]['mtm2'];
                        summsbrands['Other']['mtm3'] += summsbrands[brands[i]]['mtm3'];
                        summsbrands['Other']['ytd'] += summsbrands[brands[i]]['ytd'];
                        summsbrands['Other']['ytd1'] += summsbrands[brands[i]]['ytd1'];
                        summsbrands['Other']['ytd2'] += summsbrands[brands[i]]['ytd2'];
                        summsbrands['Other']['ytd3'] += summsbrands[brands[i]]['ytd3'];
                        summsbrands['Other']['q0'] += summsbrands[brands[i]]['q0'];
                        summsbrands['Other']['q1'] += summsbrands[brands[i]]['q1'];
                        summsbrands['Other']['q2'] += summsbrands[brands[i]]['q2'];
                        summsbrands['Other']['q3'] += summsbrands[brands[i]]['q3'];
                        summsbrands['Other']['q4'] += summsbrands[brands[i]]['q4'];
                        summsbrands['Other']['q5'] += summsbrands[brands[i]]['q5'];
                        summsbrands['Other']['q6'] += summsbrands[brands[i]]['q6'];
                        summsbrands['Other']['q7'] += summsbrands[brands[i]]['q7'];
                        summsbrands['Other']['q8'] += summsbrands[brands[i]]['q8'];
                        summsbrands['Other']['q9'] += summsbrands[brands[i]]['q9'];
                        summsbrands['Other']['q10'] += summsbrands[brands[i]]['q10'];
                        summsbrands['Other']['q11'] += summsbrands[brands[i]]['q11'];
                        summsbrands['Other']['q12'] += summsbrands[brands[i]]['q12'];
                        summsbrands['Other']['q13'] += summsbrands[brands[i]]['q13'];
                    };
                    maxsumms[maxbrands.length] = summsbrands['Other']['all'];
                    maxbrands.push('Other');
                    brandsInd.push('Other');
                    for (let i = 0; i < (brandsInd.length - 1); i++) {
                        let f = false;
                        for (let j = 0; j < 6; j++) {
                            if (brandsInd[i] === maxbrandsInd[j]) {
                                f = true;
                                break;
                            };
                        };
                        if (f) {
                            continue;
                        };
                        summsbrands['OtherIndirect']['all'] += summsbrands[brandsInd[i] + 'Indirect']['all'];
                        summsbrands['OtherIndirect']['mtm'] += summsbrands[brandsInd[i] + 'Indirect']['mtm'];
                        summsbrands['OtherIndirect']['mtm1'] += summsbrands[brandsInd[i] + 'Indirect']['mtm1'];
                        summsbrands['OtherIndirect']['mtm2'] += summsbrands[brandsInd[i] + 'Indirect']['mtm2'];
                        summsbrands['OtherIndirect']['mtm3'] += summsbrands[brandsInd[i] + 'Indirect']['mtm3'];
                        summsbrands['OtherIndirect']['ytd'] += summsbrands[brandsInd[i] + 'Indirect']['ytd'];
                        summsbrands['OtherIndirect']['ytd1'] += summsbrands[brandsInd[i] + 'Indirect']['ytd1'];
                        summsbrands['OtherIndirect']['ytd2'] += summsbrands[brandsInd[i] + 'Indirect']['ytd2'];
                        summsbrands['OtherIndirect']['ytd3'] += summsbrands[brandsInd[i] + 'Indirect']['ytd3'];
                        summsbrands['OtherIndirect']['q0'] += summsbrands[brandsInd[i] + 'Indirect']['q0'];
                        summsbrands['OtherIndirect']['q1'] += summsbrands[brandsInd[i] + 'Indirect']['q1'];
                        summsbrands['OtherIndirect']['q2'] += summsbrands[brandsInd[i] + 'Indirect']['q2'];
                        summsbrands['OtherIndirect']['q3'] += summsbrands[brandsInd[i] + 'Indirect']['q3'];
                        summsbrands['OtherIndirect']['q4'] += summsbrands[brandsInd[i] + 'Indirect']['q4'];
                        summsbrands['OtherIndirect']['q5'] += summsbrands[brandsInd[i] + 'Indirect']['q5'];
                        summsbrands['OtherIndirect']['q6'] += summsbrands[brandsInd[i] + 'Indirect']['q6'];
                        summsbrands['OtherIndirect']['q7'] += summsbrands[brandsInd[i] + 'Indirect']['q7'];
                        summsbrands['OtherIndirect']['q8'] += summsbrands[brandsInd[i] + 'Indirect']['q8'];
                        summsbrands['OtherIndirect']['q9'] += summsbrands[brandsInd[i] + 'Indirect']['q9'];
                        summsbrands['OtherIndirect']['q10'] += summsbrands[brandsInd[i] + 'Indirect']['q10'];
                        summsbrands['OtherIndirect']['q11'] += summsbrands[brandsInd[i] + 'Indirect']['q11'];
                        summsbrands['OtherIndirect']['q12'] += summsbrands[brandsInd[i] + 'Indirect']['q12'];
                        summsbrands['OtherIndirect']['q13'] += summsbrands[brandsInd[i] + 'Indirect']['q13'];
                        summsbrands['OtherIndirect']['q14'] += summsbrands[brandsInd[i] + 'Indirect']['q14'];
                        summsbrands['OtherIndirect']['q15'] += summsbrands[brandsInd[i] + 'Indirect']['q15'];
                        summsbrands['OtherIndirect']['q16'] += summsbrands[brandsInd[i] + 'Indirect']['q16'];
                        summsbrands['OtherIndirect']['q17'] += summsbrands[brandsInd[i] + 'Indirect']['q17'];
                        summsbrands['OtherIndirect']['q18'] += summsbrands[brandsInd[i] + 'Indirect']['q18'];
                        summsbrands['OtherIndirect']['q19'] += summsbrands[brandsInd[i] + 'Indirect']['q19'];
                        summsbrands['OtherIndirect']['q20'] += summsbrands[brandsInd[i] + 'Indirect']['q20'];
                        summsbrands['OtherIndirect']['q21'] += summsbrands[brandsInd[i] + 'Indirect']['q21'];
                        summsbrands['OtherIndirect']['q22'] += summsbrands[brandsInd[i] + 'Indirect']['q22'];
                        summsbrands['OtherIndirect']['q23'] += summsbrands[brandsInd[i] + 'Indirect']['q23'];
                        summsbrands['OtherIndirect']['q24'] += summsbrands[brandsInd[i] + 'Indirect']['q24'];
                        summsbrands['OtherIndirect']['q25'] += summsbrands[brandsInd[i] + 'Indirect']['q25'];
                        segments.forEach(function(item) {
                            summssegments[item + 'Other']['all'] += summssegments[item + brandsInd[i]]['all'];
                            summssegments[item + 'Other']['mtm'] += summssegments[item + brandsInd[i]]['mtm'];
                            summssegments[item + 'Other']['mtm1'] += summssegments[item + brandsInd[i]]['mtm1'];
                            summssegments[item + 'Other']['mtm2'] += summssegments[item + brandsInd[i]]['mtm2'];
                            summssegments[item + 'Other']['mtm3'] += summssegments[item + brandsInd[i]]['mtm3'];
                            summssegments[item + 'Other']['ytd'] += summssegments[item + brandsInd[i]]['ytd'];
                            summssegments[item + 'Other']['ytd1'] += summssegments[item + brandsInd[i]]['ytd1'];
                            summssegments[item + 'Other']['ytd2'] += summssegments[item + brandsInd[i]]['ytd2'];
                            summssegments[item + 'Other']['ytd3'] += summssegments[item + brandsInd[i]]['ytd3'];
                            summssegments[item + 'Other']['q0'] += summssegments[item + brandsInd[i]]['q0'];
                            summssegments[item + 'Other']['q1'] += summssegments[item + brandsInd[i]]['q1'];
                            summssegments[item + 'Other']['q2'] += summssegments[item + brandsInd[i]]['q2'];
                            summssegments[item + 'Other']['q3'] += summssegments[item + brandsInd[i]]['q3'];
                            summssegments[item + 'Other']['q4'] += summssegments[item + brandsInd[i]]['q4'];
                            summssegments[item + 'Other']['q5'] += summssegments[item + brandsInd[i]]['q5'];
                            summssegments[item + 'Other']['q6'] += summssegments[item + brandsInd[i]]['q6'];
                            summssegments[item + 'Other']['q7'] += summssegments[item + brandsInd[i]]['q7'];
                            summssegments[item + 'Other']['q8'] += summssegments[item + brandsInd[i]]['q8'];
                            summssegments[item + 'Other']['q9'] += summssegments[item + brandsInd[i]]['q9'];
                            summssegments[item + 'Other']['q10'] += summssegments[item + brandsInd[i]]['q10'];
                            summssegments[item + 'Other']['q11'] += summssegments[item + brandsInd[i]]['q11'];
                            summssegments[item + 'Other']['q12'] += summssegments[item + brandsInd[i]]['q12'];
                            summssegments[item + 'Other']['q13'] += summssegments[item + brandsInd[i]]['q13'];
                        });
                    };
                    maxsummsInd[maxbrandsInd.length] = summsbrands['OtherIndirect']['all'];
                    maxbrandsInd.push('Other');
                    brandsInd.forEach(function(item) {
                        summsbrands5[item] = {};
                        summsbrands5[item]['all'] = summsbrands[item + 'Indirect']['all'];
                        summsbrands5[item]['mtm'] = summsbrands[item + 'Indirect']['mtm'];
                        summsbrands5[item]['mtm1'] = summsbrands[item + 'Indirect']['mtm1'];
                        summsbrands5[item]['mtm2'] = summsbrands[item + 'Indirect']['mtm2'];
                        summsbrands5[item]['mtm3'] = summsbrands[item + 'Indirect']['mtm3'];
                        summsbrands5[item]['ytd'] = summsbrands[item + 'Indirect']['ytd'];
                        summsbrands5[item]['ytd1'] = summsbrands[item + 'Indirect']['ytd1'];
                        summsbrands5[item]['ytd2'] = summsbrands[item + 'Indirect']['ytd2'];
                        summsbrands5[item]['ytd3'] = summsbrands[item + 'Indirect']['ytd3'];
                        summsbrands5[item]['q0'] = summsbrands[item + 'Indirect']['q0'];
                        summsbrands5[item]['q1'] = summsbrands[item + 'Indirect']['q1'];
                        summsbrands5[item]['q2'] = summsbrands[item + 'Indirect']['q2'];
                        summsbrands5[item]['q3'] = summsbrands[item + 'Indirect']['q3'];
                        summsbrands5[item]['q4'] = summsbrands[item + 'Indirect']['q4'];
                        summsbrands5[item]['q5'] = summsbrands[item + 'Indirect']['q5'];
                        summsbrands5[item]['q6'] = summsbrands[item + 'Indirect']['q6'];
                        summsbrands5[item]['q7'] = summsbrands[item + 'Indirect']['q7'];
                        summsbrands5[item]['q8'] = summsbrands[item + 'Indirect']['q8'];
                        summsbrands5[item]['q9'] = summsbrands[item + 'Indirect']['q9'];
                        summsbrands5[item]['q10'] = summsbrands[item + 'Indirect']['q10'];
                        summsbrands5[item]['q11'] = summsbrands[item + 'Indirect']['q11'];
                        summsbrands5[item]['q12'] = summsbrands[item + 'Indirect']['q12'];
                        summsbrands5[item]['q13'] = summsbrands[item + 'Indirect']['q13'];
                        summsbrands5[item]['q14'] = summsbrands[item + 'Indirect']['q14'];
                        summsbrands5[item]['q15'] = summsbrands[item + 'Indirect']['q15'];
                        summsbrands5[item]['q16'] = summsbrands[item + 'Indirect']['q16'];
                        summsbrands5[item]['q17'] = summsbrands[item + 'Indirect']['q17'];
                        summsbrands5[item]['q18'] = summsbrands[item + 'Indirect']['q18'];
                        summsbrands5[item]['q19'] = summsbrands[item + 'Indirect']['q19'];
                        summsbrands5[item]['q20'] = summsbrands[item + 'Indirect']['q20'];
                        summsbrands5[item]['q21'] = summsbrands[item + 'Indirect']['q21'];
                        summsbrands5[item]['q22'] = summsbrands[item + 'Indirect']['q22'];
                        summsbrands5[item]['q23'] = summsbrands[item + 'Indirect']['q23'];
                        summsbrands5[item]['q24'] = summsbrands[item + 'Indirect']['q24'];
                        summsbrands5[item]['q25'] = summsbrands[item + 'Indirect']['q25'];
                    });
                    summsbrands_t['Indirect'] = {};
                    summsbrands_t['Direct'] = {};
                    for (let key in summsbrands['Indirect']) {
                        summsbrands_t['Indirect'][key] = Math.floor(summsbrands['Indirect'][key] / 100000) / 10000;
                        summsbrands_t['Direct'][key] = Math.floor(summsbrands['Direct'][key] / 100000) / 10000;
                    };
                    brands.forEach(function(item) {
                        summsbrands_t[item] = {};
                        summsbrands_t[item + 'Indirect'] = {};
                        for (let key in summsbrands[item]) {
                            summsbrands_t[item][key] = summsbrands[item][key];
                            summsbrands_t[item + 'Indirect'][key] = summsbrands[item + 'Indirect'][key];
                        };
                        summsbrands[item]['all'] = ((summsall > 0) ? summsbrands[item]['all'] / summsall : 0.0);
                        summsbrands[item]['mtm'] = ((summsMTM['cur'] > 0) ? summsbrands[item]['mtm'] / summsMTM['cur'] : 0.0);
                        summsbrands[item]['mtm1'] = ((summsMTM['cur_1'] > 0) ? summsbrands[item]['mtm1'] / summsMTM['cur_1'] : 0.0);
                        summsbrands[item]['mtm2'] = ((summsMTM['cur_2'] > 0) ? summsbrands[item]['mtm2'] / summsMTM['cur_2'] : 0.0);
                        summsbrands[item]['mtm3'] = ((summsMTM['cur_3'] > 0) ? summsbrands[item]['mtm3'] / summsMTM['cur_3'] : 0.0);
                        summsbrands[item]['ytd'] = ((summsYTD['cur'] > 0) ? summsbrands[item]['ytd'] / summsYTD['cur'] : 0.0);
                        summsbrands[item]['ytd1'] = ((summsYTD['cur_1'] > 0) ? summsbrands[item]['ytd1'] / summsYTD['cur_1'] : 0.0);
                        summsbrands[item]['ytd2'] = ((summsYTD['cur_2'] > 0) ? summsbrands[item]['ytd2'] / summsYTD['cur_2'] : 0.0);
                        summsbrands[item]['ytd3'] = ((summsYTD['cur_3'] > 0) ? summsbrands[item]['ytd3'] / summsYTD['cur_3'] : 0.0);
                        summsbrands[item]['q0'] = ((summsM[0]['cur'] > 0) ? summsbrands[item]['q0'] / summsM[0]['cur'] : 0.0);
                        summsbrands[item]['q1'] = ((summsM[1]['cur'] > 0) ? summsbrands[item]['q1'] / summsM[1]['cur'] : 0.0);
                        summsbrands[item]['q2'] = ((summsM[2]['cur'] > 0) ? summsbrands[item]['q2'] / summsM[2]['cur'] : 0.0);
                        summsbrands[item]['q3'] = ((summsM[3]['cur'] > 0) ? summsbrands[item]['q3'] / summsM[3]['cur'] : 0.0);
                        summsbrands[item]['q4'] = ((summsM[4]['cur'] > 0) ? summsbrands[item]['q4'] / summsM[4]['cur'] : 0.0);
                        summsbrands[item]['q5'] = ((summsM[5]['cur'] > 0) ? summsbrands[item]['q5'] / summsM[5]['cur'] : 0.0);
                        summsbrands[item]['q6'] = ((summsM[6]['cur'] > 0) ? summsbrands[item]['q6'] / summsM[6]['cur'] : 0.0);
                        summsbrands[item]['q7'] = ((summsM[7]['cur'] > 0) ? summsbrands[item]['q7'] / summsM[7]['cur'] : 0.0);
                        summsbrands[item]['q8'] = ((summsM[8]['cur'] > 0) ? summsbrands[item]['q8'] / summsM[8]['cur'] : 0.0);
                        summsbrands[item]['q9'] = ((summsM[9]['cur'] > 0) ? summsbrands[item]['q9'] / summsM[9]['cur'] : 0.0);
                        summsbrands[item]['q10'] = ((summsM[10]['cur'] > 0) ? summsbrands[item]['q10'] / summsM[10]['cur'] : 0.0);
                        summsbrands[item]['q11'] = ((summsM[11]['cur'] > 0) ? summsbrands[item]['q11'] / summsM[11]['cur'] : 0.0);
                        summsbrands[item]['q12'] = ((summsM[12]['cur'] > 0) ? summsbrands[item]['q12'] / summsM[12]['cur'] : 0.0);
                        summsbrands[item]['q13'] = ((summsM[13]['cur'] > 0) ? summsbrands[item]['q13'] / summsM[13]['cur'] : 0.0);
                        summsbrands[item + 'Indirect']['all'] = ((summsallInd > 0) ? summsbrands[item + 'Indirect']['all'] / summsallInd : 0.0);
                        summsbrands[item + 'Indirect']['mtm'] = ((summsMTM['curi'] > 0) ? summsbrands[item + 'Indirect']['mtm'] / summsMTM['curi'] : 0.0);
                        summsbrands[item + 'Indirect']['mtm1'] = ((summsMTM['curi_1'] > 0) ? summsbrands[item + 'Indirect']['mtm1'] / summsMTM['curi_1'] : 0.0);
                        summsbrands[item + 'Indirect']['mtm2'] = ((summsMTM['curi_2'] > 0) ? summsbrands[item + 'Indirect']['mtm2'] / summsMTM['curi_2'] : 0.0);
                        summsbrands[item + 'Indirect']['mtm3'] = ((summsMTM['curi_3'] > 0) ? summsbrands[item + 'Indirect']['mtm3'] / summsMTM['curi_3'] : 0.0);
                        summsbrands[item + 'Indirect']['ytd'] = ((summsYTD['curi'] > 0) ? summsbrands[item + 'Indirect']['ytd'] / summsYTD['curi'] : 0.0);
                        summsbrands[item + 'Indirect']['ytd1'] = ((summsYTD['curi_1'] > 0) ? summsbrands[item + 'Indirect']['ytd1'] / summsYTD['curi_1'] : 0.0);
                        summsbrands[item + 'Indirect']['ytd2'] = ((summsYTD['curi_2'] > 0) ? summsbrands[item + 'Indirect']['ytd2'] / summsYTD['curi_2'] : 0.0);
                        summsbrands[item + 'Indirect']['ytd3'] = ((summsYTD['curi_3'] > 0) ? summsbrands[item + 'Indirect']['ytd3'] / summsYTD['curi_3'] : 0.0);
                        summsbrands[item + 'Indirect']['q0'] = ((summsM[0]['curi'] > 0) ? summsbrands[item + 'Indirect']['q0'] / summsM[0]['curi'] : 0.0);
                        summsbrands[item + 'Indirect']['q1'] = ((summsM[1]['curi'] > 0) ? summsbrands[item + 'Indirect']['q1'] / summsM[1]['curi'] : 0.0);
                        summsbrands[item + 'Indirect']['q2'] = ((summsM[2]['curi'] > 0) ? summsbrands[item + 'Indirect']['q2'] / summsM[2]['curi'] : 0.0);
                        summsbrands[item + 'Indirect']['q3'] = ((summsM[3]['curi'] > 0) ? summsbrands[item + 'Indirect']['q3'] / summsM[3]['curi'] : 0.0);
                        summsbrands[item + 'Indirect']['q4'] = ((summsM[4]['curi'] > 0) ? summsbrands[item + 'Indirect']['q4'] / summsM[4]['curi'] : 0.0);
                        summsbrands[item + 'Indirect']['q5'] = ((summsM[5]['curi'] > 0) ? summsbrands[item + 'Indirect']['q5'] / summsM[5]['curi'] : 0.0);
                        summsbrands[item + 'Indirect']['q6'] = ((summsM[6]['curi'] > 0) ? summsbrands[item + 'Indirect']['q6'] / summsM[6]['curi'] : 0.0);
                        summsbrands[item + 'Indirect']['q7'] = ((summsM[7]['curi'] > 0) ? summsbrands[item + 'Indirect']['q7'] / summsM[7]['curi'] : 0.0);
                        summsbrands[item + 'Indirect']['q8'] = ((summsM[8]['curi'] > 0) ? summsbrands[item + 'Indirect']['q8'] / summsM[8]['curi'] : 0.0);
                        summsbrands[item + 'Indirect']['q9'] = ((summsM[9]['curi'] > 0) ? summsbrands[item + 'Indirect']['q9'] / summsM[9]['curi'] : 0.0);
                        summsbrands[item + 'Indirect']['q10'] = ((summsM[10]['curi'] > 0) ? summsbrands[item + 'Indirect']['q10'] / summsM[10]['curi'] : 0.0);
                        summsbrands[item + 'Indirect']['q11'] = ((summsM[11]['curi'] > 0) ? summsbrands[item + 'Indirect']['q11'] / summsM[11]['curi'] : 0.0);
                        summsbrands[item + 'Indirect']['q12'] = ((summsM[12]['curi'] > 0) ? summsbrands[item + 'Indirect']['q12'] / summsM[12]['curi'] : 0.0);
                        summsbrands[item + 'Indirect']['q13'] = ((summsM[13]['curi'] > 0) ? summsbrands[item + 'Indirect']['q13'] / summsM[13]['curi'] : 0.0);
                    });
                    segments.forEach(function(item) {
                        summssegments_t[item] = {};
                        for (let key in summssegments[item]) {
                            summssegments_t[item][key] = summssegments[item][key];
                        };
                        summssegments[item]['all'] = ((summsallInd > 0) ? summssegments[item]['all'] / summsallInd : 0.0);
                        summssegments[item]['mtm'] = ((summsMTM['curi'] > 0) ? summssegments[item]['mtm'] / summsMTM['curi'] : 0.0);
                        summssegments[item]['mtm1'] = ((summsMTM['curi_1'] > 0) ? summssegments[item]['mtm1'] / summsMTM['curi_1'] : 0.0);
                        summssegments[item]['mtm2'] = ((summsMTM['curi_2'] > 0) ? summssegments[item]['mtm2'] / summsMTM['curi_2'] : 0.0);
                        summssegments[item]['mtm3'] = ((summsMTM['curi_3'] > 0) ? summssegments[item]['mtm3'] / summsMTM['curi_3'] : 0.0);
                        summssegments[item]['ytd'] = ((summsYTD['curi'] > 0) ? summssegments[item]['ytd'] / summsYTD['curi'] : 0.0);
                        summssegments[item]['ytd1'] = ((summsYTD['curi_1'] > 0) ? summssegments[item]['ytd1'] / summsYTD['curi_1'] : 0.0);
                        summssegments[item]['ytd2'] = ((summsYTD['curi_2'] > 0) ? summssegments[item]['ytd2'] / summsYTD['curi_2'] : 0.0);
                        summssegments[item]['ytd3'] = ((summsYTD['curi_3'] > 0) ? summssegments[item]['ytd3'] / summsYTD['curi_3'] : 0.0);
                        summssegments[item]['q0'] = ((summsM[0]['curi'] > 0) ? summssegments[item]['q0'] / summsM[0]['curi'] : 0.0);
                        summssegments[item]['q1'] = ((summsM[1]['curi'] > 0) ? summssegments[item]['q1'] / summsM[1]['curi'] : 0.0);
                        summssegments[item]['q2'] = ((summsM[2]['curi'] > 0) ? summssegments[item]['q2'] / summsM[2]['curi'] : 0.0);
                        summssegments[item]['q3'] = ((summsM[3]['curi'] > 0) ? summssegments[item]['q3'] / summsM[3]['curi'] : 0.0);
                        summssegments[item]['q4'] = ((summsM[4]['curi'] > 0) ? summssegments[item]['q4'] / summsM[4]['curi'] : 0.0);
                        summssegments[item]['q5'] = ((summsM[5]['curi'] > 0) ? summssegments[item]['q5'] / summsM[5]['curi'] : 0.0);
                        summssegments[item]['q6'] = ((summsM[6]['curi'] > 0) ? summssegments[item]['q6'] / summsM[6]['curi'] : 0.0);
                        summssegments[item]['q7'] = ((summsM[7]['curi'] > 0) ? summssegments[item]['q7'] / summsM[7]['curi'] : 0.0);
                        summssegments[item]['q8'] = ((summsM[8]['curi'] > 0) ? summssegments[item]['q8'] / summsM[8]['curi'] : 0.0);
                        summssegments[item]['q9'] = ((summsM[9]['curi'] > 0) ? summssegments[item]['q9'] / summsM[9]['curi'] : 0.0);
                        summssegments[item]['q10'] = ((summsM[10]['curi'] > 0) ? summssegments[item]['q10'] / summsM[10]['curi'] : 0.0);
                        summssegments[item]['q11'] = ((summsM[11]['curi'] > 0) ? summssegments[item]['q11'] / summsM[11]['curi'] : 0.0);
                        summssegments[item]['q12'] = ((summsM[12]['curi'] > 0) ? summssegments[item]['q12'] / summsM[12]['curi'] : 0.0);
                        summssegments[item]['q13'] = ((summsM[13]['curi'] > 0) ? summssegments[item]['q13'] / summsM[13]['curi'] : 0.0);
                        brandsInd.forEach(function(item2) {
                            summssegments_t[item + item2] = {};
                            for (let key in summssegments[item + item2]) {
                                summssegments_t[item + item2][key] = summssegments[item + item2][key];
                            };
                            summssegments[item + item2]['all'] = ((summsallInd > 0) ? summssegments[item + item2]['all'] / summsallInd : 0.0);
                            summssegments[item + item2]['mtm'] = ((summsMTM['curi'] > 0) ? summssegments[item + item2]['mtm'] / summsMTM['curi'] : 0.0);
                            summssegments[item + item2]['mtm1'] = ((summsMTM['curi_1'] > 0) ? summssegments[item + item2]['mtm1'] / summsMTM['curi_1'] : 0.0);
                            summssegments[item + item2]['mtm2'] = ((summsMTM['curi_2'] > 0) ? summssegments[item + item2]['mtm2'] / summsMTM['curi_2'] : 0.0);
                            summssegments[item + item2]['mtm3'] = ((summsMTM['curi_3'] > 0) ? summssegments[item + item2]['mtm3'] / summsMTM['curi_3'] : 0.0);
                            summssegments[item + item2]['ytd'] = ((summsYTD['curi'] > 0) ? summssegments[item + item2]['ytd'] / summsYTD['curi'] : 0.0);
                            summssegments[item + item2]['ytd1'] = ((summsYTD['curi_1'] > 0) ? summssegments[item + item2]['ytd1'] / summsYTD['curi_1'] : 0.0);
                            summssegments[item + item2]['ytd2'] = ((summsYTD['curi_2'] > 0) ? summssegments[item + item2]['ytd2'] / summsYTD['curi_2'] : 0.0);
                            summssegments[item + item2]['ytd3'] = ((summsYTD['curi_3'] > 0) ? summssegments[item + item2]['ytd3'] / summsYTD['curi_3'] : 0.0);
                            summssegments[item + item2]['q0'] = ((summsM[0]['curi'] > 0) ? summssegments[item + item2]['q0'] / summsM[0]['curi'] : 0.0);
                            summssegments[item + item2]['q1'] = ((summsM[1]['curi'] > 0) ? summssegments[item + item2]['q1'] / summsM[1]['curi'] : 0.0);
                            summssegments[item + item2]['q2'] = ((summsM[2]['curi'] > 0) ? summssegments[item + item2]['q2'] / summsM[2]['curi'] : 0.0);
                            summssegments[item + item2]['q3'] = ((summsM[3]['curi'] > 0) ? summssegments[item + item2]['q3'] / summsM[3]['curi'] : 0.0);
                            summssegments[item + item2]['q4'] = ((summsM[4]['curi'] > 0) ? summssegments[item + item2]['q4'] / summsM[4]['curi'] : 0.0);
                            summssegments[item + item2]['q5'] = ((summsM[5]['curi'] > 0) ? summssegments[item + item2]['q5'] / summsM[5]['curi'] : 0.0);
                            summssegments[item + item2]['q6'] = ((summsM[6]['curi'] > 0) ? summssegments[item + item2]['q6'] / summsM[6]['curi'] : 0.0);
                            summssegments[item + item2]['q7'] = ((summsM[7]['curi'] > 0) ? summssegments[item + item2]['q7'] / summsM[7]['curi'] : 0.0);
                            summssegments[item + item2]['q8'] = ((summsM[8]['curi'] > 0) ? summssegments[item + item2]['q8'] / summsM[8]['curi'] : 0.0);
                            summssegments[item + item2]['q9'] = ((summsM[9]['curi'] > 0) ? summssegments[item + item2]['q9'] / summsM[9]['curi'] : 0.0);
                            summssegments[item + item2]['q10'] = ((summsM[10]['curi'] > 0) ? summssegments[item + item2]['q10'] / summsM[10]['curi'] : 0.0);
                            summssegments[item + item2]['q11'] = ((summsM[11]['curi'] > 0) ? summssegments[item + item2]['q11'] / summsM[11]['curi'] : 0.0);
                            summssegments[item + item2]['q12'] = ((summsM[12]['curi'] > 0) ? summssegments[item + item2]['q12'] / summsM[12]['curi'] : 0.0);
                            summssegments[item + item2]['q13'] = ((summsM[13]['curi'] > 0) ? summssegments[item + item2]['q13'] / summsM[13]['curi'] : 0.0);
                            summssegments[item + item2]['mtm1'] = Math.floor(summssegments[item + item2]['mtm1'] * 1000) / 10;
                            summssegments[item + item2]['mtm'] = Math.floor(summssegments[item + item2]['mtm'] * 1000) / 10;
                            summssegments[item + item2]['ytd1'] = Math.floor(summssegments[item + item2]['ytd1'] * 1000) / 10;
                            summssegments[item + item2]['ytd'] = Math.floor(summssegments[item + item2]['ytd'] * 1000) / 10;
                            summssegments[item + item2]['q0'] = Math.floor(summssegments[item + item2]['q0'] * 1000) / 10;
                            summssegments[item + item2]['q1'] = Math.floor(summssegments[item + item2]['q1'] * 1000) / 10;
                            summssegments[item + item2]['q2'] = Math.floor(summssegments[item + item2]['q2'] * 1000) / 10;
                            summssegments[item + item2]['q3'] = Math.floor(summssegments[item + item2]['q3'] * 1000) / 10;
                            summssegments[item + item2]['q4'] = Math.floor(summssegments[item + item2]['q4'] * 1000) / 10;
                            summssegments[item + item2]['q5'] = Math.floor(summssegments[item + item2]['q5'] * 1000) / 10;
                            summssegments[item + item2]['q6'] = Math.floor(summssegments[item + item2]['q6'] * 1000) / 10;
                            summssegments[item + item2]['q7'] = Math.floor(summssegments[item + item2]['q7'] * 1000) / 10;
                            summssegments[item + item2]['q8'] = Math.floor(summssegments[item + item2]['q8'] * 1000) / 10;
                            summssegments[item + item2]['q9'] = Math.floor(summssegments[item + item2]['q9'] * 1000) / 10;
                            summssegments[item + item2]['q10'] = Math.floor(summssegments[item + item2]['q10'] * 1000) / 10;
                            summssegments[item + item2]['q11'] = Math.floor(summssegments[item + item2]['q11'] * 1000) / 10;
                            summssegments[item + item2]['q12'] = Math.floor(summssegments[item + item2]['q12'] * 1000) / 10;
                            summssegments[item + item2]['q13'] = Math.floor(summssegments[item + item2]['q13'] * 1000) / 10;
                            summssegments_t[item + item2]['mtm1'] = Math.floor(summssegments_t[item + item2]['mtm1'] / 100000) / 10000;
                            summssegments_t[item + item2]['mtm'] = Math.floor(summssegments_t[item + item2]['mtm'] / 100000) / 10000;
                            summssegments_t[item + item2]['ytd1'] = Math.floor(summssegments_t[item + item2]['ytd1'] / 100000) / 10000;
                            summssegments_t[item + item2]['ytd'] = Math.floor(summssegments_t[item + item2]['ytd'] / 100000) / 10000;
                            summssegments_t[item + item2]['q0'] = Math.floor(summssegments_t[item + item2]['q0'] / 100000) / 10000;
                            summssegments_t[item + item2]['q1'] = Math.floor(summssegments_t[item + item2]['q1'] / 100000) / 10000;
                            summssegments_t[item + item2]['q2'] = Math.floor(summssegments_t[item + item2]['q2'] / 100000) / 10000;
                            summssegments_t[item + item2]['q3'] = Math.floor(summssegments_t[item + item2]['q3'] / 100000) / 10000;
                            summssegments_t[item + item2]['q4'] = Math.floor(summssegments_t[item + item2]['q4'] / 100000) / 10000;
                            summssegments_t[item + item2]['q5'] = Math.floor(summssegments_t[item + item2]['q5'] / 100000) / 10000;
                            summssegments_t[item + item2]['q6'] = Math.floor(summssegments_t[item + item2]['q6'] / 100000) / 10000;
                            summssegments_t[item + item2]['q7'] = Math.floor(summssegments_t[item + item2]['q7'] / 100000) / 10000;
                            summssegments_t[item + item2]['q8'] = Math.floor(summssegments_t[item + item2]['q8'] / 100000) / 10000;
                            summssegments_t[item + item2]['q9'] = Math.floor(summssegments_t[item + item2]['q9'] / 100000) / 10000;
                            summssegments_t[item + item2]['q10'] = Math.floor(summssegments_t[item + item2]['q10'] / 100000) / 10000;
                            summssegments_t[item + item2]['q11'] = Math.floor(summssegments_t[item + item2]['q11'] / 100000) / 10000;
                            summssegments_t[item + item2]['q12'] = Math.floor(summssegments_t[item + item2]['q12'] / 100000) / 10000;
                            summssegments_t[item + item2]['q13'] = Math.floor(summssegments_t[item + item2]['q13'] / 100000) / 10000;
                        });
                    });
                    let grapgbrands = [];
                    let count = brands.length - 1;
                    if (count > 6) {
                        count = 6;
                    };
                    let elnum = maxbrands.indexOf('ФИЛИПС');
                    if ((elnum != -1) && (elnum < count)) {
                        grapgbrands.push('ФИЛИПС');
                        maxbrands.splice(elnum, 1);
                        count--;
                    };
                    elnum = maxbrands.indexOf('ДЖЕНЕРАЛ ЭЛЕКТРИК');
                    if ((elnum != -1) && (elnum < count)) {
                        grapgbrands.push('ДЖЕНЕРАЛ ЭЛЕКТРИК');
                        maxbrands.splice(elnum, 1);
                        count--;
                    };
                    elnum = maxbrands.indexOf('САМСУНГ МЕДИСОН КО.; ЛТД.');
                    if ((elnum != -1) && (elnum < count)) {
                        grapgbrands.push('САМСУНГ МЕДИСОН КО.; ЛТД.');
                        maxbrands.splice(elnum, 1);
                        count--;
                    };
                    elnum = maxbrands.indexOf('СИМЕНС АГ');
                    if ((elnum != -1) && (elnum < count)) {
                        grapgbrands.push('СИМЕНС АГ');
                        maxbrands.splice(elnum, 1);
                        count--;
                    };
                    elnum = maxbrands.indexOf('МИНДРЕЙ');
                    if ((elnum != -1) && (elnum < count)) {
                        grapgbrands.push('МИНДРЕЙ');
                        maxbrands.splice(elnum, 1);
                        count--;
                    };
                    elnum = maxbrands.indexOf('ХИТАЧИ АЛОКА МЕДИКАЛ');
                    if ((elnum != -1) && (elnum < count)) {
                        grapgbrands.push('ХИТАЧИ АЛОКА МЕДИКАЛ');
                        maxbrands.splice(elnum, 1);
                        count--;
                    };
                    for (let i = 0; i < count; i++) {
                        grapgbrands.push(maxbrands[i]);
                    };
                    grapgbrands.push('Other');
                    let grapgbrandsind = [];
                    count = brandsInd.length - 1;
                    if (count > 6) {
                        count = 6;
                    };
                    elnum = maxbrandsInd.indexOf('ФИЛИПС');
                    if ((elnum != -1) && (elnum < count)) {
                        grapgbrandsind.push('ФИЛИПС');
                        maxbrandsInd.splice(elnum, 1);
                        count--;
                    };
                    elnum = maxbrandsInd.indexOf('ДЖЕНЕРАЛ ЭЛЕКТРИК');
                    if ((elnum != -1) && (elnum < count)) {
                        grapgbrandsind.push('ДЖЕНЕРАЛ ЭЛЕКТРИК');
                        maxbrandsInd.splice(elnum, 1);
                        count--;
                    };
                    elnum = maxbrandsInd.indexOf('САМСУНГ МЕДИСОН КО.; ЛТД.');
                    if ((elnum != -1) && (elnum < count)) {
                        grapgbrandsind.push('САМСУНГ МЕДИСОН КО.; ЛТД.');
                        maxbrandsInd.splice(elnum, 1);
                        count--;
                    };
                    elnum = maxbrandsInd.indexOf('СИМЕНС АГ');
                    if ((elnum != -1) && (elnum < count)) {
                        grapgbrandsind.push('СИМЕНС АГ');
                        maxbrandsInd.splice(elnum, 1);
                        count--;
                    };
                    elnum = maxbrandsInd.indexOf('МИНДРЕЙ');
                    if ((elnum != -1) && (elnum < count)) {
                        grapgbrandsind.push('МИНДРЕЙ');
                        maxbrandsInd.splice(elnum, 1);
                        count--;
                    };
                    elnum = maxbrandsInd.indexOf('ХИТАЧИ АЛОКА МЕДИКАЛ');
                    if ((elnum != -1) && (elnum < count)) {
                        grapgbrandsind.push('ХИТАЧИ АЛОКА МЕДИКАЛ');
                        maxbrandsInd.splice(elnum, 1);
                        count--;
                    };
                    for (let i = 0; i < count; i++) {
                        grapgbrandsind.push(maxbrandsInd[i]);
                    };
                    grapgbrandsind.push('Other');
                    let mydata1_1 = [];
                    let mydata2_1 = [];
                    let mydata4_1 = [];
                    let mydata1_2 = [];
                    let mydata2_2 = [];
                    let mydata4_2 = [];
                    let mydata1_3 = [];
                    let mydata2_3 = [];
                    let mydata4_3 = [];
                    let mydata1_4 = [];
                    let mydata2_4 = [];
                    let mydata4_4 = [];
                    let mydata1_5 = [];
                    let mydata2_5 = [];
                    let mydata4_5 = [];
                    window.ChartData3 = [
                        [],
                        [{
                            label: 'Direct',
                            backgroundColor: 'rgba(84, 150, 211, 1)',
                        }, {
                            label: 'Indirect',
                            backgroundColor: 'rgba(237, 125, 49, 1)',
                        }, ],
                        [],
                        [],
                        [],
                    ];
                    let diff = {};
                    let diffInd = {};
                    window.PercData1 = [
                        [
                            ((summsMTM['cur_1'] > 0) ? Math.floor(summsbrands['Indirect']['mtm1'] / summsMTM['cur_1'] * 1000) / 10 : 100.0),
                            ((summsMTM['cur'] > 0) ? Math.floor(summsbrands['Indirect']['mtm'] / summsMTM['cur'] * 1000) / 10 : 100.0),
                        ],
                        [
                            ((summsMTM['cur_1'] > 0) ? Math.floor(summsbrands['Direct']['mtm1'] / summsMTM['cur_1'] * 1000) / 10 : 100.0),
                            ((summsMTM['cur'] > 0) ? Math.floor(summsbrands['Direct']['mtm'] / summsMTM['cur'] * 1000) / 10 : 100.0),
                        ],
                    ];
                    window.PercData2 = [
                        [
                            ((summsYTD['cur_1'] > 0) ? Math.floor(summsbrands['Indirect']['ytd1'] / summsYTD['cur_1'] * 1000) / 10 : 100.0),
                            ((summsYTD['cur'] > 0) ? Math.floor(summsbrands['Indirect']['ytd'] / summsYTD['cur'] * 1000) / 10 : 100.0),
                        ],
                        [
                            ((summsYTD['cur_1'] > 0) ? Math.floor(summsbrands['Direct']['ytd1'] / summsYTD['cur_1'] * 1000) / 10 : 100.0),
                            ((summsYTD['cur'] > 0) ? Math.floor(summsbrands['Direct']['ytd'] / summsYTD['cur'] * 1000) / 10 : 100.0),
                        ],
                    ];
                    window.PercData4 = [
                        [
                            ((summsM[13]['cur'] > 0) ? Math.floor(summsbrands['Indirect']['q13'] / summsM[13]['cur'] * 1000) / 10 : 100.0),
                            ((summsM[12]['cur'] > 0) ? Math.floor(summsbrands['Indirect']['q12'] / summsM[12]['cur'] * 1000) / 10 : 100.0),
                            ((summsM[11]['cur'] > 0) ? Math.floor(summsbrands['Indirect']['q11'] / summsM[11]['cur'] * 1000) / 10 : 100.0),
                            ((summsM[10]['cur'] > 0) ? Math.floor(summsbrands['Indirect']['q10'] / summsM[10]['cur'] * 1000) / 10 : 100.0),
                            ((summsM[9]['cur'] > 0) ? Math.floor(summsbrands['Indirect']['q9'] / summsM[9]['cur'] * 1000) / 10 : 100.0),
                            ((summsM[8]['cur'] > 0) ? Math.floor(summsbrands['Indirect']['q8'] / summsM[8]['cur'] * 1000) / 10 : 100.0),
                            ((summsM[7]['cur'] > 0) ? Math.floor(summsbrands['Indirect']['q7'] / summsM[7]['cur'] * 1000) / 10 : 100.0),
                            ((summsM[6]['cur'] > 0) ? Math.floor(summsbrands['Indirect']['q6'] / summsM[6]['cur'] * 1000) / 10 : 100.0),
                            ((summsM[5]['cur'] > 0) ? Math.floor(summsbrands['Indirect']['q5'] / summsM[5]['cur'] * 1000) / 10 : 100.0),
                            ((summsM[4]['cur'] > 0) ? Math.floor(summsbrands['Indirect']['q4'] / summsM[4]['cur'] * 1000) / 10 : 100.0),
                            ((summsM[3]['cur'] > 0) ? Math.floor(summsbrands['Indirect']['q3'] / summsM[3]['cur'] * 1000) / 10 : 100.0),
                            ((summsM[2]['cur'] > 0) ? Math.floor(summsbrands['Indirect']['q2'] / summsM[2]['cur'] * 1000) / 10 : 100.0),
                            ((summsM[1]['cur'] > 0) ? Math.floor(summsbrands['Indirect']['q1'] / summsM[1]['cur'] * 1000) / 10 : 100.0),
                            ((summsM[0]['cur'] > 0) ? Math.floor(summsbrands['Indirect']['q0'] / summsM[0]['cur'] * 1000) / 10 : 100.0),
                        ],
                        [
                            ((summsM[13]['cur'] > 0) ? Math.floor(summsbrands['Direct']['q13'] / summsM[13]['cur'] * 1000) / 10 : 100.0),
                            ((summsM[12]['cur'] > 0) ? Math.floor(summsbrands['Direct']['q12'] / summsM[12]['cur'] * 1000) / 10 : 100.0),
                            ((summsM[11]['cur'] > 0) ? Math.floor(summsbrands['Direct']['q11'] / summsM[11]['cur'] * 1000) / 10 : 100.0),
                            ((summsM[10]['cur'] > 0) ? Math.floor(summsbrands['Direct']['q10'] / summsM[10]['cur'] * 1000) / 10 : 100.0),
                            ((summsM[9]['cur'] > 0) ? Math.floor(summsbrands['Direct']['q9'] / summsM[9]['cur'] * 1000) / 10 : 100.0),
                            ((summsM[8]['cur'] > 0) ? Math.floor(summsbrands['Direct']['q8'] / summsM[8]['cur'] * 1000) / 10 : 100.0),
                            ((summsM[7]['cur'] > 0) ? Math.floor(summsbrands['Direct']['q7'] / summsM[7]['cur'] * 1000) / 10 : 100.0),
                            ((summsM[6]['cur'] > 0) ? Math.floor(summsbrands['Direct']['q6'] / summsM[6]['cur'] * 1000) / 10 : 100.0),
                            ((summsM[5]['cur'] > 0) ? Math.floor(summsbrands['Direct']['q5'] / summsM[5]['cur'] * 1000) / 10 : 100.0),
                            ((summsM[4]['cur'] > 0) ? Math.floor(summsbrands['Direct']['q4'] / summsM[4]['cur'] * 1000) / 10 : 100.0),
                            ((summsM[3]['cur'] > 0) ? Math.floor(summsbrands['Direct']['q3'] / summsM[3]['cur'] * 1000) / 10 : 100.0),
                            ((summsM[2]['cur'] > 0) ? Math.floor(summsbrands['Direct']['q2'] / summsM[2]['cur'] * 1000) / 10 : 100.0),
                            ((summsM[1]['cur'] > 0) ? Math.floor(summsbrands['Direct']['q1'] / summsM[1]['cur'] * 1000) / 10 : 100.0),
                            ((summsM[0]['cur'] > 0) ? Math.floor(summsbrands['Direct']['q0'] / summsM[0]['cur'] * 1000) / 10 : 100.0),
                        ],
                    ];
                    mydata1_2.push({
                        label: window.DirIndirData['Indirect']['name'],
                        backgroundColor: window.DirIndirData['Indirect']['color'],
                        data: [
                            summsbrands['Indirect']['mtm1'],
                            summsbrands['Indirect']['mtm'],
                        ],
                        tooltipsValues: [
                            summsbrands_t['Indirect']['mtm1'],
                            summsbrands_t['Indirect']['mtm'],
                        ],
                    });
                    mydata2_2.push({
                        label: window.DirIndirData['Indirect']['name'],
                        backgroundColor: window.DirIndirData['Indirect']['color'],
                        data: [
                            summsbrands['Indirect']['ytd1'],
                            summsbrands['Indirect']['ytd'],
                        ],
                        tooltipsValues: [
                            summsbrands_t['Indirect']['ytd1'],
                            summsbrands_t['Indirect']['ytd'],
                        ],
                    });
                    mydata4_2.push({
                        label: window.DirIndirData['Indirect']['name'],
                        backgroundColor: window.DirIndirData['Indirect']['color'],
                        data: [
                            summsbrands['Indirect']['q13'],
                            summsbrands['Indirect']['q12'],
                            summsbrands['Indirect']['q11'],
                            summsbrands['Indirect']['q10'],
                            summsbrands['Indirect']['q9'],
                            summsbrands['Indirect']['q8'],
                            summsbrands['Indirect']['q7'],
                            summsbrands['Indirect']['q6'],
                            summsbrands['Indirect']['q5'],
                            summsbrands['Indirect']['q4'],
                            summsbrands['Indirect']['q3'],
                            summsbrands['Indirect']['q2'],
                            summsbrands['Indirect']['q1'],
                            summsbrands['Indirect']['q0'],
                        ],
                        tooltipsValues: [
                            summsbrands_t['Indirect']['q13'],
                            summsbrands_t['Indirect']['q12'],
                            summsbrands_t['Indirect']['q11'],
                            summsbrands_t['Indirect']['q10'],
                            summsbrands_t['Indirect']['q9'],
                            summsbrands_t['Indirect']['q8'],
                            summsbrands_t['Indirect']['q7'],
                            summsbrands_t['Indirect']['q6'],
                            summsbrands_t['Indirect']['q5'],
                            summsbrands_t['Indirect']['q4'],
                            summsbrands_t['Indirect']['q3'],
                            summsbrands_t['Indirect']['q2'],
                            summsbrands_t['Indirect']['q1'],
                            summsbrands_t['Indirect']['q0'],
                        ],
                    });
                    mydata1_2.push({
                        label: window.DirIndirData['Direct']['name'],
                        backgroundColor: window.DirIndirData['Direct']['color'],
                        data: [
                            summsbrands['Direct']['mtm1'],
                            summsbrands['Direct']['mtm'],
                        ],
                        tooltipsValues: [
                            summsbrands_t['Direct']['mtm1'],
                            summsbrands_t['Direct']['mtm'],
                        ],
                    });
                    mydata2_2.push({
                        label: window.DirIndirData['Direct']['name'],
                        backgroundColor: window.DirIndirData['Direct']['color'],
                        data: [
                            summsbrands['Direct']['ytd1'],
                            summsbrands['Direct']['ytd'],
                        ],
                        tooltipsValues: [
                            summsbrands_t['Direct']['ytd1'],
                            summsbrands_t['Direct']['ytd'],
                        ],
                    });
                    mydata4_2.push({
                        label: window.DirIndirData['Direct']['name'],
                        backgroundColor: window.DirIndirData['Direct']['color'],
                        data: [
                            summsbrands['Direct']['q13'],
                            summsbrands['Direct']['q12'],
                            summsbrands['Direct']['q11'],
                            summsbrands['Direct']['q10'],
                            summsbrands['Direct']['q9'],
                            summsbrands['Direct']['q8'],
                            summsbrands['Direct']['q7'],
                            summsbrands['Direct']['q6'],
                            summsbrands['Direct']['q5'],
                            summsbrands['Direct']['q4'],
                            summsbrands['Direct']['q3'],
                            summsbrands['Direct']['q2'],
                            summsbrands['Direct']['q1'],
                            summsbrands['Direct']['q0'],
                        ],
                        tooltipsValues: [
                            summsbrands_t['Direct']['q13'],
                            summsbrands_t['Direct']['q12'],
                            summsbrands_t['Direct']['q11'],
                            summsbrands_t['Direct']['q10'],
                            summsbrands_t['Direct']['q9'],
                            summsbrands_t['Direct']['q8'],
                            summsbrands_t['Direct']['q7'],
                            summsbrands_t['Direct']['q6'],
                            summsbrands_t['Direct']['q5'],
                            summsbrands_t['Direct']['q4'],
                            summsbrands_t['Direct']['q3'],
                            summsbrands_t['Direct']['q2'],
                            summsbrands_t['Direct']['q1'],
                            summsbrands_t['Direct']['q0'],
                        ],
                    });
                    grapgbrands.forEach(function(item) {
                        summsbrands[item]['mtm1'] = Math.floor(summsbrands[item]['mtm1'] * 1000) / 10;
                        summsbrands[item]['mtm'] = Math.floor(summsbrands[item]['mtm'] * 1000) / 10;
                        summsbrands[item]['ytd1'] = Math.floor(summsbrands[item]['ytd1'] * 1000) / 10;
                        summsbrands[item]['ytd'] = Math.floor(summsbrands[item]['ytd'] * 1000) / 10;
                        summsbrands[item]['q13'] = Math.floor(summsbrands[item]['q13'] * 1000) / 10;
                        summsbrands[item]['q12'] = Math.floor(summsbrands[item]['q12'] * 1000) / 10;
                        summsbrands[item]['q11'] = Math.floor(summsbrands[item]['q11'] * 1000) / 10;
                        summsbrands[item]['q10'] = Math.floor(summsbrands[item]['q10'] * 1000) / 10;
                        summsbrands[item]['q9'] = Math.floor(summsbrands[item]['q9'] * 1000) / 10;
                        summsbrands[item]['q8'] = Math.floor(summsbrands[item]['q8'] * 1000) / 10;
                        summsbrands[item]['q7'] = Math.floor(summsbrands[item]['q7'] * 1000) / 10;
                        summsbrands[item]['q6'] = Math.floor(summsbrands[item]['q6'] * 1000) / 10;
                        summsbrands[item]['q5'] = Math.floor(summsbrands[item]['q5'] * 1000) / 10;
                        summsbrands[item]['q4'] = Math.floor(summsbrands[item]['q4'] * 1000) / 10;
                        summsbrands[item]['q3'] = Math.floor(summsbrands[item]['q3'] * 1000) / 10;
                        summsbrands[item]['q2'] = Math.floor(summsbrands[item]['q2'] * 1000) / 10;
                        summsbrands[item]['q1'] = Math.floor(summsbrands[item]['q1'] * 1000) / 10;
                        summsbrands[item]['q0'] = Math.floor(summsbrands[item]['q0'] * 1000) / 10;
                        summsbrands_t[item]['mtm1'] = Math.floor(summsbrands_t[item]['mtm1'] / 100000) / 10000;
                        summsbrands_t[item]['mtm'] = Math.floor(summsbrands_t[item]['mtm'] / 100000) / 10000;
                        summsbrands_t[item]['ytd1'] = Math.floor(summsbrands_t[item]['ytd1'] / 100000) / 10000;
                        summsbrands_t[item]['ytd'] = Math.floor(summsbrands_t[item]['ytd'] / 100000) / 10000;
                        summsbrands_t[item]['q13'] = Math.floor(summsbrands_t[item]['q13'] / 100000) / 10000;
                        summsbrands_t[item]['q12'] = Math.floor(summsbrands_t[item]['q12'] / 100000) / 10000;
                        summsbrands_t[item]['q11'] = Math.floor(summsbrands_t[item]['q11'] / 100000) / 10000;
                        summsbrands_t[item]['q10'] = Math.floor(summsbrands_t[item]['q10'] / 100000) / 10000;
                        summsbrands_t[item]['q9'] = Math.floor(summsbrands_t[item]['q9'] / 100000) / 10000;
                        summsbrands_t[item]['q8'] = Math.floor(summsbrands_t[item]['q8'] / 100000) / 10000;
                        summsbrands_t[item]['q7'] = Math.floor(summsbrands_t[item]['q7'] / 100000) / 10000;
                        summsbrands_t[item]['q6'] = Math.floor(summsbrands_t[item]['q6'] / 100000) / 10000;
                        summsbrands_t[item]['q5'] = Math.floor(summsbrands_t[item]['q5'] / 100000) / 10000;
                        summsbrands_t[item]['q4'] = Math.floor(summsbrands_t[item]['q4'] / 100000) / 10000;
                        summsbrands_t[item]['q3'] = Math.floor(summsbrands_t[item]['q3'] / 100000) / 10000;
                        summsbrands_t[item]['q2'] = Math.floor(summsbrands_t[item]['q2'] / 100000) / 10000;
                        summsbrands_t[item]['q1'] = Math.floor(summsbrands_t[item]['q1'] / 100000) / 10000;
                        summsbrands_t[item]['q0'] = Math.floor(summsbrands_t[item]['q0'] / 100000) / 10000;
                        diff[item] = Math.floor((summsbrands[item]['ytd'] - summsbrands[item]['ytd1']) * 10) / 10;
                        mydata1_1.unshift({
                            label: window.BrandsData[item]['name'],
                            backgroundColor: window.BrandsData[item]['color'],
                            data: [
                                summsbrands[item]['mtm1'],
                                summsbrands[item]['mtm'],
                            ],
                            tooltipsValues: [
                                summsbrands_t[item]['mtm1'],
                                summsbrands_t[item]['mtm'],
                            ],
                        });
                        mydata2_1.unshift({
                            label: window.BrandsData[item]['name'],
                            backgroundColor: window.BrandsData[item]['color'],
                            data: [
                                summsbrands[item]['ytd1'],
                                summsbrands[item]['ytd'],
                            ],
                            tooltipsValues: [
                                summsbrands_t[item]['ytd1'],
                                summsbrands_t[item]['ytd'],
                            ],
                        });
                        window.ChartData3[0].push({
                            label: window.BrandsData[item]['name'],
                            backgroundColor: window.BrandsData[item]['color'],
                        });
                        mydata4_1.unshift({
                            label: window.BrandsData[item]['name'],
                            backgroundColor: window.BrandsData[item]['color'],
                            data: [
                                summsbrands[item]['q13'],
                                summsbrands[item]['q12'],
                                summsbrands[item]['q11'],
                                summsbrands[item]['q10'],
                                summsbrands[item]['q9'],
                                summsbrands[item]['q8'],
                                summsbrands[item]['q7'],
                                summsbrands[item]['q6'],
                                summsbrands[item]['q5'],
                                summsbrands[item]['q4'],
                                summsbrands[item]['q3'],
                                summsbrands[item]['q2'],
                                summsbrands[item]['q1'],
                                summsbrands[item]['q0'],
                            ],
                            tooltipsValues: [
                                summsbrands_t[item]['q13'],
                                summsbrands_t[item]['q12'],
                                summsbrands_t[item]['q11'],
                                summsbrands_t[item]['q10'],
                                summsbrands_t[item]['q9'],
                                summsbrands_t[item]['q8'],
                                summsbrands_t[item]['q7'],
                                summsbrands_t[item]['q6'],
                                summsbrands_t[item]['q5'],
                                summsbrands_t[item]['q4'],
                                summsbrands_t[item]['q3'],
                                summsbrands_t[item]['q2'],
                                summsbrands_t[item]['q1'],
                                summsbrands_t[item]['q0'],
                            ],
                        });
                    });
                    window.MarkCodes = [];
                    window.MarkNames = [];
                    grapgbrandsind.forEach(function(item) {
                        window.MarkCodes.push(item);
                        window.MarkNames.push(window.BrandsData[item].name);
                        summsbrands[item + 'Indirect']['mtm1'] = Math.floor(summsbrands[item + 'Indirect']['mtm1'] * 1000) / 10;
                        summsbrands[item + 'Indirect']['mtm'] = Math.floor(summsbrands[item + 'Indirect']['mtm'] * 1000) / 10;
                        summsbrands[item + 'Indirect']['ytd1'] = Math.floor(summsbrands[item + 'Indirect']['ytd1'] * 1000) / 10;
                        summsbrands[item + 'Indirect']['ytd'] = Math.floor(summsbrands[item + 'Indirect']['ytd'] * 1000) / 10;
                        summsbrands[item + 'Indirect']['q13'] = Math.floor(summsbrands[item + 'Indirect']['q13'] * 1000) / 10;
                        summsbrands[item + 'Indirect']['q12'] = Math.floor(summsbrands[item + 'Indirect']['q12'] * 1000) / 10;
                        summsbrands[item + 'Indirect']['q11'] = Math.floor(summsbrands[item + 'Indirect']['q11'] * 1000) / 10;
                        summsbrands[item + 'Indirect']['q10'] = Math.floor(summsbrands[item + 'Indirect']['q10'] * 1000) / 10;
                        summsbrands[item + 'Indirect']['q9'] = Math.floor(summsbrands[item + 'Indirect']['q9'] * 1000) / 10;
                        summsbrands[item + 'Indirect']['q8'] = Math.floor(summsbrands[item + 'Indirect']['q8'] * 1000) / 10;
                        summsbrands[item + 'Indirect']['q7'] = Math.floor(summsbrands[item + 'Indirect']['q7'] * 1000) / 10;
                        summsbrands[item + 'Indirect']['q6'] = Math.floor(summsbrands[item + 'Indirect']['q6'] * 1000) / 10;
                        summsbrands[item + 'Indirect']['q5'] = Math.floor(summsbrands[item + 'Indirect']['q5'] * 1000) / 10;
                        summsbrands[item + 'Indirect']['q4'] = Math.floor(summsbrands[item + 'Indirect']['q4'] * 1000) / 10;
                        summsbrands[item + 'Indirect']['q3'] = Math.floor(summsbrands[item + 'Indirect']['q3'] * 1000) / 10;
                        summsbrands[item + 'Indirect']['q2'] = Math.floor(summsbrands[item + 'Indirect']['q2'] * 1000) / 10;
                        summsbrands[item + 'Indirect']['q1'] = Math.floor(summsbrands[item + 'Indirect']['q1'] * 1000) / 10;
                        summsbrands[item + 'Indirect']['q0'] = Math.floor(summsbrands[item + 'Indirect']['q0'] * 1000) / 10;
                        summsbrands_t[item + 'Indirect']['mtm1'] = Math.floor(summsbrands_t[item + 'Indirect']['mtm1'] / 100000) / 10000;
                        summsbrands_t[item + 'Indirect']['mtm'] = Math.floor(summsbrands_t[item + 'Indirect']['mtm'] / 100000) / 10000;
                        summsbrands_t[item + 'Indirect']['ytd1'] = Math.floor(summsbrands_t[item + 'Indirect']['ytd1'] / 100000) / 10000;
                        summsbrands_t[item + 'Indirect']['ytd'] = Math.floor(summsbrands_t[item + 'Indirect']['ytd'] / 100000) / 10000;
                        summsbrands_t[item + 'Indirect']['q13'] = Math.floor(summsbrands_t[item + 'Indirect']['q13'] / 100000) / 10000;
                        summsbrands_t[item + 'Indirect']['q12'] = Math.floor(summsbrands_t[item + 'Indirect']['q12'] / 100000) / 10000;
                        summsbrands_t[item + 'Indirect']['q11'] = Math.floor(summsbrands_t[item + 'Indirect']['q11'] / 100000) / 10000;
                        summsbrands_t[item + 'Indirect']['q10'] = Math.floor(summsbrands_t[item + 'Indirect']['q10'] / 100000) / 10000;
                        summsbrands_t[item + 'Indirect']['q9'] = Math.floor(summsbrands_t[item + 'Indirect']['q9'] / 100000) / 10000;
                        summsbrands_t[item + 'Indirect']['q8'] = Math.floor(summsbrands_t[item + 'Indirect']['q8'] / 100000) / 10000;
                        summsbrands_t[item + 'Indirect']['q7'] = Math.floor(summsbrands_t[item + 'Indirect']['q7'] / 100000) / 10000;
                        summsbrands_t[item + 'Indirect']['q6'] = Math.floor(summsbrands_t[item + 'Indirect']['q6'] / 100000) / 10000;
                        summsbrands_t[item + 'Indirect']['q5'] = Math.floor(summsbrands_t[item + 'Indirect']['q5'] / 100000) / 10000;
                        summsbrands_t[item + 'Indirect']['q4'] = Math.floor(summsbrands_t[item + 'Indirect']['q4'] / 100000) / 10000;
                        summsbrands_t[item + 'Indirect']['q3'] = Math.floor(summsbrands_t[item + 'Indirect']['q3'] / 100000) / 10000;
                        summsbrands_t[item + 'Indirect']['q2'] = Math.floor(summsbrands_t[item + 'Indirect']['q2'] / 100000) / 10000;
                        summsbrands_t[item + 'Indirect']['q1'] = Math.floor(summsbrands_t[item + 'Indirect']['q1'] / 100000) / 10000;
                        summsbrands_t[item + 'Indirect']['q0'] = Math.floor(summsbrands_t[item + 'Indirect']['q0'] / 100000) / 10000;
                        diffInd[item] = Math.floor((summsbrands[item + 'Indirect']['ytd'] - summsbrands[item + 'Indirect']['ytd1']) * 10) / 10;
                        mydata1_3.unshift({
                            label: window.BrandsData[item]['name'],
                            backgroundColor: window.BrandsData[item]['color'],
                            data: [
                                summsbrands[item + 'Indirect']['mtm1'],
                                summsbrands[item + 'Indirect']['mtm'],
                            ],
                            tooltipsValues: [
                                summsbrands_t[item + 'Indirect']['mtm1'],
                                summsbrands_t[item + 'Indirect']['mtm'],
                            ],
                        });
                        mydata2_3.unshift({
                            label: window.BrandsData[item]['name'],
                            backgroundColor: window.BrandsData[item]['color'],
                            data: [
                                summsbrands[item + 'Indirect']['ytd1'],
                                summsbrands[item + 'Indirect']['ytd'],
                            ],
                            tooltipsValues: [
                                summsbrands_t[item + 'Indirect']['ytd1'],
                                summsbrands_t[item + 'Indirect']['ytd'],
                            ],
                        });
                        window.ChartData3[2].push({
                            label: window.BrandsData[item]['name'],
                            backgroundColor: window.BrandsData[item]['color'],
                        });
                        mydata4_3.unshift({
                            label: window.BrandsData[item]['name'],
                            backgroundColor: window.BrandsData[item]['color'],
                            data: [
                                summsbrands[item + 'Indirect']['q13'],
                                summsbrands[item + 'Indirect']['q12'],
                                summsbrands[item + 'Indirect']['q11'],
                                summsbrands[item + 'Indirect']['q10'],
                                summsbrands[item + 'Indirect']['q9'],
                                summsbrands[item + 'Indirect']['q8'],
                                summsbrands[item + 'Indirect']['q7'],
                                summsbrands[item + 'Indirect']['q6'],
                                summsbrands[item + 'Indirect']['q5'],
                                summsbrands[item + 'Indirect']['q4'],
                                summsbrands[item + 'Indirect']['q3'],
                                summsbrands[item + 'Indirect']['q2'],
                                summsbrands[item + 'Indirect']['q1'],
                                summsbrands[item + 'Indirect']['q0'],
                            ],
                            tooltipsValues: [
                                summsbrands_t[item + 'Indirect']['q13'],
                                summsbrands_t[item + 'Indirect']['q12'],
                                summsbrands_t[item + 'Indirect']['q11'],
                                summsbrands_t[item + 'Indirect']['q10'],
                                summsbrands_t[item + 'Indirect']['q9'],
                                summsbrands_t[item + 'Indirect']['q8'],
                                summsbrands_t[item + 'Indirect']['q7'],
                                summsbrands_t[item + 'Indirect']['q6'],
                                summsbrands_t[item + 'Indirect']['q5'],
                                summsbrands_t[item + 'Indirect']['q4'],
                                summsbrands_t[item + 'Indirect']['q3'],
                                summsbrands_t[item + 'Indirect']['q2'],
                                summsbrands_t[item + 'Indirect']['q1'],
                                summsbrands_t[item + 'Indirect']['q0'],
                            ],
                        });
                    });
                    if (window.MarkCodes.length < 1) {
                        window.MarkCodes = ['САМСУНГ МЕДИСОН КО.; ЛТД.', ];
                        window.MarkNames = ['SAMSUNG', ];
                    }
                    segments.forEach(function(item) {
                        summssegments[item]['mtm1'] = Math.floor(summssegments[item]['mtm1'] * 1000) / 10;
                        summssegments[item]['mtm'] = Math.floor(summssegments[item]['mtm'] * 1000) / 10;
                        summssegments[item]['ytd1'] = Math.floor(summssegments[item]['ytd1'] * 1000) / 10;
                        summssegments[item]['ytd'] = Math.floor(summssegments[item]['ytd'] * 1000) / 10;
                        summssegments[item]['q13'] = Math.floor(summssegments[item]['q13'] * 1000) / 10;
                        summssegments[item]['q12'] = Math.floor(summssegments[item]['q12'] * 1000) / 10;
                        summssegments[item]['q11'] = Math.floor(summssegments[item]['q11'] * 1000) / 10;
                        summssegments[item]['q10'] = Math.floor(summssegments[item]['q10'] * 1000) / 10;
                        summssegments[item]['q9'] = Math.floor(summssegments[item]['q9'] * 1000) / 10;
                        summssegments[item]['q8'] = Math.floor(summssegments[item]['q8'] * 1000) / 10;
                        summssegments[item]['q7'] = Math.floor(summssegments[item]['q7'] * 1000) / 10;
                        summssegments[item]['q6'] = Math.floor(summssegments[item]['q6'] * 1000) / 10;
                        summssegments[item]['q5'] = Math.floor(summssegments[item]['q5'] * 1000) / 10;
                        summssegments[item]['q4'] = Math.floor(summssegments[item]['q4'] * 1000) / 10;
                        summssegments[item]['q3'] = Math.floor(summssegments[item]['q3'] * 1000) / 10;
                        summssegments[item]['q2'] = Math.floor(summssegments[item]['q2'] * 1000) / 10;
                        summssegments[item]['q1'] = Math.floor(summssegments[item]['q1'] * 1000) / 10;
                        summssegments[item]['q0'] = Math.floor(summssegments[item]['q0'] * 1000) / 10;
                        summssegments_t[item]['mtm1'] = Math.floor(summssegments_t[item]['mtm1'] / 100000) / 10000;
                        summssegments_t[item]['mtm'] = Math.floor(summssegments_t[item]['mtm'] / 100000) / 10000;
                        summssegments_t[item]['ytd1'] = Math.floor(summssegments_t[item]['ytd1'] / 100000) / 10000;
                        summssegments_t[item]['ytd'] = Math.floor(summssegments_t[item]['ytd'] / 100000) / 10000;
                        summssegments_t[item]['q13'] = Math.floor(summssegments_t[item]['q13'] / 100000) / 10000;
                        summssegments_t[item]['q12'] = Math.floor(summssegments_t[item]['q12'] / 100000) / 10000;
                        summssegments_t[item]['q11'] = Math.floor(summssegments_t[item]['q11'] / 100000) / 10000;
                        summssegments_t[item]['q10'] = Math.floor(summssegments_t[item]['q10'] / 100000) / 10000;
                        summssegments_t[item]['q9'] = Math.floor(summssegments_t[item]['q9'] / 100000) / 10000;
                        summssegments_t[item]['q8'] = Math.floor(summssegments_t[item]['q8'] / 100000) / 10000;
                        summssegments_t[item]['q7'] = Math.floor(summssegments_t[item]['q7'] / 100000) / 10000;
                        summssegments_t[item]['q6'] = Math.floor(summssegments_t[item]['q6'] / 100000) / 10000;
                        summssegments_t[item]['q5'] = Math.floor(summssegments_t[item]['q5'] / 100000) / 10000;
                        summssegments_t[item]['q4'] = Math.floor(summssegments_t[item]['q4'] / 100000) / 10000;
                        summssegments_t[item]['q3'] = Math.floor(summssegments_t[item]['q3'] / 100000) / 10000;
                        summssegments_t[item]['q2'] = Math.floor(summssegments_t[item]['q2'] / 100000) / 10000;
                        summssegments_t[item]['q1'] = Math.floor(summssegments_t[item]['q1'] / 100000) / 10000;
                        summssegments_t[item]['q0'] = Math.floor(summssegments_t[item]['q0'] / 100000) / 10000;
                    });
                    window.forFifthPage = {};
                    window.forFifthPage.segments = segments;
                    window.forFifthPage.summssegments = summssegments;
                    window.forFifthPage.summssegments_t = summssegments_t;
                    window.forFifthPage.summsbrands5 = summsbrands5;
                    segments.forEach(function(item) {
                        mydata1_4.unshift({
                            label: window.ProdSegData[item]['name'],
                            backgroundColor: window.ProdSegData[item]['color'],
                            data: [
                                summssegments[item]['mtm1'],
                                summssegments[item]['mtm'],
                            ],
                            tooltipsValues: [
                                summssegments_t[item]['mtm1'],
                                summssegments_t[item]['mtm'],
                            ],
                        });
                        mydata2_4.unshift({
                            label: window.ProdSegData[item]['name'],
                            backgroundColor: window.ProdSegData[item]['color'],
                            data: [
                                summssegments[item]['ytd1'],
                                summssegments[item]['ytd'],
                            ],
                            tooltipsValues: [
                                summssegments_t[item]['ytd1'],
                                summssegments_t[item]['ytd'],
                            ],
                        });
                        window.ChartData3[3].push({
                            label: window.ProdSegData[item]['name'],
                            backgroundColor: window.ProdSegData[item]['color'],
                        });
                        mydata4_4.unshift({
                            label: window.ProdSegData[item]['name'],
                            backgroundColor: window.ProdSegData[item]['color'],
                            data: [
                                summssegments[item]['q13'],
                                summssegments[item]['q12'],
                                summssegments[item]['q11'],
                                summssegments[item]['q10'],
                                summssegments[item]['q9'],
                                summssegments[item]['q8'],
                                summssegments[item]['q7'],
                                summssegments[item]['q6'],
                                summssegments[item]['q5'],
                                summssegments[item]['q4'],
                                summssegments[item]['q3'],
                                summssegments[item]['q2'],
                                summssegments[item]['q1'],
                                summssegments[item]['q0'],
                            ],
                            tooltipsValues: [
                                summssegments_t[item]['q13'],
                                summssegments_t[item]['q12'],
                                summssegments_t[item]['q11'],
                                summssegments_t[item]['q10'],
                                summssegments_t[item]['q9'],
                                summssegments_t[item]['q8'],
                                summssegments_t[item]['q7'],
                                summssegments_t[item]['q6'],
                                summssegments_t[item]['q5'],
                                summssegments_t[item]['q4'],
                                summssegments_t[item]['q3'],
                                summssegments_t[item]['q2'],
                                summssegments_t[item]['q1'],
                                summssegments_t[item]['q0'],
                            ],
                        });
                        mydata1_5.unshift({
                            label: window.ProdSegData[item]['name'],
                            backgroundColor: window.ProdSegData[item]['color'],
                            data: [
                                summssegments[item + window.MarkCodes[0]]['mtm1'],
                                summssegments[item + window.MarkCodes[0]]['mtm'],
                            ],
                            tooltipsValues: [
                                summssegments_t[item + window.MarkCodes[0]]['mtm1'],
                                summssegments_t[item + window.MarkCodes[0]]['mtm'],
                            ],
                        });
                        mydata2_5.unshift({
                            label: window.ProdSegData[item]['name'],
                            backgroundColor: window.ProdSegData[item]['color'],
                            data: [
                                summssegments[item + window.MarkCodes[0]]['ytd1'],
                                summssegments[item + window.MarkCodes[0]]['ytd'],
                            ],
                            tooltipsValues: [
                                summssegments_t[item + window.MarkCodes[0]]['ytd1'],
                                summssegments_t[item + window.MarkCodes[0]]['ytd'],
                            ],
                        });
                        window.ChartData3[4].push({
                            label: window.ProdSegData[item]['name'],
                            backgroundColor: window.ProdSegData[item]['color'],
                        });
                        mydata4_5.unshift({
                            label: window.ProdSegData[item]['name'],
                            backgroundColor: window.ProdSegData[item]['color'],
                            data: [
                                summssegments[item + window.MarkCodes[0]]['q13'],
                                summssegments[item + window.MarkCodes[0]]['q12'],
                                summssegments[item + window.MarkCodes[0]]['q11'],
                                summssegments[item + window.MarkCodes[0]]['q10'],
                                summssegments[item + window.MarkCodes[0]]['q9'],
                                summssegments[item + window.MarkCodes[0]]['q8'],
                                summssegments[item + window.MarkCodes[0]]['q7'],
                                summssegments[item + window.MarkCodes[0]]['q6'],
                                summssegments[item + window.MarkCodes[0]]['q5'],
                                summssegments[item + window.MarkCodes[0]]['q4'],
                                summssegments[item + window.MarkCodes[0]]['q3'],
                                summssegments[item + window.MarkCodes[0]]['q2'],
                                summssegments[item + window.MarkCodes[0]]['q1'],
                                summssegments[item + window.MarkCodes[0]]['q0'],
                            ],
                            tooltipsValues: [
                                summssegments_t[item + window.MarkCodes[0]]['q13'],
                                summssegments_t[item + window.MarkCodes[0]]['q12'],
                                summssegments_t[item + window.MarkCodes[0]]['q11'],
                                summssegments_t[item + window.MarkCodes[0]]['q10'],
                                summssegments_t[item + window.MarkCodes[0]]['q9'],
                                summssegments_t[item + window.MarkCodes[0]]['q8'],
                                summssegments_t[item + window.MarkCodes[0]]['q7'],
                                summssegments_t[item + window.MarkCodes[0]]['q6'],
                                summssegments_t[item + window.MarkCodes[0]]['q5'],
                                summssegments_t[item + window.MarkCodes[0]]['q4'],
                                summssegments_t[item + window.MarkCodes[0]]['q3'],
                                summssegments_t[item + window.MarkCodes[0]]['q2'],
                                summssegments_t[item + window.MarkCodes[0]]['q1'],
                                summssegments_t[item + window.MarkCodes[0]]['q0'],
                            ],
                        });
                    });
                    window.ChartData1 = [{
                        labels: [
                            window.MonthsForLegend[(Number(window.Month1) - 1)] + '’' + String(window.Year3 - 2000) + '-' + window.MonthsForLegend[(Number(window.Month) - 1)] + '’' + String(window.Year2 - 2000),
                            window.MonthsForLegend[(Number(window.Month1) - 1)] + '’' + String(window.Year1 - 2000) + '-' + window.MonthsForLegend[(Number(window.Month) - 1)] + '’' + String(window.Year - 2000),
                        ],
                        datasets: mydata1_1,
                    }, {
                        labels: [
                            window.MonthsForLegend[(Number(window.Month1) - 1)] + '’' + String(window.Year3 - 2000) + '-' + window.MonthsForLegend[(Number(window.Month) - 1)] + '’' + String(window.Year2 - 2000),
                            window.MonthsForLegend[(Number(window.Month1) - 1)] + '’' + String(window.Year1 - 2000) + '-' + window.MonthsForLegend[(Number(window.Month) - 1)] + '’' + String(window.Year - 2000),
                        ],
                        datasets: mydata1_2,
                    }, {
                        labels: [
                            window.MonthsForLegend[(Number(window.Month1) - 1)] + '’' + String(window.Year3 - 2000) + '-' + window.MonthsForLegend[(Number(window.Month) - 1)] + '’' + String(window.Year2 - 2000),
                            window.MonthsForLegend[(Number(window.Month1) - 1)] + '’' + String(window.Year1 - 2000) + '-' + window.MonthsForLegend[(Number(window.Month) - 1)] + '’' + String(window.Year - 2000),
                        ],
                        datasets: mydata1_3,
                    }, {
                        labels: [
                            window.MonthsForLegend[(Number(window.Month1) - 1)] + '’' + String(window.Year3 - 2000) + '-' + window.MonthsForLegend[(Number(window.Month) - 1)] + '’' + String(window.Year2 - 2000),
                            window.MonthsForLegend[(Number(window.Month1) - 1)] + '’' + String(window.Year1 - 2000) + '-' + window.MonthsForLegend[(Number(window.Month) - 1)] + '’' + String(window.Year - 2000),
                        ],
                        datasets: mydata1_4,
                    }, {
                        labels: [
                            window.MonthsForLegend[(Number(window.Month1) - 1)] + '’' + String(window.Year3 - 2000) + '-' + window.MonthsForLegend[(Number(window.Month) - 1)] + '’' + String(window.Year2 - 2000),
                            window.MonthsForLegend[(Number(window.Month1) - 1)] + '’' + String(window.Year1 - 2000) + '-' + window.MonthsForLegend[(Number(window.Month) - 1)] + '’' + String(window.Year - 2000),
                        ],
                        datasets: mydata1_5,
                    }, ];
                    window.ChartData2 = [{
                        labels: [
                            'YTD’' + String(window.Year2 - 2000) + ' ' + window.MonthsForLegend[(Number(window.Month) - 1)],
                            'YTD’' + String(window.Year - 2000) + ' ' + window.MonthsForLegend[(Number(window.Month) - 1)],
                        ],
                        datasets: mydata2_1,
                    }, {
                        labels: [
                            'YTD’' + String(window.Year2 - 2000) + ' ' + window.MonthsForLegend[(Number(window.Month) - 1)],
                            'YTD’' + String(window.Year - 2000) + ' ' + window.MonthsForLegend[(Number(window.Month) - 1)],
                        ],
                        datasets: mydata2_2,
                    }, {
                        labels: [
                            'YTD’' + String(window.Year2 - 2000) + ' ' + window.MonthsForLegend[(Number(window.Month) - 1)],
                            'YTD’' + String(window.Year - 2000) + ' ' + window.MonthsForLegend[(Number(window.Month) - 1)],
                        ],
                        datasets: mydata2_3,
                    }, {
                        labels: [
                            'YTD’' + String(window.Year2 - 2000) + ' ' + window.MonthsForLegend[(Number(window.Month) - 1)],
                            'YTD’' + String(window.Year - 2000) + ' ' + window.MonthsForLegend[(Number(window.Month) - 1)],
                        ],
                        datasets: mydata2_4,
                    }, {
                        labels: [
                            'YTD’' + String(window.Year2 - 2000) + ' ' + window.MonthsForLegend[(Number(window.Month) - 1)],
                            'YTD’' + String(window.Year - 2000) + ' ' + window.MonthsForLegend[(Number(window.Month) - 1)],
                        ],
                        datasets: mydata2_5,
                    }, ];
                    let tmpLabels = [
                        window.MonthsForLegend[((Number(window.Month) - 2) < 0) ? 11 : (Number(window.Month) - 2)] + '’' + String(((Number(window.Month) - 2) < 0) ? (window.Year - 2002) : (window.Year - 2001)),
                        window.MonthsForLegend[Number(window.Month) - 1] + '’' + String(window.Year - 2001),
                        window.MonthsForLegend[(Number(window.Month) > 11) ? (Number(window.Month) - 12) : Number(window.Month)] + '’' + String((Number(window.Month) > 11) ? (window.Year - 2000) : (window.Year - 2001)),
                        window.MonthsForLegend[((Number(window.Month) + 1) > 11) ? (Number(window.Month) - 11) : (Number(window.Month) + 1)] + '’' + String(((Number(window.Month) + 1) > 11) ? (window.Year - 2000) : (window.Year - 2001)),
                        window.MonthsForLegend[((Number(window.Month) + 2) > 11) ? (Number(window.Month) - 10) : (Number(window.Month) + 2)] + '’' + String(((Number(window.Month) + 2) > 11) ? (window.Year - 2000) : (window.Year - 2001)),
                        window.MonthsForLegend[((Number(window.Month) + 3) > 11) ? (Number(window.Month) - 9) : (Number(window.Month) + 3)] + '’' + String(((Number(window.Month) + 3) > 11) ? (window.Year - 2000) : (window.Year - 2001)),
                        window.MonthsForLegend[((Number(window.Month) + 4) > 11) ? (Number(window.Month) - 8) : (Number(window.Month) + 4)] + '’' + String(((Number(window.Month) + 4) > 11) ? (window.Year - 2000) : (window.Year - 2001)),
                        window.MonthsForLegend[((Number(window.Month) + 5) > 11) ? (Number(window.Month) - 7) : (Number(window.Month) + 5)] + '’' + String(((Number(window.Month) + 5) > 11) ? (window.Year - 2000) : (window.Year - 2001)),
                        window.MonthsForLegend[((Number(window.Month) + 6) > 11) ? (Number(window.Month) - 6) : (Number(window.Month) + 6)] + '’' + String(((Number(window.Month) + 6) > 11) ? (window.Year - 2000) : (window.Year - 2001)),
                        window.MonthsForLegend[((Number(window.Month) + 7) > 11) ? (Number(window.Month) - 5) : (Number(window.Month) + 7)] + '’' + String(((Number(window.Month) + 7) > 11) ? (window.Year - 2000) : (window.Year - 2001)),
                        window.MonthsForLegend[((Number(window.Month) + 8) > 11) ? (Number(window.Month) - 4) : (Number(window.Month) + 8)] + '’' + String(((Number(window.Month) + 8) > 11) ? (window.Year - 2000) : (window.Year - 2001)),
                        window.MonthsForLegend[((Number(window.Month) + 9) > 11) ? (Number(window.Month) - 3) : (Number(window.Month) + 9)] + '’' + String(((Number(window.Month) + 9) > 11) ? (window.Year - 2000) : (window.Year - 2001)),
                        window.MonthsForLegend[((Number(window.Month) + 10) > 11) ? (Number(window.Month) - 2) : (Number(window.Month) + 10)] + '’' + String(((Number(window.Month) + 10) > 11) ? (window.Year - 2000) : (window.Year - 2001)),
                        window.MonthsForLegend[Number(window.Month) - 1] + '’' + String(window.Year - 2000),
                    ];
                    window.ChartData4 = [{
                        labels: tmpLabels,
                        datasets: mydata4_1,
                    }, {
                        labels: tmpLabels,
                        datasets: mydata4_2,
                    }, {
                        labels: tmpLabels,
                        datasets: mydata4_3,
                    }, {
                        labels: tmpLabels,
                        datasets: mydata4_4,
                    }, {
                        labels: tmpLabels,
                        datasets: mydata4_5,
                    }, ];
                    window.SelectedBU = [
                        window.BUNames[document.getElementById("buselector").selectedIndex],
                        window.BUNames[document.getElementById("buselector").selectedIndex],
                        window.BUNames[document.getElementById("buselector").selectedIndex],
                        window.BUNames[document.getElementById("buselector").selectedIndex],
                        window.BUNames[document.getElementById("buselector").selectedIndex],
                    ];
                    let keysSorted = Object.keys(diff).sort(function(a, b) {
                        return diff[a] - diff[b]
                    });
                    window.SummaryList[0] = ['YTD MS% gainers: ', 'YTD MS% losers: '];
                    for (let i = 0; i < keysSorted.length; i++) {
                        if (diff[keysSorted[i]] < 0) {
                            window.SummaryList[0][1] += window.BrandsData[keysSorted[i]]['name'] + ' ' + diff[keysSorted[i]] + '%,';
                        } else {
                            break;
                        };
                    };
                    for (let i = (keysSorted.length - 1); i > -1; i--) {
                        if (diff[keysSorted[i]] > 0) {
                            window.SummaryList[0][0] += window.BrandsData[keysSorted[i]]['name'] + ' +' + diff[keysSorted[i]] + '%,';
                        } else {
                            break;
                        };
                    };
                    window.SummaryList[0][0].slice(0, window.SummaryList[0][0].length - 1);
                    window.SummaryList[0][1].slice(0, window.SummaryList[0][1].length - 1);
                    window.SummaryList[1] = [
                        'Market growth ' + String(((summsYTD['cur_1'] > 0) ? Math.floor(((summsYTD['cur'] - summsYTD['cur_1']) / summsYTD['cur_1']) * 1000) / 10 : 100.0)) + '% YTD driven by Direct / MTS',
                        'Direct channel (2020 YTD - MTS Moscow only) impacted TTL market dramatically in Q2 / Q3',
                    ];
                    keysSorted = Object.keys(diffInd).sort(function(a, b) {
                        return diffInd[a] - diffInd[b]
                    });
                    window.SummaryList[2] = [
                        'Indirect channel +/- flat ' + window.MonthsForLegend[(Number(window.Month) - 1)] + '’' + String(window.Year - 2000) + ' YTD, with strong growth Q1 / Q2 reversing to sharp decline in Q3',
                        'YTD MS% gainers: ',
                        'YTD MS% losers: ',
                    ];
                    for (let i = 0; i < keysSorted.length; i++) {
                        if (diffInd[keysSorted[i]] < 0) {
                            window.SummaryList[2][2] += window.BrandsData[keysSorted[i]]['name'] + ' ' + diffInd[keysSorted[i]] + '%,';
                        } else {
                            break;
                        };
                    };
                    for (let i = (keysSorted.length - 1); i > -1; i--) {
                        if (diffInd[keysSorted[i]] > 0) {
                            window.SummaryList[2][1] += window.BrandsData[keysSorted[i]]['name'] + ' +' + diffInd[keysSorted[i]] + '%,';
                        } else {
                            break;
                        };
                    };
                    window.SummaryList[2][1].slice(0, window.SummaryList[2][1].length - 1);
                    window.SummaryList[2][2].slice(0, window.SummaryList[2][2].length - 1);
                    window.SummaryList[3] = [
                        '<p style="font-size: 11px; color: #FF0000">Indirect channel +/- flat ' + window.MonthsForLegend[(Number(window.Month) - 1)] + '’' + String(window.Year - 2000) + ' YTD, with strong growth Q1 / Q2 reversing to sharp decline in Q3</p>',
                        '<p style="font-size: 11px; color: #FF0000">Major segments are +/- flat except Low-end compact boosted by Covid / emergency orders in Q2’20</p>',
                        '<p style="font-size: 11px; color: #FF0000">Handheld (Lumify etc.) remains marginal segment</p>',
                    ];
                    window.SummaryList[4] = [
                        '<p style="font-size: 11px; color: #FF0000">MS drivers for ' + window.MarkNames[0] + ': High-end Cart / Low-end Compact</p>',
                        '<p style="font-size: 11px; color: #FF0000">High-end Cart: extended portfolio (6 models)</p>',
                        '<p style="font-size: 11px; color: #FF0000">Low-end Compact: Covid / Emergency demand with decreased lead-times</p>',
                    ];
                    window.Legends[0] = [
                        String(Math.floor(summsMTM['cur_1'] / 100000000) / 10) + '<br><font color="#' + ((summsMTM['cur_1'] < summsMTM['cur_2']) ? 'c000' : '008') + '000">' + String(((summsMTM['cur_2'] > 0) ? Math.floor(((summsMTM['cur_1'] - summsMTM['cur_2']) / summsMTM['cur_2']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsMTM['cur'] / 100000000) / 10) + '<br><font color="#' + ((summsMTM['cur'] < summsMTM['cur_1']) ? 'c000' : '008') + '000">' + String(((summsMTM['cur_1'] > 0) ? Math.floor(((summsMTM['cur'] - summsMTM['cur_1']) / summsMTM['cur_1']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsYTD['cur_1'] / 100000000) / 10) + '<br><font color="#' + ((summsYTD['cur_1'] < summsYTD['cur_2']) ? 'c000' : '008') + '000">' + String(((summsYTD['cur_2'] > 0) ? Math.floor(((summsYTD['cur_1'] - summsYTD['cur_2']) / summsYTD['cur_2']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsYTD['cur'] / 100000000) / 10) + '<br><font color="#' + ((summsYTD['cur'] < summsYTD['cur_1']) ? 'c000' : '008') + '000">' + String(((summsYTD['cur_1'] > 0) ? Math.floor(((summsYTD['cur'] - summsYTD['cur_1']) / summsYTD['cur_1']) * 1000) / 10 : 100.0)) + '%</font>',
                        'Bln.RUB<br>+/- % SPLY',
                        String(Math.floor(summsM[13]['cur'] / 100000000) / 10) + '<br><font color="#' + ((summsM[13]['cur'] < summsM[25]['cur']) ? 'c000' : '008') + '000">' + String(((summsM[25]['cur'] > 0) ? Math.floor(((summsM[13]['cur'] - summsM[25]['cur']) / summsM[25]['cur']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsM[12]['cur'] / 100000000) / 10) + '<br><font color="#' + ((summsM[12]['cur'] < summsM[24]['cur']) ? 'c000' : '008') + '000">' + String(((summsM[24]['cur'] > 0) ? Math.floor(((summsM[12]['cur'] - summsM[24]['cur']) / summsM[24]['cur']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsM[11]['cur'] / 100000000) / 10) + '<br><font color="#' + ((summsM[11]['cur'] < summsM[23]['cur']) ? 'c000' : '008') + '000">' + String(((summsM[23]['cur'] > 0) ? Math.floor(((summsM[11]['cur'] - summsM[23]['cur']) / summsM[23]['cur']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsM[10]['cur'] / 100000000) / 10) + '<br><font color="#' + ((summsM[10]['cur'] < summsM[22]['cur']) ? 'c000' : '008') + '000">' + String(((summsM[22]['cur'] > 0) ? Math.floor(((summsM[10]['cur'] - summsM[22]['cur']) / summsM[22]['cur']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsM[9]['cur'] / 100000000) / 10) + '<br><font color="#' + ((summsM[9]['cur'] < summsM[21]['cur']) ? 'c000' : '008') + '000">' + String(((summsM[21]['cur'] > 0) ? Math.floor(((summsM[9]['cur'] - summsM[21]['cur']) / summsM[21]['cur']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsM[8]['cur'] / 100000000) / 10) + '<br><font color="#' + ((summsM[8]['cur'] < summsM[20]['cur']) ? 'c000' : '008') + '000">' + String(((summsM[20]['cur'] > 0) ? Math.floor(((summsM[8]['cur'] - summsM[20]['cur']) / summsM[20]['cur']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsM[7]['cur'] / 100000000) / 10) + '<br><font color="#' + ((summsM[7]['cur'] < summsM[19]['cur']) ? 'c000' : '008') + '000">' + String(((summsM[19]['cur'] > 0) ? Math.floor(((summsM[7]['cur'] - summsM[19]['cur']) / summsM[19]['cur']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsM[6]['cur'] / 100000000) / 10) + '<br><font color="#' + ((summsM[6]['cur'] < summsM[18]['cur']) ? 'c000' : '008') + '000">' + String(((summsM[18]['cur'] > 0) ? Math.floor(((summsM[6]['cur'] - summsM[18]['cur']) / summsM[18]['cur']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsM[5]['cur'] / 100000000) / 10) + '<br><font color="#' + ((summsM[5]['cur'] < summsM[17]['cur']) ? 'c000' : '008') + '000">' + String(((summsM[17]['cur'] > 0) ? Math.floor(((summsM[5]['cur'] - summsM[17]['cur']) / summsM[17]['cur']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsM[4]['cur'] / 100000000) / 10) + '<br><font color="#' + ((summsM[4]['cur'] < summsM[16]['cur']) ? 'c000' : '008') + '000">' + String(((summsM[16]['cur'] > 0) ? Math.floor(((summsM[4]['cur'] - summsM[16]['cur']) / summsM[16]['cur']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsM[3]['cur'] / 100000000) / 10) + '<br><font color="#' + ((summsM[3]['cur'] < summsM[15]['cur']) ? 'c000' : '008') + '000">' + String(((summsM[15]['cur'] > 0) ? Math.floor(((summsM[3]['cur'] - summsM[15]['cur']) / summsM[15]['cur']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsM[2]['cur'] / 100000000) / 10) + '<br><font color="#' + ((summsM[2]['cur'] < summsM[14]['cur']) ? 'c000' : '008') + '000">' + String(((summsM[14]['cur'] > 0) ? Math.floor(((summsM[2]['cur'] - summsM[14]['cur']) / summsM[14]['cur']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsM[1]['cur'] / 100000000) / 10) + '<br><font color="#' + ((summsM[1]['cur'] < summsM[13]['cur']) ? 'c000' : '008') + '000">' + String(((summsM[13]['cur'] > 0) ? Math.floor(((summsM[1]['cur'] - summsM[13]['cur']) / summsM[13]['cur']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsM[0]['cur'] / 100000000) / 10) + '<br><font color="#' + ((summsM[0]['cur'] < summsM[12]['cur']) ? 'c000' : '008') + '000">' + String(((summsM[12]['cur'] > 0) ? Math.floor(((summsM[0]['cur'] - summsM[12]['cur']) / summsM[12]['cur']) * 1000) / 10 : 100.0)) + '%</font>',
                    ];
                    window.Legends[2] = [
                        String(Math.floor(summsMTM['curi_1'] / 100000000) / 10) + '<br><font color="#' + ((summsMTM['curi_1'] < summsMTM['curi_2']) ? 'c000' : '008') + '000">' + String(((summsMTM['curi_2'] > 0) ? Math.floor(((summsMTM['curi_1'] - summsMTM['curi_2']) / summsMTM['curi_2']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsMTM['curi'] / 100000000) / 10) + '<br><font color="#' + ((summsMTM['curi'] < summsMTM['curi_1']) ? 'c000' : '008') + '000">' + String(((summsMTM['curi_1'] > 0) ? Math.floor(((summsMTM['curi'] - summsMTM['curi_1']) / summsMTM['curi_1']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsYTD['curi_1'] / 100000000) / 10) + '<br><font color="#' + ((summsYTD['curi_1'] < summsYTD['curi_2']) ? 'c000' : '008') + '000">' + String(((summsYTD['curi_2'] > 0) ? Math.floor(((summsYTD['curi_1'] - summsYTD['curi_2']) / summsYTD['curi_2']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsYTD['curi'] / 100000000) / 10) + '<br><font color="#' + ((summsYTD['curi'] < summsYTD['curi_1']) ? 'c000' : '008') + '000">' + String(((summsYTD['curi_1'] > 0) ? Math.floor(((summsYTD['curi'] - summsYTD['curi_1']) / summsYTD['curi_1']) * 1000) / 10 : 100.0)) + '%</font>',
                        'Bln.RUB<br>+/- % SPLY',
                        String(Math.floor(summsM[13]['curi'] / 100000000) / 10) + '<br><font color="#' + ((summsM[13]['curi'] < summsM[25]['curi']) ? 'c000' : '008') + '000">' + String(((summsM[25]['curi'] > 0) ? Math.floor(((summsM[13]['curi'] - summsM[25]['curi']) / summsM[25]['curi']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsM[12]['curi'] / 100000000) / 10) + '<br><font color="#' + ((summsM[12]['curi'] < summsM[24]['curi']) ? 'c000' : '008') + '000">' + String(((summsM[24]['curi'] > 0) ? Math.floor(((summsM[12]['curi'] - summsM[24]['curi']) / summsM[24]['curi']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsM[11]['curi'] / 100000000) / 10) + '<br><font color="#' + ((summsM[11]['curi'] < summsM[23]['curi']) ? 'c000' : '008') + '000">' + String(((summsM[23]['curi'] > 0) ? Math.floor(((summsM[11]['curi'] - summsM[23]['curi']) / summsM[23]['curi']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsM[10]['curi'] / 100000000) / 10) + '<br><font color="#' + ((summsM[10]['curi'] < summsM[22]['curi']) ? 'c000' : '008') + '000">' + String(((summsM[22]['curi'] > 0) ? Math.floor(((summsM[10]['curi'] - summsM[22]['curi']) / summsM[22]['curi']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsM[9]['curi'] / 100000000) / 10) + '<br><font color="#' + ((summsM[9]['curi'] < summsM[21]['curi']) ? 'c000' : '008') + '000">' + String(((summsM[21]['curi'] > 0) ? Math.floor(((summsM[9]['curi'] - summsM[21]['curi']) / summsM[21]['curi']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsM[8]['curi'] / 100000000) / 10) + '<br><font color="#' + ((summsM[8]['curi'] < summsM[20]['curi']) ? 'c000' : '008') + '000">' + String(((summsM[20]['curi'] > 0) ? Math.floor(((summsM[8]['curi'] - summsM[20]['curi']) / summsM[20]['curi']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsM[7]['curi'] / 100000000) / 10) + '<br><font color="#' + ((summsM[7]['curi'] < summsM[19]['curi']) ? 'c000' : '008') + '000">' + String(((summsM[19]['curi'] > 0) ? Math.floor(((summsM[7]['curi'] - summsM[19]['curi']) / summsM[19]['curi']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsM[6]['curi'] / 100000000) / 10) + '<br><font color="#' + ((summsM[6]['curi'] < summsM[18]['curi']) ? 'c000' : '008') + '000">' + String(((summsM[18]['curi'] > 0) ? Math.floor(((summsM[6]['curi'] - summsM[18]['curi']) / summsM[18]['curi']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsM[5]['curi'] / 100000000) / 10) + '<br><font color="#' + ((summsM[5]['curi'] < summsM[17]['curi']) ? 'c000' : '008') + '000">' + String(((summsM[17]['curi'] > 0) ? Math.floor(((summsM[5]['curi'] - summsM[17]['curi']) / summsM[17]['curi']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsM[4]['curi'] / 100000000) / 10) + '<br><font color="#' + ((summsM[4]['curi'] < summsM[16]['curi']) ? 'c000' : '008') + '000">' + String(((summsM[16]['curi'] > 0) ? Math.floor(((summsM[4]['curi'] - summsM[16]['curi']) / summsM[16]['curi']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsM[3]['curi'] / 100000000) / 10) + '<br><font color="#' + ((summsM[3]['curi'] < summsM[15]['curi']) ? 'c000' : '008') + '000">' + String(((summsM[15]['curi'] > 0) ? Math.floor(((summsM[3]['curi'] - summsM[15]['curi']) / summsM[15]['curi']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsM[2]['curi'] / 100000000) / 10) + '<br><font color="#' + ((summsM[2]['curi'] < summsM[14]['curi']) ? 'c000' : '008') + '000">' + String(((summsM[14]['curi'] > 0) ? Math.floor(((summsM[2]['curi'] - summsM[14]['curi']) / summsM[14]['curi']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsM[1]['curi'] / 100000000) / 10) + '<br><font color="#' + ((summsM[1]['curi'] < summsM[13]['curi']) ? 'c000' : '008') + '000">' + String(((summsM[13]['curi'] > 0) ? Math.floor(((summsM[1]['curi'] - summsM[13]['curi']) / summsM[13]['curi']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsM[0]['curi'] / 100000000) / 10) + '<br><font color="#' + ((summsM[0]['curi'] < summsM[12]['curi']) ? 'c000' : '008') + '000">' + String(((summsM[12]['curi'] > 0) ? Math.floor(((summsM[0]['curi'] - summsM[12]['curi']) / summsM[12]['curi']) * 1000) / 10 : 100.0)) + '%</font>',
                    ];
                    window.Legends[4] = [
                        String(Math.floor(summsbrands5[window.MarkCodes[0]]['mtm1'] / 100000000) / 10) + '<br><font color="#' + ((summsbrands5[window.MarkCodes[0]]['mtm1'] < summsbrands5[window.MarkCodes[0]]['mtm2']) ? 'c000' : '008') + '000">' + String(((summsbrands5[window.MarkCodes[0]]['mtm2'] > 0) ? Math.floor(((summsbrands5[window.MarkCodes[0]]['mtm1'] - summsbrands5[window.MarkCodes[0]]['mtm2']) / summsbrands5[window.MarkCodes[0]]['mtm2']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsbrands5[window.MarkCodes[0]]['mtm'] / 100000000) / 10) + '<br><font color="#' + ((summsbrands5[window.MarkCodes[0]]['mtm'] < summsbrands5[window.MarkCodes[0]]['mtm1']) ? 'c000' : '008') + '000">' + String(((summsbrands5[window.MarkCodes[0]]['mtm1'] > 0) ? Math.floor(((summsbrands5[window.MarkCodes[0]]['mtm'] - summsbrands5[window.MarkCodes[0]]['mtm1']) / summsbrands5[window.MarkCodes[0]]['mtm1']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsbrands5[window.MarkCodes[0]]['ytd1'] / 100000000) / 10) + '<br><font color="#' + ((summsbrands5[window.MarkCodes[0]]['ytd1'] < summsbrands5[window.MarkCodes[0]]['ytd2']) ? 'c000' : '008') + '000">' + String(((summsbrands5[window.MarkCodes[0]]['ytd2'] > 0) ? Math.floor(((summsbrands5[window.MarkCodes[0]]['ytd1'] - summsbrands5[window.MarkCodes[0]]['ytd2']) / summsbrands5[window.MarkCodes[0]]['ytd2']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsbrands5[window.MarkCodes[0]]['ytd'] / 100000000) / 10) + '<br><font color="#' + ((summsbrands5[window.MarkCodes[0]]['ytd'] < summsbrands5[window.MarkCodes[0]]['ytd1']) ? 'c000' : '008') + '000">' + String(((summsbrands5[window.MarkCodes[0]]['ytd1'] > 0) ? Math.floor(((summsbrands5[window.MarkCodes[0]]['ytd'] - summsbrands5[window.MarkCodes[0]]['ytd1']) / summsbrands5[window.MarkCodes[0]]['ytd1']) * 1000) / 10 : 100.0)) + '%</font>',
                        'Bln.RUB<br>+/- % SPLY',
                        String(Math.floor(summsbrands5[window.MarkCodes[0]]['q13'] / 100000000) / 10) + '<br><font color="#' + ((summsbrands5[window.MarkCodes[0]]['q13'] < summsbrands5[window.MarkCodes[0]]['q25']) ? 'c000' : '008') + '000">' + String(((summsbrands5[window.MarkCodes[0]]['q25'] > 0) ? Math.floor(((summsbrands5[window.MarkCodes[0]]['q13'] - summsbrands5[window.MarkCodes[0]]['q25']) / summsbrands5[window.MarkCodes[0]]['q25']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsbrands5[window.MarkCodes[0]]['q12'] / 100000000) / 10) + '<br><font color="#' + ((summsbrands5[window.MarkCodes[0]]['q12'] < summsbrands5[window.MarkCodes[0]]['q24']) ? 'c000' : '008') + '000">' + String(((summsbrands5[window.MarkCodes[0]]['q24'] > 0) ? Math.floor(((summsbrands5[window.MarkCodes[0]]['q12'] - summsbrands5[window.MarkCodes[0]]['q24']) / summsbrands5[window.MarkCodes[0]]['q24']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsbrands5[window.MarkCodes[0]]['q11'] / 100000000) / 10) + '<br><font color="#' + ((summsbrands5[window.MarkCodes[0]]['q11'] < summsbrands5[window.MarkCodes[0]]['q23']) ? 'c000' : '008') + '000">' + String(((summsbrands5[window.MarkCodes[0]]['q23'] > 0) ? Math.floor(((summsbrands5[window.MarkCodes[0]]['q11'] - summsbrands5[window.MarkCodes[0]]['q23']) / summsbrands5[window.MarkCodes[0]]['q23']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsbrands5[window.MarkCodes[0]]['q10'] / 100000000) / 10) + '<br><font color="#' + ((summsbrands5[window.MarkCodes[0]]['q10'] < summsbrands5[window.MarkCodes[0]]['q22']) ? 'c000' : '008') + '000">' + String(((summsbrands5[window.MarkCodes[0]]['q22'] > 0) ? Math.floor(((summsbrands5[window.MarkCodes[0]]['q10'] - summsbrands5[window.MarkCodes[0]]['q22']) / summsbrands5[window.MarkCodes[0]]['q22']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsbrands5[window.MarkCodes[0]]['q9'] / 100000000) / 10) + '<br><font color="#' + ((summsbrands5[window.MarkCodes[0]]['q9'] < summsbrands5[window.MarkCodes[0]]['q21']) ? 'c000' : '008') + '000">' + String(((summsbrands5[window.MarkCodes[0]]['q21'] > 0) ? Math.floor(((summsbrands5[window.MarkCodes[0]]['q9'] - summsbrands5[window.MarkCodes[0]]['q21']) / summsbrands5[window.MarkCodes[0]]['q21']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsbrands5[window.MarkCodes[0]]['q8'] / 100000000) / 10) + '<br><font color="#' + ((summsbrands5[window.MarkCodes[0]]['q8'] < summsbrands5[window.MarkCodes[0]]['q20']) ? 'c000' : '008') + '000">' + String(((summsbrands5[window.MarkCodes[0]]['q20'] > 0) ? Math.floor(((summsbrands5[window.MarkCodes[0]]['q8'] - summsbrands5[window.MarkCodes[0]]['q20']) / summsbrands5[window.MarkCodes[0]]['q20']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsbrands5[window.MarkCodes[0]]['q7'] / 100000000) / 10) + '<br><font color="#' + ((summsbrands5[window.MarkCodes[0]]['q7'] < summsbrands5[window.MarkCodes[0]]['q19']) ? 'c000' : '008') + '000">' + String(((summsbrands5[window.MarkCodes[0]]['q19'] > 0) ? Math.floor(((summsbrands5[window.MarkCodes[0]]['q7'] - summsbrands5[window.MarkCodes[0]]['q19']) / summsbrands5[window.MarkCodes[0]]['q19']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsbrands5[window.MarkCodes[0]]['q6'] / 100000000) / 10) + '<br><font color="#' + ((summsbrands5[window.MarkCodes[0]]['q6'] < summsbrands5[window.MarkCodes[0]]['q18']) ? 'c000' : '008') + '000">' + String(((summsbrands5[window.MarkCodes[0]]['q18'] > 0) ? Math.floor(((summsbrands5[window.MarkCodes[0]]['q6'] - summsbrands5[window.MarkCodes[0]]['q18']) / summsbrands5[window.MarkCodes[0]]['q18']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsbrands5[window.MarkCodes[0]]['q5'] / 100000000) / 10) + '<br><font color="#' + ((summsbrands5[window.MarkCodes[0]]['q5'] < summsbrands5[window.MarkCodes[0]]['q17']) ? 'c000' : '008') + '000">' + String(((summsbrands5[window.MarkCodes[0]]['q17'] > 0) ? Math.floor(((summsbrands5[window.MarkCodes[0]]['q5'] - summsbrands5[window.MarkCodes[0]]['q17']) / summsbrands5[window.MarkCodes[0]]['q17']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsbrands5[window.MarkCodes[0]]['q4'] / 100000000) / 10) + '<br><font color="#' + ((summsbrands5[window.MarkCodes[0]]['q4'] < summsbrands5[window.MarkCodes[0]]['q16']) ? 'c000' : '008') + '000">' + String(((summsbrands5[window.MarkCodes[0]]['q16'] > 0) ? Math.floor(((summsbrands5[window.MarkCodes[0]]['q4'] - summsbrands5[window.MarkCodes[0]]['q16']) / summsbrands5[window.MarkCodes[0]]['q16']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsbrands5[window.MarkCodes[0]]['q3'] / 100000000) / 10) + '<br><font color="#' + ((summsbrands5[window.MarkCodes[0]]['q3'] < summsbrands5[window.MarkCodes[0]]['q15']) ? 'c000' : '008') + '000">' + String(((summsbrands5[window.MarkCodes[0]]['q15'] > 0) ? Math.floor(((summsbrands5[window.MarkCodes[0]]['q3'] - summsbrands5[window.MarkCodes[0]]['q15']) / summsbrands5[window.MarkCodes[0]]['q15']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsbrands5[window.MarkCodes[0]]['q2'] / 100000000) / 10) + '<br><font color="#' + ((summsbrands5[window.MarkCodes[0]]['q2'] < summsbrands5[window.MarkCodes[0]]['q14']) ? 'c000' : '008') + '000">' + String(((summsbrands5[window.MarkCodes[0]]['q14'] > 0) ? Math.floor(((summsbrands5[window.MarkCodes[0]]['q2'] - summsbrands5[window.MarkCodes[0]]['q14']) / summsbrands5[window.MarkCodes[0]]['q14']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsbrands5[window.MarkCodes[0]]['q1'] / 100000000) / 10) + '<br><font color="#' + ((summsbrands5[window.MarkCodes[0]]['q1'] < summsbrands5[window.MarkCodes[0]]['q13']) ? 'c000' : '008') + '000">' + String(((summsbrands5[window.MarkCodes[0]]['q13'] > 0) ? Math.floor(((summsbrands5[window.MarkCodes[0]]['q1'] - summsbrands5[window.MarkCodes[0]]['q13']) / summsbrands5[window.MarkCodes[0]]['q13']) * 1000) / 10 : 100.0)) + '%</font>',
                        String(Math.floor(summsbrands5[window.MarkCodes[0]]['q0'] / 100000000) / 10) + '<br><font color="#' + ((summsbrands5[window.MarkCodes[0]]['q0'] < summsbrands5[window.MarkCodes[0]]['q12']) ? 'c000' : '008') + '000">' + String(((summsbrands5[window.MarkCodes[0]]['q12'] > 0) ? Math.floor(((summsbrands5[window.MarkCodes[0]]['q0'] - summsbrands5[window.MarkCodes[0]]['q12']) / summsbrands5[window.MarkCodes[0]]['q12']) * 1000) / 10 : 100.0)) + '%</font>',
                    ];
                    window.Legends[1] = window.Legends[0];
                    window.Legends[3] = window.Legends[2];
                }
            } else {
                window.ChartData1 = [{
                    labels: ['Oct’18-Sep’19', 'Oct’19-Sep’20'],
                    datasets: [{
                        label: 'Other',
                        backgroundColor: 'rgba(127, 127, 127, 1)',
                        data: [8.9, 7.7, ],
                        tooltipsValues: [8.9, 7.7, ],
                    }, {
                        label: 'ESAOTE',
                        backgroundColor: 'rgba(127, 127, 0, 1)',
                        data: [3.6, 1.6, ],
                        tooltipsValues: [3.6, 1.6, ],
                    }, {
                        label: 'SONOSCAPE',
                        backgroundColor: 'rgba(92, 185, 187, 1)',
                        data: [5.4, 3.0, ],
                        tooltipsValues: [5.4, 3.0, ],
                    }, {
                        label: 'HITACHI',
                        backgroundColor: 'rgba(255, 255, 0, 1)',
                        data: [5.9, 4.7, ],
                        tooltipsValues: [5.9, 4.7, ],
                    }, {
                        label: 'MINDRAY',
                        backgroundColor: 'rgba(167, 122, 215, 1)',
                        data: [7.1, 6.2, ],
                        tooltipsValues: [7.1, 6.2, ],
                    }, {
                        label: 'SIEMENS',
                        backgroundColor: 'rgba(255, 127, 0, 1)',
                        data: [6.1, 6.1, ],
                        tooltipsValues: [6.1, 6.1, ],
                    }, {
                        label: 'SAMSUNG',
                        backgroundColor: 'rgba(240, 66, 121, 1)',
                        data: [14.3, 13.1, ],
                        tooltipsValues: [14.3, 13.1, ],
                    }, {
                        label: 'GE',
                        backgroundColor: 'rgba(0, 156, 73, 1)',
                        data: [28.7, 38.0, ],
                        tooltipsValues: [28.7, 38.0, ],
                    }, {
                        label: 'ФИЛИПС',
                        backgroundColor: 'rgba(0, 114, 218, 1)',
                        data: [20.0, 19.7, ],
                        tooltipsValues: [20.0, 19.7, ],
                    }, ],
                }, {
                    labels: ['Oct’18-Sep’19', 'Oct’19-Sep’20'],
                    datasets: [{
                        label: 'Indirect',
                        backgroundColor: 'rgba(237, 125, 49, 1)',
                        data: [21.1, 20.1, ],
                        tooltipsValues: [21.1, 20.1, ],
                    }, {
                        label: 'Direct',
                        backgroundColor: 'rgba(84, 150, 211, 1)',
                        data: [0.7, 6.7, ],
                        tooltipsValues: [0.7, 6.7, ],
                    }, ],
                }, {
                    labels: ['Oct’18-Sep’19', 'Oct’19-Sep’20'],
                    datasets: [{
                        label: 'Other',
                        backgroundColor: 'rgba(127, 127, 127, 1)',
                        data: [9.1, 10.3, ],
                        tooltipsValues: [9.1, 10.3, ],
                    }, {
                        label: 'ESAOTE',
                        backgroundColor: 'rgba(127, 127, 0, 1)',
                        data: [3.7, 2.1, ],
                        tooltipsValues: [3.7, 2.1, ],
                    }, {
                        label: 'SONOSCAPE',
                        backgroundColor: 'rgba(92, 185, 187, 1)',
                        data: [5.6, 4.1, ],
                        tooltipsValues: [5.6, 4.1, ],
                    }, {
                        label: 'HITACHI',
                        backgroundColor: 'rgba(255, 255, 0, 1)',
                        data: [6.0, 6.3, ],
                        tooltipsValues: [6.0, 6.3, ],
                    }, {
                        label: 'MINDRAY',
                        backgroundColor: 'rgba(167, 122, 215, 1)',
                        data: [7.3, 8.2, ],
                        tooltipsValues: [7.3, 8.2, ],
                    }, {
                        label: 'SIEMENS',
                        backgroundColor: 'rgba(255, 127, 0, 1)',
                        data: [4.9, 2.3, ],
                        tooltipsValues: [4.9, 2.3, ],
                    }, {
                        label: 'SAMSUNG',
                        backgroundColor: 'rgba(240, 66, 121, 1)',
                        data: [14.7, 17.6, ],
                        tooltipsValues: [14.7, 17.6, ],
                    }, {
                        label: 'GE',
                        backgroundColor: 'rgba(0, 156, 73, 1)',
                        data: [29.5, 30.8, ],
                        tooltipsValues: [29.5, 30.8, ],
                    }, {
                        label: 'ФИЛИПС',
                        backgroundColor: 'rgba(0, 114, 218, 1)',
                        data: [19.3, 18.2, ],
                        tooltipsValues: [19.3, 18.2, ],
                    }, ],
                }, {
                    labels: ['Oct’18-Sep’19', 'Oct’19-Sep’20'],
                    datasets: [{
                        label: '~',
                        backgroundColor: 'rgba(90, 155, 213, 1)',
                        data: [2.3, 3.7, ],
                        tooltipsValues: [2.3, 3.7, ],
                    }, {
                        label: 'Handheld',
                        backgroundColor: 'rgba(238, 125, 49, 1)',
                        data: [0.1, 1.1, ],
                        tooltipsValues: [0.1, 1.1, ],
                    }, {
                        label: 'Low-end compact 0-4M',
                        backgroundColor: 'rgba(165, 165, 165, 1)',
                        data: [16.5, 19.7, ],
                        tooltipsValues: [16.5, 19.7, ],
                    }, {
                        label: 'High-end compact 4M+',
                        backgroundColor: 'rgba(255, 192, 0, 1)',
                        data: [8.9, 10.8, ],
                        tooltipsValues: [8.9, 10.8, ],
                    }, {
                        label: 'Low-end cart 0-4M',
                        backgroundColor: 'rgba(68, 115, 197, 1)',
                        data: [11.3, 6.4, ],
                        tooltipsValues: [11.3, 6.4, ],
                    }, {
                        label: 'High-end cart 4-8M',
                        backgroundColor: 'rgba(112, 173, 70, 1)',
                        data: [26.4, 24.3, ],
                        tooltipsValues: [26.4, 24.3, ],
                    }, {
                        label: 'Premium cart 8M+',
                        backgroundColor: 'rgba(7, 93, 144, 1)',
                        data: [34.5, 34.0, ],
                        tooltipsValues: [34.5, 34.0, ],
                    }, ],
                }, {
                    labels: ['Oct’18-Sep’19', 'Oct’19-Sep’20'],
                    datasets: [{
                        label: '~',
                        backgroundColor: 'rgba(90, 155, 213, 1)',
                        data: [0.0, 0.1, ],
                        tooltipsValues: [0.0, 0.1, ],
                    }, {
                        label: 'Handheld',
                        backgroundColor: 'rgba(238, 125, 49, 1)',
                        data: [0.0, 0.0, ],
                        tooltipsValues: [0.0, 0.0, ],
                    }, {
                        label: 'Low-end compact 0-4M',
                        backgroundColor: 'rgba(165, 165, 165, 1)',
                        data: [3.6, 5.3, ],
                        tooltipsValues: [3.6, 5.3, ],
                    }, {
                        label: 'High-end compact 4M+',
                        backgroundColor: 'rgba(255, 192, 0, 1)',
                        data: [2.1, 1.8, ],
                        tooltipsValues: [2.1, 1.8, ],
                    }, {
                        label: 'Low-end cart 0-4M',
                        backgroundColor: 'rgba(68, 115, 197, 1)',
                        data: [2.2, 1.2, ],
                        tooltipsValues: [2.2, 1.2, ],
                    }, {
                        label: 'High-end cart 4-8M',
                        backgroundColor: 'rgba(112, 173, 70, 1)',
                        data: [3.1, 5.2, ],
                        tooltipsValues: [3.1, 5.2, ],
                    }, {
                        label: 'Premium cart 8M+',
                        backgroundColor: 'rgba(7, 93, 144, 1)',
                        data: [3.8, 4.1, ],
                        tooltipsValues: [3.8, 4.1, ],
                    }, ],
                }, ];
                window.PercData1 = [
                    [97, 75],
                    [3, 25],
                ];
                window.ChartData2 = [{
                    labels: ['YTD’19 Sep', 'YTD’20 Sep'],
                    datasets: [{
                        label: 'Other',
                        backgroundColor: 'rgba(127, 127, 127, 1)',
                        data: [9.1, 6.3, ],
                        tooltipsValues: [9.1, 6.3, ],
                    }, {
                        label: 'ESAOTE',
                        backgroundColor: 'rgba(127, 127, 0, 1)',
                        data: [4.3, 1.5, ],
                        tooltipsValues: [4.3, 1.5, ],
                    }, {
                        label: 'SONOSCAPE',
                        backgroundColor: 'rgba(92, 185, 187, 1)',
                        data: [4.3, 2.8, ],
                        tooltipsValues: [4.3, 2.8, ],
                    }, {
                        label: 'HITACHI',
                        backgroundColor: 'rgba(255, 255, 0, 1)',
                        data: [6.1, 3.7, ],
                        tooltipsValues: [6.1, 3.7, ],
                    }, {
                        label: 'MINDRAY',
                        backgroundColor: 'rgba(167, 122, 215, 1)',
                        data: [6.6, 5.7, ],
                        tooltipsValues: [6.6, 5.7, ],
                    }, {
                        label: 'SIEMENS',
                        backgroundColor: 'rgba(255, 127, 0, 1)',
                        data: [5.8, 7.3, ],
                        tooltipsValues: [5.8, 7.3, ],
                    }, {
                        label: 'SAMSUNG',
                        backgroundColor: 'rgba(240, 66, 121, 1)',
                        data: [13.8, 13.7, ],
                        tooltipsValues: [13.8, 13.7, ],
                    }, {
                        label: 'GE',
                        backgroundColor: 'rgba(0, 156, 73, 1)',
                        data: [28.9, 39.6, ],
                        tooltipsValues: [28.9, 39.6, ],
                    }, {
                        label: 'ФИЛИПС',
                        backgroundColor: 'rgba(0, 114, 218, 1)',
                        data: [21.1, 19.4, ],
                        tooltipsValues: [21.1, 19.4, ],
                    }, ],
                }, {
                    labels: ['YTD’19 Sep', 'YTD’20 Sep'],
                    datasets: [{
                        label: 'Indirect',
                        backgroundColor: 'rgba(237, 125, 49, 1)',
                        data: [14.8, 15.2, ],
                        tooltipsValues: [14.8, 15.2, ],
                    }, {
                        label: 'Direct',
                        backgroundColor: 'rgba(84, 150, 211, 1)',
                        data: [0.6, 6.8, ],
                        tooltipsValues: [0.6, 6.8, ],
                    }, ],
                }, {
                    labels: ['YTD’19 Sep', 'YTD’20 Sep'],
                    datasets: [{
                        label: 'Other',
                        backgroundColor: 'rgba(127, 127, 127, 1)',
                        data: [9.4, 9.1, ],
                        tooltipsValues: [9.4, 9.1, ],
                    }, {
                        label: 'ESAOTE',
                        backgroundColor: 'rgba(127, 127, 0, 1)',
                        data: [4.5, 2.2, ],
                        tooltipsValues: [4.5, 2.2, ],
                    }, {
                        label: 'SONOSCAPE',
                        backgroundColor: 'rgba(92, 185, 187, 1)',
                        data: [4.4, 4.1, ],
                        tooltipsValues: [4.4, 4.1, ],
                    }, {
                        label: 'HITACHI',
                        backgroundColor: 'rgba(255, 255, 0, 1)',
                        data: [6.3, 5.4, ],
                        tooltipsValues: [6.3, 5.4, ],
                    }, {
                        label: 'MINDRAY',
                        backgroundColor: 'rgba(167, 122, 215, 1)',
                        data: [6.8, 8.2, ],
                        tooltipsValues: [6.8, 8.2, ],
                    }, {
                        label: 'SIEMENS',
                        backgroundColor: 'rgba(255, 127, 0, 1)',
                        data: [4.1, 2.9, ],
                        tooltipsValues: [4.1, 2.9, ],
                    }, {
                        label: 'SAMSUNG',
                        backgroundColor: 'rgba(240, 66, 121, 1)',
                        data: [14.3, 19.8, ],
                        tooltipsValues: [14.3, 19.8, ],
                    }, {
                        label: 'GE',
                        backgroundColor: 'rgba(0, 156, 73, 1)',
                        data: [30.0, 30.9, ],
                        tooltipsValues: [30.0, 30.9, ],
                    }, {
                        label: 'ФИЛИПС',
                        backgroundColor: 'rgba(0, 114, 218, 1)',
                        data: [20.1, 17.4, ],
                        tooltipsValues: [20.1, 17.4, ],
                    }, ],
                }, {
                    labels: ['YTD’19 Sep', 'YTD’20 Sep'],
                    datasets: [{
                        label: '~',
                        backgroundColor: 'rgba(90, 155, 213, 1)',
                        data: [1.5, 2.9, ],
                        tooltipsValues: [1.5, 2.9, ],
                    }, {
                        label: 'Handheld',
                        backgroundColor: 'rgba(238, 125, 49, 1)',
                        data: [0.1, 1.2, ],
                        tooltipsValues: [0.1, 1.2, ],
                    }, {
                        label: 'Low-end compact 0-4M',
                        backgroundColor: 'rgba(165, 165, 165, 1)',
                        data: [13.9, 21.5, ],
                        tooltipsValues: [13.9, 21.5, ],
                    }, {
                        label: 'High-end compact 4M+',
                        backgroundColor: 'rgba(255, 192, 0, 1)',
                        data: [10.4, 11.2, ],
                        tooltipsValues: [10.4, 11.2, ],
                    }, {
                        label: 'Low-end cart 0-4M',
                        backgroundColor: 'rgba(68, 115, 197, 1)',
                        data: [11.2, 5.7, ],
                        tooltipsValues: [11.2, 5.7, ],
                    }, {
                        label: 'High-end cart 4-8M',
                        backgroundColor: 'rgba(112, 173, 70, 1)',
                        data: [26.3, 23.1, ],
                        tooltipsValues: [26.3, 23.1, ],
                    }, {
                        label: 'Premium cart 8M+',
                        backgroundColor: 'rgba(7, 93, 144, 1)',
                        data: [36.6, 34.4, ],
                        tooltipsValues: [36.6, 34.4, ],
                    }, ],
                }, {
                    labels: ['YTD’19 Sep', 'YTD’20 Sep'],
                    datasets: [{
                        label: '~',
                        backgroundColor: 'rgba(90, 155, 213, 1)',
                        data: [0.0, 0.1, ],
                        tooltipsValues: [0.0, 0.1, ],
                    }, {
                        label: 'Handheld',
                        backgroundColor: 'rgba(238, 125, 49, 1)',
                        data: [0.0, 0.0, ],
                        tooltipsValues: [0.0, 0.0, ],
                    }, {
                        label: 'Low-end compact 0-4M',
                        backgroundColor: 'rgba(165, 165, 165, 1)',
                        data: [2.5, 6.4, ],
                        tooltipsValues: [2.5, 6.4, ],
                    }, {
                        label: 'High-end compact 4M+',
                        backgroundColor: 'rgba(255, 192, 0, 1)',
                        data: [2.5, 2.2, ],
                        tooltipsValues: [2.5, 2.2, ],
                    }, {
                        label: 'Low-end cart 0-4M',
                        backgroundColor: 'rgba(68, 115, 197, 1)',
                        data: [2.1, 1.0, ],
                        tooltipsValues: [2.1, 1.0, ],
                    }, {
                        label: 'High-end cart 4-8M',
                        backgroundColor: 'rgba(112, 173, 70, 1)',
                        data: [2.9, 5.5, ],
                        tooltipsValues: [2.9, 5.5, ],
                    }, {
                        label: 'Premium cart 8M+',
                        backgroundColor: 'rgba(7, 93, 144, 1)',
                        data: [4.4, 4.7, ],
                        tooltipsValues: [4.4, 4.7, ],
                    }, ],
                }, ];
                window.PercData2 = [
                    [96, 69],
                    [4, 31],
                ];
                window.ChartData3 = [
                    [{
                        label: 'ФИЛИПС',
                        backgroundColor: 'rgba(0, 114, 218, 1)',
                    }, {
                        label: 'GE',
                        backgroundColor: 'rgba(0, 156, 73, 1)',
                    }, {
                        label: 'SAMSUNG',
                        backgroundColor: 'rgba(240, 66, 121, 1)',
                    }, {
                        label: 'SIEMENS',
                        backgroundColor: 'rgba(255, 127, 0, 1)',
                    }, {
                        label: 'MINDRAY',
                        backgroundColor: 'rgba(167, 122, 215, 1)',
                    }, {
                        label: 'HITACHI',
                        backgroundColor: 'rgba(255, 255, 0, 1)',
                    }, {
                        label: 'SONOSCAPE',
                        backgroundColor: 'rgba(92, 185, 187, 1)',
                    }, {
                        label: 'ESAOTE',
                        backgroundColor: 'rgba(127, 127, 0, 1)',
                    }, {
                        label: 'Other',
                        backgroundColor: 'rgba(127, 127, 127, 1)',
                    }, ],
                    [{
                        label: 'Direct',
                        backgroundColor: 'rgba(84, 150, 211, 1)',
                    }, {
                        label: 'Indirect',
                        backgroundColor: 'rgba(237, 125, 49, 1)',
                    }, ],
                    [{
                        label: 'ФИЛИПС',
                        backgroundColor: 'rgba(0, 114, 218, 1)',
                    }, {
                        label: 'GE',
                        backgroundColor: 'rgba(0, 156, 73, 1)',
                    }, {
                        label: 'SAMSUNG',
                        backgroundColor: 'rgba(240, 66, 121, 1)',
                    }, {
                        label: 'SIEMENS',
                        backgroundColor: 'rgba(255, 127, 0, 1)',
                    }, {
                        label: 'MINDRAY',
                        backgroundColor: 'rgba(167, 122, 215, 1)',
                    }, {
                        label: 'HITACHI',
                        backgroundColor: 'rgba(255, 255, 0, 1)',
                    }, {
                        label: 'SONOSCAPE',
                        backgroundColor: 'rgba(92, 185, 187, 1)',
                    }, {
                        label: 'ESAOTE',
                        backgroundColor: 'rgba(127, 127, 0, 1)',
                    }, {
                        label: 'Other',
                        backgroundColor: 'rgba(127, 127, 127, 1)',
                    }, ],
                    [{
                        label: 'Premium cart 8M+',
                        backgroundColor: 'rgba(7, 93, 144, 1)',
                    }, {
                        label: 'High-end cart 4-8M',
                        backgroundColor: 'rgba(112, 173, 70, 1)',
                    }, {
                        label: 'Low-end cart 0-4M',
                        backgroundColor: 'rgba(68, 115, 197, 1)',
                    }, {
                        label: 'High-end compact 4M+',
                        backgroundColor: 'rgba(255, 192, 0, 1)',
                    }, {
                        label: 'Low-end compact 0-4M',
                        backgroundColor: 'rgba(165, 165, 165, 1)',
                    }, {
                        label: 'Handheld',
                        backgroundColor: 'rgba(238, 125, 49, 1)',
                    }, {
                        label: '~',
                        backgroundColor: 'rgba(90, 155, 213, 1)',
                    }, ],
                    [{
                        label: 'Premium cart 8M+',
                        backgroundColor: 'rgba(7, 93, 144, 1)',
                    }, {
                        label: 'High-end cart 4-8M',
                        backgroundColor: 'rgba(112, 173, 70, 1)',
                    }, {
                        label: 'Low-end cart 0-4M',
                        backgroundColor: 'rgba(68, 115, 197, 1)',
                    }, {
                        label: 'High-end compact 4M+',
                        backgroundColor: 'rgba(255, 192, 0, 1)',
                    }, {
                        label: 'Low-end compact 0-4M',
                        backgroundColor: 'rgba(165, 165, 165, 1)',
                    }, {
                        label: 'Handheld',
                        backgroundColor: 'rgba(238, 125, 49, 1)',
                    }, {
                        label: '~',
                        backgroundColor: 'rgba(90, 155, 213, 1)',
                    }, ],
                ];
                window.ChartData4 = [{
                    labels: ['2019 Q1', '2019 Q2', '2019 Q3', '2019 Q4', '2020 Q1', '2020 Q2', '2020 Q3', ],
                    datasets: [{
                        label: 'Other',
                        backgroundColor: 'rgba(127, 127, 127, 1)',
                        data: [14.9, 8.9, 8.5, 14.2, 10.2, 7.6, 4.0, ],
                        tooltipsValues: [14.9, 8.9, 8.5, 14.2, 10.2, 7.6, 4.0, ],
                    }, {
                        label: 'ESAOTE',
                        backgroundColor: 'rgba(127, 127, 0, 1)',
                        data: [0.2, 5.4, 4.2, 1.9, 2.9, 1.9, 0.7, ],
                        tooltipsValues: [0.2, 5.4, 4.2, 1.9, 2.9, 1.9, 0.7, ],
                    }, {
                        label: 'SONOSCAPE',
                        backgroundColor: 'rgba(92, 185, 187, 1)',
                        data: [1.7, 3.3, 5.2, 4.0, 6.0, 3.7, 1.1, ],
                        tooltipsValues: [1.7, 3.3, 5.2, 4.0, 6.0, 3.7, 1.1, ],
                    }, {
                        label: 'HITACHI',
                        backgroundColor: 'rgba(255, 255, 0, 1)',
                        data: [1.4, 6.0, 6.8, 9.1, 9.2, 2.8, 3.2, ],
                        tooltipsValues: [1.4, 6.0, 6.8, 9.1, 9.2, 2.8, 3.2, ],
                    }, {
                        label: 'MINDRAY',
                        backgroundColor: 'rgba(167, 122, 215, 1)',
                        data: [7.3, 7.5, 5.9, 8.4, 5.4, 6.9, 4.4, ],
                        tooltipsValues: [7.3, 7.5, 5.9, 8.4, 5.4, 6.9, 4.4, ],
                    }, {
                        label: 'SIEMENS',
                        backgroundColor: 'rgba(255, 127, 0, 1)',
                        data: [13.3, 2.2, 7.1, 0.5, 2.8, 3.3, 12.7, ],
                        tooltipsValues: [13.3, 2.2, 7.1, 0.5, 2.8, 3.3, 12.7, ],
                    }, {
                        label: 'SAMSUNG',
                        backgroundColor: 'rgba(240, 66, 121, 1)',
                        data: [7.5, 14.5, 14.2, 10.6, 12.8, 17.2, 10.3, ],
                        tooltipsValues: [7.5, 14.5, 14.2, 10.6, 12.8, 17.2, 10.3, ],
                    }, {
                        label: 'GE',
                        backgroundColor: 'rgba(0, 156, 73, 1)',
                        data: [28.6, 37.1, 23.9, 30.4, 33.5, 42.9, 37.7, ],
                        tooltipsValues: [28.6, 37.1, 23.9, 30.4, 33.5, 42.9, 37.7, ],
                    }, {
                        label: 'ФИЛИПС',
                        backgroundColor: 'rgba(0, 114, 218, 1)',
                        data: [25.1, 15.1, 24.3, 20.8, 17.3, 13.8, 25.9, ],
                        tooltipsValues: [25.1, 15.1, 24.3, 20.8, 17.3, 13.8, 25.9, ],
                    }, ],
                }, {
                    labels: ['2019 Q1', '2019 Q2', '2019 Q3', '2019 Q4', '2020 Q1', '2020 Q2', '2020 Q3', ],
                    datasets: [{
                        label: 'Indirect',
                        backgroundColor: 'rgba(237, 125, 49, 1)',
                        data: [1.2, 5.4, 8.3, 4.75, 2.5, 8, 4.7, ],
                        tooltipsValues: [1.2, 5.4, 8.3, 4.75, 2.5, 8, 4.7, ],
                    }, {
                        label: 'Direct',
                        backgroundColor: 'rgba(84, 150, 211, 1)',
                        data: [0, 0, 0.5, 0.05, 0, 2, 4.8, ],
                        tooltipsValues: [0, 0, 0.5, 0.05, 0, 2, 4.8, ],
                    }, ],
                }, {
                    labels: ['2019 Q1', '2019 Q2', '2019 Q3', '2019 Q4', '2020 Q1', '2020 Q2', '2020 Q3', ],
                    datasets: [{
                        label: 'Other',
                        backgroundColor: 'rgba(127, 127, 127, 1)',
                        data: [14.9, 8.8, 9.0, 14.1, 10.2, 9.4, 8.1, ],
                        tooltipsValues: [14.9, 8.8, 9.0, 14.1, 10.2, 9.4, 8.1, ],
                    }, {
                        label: 'ESAOTE',
                        backgroundColor: 'rgba(127, 127, 0, 1)',
                        data: [0.2, 5.4, 4.5, 1.9, 2.9, 2.4, 1.4, ],
                        tooltipsValues: [0.2, 5.4, 4.5, 1.9, 2.9, 2.4, 1.4, ],
                    }, {
                        label: 'SONOSCAPE',
                        backgroundColor: 'rgba(92, 185, 187, 1)',
                        data: [1.7, 3.3, 5.5, 4.0, 6.0, 4.6, 2.2, ],
                        tooltipsValues: [1.7, 3.3, 5.5, 4.0, 6.0, 4.6, 2.2, ],
                    }, {
                        label: 'HITACHI',
                        backgroundColor: 'rgba(255, 255, 0, 1)',
                        data: [1.4, 6.0, 7.2, 9.1, 9.2, 3.5, 6.6, ],
                        tooltipsValues: [1.4, 6.0, 7.2, 9.1, 9.2, 3.5, 6.6, ],
                    }, {
                        label: 'MINDRAY',
                        backgroundColor: 'rgba(167, 122, 215, 1)',
                        data: [7.3, 7.5, 6.3, 8.4, 5.4, 8.6, 9.0, ],
                        tooltipsValues: [7.3, 7.5, 6.3, 8.4, 5.4, 8.6, 9.0, ],
                    }, {
                        label: 'SIEMENS',
                        backgroundColor: 'rgba(255, 127, 0, 1)',
                        data: [13.3, 2.2, 4.1, 0.5, 2.8, 4.1, 0.9, ],
                        tooltipsValues: [13.3, 2.2, 4.1, 0.5, 2.8, 4.1, 0.9, ],
                    }, {
                        label: 'SAMSUNG',
                        backgroundColor: 'rgba(240, 66, 121, 1)',
                        data: [7.5, 14.5, 15.1, 10.6, 12.8, 21.4, 20.9, ],
                        tooltipsValues: [7.5, 14.5, 15.1, 10.6, 12.8, 21.4, 20.9, ],
                    }, {
                        label: 'GE',
                        backgroundColor: 'rgba(0, 156, 73, 1)',
                        data: [28.6, 37.1, 25.6, 30.5, 33.5, 28.9, 33.0, ],
                        tooltipsValues: [28.6, 37.1, 25.6, 30.5, 33.5, 28.9, 33.0, ],
                    }, {
                        label: 'ФИЛИПС',
                        backgroundColor: 'rgba(0, 114, 218, 1)',
                        data: [25.1, 15.1, 22.6, 20.8, 17.3, 17.2, 17.9, ],
                        tooltipsValues: [25.1, 15.1, 22.6, 20.8, 17.3, 17.2, 17.9, ],
                    }, ],
                }, {
                    labels: ['2019 Q1', '2019 Q2', '2019 Q3', '2019 Q4', '2020 Q1', '2020 Q2', '2020 Q3', ],
                    datasets: [{
                        label: '~',
                        backgroundColor: 'rgba(90, 155, 213, 1)',
                        data: [0.1, 1.4, 1.7, 6.2, 1.0, 2.4, 4.9, ],
                        tooltipsValues: [0.1, 1.4, 1.7, 6.2, 1.0, 2.4, 4.9, ],
                    }, {
                        label: 'Handheld',
                        backgroundColor: 'rgba(238, 125, 49, 1)',
                        data: [0.0, 0.1, 0.1, 0.8, 0.1, 2.0, 0.2, ],
                        tooltipsValues: [0.0, 0.1, 0.1, 0.8, 0.1, 2.0, 0.2, ],
                    }, {
                        label: 'Low-end compact 0-4M',
                        backgroundColor: 'rgba(165, 165, 165, 1)',
                        data: [8.2, 17.4, 12.4, 14.0, 18.5, 25.8, 15.8, ],
                        tooltipsValues: [8.2, 17.4, 12.4, 14.0, 18.5, 25.8, 15.8, ],
                    }, {
                        label: 'High-end compact 4M+',
                        backgroundColor: 'rgba(255, 192, 0, 1)',
                        data: [5.4, 10.9, 10.8, 9.4, 10.6, 9.9, 13.8, ],
                        tooltipsValues: [5.4, 10.9, 10.8, 9.4, 10.6, 9.9, 13.8, ],
                    }, {
                        label: 'Low-end cart 0-4M',
                        backgroundColor: 'rgba(68, 115, 197, 1)',
                        data: [11.5, 7.9, 13.4, 8.5, 6.1, 7.3, 2.8, ],
                        tooltipsValues: [11.5, 7.9, 13.4, 8.5, 6.1, 7.3, 2.8, ],
                    }, {
                        label: 'High-end cart 4-8M',
                        backgroundColor: 'rgba(112, 173, 70, 1)',
                        data: [39.6, 27.8, 23.5, 28.2, 21.7, 23.4, 23.2, ],
                        tooltipsValues: [39.6, 27.8, 23.5, 28.2, 21.7, 23.4, 23.2, ],
                    }, {
                        label: 'Premium cart 8M+',
                        backgroundColor: 'rgba(7, 93, 144, 1)',
                        data: [35.2, 34.5, 38.1, 32.9, 42.0, 29.2, 39.3, ],
                        tooltipsValues: [35.2, 34.5, 38.1, 32.9, 42.0, 29.2, 39.3, ],
                    }, ],
                }, {
                    labels: ['2019 Q1', '2019 Q2', '2019 Q3', '2019 Q4', '2020 Q1', '2020 Q2', '2020 Q3', ],
                    datasets: [{
                        label: '~',
                        backgroundColor: 'rgba(90, 155, 213, 1)',
                        data: [0.0, 0.0, 0.1, 0.0, 0.3, 0.0, 0.0, ],
                        tooltipsValues: [0.0, 0.0, 0.1, 0.0, 0.3, 0.0, 0.0, ],
                    }, {
                        label: 'Handheld',
                        backgroundColor: 'rgba(238, 125, 49, 1)',
                        data: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, ],
                        tooltipsValues: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, ],
                    }, {
                        label: 'Low-end compact 0-4M',
                        backgroundColor: 'rgba(165, 165, 165, 1)',
                        data: [1.6, 2.7, 2.5, 1.7, 1.9, 8.8, 4.7, ],
                        tooltipsValues: [1.6, 2.7, 2.5, 1.7, 1.9, 8.8, 4.7, ],
                    }, {
                        label: 'High-end compact 4M+',
                        backgroundColor: 'rgba(255, 192, 0, 1)',
                        data: [0.8, 2.4, 2.8, 0.5, 2.9, 1.8, 2.4, ],
                        tooltipsValues: [0.8, 2.4, 2.8, 0.5, 2.9, 1.8, 2.4, ],
                    }, {
                        label: 'Low-end cart 0-4M',
                        backgroundColor: 'rgba(68, 115, 197, 1)',
                        data: [1.7, 1.1, 2.8, 1.8, 1.5, 1.1, 0.7, ],
                        tooltipsValues: [1.7, 1.1, 2.8, 1.8, 1.5, 1.1, 0.7, ],
                    }, {
                        label: 'High-end cart 4-8M',
                        backgroundColor: 'rgba(112, 173, 70, 1)',
                        data: [2.6, 5.0, 1.4, 4.4, 3.7, 5.4, 6.5, ],
                        tooltipsValues: [2.6, 5.0, 1.4, 4.4, 3.7, 5.4, 6.5, ],
                    }, {
                        label: 'Premium cart 8M+',
                        backgroundColor: 'rgba(7, 93, 144, 1)',
                        data: [0.8, 3.3, 5.5, 2.2, 2.5, 4.3, 6.6, ],
                        tooltipsValues: [0.8, 3.3, 5.5, 2.2, 2.5, 4.3, 6.6, ],
                    }, ],
                }, ];
                window.PercData4 = [
                    [100, 100, 94, 99, 100, 80, 49],
                    [0, 0, 6, 1, 0, 20, 51],
                ];
                window.SelectedBU = [
                    'Ultrasound',
                    'Ultrasound',
                    'Ultrasound',
                    'Ultrasound',
                    'Ultrasound',
                ];
                window.SummaryList = [
                    [
                        'YTD MS% gainers: GE +10,7%, Siemens +1,5%',
                        'YTD MS% losers: Hitachi –2,4%, Esaote –1,8%, Philips –1,7%, Sonoscape –1,5%, Mindray –0,9%',
                    ],
                    [
                        'Market growth +42,6% YTD driven by Direct / MTS',
                        'Direct channel (2020 YTD - MTS Moscow only) impacted TTL market dramatically in Q2 / Q3'
                    ],
                    [
                        'Indirect channel +/- flat Sep’20 YTD, with strong growth Q1 / Q2 reversing to sharp decline in Q3',
                        'YTD MS% gainers: SAMSUNG +5,5%, Mindray +1,4%, GE +0,9%',
                        'YTD MS% losers: Philips –2,7%, Esaote –2,3%, Siemens –1,2%, Hitachi –0,9%',
                    ],
                    [
                        'Indirect channel +/- flat Sep’20 YTD, with strong growth Q1 / Q2 reversing to sharp decline in Q3',
                        'Major segments are +/- flat except Low-end compact boosted by Covid / emergency orders in Q2’20',
                        'Handheld (Lumify etc.) remains marginal segment',
                    ],
                    [
                        'MS drivers for SAMSUNG: High-end Cart / Low-end Compact',
                        'High-end Cart: extended portfolio (6 models)',
                        'Low-end Compact: Covid / Emergency demand with decreased lead-times',
                    ],
                ];
                window.Legends = [
                    [
                        '21.8<br><font color="#008000">+152.0%</font>',
                        '26.8<br><font color="#008000">+22.9%</font>',
                        '15.4<br><font color="#008000">+78.3%</font>',
                        '22.0<br><font color="#008000">+42.6%</font>',
                        'Bln.RUB<br>+/- % SPLY',
                        '1.2<br><font color="#c00000">-5.1%</font>',
                        '5.4<br><font color="#008000">+95.8%</font>',
                        '8.8<br><font color="#008000">+89.9%</font>',
                        '4.8<br><font color="#c00000">-24.8%</font>',
                        '2.5<br><font color="#008000">+112.2%</font>',
                        '10.0<br><font color="#008000">+84.7%</font>',
                        '9.5<br><font color="#008000">+7.5%</font>',
                    ],
                    [
                        '21.8<br><font color="#008000">+152.0%</font>',
                        '26.8<br><font color="#008000">+22.9%</font>',
                        '15.4<br><font color="#008000">+78.3%</font>',
                        '22.0<br><font color="#008000">+42.6%</font>',
                        'Bln.RUB<br>+/- % SPLY',
                        '1.2<br><font color="#c00000">-5.1%</font>',
                        '5.4<br><font color="#008000">+95.8%</font>',
                        '8.8<br><font color="#008000">+89.9%</font>',
                        '4.8<br><font color="#c00000">-24.8%</font>',
                        '2.5<br><font color="#008000">+112.2%</font>',
                        '10.0<br><font color="#008000">+84.7%</font>',
                        '9.5<br><font color="#008000">+7.5%</font>',
                    ],
                    [
                        '21.2<br><font color="#008000">+145.4%</font>',
                        '20.0<br><font color="#c00000">-5.8%</font>',
                        '14.9<br><font color="#008000">+71.7%</font>',
                        '15.2<br><font color="#008000">+2.4%</font>',
                        'Bln.RUB<br>+/- % SPLY',
                        '1.2<br><font color="#c00000">-5.1%</font>',
                        '5.4<br><font color="#008000">+95.7%</font>',
                        '8.3<br><font color="#008000">+77.7%</font>',
                        '4.8<br><font color="#c00000">-24.8%</font>',
                        '2.5<br><font color="#008000">+112.2%</font>',
                        '8.1<br><font color="#008000">+48.5%</font>',
                        '4.7<br><font color="#c00000">-43.4%</font>',
                    ],
                    [
                        '21.2<br><font color="#008000">+145.4%</font>',
                        '20.0<br><font color="#c00000">-5.8%</font>',
                        '14.9<br><font color="#008000">+71.7%</font>',
                        '15.2<br><font color="#008000">+2.4%</font>',
                        'Bln.RUB<br>+/- % SPLY',
                        '1.2<br><font color="#c00000">-5.1%</font>',
                        '5.4<br><font color="#008000">+95.7%</font>',
                        '8.3<br><font color="#008000">+77.7%</font>',
                        '4.8<br><font color="#c00000">-24.8%</font>',
                        '2.5<br><font color="#008000">+112.2%</font>',
                        '8.1<br><font color="#008000">+48.5%</font>',
                        '4.7<br><font color="#c00000">-43.4%</font>',
                    ],
                    [
                        '3.1<br><font color="#008000">+175.0%</font>',
                        '3.5<br><font color="#008000">+13.0%</font>',
                        '2.1<br><font color="#008000">+87.0%</font>',
                        '3.0<br><font color="#008000">+42.0%</font>',
                        'Bln.RUB<br>+/- % SPLY',
                        '0.1<br><font color="#c00000">-36.0%</font>',
                        '0.8<br><font color="#008000">+133.0%</font>',
                        '1.2<br><font color="#008000">+89.0%</font>',
                        '0.5<br><font color="#c00000">-49.0%</font>',
                        '0.3<br><font color="#008000">+261.0%</font>',
                        '1.7<br><font color="#008000">+119.0%</font>',
                        '1.0<br><font color="#c00000">-22.0%</font>',
                    ],
                ];
            };
            GraphCreate(window.ChartData1, window.ChartData2, window.ChartData3, window.ChartData4, window.SelectedBU, window.SummaryList, window.Legends);
        };

        window.GraphCreate = function(barChartData1, barChartData2, barChartData3, barChartData4, selectedBU, summaryList, legends) {
            Chart.defaults.global.defaultFontSize = 8;
            Chart.defaults.global.defaultFontStyle = 'bold';
            Chart.defaults.global.defaultFontColor = 'black';
            if (window.myBar1_1) {
                window.myBar1_1.data = barChartData1[0];
                window.myBar2_1.data = barChartData2[0];
                window.myBar4_1.data = barChartData4[0];
                window.myBar1_2.data = barChartData1[1];
                window.myBar2_2.data = barChartData2[1];
                window.myBar4_2.data = barChartData4[1];
                window.myBar1_3.data = barChartData1[2];
                window.myBar2_3.data = barChartData2[2];
                window.myBar4_3.data = barChartData4[2];
                window.myBar1_4.data = barChartData1[3];
                window.myBar2_4.data = barChartData2[3];
                window.myBar4_4.data = barChartData4[3];
                window.myBar1_5.data = barChartData1[4];
                window.myBar2_5.data = barChartData2[4];
                window.myBar4_5.data = barChartData4[4];
                let markIndex = barChartData1[2].datasets.findIndex(el => el.label === window.MarkNames[0]);
                if (markIndex > -1) {
                    window.myBar1_5.options.scales.yAxes[0].ticks.max = Math.max.apply(null, barChartData1[2].datasets[markIndex].data) * 1.05;
                    window.myBar2_5.options.scales.yAxes[0].ticks.max = Math.max.apply(null, barChartData2[2].datasets[markIndex].data) * 1.05;
                    window.myBar4_5.options.scales.yAxes[0].ticks.max = Math.max.apply(null, barChartData4[2].datasets[markIndex].data) * 1.05;
                }
                window.myBar1_1.update();
                window.myBar2_1.update();
                window.myBar4_1.update();
                window.myBar1_2.update();
                window.myBar2_2.update();
                window.myBar4_2.update();
                window.myBar1_3.update();
                window.myBar2_3.update();
                window.myBar4_3.update();
                window.myBar1_4.update();
                window.myBar2_4.update();
                window.myBar4_4.update();
                window.myBar1_5.update();
                window.myBar2_5.update();
                window.myBar4_5.update();
            } else {
                let ctx1_1 = document.getElementById('canvas1_1').getContext('2d');
                window.myBar1_1 = new Chart(ctx1_1, {
                    type: 'bar',
                    data: barChartData1[0],
                    options: {
                        hover: {
                            mode: null,
                            events: [],
                        },
                        legend: {
                            display: false,
                        },
                        tooltips: {
                            mode: 'index',
                            backgroundColor: 'rgba(255, 255, 255, 0.75)',
                            titleFontSize: 14,
                            titleFontColor: 'rgba(0, 0, 0, 1)',
                            bodyFontSize: 14,
                            bodyFontColor: 'rgba(0, 0, 0, 1)',
                            bodyFontStyle: 'normal',
                            intersect: false,
                            callbacks: {
                                label: function(tooltipItem, data) {
                                    //let sum = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                                    //return Number(tooltipItem.yLabel);
                                    return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].tooltipsValues[tooltipItem.index];
                                },
                            },
                        },
                        responsive: true,
                        scales: {
                            xAxes: [{
                                gridLineHeight: 20,
                                gridLines: {
                                    display: false,
                                },
                                stacked: true,
                            }],
                            yAxes: [{
                                display: false,
                                gridLines: {
                                    display: false,
                                },
                                stacked: true,
                                ticks: {
                                    max: 100,
                                    display: false,
                                },
                            }],
                        },
                        plugins: {
                            datalabels: {
                                textAlign: 'center',
                                font: {
                                    size: 12,
                                },
                                formatter: function(value, ctx1_1) {
                                    if (Number(value) > 1.99) {
                                        return value + '%';
                                    } else {
                                        return '';
                                    }
                                },
                            },
                        },
                    },
                });
                let ctx2_1 = document.getElementById('canvas2_1').getContext('2d');
                window.myBar2_1 = new Chart(ctx2_1, {
                    type: 'bar',
                    data: barChartData2[0],
                    options: {
                        hover: {
                            mode: null,
                            events: [],
                        },
                        legend: {
                            display: false,
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false,
                            backgroundColor: 'rgba(255, 255, 255, 0.75)',
                            titleFontSize: 14,
                            titleFontColor: 'rgba(0, 0, 0, 1)',
                            bodyFontSize: 14,
                            bodyFontColor: 'rgba(0, 0, 0, 1)',
                            bodyFontStyle: 'normal',
                            callbacks: {
                                label: function(tooltipItem, data) {
                                    //let sum = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                                    //return Number(tooltipItem.yLabel);
                                    return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].tooltipsValues[tooltipItem.index];
                                },
                                /*afterLabel: function(tooltipItem, data) {
                                    let total = 0;
                                    for (let i = 0; i < data.datasets.length; i++) {
                                        total += data.datasets[i].data[tooltipItem.datasetIndex];
                                    }
                                    let percentage = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.datasetIndex] / total * 100;
                                    percentage = percentage.toFixed(2); // Trim decimal part
                                    return "Percentage: %" + percentage;
                                },*/
                            },
                        },
                        responsive: true,
                        scales: {
                            xAxes: [{
                                gridLineHeight: 20,
                                gridLines: {
                                    display: false,
                                },
                                stacked: true,
                            }],
                            yAxes: [{
                                display: false,
                                gridLines: {
                                    display: false,
                                },
                                stacked: true,
                                ticks: {
                                    max: 100,
                                    display: false,
                                },
                            }],
                        },
                        plugins: {
                            datalabels: {
                                textAlign: 'center',
                                font: {
                                    size: 12,
                                },
                                formatter: function(value, ctx2_1) {
                                    if (Number(value) > 1.99) {
                                        return value + '%';
                                    } else {
                                        return '';
                                    }
                                },
                            },
                        },
                    },
                });
                let ctx4_1 = document.getElementById('canvas4_1').getContext('2d');
                window.myBar4_1 = new Chart(ctx4_1, {
                    type: 'bar',
                    data: barChartData4[0],
                    options: {
                        hover: {
                            mode: null,
                            events: [],
                        },
                        legend: {
                            display: false,
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false,
                            backgroundColor: 'rgba(255, 255, 255, 0.75)',
                            titleFontSize: 14,
                            titleFontColor: 'rgba(0, 0, 0, 1)',
                            bodyFontSize: 14,
                            bodyFontColor: 'rgba(0, 0, 0, 1)',
                            bodyFontStyle: 'normal',
                            callbacks: {
                                label: function(tooltipItem, data) {
                                    //let sum = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                                    //return Number(tooltipItem.yLabel);
                                    return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].tooltipsValues[tooltipItem.index];
                                },
                            },
                        },
                        responsive: true,
                        scales: {
                            xAxes: [{
                                gridLineHeight: 20,
                                gridLines: {
                                    display: false,
                                },
                                stacked: true,
                            }],
                            yAxes: [{
                                display: false,
                                gridLines: {
                                    display: false,
                                },
                                stacked: true,
                                ticks: {
                                    display: false,
                                    max: 100,
                                },
                            }],
                        },
                        plugins: {
                            datalabels: {
                                textAlign: 'center',
                                font: {
                                    size: 12,
                                },
                                formatter: function(value, ctx4_1) {
                                    if (Number(value) > 1.99) {
                                        return value + '%';
                                    } else {
                                        return '';
                                    }
                                },
                            },
                        },
                    },
                });
                let ctx1_2 = document.getElementById('canvas1_2').getContext('2d');
                window.myBar1_2 = new Chart(ctx1_2, {
                    type: 'bar',
                    data: barChartData1[1],
                    options: {
                        hover: {
                            mode: null,
                            events: [],
                        },
                        legend: {
                            display: false,
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false,
                            backgroundColor: 'rgba(255, 255, 255, 0.75)',
                            titleFontSize: 14,
                            titleFontColor: 'rgba(0, 0, 0, 1)',
                            bodyFontSize: 14,
                            bodyFontColor: 'rgba(0, 0, 0, 1)',
                            bodyFontStyle: 'normal',
                            callbacks: {
                                label: function(tooltipItem, data) {
                                    //let sum = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                                    //return Number(tooltipItem.yLabel);
                                    return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].tooltipsValues[tooltipItem.index];
                                },
                            },
                        },
                        responsive: true,
                        scales: {
                            xAxes: [{
                                gridLines: {
                                    display: false,
                                },
                                stacked: true,
                            }],
                            yAxes: [{
                                display: false,
                                gridLines: {
                                    display: false,
                                },
                                stacked: true,
                                ticks: {
                                    display: false,
                                },
                            }],
                        },
                        plugins: {
                            datalabels: {
                                textAlign: 'center',
                                font: {
                                    size: 12,
                                },
                                formatter: function(value, ctx1_2) {
                                    if (window.PercData1[ctx1_2.datasetIndex][ctx1_2.dataIndex] > 1.99) {
                                        return String(window.PercData1[ctx1_2.datasetIndex][ctx1_2.dataIndex]) + '%';
                                    } else {
                                        return '';
                                    }
                                },
                            },
                        },
                    },
                });
                let ctx2_2 = document.getElementById('canvas2_2').getContext('2d');
                window.myBar2_2 = new Chart(ctx2_2, {
                    type: 'bar',
                    data: barChartData2[1],
                    options: {
                        hover: {
                            mode: null,
                            events: [],
                        },
                        legend: {
                            display: false,
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false,
                            backgroundColor: 'rgba(255, 255, 255, 0.75)',
                            titleFontSize: 14,
                            titleFontColor: 'rgba(0, 0, 0, 1)',
                            bodyFontSize: 14,
                            bodyFontColor: 'rgba(0, 0, 0, 1)',
                            bodyFontStyle: 'normal',
                            callbacks: {
                                label: function(tooltipItem, data) {
                                    //let sum = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                                    //return Number(tooltipItem.yLabel);
                                    return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].tooltipsValues[tooltipItem.index];
                                },
                            },
                        },
                        responsive: true,
                        scales: {
                            xAxes: [{
                                gridLines: {
                                    display: false,
                                },
                                stacked: true,
                            }],
                            yAxes: [{
                                display: false,
                                gridLines: {
                                    display: false,
                                },
                                stacked: true,
                                ticks: {
                                    display: false,
                                },
                            }],
                        },
                        plugins: {
                            datalabels: {
                                textAlign: 'center',
                                font: {
                                    size: 12,
                                },
                                formatter: function(value, ctx2_2) {
                                    if (window.PercData2[ctx2_2.datasetIndex][ctx2_2.dataIndex] > 1.99) {
                                        return String(window.PercData2[ctx2_2.datasetIndex][ctx2_2.dataIndex]) + '%';
                                    } else {
                                        return '';
                                    }
                                },
                            },
                        },
                    },
                });
                let ctx4_2 = document.getElementById('canvas4_2').getContext('2d');
                window.myBar4_2 = new Chart(ctx4_2, {
                    type: 'bar',
                    data: barChartData4[1],
                    options: {
                        hover: {
                            mode: null,
                            events: [],
                        },
                        legend: {
                            display: false,
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false,
                            backgroundColor: 'rgba(255, 255, 255, 0.75)',
                            titleFontSize: 14,
                            titleFontColor: 'rgba(0, 0, 0, 1)',
                            bodyFontSize: 14,
                            bodyFontColor: 'rgba(0, 0, 0, 1)',
                            bodyFontStyle: 'normal',
                            callbacks: {
                                label: function(tooltipItem, data) {
                                    //let sum = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                                    //return Number(tooltipItem.yLabel);
                                    return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].tooltipsValues[tooltipItem.index];
                                },
                            },
                        },
                        responsive: true,
                        scales: {
                            xAxes: [{
                                gridLines: {
                                    display: false,
                                },
                                stacked: true,
                            }],
                            yAxes: [{
                                display: false,
                                gridLines: {
                                    display: false,
                                },
                                stacked: true,
                                ticks: {
                                    display: false,
                                },
                            }],
                        },
                        plugins: {
                            datalabels: {
                                textAlign: 'center',
                                font: {
                                    size: 12,
                                },
                                formatter: function(value, ctx4_2) {
                                    if (window.PercData4[ctx4_2.datasetIndex][ctx4_2.dataIndex] > 1.99) {
                                        return String(window.PercData4[ctx4_2.datasetIndex][ctx4_2.dataIndex]) + '%';
                                    } else {
                                        return '';
                                    }
                                },
                            },
                        },
                    },
                });
                let ctx1_3 = document.getElementById('canvas1_3').getContext('2d');
                window.myBar1_3 = new Chart(ctx1_3, {
                    type: 'bar',
                    data: barChartData1[2],
                    options: {
                        hover: {
                            mode: null,
                            events: [],
                        },
                        legend: {
                            display: false,
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false,
                            backgroundColor: 'rgba(255, 255, 255, 0.75)',
                            titleFontSize: 14,
                            titleFontColor: 'rgba(0, 0, 0, 1)',
                            bodyFontSize: 14,
                            bodyFontColor: 'rgba(0, 0, 0, 1)',
                            bodyFontStyle: 'normal',
                            callbacks: {
                                label: function(tooltipItem, data) {
                                    //let sum = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                                    //return Number(tooltipItem.yLabel);
                                    return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].tooltipsValues[tooltipItem.index];
                                },
                            },
                        },
                        responsive: true,
                        scales: {
                            xAxes: [{
                                gridLineHeight: 20,
                                gridLines: {
                                    display: false,
                                },
                                stacked: true,
                            }],
                            yAxes: [{
                                display: false,
                                gridLines: {
                                    display: false,
                                },
                                stacked: true,
                                ticks: {
                                    max: 100,
                                    display: false,
                                },
                            }],
                        },
                        plugins: {
                            datalabels: {
                                textAlign: 'center',
                                font: {
                                    size: 12,
                                },
                                formatter: function(value, ctx1_3) {
                                    if (Number(value) > 1.99) {
                                        return value + '%';
                                    } else {
                                        return '';
                                    }
                                },
                            },
                        },
                    },
                });
                let ctx2_3 = document.getElementById('canvas2_3').getContext('2d');
                window.myBar2_3 = new Chart(ctx2_3, {
                    type: 'bar',
                    data: barChartData2[2],
                    options: {
                        hover: {
                            mode: null,
                            events: [],
                        },
                        legend: {
                            display: false,
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false,
                            backgroundColor: 'rgba(255, 255, 255, 0.75)',
                            titleFontSize: 14,
                            titleFontColor: 'rgba(0, 0, 0, 1)',
                            bodyFontSize: 14,
                            bodyFontColor: 'rgba(0, 0, 0, 1)',
                            bodyFontStyle: 'normal',
                            callbacks: {
                                label: function(tooltipItem, data) {
                                    //let sum = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                                    //return Number(tooltipItem.yLabel);
                                    return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].tooltipsValues[tooltipItem.index];
                                },
                            },
                        },
                        responsive: true,
                        scales: {
                            xAxes: [{
                                gridLineHeight: 20,
                                gridLines: {
                                    display: false,
                                },
                                stacked: true,
                            }],
                            yAxes: [{
                                display: false,
                                gridLines: {
                                    display: false,
                                },
                                stacked: true,
                                ticks: {
                                    max: 100,
                                    display: false,
                                },
                            }],
                        },
                        plugins: {
                            datalabels: {
                                textAlign: 'center',
                                font: {
                                    size: 12,
                                },
                                formatter: function(value, ctx2_3) {
                                    if (Number(value) > 1.99) {
                                        return value + '%';
                                    } else {
                                        return '';
                                    }
                                },
                            },
                        },
                    },
                });
                let ctx4_3 = document.getElementById('canvas4_3').getContext('2d');
                window.myBar4_3 = new Chart(ctx4_3, {
                    type: 'bar',
                    data: barChartData4[2],
                    options: {
                        hover: {
                            mode: null,
                            events: [],
                        },
                        legend: {
                            display: false,
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false,
                            backgroundColor: 'rgba(255, 255, 255, 0.75)',
                            titleFontSize: 14,
                            titleFontColor: 'rgba(0, 0, 0, 1)',
                            bodyFontSize: 14,
                            bodyFontColor: 'rgba(0, 0, 0, 1)',
                            bodyFontStyle: 'normal',
                            callbacks: {
                                label: function(tooltipItem, data) {
                                    //let sum = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                                    //return Number(tooltipItem.yLabel);
                                    return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].tooltipsValues[tooltipItem.index];
                                },
                            },
                        },
                        responsive: true,
                        scales: {
                            xAxes: [{
                                gridLineHeight: 20,
                                gridLines: {
                                    display: false,
                                },
                                stacked: true,
                            }],
                            yAxes: [{
                                display: false,
                                gridLines: {
                                    display: false,
                                },
                                stacked: true,
                                ticks: {
                                    display: false,
                                    max: 100,
                                },
                            }],
                        },
                        plugins: {
                            datalabels: {
                                textAlign: 'center',
                                font: {
                                    size: 12,
                                },
                                formatter: function(value, ctx4_3) {
                                    if (Number(value) > 1.99) {
                                        return value + '%';
                                    } else {
                                        return '';
                                    }
                                },
                            },
                        },
                    },
                });
                let ctx1_4 = document.getElementById('canvas1_4').getContext('2d');
                window.myBar1_4 = new Chart(ctx1_4, {
                    type: 'bar',
                    data: barChartData1[3],
                    options: {
                        hover: {
                            mode: null,
                            events: [],
                        },
                        legend: {
                            display: false,
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false,
                            backgroundColor: 'rgba(255, 255, 255, 0.75)',
                            titleFontSize: 14,
                            titleFontColor: 'rgba(0, 0, 0, 1)',
                            bodyFontSize: 14,
                            bodyFontColor: 'rgba(0, 0, 0, 1)',
                            bodyFontStyle: 'normal',
                            callbacks: {
                                label: function(tooltipItem, data) {
                                    //let sum = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                                    //return Number(tooltipItem.yLabel);
                                    return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].tooltipsValues[tooltipItem.index];
                                },
                            },
                        },
                        responsive: true,
                        scales: {
                            xAxes: [{
                                gridLineHeight: 20,
                                gridLines: {
                                    display: false,
                                },
                                stacked: true,
                            }],
                            yAxes: [{
                                display: false,
                                gridLines: {
                                    display: false,
                                },
                                stacked: true,
                                ticks: {
                                    max: 100,
                                    display: false,
                                },
                            }],
                        },
                        plugins: {
                            datalabels: {
                                textAlign: 'center',
                                font: {
                                    size: 12,
                                },
                                formatter: function(value, ctx1_4) {
                                    if (Number(value) > 1.99) {
                                        return value + '%';
                                    } else {
                                        return '';
                                    }
                                },
                            },
                        },
                    },
                });
                let ctx2_4 = document.getElementById('canvas2_4').getContext('2d');
                window.myBar2_4 = new Chart(ctx2_4, {
                    type: 'bar',
                    data: barChartData2[3],
                    options: {
                        hover: {
                            mode: null,
                            events: [],
                        },
                        legend: {
                            display: false,
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false,
                            backgroundColor: 'rgba(255, 255, 255, 0.75)',
                            titleFontSize: 14,
                            titleFontColor: 'rgba(0, 0, 0, 1)',
                            bodyFontSize: 14,
                            bodyFontColor: 'rgba(0, 0, 0, 1)',
                            bodyFontStyle: 'normal',
                            callbacks: {
                                label: function(tooltipItem, data) {
                                    //let sum = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                                    //return Number(tooltipItem.yLabel);
                                    return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].tooltipsValues[tooltipItem.index];
                                },
                            },
                        },
                        responsive: true,
                        scales: {
                            xAxes: [{
                                gridLineHeight: 20,
                                gridLines: {
                                    display: false,
                                },
                                stacked: true,
                            }],
                            yAxes: [{
                                display: false,
                                gridLines: {
                                    display: false,
                                },
                                stacked: true,
                                ticks: {
                                    max: 100,
                                    display: false,
                                },
                            }],
                        },
                        plugins: {
                            datalabels: {
                                textAlign: 'center',
                                font: {
                                    size: 12,
                                },
                                formatter: function(value, ctx2_4) {
                                    if (Number(value) > 1.99) {
                                        return value + '%';
                                    } else {
                                        return '';
                                    }
                                },
                            },
                        },
                    },
                });
                let ctx4_4 = document.getElementById('canvas4_4').getContext('2d');
                window.myBar4_4 = new Chart(ctx4_4, {
                    type: 'bar',
                    data: barChartData4[3],
                    options: {
                        hover: {
                            mode: null,
                            events: [],
                        },
                        legend: {
                            display: false,
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false,
                            backgroundColor: 'rgba(255, 255, 255, 0.75)',
                            titleFontSize: 14,
                            titleFontColor: 'rgba(0, 0, 0, 1)',
                            bodyFontSize: 14,
                            bodyFontColor: 'rgba(0, 0, 0, 1)',
                            bodyFontStyle: 'normal',
                            callbacks: {
                                label: function(tooltipItem, data) {
                                    //let sum = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                                    //return Number(tooltipItem.yLabel);
                                    return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].tooltipsValues[tooltipItem.index];
                                },
                            },
                        },
                        responsive: true,
                        scales: {
                            xAxes: [{
                                gridLineHeight: 20,
                                gridLines: {
                                    display: false,
                                },
                                stacked: true,
                            }],
                            yAxes: [{
                                display: false,
                                gridLines: {
                                    display: false,
                                },
                                stacked: true,
                                ticks: {
                                    display: false,
                                    max: 100,
                                },
                            }],
                        },
                        plugins: {
                            datalabels: {
                                textAlign: 'center',
                                font: {
                                    size: 12,
                                },
                                formatter: function(value, ctx4_4) {
                                    if (Number(value) > 1.99) {
                                        return value + '%';
                                    } else {
                                        return '';
                                    }
                                },
                            },
                        },
                    },
                });
                let ctx1_5 = document.getElementById('canvas1_5').getContext('2d');
                window.myBar1_5 = new Chart(ctx1_5, {
                    type: 'bar',
                    data: barChartData1[4],
                    options: {
                        hover: {
                            mode: null,
                            events: [],
                            animationDuration: 0,
                        },
                        legend: {
                            display: false,
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false,
                            backgroundColor: 'rgba(255, 255, 255, 0.75)',
                            titleFontSize: 14,
                            titleFontColor: 'rgba(0, 0, 0, 1)',
                            bodyFontSize: 14,
                            bodyFontColor: 'rgba(0, 0, 0, 1)',
                            bodyFontStyle: 'normal',
                            callbacks: {
                                label: function(tooltipItem, data) {
                                    //let sum = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                                    //return Number(tooltipItem.yLabel);
                                    return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].tooltipsValues[tooltipItem.index];
                                },
                            },
                        },
                        responsive: true,
                        scales: {
                            xAxes: [{
                                gridLineHeight: 20,
                                gridLines: {
                                    display: false,
                                },
                                stacked: true,
                            }],
                            yAxes: [{
                                display: false,
                                gridLines: {
                                    display: false,
                                },
                                stacked: true,
                                ticks: {
                                    max: Math.max.apply(null, barChartData1[2].datasets[barChartData1[2].datasets.findIndex(el => el.label === window.MarkNames[0])].data) * 1.05,
                                    display: false,
                                },
                            }],
                        },
                        plugins: {
                            datalabels: {
                                textAlign: 'center',
                                font: {
                                    size: 12,
                                },
                                formatter: function(value, ctx1_5) {
                                    if (ctx1_5.dataset.label === 'All') {
                                        return String(Math.floor(Number(value) * 50) / 10) + '%';
                                    } else if (Number(value) > 0.49) {
                                        return value + '%';
                                    } else {
                                        return '';
                                    }
                                },
                            },
                        },
                        animation: {
                            duration: 1,
                            onComplete: function() {
                                let chartInstance = this.chart,
                                    ctxthis = chartInstance.ctx;
                                ctxthis.font = Chart.helpers.fontString(12, 'bold', Chart.defaults.global.defaultFontFamily);
                                ctxthis.textAlign = 'center';
                                ctxthis.textBaseline = 'bottom';
                                let datas = this.data;
                                let maxYScales = 0;
                                this.data.labels.forEach(function(label, label_index) {
                                    let colsumm = 0;
                                    datas.datasets.forEach(function(dataset, dataset_index) {
                                        let meta = chartInstance.controller.getDatasetMeta(dataset_index);
                                        colsumm += dataset.data[label_index];
                                        if (dataset_index == datas.datasets.length - 1) {
                                            ctxthis.fillText(String(Math.floor(Number(colsumm) * 10) / 10) + '%', meta.data[label_index]._model.x, meta.data[label_index]._model.y - 5);
                                        }
                                    });
                                });
                            },
                        },
                    },
                });
                let ctx2_5 = document.getElementById('canvas2_5').getContext('2d');
                window.myBar2_5 = new Chart(ctx2_5, {
                    type: 'bar',
                    data: barChartData2[4],
                    options: {
                        hover: {
                            mode: null,
                            events: [],
                            animationDuration: 0,
                        },
                        legend: {
                            display: false,
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false,
                            backgroundColor: 'rgba(255, 255, 255, 0.75)',
                            titleFontSize: 14,
                            titleFontColor: 'rgba(0, 0, 0, 1)',
                            bodyFontSize: 14,
                            bodyFontColor: 'rgba(0, 0, 0, 1)',
                            bodyFontStyle: 'normal',
                            callbacks: {
                                label: function(tooltipItem, data) {
                                    //let sum = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                                    //return Number(tooltipItem.yLabel);
                                    return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].tooltipsValues[tooltipItem.index];
                                },
                            },
                        },
                        responsive: true,
                        scales: {
                            xAxes: [{
                                gridLineHeight: 20,
                                gridLines: {
                                    display: false,
                                },
                                stacked: true,
                            }],
                            yAxes: [{
                                display: false,
                                gridLines: {
                                    display: false,
                                },
                                stacked: true,
                                ticks: {
                                    max: Math.max.apply(null, barChartData2[2].datasets[barChartData2[2].datasets.findIndex(el => el.label === window.MarkNames[0])].data) * 1.05,
                                    display: false,
                                },
                            }],
                        },
                        plugins: {
                            datalabels: {
                                textAlign: 'center',
                                font: {
                                    size: 12,
                                },
                                formatter: function(value, ctx2_5) {
                                    if (ctx2_5.dataset.label === 'All') {
                                        return String(Math.floor(Number(value) * 50) / 10) + '%';
                                    } else if (Number(value) > 0.49) {
                                        return value + '%';
                                    } else {
                                        return '';
                                    }
                                },
                            },
                        },
                        animation: {
                            duration: 1,
                            onComplete: function() {
                                let chartInstance = this.chart,
                                    ctxthis = chartInstance.ctx;
                                ctxthis.font = Chart.helpers.fontString(12, 'bold', Chart.defaults.global.defaultFontFamily);
                                ctxthis.textAlign = 'center';
                                ctxthis.textBaseline = 'bottom';
                                let datas = this.data;
                                this.data.labels.forEach(function(label, label_index) {
                                    let colsumm = 0;
                                    datas.datasets.forEach(function(dataset, dataset_index) {
                                        let meta = chartInstance.controller.getDatasetMeta(dataset_index);
                                        colsumm += dataset.data[label_index];
                                        if (dataset_index == datas.datasets.length - 1) {
                                            ctxthis.fillText(String(Math.floor(Number(colsumm) * 10) / 10) + '%', meta.data[label_index]._model.x, meta.data[label_index]._model.y - 5);
                                        }
                                    });
                                });
                            },
                        },
                    },
                });
                let ctx4_5 = document.getElementById('canvas4_5').getContext('2d');
                window.myBar4_5 = new Chart(ctx4_5, {
                    type: 'bar',
                    data: barChartData4[4],
                    options: {
                        hover: {
                            mode: null,
                            events: [],
                            animationDuration: 0,
                        },
                        legend: {
                            display: false,
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false,
                            backgroundColor: 'rgba(255, 255, 255, 0.75)',
                            titleFontSize: 14,
                            titleFontColor: 'rgba(0, 0, 0, 1)',
                            bodyFontSize: 14,
                            bodyFontColor: 'rgba(0, 0, 0, 1)',
                            bodyFontStyle: 'normal',
                            callbacks: {
                                label: function(tooltipItem, data) {
                                    //let sum = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                                    //return Number(tooltipItem.yLabel);
                                    return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].tooltipsValues[tooltipItem.index];
                                },
                            },
                        },
                        responsive: true,
                        scales: {
                            xAxes: [{
                                gridLineHeight: 20,
                                gridLines: {
                                    display: false,
                                },
                                stacked: true,
                            }],
                            yAxes: [{
                                display: false,
                                gridLines: {
                                    display: false,
                                },
                                stacked: true,
                                ticks: {
                                    max: Math.max.apply(null, barChartData4[2].datasets[barChartData4[2].datasets.findIndex(el => el.label === window.MarkNames[0])].data) * 1.05,
                                    display: false,
                                },
                            }],
                        },
                        plugins: {
                            datalabels: {
                                textAlign: 'center',
                                font: {
                                    size: 12,
                                },
                                formatter: function(value, ctx4_5) {
                                    if (ctx4_5.dataset.label === 'All') {
                                        return String(Math.floor(Number(value) * 50) / 10) + '%';
                                    } else if (Number(value) > 0.49) {
                                        return value + '%';
                                    } else {
                                        return '';
                                    }
                                },
                            },
                        },
                        animation: {
                            duration: 1,
                            onComplete: function() {
                                let chartInstance = this.chart,
                                    ctxthis = chartInstance.ctx;
                                ctxthis.font = Chart.helpers.fontString(12, 'bold', Chart.defaults.global.defaultFontFamily);
                                ctxthis.textAlign = 'center';
                                ctxthis.textBaseline = 'bottom';
                                let datas = this.data;
                                this.data.labels.forEach(function(label, label_index) {
                                    let colsumm = 0;
                                    datas.datasets.forEach(function(dataset, dataset_index) {
                                        let meta = chartInstance.controller.getDatasetMeta(dataset_index);
                                        colsumm += dataset.data[label_index];
                                        if (dataset_index == datas.datasets.length - 1) {
                                            ctxthis.fillText(String(Math.floor(Number(colsumm) * 10) / 10) + '%', meta.data[label_index]._model.x, meta.data[label_index]._model.y - 5);
                                        }
                                    });
                                });
                            },
                        },
                    },
                });
            }
            document.getElementsByClassName('FifthPageMark')[0].innerHTML = window.MarkNames[0] + ': Indirect Channel';
            for (let i = 0; i < legends.length; i++) {
                let out_arr = document.getElementsByClassName('paragraph_1 ' + String(i));
                for (let j = 0; j < 5; j++) {
                    if (legends[i][j] !== undefined) out_arr[j].innerHTML = legends[i][j];
                };
                out_arr = '';
                for (let j = 5; j < legends[i].length; j++) {
                    out_arr += '<div class="div-block-12" style="width: 100%; height: 100%"><p class="paragraph_1 ' + String(i) + '">' + legends[i][j] + '</p></div>';
                };
                document.getElementsByClassName('div-block-10')[i].innerHTML = out_arr;
            };
            let out_arr = document.getElementsByClassName('BUType');
            for (let i = 0; i < out_arr.length; i++) {
                out_arr[i].innerHTML = selectedBU[i];
            };
            for (let i = 0; i < summaryList.length; i++) {
                out_arr = '';
                for (let j = 0; j < summaryList[i].length; j++) {
                    out_arr += '<li><p class="list_paragraph">' + summaryList[i][j] + '</p></li>';
                }
                document.getElementsByClassName('list')[i].innerHTML = out_arr;
            }
            for (let i = 0; i < barChartData3.length; i++) {
                out_arr = '';
                for (let j = 0; j < barChartData3[i].length; j++) {
                    out_arr += '<div class="legend_item_block"><div class="legend_part_block"><div class="legend_item_color_box" style="background-color: ' + barChartData3[i][j].backgroundColor + ';"></div></div><div class="legend_part_block"><div class="legend_part_block_in"></div></div><div class="legend_part_block"><p class="legend_item_name_label">' + barChartData3[i][j].label + '</p></div></div>';
                };
                document.getElementsByClassName('legends_wrap_block ' + String(i))[0].innerHTML = out_arr;
            };
            window.AddMarkSelectsOptions();
        };

        window.onresize = function() {
            GraphCreate(window.ChartData1, window.ChartData2, window.ChartData3, window.ChartData4, window.SelectedBU, window.SummaryList, window.Legends);
        };

        (function(global) {
            let MONTHS = [
                'January',
                'February',
                'March',
                'April',
                'May',
                'June',
                'July',
                'August',
                'September',
                'October',
                'November',
                'December'
            ];

            let COLORS = [
                '#4dc9f6',
                '#f67019',
                '#f53794',
                '#537bc4',
                '#acc236',
                '#166a8f',
                '#00a950',
                '#58595b',
                '#8549ba'
            ];

            let Samples = global.Samples || (global.Samples = {});
            let Color = global.Color;

            Samples.utils = {
                // Adapted from http://indiegamr.com/generate-repeatable-random-numbers-in-js/
                srand: function(seed) {
                    this._seed = seed;
                },

                rand: function(min, max) {
                    let seed = this._seed;
                    min = min === undefined ? 0 : min;
                    max = max === undefined ? 1 : max;
                    this._seed = (seed * 9301 + 49297) % 233280;
                    return min + (this._seed / 233280) * (max - min);
                },

                numbers: function(config) {
                    let cfg = config || {};
                    let min = cfg.min || 0;
                    let max = cfg.max || 1;
                    let from = cfg.from || [];
                    let count = cfg.count || 8;
                    let decimals = cfg.decimals || 8;
                    let continuity = cfg.continuity || 1;
                    let dfactor = Math.pow(10, decimals) || 0;
                    let data = [];
                    let i, value;

                    for (i = 0; i < count; ++i) {
                        value = (from[i] || 0) + this.rand(min, max);
                        if (this.rand() <= continuity) {
                            data.push(Math.round(dfactor * value) / dfactor);
                        } else {
                            data.push(null);
                        }
                    }

                    return data;
                },

                labels: function(config) {
                    let cfg = config || {};
                    let min = cfg.min || 0;
                    let max = cfg.max || 100;
                    let count = cfg.count || 8;
                    let step = (max - min) / count;
                    let decimals = cfg.decimals || 8;
                    let dfactor = Math.pow(10, decimals) || 0;
                    let prefix = cfg.prefix || '';
                    let values = [];
                    let i;

                    for (i = min; i < max; i += step) {
                        values.push(prefix + Math.round(dfactor * i) / dfactor);
                    }

                    return values;
                },

                months: function(config) {
                    let cfg = config || {};
                    let count = cfg.count || 12;
                    let section = cfg.section;
                    let values = [];
                    let i, value;

                    for (i = 0; i < count; ++i) {
                        value = MONTHS[Math.ceil(i) % 12];
                        values.push(value.substring(0, section));
                    }

                    return values;
                },

                color: function(index) {
                    return COLORS[index % COLORS.length];
                },

                transparentize: function(color, opacity) {
                    let alpha = opacity === undefined ? 0.5 : 1 - opacity;
                    return Color(color).alpha(alpha).rgbString();
                }
            };

            // DEPRECATED
            window.randomScalingFactor = function(max) {
                return Math.round(Samples.utils.rand(10, max));
            };

            // INITIALIZATION

            Samples.utils.srand(Date.now());

            // Google Analytics
            /* eslint-disable */
            if (document.location.hostname.match(/^(www\.)?chartjs\.org$/)) {
                (function(i, s, o, g, r, a, m) {
                    i['GoogleAnalyticsObject'] = r;
                    i[r] = i[r] || function() {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
                    a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                    a.async = 1;
                    a.src = g;
                    m.parentNode.insertBefore(a, m)
                })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
                ga('create', 'UA-28909194-3', 'auto');
                ga('send', 'pageview');
            }
            /* eslint-enable */

        }(this));
    </script>
    <script>
        window.Data = new Date();
        window.Year = Data.getFullYear();
        window.Month = Data.getMonth() + 1;
        if (window.Month < 10) {
            window.Month = 0 + String(window.Month);
        } else {
            window.Month = String(window.Month);
        };
        window.Day = Data.getDate();
        if (window.Day < 10) {
            window.Day = 0 + String(window.Day);
        } else {
            window.Day = String(window.Day);
        };
        window.onload = function() {
            window.AddDatesOptions();
            window.AddBUSelectsOptions();
            window.AddMarkSelectsOptions();
            document.getElementById("modeselector").innerHTML = '<option id="0" value="0">Разбивка по кварталам</option>';
            window.CalculateGraphData(window.excelRows);
        };
        $(document).ready(function() {
            $("#filefield").change(function(evt) {
                var selectedFile = evt.target.files[0];
                var regex = /(.xls|.xlsx)$/;
                if (selectedFile) {
                    if (regex.test(selectedFile.name.toLowerCase())) {
                        if (typeof(FileReader) != "undefined") {
                            var reader = new FileReader();
                            if (reader.readAsBinaryString) {
                                reader.onload = function(e) {
                                    var workbook = XLSX.read(e.target.result, {
                                        type: 'binary'
                                    });
                                    var Sheet = workbook.SheetNames[0];
                                    window.excelRows = (XLSX.utils.sheet_to_json(workbook.Sheets[Sheet]));
                                    document.getElementById("modeselector").innerHTML = '<option id="0" value="0">Разбивка по кварталам</option><option id="1" value="1">Разбивка по месяцам</option>';
                                    window.CalculateGraphData(window.excelRows);
                                };
                                reader.readAsBinaryString(selectedFile);
                            } else {
                                reader.onload = function(e) {
                                    var data = "";
                                    var bytes = new Uint8Array(e.target.result);
                                    for (var i = 0; i < bytes.byteLength; i++) {
                                        data += String.fromCharCode(bytes[i]);
                                    };
                                    var workbook = XLSX.read(data, {
                                        type: 'binary'
                                    });
                                    var Sheet = workbook.SheetNames[0];
                                    window.excelRows = JSON.stringify(XLSX.utils.sheet_to_row_object_array(workbook.Sheets[Sheet]));
                                    document.getElementById("modeselector").innerHTML = '<option id="0" value="0">Разбивка по кварталам</option><option id="1" value="1">Разбивка по месяцам</option>';
                                    window.CalculateGraphData(window.excelRows);
                                };
                                reader.readAsArrayBuffer(selectedFile);
                            };
                        } else {
                            alert("This browser does not support HTML5.");
                        };
                    } else {
                        alert("Please upload a valid Excel file.");
                    };
                };
            });
        });
    </script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.13.5/jszip.js"></script>
    <style>
        /* Generated on: Sat Dec 26 2020 13:01:53 GMT+0000 (Coordinated Universal Time) */
        /* ==========================================================================
        normalize.css v3.0.3 | MIT License | github.com/necolas/normalize.css 
        ========================================================================== */
        /*! normalize.css v3.0.3 | MIT License | github.com/necolas/normalize.css */
        
        html {
            font-family: sans-serif;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%
        }
        
        body {
            margin: 0;
        }
        
        article,
        aside,
        details,
        figcaption,
        figure,
        footer,
        header,
        hgroup,
        main,
        menu,
        nav,
        section,
        summary {
            display: block
        }
        
        audio,
        canvas,
        progress,
        video {
            display: inline-block;
            vertical-align: baseline
        }
        
        audio:not([controls]) {
            display: none;
            height: 0
        }
        
        [hidden],
        template {
            display: none
        }
        
        a {
            background-color: transparent
        }
        
        a:active,
        a:hover {
            outline: 0
        }
        
        abbr[title] {
            border-bottom: 1px dotted
        }
        
        b,
        strong {
            font-weight: bold
        }
        
        dfn {
            font-style: italic
        }
        
        h1 {
            font-size: 2em;
            margin: .67em 0
        }
        
        mark {
            background: #ff0;
            color: #000
        }
        
        small {
            font-size: 80%
        }
        
        sub,
        sup {
            font-size: 75%;
            line-height: 0;
            position: relative;
            vertical-align: baseline
        }
        
        sup {
            top: -0.5em
        }
        
        sub {
            bottom: -0.25em
        }
        
        img {
            border: 0
        }
        
        svg:not(:root) {
            overflow: hidden
        }
        
        figure {
            margin: 1em 40px
        }
        
        hr {
            box-sizing: content-box;
            height: 0
        }
        
        pre {
            overflow: auto
        }
        
        code,
        kbd,
        pre,
        samp {
            font-family: monospace, monospace;
            font-size: 1em
        }
        
        button,
        input,
        optgroup,
        select,
        textarea {
            color: inherit;
            font: inherit;
            margin: 0
        }
        
        button {
            overflow: visible
        }
        
        button,
        select {
            text-transform: none
        }
        
        button,
        html input[type="button"],
        input[type="reset"] {
            -webkit-appearance: button;
            cursor: pointer
        }
        
        button[disabled],
        html input[disabled] {
            cursor: default
        }
        
        button::-moz-focus-inner,
        input::-moz-focus-inner {
            border: 0;
            padding: 0
        }
        
        input {
            line-height: normal
        }
        
        input[type="checkbox"],
        input[type="radio"] {
            box-sizing: border-box;
            padding: 0
        }
        
        input[type="number"]::-webkit-inner-spin-button,
        input[type="number"]::-webkit-outer-spin-button {
            height: auto
        }
        
        input[type="search"] {
            -webkit-appearance: none
        }
        
        input[type="search"]::-webkit-search-cancel-button,
        input[type="search"]::-webkit-search-decoration {
            -webkit-appearance: none
        }
        
        fieldset {
            border: 1px solid #c0c0c0;
            margin: 0 2px;
            padding: .35em .625em .75em
        }
        
        legend {
            border: 0;
            padding: 0
        }
        
        textarea {
            overflow: auto
        }
        
        optgroup {
            font-weight: bold
        }
        
        table {
            border-collapse: collapse;
            border-spacing: 0
        }
        
        td,
        th {
            padding: 0
        }
        /* ==========================================================================
        Start of base Webflow CSS - If you're looking for some ultra-clean CSS, skip the boilerplate and see the unminified code below.
        ========================================================================== */
        
        @font-face {
            font-family: 'webflow-icons';
            src: url("data:application/x-font-ttf;charset=utf-8;base64,AAEAAAALAIAAAwAwT1MvMg8SBiUAAAC8AAAAYGNtYXDpP+a4AAABHAAAAFxnYXNwAAAAEAAAAXgAAAAIZ2x5ZmhS2XEAAAGAAAADHGhlYWQTFw3HAAAEnAAAADZoaGVhCXYFgQAABNQAAAAkaG10eCe4A1oAAAT4AAAAMGxvY2EDtALGAAAFKAAAABptYXhwABAAPgAABUQAAAAgbmFtZSoCsMsAAAVkAAABznBvc3QAAwAAAAAHNAAAACAAAwP4AZAABQAAApkCzAAAAI8CmQLMAAAB6wAzAQkAAAAAAAAAAAAAAAAAAAABEAAAAAAAAAAAAAAAAAAAAABAAADpAwPA/8AAQAPAAEAAAAABAAAAAAAAAAAAAAAgAAAAAAADAAAAAwAAABwAAQADAAAAHAADAAEAAAAcAAQAQAAAAAwACAACAAQAAQAg5gPpA//9//8AAAAAACDmAOkA//3//wAB/+MaBBcIAAMAAQAAAAAAAAAAAAAAAAABAAH//wAPAAEAAAAAAAAAAAACAAA3OQEAAAAAAQAAAAAAAAAAAAIAADc5AQAAAAABAAAAAAAAAAAAAgAANzkBAAAAAAEBIAAAAyADgAAFAAAJAQcJARcDIP5AQAGA/oBAAcABwED+gP6AQAABAOAAAALgA4AABQAAEwEXCQEH4AHAQP6AAYBAAcABwED+gP6AQAAAAwDAAOADQALAAA8AHwAvAAABISIGHQEUFjMhMjY9ATQmByEiBh0BFBYzITI2PQE0JgchIgYdARQWMyEyNj0BNCYDIP3ADRMTDQJADRMTDf3ADRMTDQJADRMTDf3ADRMTDQJADRMTAsATDSANExMNIA0TwBMNIA0TEw0gDRPAEw0gDRMTDSANEwAAAAABAJ0AtAOBApUABQAACQIHCQEDJP7r/upcAXEBcgKU/usBFVz+fAGEAAAAAAL//f+9BAMDwwAEAAkAABcBJwEXAwE3AQdpA5ps/GZsbAOabPxmbEMDmmz8ZmwDmvxmbAOabAAAAgAA/8AEAAPAAB0AOwAABSInLgEnJjU0Nz4BNzYzMTIXHgEXFhUUBw4BBwYjNTI3PgE3NjU0Jy4BJyYjMSIHDgEHBhUUFx4BFxYzAgBqXV6LKCgoKIteXWpqXV6LKCgoKIteXWpVSktvICEhIG9LSlVVSktvICEhIG9LSlVAKCiLXl1qal1eiygoKCiLXl1qal1eiygoZiEgb0tKVVVKS28gISEgb0tKVVVKS28gIQABAAABwAIAA8AAEgAAEzQ3PgE3NjMxFSIHDgEHBhUxIwAoKIteXWpVSktvICFmAcBqXV6LKChmISBvS0pVAAAAAgAA/8AFtgPAADIAOgAAARYXHgEXFhUUBw4BBwYHIxUhIicuAScmNTQ3PgE3NjMxOAExNDc+ATc2MzIXHgEXFhcVATMJATMVMzUEjD83NlAXFxYXTjU1PQL8kz01Nk8XFxcXTzY1PSIjd1BQWlJJSXInJw3+mdv+2/7c25MCUQYcHFg5OUA/ODlXHBwIAhcXTzY1PTw1Nk8XF1tQUHcjIhwcYUNDTgL+3QFt/pOTkwABAAAAAQAAmM7nP18PPPUACwQAAAAAANciZKUAAAAA1yJkpf/9/70FtgPDAAAACAACAAAAAAAAAAEAAAPA/8AAAAW3//3//QW2AAEAAAAAAAAAAAAAAAAAAAAMBAAAAAAAAAAAAAAAAgAAAAQAASAEAADgBAAAwAQAAJ0EAP/9BAAAAAQAAAAFtwAAAAAAAAAKABQAHgAyAEYAjACiAL4BFgE2AY4AAAABAAAADAA8AAMAAAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAADgCuAAEAAAAAAAEADQAAAAEAAAAAAAIABwCWAAEAAAAAAAMADQBIAAEAAAAAAAQADQCrAAEAAAAAAAUACwAnAAEAAAAAAAYADQBvAAEAAAAAAAoAGgDSAAMAAQQJAAEAGgANAAMAAQQJAAIADgCdAAMAAQQJAAMAGgBVAAMAAQQJAAQAGgC4AAMAAQQJAAUAFgAyAAMAAQQJAAYAGgB8AAMAAQQJAAoANADsd2ViZmxvdy1pY29ucwB3AGUAYgBmAGwAbwB3AC0AaQBjAG8AbgBzVmVyc2lvbiAxLjAAVgBlAHIAcwBpAG8AbgAgADEALgAwd2ViZmxvdy1pY29ucwB3AGUAYgBmAGwAbwB3AC0AaQBjAG8AbgBzd2ViZmxvdy1pY29ucwB3AGUAYgBmAGwAbwB3AC0AaQBjAG8AbgBzUmVndWxhcgBSAGUAZwB1AGwAYQByd2ViZmxvdy1pY29ucwB3AGUAYgBmAGwAbwB3AC0AaQBjAG8AbgBzRm9udCBnZW5lcmF0ZWQgYnkgSWNvTW9vbi4ARgBvAG4AdAAgAGcAZQBuAGUAcgBhAHQAZQBkACAAYgB5ACAASQBjAG8ATQBvAG8AbgAuAAAAAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==") format('truetype');
            font-weight: normal;
            font-style: normal
        }
        
        [class^="w-icon-"],
        [class*=" w-icon-"] {
            font-family: 'webflow-icons' !important;
            speak: none;
            font-style: normal;
            font-weight: normal;
            font-variant: normal;
            text-transform: none;
            line-height: 1;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale
        }
        
        .w-icon-slider-right:before {
            content: "\e600"
        }
        
        .w-icon-slider-left:before {
            content: "\e601"
        }
        
        .w-icon-nav-menu:before {
            content: "\e602"
        }
        
        .w-icon-arrow-down:before,
        .w-icon-dropdown-toggle:before {
            content: "\e603"
        }
        
        .w-icon-file-upload-remove:before {
            content: "\e900"
        }
        
        .w-icon-file-upload-icon:before {
            content: "\e903"
        }
        
        * {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box
        }
        
        html {
            height: 100%
        }
        
        body {
            margin: 0;
            min-height: 100%;
            background-color: #fff;
            font-family: Arial, sans-serif;
            font-size: 14px;
            line-height: 20px;
            color: #333
        }
        
        img {
            max-width: 100%;
            vertical-align: middle;
            display: inline-block
        }
        
        html.w-mod-touch * {
            background-attachment: scroll !important
        }
        
        .w-block {
            display: block
        }
        
        .w-inline-block {
            max-width: 100%;
            display: inline-block
        }
        
        .w-clearfix:before,
        .w-clearfix:after {
            content: " ";
            display: table;
            grid-column-start: 1;
            grid-row-start: 1;
            grid-column-end: 2;
            grid-row-end: 2
        }
        
        .w-clearfix:after {
            clear: both
        }
        
        .w-hidden {
            display: none
        }
        
        .w-button {
            display: inline-block;
            padding: 9px 15px;
            background-color: #3898EC;
            color: white;
            border: 0;
            line-height: inherit;
            text-decoration: none;
            cursor: pointer;
            border-radius: 0
        }
        
        input.w-button {
            -webkit-appearance: button
        }
        
        html[data-w-dynpage] [data-w-cloak] {
            color: transparent !important
        }
        
        .w-webflow-badge,
        .w-webflow-badge * {
            position: static;
            left: auto;
            top: auto;
            right: auto;
            bottom: auto;
            z-index: auto;
            display: block;
            visibility: visible;
            overflow: visible;
            overflow-x: visible;
            overflow-y: visible;
            box-sizing: border-box;
            width: auto;
            height: auto;
            max-height: none;
            max-width: none;
            min-height: 0;
            min-width: 0;
            margin: 0;
            padding: 0;
            float: none;
            clear: none;
            border: 0 none transparent;
            border-radius: 0;
            background: none;
            background-image: none;
            background-position: 0 0;
            background-size: auto auto;
            background-repeat: repeat;
            background-origin: padding-box;
            background-clip: border-box;
            background-attachment: scroll;
            background-color: transparent;
            box-shadow: none;
            opacity: 1;
            transform: none;
            transition: none;
            direction: ltr;
            font-family: inherit;
            font-weight: inherit;
            color: inherit;
            font-size: inherit;
            line-height: inherit;
            font-style: inherit;
            font-variant: inherit;
            text-align: inherit;
            letter-spacing: inherit;
            text-decoration: inherit;
            text-indent: 0;
            text-transform: inherit;
            list-style-type: disc;
            text-shadow: none;
            font-smoothing: auto;
            vertical-align: baseline;
            cursor: inherit;
            white-space: inherit;
            word-break: normal;
            word-spacing: normal;
            word-wrap: normal
        }
        
        .w-webflow-badge {
            position: fixed !important;
            display: inline-block !important;
            visibility: visible !important;
            z-index: 2147483647 !important;
            top: auto !important;
            right: 12px !important;
            bottom: 12px !important;
            left: auto !important;
            color: #AAADB0 !important;
            background-color: #fff !important;
            border-radius: 3px !important;
            padding: 6px 8px 6px 6px !important;
            font-size: 12px !important;
            opacity: 1 !important;
            line-height: 14px !important;
            text-decoration: none !important;
            transform: none !important;
            margin: 0 !important;
            width: auto !important;
            height: auto !important;
            overflow: visible !important;
            white-space: nowrap;
            box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.1), 0 1px 3px rgba(0, 0, 0, 0.1);
            cursor: pointer
        }
        
        .w-webflow-badge>img {
            display: inline-block !important;
            visibility: visible !important;
            opacity: 1 !important;
            vertical-align: middle !important
        }
        
        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
            font-weight: bold;
            margin-bottom: 10px
        }
        
        h1 {
            font-size: 38px;
            line-height: 44px;
            margin-top: 20px
        }
        
        h2 {
            font-size: 32px;
            line-height: 36px;
            margin-top: 20px
        }
        
        h3 {
            font-size: 24px;
            line-height: 30px;
            margin-top: 20px
        }
        
        h4 {
            font-size: 18px;
            line-height: 24px;
            margin-top: 10px
        }
        
        h5 {
            font-size: 14px;
            line-height: 20px;
            margin-top: 10px
        }
        
        h6 {
            font-size: 12px;
            line-height: 18px;
            margin-top: 10px
        }
        
        p {
            margin-top: 0;
            margin-bottom: 10px
        }
        
        blockquote {
            margin: 0 0 10px 0;
            padding: 10px 20px;
            border-left: 5px solid #E2E2E2;
            font-size: 18px;
            line-height: 22px
        }
        
        figure {
            margin: 0;
            margin-bottom: 10px
        }
        
        figcaption {
            margin-top: 5px;
            text-align: center
        }
        
        ul,
        ol {
            margin-top: 0;
            margin-bottom: 10px;
            padding-left: 40px
        }
        
        .w-list-unstyled {
            padding-left: 0;
            list-style: none
        }
        
        .w-embed:before,
        .w-embed:after {
            content: " ";
            display: table;
            grid-column-start: 1;
            grid-row-start: 1;
            grid-column-end: 2;
            grid-row-end: 2
        }
        
        .w-embed:after {
            clear: both
        }
        
        .w-video {
            width: 100%;
            position: relative;
            padding: 0
        }
        
        .w-video iframe,
        .w-video object,
        .w-video embed {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%
        }
        
        fieldset {
            padding: 0;
            margin: 0;
            border: 0
        }
        
        button,
        html input[type="button"],
        input[type="reset"] {
            border: 0;
            cursor: pointer;
            -webkit-appearance: button
        }
        
        .w-form {
            margin: 0 0 15px
        }
        
        .w-form-done {
            display: none;
            padding: 20px;
            text-align: center;
            background-color: #dddddd
        }
        
        .w-form-fail {
            display: none;
            margin-top: 10px;
            padding: 10px;
            background-color: #ffdede
        }
        
        label {
            display: block;
            margin-bottom: 5px;
            font-weight: bold
        }
        
        .w-input,
        .w-select {
            display: block;
            width: 100%;
            height: 38px;
            padding: 8px 12px;
            margin-bottom: 10px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #333333;
            vertical-align: middle;
            background-color: #ffffff;
            border: 1px solid #cccccc
        }
        
        .w-input:-moz-placeholder,
        .w-select:-moz-placeholder {
            color: #999
        }
        
        .w-input::-moz-placeholder,
        .w-select::-moz-placeholder {
            color: #999;
            opacity: 1
        }
        
        .w-input:-ms-input-placeholder,
        .w-select:-ms-input-placeholder {
            color: #999
        }
        
        .w-input::-webkit-input-placeholder,
        .w-select::-webkit-input-placeholder {
            color: #999
        }
        
        .w-input:focus,
        .w-select:focus {
            border-color: #3898EC;
            outline: 0
        }
        
        .w-input[disabled],
        .w-select[disabled],
        .w-input[readonly],
        .w-select[readonly],
        fieldset[disabled] .w-input,
        fieldset[disabled] .w-select {
            cursor: not-allowed;
            background-color: #eeeeee
        }
        
        textarea.w-input,
        textarea.w-select {
            height: auto
        }
        
        .w-select {
            background-color: #f3f3f3
        }
        
        .w-select[multiple] {
            height: auto
        }
        
        .w-form-label {
            display: inline-block;
            cursor: pointer;
            font-weight: normal;
            margin-bottom: 0
        }
        
        .w-radio {
            display: block;
            margin-bottom: 5px;
            padding-left: 20px
        }
        
        .w-radio:before,
        .w-radio:after {
            content: " ";
            display: table;
            grid-column-start: 1;
            grid-row-start: 1;
            grid-column-end: 2;
            grid-row-end: 2
        }
        
        .w-radio:after {
            clear: both
        }
        
        .w-radio-input {
            margin: 4px 0 0;
            margin-top: 1px \9;
            line-height: normal;
            float: left;
            margin-left: -20px
        }
        
        .w-radio-input {
            margin-top: 3px
        }
        
        .w-file-upload {
            display: block;
            margin-bottom: 10px
        }
        
        .w-file-upload-input {
            width: .1px;
            height: .1px;
            opacity: 0;
            overflow: hidden;
            position: absolute;
            z-index: -100
        }
        
        .w-file-upload-default,
        .w-file-upload-uploading,
        .w-file-upload-success {
            display: inline-block;
            color: #333333
        }
        
        .w-file-upload-error {
            display: block;
            margin-top: 10px
        }
        
        .w-file-upload-default.w-hidden,
        .w-file-upload-uploading.w-hidden,
        .w-file-upload-error.w-hidden,
        .w-file-upload-success.w-hidden {
            display: none
        }
        
        .w-file-upload-uploading-btn {
            display: flex;
            font-size: 14px;
            font-weight: normal;
            cursor: pointer;
            margin: 0;
            padding: 8px 12px;
            border: 1px solid #cccccc;
            background-color: #fafafa
        }
        
        .w-file-upload-file {
            display: flex;
            flex-grow: 1;
            justify-content: space-between;
            margin: 0;
            padding: 8px 9px 8px 11px;
            border: 1px solid #cccccc;
            background-color: #fafafa
        }
        
        .w-file-upload-file-name {
            font-size: 14px;
            font-weight: normal;
            display: block
        }
        
        .w-file-remove-link {
            margin-top: 3px;
            margin-left: 10px;
            width: auto;
            height: auto;
            padding: 3px;
            display: block;
            cursor: pointer
        }
        
        .w-icon-file-upload-remove {
            margin: auto;
            font-size: 10px
        }
        
        .w-file-upload-error-msg {
            display: inline-block;
            color: #ea384c;
            padding: 2px 0
        }
        
        .w-file-upload-info {
            display: inline-block;
            line-height: 38px;
            padding: 0 12px
        }
        
        .w-file-upload-label {
            display: inline-block;
            font-size: 14px;
            font-weight: normal;
            cursor: pointer;
            margin: 0;
            padding: 8px 12px;
            border: 1px solid #cccccc;
            background-color: #fafafa
        }
        
        .w-icon-file-upload-icon,
        .w-icon-file-upload-uploading {
            display: inline-block;
            margin-right: 8px;
            width: 20px
        }
        
        .w-icon-file-upload-uploading {
            height: 20px
        }
        
        .w-container {
            margin-left: auto;
            margin-right: auto;
            max-width: 940px
        }
        
        .w-container:before,
        .w-container:after {
            content: " ";
            display: table;
            grid-column-start: 1;
            grid-row-start: 1;
            grid-column-end: 2;
            grid-row-end: 2
        }
        
        .w-container:after {
            clear: both
        }
        
        .w-container .w-row {
            margin-left: -10px;
            margin-right: -10px
        }
        
        .w-row:before,
        .w-row:after {
            content: " ";
            display: table;
            grid-column-start: 1;
            grid-row-start: 1;
            grid-column-end: 2;
            grid-row-end: 2
        }
        
        .w-row:after {
            clear: both
        }
        
        .w-row .w-row {
            margin-left: 0;
            margin-right: 0
        }
        
        .w-col {
            position: relative;
            float: left;
            width: 100%;
            min-height: 1px;
            padding-left: 10px;
            padding-right: 10px
        }
        
        .w-col .w-col {
            padding-left: 0;
            padding-right: 0
        }
        
        .w-col-1 {
            width: 8.33333333%
        }
        
        .w-col-2 {
            width: 16.66666667%
        }
        
        .w-col-3 {
            width: 25%
        }
        
        .w-col-4 {
            width: 33.33333333%
        }
        
        .w-col-5 {
            width: 41.66666667%
        }
        
        .w-col-6 {
            width: 50%
        }
        
        .w-col-7 {
            width: 58.33333333%
        }
        
        .w-col-8 {
            width: 66.66666667%
        }
        
        .w-col-9 {
            width: 75%
        }
        
        .w-col-10 {
            width: 83.33333333%
        }
        
        .w-col-11 {
            width: 91.66666667%
        }
        
        .w-col-12 {
            width: 100%
        }
        
        .w-hidden-main {
            display: none !important
        }
        
        @media screen and (max-width:991px) {
            .w-container {
                max-width: 728px
            }
            .w-hidden-main {
                display: inherit !important
            }
            .w-hidden-medium {
                display: none !important
            }
            .w-col-medium-1 {
                width: 8.33333333%
            }
            .w-col-medium-2 {
                width: 16.66666667%
            }
            .w-col-medium-3 {
                width: 25%
            }
            .w-col-medium-4 {
                width: 33.33333333%
            }
            .w-col-medium-5 {
                width: 41.66666667%
            }
            .w-col-medium-6 {
                width: 50%
            }
            .w-col-medium-7 {
                width: 58.33333333%
            }
            .w-col-medium-8 {
                width: 66.66666667%
            }
            .w-col-medium-9 {
                width: 75%
            }
            .w-col-medium-10 {
                width: 83.33333333%
            }
            .w-col-medium-11 {
                width: 91.66666667%
            }
            .w-col-medium-12 {
                width: 100%
            }
            .w-col-stack {
                width: 100%;
                left: auto;
                right: auto
            }
        }
        
        @media screen and (max-width:767px) {
            .w-hidden-main {
                display: inherit !important
            }
            .w-hidden-medium {
                display: inherit !important
            }
            .w-hidden-small {
                display: none !important
            }
            .w-row,
            .w-container .w-row {
                margin-left: 0;
                margin-right: 0
            }
            .w-col {
                width: 100%;
                left: auto;
                right: auto
            }
            .w-col-small-1 {
                width: 8.33333333%
            }
            .w-col-small-2 {
                width: 16.66666667%
            }
            .w-col-small-3 {
                width: 25%
            }
            .w-col-small-4 {
                width: 33.33333333%
            }
            .w-col-small-5 {
                width: 41.66666667%
            }
            .w-col-small-6 {
                width: 50%
            }
            .w-col-small-7 {
                width: 58.33333333%
            }
            .w-col-small-8 {
                width: 66.66666667%
            }
            .w-col-small-9 {
                width: 75%
            }
            .w-col-small-10 {
                width: 83.33333333%
            }
            .w-col-small-11 {
                width: 91.66666667%
            }
            .w-col-small-12 {
                width: 100%
            }
        }
        
        @media screen and (max-width:479px) {
            .w-container {
                max-width: none
            }
            .w-hidden-main {
                display: inherit !important
            }
            .w-hidden-medium {
                display: inherit !important
            }
            .w-hidden-small {
                display: inherit !important
            }
            .w-hidden-tiny {
                display: none !important
            }
            .w-col {
                width: 100%
            }
            .w-col-tiny-1 {
                width: 8.33333333%
            }
            .w-col-tiny-2 {
                width: 16.66666667%
            }
            .w-col-tiny-3 {
                width: 25%
            }
            .w-col-tiny-4 {
                width: 33.33333333%
            }
            .w-col-tiny-5 {
                width: 41.66666667%
            }
            .w-col-tiny-6 {
                width: 50%
            }
            .w-col-tiny-7 {
                width: 58.33333333%
            }
            .w-col-tiny-8 {
                width: 66.66666667%
            }
            .w-col-tiny-9 {
                width: 75%
            }
            .w-col-tiny-10 {
                width: 83.33333333%
            }
            .w-col-tiny-11 {
                width: 91.66666667%
            }
            .w-col-tiny-12 {
                width: 100%
            }
        }
        
        .w-widget {
            position: relative
        }
        
        .w-widget-map {
            width: 100%;
            height: 400px
        }
        
        .w-widget-map label {
            width: auto;
            display: inline
        }
        
        .w-widget-map img {
            max-width: inherit
        }
        
        .w-widget-map .gm-style-iw {
            text-align: center
        }
        
        .w-widget-map .gm-style-iw>button {
            display: none !important
        }
        
        .w-widget-twitter {
            overflow: hidden
        }
        
        .w-widget-twitter-count-shim {
            display: inline-block;
            vertical-align: top;
            position: relative;
            width: 28px;
            height: 20px;
            text-align: center;
            background: white;
            border: #758696 solid 1px;
            border-radius: 3px
        }
        
        .w-widget-twitter-count-shim * {
            pointer-events: none;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none
        }
        
        .w-widget-twitter-count-shim .w-widget-twitter-count-inner {
            position: relative;
            font-size: 15px;
            line-height: 12px;
            text-align: center;
            color: #999;
            font-family: serif
        }
        
        .w-widget-twitter-count-shim .w-widget-twitter-count-clear {
            position: relative;
            display: block
        }
        
        .w-widget-twitter-count-shim.w--large {
            width: 36px;
            height: 28px
        }
        
        .w-widget-twitter-count-shim.w--large .w-widget-twitter-count-inner {
            font-size: 18px;
            line-height: 18px
        }
        
        .w-widget-twitter-count-shim:not(.w--vertical) {
            margin-left: 5px;
            margin-right: 8px
        }
        
        .w-widget-twitter-count-shim:not(.w--vertical).w--large {
            margin-left: 6px
        }
        
        .w-widget-twitter-count-shim:not(.w--vertical):before,
        .w-widget-twitter-count-shim:not(.w--vertical):after {
            top: 50%;
            left: 0;
            border: solid transparent;
            content: " ";
            height: 0;
            width: 0;
            position: absolute;
            pointer-events: none
        }
        
        .w-widget-twitter-count-shim:not(.w--vertical):before {
            border-color: rgba(117, 134, 150, 0);
            border-right-color: #5d6c7b;
            border-width: 4px;
            margin-left: -9px;
            margin-top: -4px
        }
        
        .w-widget-twitter-count-shim:not(.w--vertical).w--large:before {
            border-width: 5px;
            margin-left: -10px;
            margin-top: -5px
        }
        
        .w-widget-twitter-count-shim:not(.w--vertical):after {
            border-color: rgba(255, 255, 255, 0);
            border-right-color: white;
            border-width: 4px;
            margin-left: -8px;
            margin-top: -4px
        }
        
        .w-widget-twitter-count-shim:not(.w--vertical).w--large:after {
            border-width: 5px;
            margin-left: -9px;
            margin-top: -5px
        }
        
        .w-widget-twitter-count-shim.w--vertical {
            width: 61px;
            height: 33px;
            margin-bottom: 8px
        }
        
        .w-widget-twitter-count-shim.w--vertical:before,
        .w-widget-twitter-count-shim.w--vertical:after {
            top: 100%;
            left: 50%;
            border: solid transparent;
            content: " ";
            height: 0;
            width: 0;
            position: absolute;
            pointer-events: none
        }
        
        .w-widget-twitter-count-shim.w--vertical:before {
            border-color: rgba(117, 134, 150, 0);
            border-top-color: #5d6c7b;
            border-width: 5px;
            margin-left: -5px
        }
        
        .w-widget-twitter-count-shim.w--vertical:after {
            border-color: rgba(255, 255, 255, 0);
            border-top-color: white;
            border-width: 4px;
            margin-left: -4px
        }
        
        .w-widget-twitter-count-shim.w--vertical .w-widget-twitter-count-inner {
            font-size: 18px;
            line-height: 22px
        }
        
        .w-widget-twitter-count-shim.w--vertical.w--large {
            width: 76px
        }
        
        .w-widget-gplus {
            overflow: hidden
        }
        
        .w-background-video {
            position: relative;
            overflow: hidden;
            height: 500px;
            color: white
        }
        
        .w-background-video>video {
            background-size: cover;
            background-position: 50% 50%;
            position: absolute;
            margin: auto;
            width: 100%;
            height: 100%;
            right: -100%;
            bottom: -100%;
            top: -100%;
            left: -100%;
            object-fit: cover;
            z-index: -100
        }
        
        .w-background-video>video::-webkit-media-controls-start-playback-button {
            display: none !important;
            -webkit-appearance: none
        }
        
        .w-slider {
            position: relative;
            height: 100%;
            text-align: center;
            /*background: #f8f8f8;*/
            clear: both;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
            tap-highlight-color: rgba(0, 0, 0, 0)
        }
        
        .w-slider-mask {
            position: relative;
            display: block;
            overflow: hidden;
            z-index: 1;
            left: 0;
            right: 0;
            height: 100%;
            white-space: nowrap
        }
        
        .w-slide {
            position: relative;
            display: inline-block;
            vertical-align: top;
            width: 100%;
            height: 100%;
            white-space: normal;
            text-align: left
        }
        
        .w-slider-nav {
            position: absolute;
            z-index: 2;
            top: auto;
            right: 0;
            bottom: 0;
            left: 0;
            margin: auto;
            padding-top: 10px;
            height: 40px;
            text-align: center;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
            tap-highlight-color: rgba(0, 0, 0, 0)
        }
        
        .w-slider-nav.w-round>div {
            border-radius: 100%
        }
        
        .w-slider-nav.w-num>div {
            width: auto;
            height: auto;
            padding: .2em .5em;
            font-size: inherit;
            line-height: inherit
        }
        
        .w-slider-nav.w-shadow>div {
            box-shadow: 0 0 3px rgba(51, 51, 51, 0.4)
        }
        
        .w-slider-nav-invert {
            color: #fff
        }
        
        .w-slider-nav-invert>div {
            background-color: rgba(34, 34, 34, 0.4)
        }
        
        .w-slider-nav-invert>div.w-active {
            background-color: #222
        }
        
        .w-slider-dot {
            position: relative;
            display: inline-block;
            width: 1em;
            height: 1em;
            background-color: rgba(255, 255, 255, 0.4);
            cursor: pointer;
            margin: 0 3px .5em;
            transition: background-color 100ms, color 100ms
        }
        
        .w-slider-dot.w-active {
            background-color: #fff
        }
        
        .w-slider-dot:focus {
            outline: none;
            box-shadow: 0 0 0 2px #fff
        }
        
        .w-slider-dot:focus.w-active {
            box-shadow: none
        }
        
        .w-slider-arrow-left,
        .w-slider-arrow-right {
            position: absolute;
            width: 31px;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            margin: auto;
            cursor: pointer;
            overflow: hidden;
            color: rgba(100, 100, 100, 1);
            font-size: 40px;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
            tap-highlight-color: rgba(0, 0, 0, 0);
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none
        }
        
        .w-slider-arrow-left [class^="w-icon-"],
        .w-slider-arrow-right [class^="w-icon-"],
        .w-slider-arrow-left [class*=" w-icon-"],
        .w-slider-arrow-right [class*=" w-icon-"] {
            position: absolute
        }
        
        .w-slider-arrow-left:focus,
        .w-slider-arrow-right:focus {
            outline: 0
        }
        
        .w-slider-arrow-left {
            z-index: 3;
            right: auto
        }
        
        .w-slider-arrow-right {
            z-index: 4;
            left: auto
        }
        
        .w-icon-slider-left,
        .w-icon-slider-right {
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            margin: auto;
            width: 1em;
            height: 1em
        }
        
        .w-slider-aria-label {
            border: 0;
            clip: rect(0 0 0 0);
            height: 1px;
            margin: -1px;
            overflow: hidden;
            padding: 0;
            position: absolute;
            width: 1px
        }
        
        .w-dropdown {
            display: inline-block;
            position: relative;
            text-align: left;
            margin-left: auto;
            margin-right: auto;
            z-index: 900
        }
        
        .w-dropdown-btn,
        .w-dropdown-toggle,
        .w-dropdown-link {
            position: relative;
            vertical-align: top;
            text-decoration: none;
            color: #222222;
            padding: 20px;
            text-align: left;
            margin-left: auto;
            margin-right: auto;
            white-space: nowrap
        }
        
        .w-dropdown-toggle {
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            display: inline-block;
            cursor: pointer;
            padding-right: 40px
        }
        
        .w-dropdown-toggle:focus {
            outline: 0
        }
        
        .w-icon-dropdown-toggle {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            margin: auto;
            margin-right: 20px;
            width: 1em;
            height: 1em
        }
        
        .w-dropdown-list {
            position: absolute;
            background: #dddddd;
            display: none;
            min-width: 100%
        }
        
        .w-dropdown-list.w--open {
            display: block
        }
        
        .w-dropdown-link {
            padding: 10px 20px;
            display: block;
            color: #222222
        }
        
        .w-dropdown-link.w--current {
            color: #0082f3
        }
        
        .w-dropdown-link:focus {
            outline: 0
        }
        
        @media screen and (max-width:767px) {
            .w-nav-brand {
                padding-left: 10px
            }
        }
        
        .w-lightbox-backdrop {
            color: #000;
            cursor: auto;
            font-family: serif;
            font-size: medium;
            font-style: normal;
            font-variant: normal;
            font-weight: normal;
            letter-spacing: normal;
            line-height: normal;
            list-style: disc;
            text-align: start;
            text-indent: 0;
            text-shadow: none;
            text-transform: none;
            visibility: visible;
            white-space: normal;
            word-break: normal;
            word-spacing: normal;
            word-wrap: normal;
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            color: #fff;
            font-family: "Helvetica Neue", Helvetica, Ubuntu, "Segoe UI", Verdana, sans-serif;
            font-size: 17px;
            line-height: 1.2;
            font-weight: 300;
            text-align: center;
            background: rgba(0, 0, 0, 0.9);
            z-index: 2000;
            outline: 0;
            opacity: 0;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            -webkit-tap-highlight-color: transparent;
            -webkit-transform: translate(0, 0)
        }
        
        .w-lightbox-backdrop,
        .w-lightbox-container {
            height: 100%;
            overflow: auto;
            -webkit-overflow-scrolling: touch
        }
        
        .w-lightbox-content {
            position: relative;
            height: 100vh;
            overflow: hidden
        }
        
        .w-lightbox-view {
            position: absolute;
            width: 100vw;
            height: 100vh;
            opacity: 0
        }
        
        .w-lightbox-view:before {
            content: "";
            height: 100vh
        }
        
        .w-lightbox-group,
        .w-lightbox-group .w-lightbox-view,
        .w-lightbox-group .w-lightbox-view:before {
            height: 86vh
        }
        
        .w-lightbox-frame,
        .w-lightbox-view:before {
            display: inline-block;
            vertical-align: middle
        }
        
        .w-lightbox-figure {
            position: relative;
            margin: 0
        }
        
        .w-lightbox-group .w-lightbox-figure {
            cursor: pointer
        }
        
        .w-lightbox-img {
            width: auto;
            height: auto;
            max-width: none
        }
        
        .w-lightbox-image {
            display: block;
            float: none;
            max-width: 100vw;
            max-height: 100vh
        }
        
        .w-lightbox-group .w-lightbox-image {
            max-height: 86vh
        }
        
        .w-lightbox-caption {
            position: absolute;
            right: 0;
            bottom: 0;
            left: 0;
            padding: .5em 1em;
            background: rgba(0, 0, 0, 0.4);
            text-align: left;
            text-overflow: ellipsis;
            white-space: nowrap;
            overflow: hidden
        }
        
        .w-lightbox-embed {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            width: 100%;
            height: 100%
        }
        
        .w-lightbox-control {
            position: absolute;
            top: 0;
            width: 4em;
            background-size: 24px;
            background-repeat: no-repeat;
            background-position: center;
            cursor: pointer;
            -webkit-transition: all .3s;
            transition: all .3s
        }
        
        .w-lightbox-left {
            display: none;
            bottom: 0;
            left: 0;
            background-image: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9Ii0yMCAwIDI0IDQwIiB3aWR0aD0iMjQiIGhlaWdodD0iNDAiPjxnIHRyYW5zZm9ybT0icm90YXRlKDQ1KSI+PHBhdGggZD0ibTAgMGg1djIzaDIzdjVoLTI4eiIgb3BhY2l0eT0iLjQiLz48cGF0aCBkPSJtMSAxaDN2MjNoMjN2M2gtMjZ6IiBmaWxsPSIjZmZmIi8+PC9nPjwvc3ZnPg==")
        }
        
        .w-lightbox-right {
            display: none;
            right: 0;
            bottom: 0;
            background-image: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9Ii00IDAgMjQgNDAiIHdpZHRoPSIyNCIgaGVpZ2h0PSI0MCI+PGcgdHJhbnNmb3JtPSJyb3RhdGUoNDUpIj48cGF0aCBkPSJtMC0waDI4djI4aC01di0yM2gtMjN6IiBvcGFjaXR5PSIuNCIvPjxwYXRoIGQ9Im0xIDFoMjZ2MjZoLTN2LTIzaC0yM3oiIGZpbGw9IiNmZmYiLz48L2c+PC9zdmc+")
        }
        
        .w-lightbox-close {
            right: 0;
            height: 2.6em;
            background-image: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9Ii00IDAgMTggMTciIHdpZHRoPSIxOCIgaGVpZ2h0PSIxNyI+PGcgdHJhbnNmb3JtPSJyb3RhdGUoNDUpIj48cGF0aCBkPSJtMCAwaDd2LTdoNXY3aDd2NWgtN3Y3aC01di03aC03eiIgb3BhY2l0eT0iLjQiLz48cGF0aCBkPSJtMSAxaDd2LTdoM3Y3aDd2M2gtN3Y3aC0zdi03aC03eiIgZmlsbD0iI2ZmZiIvPjwvZz48L3N2Zz4=");
            background-size: 18px
        }
        
        .w-lightbox-strip {
            position: absolute;
            bottom: 0;
            left: 0;
            right: 0;
            padding: 0 1vh;
            line-height: 0;
            white-space: nowrap;
            overflow-x: auto;
            overflow-y: hidden
        }
        
        .w-lightbox-item {
            display: inline-block;
            width: 10vh;
            padding: 2vh 1vh;
            box-sizing: content-box;
            cursor: pointer;
            -webkit-transform: translate3d(0, 0, 0)
        }
        
        .w-lightbox-active {
            opacity: .3
        }
        
        .w-lightbox-thumbnail {
            position: relative;
            height: 10vh;
            background: #222;
            overflow: hidden
        }
        
        .w-lightbox-thumbnail-image {
            position: absolute;
            top: 0;
            left: 0
        }
        
        .w-lightbox-thumbnail .w-lightbox-tall {
            top: 50%;
            width: 100%;
            -webkit-transform: translate(0, -50%);
            -ms-transform: translate(0, -50%);
            transform: translate(0, -50%)
        }
        
        .w-lightbox-thumbnail .w-lightbox-wide {
            left: 50%;
            height: 100%;
            -webkit-transform: translate(-50%, 0);
            -ms-transform: translate(-50%, 0);
            transform: translate(-50%, 0)
        }
        
        .w-lightbox-spinner {
            position: absolute;
            top: 50%;
            left: 50%;
            box-sizing: border-box;
            width: 40px;
            height: 40px;
            margin-top: -20px;
            margin-left: -20px;
            border: 5px solid rgba(0, 0, 0, 0.4);
            border-radius: 50%;
            -webkit-animation: spin .8s infinite linear;
            animation: spin .8s infinite linear
        }
        
        .w-lightbox-spinner:after {
            content: "";
            position: absolute;
            top: -4px;
            right: -4px;
            bottom: -4px;
            left: -4px;
            border: 3px solid transparent;
            border-bottom-color: #fff;
            border-radius: 50%
        }
        
        .w-lightbox-hide {
            display: none
        }
        
        .w-lightbox-noscroll {
            overflow: hidden
        }
        
        @media (min-width:768px) {
            .w-lightbox-content {
                height: 96vh;
                margin-top: 2vh
            }
            .w-lightbox-view,
            .w-lightbox-view:before {
                height: 96vh
            }
            .w-lightbox-group,
            .w-lightbox-group .w-lightbox-view,
            .w-lightbox-group .w-lightbox-view:before {
                height: 84vh
            }
            .w-lightbox-image {
                max-width: 96vw;
                max-height: 96vh
            }
            .w-lightbox-group .w-lightbox-image {
                max-width: 82.3vw;
                max-height: 84vh
            }
            .w-lightbox-left,
            .w-lightbox-right {
                display: block;
                opacity: .5
            }
            .w-lightbox-close {
                opacity: .8
            }
            .w-lightbox-control:hover {
                opacity: 1
            }
        }
        
        .w-lightbox-inactive,
        .w-lightbox-inactive:hover {
            opacity: 0
        }
        
        .w-richtext:before,
        .w-richtext:after {
            content: " ";
            display: table;
            grid-column-start: 1;
            grid-row-start: 1;
            grid-column-end: 2;
            grid-row-end: 2
        }
        
        .w-richtext:after {
            clear: both
        }
        
        .w-richtext[contenteditable="true"]:before,
        .w-richtext[contenteditable="true"]:after {
            white-space: initial
        }
        
        .w-richtext ol,
        .w-richtext ul {
            overflow: hidden
        }
        
        .w-richtext .w-richtext-figure-selected.w-richtext-figure-type-video div:after,
        .w-richtext .w-richtext-figure-selected[data-rt-type="video"] div:after {
            outline: 2px solid #2895f7
        }
        
        .w-richtext .w-richtext-figure-selected.w-richtext-figure-type-image div,
        .w-richtext .w-richtext-figure-selected[data-rt-type="image"] div {
            outline: 2px solid #2895f7
        }
        
        .w-richtext figure.w-richtext-figure-type-video>div:after,
        .w-richtext figure[data-rt-type="video"]>div:after {
            content: '';
            position: absolute;
            display: none;
            left: 0;
            top: 0;
            right: 0;
            bottom: 0
        }
        
        .w-richtext figure {
            position: relative;
            max-width: 60%
        }
        
        .w-richtext figure>div:before {
            cursor: default !important
        }
        
        .w-richtext figure img {
            width: 100%
        }
        
        .w-richtext figure figcaption.w-richtext-figcaption-placeholder {
            opacity: .6
        }
        
        .w-richtext figure div {
            font-size: 0;
            color: transparent
        }
        
        .w-richtext figure.w-richtext-figure-type-image,
        .w-richtext figure[data-rt-type="image"] {
            display: table
        }
        
        .w-richtext figure.w-richtext-figure-type-image>div,
        .w-richtext figure[data-rt-type="image"]>div {
            display: inline-block
        }
        
        .w-richtext figure.w-richtext-figure-type-image>figcaption,
        .w-richtext figure[data-rt-type="image"]>figcaption {
            display: table-caption;
            caption-side: bottom
        }
        
        .w-richtext figure.w-richtext-figure-type-video,
        .w-richtext figure[data-rt-type="video"] {
            width: 60%;
            height: 0
        }
        
        .w-richtext figure.w-richtext-figure-type-video iframe,
        .w-richtext figure[data-rt-type="video"] iframe {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%
        }
        
        .w-richtext figure.w-richtext-figure-type-video>div,
        .w-richtext figure[data-rt-type="video"]>div {
            width: 100%
        }
        
        .w-richtext figure.w-richtext-align-center {
            margin-right: auto;
            margin-left: auto;
            clear: both
        }
        
        .w-richtext figure.w-richtext-align-center.w-richtext-figure-type-image>div,
        .w-richtext figure.w-richtext-align-center[data-rt-type="image"]>div {
            max-width: 100%
        }
        
        .w-richtext figure.w-richtext-align-normal {
            clear: both
        }
        
        .w-richtext figure.w-richtext-align-fullwidth {
            width: 100%;
            max-width: 100%;
            text-align: center;
            clear: both;
            display: block;
            margin-right: auto;
            margin-left: auto
        }
        
        .w-richtext figure.w-richtext-align-fullwidth>div {
            display: inline-block;
            padding-bottom: inherit
        }
        
        .w-richtext figure.w-richtext-align-fullwidth>figcaption {
            display: block
        }
        
        .w-richtext figure.w-richtext-align-floatleft {
            float: left;
            margin-right: 15px;
            clear: none
        }
        
        .w-richtext figure.w-richtext-align-floatright {
            float: right;
            margin-left: 15px;
            clear: none
        }
        
        .w-nav {
            position: relative;
            background: #dddddd;
            z-index: 1000
        }
        
        .w-nav:before,
        .w-nav:after {
            content: " ";
            display: table;
            grid-column-start: 1;
            grid-row-start: 1;
            grid-column-end: 2;
            grid-row-end: 2
        }
        
        .w-nav:after {
            clear: both
        }
        
        .w-nav-brand {
            position: relative;
            float: left;
            text-decoration: none;
            color: #333333
        }
        
        .w-nav-link {
            position: relative;
            display: inline-block;
            vertical-align: top;
            text-decoration: none;
            color: #222222;
            padding: 20px;
            text-align: left;
            margin-left: auto;
            margin-right: auto
        }
        
        .w-nav-link.w--current {
            color: #0082f3
        }
        
        .w-nav-menu {
            position: relative;
            float: right
        }
        
        [data-nav-menu-open] {
            display: block !important;
            position: absolute;
            top: 100%;
            left: 0;
            right: 0;
            background: #C8C8C8;
            text-align: center;
            overflow: visible;
            min-width: 200px
        }
        
        .w--nav-link-open {
            display: block;
            position: relative
        }
        
        .w-nav-overlay {
            position: absolute;
            overflow: hidden;
            display: none;
            top: 100%;
            left: 0;
            right: 0;
            width: 100%
        }
        
        .w-nav-overlay [data-nav-menu-open] {
            top: 0
        }
        
        .w-nav[data-animation="over-left"] .w-nav-overlay {
            width: auto
        }
        
        .w-nav[data-animation="over-left"] .w-nav-overlay,
        .w-nav[data-animation="over-left"] [data-nav-menu-open] {
            right: auto;
            z-index: 1;
            top: 0
        }
        
        .w-nav[data-animation="over-right"] .w-nav-overlay {
            width: auto
        }
        
        .w-nav[data-animation="over-right"] .w-nav-overlay,
        .w-nav[data-animation="over-right"] [data-nav-menu-open] {
            left: auto;
            z-index: 1;
            top: 0
        }
        
        .w-nav-button {
            position: relative;
            float: right;
            padding: 18px;
            font-size: 24px;
            display: none;
            cursor: pointer;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
            tap-highlight-color: rgba(0, 0, 0, 0);
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none
        }
        
        .w-nav-button:focus {
            outline: 0
        }
        
        .w-nav-button.w--open {
            background-color: #C8C8C8;
            color: white
        }
        
        .w-nav[data-collapse="all"] .w-nav-menu {
            display: none
        }
        
        .w-nav[data-collapse="all"] .w-nav-button {
            display: block
        }
        
        .w--nav-dropdown-open {
            display: block
        }
        
        .w--nav-dropdown-toggle-open {
            display: block
        }
        
        .w--nav-dropdown-list-open {
            position: static
        }
        
        @media screen and (max-width:991px) {
            .w-nav[data-collapse="medium"] .w-nav-menu {
                display: none
            }
            .w-nav[data-collapse="medium"] .w-nav-button {
                display: block
            }
        }
        
        @media screen and (max-width:767px) {
            .w-nav[data-collapse="small"] .w-nav-menu {
                display: none
            }
            .w-nav[data-collapse="small"] .w-nav-button {
                display: block
            }
            .w-nav-brand {
                padding-left: 10px
            }
        }
        
        @media screen and (max-width:479px) {
            .w-nav[data-collapse="tiny"] .w-nav-menu {
                display: none
            }
            .w-nav[data-collapse="tiny"] .w-nav-button {
                display: block
            }
        }
        
        .w-tabs {
            position: relative
        }
        
        .w-tabs:before,
        .w-tabs:after {
            content: " ";
            display: table;
            grid-column-start: 1;
            grid-row-start: 1;
            grid-column-end: 2;
            grid-row-end: 2
        }
        
        .w-tabs:after {
            clear: both
        }
        
        .w-tab-menu {
            position: relative
        }
        
        .w-tab-link {
            position: relative;
            display: inline-block;
            vertical-align: top;
            text-decoration: none;
            padding: 9px 30px;
            text-align: left;
            cursor: pointer;
            color: #222222;
            background-color: #dddddd
        }
        
        .w-tab-link.w--current {
            background-color: #C8C8C8
        }
        
        .w-tab-link:focus {
            outline: 0
        }
        
        .w-tab-content {
            position: relative;
            display: block;
            overflow: hidden
        }
        
        .w-tab-pane {
            position: relative;
            display: none
        }
        
        .w--tab-active {
            display: block
        }
        
        @media screen and (max-width:479px) {
            .w-tab-link {
                display: block
            }
        }
        
        .w-ix-emptyfix:after {
            content: ""
        }
        
        @keyframes spin {
            0% {
                transform: rotate(0deg)
            }
            100% {
                transform: rotate(360deg)
            }
        }
        
        .w-dyn-empty {
            padding: 10px;
            background-color: #dddddd
        }
        
        .w-dyn-hide {
            display: none !important
        }
        
        .w-dyn-bind-empty {
            display: none !important
        }
        
        .w-condition-invisible {
            display: none !important
        }
        /* ==========================================================================
        Start of custom Webflow CSS
        ========================================================================== */
        
        .graph_section_1 {
            min-width: 750px;
            min-height: 450px;
            margin-top: 20px;
            padding-bottom: 20px;
        }
        
        .graph_container_1 {
            max-width: 90%;
            margin-top: 10px;
        }
        
        .graph_dblock_main_1 {
            height: 95vh;
            box-shadow: 0 0 10px 1px #000;
        }
        
        .heading_dblock {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            height: 6%;
            margin-top: 0px;
            -webkit-box-orient: horizontal;
            -webkit-box-direction: normal;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-flex-wrap: nowrap;
            -ms-flex-wrap: nowrap;
            flex-wrap: nowrap;
            -webkit-box-align: center;
            -webkit-align-items: center;
            -ms-flex-align: center;
            align-items: center;
            -webkit-align-content: stretch;
            -ms-flex-line-pack: stretch;
            align-content: stretch;
            background-color: #cfcfcf;
        }
        
        .column {
            margin-right: auto;
            margin-left: auto;
            -o-object-fit: fill;
            object-fit: fill;
        }
        
        .head_box_dblock {
            height: 100%;
            -webkit-box-flex: 0;
            -webkit-flex: 0 33.333333333333336%;
            -ms-flex: 0 33.333333333333336%;
            flex: 0 33.333333333333336%;
        }
        
        .marging_dblock {
            width: 100%;
            height: 3%;
        }
        
        .head_text_dblock_1 {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            width: 100%;
            height: 100%;
            margin-right: 0px;
            margin-left: 0px;
            padding-left: 60px;
            -webkit-box-align: stretch;
            -webkit-align-items: stretch;
            -ms-flex-align: stretch;
            align-items: stretch;
        }
        
        .head_text_dblock_1._2 {
            margin-left: 0px;
        }
        
        .head_text_dblock_1._3 {
            margin-right: 20px;
            margin-left: 0px;
        }
        
        .head_text_dblock_2 {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            width: 100%;
            height: 100%;
            margin-right: 0px;
            margin-left: 0px;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-align: stretch;
            -webkit-align-items: stretch;
            -ms-flex-align: stretch;
            align-items: stretch;
        }
        
        .head_text_dblock_2._2 {
            margin-left: 0px;
        }
        
        .head_text_dblock_2._3 {
            margin-right: 20px;
            margin-left: 0px;
        }
        
        .head_text_dblock_3 {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            width: 100%;
            height: 100%;
            margin-right: 0px;
            margin-left: 0px;
            padding-right: 60px;
            -webkit-box-pack: end;
            -webkit-justify-content: flex-end;
            -ms-flex-pack: end;
            justify-content: flex-end;
        }
        
        .head_text_dblock_3._2 {
            margin-left: 0px;
        }
        
        .head_text_dblock_3._3 {
            margin-right: 20px;
            margin-left: 0px;
        }
        
        .div-block-2 {
            width: 100%;
            height: 100%;
        }
        
        .text_heading {
            margin-top: auto;
            margin-bottom: auto;
            font-size: 15px;
            line-height: 1.5;
        }
        
        .summary_dblock {
            height: auto;
            /*            background-color: #ff8;*/
        }
        
        .graph_dblock {
            margin-top: 10px;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            height: 100%;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
            /*            background-color: #dfc;
            color: transparent;*/
        }
        
        .legends_dblock {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            height: 9%;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-flex-wrap: nowrap;
            -ms-flex-wrap: nowrap;
            flex-wrap: nowrap;
            /*            background-color: #ff8;*/
        }
        
        .wrapper_dblock {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            height: 79%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
            flex-direction: column;
            -webkit-box-pack: justify;
            -webkit-justify-content: space-between;
            -ms-flex-pack: justify;
            justify-content: space-between;
        }
        
        .div-block-3 {
            -webkit-box-flex: 0;
            -webkit-flex: 0 16%;
            -ms-flex: 0 16%;
            flex: 0 16%;
            /*  background-color: #c7a109;*/
        }
        
        .div-block-3_4 {
            -webkit-box-flex: 0;
            -webkit-flex: 0 2%;
            -ms-flex: 0 2%;
            flex: 0 2%;
            /*  background-color: #999;*/
        }
        
        .div-block-4 {
            -webkit-box-flex: 0;
            -webkit-flex: 0 16%;
            -ms-flex: 0 16%;
            flex: 0 16%;
            /*  background-color: #999;*/
        }
        
        .div-block-5 {
            -webkit-box-flex: 0;
            -webkit-flex: 0 11%;
            -ms-flex: 0 11%;
            flex: 0 11%;
            /*  background-color: #c7a109;*/
            padding-top: 1%;
            padding-bottom: 3%;
        }
        
        .div-block-5_in {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
            flex-direction: column;
            -webkit-justify-content: space-around;
            -ms-flex-pack: distribute;
            justify-content: space-around;
            border: 1px solid black;
            /*  background-color: #c7a109;*/
        }
        
        .div-block-6 {
            -webkit-box-flex: 0;
            -webkit-flex: 0 55%;
            -ms-flex: 0 55%;
            flex: 0 55%;
            /*  background-color: #999;*/
        }
        
        .div-block-7 {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-flex: 0;
            -webkit-flex: 0 16%;
            -ms-flex: 0 16%;
            flex: 0 16%;
        }
        
        .div-block-7_8 {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-flex: 0;
            -webkit-flex: 0 2%;
            -ms-flex: 0 2%;
            flex: 0 2%;
        }
        
        .div-block-8 {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-flex: 0;
            -webkit-flex: 0 16%;
            -ms-flex: 0 16%;
            flex: 0 16%;
        }
        
        .div-block-9 {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-align: center;
            -webkit-align-items: center;
            -ms-flex-align: center;
            align-items: center;
            -webkit-box-flex: 0;
            -webkit-flex: 0 11%;
            -ms-flex: 0 11%;
            flex: 0 11%;
        }
        
        .div-block-10 {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -webkit-justify-content: space-around;
            -ms-flex-pack: distribute;
            justify-content: space-around;
            -webkit-box-flex: 0;
            -webkit-flex: 0 55%;
            -ms-flex: 0 55%;
            flex: 0 55%;
        }
        
        .div-block-11 {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-align: center;
            -webkit-align-items: center;
            -ms-flex-align: center;
            align-items: center;
            -webkit-box-flex: 0;
            -webkit-flex: 0 50%;
            -ms-flex: 0 50%;
            flex: 0 50%;
        }
        
        .div-block-12 {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-align: center;
            -webkit-align-items: center;
            -ms-flex-align: center;
            align-items: center;
            -webkit-box-flex: 0;
            -webkit-flex: 0 12.5%;
            -ms-flex: 0 12.5%;
            flex: 0 12.5%;
        }
        
        .paragraph_1 {
            padding-right: 10px;
            padding-left: 10px;
            text-align: center;
            font-size: 10px;
            /*            color: #008000;*/
            line-height: 1.5;
            margin-top: 0px;
            margin-bottom: 0px;
        }
        
        .list {
            margin-top: 5px;
            margin-bottom: 5px;
            font-size: 10px;
            line-height: 1.5;
        }
        
        .list_paragraph {
            margin-bottom: 0px;
            font-size: 11px;
            line-height: 1.5;
        }
        
        .div-block-13 {
            height: 100%;
            background-color: transparent;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-align: center;
            -webkit-align-items: center;
            -ms-flex-align: center;
            align-items: center;
            -webkit-box-flex: 0;
            /*    -webkit-flex: 0 14.285714285714286%;
            -ms-flex: 0 14.285714285714286%;
            flex: 0 14.285714285714286%;*/
        }
        
        .form {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-justify-content: space-around;
            -ms-flex-pack: distribute;
            justify-content: space-around;
        }
        
        .date_block {
            -webkit-box-flex: 0;
            -webkit-flex: 0 25%;
            -ms-flex: 0 25%;
            flex: 0 25%;
        }
        
        .buselector_block {
            -webkit-box-flex: 0;
            -webkit-flex: 0 25%;
            -ms-flex: 0 25%;
            flex: 0 25%;
        }
        
        .markselector_block {
            -webkit-box-flex: 0;
            -webkit-flex: 0 25%;
            -ms-flex: 0 25%;
            flex: 0 25%;
        }
        
        .modeselector_block {
            -webkit-box-flex: 0;
            -webkit-flex: 0 25%;
            -ms-flex: 0 25%;
            flex: 0 25%;
        }
        
        .file_block {
            -webkit-box-flex: 0;
            -webkit-flex: 0 25%;
            -ms-flex: 0 25%;
            flex: 0 25%;
        }
        
        .form_container {
            margin-top: 20px;
            margin-bottom: 10px;
        }
        
        .legends_main_block {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: horizontal;
            -webkit-box-direction: normal;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
            -webkit-justify-content: space-around;
            -ms-flex-pack: distribute;
            justify-content: space-around;
            -webkit-box-flex: 0;
            -webkit-flex: 0 11%;
            -ms-flex: 0 11%;
            -webkit-box-align: center;
            -webkit-align-items: center;
            -ms-flex-align: center;
            align-items: center;
            flex: 0 11%;
            /*  background-color: #c7a109;*/
            padding-top: 1%;
            padding-bottom: 3%;
        }
        
        .legends_wrap_block {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            max-height: 100%;
            max-width: 100%;
            min-height: 120px;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
            flex-direction: column;
            -webkit-justify-content: space-around;
            -ms-flex-pack: distribute;
            justify-content: space-around;
            border: 1px solid #000;
        }
        
        .legend_item_block {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            margin-right: 10px;
            margin-left: 10px;
            /*            min-height: 40px;*/
            margin-top: 10px;
            margin-bottom: 10px;
            -webkit-box-pack: start;
            -webkit-justify-content: flex-start;
            -ms-flex-pack: start;
            justify-content: flex-start;
            -webkit-box-align: center;
            -webkit-align-items: center;
            -ms-flex-align: center;
            align-items: center;
            -webkit-box-flex: 0;
            -webkit-flex: 0 auto;
            -ms-flex: 0 auto;
            flex: 0 auto;
        }
        
        .legend_part_block {}
        
        .legend_part_block_in {
            height: 10px;
            width: 10px;
        }
        
        .legend_item_color_box {
            height: 17px;
            width: 17px;
            border-style: solid;
            border-width: 1px;
            border-color: #a3a3a3;
            background-color: #8c14a1;
        }
        
        .legend_item_name_label {
            margin-bottom: 0px;
            font-size: 12px;
        }
    </style>
</head>

<body>
    <div class="form_section">
        <div class="form_conrainer w-container">
            <div class="form_block w-form">
                <form id="dataForm" name="dataForm" data-name="Data Form" class="form">
                    <div class="file_block">
                        <input type="file" class="fileuploader w-input" maxlength="256" name="filefield" data-name="filefield" id="filefield" accept=".xls, .xlsx" />
                    </div>
                    <div class="buselector_block">
                        <select id="buselector" name="buselector" class="buselect w-select" onchange="window.CalculateGraphData(window.excelRows);"></select>
                    </div>
                    <div class="date_block">
                        <input type="date" class="datefield w-input" maxlength="256" name="datefield" data-name="datefield" id="datefield" value="2020-09-30" min="2020-01-01" max="2021-01-07" onchange="window.CalculateGraphData(window.excelRows);" />
                    </div>
                    <div class="markselector_block">
                        <select id="markselector" name="markselector" class="markselect w-select" onchange="window.FifthPageRecalculate(window.excelRows);"></select>
                    </div>
                    <div class="modeselector_block">
                        <select id="modeselector" name="modeselector" class="modeselect w-select" onchange="window.CalculateGraphData(window.excelRows);"></select>
                    </div>

                </form>
            </div>
        </div>
    </div>
    <div class="graph_section_1">
        <div class="graph_container_1 w-container">
            <div data-animation="slide" data-duration="500" data-infinite="1" class="slider w-slider" role="region" aria-label="carousel">
                <div class="w-slider-mask" id="w-slider-mask-0">
                    <div class="w-slide" aria-label="1 of 5" role="group" style="transform: translateX(0px); opacity: 1;">
                        <div class="graph_dblock_main_1">
                            <div class="marging_dblock"></div>
                            <div class="heading_dblock">
                                <div class="head_box_dblock">
                                    <div class="head_text_dblock_1">
                                        <h2 class="text_heading BUType">Ultrasound</h2>
                                    </div>
                                </div>
                                <div class="head_box_dblock">
                                    <div class="head_text_dblock_2">
                                        <h2 class="text_heading">Tenders Market: Total Market</h2>
                                    </div>
                                </div>
                                <div class="head_box_dblock">
                                    <div class="head_text_dblock_3">
                                        <h2 class="text_heading"><strong>Sales Value RUB, Vert.%</strong></h2>
                                    </div>
                                </div>
                            </div>
                            <div class="wrapper_dblock">
                                <div class="summary_dblock">
                                    <ul role="list" class="list">
                                        <li>
                                            <p class="list_paragraph">YTD MS% gainers: GE +10,7%, Siemens +1,5%</p>
                                        </li>
                                        <li>
                                            <p class="list_paragraph">YTD MS% losers: Hitachi –2,4%, Esaote –1,8%, Philips –1,7%, Sonoscape –1,5%, Mindray –0,9%</p>
                                        </li>
                                    </ul>
                                </div>
                                <div class="graph_dblock">
                                    <div class="div-block-3" style="position: relative; height:100%; width:100%">
                                        <!--                        <div class="div-block-13" id="div-block-13_3">-->
                                        <canvas id="canvas1_1" style="width: 100%; height: 100%"></canvas>
                                        <!--                        </div>-->
                                    </div>
                                    <div class="div-block-3_4" style="position: relative; height:100%; width:100%">
                                    </div>
                                    <div class="div-block-4" style="position: relative; height:100%; width:100%">
                                        <!--                        <div class="div-block-13">-->
                                        <canvas id="canvas2_1" style="width: 100%; height: 100%"></canvas>
                                        <!--                        </div>-->
                                    </div>
                                    <div class="legends_main_block">
                                        <div class="legends_wrap_block 0" style="width: 100%;">
                                            <div class="legend_item_block">
                                                <div class="legend_part_block">
                                                    <div class="legend_item_color_box" style="background-color: rgba(0, 114, 218, 1);"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <div class="legend_part_block_in"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <p class="legend_item_name_label">ФИЛИПС</p>
                                                </div>
                                            </div>
                                            <div class="legend_item_block">
                                                <div class="legend_part_block">
                                                    <div class="legend_item_color_box" style="background-color: rgba(0, 156, 73, 1);"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <div class="legend_part_block_in"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <p class="legend_item_name_label">GE</p>
                                                </div>
                                            </div>
                                            <div class="legend_item_block">
                                                <div class="legend_part_block">
                                                    <div class="legend_item_color_box" style="background-color: rgba(240, 66, 121, 1);"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <div class="legend_part_block_in"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <p class="legend_item_name_label">SAMSUNG</p>
                                                </div>
                                            </div>
                                            <div class="legend_item_block">
                                                <div class="legend_part_block">
                                                    <div class="legend_item_color_box" style="background-color: rgba(255, 127, 0, 1);"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <div class="legend_part_block_in"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <p class="legend_item_name_label">SIEMENS</p>
                                                </div>
                                            </div>
                                            <div class="legend_item_block">
                                                <div class="legend_part_block">
                                                    <div class="legend_item_color_box" style="background-color: rgba(167, 122, 215, 1);"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <div class="legend_part_block_in"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <p class="legend_item_name_label">MINDRAY</p>
                                                </div>
                                            </div>
                                            <div class="legend_item_block">
                                                <div class="legend_part_block">
                                                    <div class="legend_item_color_box" style="background-color: rgba(255, 255, 0, 1);"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <div class="legend_part_block_in"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <p class="legend_item_name_label">HITACHI</p>
                                                </div>
                                            </div>
                                            <div class="legend_item_block">
                                                <div class="legend_part_block">
                                                    <div class="legend_item_color_box" style="background-color: rgba(92, 185, 187, 1);"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <div class="legend_part_block_in"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <p class="legend_item_name_label">SONOSCAPE</p>
                                                </div>
                                            </div>
                                            <div class="legend_item_block">
                                                <div class="legend_part_block">
                                                    <div class="legend_item_color_box" style="background-color: rgba(127, 127, 0, 1);"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <div class="legend_part_block_in"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <p class="legend_item_name_label">ESAOTE</p>
                                                </div>
                                            </div>
                                            <div class="legend_item_block">
                                                <div class="legend_part_block">
                                                    <div class="legend_item_color_box" style="background-color: rgba(127, 127, 127, 1);"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <div class="legend_part_block_in"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <p class="legend_item_name_label">Other</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="div-block-6" style="position: relative; height:100%; width:100%">
                                        <!--                        <div class="div-block-13" style="width: 100%; height: 100%">-->
                                        <canvas id="canvas4_1" style="width: 100%; height: 100%"></canvas>
                                        <!--                        </div>-->
                                    </div>
                                </div>
                            </div>
                            <b>
                                <div class="legends_dblock">
                                    <div class="div-block-7">
                                        <div class="div-block-11" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 0">21.8<br><font color="#008000">+152.0%</font></p>
                                        </div>
                                        <div class="div-block-11" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 0">26.8<br><font color="#008000">+22.9%</font></p>
                                        </div>
                                    </div>
                                    <div class="div-block-7_8">
                                    </div>
                                    <div class="div-block-8">
                                        <div class="div-block-11" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 0">15.4<br><font color="#008000">+78.3%</font></p>
                                        </div>
                                        <div class="div-block-11" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 0">22.0<br><font color="#008000">x+42.6%</font></p>
                                        </div>
                                    </div>
                                    <div class="div-block-9" style="width: 100%; height: 100%">
                                        <p class="paragraph_1 0">Bln.RUB<br>+/- % SPLY</p>
                                    </div>
                                    <div class="div-block-10">
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 0">1.2<br><font color="#c00000">-5.1%</font></p>
                                        </div>
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 0">5.4<br><font color="#008000">+95.8%</font></p>
                                        </div>
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 0">8.8<br><font color="#008000">+89.9%</font></p>
                                        </div>
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 0">4.8<br><font color="#c00000">-24.8%</font></p>
                                        </div>
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 0">2.5<br><font color="#008000">+112.2%</font></p>
                                        </div>
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 0">10.0<br><font color="#008000">+84.7%</font></p>
                                        </div>
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 0">9.5<br><font color="#008000">+7.5%</font></p>
                                        </div>
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 0">0<br><font color="#008000">+0%</font></p>
                                        </div>
                                    </div>
                                </div>
                            </b>
                            <div class="marging_dblock"></div>
                        </div>
                    </div>
                    <div class="w-slide" aria-label="2 of 5" role="group" aria-hidden="true" style="transform: translateX(0px); opacity: 1;">
                        <div class="graph_dblock_main_1">
                            <div class="marging_dblock"></div>
                            <div class="heading_dblock">
                                <div class="head_box_dblock">
                                    <div class="head_text_dblock_1">
                                        <h2 class="text_heading BUType">Ultrasound</h2>
                                    </div>
                                </div>
                                <div class="head_box_dblock">
                                    <div class="head_text_dblock_2">
                                        <h2 class="text_heading">Tenders Market: Total Market</h2>
                                    </div>
                                </div>
                                <div class="head_box_dblock">
                                    <div class="head_text_dblock_3">
                                        <h2 class="text_heading"><strong>Sales Value RUB, Vert.%</strong></h2>
                                    </div>
                                </div>
                            </div>
                            <div class="wrapper_dblock">
                                <div class="summary_dblock">
                                    <ul role="list" class="list">
                                        <li>
                                            <p class="list_paragraph">Market growth +42,6% YTD driven by Direct / MTS</p>
                                        </li>
                                        <li>
                                            <p class="list_paragraph">Direct channel (2020 YTD - MTS Moscow only) impacted TTL market dramatically in Q2 / Q3</p>
                                        </li>
                                    </ul>
                                </div>
                                <div class="graph_dblock">
                                    <div class="div-block-3" style="position: relative; height:100%; width:100%">
                                        <canvas id="canvas1_2" style="width: 100%; height: 100%"></canvas>
                                    </div>
                                    <div class="div-block-3_4" style="position: relative; height:100%; width:100%">
                                    </div>
                                    <div class="div-block-4" style="position: relative; height:100%; width:100%">
                                        <canvas id="canvas2_2" style="width: 100%; height: 100%"></canvas>
                                    </div>
                                    <div class="legends_main_block">
                                        <div class="legends_wrap_block 1" style="width: 100%;">
                                            <div class="legend_item_block">
                                                <div class="legend_part_block">
                                                    <div class="legend_item_color_box" style="background-color: rgba(84, 150, 211, 1);"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <div class="legend_part_block_in"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <p class="legend_item_name_label">Direct</p>
                                                </div>
                                            </div>
                                            <div class="legend_item_block">
                                                <div class="legend_part_block">
                                                    <div class="legend_item_color_box" style="background-color: rgba(237, 125, 49, 1);"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <div class="legend_part_block_in"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <p class="legend_item_name_label">Indirect</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="div-block-6" style="position: relative; height:100%; width:100%">
                                        <!--                        <div class="div-block-13" style="width: 100%; height: 100%">-->
                                        <canvas id="canvas4_2" style="width: 100%; height: 100%"></canvas>
                                        <!--                        </div>-->
                                    </div>
                                </div>
                            </div>
                            <b>
                                <div class="legends_dblock">
                                    <div class="div-block-7">
                                        <div class="div-block-11" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 1">21.8<br><font color="#008000">+152.0%</font></p>
                                        </div>
                                        <div class="div-block-11" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 1">26.8<br><font color="#008000">+22.9%</font></p>
                                        </div>
                                    </div>
                                    <div class="div-block-7_8">
                                    </div>
                                    <div class="div-block-8">
                                        <div class="div-block-11" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 1">15.4<br><font color="#008000">+78.3%</font></p>
                                        </div>
                                        <div class="div-block-11" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 1">22.0<br><font color="#008000">+42.6%</font></p>
                                        </div>
                                    </div>
                                    <div class="div-block-9" style="width: 100%; height: 100%">
                                        <p class="paragraph_1 1">Bln.RUB<br>+/- % SPLY</p>
                                    </div>
                                    <div class="div-block-10">
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 1">1.2<br><font color="#c00000">-5.1%</font></p>
                                        </div>
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 1">5.4<br><font color="#008000">+95.8%</font></p>
                                        </div>
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 1">8.8<br><font color="#008000">+89.9%</font></p>
                                        </div>
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 1">4.8<br><font color="#c00000">-24.8%</font></p>
                                        </div>
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 1">2.5<br><font color="#008000">+112.2%</font></p>
                                        </div>
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 1">10.0<br><font color="#008000">+84.7%</font></p>
                                        </div>
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 1">9.5<br><font color="#008000">+7.5%</font></p>
                                        </div>
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 1">0<br><font color="#008000">+0%</font></p>
                                        </div>
                                    </div>
                                </div>
                            </b>
                            <div class="marging_dblock"></div>
                        </div>
                    </div>
                    <div class="w-slide" aria-label="3 of 5" role="group" aria-hidden="true" style="transform: translateX(0px); opacity: 1;">
                        <div class="graph_dblock_main_1">
                            <div class="marging_dblock"></div>
                            <div class="heading_dblock">
                                <div class="head_box_dblock">
                                    <div class="head_text_dblock_1">
                                        <h2 class="text_heading BUType">Ultrasound</h2>
                                    </div>
                                </div>
                                <div class="head_box_dblock">
                                    <div class="head_text_dblock_2">
                                        <h2 class="text_heading">Tenders Market: Indirect Channel</h2>
                                    </div>
                                </div>
                                <div class="head_box_dblock">
                                    <div class="head_text_dblock_3">
                                        <h2 class="text_heading"><strong>Sales Value RUB, Vert.%</strong></h2>
                                    </div>
                                </div>
                            </div>
                            <div class="wrapper_dblock">
                                <div class="summary_dblock">
                                    <ul role="list" class="list">
                                        <li>
                                            <p class="list_paragraph">Indirect channel +/- flat Sep’20 YTD, with strong growth Q1 / Q2 reversing to sharp decline in Q3</p>
                                        </li>
                                        <li>
                                            <p class="list_paragraph">YTD MS% gainers: SAMSUNG +5,5%, Mindray +1,4%, GE +0,9%</p>
                                        </li>
                                        <li>
                                            <p class="list_paragraph">YTD MS% losers: Philips –2,7%, Esaote –2,3%, Siemens –1,2%, Hitachi –0,9%</p>
                                        </li>
                                    </ul>
                                </div>
                                <div class="graph_dblock">
                                    <div class="div-block-3" style="position: relative; height:100%; width:100%">
                                        <canvas id="canvas1_3" style="width: 100%; height: 100%"></canvas>
                                    </div>
                                    <div class="div-block-3_4" style="position: relative; height:100%; width:100%">
                                    </div>
                                    <div class="div-block-4" style="position: relative; height:100%; width:100%">
                                        <canvas id="canvas2_3" style="width: 100%; height: 100%"></canvas>
                                    </div>
                                    <div class="legends_main_block">
                                        <div class="legends_wrap_block 2" style="width: 100%;">
                                            <div class="legend_item_block">
                                                <div class="legend_part_block">
                                                    <div class="legend_item_color_box" style="background-color: rgba(0, 114, 218, 1);"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <div class="legend_part_block_in"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <p class="legend_item_name_label">ФИЛИПС</p>
                                                </div>
                                            </div>
                                            <div class="legend_item_block">
                                                <div class="legend_part_block">
                                                    <div class="legend_item_color_box" style="background-color: rgba(0, 156, 73, 1);"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <div class="legend_part_block_in"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <p class="legend_item_name_label">GE</p>
                                                </div>
                                            </div>
                                            <div class="legend_item_block">
                                                <div class="legend_part_block">
                                                    <div class="legend_item_color_box" style="background-color: rgba(240, 66, 121, 1);"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <div class="legend_part_block_in"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <p class="legend_item_name_label">SAMSUNG</p>
                                                </div>
                                            </div>
                                            <div class="legend_item_block">
                                                <div class="legend_part_block">
                                                    <div class="legend_item_color_box" style="background-color: rgba(255, 127, 0, 1);"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <div class="legend_part_block_in"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <p class="legend_item_name_label">SIEMENS</p>
                                                </div>
                                            </div>
                                            <div class="legend_item_block">
                                                <div class="legend_part_block">
                                                    <div class="legend_item_color_box" style="background-color: rgba(167, 122, 215, 1);"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <div class="legend_part_block_in"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <p class="legend_item_name_label">MINDRAY</p>
                                                </div>
                                            </div>
                                            <div class="legend_item_block">
                                                <div class="legend_part_block">
                                                    <div class="legend_item_color_box" style="background-color: rgba(255, 255, 0, 1);"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <div class="legend_part_block_in"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <p class="legend_item_name_label">HITACHI</p>
                                                </div>
                                            </div>
                                            <div class="legend_item_block">
                                                <div class="legend_part_block">
                                                    <div class="legend_item_color_box" style="background-color: rgba(92, 185, 187, 1);"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <div class="legend_part_block_in"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <p class="legend_item_name_label">SONOSCAPE</p>
                                                </div>
                                            </div>
                                            <div class="legend_item_block">
                                                <div class="legend_part_block">
                                                    <div class="legend_item_color_box" style="background-color: rgba(127, 127, 0, 1);"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <div class="legend_part_block_in"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <p class="legend_item_name_label">ESAOTE</p>
                                                </div>
                                            </div>
                                            <div class="legend_item_block">
                                                <div class="legend_part_block">
                                                    <div class="legend_item_color_box" style="background-color: rgba(127, 127, 127, 1);"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <div class="legend_part_block_in"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <p class="legend_item_name_label">Other</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="div-block-6" style="position: relative; height:100%; width:100%">
                                        <canvas id="canvas4_3" style="width: 100%; height: 100%"></canvas>
                                    </div>
                                </div>
                            </div>
                            <b>
                                <div class="legends_dblock">
                                    <div class="div-block-7">
                                        <div class="div-block-11" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 2">21.2<br><font color="#008000">+145.4%</font></p>
                                        </div>
                                        <div class="div-block-11" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 2">20.0<br><font color="#c00000">-5.8%</font></p>
                                        </div>
                                    </div>
                                    <div class="div-block-7_8">
                                    </div>
                                    <div class="div-block-8">
                                        <div class="div-block-11" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 2">14.9<br><font color="#008000">+71.7%</font></p>
                                        </div>
                                        <div class="div-block-11" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 2">15.2<br><font color="#008000">+2.4%</font></p>
                                        </div>
                                    </div>
                                    <div class="div-block-9" style="width: 100%; height: 100%">
                                        <p class="paragraph_1 2">Bln.RUB<br>+/- % SPLY</p>
                                    </div>
                                    <div class="div-block-10">
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 2">1.2<br><font color="#c00000">-5.1%</font></p>
                                        </div>
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 2">5.4<br><font color="#008000">+95.7%</font></p>
                                        </div>
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 2">8.3<br><font color="#008000">+77.7%</font></p>
                                        </div>
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 2">4.8<br><font color="#c00000">-24.8%</font></p>
                                        </div>
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 2">2.5<br><font color="#008000">+112.2%</font></p>
                                        </div>
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 2">8.1<br><font color="#008000">+48.5%</font></p>
                                        </div>
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 2">4.7<br><font color="#c00000">-43.4%</font></p>
                                        </div>
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 2">0<br><font color="#008000">+0%</font></p>
                                        </div>
                                    </div>
                                </div>
                            </b>
                            <div class="marging_dblock"></div>
                        </div>
                    </div>
                    <div class="w-slide" aria-label="4 of 5" role="group" aria-hidden="true" style="transform: translateX(0px); opacity: 1;">
                        <div class="graph_dblock_main_1">
                            <div class="marging_dblock"></div>
                            <div class="heading_dblock">
                                <div class="head_box_dblock">
                                    <div class="head_text_dblock_1">
                                        <h2 class="text_heading BUType">Ultrasound</h2>
                                    </div>
                                </div>
                                <div class="head_box_dblock">
                                    <div class="head_text_dblock_2">
                                        <h2 class="text_heading">Tenders Market: Indirect Channel</h2>
                                    </div>
                                </div>
                                <div class="head_box_dblock">
                                    <div class="head_text_dblock_3">
                                        <h2 class="text_heading"><strong>Sales Value RUB, Vert.%</strong></h2>
                                    </div>
                                </div>
                            </div>
                            <div class="wrapper_dblock">
                                <div class="summary_dblock">
                                    <ul role="list" class="list">
                                        <li>
                                            <p class="list_paragraph">Indirect channel +/- flat Sep’20 YTD, with strong growth Q1 / Q2 reversing to sharp decline in Q3</p>
                                        </li>
                                        <li>
                                            <p class="list_paragraph">Major segments are +/- flat except Low-end compact boosted by Covid / emergency orders in Q2’20</p>
                                        </li>
                                        <li>
                                            <p class="list_paragraph">Handheld (Lumify etc.) remains marginal segment</p>
                                        </li>
                                    </ul>
                                </div>
                                <div class="graph_dblock">
                                    <div class="div-block-3" style="position: relative; height:100%; width:100%">
                                        <canvas id="canvas1_4" style="width: 100%; height: 100%"></canvas>
                                    </div>
                                    <div class="div-block-3_4" style="position: relative; height:100%; width:100%">
                                    </div>
                                    <div class="div-block-4" style="position: relative; height:100%; width:100%">
                                        <canvas id="canvas2_4" style="width: 100%; height: 100%"></canvas>
                                    </div>
                                    <div class="legends_main_block">
                                        <div class="legends_wrap_block 3" style="width: 100%;">
                                            <div class="legend_item_block">
                                                <div class="legend_part_block">
                                                    <div class="legend_item_color_box" style="background-color: rgba(37, 93, 144, 1);"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <div class="legend_part_block_in"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <p class="legend_item_name_label">Premium cart 8M+</p>
                                                </div>
                                            </div>
                                            <div class="legend_item_block">
                                                <div class="legend_part_block">
                                                    <div class="legend_item_color_box" style="background-color: rgba(112, 173, 70, 1);"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <div class="legend_part_block_in"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <p class="legend_item_name_label">High-end cart 4-8M</p>
                                                </div>
                                            </div>
                                            <div class="legend_item_block">
                                                <div class="legend_part_block">
                                                    <div class="legend_item_color_box" style="background-color: rgba(68, 115, 197, 1);"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <div class="legend_part_block_in"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <p class="legend_item_name_label">Low-end cart 0-4M</p>
                                                </div>
                                            </div>
                                            <div class="legend_item_block">
                                                <div class="legend_part_block">
                                                    <div class="legend_item_color_box" style="background-color: rgba(255, 192, 0, 1);"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <div class="legend_part_block_in"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <p class="legend_item_name_label">High-end compact 4M+</p>
                                                </div>
                                            </div>
                                            <div class="legend_item_block">
                                                <div class="legend_part_block">
                                                    <div class="legend_item_color_box" style="background-color: rgba(165, 165, 165, 1);"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <div class="legend_part_block_in"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <p class="legend_item_name_label">Low-end compact 0-4M</p>
                                                </div>
                                            </div>
                                            <div class="legend_item_block">
                                                <div class="legend_part_block">
                                                    <div class="legend_item_color_box" style="background-color: rgba(238, 125, 49, 1);"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <div class="legend_part_block_in"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <p class="legend_item_name_label">Handheld</p>
                                                </div>
                                            </div>
                                            <div class="legend_item_block">
                                                <div class="legend_part_block">
                                                    <div class="legend_item_color_box" style="background-color: rgba(90, 155, 213, 1);"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <div class="legend_part_block_in"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <p class="legend_item_name_label">~</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="div-block-6" style="position: relative; height:100%; width:100%">
                                        <canvas id="canvas4_4" style="width: 100%; height: 100%"></canvas>
                                    </div>
                                </div>
                            </div>
                            <b>
                                <div class="legends_dblock">
                                    <div class="div-block-7">
                                        <div class="div-block-11" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 3">21.2<br><font color="#008000">+145.4%</font></p>
                                        </div>
                                        <div class="div-block-11" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 3">20.0<br><font color="#c00000">-5.8%</font></p>
                                        </div>
                                    </div>
                                    <div class="div-block-7_8">
                                    </div>
                                    <div class="div-block-8">
                                        <div class="div-block-11" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 3">14.9<br><font color="#008000">+71.7%</font></p>
                                        </div>
                                        <div class="div-block-11" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 3">15.2<br><font color="#008000">+2.4%</font></p>
                                        </div>
                                    </div>
                                    <div class="div-block-9" style="width: 100%; height: 100%">
                                        <p class="paragraph_1 3">Bln.RUB<br>+/- % SPLY</p>
                                    </div>
                                    <div class="div-block-10">
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 3">1.2<br><font color="#c00000">-5.1%</font></p>
                                        </div>
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 3">5.4<br><font color="#008000">+95.7%</font></p>
                                        </div>
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 3">8.3<br><font color="#008000">+77.7%</font></p>
                                        </div>
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 3">4.8<br><font color="#c00000">-24.8%</font></p>
                                        </div>
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 3">2.5<br><font color="#008000">+112.2%</font></p>
                                        </div>
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 3">8.1<br><font color="#008000">+48.5%</font></p>
                                        </div>
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 3">4.7<br><font color="#c00000">-43.4%</font></p>
                                        </div>
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 3">0<br><font color="#008000">+0%</font></p>
                                        </div>
                                    </div>
                                </div>
                            </b>
                            <div class="marging_dblock"></div>
                        </div>
                    </div>
                    <div class="w-slide" aria-label="5 of 5" role="group" aria-hidden="true" style="transform: translateX(0px); opacity: 1;">
                        <div class="graph_dblock_main_1">
                            <div class="marging_dblock"></div>
                            <div class="heading_dblock">
                                <div class="head_box_dblock">
                                    <div class="head_text_dblock_1">
                                        <h2 class="text_heading BUType">Ultrasound</h2>
                                    </div>
                                </div>
                                <div class="head_box_dblock">
                                    <div class="head_text_dblock_2">
                                        <h2 class="text_heading FifthPageMark">SAMSUNG: Indirect Channel</h2>
                                    </div>
                                </div>
                                <div class="head_box_dblock">
                                    <div class="head_text_dblock_3">
                                        <h2 class="text_heading"><strong>Sales Value RUB, Vert.%</strong></h2>
                                    </div>
                                </div>
                            </div>
                            <div class="wrapper_dblock">
                                <div class="summary_dblock">
                                    <ul role="list" class="list">
                                        <li>
                                            <p class="list_paragraph">MS drivers for SAMSUNG: High-end Cart / Low-end Compact</p>
                                        </li>
                                        <li>
                                            <p class="list_paragraph">High-end Cart: extended portfolio (6 models)</p>
                                        </li>
                                        <li>
                                            <p class="list_paragraph">Low-end Compact: Covid / Emergency demand with decreased lead-times</p>
                                        </li>
                                    </ul>
                                </div>
                                <div class="graph_dblock">
                                    <div class="div-block-3" style="position: relative; height:100%; width:100%">
                                        <canvas id="canvas1_5" style="width: 100%; height: 100%"></canvas>
                                    </div>
                                    <div class="div-block-3_4" style="position: relative; height:100%; width:100%">
                                    </div>
                                    <div class="div-block-4" style="position: relative; height:100%; width:100%">
                                        <canvas id="canvas2_5" style="width: 100%; height: 100%"></canvas>
                                    </div>
                                    <div class="legends_main_block">
                                        <div class="legends_wrap_block 4" style="width: 100%;">
                                            <div class="legend_item_block">
                                                <div class="legend_part_block">
                                                    <div class="legend_item_color_box" style="background-color: rgba(37, 93, 144, 1);"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <div class="legend_part_block_in"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <p class="legend_item_name_label">Premium cart 8M+</p>
                                                </div>
                                            </div>
                                            <div class="legend_item_block">
                                                <div class="legend_part_block">
                                                    <div class="legend_item_color_box" style="background-color: rgba(112, 173, 70, 1);"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <div class="legend_part_block_in"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <p class="legend_item_name_label">High-end cart 4-8M</p>
                                                </div>
                                            </div>
                                            <div class="legend_item_block">
                                                <div class="legend_part_block">
                                                    <div class="legend_item_color_box" style="background-color: rgba(68, 115, 197, 1);"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <div class="legend_part_block_in"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <p class="legend_item_name_label">Low-end cart 0-4M</p>
                                                </div>
                                            </div>
                                            <div class="legend_item_block">
                                                <div class="legend_part_block">
                                                    <div class="legend_item_color_box" style="background-color: rgba(255, 192, 0, 1);"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <div class="legend_part_block_in"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <p class="legend_item_name_label">High-end compact 4M+</p>
                                                </div>
                                            </div>
                                            <div class="legend_item_block">
                                                <div class="legend_part_block">
                                                    <div class="legend_item_color_box" style="background-color: rgba(165, 165, 165, 1);"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <div class="legend_part_block_in"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <p class="legend_item_name_label">Low-end compact 0-4M</p>
                                                </div>
                                            </div>
                                            <div class="legend_item_block">
                                                <div class="legend_part_block">
                                                    <div class="legend_item_color_box" style="background-color: rgba(238, 125, 49, 1);"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <div class="legend_part_block_in"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <p class="legend_item_name_label">Handheld</p>
                                                </div>
                                            </div>
                                            <div class="legend_item_block">
                                                <div class="legend_part_block">
                                                    <div class="legend_item_color_box" style="background-color: rgba(90, 155, 213, 1);"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <div class="legend_part_block_in"></div>
                                                </div>
                                                <div class="legend_part_block">
                                                    <p class="legend_item_name_label">~</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="div-block-6" style="position: relative; height:100%; width:100%">
                                        <canvas id="canvas4_5" style="width: 100%; height: 100%"></canvas>
                                    </div>
                                </div>
                            </div>
                            <b>
                                <div class="legends_dblock">
                                    <div class="div-block-7">
                                        <div class="div-block-11" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 4">3.1<br><font color="#008000">+175.0%</font></p>
                                        </div>
                                        <div class="div-block-11" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 4">3.5<br><font color="#008000">+13.0%</font></p>
                                        </div>
                                    </div>
                                    <div class="div-block-7_8">
                                    </div>
                                    <div class="div-block-8">
                                        <div class="div-block-11" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 4">2.1<br><font color="#008000">+87.0%</font></p>
                                        </div>
                                        <div class="div-block-11" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 4">3.0<br><font color="#008000">+42.0%</font></p>
                                        </div>
                                    </div>
                                    <div class="div-block-9" style="width: 100%; height: 100%">
                                        <p class="paragraph_1 4">Bln.RUB<br>+/- % SPLY</p>
                                    </div>
                                    <div class="div-block-10">
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 4">0.1<br><font color="#c00000">-36.0%</font></p>
                                        </div>
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 4">0.8<br><font color="#008000">+133.0%</font></p>
                                        </div>
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 4">1.2<br><font color="#008000">+89.0%</font></p>
                                        </div>
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 4">0.5<br><font color="#c00000">-49.0%</font></p>
                                        </div>
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 4">0.3<br><font color="#008000">+261.0%</font></p>
                                        </div>
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 4">1.7<br><font color="#008000">+119.0%</font></p>
                                        </div>
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 4">1.0<br><font color="#c00000">-22.0%</font></p>
                                        </div>
                                        <div class="div-block-12" style="width: 100%; height: 100%">
                                            <p class="paragraph_1 4">0<br><font color="#008000">+0%</font></p>
                                        </div>
                                    </div>
                                </div>
                            </b>
                            <div class="marging_dblock"></div>
                        </div>
                    </div>
                    <div aria-live="off" aria-atomic="true" class="w-slider-aria-label" data-wf-ignore=""></div>
                </div>
                <div class="w-slider-arrow-left" role="button" tabindex="0" aria-controls="w-slider-mask-0" aria-label="previous slide">
                    <div class="w-icon-slider-left"></div>
                </div>
                <div class="w-slider-arrow-right" role="button" tabindex="0" aria-controls="w-slider-mask-0" aria-label="next slide">
                    <div class="w-icon-slider-right"></div>
                </div>
                <div class="w-slider-nav w-round">
                    <div class="w-slider-dot w-active" data-wf-ignore="" aria-label="Show slide 1 of 2" aria-selected="true" role="button" tabindex="0"></div>
                    <div class="w-slider-dot" data-wf-ignore="" aria-label="Show slide 2 of 2" aria-selected="false" role="button" tabindex="-1"></div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        /*! jQuery v3.5.1 | (c) JS Foundation and other contributors | jquery.org/license */ ! function(e, t) {
            "use strict";
            "object" == typeof module && "object" == typeof module.exports ? module.exports = e.document ? t(e, !0) : function(e) {
                if (!e.document) throw new Error("jQuery requires a window with a document");
                return t(e)
            } : t(e)
        }("undefined" != typeof window ? window : this, function(C, e) {
            "use strict";
            var t = [],
                r = Object.getPrototypeOf,
                s = t.slice,
                g = t.flat ? function(e) {
                    return t.flat.call(e)
                } : function(e) {
                    return t.concat.apply([], e)
                },
                u = t.push,
                i = t.indexOf,
                n = {},
                o = n.toString,
                v = n.hasOwnProperty,
                a = v.toString,
                l = a.call(Object),
                y = {},
                m = function(e) {
                    return "function" == typeof e && "number" != typeof e.nodeType
                },
                x = function(e) {
                    return null != e && e === e.window
                },
                E = C.document,
                c = {
                    type: !0,
                    src: !0,
                    nonce: !0,
                    noModule: !0
                };

            function b(e, t, n) {
                var r, i, o = (n = n || E).createElement("script");
                if (o.text = e, t)
                    for (r in c)(i = t[r] || t.getAttribute && t.getAttribute(r)) && o.setAttribute(r, i);
                n.head.appendChild(o).parentNode.removeChild(o)
            }

            function w(e) {
                return null == e ? e + "" : "object" == typeof e || "function" == typeof e ? n[o.call(e)] || "object" : typeof e
            }
            var f = "3.5.1",
                S = function(e, t) {
                    return new S.fn.init(e, t)
                };

            function p(e) {
                var t = !!e && "length" in e && e.length,
                    n = w(e);
                return !m(e) && !x(e) && ("array" === n || 0 === t || "number" == typeof t && 0 < t && t - 1 in e)
            }
            S.fn = S.prototype = {
                jquery: f,
                constructor: S,
                length: 0,
                toArray: function() {
                    return s.call(this)
                },
                get: function(e) {
                    return null == e ? s.call(this) : e < 0 ? this[e + this.length] : this[e]
                },
                pushStack: function(e) {
                    var t = S.merge(this.constructor(), e);
                    return t.prevObject = this, t
                },
                each: function(e) {
                    return S.each(this, e)
                },
                map: function(n) {
                    return this.pushStack(S.map(this, function(e, t) {
                        return n.call(e, t, e)
                    }))
                },
                slice: function() {
                    return this.pushStack(s.apply(this, arguments))
                },
                first: function() {
                    return this.eq(0)
                },
                last: function() {
                    return this.eq(-1)
                },
                even: function() {
                    return this.pushStack(S.grep(this, function(e, t) {
                        return (t + 1) % 2
                    }))
                },
                odd: function() {
                    return this.pushStack(S.grep(this, function(e, t) {
                        return t % 2
                    }))
                },
                eq: function(e) {
                    var t = this.length,
                        n = +e + (e < 0 ? t : 0);
                    return this.pushStack(0 <= n && n < t ? [this[n]] : [])
                },
                end: function() {
                    return this.prevObject || this.constructor()
                },
                push: u,
                sort: t.sort,
                splice: t.splice
            }, S.extend = S.fn.extend = function() {
                var e, t, n, r, i, o, a = arguments[0] || {},
                    s = 1,
                    u = arguments.length,
                    l = !1;
                for ("boolean" == typeof a && (l = a, a = arguments[s] || {}, s++), "object" == typeof a || m(a) || (a = {}), s === u && (a = this, s--); s < u; s++)
                    if (null != (e = arguments[s]))
                        for (t in e) r = e[t], "__proto__" !== t && a !== r && (l && r && (S.isPlainObject(r) || (i = Array.isArray(r))) ? (n = a[t], o = i && !Array.isArray(n) ? [] : i || S.isPlainObject(n) ? n : {}, i = !1, a[t] = S.extend(l, o, r)) : void 0 !== r && (a[t] = r));
                return a
            }, S.extend({
                expando: "jQuery" + (f + Math.random()).replace(/\D/g, ""),
                isReady: !0,
                error: function(e) {
                    throw new Error(e)
                },
                noop: function() {},
                isPlainObject: function(e) {
                    var t, n;
                    return !(!e || "[object Object]" !== o.call(e)) && (!(t = r(e)) || "function" == typeof(n = v.call(t, "constructor") && t.constructor) && a.call(n) === l)
                },
                isEmptyObject: function(e) {
                    var t;
                    for (t in e) return !1;
                    return !0
                },
                globalEval: function(e, t, n) {
                    b(e, {
                        nonce: t && t.nonce
                    }, n)
                },
                each: function(e, t) {
                    var n, r = 0;
                    if (p(e)) {
                        for (n = e.length; r < n; r++)
                            if (!1 === t.call(e[r], r, e[r])) break
                    } else
                        for (r in e)
                            if (!1 === t.call(e[r], r, e[r])) break; return e
                },
                makeArray: function(e, t) {
                    var n = t || [];
                    return null != e && (p(Object(e)) ? S.merge(n, "string" == typeof e ? [e] : e) : u.call(n, e)), n
                },
                inArray: function(e, t, n) {
                    return null == t ? -1 : i.call(t, e, n)
                },
                merge: function(e, t) {
                    for (var n = +t.length, r = 0, i = e.length; r < n; r++) e[i++] = t[r];
                    return e.length = i, e
                },
                grep: function(e, t, n) {
                    for (var r = [], i = 0, o = e.length, a = !n; i < o; i++) !t(e[i], i) !== a && r.push(e[i]);
                    return r
                },
                map: function(e, t, n) {
                    var r, i, o = 0,
                        a = [];
                    if (p(e))
                        for (r = e.length; o < r; o++) null != (i = t(e[o], o, n)) && a.push(i);
                    else
                        for (o in e) null != (i = t(e[o], o, n)) && a.push(i);
                    return g(a)
                },
                guid: 1,
                support: y
            }), "function" == typeof Symbol && (S.fn[Symbol.iterator] = t[Symbol.iterator]), S.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function(e, t) {
                n["[object " + t + "]"] = t.toLowerCase()
            });
            var d = function(n) {
                var e, d, b, o, i, h, f, g, w, u, l, T, C, a, E, v, s, c, y, S = "sizzle" + 1 * new Date,
                    p = n.document,
                    k = 0,
                    r = 0,
                    m = ue(),
                    x = ue(),
                    A = ue(),
                    N = ue(),
                    D = function(e, t) {
                        return e === t && (l = !0), 0
                    },
                    j = {}.hasOwnProperty,
                    t = [],
                    q = t.pop,
                    L = t.push,
                    H = t.push,
                    O = t.slice,
                    P = function(e, t) {
                        for (var n = 0, r = e.length; n < r; n++)
                            if (e[n] === t) return n;
                        return -1
                    },
                    R = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
                    M = "[\\x20\\t\\r\\n\\f]",
                    I = "(?:\\\\[\\da-fA-F]{1,6}" + M + "?|\\\\[^\\r\\n\\f]|[\\w-]|[^\0-\\x7f])+",
                    W = "\\[" + M + "*(" + I + ")(?:" + M + "*([*^$|!~]?=)" + M + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + I + "))|)" + M + "*\\]",
                    F = ":(" + I + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + W + ")*)|.*)\\)|)",
                    B = new RegExp(M + "+", "g"),
                    $ = new RegExp("^" + M + "+|((?:^|[^\\\\])(?:\\\\.)*)" + M + "+$", "g"),
                    _ = new RegExp("^" + M + "*," + M + "*"),
                    z = new RegExp("^" + M + "*([>+~]|" + M + ")" + M + "*"),
                    U = new RegExp(M + "|>"),
                    X = new RegExp(F),
                    V = new RegExp("^" + I + "$"),
                    G = {
                        ID: new RegExp("^#(" + I + ")"),
                        CLASS: new RegExp("^\\.(" + I + ")"),
                        TAG: new RegExp("^(" + I + "|[*])"),
                        ATTR: new RegExp("^" + W),
                        PSEUDO: new RegExp("^" + F),
                        CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + M + "*(even|odd|(([+-]|)(\\d*)n|)" + M + "*(?:([+-]|)" + M + "*(\\d+)|))" + M + "*\\)|)", "i"),
                        bool: new RegExp("^(?:" + R + ")$", "i"),
                        needsContext: new RegExp("^" + M + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + M + "*((?:-\\d)?\\d*)" + M + "*\\)|)(?=[^-]|$)", "i")
                    },
                    Y = /HTML$/i,
                    Q = /^(?:input|select|textarea|button)$/i,
                    J = /^h\d$/i,
                    K = /^[^{]+\{\s*\[native \w/,
                    Z = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
                    ee = /[+~]/,
                    te = new RegExp("\\\\[\\da-fA-F]{1,6}" + M + "?|\\\\([^\\r\\n\\f])", "g"),
                    ne = function(e, t) {
                        var n = "0x" + e.slice(1) - 65536;
                        return t || (n < 0 ? String.fromCharCode(n + 65536) : String.fromCharCode(n >> 10 | 55296, 1023 & n | 56320))
                    },
                    re = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
                    ie = function(e, t) {
                        return t ? "\0" === e ? "\ufffd" : e.slice(0, -1) + "\\" + e.charCodeAt(e.length - 1).toString(16) + " " : "\\" + e
                    },
                    oe = function() {
                        T()
                    },
                    ae = be(function(e) {
                        return !0 === e.disabled && "fieldset" === e.nodeName.toLowerCase()
                    }, {
                        dir: "parentNode",
                        next: "legend"
                    });
                try {
                    H.apply(t = O.call(p.childNodes), p.childNodes), t[p.childNodes.length].nodeType
                } catch (e) {
                    H = {
                        apply: t.length ? function(e, t) {
                            L.apply(e, O.call(t))
                        } : function(e, t) {
                            var n = e.length,
                                r = 0;
                            while (e[n++] = t[r++]);
                            e.length = n - 1
                        }
                    }
                }

                function se(t, e, n, r) {
                    var i, o, a, s, u, l, c, f = e && e.ownerDocument,
                        p = e ? e.nodeType : 9;
                    if (n = n || [], "string" != typeof t || !t || 1 !== p && 9 !== p && 11 !== p) return n;
                    if (!r && (T(e), e = e || C, E)) {
                        if (11 !== p && (u = Z.exec(t)))
                            if (i = u[1]) {
                                if (9 === p) {
                                    if (!(a = e.getElementById(i))) return n;
                                    if (a.id === i) return n.push(a), n
                                } else if (f && (a = f.getElementById(i)) && y(e, a) && a.id === i) return n.push(a), n
                            } else {
                                if (u[2]) return H.apply(n, e.getElementsByTagName(t)), n;
                                if ((i = u[3]) && d.getElementsByClassName && e.getElementsByClassName) return H.apply(n, e.getElementsByClassName(i)), n
                            }
                        if (d.qsa && !N[t + " "] && (!v || !v.test(t)) && (1 !== p || "object" !== e.nodeName.toLowerCase())) {
                            if (c = t, f = e, 1 === p && (U.test(t) || z.test(t))) {
                                (f = ee.test(t) && ye(e.parentNode) || e) === e && d.scope || ((s = e.getAttribute("id")) ? s = s.replace(re, ie) : e.setAttribute("id", s = S)), o = (l = h(t)).length;
                                while (o--) l[o] = (s ? "#" + s : ":scope") + " " + xe(l[o]);
                                c = l.join(",")
                            }
                            try {
                                return H.apply(n, f.querySelectorAll(c)), n
                            } catch (e) {
                                N(t, !0)
                            } finally {
                                s === S && e.removeAttribute("id")
                            }
                        }
                    }
                    return g(t.replace($, "$1"), e, n, r)
                }

                function ue() {
                    var r = [];
                    return function e(t, n) {
                        return r.push(t + " ") > b.cacheLength && delete e[r.shift()], e[t + " "] = n
                    }
                }

                function le(e) {
                    return e[S] = !0, e
                }

                function ce(e) {
                    var t = C.createElement("fieldset");
                    try {
                        return !!e(t)
                    } catch (e) {
                        return !1
                    } finally {
                        t.parentNode && t.parentNode.removeChild(t), t = null
                    }
                }

                function fe(e, t) {
                    var n = e.split("|"),
                        r = n.length;
                    while (r--) b.attrHandle[n[r]] = t
                }

                function pe(e, t) {
                    var n = t && e,
                        r = n && 1 === e.nodeType && 1 === t.nodeType && e.sourceIndex - t.sourceIndex;
                    if (r) return r;
                    if (n)
                        while (n = n.nextSibling)
                            if (n === t) return -1;
                    return e ? 1 : -1
                }

                function de(t) {
                    return function(e) {
                        return "input" === e.nodeName.toLowerCase() && e.type === t
                    }
                }

                function he(n) {
                    return function(e) {
                        var t = e.nodeName.toLowerCase();
                        return ("input" === t || "button" === t) && e.type === n
                    }
                }

                function ge(t) {
                    return function(e) {
                        return "form" in e ? e.parentNode && !1 === e.disabled ? "label" in e ? "label" in e.parentNode ? e.parentNode.disabled === t : e.disabled === t : e.isDisabled === t || e.isDisabled !== !t && ae(e) === t : e.disabled === t : "label" in e && e.disabled === t
                    }
                }

                function ve(a) {
                    return le(function(o) {
                        return o = +o, le(function(e, t) {
                            var n, r = a([], e.length, o),
                                i = r.length;
                            while (i--) e[n = r[i]] && (e[n] = !(t[n] = e[n]))
                        })
                    })
                }

                function ye(e) {
                    return e && "undefined" != typeof e.getElementsByTagName && e
                }
                for (e in d = se.support = {}, i = se.isXML = function(e) {
                        var t = e.namespaceURI,
                            n = (e.ownerDocument || e).documentElement;
                        return !Y.test(t || n && n.nodeName || "HTML")
                    }, T = se.setDocument = function(e) {
                        var t, n, r = e ? e.ownerDocument || e : p;
                        return r != C && 9 === r.nodeType && r.documentElement && (a = (C = r).documentElement, E = !i(C), p != C && (n = C.defaultView) && n.top !== n && (n.addEventListener ? n.addEventListener("unload", oe, !1) : n.attachEvent && n.attachEvent("onunload", oe)), d.scope = ce(function(e) {
                            return a.appendChild(e).appendChild(C.createElement("div")), "undefined" != typeof e.querySelectorAll && !e.querySelectorAll(":scope fieldset div").length
                        }), d.attributes = ce(function(e) {
                            return e.className = "i", !e.getAttribute("className")
                        }), d.getElementsByTagName = ce(function(e) {
                            return e.appendChild(C.createComment("")), !e.getElementsByTagName("*").length
                        }), d.getElementsByClassName = K.test(C.getElementsByClassName), d.getById = ce(function(e) {
                            return a.appendChild(e).id = S, !C.getElementsByName || !C.getElementsByName(S).length
                        }), d.getById ? (b.filter.ID = function(e) {
                            var t = e.replace(te, ne);
                            return function(e) {
                                return e.getAttribute("id") === t
                            }
                        }, b.find.ID = function(e, t) {
                            if ("undefined" != typeof t.getElementById && E) {
                                var n = t.getElementById(e);
                                return n ? [n] : []
                            }
                        }) : (b.filter.ID = function(e) {
                            var n = e.replace(te, ne);
                            return function(e) {
                                var t = "undefined" != typeof e.getAttributeNode && e.getAttributeNode("id");
                                return t && t.value === n
                            }
                        }, b.find.ID = function(e, t) {
                            if ("undefined" != typeof t.getElementById && E) {
                                var n, r, i, o = t.getElementById(e);
                                if (o) {
                                    if ((n = o.getAttributeNode("id")) && n.value === e) return [o];
                                    i = t.getElementsByName(e), r = 0;
                                    while (o = i[r++])
                                        if ((n = o.getAttributeNode("id")) && n.value === e) return [o]
                                }
                                return []
                            }
                        }), b.find.TAG = d.getElementsByTagName ? function(e, t) {
                            return "undefined" != typeof t.getElementsByTagName ? t.getElementsByTagName(e) : d.qsa ? t.querySelectorAll(e) : void 0
                        } : function(e, t) {
                            var n, r = [],
                                i = 0,
                                o = t.getElementsByTagName(e);
                            if ("*" === e) {
                                while (n = o[i++]) 1 === n.nodeType && r.push(n);
                                return r
                            }
                            return o
                        }, b.find.CLASS = d.getElementsByClassName && function(e, t) {
                            if ("undefined" != typeof t.getElementsByClassName && E) return t.getElementsByClassName(e)
                        }, s = [], v = [], (d.qsa = K.test(C.querySelectorAll)) && (ce(function(e) {
                            var t;
                            a.appendChild(e).innerHTML = "<a id='" + S + "'></a><select id='" + S + "-\r\\' msallowcapture=''><option selected=''></option></select>", e.querySelectorAll("[msallowcapture^='']").length && v.push("[*^$]=" + M + "*(?:''|\"\")"), e.querySelectorAll("[selected]").length || v.push("\\[" + M + "*(?:value|" + R + ")"), e.querySelectorAll("[id~=" + S + "-]").length || v.push("~="), (t = C.createElement("input")).setAttribute("name", ""), e.appendChild(t), e.querySelectorAll("[name='']").length || v.push("\\[" + M + "*name" + M + "*=" + M + "*(?:''|\"\")"), e.querySelectorAll(":checked").length || v.push(":checked"), e.querySelectorAll("a#" + S + "+*").length || v.push(".#.+[+~]"), e.querySelectorAll("\\\f"), v.push("[\\r\\n\\f]")
                        }), ce(function(e) {
                            e.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
                            var t = C.createElement("input");
                            t.setAttribute("type", "hidden"), e.appendChild(t).setAttribute("name", "D"), e.querySelectorAll("[name=d]").length && v.push("name" + M + "*[*^$|!~]?="), 2 !== e.querySelectorAll(":enabled").length && v.push(":enabled", ":disabled"), a.appendChild(e).disabled = !0, 2 !== e.querySelectorAll(":disabled").length && v.push(":enabled", ":disabled"), e.querySelectorAll("*,:x"), v.push(",.*:")
                        })), (d.matchesSelector = K.test(c = a.matches || a.webkitMatchesSelector || a.mozMatchesSelector || a.oMatchesSelector || a.msMatchesSelector)) && ce(function(e) {
                            d.disconnectedMatch = c.call(e, "*"), c.call(e, "[s!='']:x"), s.push("!=", F)
                        }), v = v.length && new RegExp(v.join("|")), s = s.length && new RegExp(s.join("|")), t = K.test(a.compareDocumentPosition), y = t || K.test(a.contains) ? function(e, t) {
                            var n = 9 === e.nodeType ? e.documentElement : e,
                                r = t && t.parentNode;
                            return e === r || !(!r || 1 !== r.nodeType || !(n.contains ? n.contains(r) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(r)))
                        } : function(e, t) {
                            if (t)
                                while (t = t.parentNode)
                                    if (t === e) return !0;
                            return !1
                        }, D = t ? function(e, t) {
                            if (e === t) return l = !0, 0;
                            var n = !e.compareDocumentPosition - !t.compareDocumentPosition;
                            return n || (1 & (n = (e.ownerDocument || e) == (t.ownerDocument || t) ? e.compareDocumentPosition(t) : 1) || !d.sortDetached && t.compareDocumentPosition(e) === n ? e == C || e.ownerDocument == p && y(p, e) ? -1 : t == C || t.ownerDocument == p && y(p, t) ? 1 : u ? P(u, e) - P(u, t) : 0 : 4 & n ? -1 : 1)
                        } : function(e, t) {
                            if (e === t) return l = !0, 0;
                            var n, r = 0,
                                i = e.parentNode,
                                o = t.parentNode,
                                a = [e],
                                s = [t];
                            if (!i || !o) return e == C ? -1 : t == C ? 1 : i ? -1 : o ? 1 : u ? P(u, e) - P(u, t) : 0;
                            if (i === o) return pe(e, t);
                            n = e;
                            while (n = n.parentNode) a.unshift(n);
                            n = t;
                            while (n = n.parentNode) s.unshift(n);
                            while (a[r] === s[r]) r++;
                            return r ? pe(a[r], s[r]) : a[r] == p ? -1 : s[r] == p ? 1 : 0
                        }), C
                    }, se.matches = function(e, t) {
                        return se(e, null, null, t)
                    }, se.matchesSelector = function(e, t) {
                        if (T(e), d.matchesSelector && E && !N[t + " "] && (!s || !s.test(t)) && (!v || !v.test(t))) try {
                            var n = c.call(e, t);
                            if (n || d.disconnectedMatch || e.document && 11 !== e.document.nodeType) return n
                        } catch (e) {
                            N(t, !0)
                        }
                        return 0 < se(t, C, null, [e]).length
                    }, se.contains = function(e, t) {
                        return (e.ownerDocument || e) != C && T(e), y(e, t)
                    }, se.attr = function(e, t) {
                        (e.ownerDocument || e) != C && T(e);
                        var n = b.attrHandle[t.toLowerCase()],
                            r = n && j.call(b.attrHandle, t.toLowerCase()) ? n(e, t, !E) : void 0;
                        return void 0 !== r ? r : d.attributes || !E ? e.getAttribute(t) : (r = e.getAttributeNode(t)) && r.specified ? r.value : null
                    }, se.escape = function(e) {
                        return (e + "").replace(re, ie)
                    }, se.error = function(e) {
                        throw new Error("Syntax error, unrecognized expression: " + e)
                    }, se.uniqueSort = function(e) {
                        var t, n = [],
                            r = 0,
                            i = 0;
                        if (l = !d.detectDuplicates, u = !d.sortStable && e.slice(0), e.sort(D), l) {
                            while (t = e[i++]) t === e[i] && (r = n.push(i));
                            while (r--) e.splice(n[r], 1)
                        }
                        return u = null, e
                    }, o = se.getText = function(e) {
                        var t, n = "",
                            r = 0,
                            i = e.nodeType;
                        if (i) {
                            if (1 === i || 9 === i || 11 === i) {
                                if ("string" == typeof e.textContent) return e.textContent;
                                for (e = e.firstChild; e; e = e.nextSibling) n += o(e)
                            } else if (3 === i || 4 === i) return e.nodeValue
                        } else
                            while (t = e[r++]) n += o(t);
                        return n
                    }, (b = se.selectors = {
                        cacheLength: 50,
                        createPseudo: le,
                        match: G,
                        attrHandle: {},
                        find: {},
                        relative: {
                            ">": {
                                dir: "parentNode",
                                first: !0
                            },
                            " ": {
                                dir: "parentNode"
                            },
                            "+": {
                                dir: "previousSibling",
                                first: !0
                            },
                            "~": {
                                dir: "previousSibling"
                            }
                        },
                        preFilter: {
                            ATTR: function(e) {
                                return e[1] = e[1].replace(te, ne), e[3] = (e[3] || e[4] || e[5] || "").replace(te, ne), "~=" === e[2] && (e[3] = " " + e[3] + " "), e.slice(0, 4)
                            },
                            CHILD: function(e) {
                                return e[1] = e[1].toLowerCase(), "nth" === e[1].slice(0, 3) ? (e[3] || se.error(e[0]), e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])), e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && se.error(e[0]), e
                            },
                            PSEUDO: function(e) {
                                var t, n = !e[6] && e[2];
                                return G.CHILD.test(e[0]) ? null : (e[3] ? e[2] = e[4] || e[5] || "" : n && X.test(n) && (t = h(n, !0)) && (t = n.indexOf(")", n.length - t) - n.length) && (e[0] = e[0].slice(0, t), e[2] = n.slice(0, t)), e.slice(0, 3))
                            }
                        },
                        filter: {
                            TAG: function(e) {
                                var t = e.replace(te, ne).toLowerCase();
                                return "*" === e ? function() {
                                    return !0
                                } : function(e) {
                                    return e.nodeName && e.nodeName.toLowerCase() === t
                                }
                            },
                            CLASS: function(e) {
                                var t = m[e + " "];
                                return t || (t = new RegExp("(^|" + M + ")" + e + "(" + M + "|$)")) && m(e, function(e) {
                                    return t.test("string" == typeof e.className && e.className || "undefined" != typeof e.getAttribute && e.getAttribute("class") || "")
                                })
                            },
                            ATTR: function(n, r, i) {
                                return function(e) {
                                    var t = se.attr(e, n);
                                    return null == t ? "!=" === r : !r || (t += "", "=" === r ? t === i : "!=" === r ? t !== i : "^=" === r ? i && 0 === t.indexOf(i) : "*=" === r ? i && -1 < t.indexOf(i) : "$=" === r ? i && t.slice(-i.length) === i : "~=" === r ? -1 < (" " + t.replace(B, " ") + " ").indexOf(i) : "|=" === r && (t === i || t.slice(0, i.length + 1) === i + "-"))
                                }
                            },
                            CHILD: function(h, e, t, g, v) {
                                var y = "nth" !== h.slice(0, 3),
                                    m = "last" !== h.slice(-4),
                                    x = "of-type" === e;
                                return 1 === g && 0 === v ? function(e) {
                                    return !!e.parentNode
                                } : function(e, t, n) {
                                    var r, i, o, a, s, u, l = y !== m ? "nextSibling" : "previousSibling",
                                        c = e.parentNode,
                                        f = x && e.nodeName.toLowerCase(),
                                        p = !n && !x,
                                        d = !1;
                                    if (c) {
                                        if (y) {
                                            while (l) {
                                                a = e;
                                                while (a = a[l])
                                                    if (x ? a.nodeName.toLowerCase() === f : 1 === a.nodeType) return !1;
                                                u = l = "only" === h && !u && "nextSibling"
                                            }
                                            return !0
                                        }
                                        if (u = [m ? c.firstChild : c.lastChild], m && p) {
                                            d = (s = (r = (i = (o = (a = c)[S] || (a[S] = {}))[a.uniqueID] || (o[a.uniqueID] = {}))[h] || [])[0] === k && r[1]) && r[2], a = s && c.childNodes[s];
                                            while (a = ++s && a && a[l] || (d = s = 0) || u.pop())
                                                if (1 === a.nodeType && ++d && a === e) {
                                                    i[h] = [k, s, d];
                                                    break
                                                }
                                        } else if (p && (d = s = (r = (i = (o = (a = e)[S] || (a[S] = {}))[a.uniqueID] || (o[a.uniqueID] = {}))[h] || [])[0] === k && r[1]), !1 === d)
                                            while (a = ++s && a && a[l] || (d = s = 0) || u.pop())
                                                if ((x ? a.nodeName.toLowerCase() === f : 1 === a.nodeType) && ++d && (p && ((i = (o = a[S] || (a[S] = {}))[a.uniqueID] || (o[a.uniqueID] = {}))[h] = [k, d]), a === e)) break;
                                        return (d -= v) === g || d % g == 0 && 0 <= d / g
                                    }
                                }
                            },
                            PSEUDO: function(e, o) {
                                var t, a = b.pseudos[e] || b.setFilters[e.toLowerCase()] || se.error("unsupported pseudo: " + e);
                                return a[S] ? a(o) : 1 < a.length ? (t = [e, e, "", o], b.setFilters.hasOwnProperty(e.toLowerCase()) ? le(function(e, t) {
                                    var n, r = a(e, o),
                                        i = r.length;
                                    while (i--) e[n = P(e, r[i])] = !(t[n] = r[i])
                                }) : function(e) {
                                    return a(e, 0, t)
                                }) : a
                            }
                        },
                        pseudos: {
                            not: le(function(e) {
                                var r = [],
                                    i = [],
                                    s = f(e.replace($, "$1"));
                                return s[S] ? le(function(e, t, n, r) {
                                    var i, o = s(e, null, r, []),
                                        a = e.length;
                                    while (a--)(i = o[a]) && (e[a] = !(t[a] = i))
                                }) : function(e, t, n) {
                                    return r[0] = e, s(r, null, n, i), r[0] = null, !i.pop()
                                }
                            }),
                            has: le(function(t) {
                                return function(e) {
                                    return 0 < se(t, e).length
                                }
                            }),
                            contains: le(function(t) {
                                return t = t.replace(te, ne),
                                    function(e) {
                                        return -1 < (e.textContent || o(e)).indexOf(t)
                                    }
                            }),
                            lang: le(function(n) {
                                return V.test(n || "") || se.error("unsupported lang: " + n), n = n.replace(te, ne).toLowerCase(),
                                    function(e) {
                                        var t;
                                        do {
                                            if (t = E ? e.lang : e.getAttribute("xml:lang") || e.getAttribute("lang")) return (t = t.toLowerCase()) === n || 0 === t.indexOf(n + "-")
                                        } while ((e = e.parentNode) && 1 === e.nodeType);
                                        return !1
                                    }
                            }),
                            target: function(e) {
                                var t = n.location && n.location.hash;
                                return t && t.slice(1) === e.id
                            },
                            root: function(e) {
                                return e === a
                            },
                            focus: function(e) {
                                return e === C.activeElement && (!C.hasFocus || C.hasFocus()) && !!(e.type || e.href || ~e.tabIndex)
                            },
                            enabled: ge(!1),
                            disabled: ge(!0),
                            checked: function(e) {
                                var t = e.nodeName.toLowerCase();
                                return "input" === t && !!e.checked || "option" === t && !!e.selected
                            },
                            selected: function(e) {
                                return e.parentNode && e.parentNode.selectedIndex, !0 === e.selected
                            },
                            empty: function(e) {
                                for (e = e.firstChild; e; e = e.nextSibling)
                                    if (e.nodeType < 6) return !1;
                                return !0
                            },
                            parent: function(e) {
                                return !b.pseudos.empty(e)
                            },
                            header: function(e) {
                                return J.test(e.nodeName)
                            },
                            input: function(e) {
                                return Q.test(e.nodeName)
                            },
                            button: function(e) {
                                var t = e.nodeName.toLowerCase();
                                return "input" === t && "button" === e.type || "button" === t
                            },
                            text: function(e) {
                                var t;
                                return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || "text" === t.toLowerCase())
                            },
                            first: ve(function() {
                                return [0]
                            }),
                            last: ve(function(e, t) {
                                return [t - 1]
                            }),
                            eq: ve(function(e, t, n) {
                                return [n < 0 ? n + t : n]
                            }),
                            even: ve(function(e, t) {
                                for (var n = 0; n < t; n += 2) e.push(n);
                                return e
                            }),
                            odd: ve(function(e, t) {
                                for (var n = 1; n < t; n += 2) e.push(n);
                                return e
                            }),
                            lt: ve(function(e, t, n) {
                                for (var r = n < 0 ? n + t : t < n ? t : n; 0 <= --r;) e.push(r);
                                return e
                            }),
                            gt: ve(function(e, t, n) {
                                for (var r = n < 0 ? n + t : n; ++r < t;) e.push(r);
                                return e
                            })
                        }
                    }).pseudos.nth = b.pseudos.eq, {
                        radio: !0,
                        checkbox: !0,
                        file: !0,
                        password: !0,
                        image: !0
                    }) b.pseudos[e] = de(e);
                for (e in {
                        submit: !0,
                        reset: !0
                    }) b.pseudos[e] = he(e);

                function me() {}

                function xe(e) {
                    for (var t = 0, n = e.length, r = ""; t < n; t++) r += e[t].value;
                    return r
                }

                function be(s, e, t) {
                    var u = e.dir,
                        l = e.next,
                        c = l || u,
                        f = t && "parentNode" === c,
                        p = r++;
                    return e.first ? function(e, t, n) {
                        while (e = e[u])
                            if (1 === e.nodeType || f) return s(e, t, n);
                        return !1
                    } : function(e, t, n) {
                        var r, i, o, a = [k, p];
                        if (n) {
                            while (e = e[u])
                                if ((1 === e.nodeType || f) && s(e, t, n)) return !0
                        } else
                            while (e = e[u])
                                if (1 === e.nodeType || f)
                                    if (i = (o = e[S] || (e[S] = {}))[e.uniqueID] || (o[e.uniqueID] = {}), l && l === e.nodeName.toLowerCase()) e = e[u] || e;
                                    else {
                                        if ((r = i[c]) && r[0] === k && r[1] === p) return a[2] = r[2];
                                        if ((i[c] = a)[2] = s(e, t, n)) return !0
                                    } return !1
                    }
                }

                function we(i) {
                    return 1 < i.length ? function(e, t, n) {
                        var r = i.length;
                        while (r--)
                            if (!i[r](e, t, n)) return !1;
                        return !0
                    } : i[0]
                }

                function Te(e, t, n, r, i) {
                    for (var o, a = [], s = 0, u = e.length, l = null != t; s < u; s++)(o = e[s]) && (n && !n(o, r, i) || (a.push(o), l && t.push(s)));
                    return a
                }

                function Ce(d, h, g, v, y, e) {
                    return v && !v[S] && (v = Ce(v)), y && !y[S] && (y = Ce(y, e)), le(function(e, t, n, r) {
                        var i, o, a, s = [],
                            u = [],
                            l = t.length,
                            c = e || function(e, t, n) {
                                for (var r = 0, i = t.length; r < i; r++) se(e, t[r], n);
                                return n
                            }(h || "*", n.nodeType ? [n] : n, []),
                            f = !d || !e && h ? c : Te(c, s, d, n, r),
                            p = g ? y || (e ? d : l || v) ? [] : t : f;
                        if (g && g(f, p, n, r), v) {
                            i = Te(p, u), v(i, [], n, r), o = i.length;
                            while (o--)(a = i[o]) && (p[u[o]] = !(f[u[o]] = a))
                        }
                        if (e) {
                            if (y || d) {
                                if (y) {
                                    i = [], o = p.length;
                                    while (o--)(a = p[o]) && i.push(f[o] = a);
                                    y(null, p = [], i, r)
                                }
                                o = p.length;
                                while (o--)(a = p[o]) && -1 < (i = y ? P(e, a) : s[o]) && (e[i] = !(t[i] = a))
                            }
                        } else p = Te(p === t ? p.splice(l, p.length) : p), y ? y(null, t, p, r) : H.apply(t, p)
                    })
                }

                function Ee(e) {
                    for (var i, t, n, r = e.length, o = b.relative[e[0].type], a = o || b.relative[" "], s = o ? 1 : 0, u = be(function(e) {
                            return e === i
                        }, a, !0), l = be(function(e) {
                            return -1 < P(i, e)
                        }, a, !0), c = [function(e, t, n) {
                            var r = !o && (n || t !== w) || ((i = t).nodeType ? u(e, t, n) : l(e, t, n));
                            return i = null, r
                        }]; s < r; s++)
                        if (t = b.relative[e[s].type]) c = [be(we(c), t)];
                        else {
                            if ((t = b.filter[e[s].type].apply(null, e[s].matches))[S]) {
                                for (n = ++s; n < r; n++)
                                    if (b.relative[e[n].type]) break;
                                return Ce(1 < s && we(c), 1 < s && xe(e.slice(0, s - 1).concat({
                                    value: " " === e[s - 2].type ? "*" : ""
                                })).replace($, "$1"), t, s < n && Ee(e.slice(s, n)), n < r && Ee(e = e.slice(n)), n < r && xe(e))
                            }
                            c.push(t)
                        }
                    return we(c)
                }
                return me.prototype = b.filters = b.pseudos, b.setFilters = new me, h = se.tokenize = function(e, t) {
                    var n, r, i, o, a, s, u, l = x[e + " "];
                    if (l) return t ? 0 : l.slice(0);
                    a = e, s = [], u = b.preFilter;
                    while (a) {
                        for (o in n && !(r = _.exec(a)) || (r && (a = a.slice(r[0].length) || a), s.push(i = [])), n = !1, (r = z.exec(a)) && (n = r.shift(), i.push({
                                value: n,
                                type: r[0].replace($, " ")
                            }), a = a.slice(n.length)), b.filter) !(r = G[o].exec(a)) || u[o] && !(r = u[o](r)) || (n = r.shift(), i.push({
                            value: n,
                            type: o,
                            matches: r
                        }), a = a.slice(n.length));
                        if (!n) break
                    }
                    return t ? a.length : a ? se.error(e) : x(e, s).slice(0)
                }, f = se.compile = function(e, t) {
                    var n, v, y, m, x, r, i = [],
                        o = [],
                        a = A[e + " "];
                    if (!a) {
                        t || (t = h(e)), n = t.length;
                        while (n--)(a = Ee(t[n]))[S] ? i.push(a) : o.push(a);
                        (a = A(e, (v = o, m = 0 < (y = i).length, x = 0 < v.length, r = function(e, t, n, r, i) {
                            var o, a, s, u = 0,
                                l = "0",
                                c = e && [],
                                f = [],
                                p = w,
                                d = e || x && b.find.TAG("*", i),
                                h = k += null == p ? 1 : Math.random() || .1,
                                g = d.length;
                            for (i && (w = t == C || t || i); l !== g && null != (o = d[l]); l++) {
                                if (x && o) {
                                    a = 0, t || o.ownerDocument == C || (T(o), n = !E);
                                    while (s = v[a++])
                                        if (s(o, t || C, n)) {
                                            r.push(o);
                                            break
                                        }
                                    i && (k = h)
                                }
                                m && ((o = !s && o) && u--, e && c.push(o))
                            }
                            if (u += l, m && l !== u) {
                                a = 0;
                                while (s = y[a++]) s(c, f, t, n);
                                if (e) {
                                    if (0 < u)
                                        while (l--) c[l] || f[l] || (f[l] = q.call(r));
                                    f = Te(f)
                                }
                                H.apply(r, f), i && !e && 0 < f.length && 1 < u + y.length && se.uniqueSort(r)
                            }
                            return i && (k = h, w = p), c
                        }, m ? le(r) : r))).selector = e
                    }
                    return a
                }, g = se.select = function(e, t, n, r) {
                    var i, o, a, s, u, l = "function" == typeof e && e,
                        c = !r && h(e = l.selector || e);
                    if (n = n || [], 1 === c.length) {
                        if (2 < (o = c[0] = c[0].slice(0)).length && "ID" === (a = o[0]).type && 9 === t.nodeType && E && b.relative[o[1].type]) {
                            if (!(t = (b.find.ID(a.matches[0].replace(te, ne), t) || [])[0])) return n;
                            l && (t = t.parentNode), e = e.slice(o.shift().value.length)
                        }
                        i = G.needsContext.test(e) ? 0 : o.length;
                        while (i--) {
                            if (a = o[i], b.relative[s = a.type]) break;
                            if ((u = b.find[s]) && (r = u(a.matches[0].replace(te, ne), ee.test(o[0].type) && ye(t.parentNode) || t))) {
                                if (o.splice(i, 1), !(e = r.length && xe(o))) return H.apply(n, r), n;
                                break
                            }
                        }
                    }
                    return (l || f(e, c))(r, t, !E, n, !t || ee.test(e) && ye(t.parentNode) || t), n
                }, d.sortStable = S.split("").sort(D).join("") === S, d.detectDuplicates = !!l, T(), d.sortDetached = ce(function(e) {
                    return 1 & e.compareDocumentPosition(C.createElement("fieldset"))
                }), ce(function(e) {
                    return e.innerHTML = "<a href='#'></a>", "#" === e.firstChild.getAttribute("href")
                }) || fe("type|href|height|width", function(e, t, n) {
                    if (!n) return e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2)
                }), d.attributes && ce(function(e) {
                    return e.innerHTML = "<input/>", e.firstChild.setAttribute("value", ""), "" === e.firstChild.getAttribute("value")
                }) || fe("value", function(e, t, n) {
                    if (!n && "input" === e.nodeName.toLowerCase()) return e.defaultValue
                }), ce(function(e) {
                    return null == e.getAttribute("disabled")
                }) || fe(R, function(e, t, n) {
                    var r;
                    if (!n) return !0 === e[t] ? t.toLowerCase() : (r = e.getAttributeNode(t)) && r.specified ? r.value : null
                }), se
            }(C);
            S.find = d, S.expr = d.selectors, S.expr[":"] = S.expr.pseudos, S.uniqueSort = S.unique = d.uniqueSort, S.text = d.getText, S.isXMLDoc = d.isXML, S.contains = d.contains, S.escapeSelector = d.escape;
            var h = function(e, t, n) {
                    var r = [],
                        i = void 0 !== n;
                    while ((e = e[t]) && 9 !== e.nodeType)
                        if (1 === e.nodeType) {
                            if (i && S(e).is(n)) break;
                            r.push(e)
                        }
                    return r
                },
                T = function(e, t) {
                    for (var n = []; e; e = e.nextSibling) 1 === e.nodeType && e !== t && n.push(e);
                    return n
                },
                k = S.expr.match.needsContext;

            function A(e, t) {
                return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase()
            }
            var N = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i;

            function D(e, n, r) {
                return m(n) ? S.grep(e, function(e, t) {
                    return !!n.call(e, t, e) !== r
                }) : n.nodeType ? S.grep(e, function(e) {
                    return e === n !== r
                }) : "string" != typeof n ? S.grep(e, function(e) {
                    return -1 < i.call(n, e) !== r
                }) : S.filter(n, e, r)
            }
            S.filter = function(e, t, n) {
                var r = t[0];
                return n && (e = ":not(" + e + ")"), 1 === t.length && 1 === r.nodeType ? S.find.matchesSelector(r, e) ? [r] : [] : S.find.matches(e, S.grep(t, function(e) {
                    return 1 === e.nodeType
                }))
            }, S.fn.extend({
                find: function(e) {
                    var t, n, r = this.length,
                        i = this;
                    if ("string" != typeof e) return this.pushStack(S(e).filter(function() {
                        for (t = 0; t < r; t++)
                            if (S.contains(i[t], this)) return !0
                    }));
                    for (n = this.pushStack([]), t = 0; t < r; t++) S.find(e, i[t], n);
                    return 1 < r ? S.uniqueSort(n) : n
                },
                filter: function(e) {
                    return this.pushStack(D(this, e || [], !1))
                },
                not: function(e) {
                    return this.pushStack(D(this, e || [], !0))
                },
                is: function(e) {
                    return !!D(this, "string" == typeof e && k.test(e) ? S(e) : e || [], !1).length
                }
            });
            var j, q = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;
            (S.fn.init = function(e, t, n) {
                var r, i;
                if (!e) return this;
                if (n = n || j, "string" == typeof e) {
                    if (!(r = "<" === e[0] && ">" === e[e.length - 1] && 3 <= e.length ? [null, e, null] : q.exec(e)) || !r[1] && t) return !t || t.jquery ? (t || n).find(e) : this.constructor(t).find(e);
                    if (r[1]) {
                        if (t = t instanceof S ? t[0] : t, S.merge(this, S.parseHTML(r[1], t && t.nodeType ? t.ownerDocument || t : E, !0)), N.test(r[1]) && S.isPlainObject(t))
                            for (r in t) m(this[r]) ? this[r](t[r]) : this.attr(r, t[r]);
                        return this
                    }
                    return (i = E.getElementById(r[2])) && (this[0] = i, this.length = 1), this
                }
                return e.nodeType ? (this[0] = e, this.length = 1, this) : m(e) ? void 0 !== n.ready ? n.ready(e) : e(S) : S.makeArray(e, this)
            }).prototype = S.fn, j = S(E);
            var L = /^(?:parents|prev(?:Until|All))/,
                H = {
                    children: !0,
                    contents: !0,
                    next: !0,
                    prev: !0
                };

            function O(e, t) {
                while ((e = e[t]) && 1 !== e.nodeType);
                return e
            }
            S.fn.extend({
                has: function(e) {
                    var t = S(e, this),
                        n = t.length;
                    return this.filter(function() {
                        for (var e = 0; e < n; e++)
                            if (S.contains(this, t[e])) return !0
                    })
                },
                closest: function(e, t) {
                    var n, r = 0,
                        i = this.length,
                        o = [],
                        a = "string" != typeof e && S(e);
                    if (!k.test(e))
                        for (; r < i; r++)
                            for (n = this[r]; n && n !== t; n = n.parentNode)
                                if (n.nodeType < 11 && (a ? -1 < a.index(n) : 1 === n.nodeType && S.find.matchesSelector(n, e))) {
                                    o.push(n);
                                    break
                                }
                    return this.pushStack(1 < o.length ? S.uniqueSort(o) : o)
                },
                index: function(e) {
                    return e ? "string" == typeof e ? i.call(S(e), this[0]) : i.call(this, e.jquery ? e[0] : e) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
                },
                add: function(e, t) {
                    return this.pushStack(S.uniqueSort(S.merge(this.get(), S(e, t))))
                },
                addBack: function(e) {
                    return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
                }
            }), S.each({
                parent: function(e) {
                    var t = e.parentNode;
                    return t && 11 !== t.nodeType ? t : null
                },
                parents: function(e) {
                    return h(e, "parentNode")
                },
                parentsUntil: function(e, t, n) {
                    return h(e, "parentNode", n)
                },
                next: function(e) {
                    return O(e, "nextSibling")
                },
                prev: function(e) {
                    return O(e, "previousSibling")
                },
                nextAll: function(e) {
                    return h(e, "nextSibling")
                },
                prevAll: function(e) {
                    return h(e, "previousSibling")
                },
                nextUntil: function(e, t, n) {
                    return h(e, "nextSibling", n)
                },
                prevUntil: function(e, t, n) {
                    return h(e, "previousSibling", n)
                },
                siblings: function(e) {
                    return T((e.parentNode || {}).firstChild, e)
                },
                children: function(e) {
                    return T(e.firstChild)
                },
                contents: function(e) {
                    return null != e.contentDocument && r(e.contentDocument) ? e.contentDocument : (A(e, "template") && (e = e.content || e), S.merge([], e.childNodes))
                }
            }, function(r, i) {
                S.fn[r] = function(e, t) {
                    var n = S.map(this, i, e);
                    return "Until" !== r.slice(-5) && (t = e), t && "string" == typeof t && (n = S.filter(t, n)), 1 < this.length && (H[r] || S.uniqueSort(n), L.test(r) && n.reverse()), this.pushStack(n)
                }
            });
            var P = /[^\x20\t\r\n\f]+/g;

            function R(e) {
                return e
            }

            function M(e) {
                throw e
            }

            function I(e, t, n, r) {
                var i;
                try {
                    e && m(i = e.promise) ? i.call(e).done(t).fail(n) : e && m(i = e.then) ? i.call(e, t, n) : t.apply(void 0, [e].slice(r))
                } catch (e) {
                    n.apply(void 0, [e])
                }
            }
            S.Callbacks = function(r) {
                var e, n;
                r = "string" == typeof r ? (e = r, n = {}, S.each(e.match(P) || [], function(e, t) {
                    n[t] = !0
                }), n) : S.extend({}, r);
                var i, t, o, a, s = [],
                    u = [],
                    l = -1,
                    c = function() {
                        for (a = a || r.once, o = i = !0; u.length; l = -1) {
                            t = u.shift();
                            while (++l < s.length) !1 === s[l].apply(t[0], t[1]) && r.stopOnFalse && (l = s.length, t = !1)
                        }
                        r.memory || (t = !1), i = !1, a && (s = t ? [] : "")
                    },
                    f = {
                        add: function() {
                            return s && (t && !i && (l = s.length - 1, u.push(t)), function n(e) {
                                S.each(e, function(e, t) {
                                    m(t) ? r.unique && f.has(t) || s.push(t) : t && t.length && "string" !== w(t) && n(t)
                                })
                            }(arguments), t && !i && c()), this
                        },
                        remove: function() {
                            return S.each(arguments, function(e, t) {
                                var n;
                                while (-1 < (n = S.inArray(t, s, n))) s.splice(n, 1), n <= l && l--
                            }), this
                        },
                        has: function(e) {
                            return e ? -1 < S.inArray(e, s) : 0 < s.length
                        },
                        empty: function() {
                            return s && (s = []), this
                        },
                        disable: function() {
                            return a = u = [], s = t = "", this
                        },
                        disabled: function() {
                            return !s
                        },
                        lock: function() {
                            return a = u = [], t || i || (s = t = ""), this
                        },
                        locked: function() {
                            return !!a
                        },
                        fireWith: function(e, t) {
                            return a || (t = [e, (t = t || []).slice ? t.slice() : t], u.push(t), i || c()), this
                        },
                        fire: function() {
                            return f.fireWith(this, arguments), this
                        },
                        fired: function() {
                            return !!o
                        }
                    };
                return f
            }, S.extend({
                Deferred: function(e) {
                    var o = [
                            ["notify", "progress", S.Callbacks("memory"), S.Callbacks("memory"), 2],
                            ["resolve", "done", S.Callbacks("once memory"), S.Callbacks("once memory"), 0, "resolved"],
                            ["reject", "fail", S.Callbacks("once memory"), S.Callbacks("once memory"), 1, "rejected"]
                        ],
                        i = "pending",
                        a = {
                            state: function() {
                                return i
                            },
                            always: function() {
                                return s.done(arguments).fail(arguments), this
                            },
                            "catch": function(e) {
                                return a.then(null, e)
                            },
                            pipe: function() {
                                var i = arguments;
                                return S.Deferred(function(r) {
                                    S.each(o, function(e, t) {
                                        var n = m(i[t[4]]) && i[t[4]];
                                        s[t[1]](function() {
                                            var e = n && n.apply(this, arguments);
                                            e && m(e.promise) ? e.promise().progress(r.notify).done(r.resolve).fail(r.reject) : r[t[0] + "With"](this, n ? [e] : arguments)
                                        })
                                    }), i = null
                                }).promise()
                            },
                            then: function(t, n, r) {
                                var u = 0;

                                function l(i, o, a, s) {
                                    return function() {
                                        var n = this,
                                            r = arguments,
                                            e = function() {
                                                var e, t;
                                                if (!(i < u)) {
                                                    if ((e = a.apply(n, r)) === o.promise()) throw new TypeError("Thenable self-resolution");
                                                    t = e && ("object" == typeof e || "function" == typeof e) && e.then, m(t) ? s ? t.call(e, l(u, o, R, s), l(u, o, M, s)) : (u++, t.call(e, l(u, o, R, s), l(u, o, M, s), l(u, o, R, o.notifyWith))) : (a !== R && (n = void 0, r = [e]), (s || o.resolveWith)(n, r))
                                                }
                                            },
                                            t = s ? e : function() {
                                                try {
                                                    e()
                                                } catch (e) {
                                                    S.Deferred.exceptionHook && S.Deferred.exceptionHook(e, t.stackTrace), u <= i + 1 && (a !== M && (n = void 0, r = [e]), o.rejectWith(n, r))
                                                }
                                            };
                                        i ? t() : (S.Deferred.getStackHook && (t.stackTrace = S.Deferred.getStackHook()), C.setTimeout(t))
                                    }
                                }
                                return S.Deferred(function(e) {
                                    o[0][3].add(l(0, e, m(r) ? r : R, e.notifyWith)), o[1][3].add(l(0, e, m(t) ? t : R)), o[2][3].add(l(0, e, m(n) ? n : M))
                                }).promise()
                            },
                            promise: function(e) {
                                return null != e ? S.extend(e, a) : a
                            }
                        },
                        s = {};
                    return S.each(o, function(e, t) {
                        var n = t[2],
                            r = t[5];
                        a[t[1]] = n.add, r && n.add(function() {
                            i = r
                        }, o[3 - e][2].disable, o[3 - e][3].disable, o[0][2].lock, o[0][3].lock), n.add(t[3].fire), s[t[0]] = function() {
                            return s[t[0] + "With"](this === s ? void 0 : this, arguments), this
                        }, s[t[0] + "With"] = n.fireWith
                    }), a.promise(s), e && e.call(s, s), s
                },
                when: function(e) {
                    var n = arguments.length,
                        t = n,
                        r = Array(t),
                        i = s.call(arguments),
                        o = S.Deferred(),
                        a = function(t) {
                            return function(e) {
                                r[t] = this, i[t] = 1 < arguments.length ? s.call(arguments) : e, --n || o.resolveWith(r, i)
                            }
                        };
                    if (n <= 1 && (I(e, o.done(a(t)).resolve, o.reject, !n), "pending" === o.state() || m(i[t] && i[t].then))) return o.then();
                    while (t--) I(i[t], a(t), o.reject);
                    return o.promise()
                }
            });
            var W = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
            S.Deferred.exceptionHook = function(e, t) {
                C.console && C.console.warn && e && W.test(e.name) && C.console.warn("jQuery.Deferred exception: " + e.message, e.stack, t)
            }, S.readyException = function(e) {
                C.setTimeout(function() {
                    throw e
                })
            };
            var F = S.Deferred();

            function B() {
                E.removeEventListener("DOMContentLoaded", B), C.removeEventListener("load", B), S.ready()
            }
            S.fn.ready = function(e) {
                return F.then(e)["catch"](function(e) {
                    S.readyException(e)
                }), this
            }, S.extend({
                isReady: !1,
                readyWait: 1,
                ready: function(e) {
                    (!0 === e ? --S.readyWait : S.isReady) || (S.isReady = !0) !== e && 0 < --S.readyWait || F.resolveWith(E, [S])
                }
            }), S.ready.then = F.then, "complete" === E.readyState || "loading" !== E.readyState && !E.documentElement.doScroll ? C.setTimeout(S.ready) : (E.addEventListener("DOMContentLoaded", B), C.addEventListener("load", B));
            var $ = function(e, t, n, r, i, o, a) {
                    var s = 0,
                        u = e.length,
                        l = null == n;
                    if ("object" === w(n))
                        for (s in i = !0, n) $(e, t, s, n[s], !0, o, a);
                    else if (void 0 !== r && (i = !0, m(r) || (a = !0), l && (a ? (t.call(e, r), t = null) : (l = t, t = function(e, t, n) {
                            return l.call(S(e), n)
                        })), t))
                        for (; s < u; s++) t(e[s], n, a ? r : r.call(e[s], s, t(e[s], n)));
                    return i ? e : l ? t.call(e) : u ? t(e[0], n) : o
                },
                _ = /^-ms-/,
                z = /-([a-z])/g;

            function U(e, t) {
                return t.toUpperCase()
            }

            function X(e) {
                return e.replace(_, "ms-").replace(z, U)
            }
            var V = function(e) {
                return 1 === e.nodeType || 9 === e.nodeType || !+e.nodeType
            };

            function G() {
                this.expando = S.expando + G.uid++
            }
            G.uid = 1, G.prototype = {
                cache: function(e) {
                    var t = e[this.expando];
                    return t || (t = {}, V(e) && (e.nodeType ? e[this.expando] = t : Object.defineProperty(e, this.expando, {
                        value: t,
                        configurable: !0
                    }))), t
                },
                set: function(e, t, n) {
                    var r, i = this.cache(e);
                    if ("string" == typeof t) i[X(t)] = n;
                    else
                        for (r in t) i[X(r)] = t[r];
                    return i
                },
                get: function(e, t) {
                    return void 0 === t ? this.cache(e) : e[this.expando] && e[this.expando][X(t)]
                },
                access: function(e, t, n) {
                    return void 0 === t || t && "string" == typeof t && void 0 === n ? this.get(e, t) : (this.set(e, t, n), void 0 !== n ? n : t)
                },
                remove: function(e, t) {
                    var n, r = e[this.expando];
                    if (void 0 !== r) {
                        if (void 0 !== t) {
                            n = (t = Array.isArray(t) ? t.map(X) : (t = X(t)) in r ? [t] : t.match(P) || []).length;
                            while (n--) delete r[t[n]]
                        }(void 0 === t || S.isEmptyObject(r)) && (e.nodeType ? e[this.expando] = void 0 : delete e[this.expando])
                    }
                },
                hasData: function(e) {
                    var t = e[this.expando];
                    return void 0 !== t && !S.isEmptyObject(t)
                }
            };
            var Y = new G,
                Q = new G,
                J = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
                K = /[A-Z]/g;

            function Z(e, t, n) {
                var r, i;
                if (void 0 === n && 1 === e.nodeType)
                    if (r = "data-" + t.replace(K, "-$&").toLowerCase(), "string" == typeof(n = e.getAttribute(r))) {
                        try {
                            n = "true" === (i = n) || "false" !== i && ("null" === i ? null : i === +i + "" ? +i : J.test(i) ? JSON.parse(i) : i)
                        } catch (e) {}
                        Q.set(e, t, n)
                    } else n = void 0;
                return n
            }
            S.extend({
                hasData: function(e) {
                    return Q.hasData(e) || Y.hasData(e)
                },
                data: function(e, t, n) {
                    return Q.access(e, t, n)
                },
                removeData: function(e, t) {
                    Q.remove(e, t)
                },
                _data: function(e, t, n) {
                    return Y.access(e, t, n)
                },
                _removeData: function(e, t) {
                    Y.remove(e, t)
                }
            }), S.fn.extend({
                data: function(n, e) {
                    var t, r, i, o = this[0],
                        a = o && o.attributes;
                    if (void 0 === n) {
                        if (this.length && (i = Q.get(o), 1 === o.nodeType && !Y.get(o, "hasDataAttrs"))) {
                            t = a.length;
                            while (t--) a[t] && 0 === (r = a[t].name).indexOf("data-") && (r = X(r.slice(5)), Z(o, r, i[r]));
                            Y.set(o, "hasDataAttrs", !0)
                        }
                        return i
                    }
                    return "object" == typeof n ? this.each(function() {
                        Q.set(this, n)
                    }) : $(this, function(e) {
                        var t;
                        if (o && void 0 === e) return void 0 !== (t = Q.get(o, n)) ? t : void 0 !== (t = Z(o, n)) ? t : void 0;
                        this.each(function() {
                            Q.set(this, n, e)
                        })
                    }, null, e, 1 < arguments.length, null, !0)
                },
                removeData: function(e) {
                    return this.each(function() {
                        Q.remove(this, e)
                    })
                }
            }), S.extend({
                queue: function(e, t, n) {
                    var r;
                    if (e) return t = (t || "fx") + "queue", r = Y.get(e, t), n && (!r || Array.isArray(n) ? r = Y.access(e, t, S.makeArray(n)) : r.push(n)), r || []
                },
                dequeue: function(e, t) {
                    t = t || "fx";
                    var n = S.queue(e, t),
                        r = n.length,
                        i = n.shift(),
                        o = S._queueHooks(e, t);
                    "inprogress" === i && (i = n.shift(), r--), i && ("fx" === t && n.unshift("inprogress"), delete o.stop, i.call(e, function() {
                        S.dequeue(e, t)
                    }, o)), !r && o && o.empty.fire()
                },
                _queueHooks: function(e, t) {
                    var n = t + "queueHooks";
                    return Y.get(e, n) || Y.access(e, n, {
                        empty: S.Callbacks("once memory").add(function() {
                            Y.remove(e, [t + "queue", n])
                        })
                    })
                }
            }), S.fn.extend({
                queue: function(t, n) {
                    var e = 2;
                    return "string" != typeof t && (n = t, t = "fx", e--), arguments.length < e ? S.queue(this[0], t) : void 0 === n ? this : this.each(function() {
                        var e = S.queue(this, t, n);
                        S._queueHooks(this, t), "fx" === t && "inprogress" !== e[0] && S.dequeue(this, t)
                    })
                },
                dequeue: function(e) {
                    return this.each(function() {
                        S.dequeue(this, e)
                    })
                },
                clearQueue: function(e) {
                    return this.queue(e || "fx", [])
                },
                promise: function(e, t) {
                    var n, r = 1,
                        i = S.Deferred(),
                        o = this,
                        a = this.length,
                        s = function() {
                            --r || i.resolveWith(o, [o])
                        };
                    "string" != typeof e && (t = e, e = void 0), e = e || "fx";
                    while (a--)(n = Y.get(o[a], e + "queueHooks")) && n.empty && (r++, n.empty.add(s));
                    return s(), i.promise(t)
                }
            });
            var ee = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
                te = new RegExp("^(?:([+-])=|)(" + ee + ")([a-z%]*)$", "i"),
                ne = ["Top", "Right", "Bottom", "Left"],
                re = E.documentElement,
                ie = function(e) {
                    return S.contains(e.ownerDocument, e)
                },
                oe = {
                    composed: !0
                };
            re.getRootNode && (ie = function(e) {
                return S.contains(e.ownerDocument, e) || e.getRootNode(oe) === e.ownerDocument
            });
            var ae = function(e, t) {
                return "none" === (e = t || e).style.display || "" === e.style.display && ie(e) && "none" === S.css(e, "display")
            };

            function se(e, t, n, r) {
                var i, o, a = 20,
                    s = r ? function() {
                        return r.cur()
                    } : function() {
                        return S.css(e, t, "")
                    },
                    u = s(),
                    l = n && n[3] || (S.cssNumber[t] ? "" : "px"),
                    c = e.nodeType && (S.cssNumber[t] || "px" !== l && +u) && te.exec(S.css(e, t));
                if (c && c[3] !== l) {
                    u /= 2, l = l || c[3], c = +u || 1;
                    while (a--) S.style(e, t, c + l), (1 - o) * (1 - (o = s() / u || .5)) <= 0 && (a = 0), c /= o;
                    c *= 2, S.style(e, t, c + l), n = n || []
                }
                return n && (c = +c || +u || 0, i = n[1] ? c + (n[1] + 1) * n[2] : +n[2], r && (r.unit = l, r.start = c, r.end = i)), i
            }
            var ue = {};

            function le(e, t) {
                for (var n, r, i, o, a, s, u, l = [], c = 0, f = e.length; c < f; c++)(r = e[c]).style && (n = r.style.display, t ? ("none" === n && (l[c] = Y.get(r, "display") || null, l[c] || (r.style.display = "")), "" === r.style.display && ae(r) && (l[c] = (u = a = o = void 0, a = (i = r).ownerDocument, s = i.nodeName, (u = ue[s]) || (o = a.body.appendChild(a.createElement(s)), u = S.css(o, "display"), o.parentNode.removeChild(o), "none" === u && (u = "block"), ue[s] = u)))) : "none" !== n && (l[c] = "none", Y.set(r, "display", n)));
                for (c = 0; c < f; c++) null != l[c] && (e[c].style.display = l[c]);
                return e
            }
            S.fn.extend({
                show: function() {
                    return le(this, !0)
                },
                hide: function() {
                    return le(this)
                },
                toggle: function(e) {
                    return "boolean" == typeof e ? e ? this.show() : this.hide() : this.each(function() {
                        ae(this) ? S(this).show() : S(this).hide()
                    })
                }
            });
            var ce, fe, pe = /^(?:checkbox|radio)$/i,
                de = /<([a-z][^\/\0>\x20\t\r\n\f]*)/i,
                he = /^$|^module$|\/(?:java|ecma)script/i;
            ce = E.createDocumentFragment().appendChild(E.createElement("div")), (fe = E.createElement("input")).setAttribute("type", "radio"), fe.setAttribute("checked", "checked"), fe.setAttribute("name", "t"), ce.appendChild(fe), y.checkClone = ce.cloneNode(!0).cloneNode(!0).lastChild.checked, ce.innerHTML = "<textarea>x</textarea>", y.noCloneChecked = !!ce.cloneNode(!0).lastChild.defaultValue, ce.innerHTML = "<option></option>", y.option = !!ce.lastChild;
            var ge = {
                thead: [1, "<table>", "</table>"],
                col: [2, "<table><colgroup>", "</colgroup></table>"],
                tr: [2, "<table><tbody>", "</tbody></table>"],
                td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
                _default: [0, "", ""]
            };

            function ve(e, t) {
                var n;
                return n = "undefined" != typeof e.getElementsByTagName ? e.getElementsByTagName(t || "*") : "undefined" != typeof e.querySelectorAll ? e.querySelectorAll(t || "*") : [], void 0 === t || t && A(e, t) ? S.merge([e], n) : n
            }

            function ye(e, t) {
                for (var n = 0, r = e.length; n < r; n++) Y.set(e[n], "globalEval", !t || Y.get(t[n], "globalEval"))
            }
            ge.tbody = ge.tfoot = ge.colgroup = ge.caption = ge.thead, ge.th = ge.td, y.option || (ge.optgroup = ge.option = [1, "<select multiple='multiple'>", "</select>"]);
            var me = /<|&#?\w+;/;

            function xe(e, t, n, r, i) {
                for (var o, a, s, u, l, c, f = t.createDocumentFragment(), p = [], d = 0, h = e.length; d < h; d++)
                    if ((o = e[d]) || 0 === o)
                        if ("object" === w(o)) S.merge(p, o.nodeType ? [o] : o);
                        else if (me.test(o)) {
                    a = a || f.appendChild(t.createElement("div")), s = (de.exec(o) || ["", ""])[1].toLowerCase(), u = ge[s] || ge._default, a.innerHTML = u[1] + S.htmlPrefilter(o) + u[2], c = u[0];
                    while (c--) a = a.lastChild;
                    S.merge(p, a.childNodes), (a = f.firstChild).textContent = ""
                } else p.push(t.createTextNode(o));
                f.textContent = "", d = 0;
                while (o = p[d++])
                    if (r && -1 < S.inArray(o, r)) i && i.push(o);
                    else if (l = ie(o), a = ve(f.appendChild(o), "script"), l && ye(a), n) {
                    c = 0;
                    while (o = a[c++]) he.test(o.type || "") && n.push(o)
                }
                return f
            }
            var be = /^key/,
                we = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
                Te = /^([^.]*)(?:\.(.+)|)/;

            function Ce() {
                return !0
            }

            function Ee() {
                return !1
            }

            function Se(e, t) {
                return e === function() {
                    try {
                        return E.activeElement
                    } catch (e) {}
                }() == ("focus" === t)
            }

            function ke(e, t, n, r, i, o) {
                var a, s;
                if ("object" == typeof t) {
                    for (s in "string" != typeof n && (r = r || n, n = void 0), t) ke(e, s, n, r, t[s], o);
                    return e
                }
                if (null == r && null == i ? (i = n, r = n = void 0) : null == i && ("string" == typeof n ? (i = r, r = void 0) : (i = r, r = n, n = void 0)), !1 === i) i = Ee;
                else if (!i) return e;
                return 1 === o && (a = i, (i = function(e) {
                    return S().off(e), a.apply(this, arguments)
                }).guid = a.guid || (a.guid = S.guid++)), e.each(function() {
                    S.event.add(this, t, i, r, n)
                })
            }

            function Ae(e, i, o) {
                o ? (Y.set(e, i, !1), S.event.add(e, i, {
                    namespace: !1,
                    handler: function(e) {
                        var t, n, r = Y.get(this, i);
                        if (1 & e.isTrigger && this[i]) {
                            if (r.length)(S.event.special[i] || {}).delegateType && e.stopPropagation();
                            else if (r = s.call(arguments), Y.set(this, i, r), t = o(this, i), this[i](), r !== (n = Y.get(this, i)) || t ? Y.set(this, i, !1) : n = {}, r !== n) return e.stopImmediatePropagation(), e.preventDefault(), n.value
                        } else r.length && (Y.set(this, i, {
                            value: S.event.trigger(S.extend(r[0], S.Event.prototype), r.slice(1), this)
                        }), e.stopImmediatePropagation())
                    }
                })) : void 0 === Y.get(e, i) && S.event.add(e, i, Ce)
            }
            S.event = {
                global: {},
                add: function(t, e, n, r, i) {
                    var o, a, s, u, l, c, f, p, d, h, g, v = Y.get(t);
                    if (V(t)) {
                        n.handler && (n = (o = n).handler, i = o.selector), i && S.find.matchesSelector(re, i), n.guid || (n.guid = S.guid++), (u = v.events) || (u = v.events = Object.create(null)), (a = v.handle) || (a = v.handle = function(e) {
                            return "undefined" != typeof S && S.event.triggered !== e.type ? S.event.dispatch.apply(t, arguments) : void 0
                        }), l = (e = (e || "").match(P) || [""]).length;
                        while (l--) d = g = (s = Te.exec(e[l]) || [])[1], h = (s[2] || "").split(".").sort(), d && (f = S.event.special[d] || {}, d = (i ? f.delegateType : f.bindType) || d, f = S.event.special[d] || {}, c = S.extend({
                            type: d,
                            origType: g,
                            data: r,
                            handler: n,
                            guid: n.guid,
                            selector: i,
                            needsContext: i && S.expr.match.needsContext.test(i),
                            namespace: h.join(".")
                        }, o), (p = u[d]) || ((p = u[d] = []).delegateCount = 0, f.setup && !1 !== f.setup.call(t, r, h, a) || t.addEventListener && t.addEventListener(d, a)), f.add && (f.add.call(t, c), c.handler.guid || (c.handler.guid = n.guid)), i ? p.splice(p.delegateCount++, 0, c) : p.push(c), S.event.global[d] = !0)
                    }
                },
                remove: function(e, t, n, r, i) {
                    var o, a, s, u, l, c, f, p, d, h, g, v = Y.hasData(e) && Y.get(e);
                    if (v && (u = v.events)) {
                        l = (t = (t || "").match(P) || [""]).length;
                        while (l--)
                            if (d = g = (s = Te.exec(t[l]) || [])[1], h = (s[2] || "").split(".").sort(), d) {
                                f = S.event.special[d] || {}, p = u[d = (r ? f.delegateType : f.bindType) || d] || [], s = s[2] && new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)"), a = o = p.length;
                                while (o--) c = p[o], !i && g !== c.origType || n && n.guid !== c.guid || s && !s.test(c.namespace) || r && r !== c.selector && ("**" !== r || !c.selector) || (p.splice(o, 1), c.selector && p.delegateCount--, f.remove && f.remove.call(e, c));
                                a && !p.length && (f.teardown && !1 !== f.teardown.call(e, h, v.handle) || S.removeEvent(e, d, v.handle), delete u[d])
                            } else
                                for (d in u) S.event.remove(e, d + t[l], n, r, !0);
                        S.isEmptyObject(u) && Y.remove(e, "handle events")
                    }
                },
                dispatch: function(e) {
                    var t, n, r, i, o, a, s = new Array(arguments.length),
                        u = S.event.fix(e),
                        l = (Y.get(this, "events") || Object.create(null))[u.type] || [],
                        c = S.event.special[u.type] || {};
                    for (s[0] = u, t = 1; t < arguments.length; t++) s[t] = arguments[t];
                    if (u.delegateTarget = this, !c.preDispatch || !1 !== c.preDispatch.call(this, u)) {
                        a = S.event.handlers.call(this, u, l), t = 0;
                        while ((i = a[t++]) && !u.isPropagationStopped()) {
                            u.currentTarget = i.elem, n = 0;
                            while ((o = i.handlers[n++]) && !u.isImmediatePropagationStopped()) u.rnamespace && !1 !== o.namespace && !u.rnamespace.test(o.namespace) || (u.handleObj = o, u.data = o.data, void 0 !== (r = ((S.event.special[o.origType] || {}).handle || o.handler).apply(i.elem, s)) && !1 === (u.result = r) && (u.preventDefault(), u.stopPropagation()))
                        }
                        return c.postDispatch && c.postDispatch.call(this, u), u.result
                    }
                },
                handlers: function(e, t) {
                    var n, r, i, o, a, s = [],
                        u = t.delegateCount,
                        l = e.target;
                    if (u && l.nodeType && !("click" === e.type && 1 <= e.button))
                        for (; l !== this; l = l.parentNode || this)
                            if (1 === l.nodeType && ("click" !== e.type || !0 !== l.disabled)) {
                                for (o = [], a = {}, n = 0; n < u; n++) void 0 === a[i = (r = t[n]).selector + " "] && (a[i] = r.needsContext ? -1 < S(i, this).index(l) : S.find(i, this, null, [l]).length), a[i] && o.push(r);
                                o.length && s.push({
                                    elem: l,
                                    handlers: o
                                })
                            }
                    return l = this, u < t.length && s.push({
                        elem: l,
                        handlers: t.slice(u)
                    }), s
                },
                addProp: function(t, e) {
                    Object.defineProperty(S.Event.prototype, t, {
                        enumerable: !0,
                        configurable: !0,
                        get: m(e) ? function() {
                            if (this.originalEvent) return e(this.originalEvent)
                        } : function() {
                            if (this.originalEvent) return this.originalEvent[t]
                        },
                        set: function(e) {
                            Object.defineProperty(this, t, {
                                enumerable: !0,
                                configurable: !0,
                                writable: !0,
                                value: e
                            })
                        }
                    })
                },
                fix: function(e) {
                    return e[S.expando] ? e : new S.Event(e)
                },
                special: {
                    load: {
                        noBubble: !0
                    },
                    click: {
                        setup: function(e) {
                            var t = this || e;
                            return pe.test(t.type) && t.click && A(t, "input") && Ae(t, "click", Ce), !1
                        },
                        trigger: function(e) {
                            var t = this || e;
                            return pe.test(t.type) && t.click && A(t, "input") && Ae(t, "click"), !0
                        },
                        _default: function(e) {
                            var t = e.target;
                            return pe.test(t.type) && t.click && A(t, "input") && Y.get(t, "click") || A(t, "a")
                        }
                    },
                    beforeunload: {
                        postDispatch: function(e) {
                            void 0 !== e.result && e.originalEvent && (e.originalEvent.returnValue = e.result)
                        }
                    }
                }
            }, S.removeEvent = function(e, t, n) {
                e.removeEventListener && e.removeEventListener(t, n)
            }, S.Event = function(e, t) {
                if (!(this instanceof S.Event)) return new S.Event(e, t);
                e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || void 0 === e.defaultPrevented && !1 === e.returnValue ? Ce : Ee, this.target = e.target && 3 === e.target.nodeType ? e.target.parentNode : e.target, this.currentTarget = e.currentTarget, this.relatedTarget = e.relatedTarget) : this.type = e, t && S.extend(this, t), this.timeStamp = e && e.timeStamp || Date.now(), this[S.expando] = !0
            }, S.Event.prototype = {
                constructor: S.Event,
                isDefaultPrevented: Ee,
                isPropagationStopped: Ee,
                isImmediatePropagationStopped: Ee,
                isSimulated: !1,
                preventDefault: function() {
                    var e = this.originalEvent;
                    this.isDefaultPrevented = Ce, e && !this.isSimulated && e.preventDefault()
                },
                stopPropagation: function() {
                    var e = this.originalEvent;
                    this.isPropagationStopped = Ce, e && !this.isSimulated && e.stopPropagation()
                },
                stopImmediatePropagation: function() {
                    var e = this.originalEvent;
                    this.isImmediatePropagationStopped = Ce, e && !this.isSimulated && e.stopImmediatePropagation(), this.stopPropagation()
                }
            }, S.each({
                altKey: !0,
                bubbles: !0,
                cancelable: !0,
                changedTouches: !0,
                ctrlKey: !0,
                detail: !0,
                eventPhase: !0,
                metaKey: !0,
                pageX: !0,
                pageY: !0,
                shiftKey: !0,
                view: !0,
                "char": !0,
                code: !0,
                charCode: !0,
                key: !0,
                keyCode: !0,
                button: !0,
                buttons: !0,
                clientX: !0,
                clientY: !0,
                offsetX: !0,
                offsetY: !0,
                pointerId: !0,
                pointerType: !0,
                screenX: !0,
                screenY: !0,
                targetTouches: !0,
                toElement: !0,
                touches: !0,
                which: function(e) {
                    var t = e.button;
                    return null == e.which && be.test(e.type) ? null != e.charCode ? e.charCode : e.keyCode : !e.which && void 0 !== t && we.test(e.type) ? 1 & t ? 1 : 2 & t ? 3 : 4 & t ? 2 : 0 : e.which
                }
            }, S.event.addProp), S.each({
                focus: "focusin",
                blur: "focusout"
            }, function(e, t) {
                S.event.special[e] = {
                    setup: function() {
                        return Ae(this, e, Se), !1
                    },
                    trigger: function() {
                        return Ae(this, e), !0
                    },
                    delegateType: t
                }
            }), S.each({
                mouseenter: "mouseover",
                mouseleave: "mouseout",
                pointerenter: "pointerover",
                pointerleave: "pointerout"
            }, function(e, i) {
                S.event.special[e] = {
                    delegateType: i,
                    bindType: i,
                    handle: function(e) {
                        var t, n = e.relatedTarget,
                            r = e.handleObj;
                        return n && (n === this || S.contains(this, n)) || (e.type = r.origType, t = r.handler.apply(this, arguments), e.type = i), t
                    }
                }
            }), S.fn.extend({
                on: function(e, t, n, r) {
                    return ke(this, e, t, n, r)
                },
                one: function(e, t, n, r) {
                    return ke(this, e, t, n, r, 1)
                },
                off: function(e, t, n) {
                    var r, i;
                    if (e && e.preventDefault && e.handleObj) return r = e.handleObj, S(e.delegateTarget).off(r.namespace ? r.origType + "." + r.namespace : r.origType, r.selector, r.handler), this;
                    if ("object" == typeof e) {
                        for (i in e) this.off(i, t, e[i]);
                        return this
                    }
                    return !1 !== t && "function" != typeof t || (n = t, t = void 0), !1 === n && (n = Ee), this.each(function() {
                        S.event.remove(this, e, n, t)
                    })
                }
            });
            var Ne = /<script|<style|<link/i,
                De = /checked\s*(?:[^=]|=\s*.checked.)/i,
                je = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;

            function qe(e, t) {
                return A(e, "table") && A(11 !== t.nodeType ? t : t.firstChild, "tr") && S(e).children("tbody")[0] || e
            }

            function Le(e) {
                return e.type = (null !== e.getAttribute("type")) + "/" + e.type, e
            }

            function He(e) {
                return "true/" === (e.type || "").slice(0, 5) ? e.type = e.type.slice(5) : e.removeAttribute("type"), e
            }

            function Oe(e, t) {
                var n, r, i, o, a, s;
                if (1 === t.nodeType) {
                    if (Y.hasData(e) && (s = Y.get(e).events))
                        for (i in Y.remove(t, "handle events"), s)
                            for (n = 0, r = s[i].length; n < r; n++) S.event.add(t, i, s[i][n]);
                    Q.hasData(e) && (o = Q.access(e), a = S.extend({}, o), Q.set(t, a))
                }
            }

            function Pe(n, r, i, o) {
                r = g(r);
                var e, t, a, s, u, l, c = 0,
                    f = n.length,
                    p = f - 1,
                    d = r[0],
                    h = m(d);
                if (h || 1 < f && "string" == typeof d && !y.checkClone && De.test(d)) return n.each(function(e) {
                    var t = n.eq(e);
                    h && (r[0] = d.call(this, e, t.html())), Pe(t, r, i, o)
                });
                if (f && (t = (e = xe(r, n[0].ownerDocument, !1, n, o)).firstChild, 1 === e.childNodes.length && (e = t), t || o)) {
                    for (s = (a = S.map(ve(e, "script"), Le)).length; c < f; c++) u = e, c !== p && (u = S.clone(u, !0, !0), s && S.merge(a, ve(u, "script"))), i.call(n[c], u, c);
                    if (s)
                        for (l = a[a.length - 1].ownerDocument, S.map(a, He), c = 0; c < s; c++) u = a[c], he.test(u.type || "") && !Y.access(u, "globalEval") && S.contains(l, u) && (u.src && "module" !== (u.type || "").toLowerCase() ? S._evalUrl && !u.noModule && S._evalUrl(u.src, {
                            nonce: u.nonce || u.getAttribute("nonce")
                        }, l) : b(u.textContent.replace(je, ""), u, l))
                }
                return n
            }

            function Re(e, t, n) {
                for (var r, i = t ? S.filter(t, e) : e, o = 0; null != (r = i[o]); o++) n || 1 !== r.nodeType || S.cleanData(ve(r)), r.parentNode && (n && ie(r) && ye(ve(r, "script")), r.parentNode.removeChild(r));
                return e
            }
            S.extend({
                htmlPrefilter: function(e) {
                    return e
                },
                clone: function(e, t, n) {
                    var r, i, o, a, s, u, l, c = e.cloneNode(!0),
                        f = ie(e);
                    if (!(y.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || S.isXMLDoc(e)))
                        for (a = ve(c), r = 0, i = (o = ve(e)).length; r < i; r++) s = o[r], u = a[r], void 0, "input" === (l = u.nodeName.toLowerCase()) && pe.test(s.type) ? u.checked = s.checked : "input" !== l && "textarea" !== l || (u.defaultValue = s.defaultValue);
                    if (t)
                        if (n)
                            for (o = o || ve(e), a = a || ve(c), r = 0, i = o.length; r < i; r++) Oe(o[r], a[r]);
                        else Oe(e, c);
                    return 0 < (a = ve(c, "script")).length && ye(a, !f && ve(e, "script")), c
                },
                cleanData: function(e) {
                    for (var t, n, r, i = S.event.special, o = 0; void 0 !== (n = e[o]); o++)
                        if (V(n)) {
                            if (t = n[Y.expando]) {
                                if (t.events)
                                    for (r in t.events) i[r] ? S.event.remove(n, r) : S.removeEvent(n, r, t.handle);
                                n[Y.expando] = void 0
                            }
                            n[Q.expando] && (n[Q.expando] = void 0)
                        }
                }
            }), S.fn.extend({
                detach: function(e) {
                    return Re(this, e, !0)
                },
                remove: function(e) {
                    return Re(this, e)
                },
                text: function(e) {
                    return $(this, function(e) {
                        return void 0 === e ? S.text(this) : this.empty().each(function() {
                            1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = e)
                        })
                    }, null, e, arguments.length)
                },
                append: function() {
                    return Pe(this, arguments, function(e) {
                        1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || qe(this, e).appendChild(e)
                    })
                },
                prepend: function() {
                    return Pe(this, arguments, function(e) {
                        if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                            var t = qe(this, e);
                            t.insertBefore(e, t.firstChild)
                        }
                    })
                },
                before: function() {
                    return Pe(this, arguments, function(e) {
                        this.parentNode && this.parentNode.insertBefore(e, this)
                    })
                },
                after: function() {
                    return Pe(this, arguments, function(e) {
                        this.parentNode && this.parentNode.insertBefore(e, this.nextSibling)
                    })
                },
                empty: function() {
                    for (var e, t = 0; null != (e = this[t]); t++) 1 === e.nodeType && (S.cleanData(ve(e, !1)), e.textContent = "");
                    return this
                },
                clone: function(e, t) {
                    return e = null != e && e, t = null == t ? e : t, this.map(function() {
                        return S.clone(this, e, t)
                    })
                },
                html: function(e) {
                    return $(this, function(e) {
                        var t = this[0] || {},
                            n = 0,
                            r = this.length;
                        if (void 0 === e && 1 === t.nodeType) return t.innerHTML;
                        if ("string" == typeof e && !Ne.test(e) && !ge[(de.exec(e) || ["", ""])[1].toLowerCase()]) {
                            e = S.htmlPrefilter(e);
                            try {
                                for (; n < r; n++) 1 === (t = this[n] || {}).nodeType && (S.cleanData(ve(t, !1)), t.innerHTML = e);
                                t = 0
                            } catch (e) {}
                        }
                        t && this.empty().append(e)
                    }, null, e, arguments.length)
                },
                replaceWith: function() {
                    var n = [];
                    return Pe(this, arguments, function(e) {
                        var t = this.parentNode;
                        S.inArray(this, n) < 0 && (S.cleanData(ve(this)), t && t.replaceChild(e, this))
                    }, n)
                }
            }), S.each({
                appendTo: "append",
                prependTo: "prepend",
                insertBefore: "before",
                insertAfter: "after",
                replaceAll: "replaceWith"
            }, function(e, a) {
                S.fn[e] = function(e) {
                    for (var t, n = [], r = S(e), i = r.length - 1, o = 0; o <= i; o++) t = o === i ? this : this.clone(!0), S(r[o])[a](t), u.apply(n, t.get());
                    return this.pushStack(n)
                }
            });
            var Me = new RegExp("^(" + ee + ")(?!px)[a-z%]+$", "i"),
                Ie = function(e) {
                    var t = e.ownerDocument.defaultView;
                    return t && t.opener || (t = C), t.getComputedStyle(e)
                },
                We = function(e, t, n) {
                    var r, i, o = {};
                    for (i in t) o[i] = e.style[i], e.style[i] = t[i];
                    for (i in r = n.call(e), t) e.style[i] = o[i];
                    return r
                },
                Fe = new RegExp(ne.join("|"), "i");

            function Be(e, t, n) {
                var r, i, o, a, s = e.style;
                return (n = n || Ie(e)) && ("" !== (a = n.getPropertyValue(t) || n[t]) || ie(e) || (a = S.style(e, t)), !y.pixelBoxStyles() && Me.test(a) && Fe.test(t) && (r = s.width, i = s.minWidth, o = s.maxWidth, s.minWidth = s.maxWidth = s.width = a, a = n.width, s.width = r, s.minWidth = i, s.maxWidth = o)), void 0 !== a ? a + "" : a
            }

            function $e(e, t) {
                return {
                    get: function() {
                        if (!e()) return (this.get = t).apply(this, arguments);
                        delete this.get
                    }
                }
            }! function() {
                function e() {
                    if (l) {
                        u.style.cssText = "position:absolute;left:-11111px;width:60px;margin-top:1px;padding:0;border:0", l.style.cssText = "position:relative;display:block;box-sizing:border-box;overflow:scroll;margin:auto;border:1px;padding:1px;width:60%;top:1%", re.appendChild(u).appendChild(l);
                        var e = C.getComputedStyle(l);
                        n = "1%" !== e.top, s = 12 === t(e.marginLeft), l.style.right = "60%", o = 36 === t(e.right), r = 36 === t(e.width), l.style.position = "absolute", i = 12 === t(l.offsetWidth / 3), re.removeChild(u), l = null
                    }
                }

                function t(e) {
                    return Math.round(parseFloat(e))
                }
                var n, r, i, o, a, s, u = E.createElement("div"),
                    l = E.createElement("div");
                l.style && (l.style.backgroundClip = "content-box", l.cloneNode(!0).style.backgroundClip = "", y.clearCloneStyle = "content-box" === l.style.backgroundClip, S.extend(y, {
                    boxSizingReliable: function() {
                        return e(), r
                    },
                    pixelBoxStyles: function() {
                        return e(), o
                    },
                    pixelPosition: function() {
                        return e(), n
                    },
                    reliableMarginLeft: function() {
                        return e(), s
                    },
                    scrollboxSize: function() {
                        return e(), i
                    },
                    reliableTrDimensions: function() {
                        var e, t, n, r;
                        return null == a && (e = E.createElement("table"), t = E.createElement("tr"), n = E.createElement("div"), e.style.cssText = "position:absolute;left:-11111px", t.style.height = "1px", n.style.height = "9px", re.appendChild(e).appendChild(t).appendChild(n), r = C.getComputedStyle(t), a = 3 < parseInt(r.height), re.removeChild(e)), a
                    }
                }))
            }();
            var _e = ["Webkit", "Moz", "ms"],
                ze = E.createElement("div").style,
                Ue = {};

            function Xe(e) {
                var t = S.cssProps[e] || Ue[e];
                return t || (e in ze ? e : Ue[e] = function(e) {
                    var t = e[0].toUpperCase() + e.slice(1),
                        n = _e.length;
                    while (n--)
                        if ((e = _e[n] + t) in ze) return e
                }(e) || e)
            }
            var Ve = /^(none|table(?!-c[ea]).+)/,
                Ge = /^--/,
                Ye = {
                    position: "absolute",
                    visibility: "hidden",
                    display: "block"
                },
                Qe = {
                    letterSpacing: "0",
                    fontWeight: "400"
                };

            function Je(e, t, n) {
                var r = te.exec(t);
                return r ? Math.max(0, r[2] - (n || 0)) + (r[3] || "px") : t
            }

            function Ke(e, t, n, r, i, o) {
                var a = "width" === t ? 1 : 0,
                    s = 0,
                    u = 0;
                if (n === (r ? "border" : "content")) return 0;
                for (; a < 4; a += 2) "margin" === n && (u += S.css(e, n + ne[a], !0, i)), r ? ("content" === n && (u -= S.css(e, "padding" + ne[a], !0, i)), "margin" !== n && (u -= S.css(e, "border" + ne[a] + "Width", !0, i))) : (u += S.css(e, "padding" + ne[a], !0, i), "padding" !== n ? u += S.css(e, "border" + ne[a] + "Width", !0, i) : s += S.css(e, "border" + ne[a] + "Width", !0, i));
                return !r && 0 <= o && (u += Math.max(0, Math.ceil(e["offset" + t[0].toUpperCase() + t.slice(1)] - o - u - s - .5)) || 0), u
            }

            function Ze(e, t, n) {
                var r = Ie(e),
                    i = (!y.boxSizingReliable() || n) && "border-box" === S.css(e, "boxSizing", !1, r),
                    o = i,
                    a = Be(e, t, r),
                    s = "offset" + t[0].toUpperCase() + t.slice(1);
                if (Me.test(a)) {
                    if (!n) return a;
                    a = "auto"
                }
                return (!y.boxSizingReliable() && i || !y.reliableTrDimensions() && A(e, "tr") || "auto" === a || !parseFloat(a) && "inline" === S.css(e, "display", !1, r)) && e.getClientRects().length && (i = "border-box" === S.css(e, "boxSizing", !1, r), (o = s in e) && (a = e[s])), (a = parseFloat(a) || 0) + Ke(e, t, n || (i ? "border" : "content"), o, r, a) + "px"
            }

            function et(e, t, n, r, i) {
                return new et.prototype.init(e, t, n, r, i)
            }
            S.extend({
                cssHooks: {
                    opacity: {
                        get: function(e, t) {
                            if (t) {
                                var n = Be(e, "opacity");
                                return "" === n ? "1" : n
                            }
                        }
                    }
                },
                cssNumber: {
                    animationIterationCount: !0,
                    columnCount: !0,
                    fillOpacity: !0,
                    flexGrow: !0,
                    flexShrink: !0,
                    fontWeight: !0,
                    gridArea: !0,
                    gridColumn: !0,
                    gridColumnEnd: !0,
                    gridColumnStart: !0,
                    gridRow: !0,
                    gridRowEnd: !0,
                    gridRowStart: !0,
                    lineHeight: !0,
                    opacity: !0,
                    order: !0,
                    orphans: !0,
                    widows: !0,
                    zIndex: !0,
                    zoom: !0
                },
                cssProps: {},
                style: function(e, t, n, r) {
                    if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
                        var i, o, a, s = X(t),
                            u = Ge.test(t),
                            l = e.style;
                        if (u || (t = Xe(s)), a = S.cssHooks[t] || S.cssHooks[s], void 0 === n) return a && "get" in a && void 0 !== (i = a.get(e, !1, r)) ? i : l[t];
                        "string" === (o = typeof n) && (i = te.exec(n)) && i[1] && (n = se(e, t, i), o = "number"), null != n && n == n && ("number" !== o || u || (n += i && i[3] || (S.cssNumber[s] ? "" : "px")), y.clearCloneStyle || "" !== n || 0 !== t.indexOf("background") || (l[t] = "inherit"), a && "set" in a && void 0 === (n = a.set(e, n, r)) || (u ? l.setProperty(t, n) : l[t] = n))
                    }
                },
                css: function(e, t, n, r) {
                    var i, o, a, s = X(t);
                    return Ge.test(t) || (t = Xe(s)), (a = S.cssHooks[t] || S.cssHooks[s]) && "get" in a && (i = a.get(e, !0, n)), void 0 === i && (i = Be(e, t, r)), "normal" === i && t in Qe && (i = Qe[t]), "" === n || n ? (o = parseFloat(i), !0 === n || isFinite(o) ? o || 0 : i) : i
                }
            }), S.each(["height", "width"], function(e, u) {
                S.cssHooks[u] = {
                    get: function(e, t, n) {
                        if (t) return !Ve.test(S.css(e, "display")) || e.getClientRects().length && e.getBoundingClientRect().width ? Ze(e, u, n) : We(e, Ye, function() {
                            return Ze(e, u, n)
                        })
                    },
                    set: function(e, t, n) {
                        var r, i = Ie(e),
                            o = !y.scrollboxSize() && "absolute" === i.position,
                            a = (o || n) && "border-box" === S.css(e, "boxSizing", !1, i),
                            s = n ? Ke(e, u, n, a, i) : 0;
                        return a && o && (s -= Math.ceil(e["offset" + u[0].toUpperCase() + u.slice(1)] - parseFloat(i[u]) - Ke(e, u, "border", !1, i) - .5)), s && (r = te.exec(t)) && "px" !== (r[3] || "px") && (e.style[u] = t, t = S.css(e, u)), Je(0, t, s)
                    }
                }
            }), S.cssHooks.marginLeft = $e(y.reliableMarginLeft, function(e, t) {
                if (t) return (parseFloat(Be(e, "marginLeft")) || e.getBoundingClientRect().left - We(e, {
                    marginLeft: 0
                }, function() {
                    return e.getBoundingClientRect().left
                })) + "px"
            }), S.each({
                margin: "",
                padding: "",
                border: "Width"
            }, function(i, o) {
                S.cssHooks[i + o] = {
                    expand: function(e) {
                        for (var t = 0, n = {}, r = "string" == typeof e ? e.split(" ") : [e]; t < 4; t++) n[i + ne[t] + o] = r[t] || r[t - 2] || r[0];
                        return n
                    }
                }, "margin" !== i && (S.cssHooks[i + o].set = Je)
            }), S.fn.extend({
                css: function(e, t) {
                    return $(this, function(e, t, n) {
                        var r, i, o = {},
                            a = 0;
                        if (Array.isArray(t)) {
                            for (r = Ie(e), i = t.length; a < i; a++) o[t[a]] = S.css(e, t[a], !1, r);
                            return o
                        }
                        return void 0 !== n ? S.style(e, t, n) : S.css(e, t)
                    }, e, t, 1 < arguments.length)
                }
            }), ((S.Tween = et).prototype = {
                constructor: et,
                init: function(e, t, n, r, i, o) {
                    this.elem = e, this.prop = n, this.easing = i || S.easing._default, this.options = t, this.start = this.now = this.cur(), this.end = r, this.unit = o || (S.cssNumber[n] ? "" : "px")
                },
                cur: function() {
                    var e = et.propHooks[this.prop];
                    return e && e.get ? e.get(this) : et.propHooks._default.get(this)
                },
                run: function(e) {
                    var t, n = et.propHooks[this.prop];
                    return this.options.duration ? this.pos = t = S.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : this.pos = t = e, this.now = (this.end - this.start) * t + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : et.propHooks._default.set(this), this
                }
            }).init.prototype = et.prototype, (et.propHooks = {
                _default: {
                    get: function(e) {
                        var t;
                        return 1 !== e.elem.nodeType || null != e.elem[e.prop] && null == e.elem.style[e.prop] ? e.elem[e.prop] : (t = S.css(e.elem, e.prop, "")) && "auto" !== t ? t : 0
                    },
                    set: function(e) {
                        S.fx.step[e.prop] ? S.fx.step[e.prop](e) : 1 !== e.elem.nodeType || !S.cssHooks[e.prop] && null == e.elem.style[Xe(e.prop)] ? e.elem[e.prop] = e.now : S.style(e.elem, e.prop, e.now + e.unit)
                    }
                }
            }).scrollTop = et.propHooks.scrollLeft = {
                set: function(e) {
                    e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now)
                }
            }, S.easing = {
                linear: function(e) {
                    return e
                },
                swing: function(e) {
                    return .5 - Math.cos(e * Math.PI) / 2
                },
                _default: "swing"
            }, S.fx = et.prototype.init, S.fx.step = {};
            var tt, nt, rt, it, ot = /^(?:toggle|show|hide)$/,
                at = /queueHooks$/;

            function st() {
                nt && (!1 === E.hidden && C.requestAnimationFrame ? C.requestAnimationFrame(st) : C.setTimeout(st, S.fx.interval), S.fx.tick())
            }

            function ut() {
                return C.setTimeout(function() {
                    tt = void 0
                }), tt = Date.now()
            }

            function lt(e, t) {
                var n, r = 0,
                    i = {
                        height: e
                    };
                for (t = t ? 1 : 0; r < 4; r += 2 - t) i["margin" + (n = ne[r])] = i["padding" + n] = e;
                return t && (i.opacity = i.width = e), i
            }

            function ct(e, t, n) {
                for (var r, i = (ft.tweeners[t] || []).concat(ft.tweeners["*"]), o = 0, a = i.length; o < a; o++)
                    if (r = i[o].call(n, t, e)) return r
            }

            function ft(o, e, t) {
                var n, a, r = 0,
                    i = ft.prefilters.length,
                    s = S.Deferred().always(function() {
                        delete u.elem
                    }),
                    u = function() {
                        if (a) return !1;
                        for (var e = tt || ut(), t = Math.max(0, l.startTime + l.duration - e), n = 1 - (t / l.duration || 0), r = 0, i = l.tweens.length; r < i; r++) l.tweens[r].run(n);
                        return s.notifyWith(o, [l, n, t]), n < 1 && i ? t : (i || s.notifyWith(o, [l, 1, 0]), s.resolveWith(o, [l]), !1)
                    },
                    l = s.promise({
                        elem: o,
                        props: S.extend({}, e),
                        opts: S.extend(!0, {
                            specialEasing: {},
                            easing: S.easing._default
                        }, t),
                        originalProperties: e,
                        originalOptions: t,
                        startTime: tt || ut(),
                        duration: t.duration,
                        tweens: [],
                        createTween: function(e, t) {
                            var n = S.Tween(o, l.opts, e, t, l.opts.specialEasing[e] || l.opts.easing);
                            return l.tweens.push(n), n
                        },
                        stop: function(e) {
                            var t = 0,
                                n = e ? l.tweens.length : 0;
                            if (a) return this;
                            for (a = !0; t < n; t++) l.tweens[t].run(1);
                            return e ? (s.notifyWith(o, [l, 1, 0]), s.resolveWith(o, [l, e])) : s.rejectWith(o, [l, e]), this
                        }
                    }),
                    c = l.props;
                for (! function(e, t) {
                        var n, r, i, o, a;
                        for (n in e)
                            if (i = t[r = X(n)], o = e[n], Array.isArray(o) && (i = o[1], o = e[n] = o[0]), n !== r && (e[r] = o, delete e[n]), (a = S.cssHooks[r]) && "expand" in a)
                                for (n in o = a.expand(o), delete e[r], o) n in e || (e[n] = o[n], t[n] = i);
                            else t[r] = i
                    }(c, l.opts.specialEasing); r < i; r++)
                    if (n = ft.prefilters[r].call(l, o, c, l.opts)) return m(n.stop) && (S._queueHooks(l.elem, l.opts.queue).stop = n.stop.bind(n)), n;
                return S.map(c, ct, l), m(l.opts.start) && l.opts.start.call(o, l), l.progress(l.opts.progress).done(l.opts.done, l.opts.complete).fail(l.opts.fail).always(l.opts.always), S.fx.timer(S.extend(u, {
                    elem: o,
                    anim: l,
                    queue: l.opts.queue
                })), l
            }
            S.Animation = S.extend(ft, {
                tweeners: {
                    "*": [function(e, t) {
                        var n = this.createTween(e, t);
                        return se(n.elem, e, te.exec(t), n), n
                    }]
                },
                tweener: function(e, t) {
                    m(e) ? (t = e, e = ["*"]) : e = e.match(P);
                    for (var n, r = 0, i = e.length; r < i; r++) n = e[r], ft.tweeners[n] = ft.tweeners[n] || [], ft.tweeners[n].unshift(t)
                },
                prefilters: [function(e, t, n) {
                    var r, i, o, a, s, u, l, c, f = "width" in t || "height" in t,
                        p = this,
                        d = {},
                        h = e.style,
                        g = e.nodeType && ae(e),
                        v = Y.get(e, "fxshow");
                    for (r in n.queue || (null == (a = S._queueHooks(e, "fx")).unqueued && (a.unqueued = 0, s = a.empty.fire, a.empty.fire = function() {
                            a.unqueued || s()
                        }), a.unqueued++, p.always(function() {
                            p.always(function() {
                                a.unqueued--, S.queue(e, "fx").length || a.empty.fire()
                            })
                        })), t)
                        if (i = t[r], ot.test(i)) {
                            if (delete t[r], o = o || "toggle" === i, i === (g ? "hide" : "show")) {
                                if ("show" !== i || !v || void 0 === v[r]) continue;
                                g = !0
                            }
                            d[r] = v && v[r] || S.style(e, r)
                        }
                    if ((u = !S.isEmptyObject(t)) || !S.isEmptyObject(d))
                        for (r in f && 1 === e.nodeType && (n.overflow = [h.overflow, h.overflowX, h.overflowY], null == (l = v && v.display) && (l = Y.get(e, "display")), "none" === (c = S.css(e, "display")) && (l ? c = l : (le([e], !0), l = e.style.display || l, c = S.css(e, "display"), le([e]))), ("inline" === c || "inline-block" === c && null != l) && "none" === S.css(e, "float") && (u || (p.done(function() {
                                h.display = l
                            }), null == l && (c = h.display, l = "none" === c ? "" : c)), h.display = "inline-block")), n.overflow && (h.overflow = "hidden", p.always(function() {
                                h.overflow = n.overflow[0], h.overflowX = n.overflow[1], h.overflowY = n.overflow[2]
                            })), u = !1, d) u || (v ? "hidden" in v && (g = v.hidden) : v = Y.access(e, "fxshow", {
                            display: l
                        }), o && (v.hidden = !g), g && le([e], !0), p.done(function() {
                            for (r in g || le([e]), Y.remove(e, "fxshow"), d) S.style(e, r, d[r])
                        })), u = ct(g ? v[r] : 0, r, p), r in v || (v[r] = u.start, g && (u.end = u.start, u.start = 0))
                }],
                prefilter: function(e, t) {
                    t ? ft.prefilters.unshift(e) : ft.prefilters.push(e)
                }
            }), S.speed = function(e, t, n) {
                var r = e && "object" == typeof e ? S.extend({}, e) : {
                    complete: n || !n && t || m(e) && e,
                    duration: e,
                    easing: n && t || t && !m(t) && t
                };
                return S.fx.off ? r.duration = 0 : "number" != typeof r.duration && (r.duration in S.fx.speeds ? r.duration = S.fx.speeds[r.duration] : r.duration = S.fx.speeds._default), null != r.queue && !0 !== r.queue || (r.queue = "fx"), r.old = r.complete, r.complete = function() {
                    m(r.old) && r.old.call(this), r.queue && S.dequeue(this, r.queue)
                }, r
            }, S.fn.extend({
                fadeTo: function(e, t, n, r) {
                    return this.filter(ae).css("opacity", 0).show().end().animate({
                        opacity: t
                    }, e, n, r)
                },
                animate: function(t, e, n, r) {
                    var i = S.isEmptyObject(t),
                        o = S.speed(e, n, r),
                        a = function() {
                            var e = ft(this, S.extend({}, t), o);
                            (i || Y.get(this, "finish")) && e.stop(!0)
                        };
                    return a.finish = a, i || !1 === o.queue ? this.each(a) : this.queue(o.queue, a)
                },
                stop: function(i, e, o) {
                    var a = function(e) {
                        var t = e.stop;
                        delete e.stop, t(o)
                    };
                    return "string" != typeof i && (o = e, e = i, i = void 0), e && this.queue(i || "fx", []), this.each(function() {
                        var e = !0,
                            t = null != i && i + "queueHooks",
                            n = S.timers,
                            r = Y.get(this);
                        if (t) r[t] && r[t].stop && a(r[t]);
                        else
                            for (t in r) r[t] && r[t].stop && at.test(t) && a(r[t]);
                        for (t = n.length; t--;) n[t].elem !== this || null != i && n[t].queue !== i || (n[t].anim.stop(o), e = !1, n.splice(t, 1));
                        !e && o || S.dequeue(this, i)
                    })
                },
                finish: function(a) {
                    return !1 !== a && (a = a || "fx"), this.each(function() {
                        var e, t = Y.get(this),
                            n = t[a + "queue"],
                            r = t[a + "queueHooks"],
                            i = S.timers,
                            o = n ? n.length : 0;
                        for (t.finish = !0, S.queue(this, a, []), r && r.stop && r.stop.call(this, !0), e = i.length; e--;) i[e].elem === this && i[e].queue === a && (i[e].anim.stop(!0), i.splice(e, 1));
                        for (e = 0; e < o; e++) n[e] && n[e].finish && n[e].finish.call(this);
                        delete t.finish
                    })
                }
            }), S.each(["toggle", "show", "hide"], function(e, r) {
                var i = S.fn[r];
                S.fn[r] = function(e, t, n) {
                    return null == e || "boolean" == typeof e ? i.apply(this, arguments) : this.animate(lt(r, !0), e, t, n)
                }
            }), S.each({
                slideDown: lt("show"),
                slideUp: lt("hide"),
                slideToggle: lt("toggle"),
                fadeIn: {
                    opacity: "show"
                },
                fadeOut: {
                    opacity: "hide"
                },
                fadeToggle: {
                    opacity: "toggle"
                }
            }, function(e, r) {
                S.fn[e] = function(e, t, n) {
                    return this.animate(r, e, t, n)
                }
            }), S.timers = [], S.fx.tick = function() {
                var e, t = 0,
                    n = S.timers;
                for (tt = Date.now(); t < n.length; t++)(e = n[t])() || n[t] !== e || n.splice(t--, 1);
                n.length || S.fx.stop(), tt = void 0
            }, S.fx.timer = function(e) {
                S.timers.push(e), S.fx.start()
            }, S.fx.interval = 13, S.fx.start = function() {
                nt || (nt = !0, st())
            }, S.fx.stop = function() {
                nt = null
            }, S.fx.speeds = {
                slow: 600,
                fast: 200,
                _default: 400
            }, S.fn.delay = function(r, e) {
                return r = S.fx && S.fx.speeds[r] || r, e = e || "fx", this.queue(e, function(e, t) {
                    var n = C.setTimeout(e, r);
                    t.stop = function() {
                        C.clearTimeout(n)
                    }
                })
            }, rt = E.createElement("input"), it = E.createElement("select").appendChild(E.createElement("option")), rt.type = "checkbox", y.checkOn = "" !== rt.value, y.optSelected = it.selected, (rt = E.createElement("input")).value = "t", rt.type = "radio", y.radioValue = "t" === rt.value;
            var pt, dt = S.expr.attrHandle;
            S.fn.extend({
                attr: function(e, t) {
                    return $(this, S.attr, e, t, 1 < arguments.length)
                },
                removeAttr: function(e) {
                    return this.each(function() {
                        S.removeAttr(this, e)
                    })
                }
            }), S.extend({
                attr: function(e, t, n) {
                    var r, i, o = e.nodeType;
                    if (3 !== o && 8 !== o && 2 !== o) return "undefined" == typeof e.getAttribute ? S.prop(e, t, n) : (1 === o && S.isXMLDoc(e) || (i = S.attrHooks[t.toLowerCase()] || (S.expr.match.bool.test(t) ? pt : void 0)), void 0 !== n ? null === n ? void S.removeAttr(e, t) : i && "set" in i && void 0 !== (r = i.set(e, n, t)) ? r : (e.setAttribute(t, n + ""), n) : i && "get" in i && null !== (r = i.get(e, t)) ? r : null == (r = S.find.attr(e, t)) ? void 0 : r)
                },
                attrHooks: {
                    type: {
                        set: function(e, t) {
                            if (!y.radioValue && "radio" === t && A(e, "input")) {
                                var n = e.value;
                                return e.setAttribute("type", t), n && (e.value = n), t
                            }
                        }
                    }
                },
                removeAttr: function(e, t) {
                    var n, r = 0,
                        i = t && t.match(P);
                    if (i && 1 === e.nodeType)
                        while (n = i[r++]) e.removeAttribute(n)
                }
            }), pt = {
                set: function(e, t, n) {
                    return !1 === t ? S.removeAttr(e, n) : e.setAttribute(n, n), n
                }
            }, S.each(S.expr.match.bool.source.match(/\w+/g), function(e, t) {
                var a = dt[t] || S.find.attr;
                dt[t] = function(e, t, n) {
                    var r, i, o = t.toLowerCase();
                    return n || (i = dt[o], dt[o] = r, r = null != a(e, t, n) ? o : null, dt[o] = i), r
                }
            });
            var ht = /^(?:input|select|textarea|button)$/i,
                gt = /^(?:a|area)$/i;

            function vt(e) {
                return (e.match(P) || []).join(" ")
            }

            function yt(e) {
                return e.getAttribute && e.getAttribute("class") || ""
            }

            function mt(e) {
                return Array.isArray(e) ? e : "string" == typeof e && e.match(P) || []
            }
            S.fn.extend({
                prop: function(e, t) {
                    return $(this, S.prop, e, t, 1 < arguments.length)
                },
                removeProp: function(e) {
                    return this.each(function() {
                        delete this[S.propFix[e] || e]
                    })
                }
            }), S.extend({
                prop: function(e, t, n) {
                    var r, i, o = e.nodeType;
                    if (3 !== o && 8 !== o && 2 !== o) return 1 === o && S.isXMLDoc(e) || (t = S.propFix[t] || t, i = S.propHooks[t]), void 0 !== n ? i && "set" in i && void 0 !== (r = i.set(e, n, t)) ? r : e[t] = n : i && "get" in i && null !== (r = i.get(e, t)) ? r : e[t]
                },
                propHooks: {
                    tabIndex: {
                        get: function(e) {
                            var t = S.find.attr(e, "tabindex");
                            return t ? parseInt(t, 10) : ht.test(e.nodeName) || gt.test(e.nodeName) && e.href ? 0 : -1
                        }
                    }
                },
                propFix: {
                    "for": "htmlFor",
                    "class": "className"
                }
            }), y.optSelected || (S.propHooks.selected = {
                get: function(e) {
                    var t = e.parentNode;
                    return t && t.parentNode && t.parentNode.selectedIndex, null
                },
                set: function(e) {
                    var t = e.parentNode;
                    t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex)
                }
            }), S.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function() {
                S.propFix[this.toLowerCase()] = this
            }), S.fn.extend({
                addClass: function(t) {
                    var e, n, r, i, o, a, s, u = 0;
                    if (m(t)) return this.each(function(e) {
                        S(this).addClass(t.call(this, e, yt(this)))
                    });
                    if ((e = mt(t)).length)
                        while (n = this[u++])
                            if (i = yt(n), r = 1 === n.nodeType && " " + vt(i) + " ") {
                                a = 0;
                                while (o = e[a++]) r.indexOf(" " + o + " ") < 0 && (r += o + " ");
                                i !== (s = vt(r)) && n.setAttribute("class", s)
                            }
                    return this
                },
                removeClass: function(t) {
                    var e, n, r, i, o, a, s, u = 0;
                    if (m(t)) return this.each(function(e) {
                        S(this).removeClass(t.call(this, e, yt(this)))
                    });
                    if (!arguments.length) return this.attr("class", "");
                    if ((e = mt(t)).length)
                        while (n = this[u++])
                            if (i = yt(n), r = 1 === n.nodeType && " " + vt(i) + " ") {
                                a = 0;
                                while (o = e[a++])
                                    while (-1 < r.indexOf(" " + o + " ")) r = r.replace(" " + o + " ", " ");
                                i !== (s = vt(r)) && n.setAttribute("class", s)
                            }
                    return this
                },
                toggleClass: function(i, t) {
                    var o = typeof i,
                        a = "string" === o || Array.isArray(i);
                    return "boolean" == typeof t && a ? t ? this.addClass(i) : this.removeClass(i) : m(i) ? this.each(function(e) {
                        S(this).toggleClass(i.call(this, e, yt(this), t), t)
                    }) : this.each(function() {
                        var e, t, n, r;
                        if (a) {
                            t = 0, n = S(this), r = mt(i);
                            while (e = r[t++]) n.hasClass(e) ? n.removeClass(e) : n.addClass(e)
                        } else void 0 !== i && "boolean" !== o || ((e = yt(this)) && Y.set(this, "__className__", e), this.setAttribute && this.setAttribute("class", e || !1 === i ? "" : Y.get(this, "__className__") || ""))
                    })
                },
                hasClass: function(e) {
                    var t, n, r = 0;
                    t = " " + e + " ";
                    while (n = this[r++])
                        if (1 === n.nodeType && -1 < (" " + vt(yt(n)) + " ").indexOf(t)) return !0;
                    return !1
                }
            });
            var xt = /\r/g;
            S.fn.extend({
                val: function(n) {
                    var r, e, i, t = this[0];
                    return arguments.length ? (i = m(n), this.each(function(e) {
                        var t;
                        1 === this.nodeType && (null == (t = i ? n.call(this, e, S(this).val()) : n) ? t = "" : "number" == typeof t ? t += "" : Array.isArray(t) && (t = S.map(t, function(e) {
                            return null == e ? "" : e + ""
                        })), (r = S.valHooks[this.type] || S.valHooks[this.nodeName.toLowerCase()]) && "set" in r && void 0 !== r.set(this, t, "value") || (this.value = t))
                    })) : t ? (r = S.valHooks[t.type] || S.valHooks[t.nodeName.toLowerCase()]) && "get" in r && void 0 !== (e = r.get(t, "value")) ? e : "string" == typeof(e = t.value) ? e.replace(xt, "") : null == e ? "" : e : void 0
                }
            }), S.extend({
                valHooks: {
                    option: {
                        get: function(e) {
                            var t = S.find.attr(e, "value");
                            return null != t ? t : vt(S.text(e))
                        }
                    },
                    select: {
                        get: function(e) {
                            var t, n, r, i = e.options,
                                o = e.selectedIndex,
                                a = "select-one" === e.type,
                                s = a ? null : [],
                                u = a ? o + 1 : i.length;
                            for (r = o < 0 ? u : a ? o : 0; r < u; r++)
                                if (((n = i[r]).selected || r === o) && !n.disabled && (!n.parentNode.disabled || !A(n.parentNode, "optgroup"))) {
                                    if (t = S(n).val(), a) return t;
                                    s.push(t)
                                }
                            return s
                        },
                        set: function(e, t) {
                            var n, r, i = e.options,
                                o = S.makeArray(t),
                                a = i.length;
                            while (a--)((r = i[a]).selected = -1 < S.inArray(S.valHooks.option.get(r), o)) && (n = !0);
                            return n || (e.selectedIndex = -1), o
                        }
                    }
                }
            }), S.each(["radio", "checkbox"], function() {
                S.valHooks[this] = {
                    set: function(e, t) {
                        if (Array.isArray(t)) return e.checked = -1 < S.inArray(S(e).val(), t)
                    }
                }, y.checkOn || (S.valHooks[this].get = function(e) {
                    return null === e.getAttribute("value") ? "on" : e.value
                })
            }), y.focusin = "onfocusin" in C;
            var bt = /^(?:focusinfocus|focusoutblur)$/,
                wt = function(e) {
                    e.stopPropagation()
                };
            S.extend(S.event, {
                trigger: function(e, t, n, r) {
                    var i, o, a, s, u, l, c, f, p = [n || E],
                        d = v.call(e, "type") ? e.type : e,
                        h = v.call(e, "namespace") ? e.namespace.split(".") : [];
                    if (o = f = a = n = n || E, 3 !== n.nodeType && 8 !== n.nodeType && !bt.test(d + S.event.triggered) && (-1 < d.indexOf(".") && (d = (h = d.split(".")).shift(), h.sort()), u = d.indexOf(":") < 0 && "on" + d, (e = e[S.expando] ? e : new S.Event(d, "object" == typeof e && e)).isTrigger = r ? 2 : 3, e.namespace = h.join("."), e.rnamespace = e.namespace ? new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, e.result = void 0, e.target || (e.target = n), t = null == t ? [e] : S.makeArray(t, [e]), c = S.event.special[d] || {}, r || !c.trigger || !1 !== c.trigger.apply(n, t))) {
                        if (!r && !c.noBubble && !x(n)) {
                            for (s = c.delegateType || d, bt.test(s + d) || (o = o.parentNode); o; o = o.parentNode) p.push(o), a = o;
                            a === (n.ownerDocument || E) && p.push(a.defaultView || a.parentWindow || C)
                        }
                        i = 0;
                        while ((o = p[i++]) && !e.isPropagationStopped()) f = o, e.type = 1 < i ? s : c.bindType || d, (l = (Y.get(o, "events") || Object.create(null))[e.type] && Y.get(o, "handle")) && l.apply(o, t), (l = u && o[u]) && l.apply && V(o) && (e.result = l.apply(o, t), !1 === e.result && e.preventDefault());
                        return e.type = d, r || e.isDefaultPrevented() || c._default && !1 !== c._default.apply(p.pop(), t) || !V(n) || u && m(n[d]) && !x(n) && ((a = n[u]) && (n[u] = null), S.event.triggered = d, e.isPropagationStopped() && f.addEventListener(d, wt), n[d](), e.isPropagationStopped() && f.removeEventListener(d, wt), S.event.triggered = void 0, a && (n[u] = a)), e.result
                    }
                },
                simulate: function(e, t, n) {
                    var r = S.extend(new S.Event, n, {
                        type: e,
                        isSimulated: !0
                    });
                    S.event.trigger(r, null, t)
                }
            }), S.fn.extend({
                trigger: function(e, t) {
                    return this.each(function() {
                        S.event.trigger(e, t, this)
                    })
                },
                triggerHandler: function(e, t) {
                    var n = this[0];
                    if (n) return S.event.trigger(e, t, n, !0)
                }
            }), y.focusin || S.each({
                focus: "focusin",
                blur: "focusout"
            }, function(n, r) {
                var i = function(e) {
                    S.event.simulate(r, e.target, S.event.fix(e))
                };
                S.event.special[r] = {
                    setup: function() {
                        var e = this.ownerDocument || this.document || this,
                            t = Y.access(e, r);
                        t || e.addEventListener(n, i, !0), Y.access(e, r, (t || 0) + 1)
                    },
                    teardown: function() {
                        var e = this.ownerDocument || this.document || this,
                            t = Y.access(e, r) - 1;
                        t ? Y.access(e, r, t) : (e.removeEventListener(n, i, !0), Y.remove(e, r))
                    }
                }
            });
            var Tt = C.location,
                Ct = {
                    guid: Date.now()
                },
                Et = /\?/;
            S.parseXML = function(e) {
                var t;
                if (!e || "string" != typeof e) return null;
                try {
                    t = (new C.DOMParser).parseFromString(e, "text/xml")
                } catch (e) {
                    t = void 0
                }
                return t && !t.getElementsByTagName("parsererror").length || S.error("Invalid XML: " + e), t
            };
            var St = /\[\]$/,
                kt = /\r?\n/g,
                At = /^(?:submit|button|image|reset|file)$/i,
                Nt = /^(?:input|select|textarea|keygen)/i;

            function Dt(n, e, r, i) {
                var t;
                if (Array.isArray(e)) S.each(e, function(e, t) {
                    r || St.test(n) ? i(n, t) : Dt(n + "[" + ("object" == typeof t && null != t ? e : "") + "]", t, r, i)
                });
                else if (r || "object" !== w(e)) i(n, e);
                else
                    for (t in e) Dt(n + "[" + t + "]", e[t], r, i)
            }
            S.param = function(e, t) {
                var n, r = [],
                    i = function(e, t) {
                        var n = m(t) ? t() : t;
                        r[r.length] = encodeURIComponent(e) + "=" + encodeURIComponent(null == n ? "" : n)
                    };
                if (null == e) return "";
                if (Array.isArray(e) || e.jquery && !S.isPlainObject(e)) S.each(e, function() {
                    i(this.name, this.value)
                });
                else
                    for (n in e) Dt(n, e[n], t, i);
                return r.join("&")
            }, S.fn.extend({
                serialize: function() {
                    return S.param(this.serializeArray())
                },
                serializeArray: function() {
                    return this.map(function() {
                        var e = S.prop(this, "elements");
                        return e ? S.makeArray(e) : this
                    }).filter(function() {
                        var e = this.type;
                        return this.name && !S(this).is(":disabled") && Nt.test(this.nodeName) && !At.test(e) && (this.checked || !pe.test(e))
                    }).map(function(e, t) {
                        var n = S(this).val();
                        return null == n ? null : Array.isArray(n) ? S.map(n, function(e) {
                            return {
                                name: t.name,
                                value: e.replace(kt, "\r\n")
                            }
                        }) : {
                            name: t.name,
                            value: n.replace(kt, "\r\n")
                        }
                    }).get()
                }
            });
            var jt = /%20/g,
                qt = /#.*$/,
                Lt = /([?&])_=[^&]*/,
                Ht = /^(.*?):[ \t]*([^\r\n]*)$/gm,
                Ot = /^(?:GET|HEAD)$/,
                Pt = /^\/\//,
                Rt = {},
                Mt = {},
                It = "*/".concat("*"),
                Wt = E.createElement("a");

            function Ft(o) {
                return function(e, t) {
                    "string" != typeof e && (t = e, e = "*");
                    var n, r = 0,
                        i = e.toLowerCase().match(P) || [];
                    if (m(t))
                        while (n = i[r++]) "+" === n[0] ? (n = n.slice(1) || "*", (o[n] = o[n] || []).unshift(t)) : (o[n] = o[n] || []).push(t)
                }
            }

            function Bt(t, i, o, a) {
                var s = {},
                    u = t === Mt;

                function l(e) {
                    var r;
                    return s[e] = !0, S.each(t[e] || [], function(e, t) {
                        var n = t(i, o, a);
                        return "string" != typeof n || u || s[n] ? u ? !(r = n) : void 0 : (i.dataTypes.unshift(n), l(n), !1)
                    }), r
                }
                return l(i.dataTypes[0]) || !s["*"] && l("*")
            }

            function $t(e, t) {
                var n, r, i = S.ajaxSettings.flatOptions || {};
                for (n in t) void 0 !== t[n] && ((i[n] ? e : r || (r = {}))[n] = t[n]);
                return r && S.extend(!0, e, r), e
            }
            Wt.href = Tt.href, S.extend({
                active: 0,
                lastModified: {},
                etag: {},
                ajaxSettings: {
                    url: Tt.href,
                    type: "GET",
                    isLocal: /^(?:about|app|app-storage|.+-extension|file|res|widget):$/.test(Tt.protocol),
                    global: !0,
                    processData: !0,
                    async: !0,
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    accepts: {
                        "*": It,
                        text: "text/plain",
                        html: "text/html",
                        xml: "application/xml, text/xml",
                        json: "application/json, text/javascript"
                    },
                    contents: {
                        xml: /\bxml\b/,
                        html: /\bhtml/,
                        json: /\bjson\b/
                    },
                    responseFields: {
                        xml: "responseXML",
                        text: "responseText",
                        json: "responseJSON"
                    },
                    converters: {
                        "* text": String,
                        "text html": !0,
                        "text json": JSON.parse,
                        "text xml": S.parseXML
                    },
                    flatOptions: {
                        url: !0,
                        context: !0
                    }
                },
                ajaxSetup: function(e, t) {
                    return t ? $t($t(e, S.ajaxSettings), t) : $t(S.ajaxSettings, e)
                },
                ajaxPrefilter: Ft(Rt),
                ajaxTransport: Ft(Mt),
                ajax: function(e, t) {
                    "object" == typeof e && (t = e, e = void 0), t = t || {};
                    var c, f, p, n, d, r, h, g, i, o, v = S.ajaxSetup({}, t),
                        y = v.context || v,
                        m = v.context && (y.nodeType || y.jquery) ? S(y) : S.event,
                        x = S.Deferred(),
                        b = S.Callbacks("once memory"),
                        w = v.statusCode || {},
                        a = {},
                        s = {},
                        u = "canceled",
                        T = {
                            readyState: 0,
                            getResponseHeader: function(e) {
                                var t;
                                if (h) {
                                    if (!n) {
                                        n = {};
                                        while (t = Ht.exec(p)) n[t[1].toLowerCase() + " "] = (n[t[1].toLowerCase() + " "] || []).concat(t[2])
                                    }
                                    t = n[e.toLowerCase() + " "]
                                }
                                return null == t ? null : t.join(", ")
                            },
                            getAllResponseHeaders: function() {
                                return h ? p : null
                            },
                            setRequestHeader: function(e, t) {
                                return null == h && (e = s[e.toLowerCase()] = s[e.toLowerCase()] || e, a[e] = t), this
                            },
                            overrideMimeType: function(e) {
                                return null == h && (v.mimeType = e), this
                            },
                            statusCode: function(e) {
                                var t;
                                if (e)
                                    if (h) T.always(e[T.status]);
                                    else
                                        for (t in e) w[t] = [w[t], e[t]];
                                return this
                            },
                            abort: function(e) {
                                var t = e || u;
                                return c && c.abort(t), l(0, t), this
                            }
                        };
                    if (x.promise(T), v.url = ((e || v.url || Tt.href) + "").replace(Pt, Tt.protocol + "//"), v.type = t.method || t.type || v.method || v.type, v.dataTypes = (v.dataType || "*").toLowerCase().match(P) || [""], null == v.crossDomain) {
                        r = E.createElement("a");
                        try {
                            r.href = v.url, r.href = r.href, v.crossDomain = Wt.protocol + "//" + Wt.host != r.protocol + "//" + r.host
                        } catch (e) {
                            v.crossDomain = !0
                        }
                    }
                    if (v.data && v.processData && "string" != typeof v.data && (v.data = S.param(v.data, v.traditional)), Bt(Rt, v, t, T), h) return T;
                    for (i in (g = S.event && v.global) && 0 == S.active++ && S.event.trigger("ajaxStart"), v.type = v.type.toUpperCase(), v.hasContent = !Ot.test(v.type), f = v.url.replace(qt, ""), v.hasContent ? v.data && v.processData && 0 === (v.contentType || "").indexOf("application/x-www-form-urlencoded") && (v.data = v.data.replace(jt, "+")) : (o = v.url.slice(f.length), v.data && (v.processData || "string" == typeof v.data) && (f += (Et.test(f) ? "&" : "?") + v.data, delete v.data), !1 === v.cache && (f = f.replace(Lt, "$1"), o = (Et.test(f) ? "&" : "?") + "_=" + Ct.guid++ + o), v.url = f + o), v.ifModified && (S.lastModified[f] && T.setRequestHeader("If-Modified-Since", S.lastModified[f]), S.etag[f] && T.setRequestHeader("If-None-Match", S.etag[f])), (v.data && v.hasContent && !1 !== v.contentType || t.contentType) && T.setRequestHeader("Content-Type", v.contentType), T.setRequestHeader("Accept", v.dataTypes[0] && v.accepts[v.dataTypes[0]] ? v.accepts[v.dataTypes[0]] + ("*" !== v.dataTypes[0] ? ", " + It + "; q=0.01" : "") : v.accepts["*"]), v.headers) T.setRequestHeader(i, v.headers[i]);
                    if (v.beforeSend && (!1 === v.beforeSend.call(y, T, v) || h)) return T.abort();
                    if (u = "abort", b.add(v.complete), T.done(v.success), T.fail(v.error), c = Bt(Mt, v, t, T)) {
                        if (T.readyState = 1, g && m.trigger("ajaxSend", [T, v]), h) return T;
                        v.async && 0 < v.timeout && (d = C.setTimeout(function() {
                            T.abort("timeout")
                        }, v.timeout));
                        try {
                            h = !1, c.send(a, l)
                        } catch (e) {
                            if (h) throw e;
                            l(-1, e)
                        }
                    } else l(-1, "No Transport");

                    function l(e, t, n, r) {
                        var i, o, a, s, u, l = t;
                        h || (h = !0, d && C.clearTimeout(d), c = void 0, p = r || "", T.readyState = 0 < e ? 4 : 0, i = 200 <= e && e < 300 || 304 === e, n && (s = function(e, t, n) {
                            var r, i, o, a, s = e.contents,
                                u = e.dataTypes;
                            while ("*" === u[0]) u.shift(), void 0 === r && (r = e.mimeType || t.getResponseHeader("Content-Type"));
                            if (r)
                                for (i in s)
                                    if (s[i] && s[i].test(r)) {
                                        u.unshift(i);
                                        break
                                    }
                            if (u[0] in n) o = u[0];
                            else {
                                for (i in n) {
                                    if (!u[0] || e.converters[i + " " + u[0]]) {
                                        o = i;
                                        break
                                    }
                                    a || (a = i)
                                }
                                o = o || a
                            }
                            if (o) return o !== u[0] && u.unshift(o), n[o]
                        }(v, T, n)), !i && -1 < S.inArray("script", v.dataTypes) && (v.converters["text script"] = function() {}), s = function(e, t, n, r) {
                            var i, o, a, s, u, l = {},
                                c = e.dataTypes.slice();
                            if (c[1])
                                for (a in e.converters) l[a.toLowerCase()] = e.converters[a];
                            o = c.shift();
                            while (o)
                                if (e.responseFields[o] && (n[e.responseFields[o]] = t), !u && r && e.dataFilter && (t = e.dataFilter(t, e.dataType)), u = o, o = c.shift())
                                    if ("*" === o) o = u;
                                    else if ("*" !== u && u !== o) {
                                if (!(a = l[u + " " + o] || l["* " + o]))
                                    for (i in l)
                                        if ((s = i.split(" "))[1] === o && (a = l[u + " " + s[0]] || l["* " + s[0]])) {
                                            !0 === a ? a = l[i] : !0 !== l[i] && (o = s[0], c.unshift(s[1]));
                                            break
                                        }
                                if (!0 !== a)
                                    if (a && e["throws"]) t = a(t);
                                    else try {
                                        t = a(t)
                                    } catch (e) {
                                        return {
                                            state: "parsererror",
                                            error: a ? e : "No conversion from " + u + " to " + o
                                        }
                                    }
                            }
                            return {
                                state: "success",
                                data: t
                            }
                        }(v, s, T, i), i ? (v.ifModified && ((u = T.getResponseHeader("Last-Modified")) && (S.lastModified[f] = u), (u = T.getResponseHeader("etag")) && (S.etag[f] = u)), 204 === e || "HEAD" === v.type ? l = "nocontent" : 304 === e ? l = "notmodified" : (l = s.state, o = s.data, i = !(a = s.error))) : (a = l, !e && l || (l = "error", e < 0 && (e = 0))), T.status = e, T.statusText = (t || l) + "", i ? x.resolveWith(y, [o, l, T]) : x.rejectWith(y, [T, l, a]), T.statusCode(w), w = void 0, g && m.trigger(i ? "ajaxSuccess" : "ajaxError", [T, v, i ? o : a]), b.fireWith(y, [T, l]), g && (m.trigger("ajaxComplete", [T, v]), --S.active || S.event.trigger("ajaxStop")))
                    }
                    return T
                },
                getJSON: function(e, t, n) {
                    return S.get(e, t, n, "json")
                },
                getScript: function(e, t) {
                    return S.get(e, void 0, t, "script")
                }
            }), S.each(["get", "post"], function(e, i) {
                S[i] = function(e, t, n, r) {
                    return m(t) && (r = r || n, n = t, t = void 0), S.ajax(S.extend({
                        url: e,
                        type: i,
                        dataType: r,
                        data: t,
                        success: n
                    }, S.isPlainObject(e) && e))
                }
            }), S.ajaxPrefilter(function(e) {
                var t;
                for (t in e.headers) "content-type" === t.toLowerCase() && (e.contentType = e.headers[t] || "")
            }), S._evalUrl = function(e, t, n) {
                return S.ajax({
                    url: e,
                    type: "GET",
                    dataType: "script",
                    cache: !0,
                    async: !1,
                    global: !1,
                    converters: {
                        "text script": function() {}
                    },
                    dataFilter: function(e) {
                        S.globalEval(e, t, n)
                    }
                })
            }, S.fn.extend({
                wrapAll: function(e) {
                    var t;
                    return this[0] && (m(e) && (e = e.call(this[0])), t = S(e, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && t.insertBefore(this[0]), t.map(function() {
                        var e = this;
                        while (e.firstElementChild) e = e.firstElementChild;
                        return e
                    }).append(this)), this
                },
                wrapInner: function(n) {
                    return m(n) ? this.each(function(e) {
                        S(this).wrapInner(n.call(this, e))
                    }) : this.each(function() {
                        var e = S(this),
                            t = e.contents();
                        t.length ? t.wrapAll(n) : e.append(n)
                    })
                },
                wrap: function(t) {
                    var n = m(t);
                    return this.each(function(e) {
                        S(this).wrapAll(n ? t.call(this, e) : t)
                    })
                },
                unwrap: function(e) {
                    return this.parent(e).not("body").each(function() {
                        S(this).replaceWith(this.childNodes)
                    }), this
                }
            }), S.expr.pseudos.hidden = function(e) {
                return !S.expr.pseudos.visible(e)
            }, S.expr.pseudos.visible = function(e) {
                return !!(e.offsetWidth || e.offsetHeight || e.getClientRects().length)
            }, S.ajaxSettings.xhr = function() {
                try {
                    return new C.XMLHttpRequest
                } catch (e) {}
            };
            var _t = {
                    0: 200,
                    1223: 204
                },
                zt = S.ajaxSettings.xhr();
            y.cors = !!zt && "withCredentials" in zt, y.ajax = zt = !!zt, S.ajaxTransport(function(i) {
                var o, a;
                if (y.cors || zt && !i.crossDomain) return {
                    send: function(e, t) {
                        var n, r = i.xhr();
                        if (r.open(i.type, i.url, i.async, i.username, i.password), i.xhrFields)
                            for (n in i.xhrFields) r[n] = i.xhrFields[n];
                        for (n in i.mimeType && r.overrideMimeType && r.overrideMimeType(i.mimeType), i.crossDomain || e["X-Requested-With"] || (e["X-Requested-With"] = "XMLHttpRequest"), e) r.setRequestHeader(n, e[n]);
                        o = function(e) {
                            return function() {
                                o && (o = a = r.onload = r.onerror = r.onabort = r.ontimeout = r.onreadystatechange = null, "abort" === e ? r.abort() : "error" === e ? "number" != typeof r.status ? t(0, "error") : t(r.status, r.statusText) : t(_t[r.status] || r.status, r.statusText, "text" !== (r.responseType || "text") || "string" != typeof r.responseText ? {
                                    binary: r.response
                                } : {
                                    text: r.responseText
                                }, r.getAllResponseHeaders()))
                            }
                        }, r.onload = o(), a = r.onerror = r.ontimeout = o("error"), void 0 !== r.onabort ? r.onabort = a : r.onreadystatechange = function() {
                            4 === r.readyState && C.setTimeout(function() {
                                o && a()
                            })
                        }, o = o("abort");
                        try {
                            r.send(i.hasContent && i.data || null)
                        } catch (e) {
                            if (o) throw e
                        }
                    },
                    abort: function() {
                        o && o()
                    }
                }
            }), S.ajaxPrefilter(function(e) {
                e.crossDomain && (e.contents.script = !1)
            }), S.ajaxSetup({
                accepts: {
                    script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
                },
                contents: {
                    script: /\b(?:java|ecma)script\b/
                },
                converters: {
                    "text script": function(e) {
                        return S.globalEval(e), e
                    }
                }
            }), S.ajaxPrefilter("script", function(e) {
                void 0 === e.cache && (e.cache = !1), e.crossDomain && (e.type = "GET")
            }), S.ajaxTransport("script", function(n) {
                var r, i;
                if (n.crossDomain || n.scriptAttrs) return {
                    send: function(e, t) {
                        r = S("<script>").attr(n.scriptAttrs || {}).prop({
                            charset: n.scriptCharset,
                            src: n.url
                        }).on("load error", i = function(e) {
                            r.remove(), i = null, e && t("error" === e.type ? 404 : 200, e.type)
                        }), E.head.appendChild(r[0])
                    },
                    abort: function() {
                        i && i()
                    }
                }
            });
            var Ut, Xt = [],
                Vt = /(=)\?(?=&|$)|\?\?/;
            S.ajaxSetup({
                jsonp: "callback",
                jsonpCallback: function() {
                    var e = Xt.pop() || S.expando + "_" + Ct.guid++;
                    return this[e] = !0, e
                }
            }), S.ajaxPrefilter("json jsonp", function(e, t, n) {
                var r, i, o, a = !1 !== e.jsonp && (Vt.test(e.url) ? "url" : "string" == typeof e.data && 0 === (e.contentType || "").indexOf("application/x-www-form-urlencoded") && Vt.test(e.data) && "data");
                if (a || "jsonp" === e.dataTypes[0]) return r = e.jsonpCallback = m(e.jsonpCallback) ? e.jsonpCallback() : e.jsonpCallback, a ? e[a] = e[a].replace(Vt, "$1" + r) : !1 !== e.jsonp && (e.url += (Et.test(e.url) ? "&" : "?") + e.jsonp + "=" + r), e.converters["script json"] = function() {
                    return o || S.error(r + " was not called"), o[0]
                }, e.dataTypes[0] = "json", i = C[r], C[r] = function() {
                    o = arguments
                }, n.always(function() {
                    void 0 === i ? S(C).removeProp(r) : C[r] = i, e[r] && (e.jsonpCallback = t.jsonpCallback, Xt.push(r)), o && m(i) && i(o[0]), o = i = void 0
                }), "script"
            }), y.createHTMLDocument = ((Ut = E.implementation.createHTMLDocument("").body).innerHTML = "<form></form><form></form>", 2 === Ut.childNodes.length), S.parseHTML = function(e, t, n) {
                return "string" != typeof e ? [] : ("boolean" == typeof t && (n = t, t = !1), t || (y.createHTMLDocument ? ((r = (t = E.implementation.createHTMLDocument("")).createElement("base")).href = E.location.href, t.head.appendChild(r)) : t = E), o = !n && [], (i = N.exec(e)) ? [t.createElement(i[1])] : (i = xe([e], t, o), o && o.length && S(o).remove(), S.merge([], i.childNodes)));
                var r, i, o
            }, S.fn.load = function(e, t, n) {
                var r, i, o, a = this,
                    s = e.indexOf(" ");
                return -1 < s && (r = vt(e.slice(s)), e = e.slice(0, s)), m(t) ? (n = t, t = void 0) : t && "object" == typeof t && (i = "POST"), 0 < a.length && S.ajax({
                    url: e,
                    type: i || "GET",
                    dataType: "html",
                    data: t
                }).done(function(e) {
                    o = arguments, a.html(r ? S("<div>").append(S.parseHTML(e)).find(r) : e)
                }).always(n && function(e, t) {
                    a.each(function() {
                        n.apply(this, o || [e.responseText, t, e])
                    })
                }), this
            }, S.expr.pseudos.animated = function(t) {
                return S.grep(S.timers, function(e) {
                    return t === e.elem
                }).length
            }, S.offset = {
                setOffset: function(e, t, n) {
                    var r, i, o, a, s, u, l = S.css(e, "position"),
                        c = S(e),
                        f = {};
                    "static" === l && (e.style.position = "relative"), s = c.offset(), o = S.css(e, "top"), u = S.css(e, "left"), ("absolute" === l || "fixed" === l) && -1 < (o + u).indexOf("auto") ? (a = (r = c.position()).top, i = r.left) : (a = parseFloat(o) || 0, i = parseFloat(u) || 0), m(t) && (t = t.call(e, n, S.extend({}, s))), null != t.top && (f.top = t.top - s.top + a), null != t.left && (f.left = t.left - s.left + i), "using" in t ? t.using.call(e, f) : ("number" == typeof f.top && (f.top += "px"), "number" == typeof f.left && (f.left += "px"), c.css(f))
                }
            }, S.fn.extend({
                offset: function(t) {
                    if (arguments.length) return void 0 === t ? this : this.each(function(e) {
                        S.offset.setOffset(this, t, e)
                    });
                    var e, n, r = this[0];
                    return r ? r.getClientRects().length ? (e = r.getBoundingClientRect(), n = r.ownerDocument.defaultView, {
                        top: e.top + n.pageYOffset,
                        left: e.left + n.pageXOffset
                    }) : {
                        top: 0,
                        left: 0
                    } : void 0
                },
                position: function() {
                    if (this[0]) {
                        var e, t, n, r = this[0],
                            i = {
                                top: 0,
                                left: 0
                            };
                        if ("fixed" === S.css(r, "position")) t = r.getBoundingClientRect();
                        else {
                            t = this.offset(), n = r.ownerDocument, e = r.offsetParent || n.documentElement;
                            while (e && (e === n.body || e === n.documentElement) && "static" === S.css(e, "position")) e = e.parentNode;
                            e && e !== r && 1 === e.nodeType && ((i = S(e).offset()).top += S.css(e, "borderTopWidth", !0), i.left += S.css(e, "borderLeftWidth", !0))
                        }
                        return {
                            top: t.top - i.top - S.css(r, "marginTop", !0),
                            left: t.left - i.left - S.css(r, "marginLeft", !0)
                        }
                    }
                },
                offsetParent: function() {
                    return this.map(function() {
                        var e = this.offsetParent;
                        while (e && "static" === S.css(e, "position")) e = e.offsetParent;
                        return e || re
                    })
                }
            }), S.each({
                scrollLeft: "pageXOffset",
                scrollTop: "pageYOffset"
            }, function(t, i) {
                var o = "pageYOffset" === i;
                S.fn[t] = function(e) {
                    return $(this, function(e, t, n) {
                        var r;
                        if (x(e) ? r = e : 9 === e.nodeType && (r = e.defaultView), void 0 === n) return r ? r[i] : e[t];
                        r ? r.scrollTo(o ? r.pageXOffset : n, o ? n : r.pageYOffset) : e[t] = n
                    }, t, e, arguments.length)
                }
            }), S.each(["top", "left"], function(e, n) {
                S.cssHooks[n] = $e(y.pixelPosition, function(e, t) {
                    if (t) return t = Be(e, n), Me.test(t) ? S(e).position()[n] + "px" : t
                })
            }), S.each({
                Height: "height",
                Width: "width"
            }, function(a, s) {
                S.each({
                    padding: "inner" + a,
                    content: s,
                    "": "outer" + a
                }, function(r, o) {
                    S.fn[o] = function(e, t) {
                        var n = arguments.length && (r || "boolean" != typeof e),
                            i = r || (!0 === e || !0 === t ? "margin" : "border");
                        return $(this, function(e, t, n) {
                            var r;
                            return x(e) ? 0 === o.indexOf("outer") ? e["inner" + a] : e.document.documentElement["client" + a] : 9 === e.nodeType ? (r = e.documentElement, Math.max(e.body["scroll" + a], r["scroll" + a], e.body["offset" + a], r["offset" + a], r["client" + a])) : void 0 === n ? S.css(e, t, i) : S.style(e, t, n, i)
                        }, s, n ? e : void 0, n)
                    }
                })
            }), S.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function(e, t) {
                S.fn[t] = function(e) {
                    return this.on(t, e)
                }
            }), S.fn.extend({
                bind: function(e, t, n) {
                    return this.on(e, null, t, n)
                },
                unbind: function(e, t) {
                    return this.off(e, null, t)
                },
                delegate: function(e, t, n, r) {
                    return this.on(t, e, n, r)
                },
                undelegate: function(e, t, n) {
                    return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", n)
                },
                hover: function(e, t) {
                    return this.mouseenter(e).mouseleave(t || e)
                }
            }), S.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "), function(e, n) {
                S.fn[n] = function(e, t) {
                    return 0 < arguments.length ? this.on(n, null, e, t) : this.trigger(n)
                }
            });
            var Gt = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
            S.proxy = function(e, t) {
                var n, r, i;
                if ("string" == typeof t && (n = e[t], t = e, e = n), m(e)) return r = s.call(arguments, 2), (i = function() {
                    return e.apply(t || this, r.concat(s.call(arguments)))
                }).guid = e.guid = e.guid || S.guid++, i
            }, S.holdReady = function(e) {
                e ? S.readyWait++ : S.ready(!0)
            }, S.isArray = Array.isArray, S.parseJSON = JSON.parse, S.nodeName = A, S.isFunction = m, S.isWindow = x, S.camelCase = X, S.type = w, S.now = Date.now, S.isNumeric = function(e) {
                var t = S.type(e);
                return ("number" === t || "string" === t) && !isNaN(e - parseFloat(e))
            }, S.trim = function(e) {
                return null == e ? "" : (e + "").replace(Gt, "")
            }, "function" == typeof define && define.amd && define("jquery", [], function() {
                return S
            });
            var Yt = C.jQuery,
                Qt = C.$;
            return S.noConflict = function(e) {
                return C.$ === S && (C.$ = Qt), e && C.jQuery === S && (C.jQuery = Yt), S
            }, "undefined" == typeof e && (C.jQuery = C.$ = S), S
        });
    </script>
    <script type="text/javascript">
        /*!

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * Webflow: Front-end site library
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * @license MIT
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * Inline scripts may access the api using an async handler:
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *   var Webflow = Webflow || [];
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *   Webflow.push(readyFunction);
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 */
        ! function(t) {
            var e = {};

            function n(i) {
                if (e[i]) return e[i].exports;
                var r = e[i] = {
                    i: i,
                    l: !1,
                    exports: {}
                };
                return t[i].call(r.exports, r, r.exports, n), r.l = !0, r.exports
            }
            n.m = t, n.c = e, n.d = function(t, e, i) {
                n.o(t, e) || Object.defineProperty(t, e, {
                    enumerable: !0,
                    get: i
                })
            }, n.r = function(t) {
                "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, {
                    value: "Module"
                }), Object.defineProperty(t, "__esModule", {
                    value: !0
                })
            }, n.t = function(t, e) {
                if (1 & e && (t = n(t)), 8 & e) return t;
                if (4 & e && "object" == typeof t && t && t.__esModule) return t;
                var i = Object.create(null);
                if (n.r(i), Object.defineProperty(i, "default", {
                        enumerable: !0,
                        value: t
                    }), 2 & e && "string" != typeof t)
                    for (var r in t) n.d(i, r, function(e) {
                        return t[e]
                    }.bind(null, r));
                return i
            }, n.n = function(t) {
                var e = t && t.__esModule ? function() {
                    return t.default
                } : function() {
                    return t
                };
                return n.d(e, "a", e), e
            }, n.o = function(t, e) {
                return Object.prototype.hasOwnProperty.call(t, e)
            }, n.p = "", n(n.s = 2)
        }([function(t, e, n) {
                "use strict";
                var i = {},
                    r = {},
                    o = [],
                    a = window.Webflow || [],
                    s = window.jQuery,
                    u = s(window),
                    c = s(document),
                    l = s.isFunction,
                    f = i._ = n(4),
                    h = i.tram = n(1) && s.tram,
                    d = !1,
                    p = !1;

                function v(t) {
                    i.env() && (l(t.design) && u.on("__wf_design", t.design), l(t.preview) && u.on("__wf_preview", t.preview)), l(t.destroy) && u.on("__wf_destroy", t.destroy), t.ready && l(t.ready) && function(t) {
                        if (d) return void t.ready();
                        if (f.contains(o, t.ready)) return;
                        o.push(t.ready)
                    }(t)
                }

                function m(t) {
                    l(t.design) && u.off("__wf_design", t.design), l(t.preview) && u.off("__wf_preview", t.preview), l(t.destroy) && u.off("__wf_destroy", t.destroy), t.ready && l(t.ready) && function(t) {
                        o = f.filter(o, function(e) {
                            return e !== t.ready
                        })
                    }(t)
                }
                h.config.hideBackface = !1, h.config.keepInherited = !0, i.define = function(t, e, n) {
                    r[t] && m(r[t]);
                    var i = r[t] = e(s, f, n) || {};
                    return v(i), i
                }, i.require = function(t) {
                    return r[t]
                }, i.push = function(t) {
                    d ? l(t) && t() : a.push(t)
                }, i.env = function(t) {
                    var e = window.__wf_design,
                        n = void 0 !== e;
                    return t ? "design" === t ? n && e : "preview" === t ? n && !e : "slug" === t ? n && window.__wf_slug : "editor" === t ? window.WebflowEditor : "test" === t ? window.__wf_test : "frame" === t ? window !== window.top : void 0 : n
                };
                var w, g = navigator.userAgent.toLowerCase(),
                    b = i.env.touch = "ontouchstart" in window || window.DocumentTouch && document instanceof window.DocumentTouch,
                    y = i.env.chrome = /chrome/.test(g) && /Google/.test(navigator.vendor) && parseInt(g.match(/chrome\/(\d+)\./)[1], 10),
                    x = i.env.ios = /(ipod|iphone|ipad)/.test(g);
                i.env.safari = /safari/.test(g) && !y && !x, b && c.on("touchstart mousedown", function(t) {
                    w = t.target
                }), i.validClick = b ? function(t) {
                    return t === w || s.contains(t, w)
                } : function() {
                    return !0
                };
                var _, k = "resize.webflow orientationchange.webflow load.webflow";

                function E(t, e) {
                    var n = [],
                        i = {};
                    return i.up = f.throttle(function(t) {
                        f.each(n, function(e) {
                            e(t)
                        })
                    }), t && e && t.on(e, i.up), i.on = function(t) {
                        "function" == typeof t && (f.contains(n, t) || n.push(t))
                    }, i.off = function(t) {
                        n = arguments.length ? f.filter(n, function(e) {
                            return e !== t
                        }) : []
                    }, i
                }

                function O(t) {
                    l(t) && t()
                }

                function T() {
                    _ && (_.reject(), u.off("load", _.resolve)), _ = new s.Deferred, u.on("load", _.resolve)
                }
                i.resize = E(u, k), i.scroll = E(u, "scroll.webflow resize.webflow orientationchange.webflow load.webflow"), i.redraw = E(), i.location = function(t) {
                    window.location = t
                }, i.env() && (i.location = function() {}), i.ready = function() {
                    d = !0, p ? (p = !1, f.each(r, v)) : f.each(o, O), f.each(a, O), i.resize.up()
                }, i.load = function(t) {
                    _.then(t)
                }, i.destroy = function(t) {
                    t = t || {}, p = !0, u.triggerHandler("__wf_destroy"), null != t.domready && (d = t.domready), f.each(r, m), i.resize.off(), i.scroll.off(), i.redraw.off(), o = [], a = [], "pending" === _.state() && T()
                }, s(i.ready), T(), t.exports = window.Webflow = i
            }, function(t, e, n) {
                "use strict";
                var i = n(5)(n(6));
                window.tram = function(t) {
                    function e(t, e) {
                        return (new W.Bare).init(t, e)
                    }

                    function n(t) {
                        return t.replace(/[A-Z]/g, function(t) {
                            return "-" + t.toLowerCase()
                        })
                    }

                    function r(t) {
                        var e = parseInt(t.slice(1), 16);
                        return [e >> 16 & 255, e >> 8 & 255, 255 & e]
                    }

                    function o(t, e, n) {
                        return "#" + (1 << 24 | t << 16 | e << 8 | n).toString(16).slice(1)
                    }

                    function a() {}

                    function s(t, e, n) {
                        c("Units do not match [" + t + "]: " + e + ", " + n)
                    }

                    function u(t, e, n) {
                        if (void 0 !== e && (n = e), void 0 === t) return n;
                        var i = n;
                        return Q.test(t) || !V.test(t) ? i = parseInt(t, 10) : V.test(t) && (i = 1e3 * parseFloat(t)), 0 > i && (i = 0), i == i ? i : n
                    }

                    function c(t) {
                        X.debug && window && window.console.warn(t)
                    }
                    var l = function(t, e, n) {
                            function r(t) {
                                return "object" == (0, i.default)(t)
                            }

                            function o(t) {
                                return "function" == typeof t
                            }

                            function a() {}
                            return function i(s, u) {
                                function c() {
                                    var t = new l;
                                    return o(t.init) && t.init.apply(t, arguments), t
                                }

                                function l() {}
                                u === n && (u = s, s = Object), c.Bare = l;
                                var f, h = a[t] = s[t],
                                    d = l[t] = c[t] = new a;
                                return d.constructor = c, c.mixin = function(e) {
                                    return l[t] = c[t] = i(c, e)[t], c
                                }, c.open = function(t) {
                                    if (f = {}, o(t) ? f = t.call(c, d, h, c, s) : r(t) && (f = t), r(f))
                                        for (var n in f) e.call(f, n) && (d[n] = f[n]);
                                    return o(d.init) || (d.init = s), c
                                }, c.open(u)
                            }
                        }("prototype", {}.hasOwnProperty),
                        f = {
                            ease: ["ease", function(t, e, n, i) {
                                var r = (t /= i) * t,
                                    o = r * t;
                                return e + n * (-2.75 * o * r + 11 * r * r + -15.5 * o + 8 * r + .25 * t)
                            }],
                            "ease-in": ["ease-in", function(t, e, n, i) {
                                var r = (t /= i) * t,
                                    o = r * t;
                                return e + n * (-1 * o * r + 3 * r * r + -3 * o + 2 * r)
                            }],
                            "ease-out": ["ease-out", function(t, e, n, i) {
                                var r = (t /= i) * t,
                                    o = r * t;
                                return e + n * (.3 * o * r + -1.6 * r * r + 2.2 * o + -1.8 * r + 1.9 * t)
                            }],
                            "ease-in-out": ["ease-in-out", function(t, e, n, i) {
                                var r = (t /= i) * t,
                                    o = r * t;
                                return e + n * (2 * o * r + -5 * r * r + 2 * o + 2 * r)
                            }],
                            linear: ["linear", function(t, e, n, i) {
                                return n * t / i + e
                            }],
                            "ease-in-quad": ["cubic-bezier(0.550, 0.085, 0.680, 0.530)", function(t, e, n, i) {
                                return n * (t /= i) * t + e
                            }],
                            "ease-out-quad": ["cubic-bezier(0.250, 0.460, 0.450, 0.940)", function(t, e, n, i) {
                                return -n * (t /= i) * (t - 2) + e
                            }],
                            "ease-in-out-quad": ["cubic-bezier(0.455, 0.030, 0.515, 0.955)", function(t, e, n, i) {
                                return (t /= i / 2) < 1 ? n / 2 * t * t + e : -n / 2 * (--t * (t - 2) - 1) + e
                            }],
                            "ease-in-cubic": ["cubic-bezier(0.550, 0.055, 0.675, 0.190)", function(t, e, n, i) {
                                return n * (t /= i) * t * t + e
                            }],
                            "ease-out-cubic": ["cubic-bezier(0.215, 0.610, 0.355, 1)", function(t, e, n, i) {
                                return n * ((t = t / i - 1) * t * t + 1) + e
                            }],
                            "ease-in-out-cubic": ["cubic-bezier(0.645, 0.045, 0.355, 1)", function(t, e, n, i) {
                                return (t /= i / 2) < 1 ? n / 2 * t * t * t + e : n / 2 * ((t -= 2) * t * t + 2) + e
                            }],
                            "ease-in-quart": ["cubic-bezier(0.895, 0.030, 0.685, 0.220)", function(t, e, n, i) {
                                return n * (t /= i) * t * t * t + e
                            }],
                            "ease-out-quart": ["cubic-bezier(0.165, 0.840, 0.440, 1)", function(t, e, n, i) {
                                return -n * ((t = t / i - 1) * t * t * t - 1) + e
                            }],
                            "ease-in-out-quart": ["cubic-bezier(0.770, 0, 0.175, 1)", function(t, e, n, i) {
                                return (t /= i / 2) < 1 ? n / 2 * t * t * t * t + e : -n / 2 * ((t -= 2) * t * t * t - 2) + e
                            }],
                            "ease-in-quint": ["cubic-bezier(0.755, 0.050, 0.855, 0.060)", function(t, e, n, i) {
                                return n * (t /= i) * t * t * t * t + e
                            }],
                            "ease-out-quint": ["cubic-bezier(0.230, 1, 0.320, 1)", function(t, e, n, i) {
                                return n * ((t = t / i - 1) * t * t * t * t + 1) + e
                            }],
                            "ease-in-out-quint": ["cubic-bezier(0.860, 0, 0.070, 1)", function(t, e, n, i) {
                                return (t /= i / 2) < 1 ? n / 2 * t * t * t * t * t + e : n / 2 * ((t -= 2) * t * t * t * t + 2) + e
                            }],
                            "ease-in-sine": ["cubic-bezier(0.470, 0, 0.745, 0.715)", function(t, e, n, i) {
                                return -n * Math.cos(t / i * (Math.PI / 2)) + n + e
                            }],
                            "ease-out-sine": ["cubic-bezier(0.390, 0.575, 0.565, 1)", function(t, e, n, i) {
                                return n * Math.sin(t / i * (Math.PI / 2)) + e
                            }],
                            "ease-in-out-sine": ["cubic-bezier(0.445, 0.050, 0.550, 0.950)", function(t, e, n, i) {
                                return -n / 2 * (Math.cos(Math.PI * t / i) - 1) + e
                            }],
                            "ease-in-expo": ["cubic-bezier(0.950, 0.050, 0.795, 0.035)", function(t, e, n, i) {
                                return 0 === t ? e : n * Math.pow(2, 10 * (t / i - 1)) + e
                            }],
                            "ease-out-expo": ["cubic-bezier(0.190, 1, 0.220, 1)", function(t, e, n, i) {
                                return t === i ? e + n : n * (1 - Math.pow(2, -10 * t / i)) + e
                            }],
                            "ease-in-out-expo": ["cubic-bezier(1, 0, 0, 1)", function(t, e, n, i) {
                                return 0 === t ? e : t === i ? e + n : (t /= i / 2) < 1 ? n / 2 * Math.pow(2, 10 * (t - 1)) + e : n / 2 * (2 - Math.pow(2, -10 * --t)) + e
                            }],
                            "ease-in-circ": ["cubic-bezier(0.600, 0.040, 0.980, 0.335)", function(t, e, n, i) {
                                return -n * (Math.sqrt(1 - (t /= i) * t) - 1) + e
                            }],
                            "ease-out-circ": ["cubic-bezier(0.075, 0.820, 0.165, 1)", function(t, e, n, i) {
                                return n * Math.sqrt(1 - (t = t / i - 1) * t) + e
                            }],
                            "ease-in-out-circ": ["cubic-bezier(0.785, 0.135, 0.150, 0.860)", function(t, e, n, i) {
                                return (t /= i / 2) < 1 ? -n / 2 * (Math.sqrt(1 - t * t) - 1) + e : n / 2 * (Math.sqrt(1 - (t -= 2) * t) + 1) + e
                            }],
                            "ease-in-back": ["cubic-bezier(0.600, -0.280, 0.735, 0.045)", function(t, e, n, i, r) {
                                return void 0 === r && (r = 1.70158), n * (t /= i) * t * ((r + 1) * t - r) + e
                            }],
                            "ease-out-back": ["cubic-bezier(0.175, 0.885, 0.320, 1.275)", function(t, e, n, i, r) {
                                return void 0 === r && (r = 1.70158), n * ((t = t / i - 1) * t * ((r + 1) * t + r) + 1) + e
                            }],
                            "ease-in-out-back": ["cubic-bezier(0.680, -0.550, 0.265, 1.550)", function(t, e, n, i, r) {
                                return void 0 === r && (r = 1.70158), (t /= i / 2) < 1 ? n / 2 * t * t * ((1 + (r *= 1.525)) * t - r) + e : n / 2 * ((t -= 2) * t * ((1 + (r *= 1.525)) * t + r) + 2) + e
                            }]
                        },
                        h = {
                            "ease-in-back": "cubic-bezier(0.600, 0, 0.735, 0.045)",
                            "ease-out-back": "cubic-bezier(0.175, 0.885, 0.320, 1)",
                            "ease-in-out-back": "cubic-bezier(0.680, 0, 0.265, 1)"
                        },
                        d = document,
                        p = window,
                        v = "bkwld-tram",
                        m = /[\-\.0-9]/g,
                        w = /[A-Z]/,
                        g = "number",
                        b = /^(rgb|#)/,
                        y = /(em|cm|mm|in|pt|pc|px)$/,
                        x = /(em|cm|mm|in|pt|pc|px|%)$/,
                        _ = /(deg|rad|turn)$/,
                        k = "unitless",
                        E = /(all|none) 0s ease 0s/,
                        O = /^(width|height)$/,
                        T = " ",
                        S = d.createElement("a"),
                        z = ["Webkit", "Moz", "O", "ms"],
                        A = ["-webkit-", "-moz-", "-o-", "-ms-"],
                        R = function(t) {
                            if (t in S.style) return {
                                dom: t,
                                css: t
                            };
                            var e, n, i = "",
                                r = t.split("-");
                            for (e = 0; e < r.length; e++) i += r[e].charAt(0).toUpperCase() + r[e].slice(1);
                            for (e = 0; e < z.length; e++)
                                if ((n = z[e] + i) in S.style) return {
                                    dom: n,
                                    css: A[e] + t
                                }
                        },
                        C = e.support = {
                            bind: Function.prototype.bind,
                            transform: R("transform"),
                            transition: R("transition"),
                            backface: R("backface-visibility"),
                            timing: R("transition-timing-function")
                        };
                    if (C.transition) {
                        var M = C.timing.dom;
                        if (S.style[M] = f["ease-in-back"][0], !S.style[M])
                            for (var L in h) f[L][0] = h[L]
                    }
                    var j = e.frame = function() {
                            var t = p.requestAnimationFrame || p.webkitRequestAnimationFrame || p.mozRequestAnimationFrame || p.oRequestAnimationFrame || p.msRequestAnimationFrame;
                            return t && C.bind ? t.bind(p) : function(t) {
                                p.setTimeout(t, 16)
                            }
                        }(),
                        F = e.now = function() {
                            var t = p.performance,
                                e = t && (t.now || t.webkitNow || t.msNow || t.mozNow);
                            return e && C.bind ? e.bind(t) : Date.now || function() {
                                return +new Date
                            }
                        }(),
                        I = l(function(e) {
                            function r(t, e) {
                                var n = function(t) {
                                        for (var e = -1, n = t ? t.length : 0, i = []; ++e < n;) {
                                            var r = t[e];
                                            r && i.push(r)
                                        }
                                        return i
                                    }(("" + t).split(T)),
                                    i = n[0];
                                e = e || {};
                                var r = Y[i];
                                if (!r) return c("Unsupported property: " + i);
                                if (!e.weak || !this.props[i]) {
                                    var o = r[0],
                                        a = this.props[i];
                                    return a || (a = this.props[i] = new o.Bare), a.init(this.$el, n, r, e), a
                                }
                            }

                            function o(t, e, n) {
                                if (t) {
                                    var o = (0, i.default)(t);
                                    if (e || (this.timer && this.timer.destroy(), this.queue = [], this.active = !1), "number" == o && e) return this.timer = new H({
                                        duration: t,
                                        context: this,
                                        complete: a
                                    }), void(this.active = !0);
                                    if ("string" == o && e) {
                                        switch (t) {
                                            case "hide":
                                                l.call(this);
                                                break;
                                            case "stop":
                                                s.call(this);
                                                break;
                                            case "redraw":
                                                f.call(this);
                                                break;
                                            default:
                                                r.call(this, t, n && n[1])
                                        }
                                        return a.call(this)
                                    }
                                    if ("function" == o) return void t.call(this, this);
                                    if ("object" == o) {
                                        var c = 0;
                                        d.call(this, t, function(t, e) {
                                            t.span > c && (c = t.span), t.stop(), t.animate(e)
                                        }, function(t) {
                                            "wait" in t && (c = u(t.wait, 0))
                                        }), h.call(this), c > 0 && (this.timer = new H({
                                            duration: c,
                                            context: this
                                        }), this.active = !0, e && (this.timer.complete = a));
                                        var p = this,
                                            v = !1,
                                            m = {};
                                        j(function() {
                                            d.call(p, t, function(t) {
                                                t.active && (v = !0, m[t.name] = t.nextStyle)
                                            }), v && p.$el.css(m)
                                        })
                                    }
                                }
                            }

                            function a() {
                                if (this.timer && this.timer.destroy(), this.active = !1, this.queue.length) {
                                    var t = this.queue.shift();
                                    o.call(this, t.options, !0, t.args)
                                }
                            }

                            function s(t) {
                                var e;
                                this.timer && this.timer.destroy(), this.queue = [], this.active = !1, "string" == typeof t ? (e = {})[t] = 1 : e = "object" == (0, i.default)(t) && null != t ? t : this.props, d.call(this, e, p), h.call(this)
                            }

                            function l() {
                                s.call(this), this.el.style.display = "none"
                            }

                            function f() {
                                this.el.offsetHeight
                            }

                            function h() {
                                var t, e, n = [];
                                for (t in this.upstream && n.push(this.upstream), this.props)(e = this.props[t]).active && n.push(e.string);
                                n = n.join(","), this.style !== n && (this.style = n, this.el.style[C.transition.dom] = n)
                            }

                            function d(t, e, i) {
                                var o, a, s, u, c = e !== p,
                                    l = {};
                                for (o in t) s = t[o], o in K ? (l.transform || (l.transform = {}), l.transform[o] = s) : (w.test(o) && (o = n(o)), o in Y ? l[o] = s : (u || (u = {}), u[o] = s));
                                for (o in l) {
                                    if (s = l[o], !(a = this.props[o])) {
                                        if (!c) continue;
                                        a = r.call(this, o)
                                    }
                                    e.call(this, a, s)
                                }
                                i && u && i.call(this, u)
                            }

                            function p(t) {
                                t.stop()
                            }

                            function m(t, e) {
                                t.set(e)
                            }

                            function g(t) {
                                this.$el.css(t)
                            }

                            function b(t, n) {
                                e[t] = function() {
                                    return this.children ? function(t, e) {
                                        var n, i = this.children.length;
                                        for (n = 0; i > n; n++) t.apply(this.children[n], e);
                                        return this
                                    }.call(this, n, arguments) : (this.el && n.apply(this, arguments), this)
                                }
                            }
                            e.init = function(e) {
                                if (this.$el = t(e), this.el = this.$el[0], this.props = {}, this.queue = [], this.style = "", this.active = !1, X.keepInherited && !X.fallback) {
                                    var n = U(this.el, "transition");
                                    n && !E.test(n) && (this.upstream = n)
                                }
                                C.backface && X.hideBackface && G(this.el, C.backface.css, "hidden")
                            }, b("add", r), b("start", o), b("wait", function(t) {
                                t = u(t, 0), this.active ? this.queue.push({
                                    options: t
                                }) : (this.timer = new H({
                                    duration: t,
                                    context: this,
                                    complete: a
                                }), this.active = !0)
                            }), b("then", function(t) {
                                return this.active ? (this.queue.push({
                                    options: t,
                                    args: arguments
                                }), void(this.timer.complete = a)) : c("No active transition timer. Use start() or wait() before then().")
                            }), b("next", a), b("stop", s), b("set", function(t) {
                                s.call(this, t), d.call(this, t, m, g)
                            }), b("show", function(t) {
                                "string" != typeof t && (t = "block"), this.el.style.display = t
                            }), b("hide", l), b("redraw", f), b("destroy", function() {
                                s.call(this), t.removeData(this.el, v), this.$el = this.el = null
                            })
                        }),
                        W = l(I, function(e) {
                            function n(e, n) {
                                var i = t.data(e, v) || t.data(e, v, new I.Bare);
                                return i.el || i.init(e), n ? i.start(n) : i
                            }
                            e.init = function(e, i) {
                                var r = t(e);
                                if (!r.length) return this;
                                if (1 === r.length) return n(r[0], i);
                                var o = [];
                                return r.each(function(t, e) {
                                    o.push(n(e, i))
                                }), this.children = o, this
                            }
                        }),
                        q = l(function(t) {
                            function e() {
                                var t = this.get();
                                this.update("auto");
                                var e = this.get();
                                return this.update(t), e
                            }

                            function n(t) {
                                var e = /rgba?\((\d+),\s*(\d+),\s*(\d+)/.exec(t);
                                return (e ? o(e[1], e[2], e[3]) : t).replace(/#(\w)(\w)(\w)$/, "#$1$1$2$2$3$3")
                            }
                            var r = 500,
                                a = "ease",
                                s = 0;
                            t.init = function(t, e, n, i) {
                                this.$el = t, this.el = t[0];
                                var o = e[0];
                                n[2] && (o = n[2]), Z[o] && (o = Z[o]), this.name = o, this.type = n[1], this.duration = u(e[1], this.duration, r), this.ease = function(t, e, n) {
                                    return void 0 !== e && (n = e), t in f ? t : n
                                }(e[2], this.ease, a), this.delay = u(e[3], this.delay, s), this.span = this.duration + this.delay, this.active = !1, this.nextStyle = null, this.auto = O.test(this.name), this.unit = i.unit || this.unit || X.defaultUnit, this.angle = i.angle || this.angle || X.defaultAngle, X.fallback || i.fallback ? this.animate = this.fallback : (this.animate = this.transition, this.string = this.name + T + this.duration + "ms" + ("ease" != this.ease ? T + f[this.ease][0] : "") + (this.delay ? T + this.delay + "ms" : ""))
                            }, t.set = function(t) {
                                t = this.convert(t, this.type), this.update(t), this.redraw()
                            }, t.transition = function(t) {
                                this.active = !0, t = this.convert(t, this.type), this.auto && ("auto" == this.el.style[this.name] && (this.update(this.get()), this.redraw()), "auto" == t && (t = e.call(this))), this.nextStyle = t
                            }, t.fallback = function(t) {
                                var n = this.el.style[this.name] || this.convert(this.get(), this.type);
                                t = this.convert(t, this.type), this.auto && ("auto" == n && (n = this.convert(this.get(), this.type)), "auto" == t && (t = e.call(this))), this.tween = new N({
                                    from: n,
                                    to: t,
                                    duration: this.duration,
                                    delay: this.delay,
                                    ease: this.ease,
                                    update: this.update,
                                    context: this
                                })
                            }, t.get = function() {
                                return U(this.el, this.name)
                            }, t.update = function(t) {
                                G(this.el, this.name, t)
                            }, t.stop = function() {
                                (this.active || this.nextStyle) && (this.active = !1, this.nextStyle = null, G(this.el, this.name, this.get()));
                                var t = this.tween;
                                t && t.context && t.destroy()
                            }, t.convert = function(t, e) {
                                if ("auto" == t && this.auto) return t;
                                var r, o = "number" == typeof t,
                                    a = "string" == typeof t;
                                switch (e) {
                                    case g:
                                        if (o) return t;
                                        if (a && "" === t.replace(m, "")) return +t;
                                        r = "number(unitless)";
                                        break;
                                    case b:
                                        if (a) {
                                            if ("" === t && this.original) return this.original;
                                            if (e.test(t)) return "#" == t.charAt(0) && 7 == t.length ? t : n(t)
                                        }
                                        r = "hex or rgb string";
                                        break;
                                    case y:
                                        if (o) return t + this.unit;
                                        if (a && e.test(t)) return t;
                                        r = "number(px) or string(unit)";
                                        break;
                                    case x:
                                        if (o) return t + this.unit;
                                        if (a && e.test(t)) return t;
                                        r = "number(px) or string(unit or %)";
                                        break;
                                    case _:
                                        if (o) return t + this.angle;
                                        if (a && e.test(t)) return t;
                                        r = "number(deg) or string(angle)";
                                        break;
                                    case k:
                                        if (o) return t;
                                        if (a && x.test(t)) return t;
                                        r = "number(unitless) or string(unit or %)"
                                }
                                return function(t, e) {
                                    c("Type warning: Expected: [" + t + "] Got: [" + (0, i.default)(e) + "] " + e)
                                }(r, t), t
                            }, t.redraw = function() {
                                this.el.offsetHeight
                            }
                        }),
                        $ = l(q, function(t, e) {
                            t.init = function() {
                                e.init.apply(this, arguments), this.original || (this.original = this.convert(this.get(), b))
                            }
                        }),
                        D = l(q, function(t, e) {
                            t.init = function() {
                                e.init.apply(this, arguments), this.animate = this.fallback
                            }, t.get = function() {
                                return this.$el[this.name]()
                            }, t.update = function(t) {
                                this.$el[this.name](t)
                            }
                        }),
                        P = l(q, function(t, e) {
                            function n(t, e) {
                                var n, i, r, o, a;
                                for (n in t) r = (o = K[n])[0], i = o[1] || n, a = this.convert(t[n], r), e.call(this, i, a, r)
                            }
                            t.init = function() {
                                e.init.apply(this, arguments), this.current || (this.current = {}, K.perspective && X.perspective && (this.current.perspective = X.perspective, G(this.el, this.name, this.style(this.current)), this.redraw()))
                            }, t.set = function(t) {
                                n.call(this, t, function(t, e) {
                                    this.current[t] = e
                                }), G(this.el, this.name, this.style(this.current)), this.redraw()
                            }, t.transition = function(t) {
                                var e = this.values(t);
                                this.tween = new B({
                                    current: this.current,
                                    values: e,
                                    duration: this.duration,
                                    delay: this.delay,
                                    ease: this.ease
                                });
                                var n, i = {};
                                for (n in this.current) i[n] = n in e ? e[n] : this.current[n];
                                this.active = !0, this.nextStyle = this.style(i)
                            }, t.fallback = function(t) {
                                var e = this.values(t);
                                this.tween = new B({
                                    current: this.current,
                                    values: e,
                                    duration: this.duration,
                                    delay: this.delay,
                                    ease: this.ease,
                                    update: this.update,
                                    context: this
                                })
                            }, t.update = function() {
                                G(this.el, this.name, this.style(this.current))
                            }, t.style = function(t) {
                                var e, n = "";
                                for (e in t) n += e + "(" + t[e] + ") ";
                                return n
                            }, t.values = function(t) {
                                var e, i = {};
                                return n.call(this, t, function(t, n, r) {
                                    i[t] = n, void 0 === this.current[t] && (e = 0, ~t.indexOf("scale") && (e = 1), this.current[t] = this.convert(e, r))
                                }), i
                            }
                        }),
                        N = l(function(e) {
                            function n() {
                                var t, e, i, r = u.length;
                                if (r)
                                    for (j(n), e = F(), t = r; t--;)(i = u[t]) && i.render(e)
                            }
                            var i = {
                                ease: f.ease[1],
                                from: 0,
                                to: 1
                            };
                            e.init = function(t) {
                                this.duration = t.duration || 0, this.delay = t.delay || 0;
                                var e = t.ease || i.ease;
                                f[e] && (e = f[e][1]), "function" != typeof e && (e = i.ease), this.ease = e, this.update = t.update || a, this.complete = t.complete || a, this.context = t.context || this, this.name = t.name;
                                var n = t.from,
                                    r = t.to;
                                void 0 === n && (n = i.from), void 0 === r && (r = i.to), this.unit = t.unit || "", "number" == typeof n && "number" == typeof r ? (this.begin = n, this.change = r - n) : this.format(r, n), this.value = this.begin + this.unit, this.start = F(), !1 !== t.autoplay && this.play()
                            }, e.play = function() {
                                var t;
                                this.active || (this.start || (this.start = F()), this.active = !0, t = this, 1 === u.push(t) && j(n))
                            }, e.stop = function() {
                                var e, n, i;
                                this.active && (this.active = !1, e = this, (i = t.inArray(e, u)) >= 0 && (n = u.slice(i + 1), u.length = i, n.length && (u = u.concat(n))))
                            }, e.render = function(t) {
                                var e, n = t - this.start;
                                if (this.delay) {
                                    if (n <= this.delay) return;
                                    n -= this.delay
                                }
                                if (n < this.duration) {
                                    var i = this.ease(n, 0, 1, this.duration);
                                    return e = this.startRGB ? function(t, e, n) {
                                        return o(t[0] + n * (e[0] - t[0]), t[1] + n * (e[1] - t[1]), t[2] + n * (e[2] - t[2]))
                                    }(this.startRGB, this.endRGB, i) : function(t) {
                                        return Math.round(t * c) / c
                                    }(this.begin + i * this.change), this.value = e + this.unit, void this.update.call(this.context, this.value)
                                }
                                e = this.endHex || this.begin + this.change, this.value = e + this.unit, this.update.call(this.context, this.value), this.complete.call(this.context), this.destroy()
                            }, e.format = function(t, e) {
                                if (e += "", "#" == (t += "").charAt(0)) return this.startRGB = r(e), this.endRGB = r(t), this.endHex = t, this.begin = 0, void(this.change = 1);
                                if (!this.unit) {
                                    var n = e.replace(m, "");
                                    n !== t.replace(m, "") && s("tween", e, t), this.unit = n
                                }
                                e = parseFloat(e), t = parseFloat(t), this.begin = this.value = e, this.change = t - e
                            }, e.destroy = function() {
                                this.stop(), this.context = null, this.ease = this.update = this.complete = a
                            };
                            var u = [],
                                c = 1e3
                        }),
                        H = l(N, function(t) {
                            t.init = function(t) {
                                this.duration = t.duration || 0, this.complete = t.complete || a, this.context = t.context, this.play()
                            }, t.render = function(t) {
                                t - this.start < this.duration || (this.complete.call(this.context), this.destroy())
                            }
                        }),
                        B = l(N, function(t, e) {
                            t.init = function(t) {
                                var e, n;
                                for (e in this.context = t.context, this.update = t.update, this.tweens = [], this.current = t.current, t.values) n = t.values[e], this.current[e] !== n && this.tweens.push(new N({
                                    name: e,
                                    from: this.current[e],
                                    to: n,
                                    duration: t.duration,
                                    delay: t.delay,
                                    ease: t.ease,
                                    autoplay: !1
                                }));
                                this.play()
                            }, t.render = function(t) {
                                var e, n, i = !1;
                                for (e = this.tweens.length; e--;)(n = this.tweens[e]).context && (n.render(t), this.current[n.name] = n.value, i = !0);
                                return i ? void(this.update && this.update.call(this.context)) : this.destroy()
                            }, t.destroy = function() {
                                if (e.destroy.call(this), this.tweens) {
                                    var t;
                                    for (t = this.tweens.length; t--;) this.tweens[t].destroy();
                                    this.tweens = null, this.current = null
                                }
                            }
                        }),
                        X = e.config = {
                            debug: !1,
                            defaultUnit: "px",
                            defaultAngle: "deg",
                            keepInherited: !1,
                            hideBackface: !1,
                            perspective: "",
                            fallback: !C.transition,
                            agentTests: []
                        };
                    e.fallback = function(t) {
                        if (!C.transition) return X.fallback = !0;
                        X.agentTests.push("(" + t + ")");
                        var e = new RegExp(X.agentTests.join("|"), "i");
                        X.fallback = e.test(navigator.userAgent)
                    }, e.fallback("6.0.[2-5] Safari"), e.tween = function(t) {
                        return new N(t)
                    }, e.delay = function(t, e, n) {
                        return new H({
                            complete: e,
                            duration: t,
                            context: n
                        })
                    }, t.fn.tram = function(t) {
                        return e.call(null, this, t)
                    };
                    var G = t.style,
                        U = t.css,
                        Z = {
                            transform: C.transform && C.transform.css
                        },
                        Y = {
                            color: [$, b],
                            background: [$, b, "background-color"],
                            "outline-color": [$, b],
                            "border-color": [$, b],
                            "border-top-color": [$, b],
                            "border-right-color": [$, b],
                            "border-bottom-color": [$, b],
                            "border-left-color": [$, b],
                            "border-width": [q, y],
                            "border-top-width": [q, y],
                            "border-right-width": [q, y],
                            "border-bottom-width": [q, y],
                            "border-left-width": [q, y],
                            "border-spacing": [q, y],
                            "letter-spacing": [q, y],
                            margin: [q, y],
                            "margin-top": [q, y],
                            "margin-right": [q, y],
                            "margin-bottom": [q, y],
                            "margin-left": [q, y],
                            padding: [q, y],
                            "padding-top": [q, y],
                            "padding-right": [q, y],
                            "padding-bottom": [q, y],
                            "padding-left": [q, y],
                            "outline-width": [q, y],
                            opacity: [q, g],
                            top: [q, x],
                            right: [q, x],
                            bottom: [q, x],
                            left: [q, x],
                            "font-size": [q, x],
                            "text-indent": [q, x],
                            "word-spacing": [q, x],
                            width: [q, x],
                            "min-width": [q, x],
                            "max-width": [q, x],
                            height: [q, x],
                            "min-height": [q, x],
                            "max-height": [q, x],
                            "line-height": [q, k],
                            "scroll-top": [D, g, "scrollTop"],
                            "scroll-left": [D, g, "scrollLeft"]
                        },
                        K = {};
                    C.transform && (Y.transform = [P], K = {
                        x: [x, "translateX"],
                        y: [x, "translateY"],
                        rotate: [_],
                        rotateX: [_],
                        rotateY: [_],
                        scale: [g],
                        scaleX: [g],
                        scaleY: [g],
                        skew: [_],
                        skewX: [_],
                        skewY: [_]
                    }), C.transform && C.backface && (K.z = [x, "translateZ"], K.rotateZ = [_], K.scaleZ = [g], K.perspective = [y]);
                    var Q = /ms/,
                        V = /s|\./;
                    return t.tram = e
                }(window.jQuery)
            }, function(t, e, n) {
                n(3), n(7), n(8), n(9), n(10), t.exports = n(11)
            },

            function(t, e, n) {
                "use strict";
                var i = n(0);
                i.define(
                    "brand",
                    t.exports = function(t) {
                        var e, n = {},
                            r = document,
                            o = t("html"),
                            a = t("body"),
                            s = ".w-webflow-badge",
                            u = window.location,
                            c = /PhantomJS/i.test(navigator.userAgent),
                            l = "fullscreenchange webkitfullscreenchange mozfullscreenchange msfullscreenchange";

                        function f() {
                            /*
                                                                var n = r.fullScreen || r.mozFullScreen || r.webkitIsFullScreen || r.msFullscreenElement || Boolean(r.webkitFullscreenElement);
                                                                t(e).attr(
                                                                    "style",
                                                                    n ? "display: none !important;" : ""
                                                                )
                                                            */
                        }

                        function h() {}
                        return n.ready = function() {},
                            n
                    }
                )
            },

            function(t, e, n) {
                "use strict";
                var i = window.$,
                    r = n(1) && i.tram;
                /*!
                 * Webflow._ (aka) Underscore.js 1.6.0 (custom build)
                 * _.each
                 * _.map
                 * _.find
                 * _.filter
                 * _.any
                 * _.contains
                 * _.delay
                 * _.defer
                 * _.throttle (webflow)
                 * _.debounce
                 * _.keys
                 * _.has
                 * _.now
                 *
                 * http://underscorejs.org
                 * (c) 2009-2013 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
                 * Underscore may be freely distributed under the MIT license.
                 * @license MIT
                 */
                t.exports = function() {
                    var t = {
                            VERSION: "1.6.0-Webflow"
                        },
                        e = {},
                        n = Array.prototype,
                        i = Object.prototype,
                        o = Function.prototype,
                        a = (n.push, n.slice),
                        s = (n.concat, i.toString, i.hasOwnProperty),
                        u = n.forEach,
                        c = n.map,
                        l = (n.reduce, n.reduceRight, n.filter),
                        f = (n.every, n.some),
                        h = n.indexOf,
                        d = (n.lastIndexOf, Array.isArray, Object.keys),
                        p = (o.bind, t.each = t.forEach = function(n, i, r) {
                            if (null == n) return n;
                            if (u && n.forEach === u) n.forEach(i, r);
                            else if (n.length === +n.length) {
                                for (var o = 0, a = n.length; o < a; o++)
                                    if (i.call(r, n[o], o, n) === e) return
                            } else {
                                var s = t.keys(n);
                                for (o = 0, a = s.length; o < a; o++)
                                    if (i.call(r, n[s[o]], s[o], n) === e) return
                            }
                            return n
                        });
                    t.map = t.collect = function(t, e, n) {
                        var i = [];
                        return null == t ? i : c && t.map === c ? t.map(e, n) : (p(t, function(t, r, o) {
                            i.push(e.call(n, t, r, o))
                        }), i)
                    }, t.find = t.detect = function(t, e, n) {
                        var i;
                        return v(t, function(t, r, o) {
                            if (e.call(n, t, r, o)) return i = t, !0
                        }), i
                    }, t.filter = t.select = function(t, e, n) {
                        var i = [];
                        return null == t ? i : l && t.filter === l ? t.filter(e, n) : (p(t, function(t, r, o) {
                            e.call(n, t, r, o) && i.push(t)
                        }), i)
                    };
                    var v = t.some = t.any = function(n, i, r) {
                        i || (i = t.identity);
                        var o = !1;
                        return null == n ? o : f && n.some === f ? n.some(i, r) : (p(n, function(t, n, a) {
                            if (o || (o = i.call(r, t, n, a))) return e
                        }), !!o)
                    };
                    t.contains = t.include = function(t, e) {
                        return null != t && (h && t.indexOf === h ? -1 != t.indexOf(e) : v(t, function(t) {
                            return t === e
                        }))
                    }, t.delay = function(t, e) {
                        var n = a.call(arguments, 2);
                        return setTimeout(function() {
                            return t.apply(null, n)
                        }, e)
                    }, t.defer = function(e) {
                        return t.delay.apply(t, [e, 1].concat(a.call(arguments, 1)))
                    }, t.throttle = function(t) {
                        var e, n, i;
                        return function() {
                            e || (e = !0, n = arguments, i = this, r.frame(function() {
                                e = !1, t.apply(i, n)
                            }))
                        }
                    }, t.debounce = function(e, n, i) {
                        var r, o, a, s, u, c = function c() {
                            var l = t.now() - s;
                            l < n ? r = setTimeout(c, n - l) : (r = null, i || (u = e.apply(a, o), a = o = null))
                        };
                        return function() {
                            a = this, o = arguments, s = t.now();
                            var l = i && !r;
                            return r || (r = setTimeout(c, n)), l && (u = e.apply(a, o), a = o = null), u
                        }
                    }, t.defaults = function(e) {
                        if (!t.isObject(e)) return e;
                        for (var n = 1, i = arguments.length; n < i; n++) {
                            var r = arguments[n];
                            for (var o in r) void 0 === e[o] && (e[o] = r[o])
                        }
                        return e
                    }, t.keys = function(e) {
                        if (!t.isObject(e)) return [];
                        if (d) return d(e);
                        var n = [];
                        for (var i in e) t.has(e, i) && n.push(i);
                        return n
                    }, t.has = function(t, e) {
                        return s.call(t, e)
                    }, t.isObject = function(t) {
                        return t === Object(t)
                    }, t.now = Date.now || function() {
                        return (new Date).getTime()
                    }, t.templateSettings = {
                        evaluate: /<%([\s\S]+?)%>/g,
                        interpolate: /<%=([\s\S]+?)%>/g,
                        escape: /<%-([\s\S]+?)%>/g
                    };
                    var m = /(.)^/,
                        w = {
                            "'": "'",
                            "\\": "\\",
                            "\r": "r",
                            "\n": "n",
                            "\u2028": "u2028",
                            "\u2029": "u2029"
                        },
                        g = /\\|'|\r|\n|\u2028|\u2029/g,
                        b = function(t) {
                            return "\\" + w[t]
                        };
                    return t.template = function(e, n, i) {
                        !n && i && (n = i), n = t.defaults({}, n, t.templateSettings);
                        var r = RegExp([(n.escape || m).source, (n.interpolate || m).source, (n.evaluate || m).source].join("|") + "|$", "g"),
                            o = 0,
                            a = "__p+='";
                        e.replace(r, function(t, n, i, r, s) {
                            return a += e.slice(o, s).replace(g, b), o = s + t.length, n ? a += "'+\n((__t=(" + n + "))==null?'':_.escape(__t))+\n'" : i ? a += "'+\n((__t=(" + i + "))==null?'':__t)+\n'" : r && (a += "';\n" + r + "\n__p+='"), t
                        }), a += "';\n", n.variable || (a = "with(obj||{}){\n" + a + "}\n"), a = "var __t,__p='',__j=Array.prototype.join,print=function(){__p+=__j.call(arguments,'');};\n" + a + "return __p;\n";
                        try {
                            var s = new Function(n.variable || "obj", "_", a)
                        } catch (t) {
                            throw t.source = a, t
                        }
                        var u = function(e) {
                                return s.call(this, e, t)
                            },
                            c = n.variable || "obj";
                        return u.source = "function(" + c + "){\n" + a + "}", u
                    }, t
                }()
            },
            function(t, e) {
                t.exports = function(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    }
                }
            },
            function(t, e) {
                function n(t) {
                    return (n = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(t) {
                        return typeof t
                    } : function(t) {
                        return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t
                    })(t)
                }

                function i(e) {
                    return "function" == typeof Symbol && "symbol" === n(Symbol.iterator) ? t.exports = i = function(t) {
                        return n(t)
                    } : t.exports = i = function(t) {
                        return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : n(t)
                    }, i(e)
                }
                t.exports = i
            },
            function(t, e, n) {
                "use strict";
                var i = n(0);
                i.define("edit", t.exports = function(t, e, n) {
                    if (n = n || {}, (i.env("test") || i.env("frame")) && !n.fixture && ! function() {
                            try {
                                return window.top.__Cypress__
                            } catch (t) {
                                return !1
                            }
                        }()) return {
                        exit: 1
                    };
                    var r, o = t(window),
                        a = t(document.documentElement),
                        s = document.location,
                        u = "hashchange",
                        c = n.load || function() {
                            r = !0, window.WebflowEditor = !0, o.off(u, f),
                                function(t) {
                                    var e = window.document.createElement("iframe");
                                    e.src = "https://webflow.com/site/third-party-cookie-check.html", e.style.display = "none", e.sandbox = "allow-scripts allow-same-origin";
                                    var n = function n(i) {
                                        "WF_third_party_cookies_unsupported" === i.data ? (w(e, n), t(!1)) : "WF_third_party_cookies_supported" === i.data && (w(e, n), t(!0))
                                    };
                                    e.onerror = function() {
                                        w(e, n), t(!1)
                                    }, window.addEventListener("message", n, !1), window.document.body.appendChild(e)
                                }(function(e) {
                                    t.ajax({
                                        url: m("https://editor-api.webflow.com/api/editor/view"),
                                        data: {
                                            siteId: a.attr("data-wf-site")
                                        },
                                        xhrFields: {
                                            withCredentials: !0
                                        },
                                        dataType: "json",
                                        crossDomain: !0,
                                        success: h(e)
                                    })
                                })
                        },
                        l = !1;
                    try {
                        l = localStorage && localStorage.getItem && localStorage.getItem("WebflowEditor")
                    } catch (t) {}

                    function f() {
                        r || /\?edit/.test(s.hash) && c()
                    }

                    function h(t) {
                        return function(e) {
                            e ? (e.thirdPartyCookiesSupported = t, d(v(e.bugReporterScriptPath), function() {
                                d(v(e.scriptPath), function() {
                                    window.WebflowEditor(e)
                                })
                            })) : console.error("Could not load editor data")
                        }
                    }

                    function d(e, n) {
                        t.ajax({
                            type: "GET",
                            url: e,
                            dataType: "script",
                            cache: !0
                        }).then(n, p)
                    }

                    function p(t, e, n) {
                        throw console.error("Could not load editor script: " + e), n
                    }

                    function v(t) {
                        return t.indexOf("//") >= 0 ? t : m("https://editor-api.webflow.com" + t)
                    }

                    function m(t) {
                        return t.replace(/([^:])\/\//g, "$1/")
                    }

                    function w(t, e) {
                        window.removeEventListener("message", e, !1), t.remove()
                    }
                    return l ? c() : s.search ? (/[?&](edit)(?:[=&?]|$)/.test(s.search) || /\?edit$/.test(s.href)) && c() : o.on(u, f).triggerHandler(u), {}
                })
            },
            function(t, e, n) {
                "use strict";
                var i = n(0);
                i.define("links", t.exports = function(t, e) {
                    var n, r, o, a = {},
                        s = t(window),
                        u = i.env(),
                        c = window.location,
                        l = document.createElement("a"),
                        f = "w--current",
                        h = /index\.(html|php)$/,
                        d = /\/$/;

                    function p(e) {
                        var i = n && e.getAttribute("href-disabled") || e.getAttribute("href");
                        if (l.href = i, !(i.indexOf(":") >= 0)) {
                            var a = t(e);
                            if (l.hash.length > 1 && l.host + l.pathname === c.host + c.pathname) {
                                if (!/^#[a-zA-Z0-9\-\_]+$/.test(l.hash)) return;
                                var s = t(l.hash);
                                s.length && r.push({
                                    link: a,
                                    sec: s,
                                    active: !1
                                })
                            } else if ("#" !== i && "" !== i) {
                                var u = l.href === c.href || i === o || h.test(i) && d.test(o);
                                m(a, f, u)
                            }
                        }
                    }

                    function v() {
                        var t = s.scrollTop(),
                            n = s.height();
                        e.each(r, function(e) {
                            var i = e.link,
                                r = e.sec,
                                o = r.offset().top,
                                a = r.outerHeight(),
                                s = .5 * n,
                                u = r.is(":visible") && o + a - s >= t && o + s <= t + n;
                            e.active !== u && (e.active = u, m(i, f, u))
                        })
                    }

                    function m(t, e, n) {
                        var i = t.hasClass(e);
                        n && i || (n || i) && (n ? t.addClass(e) : t.removeClass(e))
                    }
                    return a.ready = a.design = a.preview = function() {
                        n = u && i.env("design"), o = i.env("slug") || c.pathname || "", i.scroll.off(v), r = [];
                        for (var t = document.links, e = 0; e < t.length; ++e) p(t[e]);
                        r.length && (i.scroll.on(v), v())
                    }, a
                })
            },
            function(t, e, n) {
                "use strict";
                var i = n(0);
                i.define("scroll", t.exports = function(t) {
                    var e, n = {
                            WF_CHANGE: "change.wf-change",
                            WF_CLICK_EMPTY: "click.wf-empty-link",
                            WF_CLICK_SCROLL: "click.wf-scroll"
                        },
                        r = t(document),
                        o = window,
                        a = o.location,
                        s = function() {
                            try {
                                return Boolean(o.frameElement)
                            } catch (t) {
                                return !0
                            }
                        }() ? null : o.history,
                        u = /^[a-zA-Z0-9][\w:.-]*$/,
                        c = 'a[href="#"]',
                        l = 'a[href*="#"]:not(.w-tab-link):not(' + c + ")";

                    function f(n) {
                        if (!(i.env("design") || window.$.mobile && t(n.currentTarget).hasClass("ui-link"))) {
                            var r = this.href.split("#"),
                                c = r[0] === e ? r[1] : null;
                            c && function(e, n) {
                                if (!u.test(e)) return;
                                var r = t("#" + e);
                                if (!r.length) return;
                                n && (n.preventDefault(), n.stopPropagation());
                                if (a.hash !== e && s && s.pushState && (!i.env.chrome || "file:" !== a.protocol)) {
                                    var c = s.state && s.state.hash;
                                    c !== e && s.pushState({
                                        hash: e
                                    }, "", "#" + e)
                                }
                                var l = i.env("editor") ? ".w-editor-body" : "body",
                                    f = t("header, " + l + " > .header, " + l + " > .w-nav:not([data-no-scroll])"),
                                    h = "fixed" === f.css("position") ? f.outerHeight() : 0;
                                o.setTimeout(function() {
                                    ! function(e, n) {
                                        var i = t(o).scrollTop(),
                                            r = e.offset().top - n;
                                        if ("mid" === e.data("scroll")) {
                                            var a = t(o).height() - n,
                                                s = e.outerHeight();
                                            s < a && (r -= Math.round((a - s) / 2))
                                        }
                                        var u = 1;
                                        t("body").add(e).each(function() {
                                            var e = parseFloat(t(this).attr("data-scroll-time"), 10);
                                            !isNaN(e) && (0 === e || e > 0) && (u = e)
                                        }), Date.now || (Date.now = function() {
                                            return (new Date).getTime()
                                        });
                                        var c = Date.now(),
                                            l = o.requestAnimationFrame || o.mozRequestAnimationFrame || o.webkitRequestAnimationFrame || function(t) {
                                                o.setTimeout(t, 15)
                                            },
                                            f = (472.143 * Math.log(Math.abs(i - r) + 125) - 2e3) * u;
                                        ! function t() {
                                            var e = Date.now() - c;
                                            o.scroll(0, function(t, e, n, i) {
                                                if (n > i) return e;
                                                return t + (e - t) * (r = n / i, r < .5 ? 4 * r * r * r : (r - 1) * (2 * r - 2) * (2 * r - 2) + 1);
                                                var r
                                            }(i, r, e, f)), e <= f && l(t)
                                        }()
                                    }(r, h)
                                }, n ? 0 : 300)
                            }(c, n)
                        }
                    }
                    return {
                        ready: function() {
                            n.WF_CHANGE;
                            var t = n.WF_CLICK_EMPTY,
                                i = n.WF_CLICK_SCROLL;
                            e = a.href.split("#")[0], r.on(i, l, f), r.on(t, c, function(t) {
                                t.preventDefault()
                            })
                        }
                    }
                })
            },
            function(t, e, n) {
                "use strict";
                n(0).define("touch", t.exports = function(t) {
                    var e = {},
                        n = window.getSelection;

                    function i(e) {
                        var i, r, o = !1,
                            a = !1,
                            s = Math.min(Math.round(.04 * window.innerWidth), 40);

                        function u(t) {
                            var e = t.touches;
                            e && e.length > 1 || (o = !0, e ? (a = !0, i = e[0].clientX) : i = t.clientX, r = i)
                        }

                        function c(e) {
                            if (o) {
                                if (a && "mousemove" === e.type) return e.preventDefault(), void e.stopPropagation();
                                var i = e.touches,
                                    u = i ? i[0].clientX : e.clientX,
                                    c = u - r;
                                r = u, Math.abs(c) > s && n && "" === String(n()) && (! function(e, n, i) {
                                    var r = t.Event(e, {
                                        originalEvent: n
                                    });
                                    t(n.target).trigger(r, i)
                                }("swipe", e, {
                                    direction: c > 0 ? "right" : "left"
                                }), f())
                            }
                        }

                        function l(t) {
                            if (o) return o = !1, a && "mouseup" === t.type ? (t.preventDefault(), t.stopPropagation(), void(a = !1)) : void 0
                        }

                        function f() {
                            o = !1
                        }
                        e.addEventListener("touchstart", u, !1), e.addEventListener("touchmove", c, !1), e.addEventListener("touchend", l, !1), e.addEventListener("touchcancel", f, !1), e.addEventListener("mousedown", u, !1), e.addEventListener("mousemove", c, !1), e.addEventListener("mouseup", l, !1), e.addEventListener("mouseout", f, !1), this.destroy = function() {
                            e.removeEventListener("touchstart", u, !1), e.removeEventListener("touchmove", c, !1), e.removeEventListener("touchend", l, !1), e.removeEventListener("touchcancel", f, !1), e.removeEventListener("mousedown", u, !1), e.removeEventListener("mousemove", c, !1), e.removeEventListener("mouseup", l, !1), e.removeEventListener("mouseout", f, !1), e = null
                        }
                    }
                    return t.event.special.tap = {
                        bindType: "click",
                        delegateType: "click"
                    }, e.init = function(e) {
                        return (e = "string" == typeof e ? t(e).get(0) : e) ? new i(e) : null
                    }, e.instance = e.init(document), e
                })
            },
            function(t, e, n) {
                "use strict";
                var i = n(0),
                    r = n(12),
                    o = {
                        ARROW_LEFT: 37,
                        ARROW_UP: 38,
                        ARROW_RIGHT: 39,
                        ARROW_DOWN: 40,
                        SPACE: 32,
                        ENTER: 13,
                        HOME: 36,
                        END: 35
                    },
                    a = 'a[href], area[href], [role="button"], input, select, textarea, button, iframe, object, embed, *[tabindex], *[contenteditable]';
                i.define("slider", t.exports = function(t, e) {
                    var n, s, u, c, l = {},
                        f = t.tram,
                        h = t(document),
                        d = i.env(),
                        p = ".w-slider",
                        v = '<div class="w-slider-dot" data-wf-ignore />',
                        m = '<div aria-live="off" aria-atomic="true" class="w-slider-aria-label" data-wf-ignore />',
                        w = r.triggers;

                    function g() {
                        (n = h.find(p)).length && (n.each(x), c = null, u || (b(), i.resize.on(y), i.redraw.on(l.redraw)))
                    }

                    function b() {
                        i.resize.off(y), i.redraw.off(l.redraw)
                    }

                    function y() {
                        n.filter(":visible").each(L)
                    }

                    function x(e, n) {
                        var i = t(n),
                            r = t.data(n, p);
                        r || (r = t.data(n, p, {
                            index: 0,
                            depth: 1,
                            hasFocus: {
                                keyboard: !1,
                                mouse: !1
                            },
                            el: i,
                            config: {}
                        })), r.mask = i.children(".w-slider-mask"), r.left = i.children(".w-slider-arrow-left"), r.right = i.children(".w-slider-arrow-right"), r.nav = i.children(".w-slider-nav"), r.slides = r.mask.children(".w-slide"), r.slides.each(w.reset), c && (r.maskWidth = 0), void 0 === i.attr("role") && i.attr("role", "region"), void 0 === i.attr("aria-label") && i.attr("aria-label", "carousel");
                        var o = r.mask.attr("id");
                        if (o || (o = "w-slider-mask-" + e, r.mask.attr("id", o)), r.ariaLiveLabel = t(m).appendTo(r.mask), r.left.attr("role", "button"), r.left.attr("tabindex", "0"), r.left.attr("aria-controls", o), void 0 === r.left.attr("aria-label") && r.left.attr("aria-label", "previous slide"), r.right.attr("role", "button"), r.right.attr("tabindex", "0"), r.right.attr("aria-controls", o), void 0 === r.right.attr("aria-label") && r.right.attr("aria-label", "next slide"), !f.support.transform) return r.left.hide(), r.right.hide(), r.nav.hide(), void(u = !0);
                        r.el.off(p), r.left.off(p), r.right.off(p), r.nav.off(p), _(r), s ? (r.el.on("setting" + p, R(r)), A(r), r.hasTimer = !1) : (r.el.on("swipe" + p, R(r)), r.left.on("click" + p, T(r)), r.right.on("click" + p, S(r)), r.left.on("keydown" + p, O(r, T)), r.right.on("keydown" + p, O(r, S)), r.nav.on("keydown" + p, "> div", R(r)), r.config.autoplay && !r.hasTimer && (r.hasTimer = !0, r.timerCount = 1, z(r)), r.el.on("mouseenter" + p, E(r, !0, "mouse")), r.el.on("focusin" + p, E(r, !0, "keyboard")), r.el.on("mouseleave" + p, E(r, !1, "mouse")), r.el.on("focusout" + p, E(r, !1, "keyboard"))), r.nav.on("click" + p, "> div", R(r)), d || r.mask.contents().filter(function() {
                            return 3 === this.nodeType
                        }).remove();
                        var a = i.filter(":hidden");
                        a.show();
                        var l = i.parents(":hidden");
                        l.show(), L(e, n), a.css("display", ""), l.css("display", "")
                    }

                    function _(t) {
                        var e = {
                            crossOver: 0
                        };
                        e.animation = t.el.attr("data-animation") || "slide", "outin" === e.animation && (e.animation = "cross", e.crossOver = .5), e.easing = t.el.attr("data-easing") || "ease";
                        var n = t.el.attr("data-duration");
                        if (e.duration = null != n ? parseInt(n, 10) : 500, k(t.el.attr("data-infinite")) && (e.infinite = !0), k(t.el.attr("data-disable-swipe")) && (e.disableSwipe = !0), k(t.el.attr("data-hide-arrows")) ? e.hideArrows = !0 : t.config.hideArrows && (t.left.show(), t.right.show()), k(t.el.attr("data-autoplay"))) {
                            e.autoplay = !0, e.delay = parseInt(t.el.attr("data-delay"), 10) || 2e3, e.timerMax = parseInt(t.el.attr("data-autoplay-limit"), 10);
                            var i = "mousedown" + p + " touchstart" + p;
                            s || t.el.off(i).one(i, function() {
                                A(t)
                            })
                        }
                        var r = t.right.width();
                        e.edge = r ? r + 40 : 100, t.config = e
                    }

                    function k(t) {
                        return "1" === t || "true" === t
                    }

                    function E(e, n, i) {
                        return function(r) {
                            if (n) e.hasFocus[i] = n;
                            else {
                                if (t.contains(e.el.get(0), r.relatedTarget)) return;
                                if (e.hasFocus[i] = n, e.hasFocus.mouse && "keyboard" === i || e.hasFocus.keyboard && "mouse" === i) return
                            }
                            n ? (e.ariaLiveLabel.attr("aria-live", "polite"), e.hasTimer && A(e)) : (e.ariaLiveLabel.attr("aria-live", "off"), e.hasTimer && z(e))
                        }
                    }

                    function O(t, e) {
                        return function(n) {
                            switch (n.keyCode) {
                                case o.SPACE:
                                case o.ENTER:
                                    return e(t)(), n.preventDefault(), n.stopPropagation()
                            }
                        }
                    }

                    function T(t) {
                        return function() {
                            M(t, {
                                index: t.index - 1,
                                vector: -1
                            })
                        }
                    }

                    function S(t) {
                        return function() {
                            M(t, {
                                index: t.index + 1,
                                vector: 1
                            })
                        }
                    }

                    function z(t) {
                        A(t);
                        var e = t.config,
                            n = e.timerMax;
                        n && t.timerCount++ > n || (t.timerId = window.setTimeout(function() {
                            null == t.timerId || s || (S(t)(), z(t))
                        }, e.delay))
                    }

                    function A(t) {
                        window.clearTimeout(t.timerId), t.timerId = null
                    }

                    function R(n) {
                        return function(r, a) {
                            a = a || {};
                            var u = n.config;
                            if (s && "setting" === r.type) {
                                if ("prev" === a.select) return T(n)();
                                if ("next" === a.select) return S(n)();
                                if (_(n), j(n), null == a.select) return;
                                ! function(n, i) {
                                    var r = null;
                                    i === n.slides.length && (g(), j(n)), e.each(n.anchors, function(e, n) {
                                        t(e.els).each(function(e, o) {
                                            t(o).index() === i && (r = n)
                                        })
                                    }), null != r && M(n, {
                                        index: r,
                                        immediate: !0
                                    })
                                }(n, a.select)
                            } else {
                                if ("swipe" === r.type) {
                                    if (u.disableSwipe) return;
                                    if (i.env("editor")) return;
                                    return "left" === a.direction ? S(n)() : "right" === a.direction ? T(n)() : void 0
                                }
                                if (n.nav.has(r.target).length) {
                                    var c = t(r.target).index();
                                    if ("click" === r.type && M(n, {
                                            index: c
                                        }), "keydown" === r.type) switch (r.keyCode) {
                                        case o.ENTER:
                                        case o.SPACE:
                                            M(n, {
                                                index: c
                                            }), r.preventDefault();
                                            break;
                                        case o.ARROW_LEFT:
                                        case o.ARROW_UP:
                                            C(n.nav, Math.max(c - 1, 0)), r.preventDefault();
                                            break;
                                        case o.ARROW_RIGHT:
                                        case o.ARROW_DOWN:
                                            C(n.nav, Math.min(c + 1, n.pages)), r.preventDefault();
                                            break;
                                        case o.HOME:
                                            C(n.nav, 0), r.preventDefault();
                                            break;
                                        case o.END:
                                            C(n.nav, n.pages), r.preventDefault();
                                            break;
                                        default:
                                            return
                                    }
                                }
                            }
                        }
                    }

                    function C(t, e) {
                        var n = t.children().eq(e).focus();
                        t.children().not(n)
                    }

                    function M(e, n) {
                        n = n || {};
                        var i = e.config,
                            r = e.anchors;
                        e.previous = e.index;
                        var o = n.index,
                            u = {};
                        o < 0 ? (o = r.length - 1, i.infinite && (u.x = -e.endX, u.from = 0, u.to = r[0].width)) : o >= r.length && (o = 0, i.infinite && (u.x = r[r.length - 1].width, u.from = -r[r.length - 1].x, u.to = u.from - u.x)), e.index = o;
                        var l = e.nav.children().eq(o).addClass("w-active").attr("aria-selected", "true").attr("tabindex", "0");
                        e.nav.children().not(l).removeClass("w-active").attr("aria-selected", "false").attr("tabindex", "-1"), i.hideArrows && (e.index === r.length - 1 ? e.right.hide() : e.right.show(), 0 === e.index ? e.left.hide() : e.left.show());
                        var h = e.offsetX || 0,
                            d = e.offsetX = -r[e.index].x,
                            p = {
                                x: d,
                                opacity: 1,
                                visibility: ""
                            },
                            v = t(r[e.index].els),
                            m = t(r[e.previous] && r[e.previous].els),
                            g = e.slides.not(v),
                            b = i.animation,
                            y = i.easing,
                            x = Math.round(i.duration),
                            _ = n.vector || (e.index > e.previous ? 1 : -1),
                            k = "opacity " + x + "ms " + y,
                            E = "transform " + x + "ms " + y;
                        if (v.find(a).removeAttr("tabindex"), v.removeAttr("aria-hidden"), v.find("*").removeAttr("aria-hidden"), g.find(a).attr("tabindex", "-1"), g.attr("aria-hidden", "true"), g.find("*").attr("aria-hidden", "true"), s || (v.each(w.intro), g.each(w.outro)), n.immediate && !c) return f(v).set(p), void S();
                        if (e.index !== e.previous) {
                            if (e.ariaLiveLabel.text("Slide ".concat(o + 1, " of ").concat(r.length, ".")), "cross" === b) {
                                var O = Math.round(x - x * i.crossOver),
                                    T = Math.round(x - O);
                                return k = "opacity " + O + "ms " + y, f(m).set({
                                    visibility: ""
                                }).add(k).start({
                                    opacity: 0
                                }), void f(v).set({
                                    visibility: "",
                                    x: d,
                                    opacity: 0,
                                    zIndex: e.depth++
                                }).add(k).wait(T).then({
                                    opacity: 1
                                }).then(S)
                            }
                            if ("fade" === b) return f(m).set({
                                visibility: ""
                            }).stop(), void f(v).set({
                                visibility: "",
                                x: d,
                                opacity: 0,
                                zIndex: e.depth++
                            }).add(k).start({
                                opacity: 1
                            }).then(S);
                            if ("over" === b) return p = {
                                x: e.endX
                            }, f(m).set({
                                visibility: ""
                            }).stop(), void f(v).set({
                                visibility: "",
                                zIndex: e.depth++,
                                x: d + r[e.index].width * _
                            }).add(E).start({
                                x: d
                            }).then(S);
                            i.infinite && u.x ? (f(e.slides.not(m)).set({
                                visibility: "",
                                x: u.x
                            }).add(E).start({
                                x: d
                            }), f(m).set({
                                visibility: "",
                                x: u.from
                            }).add(E).start({
                                x: u.to
                            }), e.shifted = m) : (i.infinite && e.shifted && (f(e.shifted).set({
                                visibility: "",
                                x: h
                            }), e.shifted = null), f(e.slides).set({
                                visibility: ""
                            }).add(E).start({
                                x: d
                            }))
                        }

                        function S() {
                            v = t(r[e.index].els), g = e.slides.not(v), "slide" !== b && (p.visibility = "hidden"), f(g).set(p)
                        }
                    }

                    function L(e, n) {
                        var i = t.data(n, p);
                        if (i) return function(t) {
                            var e = t.mask.width();
                            if (t.maskWidth !== e) return t.maskWidth = e, !0;
                            return !1
                        }(i) ? j(i) : void(s && function(e) {
                            var n = 0;
                            if (e.slides.each(function(e, i) {
                                    n += t(i).outerWidth(!0)
                                }), e.slidesWidth !== n) return e.slidesWidth = n, !0;
                            return !1
                        }(i) && j(i))
                    }

                    function j(e) {
                        var n = 1,
                            i = 0,
                            r = 0,
                            o = 0,
                            a = e.maskWidth,
                            u = a - e.config.edge;
                        u < 0 && (u = 0), e.anchors = [{
                            els: [],
                            x: 0,
                            width: 0
                        }], e.slides.each(function(s, c) {
                            r - i > u && (n++, i += a, e.anchors[n - 1] = {
                                els: [],
                                x: r,
                                width: 0
                            }), o = t(c).outerWidth(!0), r += o, e.anchors[n - 1].width += o, e.anchors[n - 1].els.push(c);
                            var l = s + 1 + " of " + e.slides.length;
                            t(c).attr("aria-label", l), t(c).attr("role", "group")
                        }), e.endX = r, s && (e.pages = null), e.nav.length && e.pages !== n && (e.pages = n, function(e) {
                            var n, i = [],
                                r = e.el.attr("data-nav-spacing");
                            r && (r = parseFloat(r) + "px");
                            for (var o = 0, a = e.pages; o < a; o++)(n = t(v)).attr("aria-label", "Show slide " + (o + 1) + " of " + a).attr("aria-selected", "false").attr("role", "button").attr("tabindex", "-1"), e.nav.hasClass("w-num") && n.text(o + 1), null != r && n.css({
                                "margin-left": r,
                                "margin-right": r
                            }), i.push(n);
                            e.nav.empty().append(i)
                        }(e));
                        var c = e.index;
                        c >= n && (c = n - 1), M(e, {
                            immediate: !0,
                            index: c
                        })
                    }
                    return l.ready = function() {
                        s = i.env("design"), g()
                    }, l.design = function() {
                        s = !0, g()
                    }, l.preview = function() {
                        s = !1, g()
                    }, l.redraw = function() {
                        c = !0, g()
                    }, l.destroy = b, l
                })
            },
            function(t, e, n) {
                "use strict";
                var i = n(13);

                function r(t, e) {
                    var n = document.createEvent("CustomEvent");
                    n.initCustomEvent(e, !0, !0, null), t.dispatchEvent(n)
                }
                var o = window.jQuery,
                    a = {},
                    s = {
                        reset: function(t, e) {
                            i.triggers.reset(t, e)
                        },
                        intro: function(t, e) {
                            i.triggers.intro(t, e), r(e, "COMPONENT_ACTIVE")
                        },
                        outro: function(t, e) {
                            i.triggers.outro(t, e), r(e, "COMPONENT_INACTIVE")
                        }
                    };
                a.triggers = {}, a.types = {
                    INTRO: "w-ix-intro.w-ix",
                    OUTRO: "w-ix-outro.w-ix"
                }, o.extend(a.triggers, s), t.exports = a
            },
            function(t, e, n) {
                "use strict";
                var i = window.jQuery,
                    r = {},
                    o = [],
                    a = {
                        reset: function(t, e) {
                            e.__wf_intro = null
                        },
                        intro: function(t, e) {
                            e.__wf_intro || (e.__wf_intro = !0, i(e).triggerHandler(r.types.INTRO))
                        },
                        outro: function(t, e) {
                            e.__wf_intro && (e.__wf_intro = null, i(e).triggerHandler(r.types.OUTRO))
                        }
                    };
                r.triggers = {}, r.types = {
                    INTRO: "w-ix-intro.w-ix",
                    OUTRO: "w-ix-outro.w-ix"
                }, r.init = function() {
                    for (var t = o.length, e = 0; e < t; e++) {
                        var n = o[e];
                        n[0](0, n[1])
                    }
                    o = [], i.extend(r.triggers, a)
                }, r.async = function() {
                    for (var t in a) {
                        var e = a[t];
                        a.hasOwnProperty(t) && (r.triggers[t] = function(t, n) {
                            o.push([e, n])
                        })
                    }
                }, r.async(), t.exports = r
            }
        ]);
    </script>
    <!--[if lte IE 9]><script src="//cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif]-->
</body>

</html>