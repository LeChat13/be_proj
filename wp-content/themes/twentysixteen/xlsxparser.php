<?php
	/*
		Template Name: xlsxparser
	*/
?>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<div>
<?
	global $user_ID, $user_identity, $user_email;
	get_currentuserinfo();
#	echo $user_email;
	if (!$user_ID):
?>
	<div>
		<?$temp = str_replace('Войти', 'Авторизация', $temp);?>
			<form name="loginform" id="autoriz" action="<?=get_settings('siteurl'); ?>/wp-login.php" method="post">
				<div class="left">
					<p>Логин:  <input type="text" name="log" placeholder="Логин" value="wpadm" id="login" /></p>
				</div>
				<div>
					<p>Пароль: <input type="password" name="pwd" placeholder="Пароль" value="" id="password" /></p>
					<input type="submit" name="submit" value="Войти" id="enter" /><br/>
				</div>
				<div>
					<input type="hidden" name="redirect_to" value="<?=$_SERVER['REQUEST_URI']; ?>" />
				</div>
			</form> 
	</div>
	<?elseif ($user_identity <> 'be_admin'):?>
	<div>
        <label style="color:red">Ваших прав доступа недостаточно для просмотра данной страницы</label>
	</div>
	<?else:?>
	<div>
        <a href="/" style="color:green">Назад на главную</a>
        <br/>
        <a href="/maileslist/" style="color:green">К списку адресов рассылок</a>
		<div>
			<form name="addtask" id="addtask" action="" method="post" enctype="multipart/form-data">
				<?require_once 'db_work.php';?>
				<div>
					<label for="script">Выберите скрипт:</label>
					<p/>
					<select name="script" style="width: 613px;" onchange="$('#description').text(value);">
						<?foreach ($xlsx_scripts_data as $dropd):?>
							<option id="<?=$dropd['id']?>" value="<?=$dropd['id']?>"><?=$dropd['script_info']?> - <?=$dropd['script_name']?></option>
						<?endforeach?>
					</select>
					<label id='scripterr' style="color:red; display:none">Необходимо обязательно выбрать скрипт</label>
				</div>
				<div>
				</div>
				<div>
					<label for="inputfile">Файл с исходными данными:</label>
					<p/>
					<input type="file" id="inputfile" name="inputfile" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel"><br/>
				</div>
				<div hidden>
				<label for="comment">Задача добавлена (комментарий):</label>
				<p/>
				<input type="text" style="width: 613px;" id="comment" name="comment" value="<?=$user_identity?>" required><br/>
				<label id='mailerr' style="color:red; display:none">Необходимо обязательно указать кем добавлена задача</label>
			</div>
			<div>
				</div>
				<div>
					<input type="submit" name="paste" value="Запустить" id="paste"><br/>
				</div>
			</form>
		</div>
		<?php
			ini_set('upload_max_filesize', '1M'); //ограничение в 1 мб
			if ($_SERVER['REQUEST_METHOD'] == "POST" )
			{
				$destiation_dir = 'noinfo';
				if ($_FILES['inputfile']['error'] == UPLOAD_ERR_OK && ($_FILES['inputfile']['type'] == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' || $_FILES['inputfile']['type'] == 'application/vnd.ms-excel'))
				{ //проверка на наличие ошибок
					$_FILES['inputfile']['name'] = preg_replace('/\s/',  '_', $_FILES['inputfile']['name']);
					$destiation_dir = '/home/h807240122/betp.website/files/' . $_FILES['inputfile']['name']; // директория для размещения файла
					if (!(move_uploaded_file($_FILES['inputfile']['tmp_name'], $destiation_dir)))
					{
						echo 'File not uploaded123';
					}
				}
				else
				{
					switch ($_FILES['inputfile']['error']) {
						case UPLOAD_ERR_FORM_SIZE:
						case UPLOAD_ERR_INI_SIZE:
							echo 'File Size exceed';
							brake;
						case UPLOAD_ERR_NO_FILE:
							echo 'FIle Not selected111';
							break;
						default:
							echo 'Something is wrong';
					}
				}
				if (($_POST['script']) && ($_FILES['inputfile']['name']))
				{
					mysqli_query ($link, "INSERT INTO `tasks_tmp` (`script_id`, `file`, `comment`, `started`) VALUES ('".$_POST['script']."','".$destiation_dir."','".$_POST['comment']."','false')") or die ('Упс13');
					printf ("ID новой записи: %d.\n", mysqli_insert_id($link));
				}
			}?>
		<div>
			<label>Таблица задач рассылки:</label>
		</div>
		<div>
			<table border="1" style="width: 100%;">
				<thead align="center">
					<tr>
						<td style="width: 3%;">
							№ пп
						</td>
						<td style="width: 20%;">
							Задача
						</td>
						<td style="width: 30%;">
							Исходный файл
						</td>
						<td style="width: 10%;">
							Задача добавлена (комментарий)
						</td>
						<td style="width: 10%;">
							Задача запущена
						</td>
<!--						<td style="width: 3%;">
							Всего строк (номеров, закупок, и т.д.)
						</td>
						<td style="width: 3%;">
							Прошел строк (номеров, закупок, и т.д.)
						</td>
-->					</tr>
				</thead>
				<tbody>
					<?
					$num = 0;
					foreach ($xlsx_tasks_data as $dropd):
						$num++;
#						$file = fopen("/home/h807240122/betp.website/logs/task_id_".$dropd['id'].".tmp","r");
#						$dropd['all_str'] = chop(fgets($file));
#						$dropd['run_str'] = chop(fgets($file));
						fclose ($file);
					?>
						<tr>
							<td align="center">
								<?=$num?>
							</td>
							<td align="left">
								<?=$dropd['script_info']?> - <?=$dropd['script_name']?>
							</td>
							<td align="left">
								<?=$dropd['file']?>
							</td>
							<td align="left">
								<?=$dropd['comment']?>
							</td>
							<td align="left">
								<?=$dropd['started_time']?>
							</td>
<!--							<td align="center">
								<?=$dropd['all_str']?>
							</td>
							<td align="center">
								<?=$dropd['run_str']?>
							</td>
-->						</tr>
					<?endforeach?>
				</tbody>
			</table>
		</div>
	</div>
	<?endif;?> 
</div>