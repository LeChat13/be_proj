<?php
	/*
		Template Name: New_Diagrams
	*/
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>betp-diagrams</title>
	<meta content="width=device-width, initial-scale=1" name="viewport">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.13.5/xlsx.full.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="/wp-content/themes/twentysixteen/js/spectrum.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.7.0"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.13.5/jszip.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/pptxgenjs@3.6.0/dist/pptxgen.bundle.js"></script>
	<script src="http://html2canvas.hertzen.com/dist/html2canvas.js"></script>

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
	<link href="<?=(get_template_directory_uri() . "/css/new_style.css")?>" rel="stylesheet">
	<link href="<?=(get_template_directory_uri() . "/css/custom.css")?>" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
	<link rel="stylesheet" href="/wp-content/themes/twentysixteen/css/jquery.message.css">
	<link rel="stylesheet" href="<?=(get_template_directory_uri() . "/css/spectrum.css")?>">

	<style>
		.connectedSortable{list-style-type:none; position: absolute; left: 1%; top: 1%; width:48%; min-height:98%; background-color:#ddd;}
		.connectedSortable_1{list-style-type:none; position: absolute; left: 51%; top: 1%; width:48%; min-height:98%; background-color:#ddd;}
		.connectedSortable li{margin:3px; height:10%; background-color:#fff; border:1px solid #888;}
		.connectedSortable_1 li{margin:3px; height:10%; background-color:#fff; border:1px solid #888;}
		.connectedOrderable{list-style-type:none; position: absolute; left: 1%; top: 1%; width:48%; min-height:98%; background-color:#ddd;}
		.connectedOrderable_1{list-style-type:none; position: absolute; left: 51%; top: 1%; width:48%; min-height:98%; background-color:#ddd;}
		.connectedOrderable li{margin:3px; height:10%; background-color:#fff; border:1px solid #888;}
		.connectedOrderable_1 li{margin:3px; height:10%; background-color:#fff; border:1px solid #888;}

		#sortable_ppt { list-style-type: none; margin: 0; padding: 0; width: 100%; cursor: pointer; }
		#sortable_ppt li { margin: 0 3px 2px 2px; padding: 0.4em; padding-left: 1.4em; font-size: 1.4em; height: 175px; }
		#sortable_ppt li span { position: absolute; margin-left: -1.2em; }
		
		.sort-placeholder {
		  width: 100%;
		  border: 1px dashed gray;
		  background-color: #fff;
		  margin: 8px 0;
		}
		.ordinal-position {
			text-align: right;
		}
		.pull-right {
		  float: right;
		}
		.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {
		  border: 1px solid #d3d3d3;
		  background: #e6e6e6 url("http://code.jquery.com/ui/1.11.2/themes/smoothness/images/ui-bg_glass_75_e6e6e6_1x400.png") 50% 75% repeat-x;
		  font-weight: normal;
		  color: #555555;
		}
		
	</style>
</head>

<body class="body">
	<div class="slider_page" id="slider_page">
		<div class="sp_left_button"></div>
		<div class="sp_slider_body" id="sp_slider_body">
			<div class="sp_sb_current_slide">
				<div class="sp_sb_cs_up_panel">
					<div class="menu_bar">
						<input type="button" class="left_arrow" name="left_arrow" id="left_arrow" value="«" style="">
						<input type="button" class="right_arrow" name="right_arrow" id="right_arrow" value="»" style="display: none;">
					</div>
					
					<div class="sp_sb_cs_up_status">
						<p>Слайды: 0 из 0</p>
					</div>
					<div class="sp_sb_cs_up_screener">
						<input type="button" class="uploader" name="uploader" id="uploader" value="Выгрузить все" disabled=""/>
						<input type="button" class="uploader" name="uploader_cur" id="uploader_cur" value="Выгрузить один" disabled=""/>
						<input type="button" class="uploader" name="add_slide" id="add_slide" value="Добавить">
						<input type="button" class="uploader" name="edit_slide" id="edit_slide" value="Редактировать" disabled=""/>
						<input type="button" class="uploader" name="del_slide" id="del_slide" value="Удалить" disabled=""/>
						<input type="button" class="uploader" name="copy_slide" id="copy_slide" value="Копировать" disabled=""/>
						<input type="button" class="uploader" name="load_tmpl" id="load_tmpl" value="Загрузить шаблон">
						<input type="button" class="uploader" name="save_tmpl" id="save_tmpl" value="Сохранить шаблон" disabled=""/>
						<input type="button" class="uploader" name="new_tmpl" id="new_tmpl" value="Добавить шаблон" disabled=""/>
						<input type="button" class="uploader" name="presets" id="presets" value="Задать предустановки">
						<a href="/coloreditor/" target="_blank">Colors Edit</a>
					</div>
				</div>
				<div class="sp_sb_cs_slide_body" id="sp_sb_cs_slide_body">
					<div class="sp_sb_cs_sb_up">
						<p style="display: none;">Отступ сверху, тут ничего не будет</p>
					</div>
					<div class="sp_sb_cs_sb_slide_title">
						<p style="display: none;">Заголовок слайда, вводится в окне добавления слайда (возможно с добавлением каких-то кусков "по умолчанию")</p>
					</div>
					<div class="sp_sb_cs_sb_user_text">
						<p style="display: none;">Поле из-под удаленных строк, оставленное под подписи добавляемые пользователем потом</p>
					</div>
					<div class="sp_sb_cs_sb_graph_block">
						<p style="display: none;">Блок с графиками</p>
					</div>
					<div class="sp_sb_cs_sb_data_block">
						<p style="display: none;">Блок подписей под графиками (числа и проценты)</p>
					</div>
					<div class="sp_sb_cs_sb_down">
						<p style="display: none;">Отступ снизу, тут ничего не будет</p>
					</div>
				</div>
			</div>
		</div>
		<div class="sp_right_button"></div>
	</div>
	<div class="slider_menu_fade">
		<div class="slider_menu">
			<div class="sm_file_value">
				<p>Выбранный файл: не выбрано</p>
			</div>
			<div class="sm_date_value">
			</div>
			<div class="sm_button_edit">
				<p>Изменить<br/>дату/БД</p>
			</div>
			<!--<div class="sm_label">
				<p>Добавленные слайды:</p>
			</div>-->
			<!--<input type="button" class="sm_button_add" name="sm_button_add" id="sm_button_add" value="Добавить слайд" />-->
			<div class="sm_slides_list">
			<ul id="sortable_ppt">
			</ul>
			</div>
		</div>
	</div>
	<div class="slide_settings_fade">
		<div class="slide_settings">
		</div>
	</div>
	<div class="tmpl_settings_fade">
		<div class="tmpl_settings">
		</div>
	</div>
	<div class="input_data_fade" id="input_data_fade">
		<div class="input_data" id="input_data">
			<div class="id_date_label">
				<p>Для начала работы с графиками выберите дату</p>
			</div>
			<input type="date" class="id_date" name="id_date" id="id_date" value="2022-02-28" min="2020-01-01" max="2022-12-31" onchange="window.changeEndDate();" />
			<div class="id_base_label">
				<p>Выберите вариант базы:</p>
			</div>
			<select class="id_base" name="id_base" id="id_base">
				<option selected="selected" id="0" value="0">Россия</option>
				<option id="1" value="1">Беларусь</option>
				<option id="2" value="2">Казахстан</option>
				<option id="3" value="3">Тестовая</option>
			</select>
			<div class="id_main_label">
				<p>Выберите основную корпорацию:</p>
			</div>
			<select class="id_main" name="id_main" id="id_main">
			</select>
			<input type="button" class="id_button" name="id_button" id="id_button" value="СТАРТ"/>
		</div>
	</div>
	<div class="tmpl_choose_fade">
		<div class="tmpl_choose">
			<div class="fc_label">
				<p>Выберите шаблон</p>
			</div>
			<div class="tmpl_select">
				<p>Выбор сохраненного шаблона</p>
			</div>
			<input type="button" class="ur_button_ok" name="ur_button_load" id="ur_button_load" value="Загрузить" />
			<input type="button" class="ur_button_del" name="ur_button_del" id="ur_button_del" value="Удалить" />
			<input type="button" class="ur_button_choose_exit" name="ur_button_choose_exit" id="ur_button_choose_exit" value="Отменить" />
		</div>
	</div>
	<div class="tmpl_add_fade">
		<div class="tmpl_add">
			<div class="fc_label">
				<p>Выберите шаблон</p>
			</div>
			<div class="tml_select">
				<p>Добавление нового шаблона</p>
			</div>
			<input type="button" class="ur_button_add" name="ur_button_add" id="ur_button_add" value="Сохранить" />
			<input type="button" class="ur_button_add_exit" name="ur_button_add_exit" id="ur_button_add_exit" value="Отменить" />
		</div>
	</div>
	<div class="new_diapason_settings_fade">
		<div class="new_diapason_settings">
			<div class="nd_label">
				<p>Задайте пераметры диапазона цен</p>
			</div>
			<div class="nd_price_min">
				<div class="ndp_label">
					<p>Цена min</p>
				</div>
				<input type="number" class="ndp_input_min" name="ndp_input_min" id="ndp_input_min" value="0" min="0" max="9999999999999" step="0.01">
			</div>
			<div class="nd_price_max">
				<div class="ndp_label">
					<p>Цена max</p>
				</div>
				<input type="number" class="ndp_input_max" name="ndp_input_max" id="ndp_input_max" value="9999999999999" min="0" max="9999999999999" step="0.01">
			</div>
			<input type="button" class="nd_button_ok" name="nd_button_ok" id="nd_button_ok" value="Ок" />
			<input type="button" class="nd_button_exit" name="nd_button_exit" id="nd_button_exit" value="Отменить" />
		</div>
	</div>
	<div class="filter_choose_fade">
		<div class="filter_choose">
			<div class="fc_label">
				<p>Выберите параметр фильтрации</p>
			</div>
			<div class="fc_select">
				<p>Селект столбца - параметра по которому фильтруются данные</p>
			</div>
			<input type="button" class="fc_button_ok" name="fc_button_ok" id="fc_button_ok" value="Ок" />
			<input type="button" class="fc_button_exit" name="fc_button_exit" id="fc_button_exit" value="Отменить" />
		</div>
	</div>
	<div class="sorter_choose_fade">
		<div class="sorter_choose">
			<div class="sc_label">
				<p>Выберите параметр сортировки</p>
			</div>
			<div class="sc_select">
				<p>Селект столбца - параметра по которому фильтруются данные</p>
			</div>
			<input type="button" class="sc_button_ok" name="sc_button_ok" id="sc_button_ok" value="Ок" />
			<input type="button" class="sc_button_exit" name="sc_button_exit" id="sc_button_exit" value="Отменить" />
		</div>
	</div>
	<div class="filter_settings_fade">
		<div class="filter_settings">
			<div class="fs_title_block">
				<div class="fs_tb_label">
					Выберите значения параметра фильтрации
				</div>
				<input type="button" class="fs_tb_button_ok" name="fs_tb_button_ok" id="fs_tb_button_ok" value="Ок" />
				<input type="button" class="fs_tb_button_exit" name="fs_tb_button_exit" id="fs_tb_button_exit" value="Отменить" />
			</div>
			<div class="fs_select_block">
				<div class="fs_sb_options_list">
				</div>
				<div class="fs_sb_selected_options">
				</div>
				<input type="text" class="fs_sb_search_string" name="fs_sb_search_string" id="fs_sb_search_string" value="">
				<input type="button" class="fs_sb_add_all" name="fs_sb_add_all" id="fs_sb_add_all" value="Выбрать все" />
				<input type="button" class="fs_sb_del_all" name="fs_sb_del_all" id="fs_sb_del_all" value="Отменить все" />
			</div>
		</div>
	</div>
	<div class="sorter_settings_fade">
		<div class="sorter_settings">
		</div>
	</div>
	<div class="orderer_settings_fade">
		<div class="orderer_settings">
		</div>
	</div>
	<div class="diapasons_settings_fade">
		<div class="diapasons_settings">
		</div>
	</div>
	<script>

		const chartAreaBorder = {
		  id: 'chartAreaBorder',
		  beforeDraw(chart, args, options) {
			const {ctx, chartArea: {left, top, width, height}} = chart;
			ctx.save();
			ctx.strokeStyle = options.borderColor;
			ctx.lineWidth = options.borderWidth;
			ctx.setLineDash(options.borderDash || []);
			ctx.lineDashOffset = options.borderDashOffset;
			ctx.strokeRect(left, top, width, height);
			ctx.restore();
		  }
		};

		const zeroCompensation = {
		  renderZeroCompensation: function (chartInstance, d) {
			  // get postion info from _view
			  const view = d._view
			  const context = chartInstance.chart.ctx

			  // the view.x is the centeral point of the bar, so we need minus half width of the bar.
			  const startX = view.x - view.width / 2
			  // common canvas API, Check it out on MDN
			  context.beginPath();
			  // set line color, you can do more custom settings here.
			  context.strokeStyle = '#aaaaaa';
			  context.moveTo(startX, view.y);
			  // draw the line!
			  context.lineTo(startX + view.width, view.y);
			  // bam！ you will see the lines.
			  context.stroke();
		  },

		  afterDatasetsDraw: function (chart, easing) {
			  // get data meta, we need the location info in _view property.
			  const meta = chart.getDatasetMeta(0)
			  // also you need get datasets to find which item is 0.
			  const dataSet = chart.config.data.datasets[0].data
			  meta.data.forEach((d, index) => {
				  // for the item which value is 0, reander a line.
				  if(dataSet[index] === 0) {
					this.renderZeroCompensation(chart, d)
				  }
			  })
		  }
		};

		Number.prototype.round2 = function(places) {
		  return +(Math.round(this + "e+" + places)  + "e-" + places);
		}
		
		function RGBAToHSLA(rgba) {
			let ex = /^rgba\((((((((1?[1-9]?\d)|10\d|(2[0-4]\d)|25[0-5]),\s?)){3})|(((([1-9]?\d(\.\d+)?)|100|(\.\d+))%,\s?){3}))|(((((1?[1-9]?\d)|10\d|(2[0-4]\d)|25[0-5])\s){3})|(((([1-9]?\d(\.\d+)?)|100|(\.\d+))%\s){3}))\/\s)((0?\.\d+)|[01]|(([1-9]?\d(\.\d+)?)|100|(\.\d+))%)\)$/i;
			if (ex.test(rgba)) {
				let sep = rgba.indexOf(",") > -1 ? "," : " ";
				rgba = rgba.substr(5).split(")")[0].split(sep);

				// strip the slash if using space-separated syntax
				if (rgba.indexOf("/") > -1)
					rgba.splice(3,1);

				for (let R in rgba) {
					let r = rgba[R];
					if (r.indexOf("%") > -1) {
						let p = r.substr(0,r.length - 1) / 100;

						if (R < 3) {
							rgba[R] = Math.round(p * 255);
						}
					}
				}

				// make r, g, and b fractions of 1
				let r = rgba[0] / 255,
					g = rgba[1] / 255,
					b = rgba[2] / 255,
					a = rgba[3],

				// find greatest and smallest channel values
					cmin = Math.min(r,g,b),
					cmax = Math.max(r,g,b),
					delta = cmax - cmin,
					h = 0,
					s = 0,
					l = 0;

				// calculate hue
				// no difference
				if (delta == 0)
					h = 0;
				// red is max
				else if (cmax == r)
					h = ((g - b) / delta) % 6;
				// green is max
				else if (cmax == g)
					h = (b - r) / delta + 2;
				// blue is max
				else
					h = (r - g) / delta + 4;

				h = Math.round(h * 60);

				// make negative hues positive behind 360°
				if (h < 0)
					h += 360;

				// calculate lightness
				l = (cmax + cmin) / 2;

				// calculate saturation
				s = delta == 0 ? 0 : delta / (1 - Math.abs(2 * l - 1));

				// multiply l and s by 100
				s = +(s * 100).toFixed(1);
				l = +(l * 100).toFixed(1);

				//return "hsla(" + h + "," + s + "%," + l + "%," + a + ")";
				return [h, s, l];

			} else {
				return "Invalid input color";
			}
		}

		function hsl2hsv(hslH, hslS, hslL) {
			const hsv1 = hslS * (hslL < 50 ? hslL : 100 - hslL) / 100;
			const hsvS = hsv1 === 0 ? 0 : 2 * hsv1 / (hslL + hsv1) * 100;
			const hsvV = hslL + hsv1;
			return [ hslH, hsvS, hsvV ];
		}

		var RGBvalues = (function() {

			var _hex2dec = function(v) {
				return parseInt(v, 16)
			};

			var _splitHEX = function(hex) {
				var c;
				if (hex.length === 4) {
					c = (hex.replace('#','')).split('');
					return {
						r: _hex2dec((c[0] + c[0])),
						g: _hex2dec((c[1] + c[1])),
						b: _hex2dec((c[2] + c[2]))
					};
				} else {
					 return {
						r: _hex2dec(hex.slice(1,3)),
						g: _hex2dec(hex.slice(3,5)),
						b: _hex2dec(hex.slice(5))
					};
				}
			};

			var _splitRGB = function(rgb) {
				var c = (rgb.slice(rgb.indexOf('(')+1, rgb.indexOf(')'))).split(',');
				var flag = false, obj;
				c = c.map(function(n,i) {
					return (i !== 3) ? parseInt(n, 10) : flag = true, parseFloat(n);
				});
				obj = {
					r: c[0],
					g: c[1],
					b: c[2]
				};
				if (flag) obj.a = c[3];
				return obj;
			};

			var color = function(col) {
				var slc = col.slice(0,1);
				if (slc === '#') {
					return _splitHEX(col);
				} else if (slc.toLowerCase() === 'r') {
					return _splitRGB(col);
				} else {
					console.log('!Ooops! RGBvalues.color('+col+') : HEX, RGB, or RGBa strings only');
				}
			};

			return {
				color: color
			};
		}());
		
		function rgbaToRgb (rgba) {
		var background = 'rgb(255, 255, 255)';
		//console.log('rgba: '+rgba);
			
		var rgba_color = RGBvalues.color(rgba);
		var background_color = RGBvalues.color(background);
		var result_rgb = rgba2rgb(background_color, rgba_color);
		return [Math.round(result_rgb.r), Math.round(result_rgb.g), Math.round(result_rgb.b)];
		}
		
		function Color(r, g, b, a) {
			this.r = r;
			this.b = b;
			this.g = g;
			this.a = typeof a == "number" ? a : 1;
		}
		function rgba2rgb(RGB_background, RGBA_color) {
			var alpha = RGBA_color.a;
			return new Color(
				(1 - alpha) * RGB_background.r + alpha * RGBA_color.r,
				(1 - alpha) * RGB_background.g + alpha * RGBA_color.g,
				(1 - alpha) * RGB_background.b + alpha * RGBA_color.b
			);
		}
		
		function contrastingColor(color) {
			return (luma(color) >= 165) ? '000' : 'fff';
		}
		function luma(color) { // color can be a hx string or an array of RGB values 0-255
			var rgb = (typeof color === 'string') ? hexToRGBArray(color) : color;
			return (0.2126 * rgb[0]) + (0.7152 * rgb[1]) + (0.0722 * rgb[2]); // SMPTE C, Rec. 709 weightings
		}
		function hexToRGBArray(color) {
			if (color.length === 3)
				color = color.charAt(0) + color.charAt(0) + color.charAt(1) + color.charAt(1) + color.charAt(2) + color.charAt(2);
			else if (color.length !== 6)
				throw('Invalid hex color: ' + color);
			var rgb = [];
			for (var i = 0; i <= 2; i++)
				rgb[i] = parseInt(color.substr(i * 2, 2), 16);
			return rgb;
		}
		
		function createPngDataTable() {
			/* Table of CRCs of all 8-bit messages. */
			const crcTable = new Int32Array(256);
			for (let n = 0; n < 256; n++) {
				let c = n;
				for (let k = 0; k < 8; k++) {
				c = (c & 1) ? 0xedb88320 ^ (c >>> 1) : c >>> 1;
				}
				crcTable[n] = c;
			}
			return crcTable;
		}

		function calcCrc(buf) {
			let c = -1;
			if (!pngDataTable) pngDataTable = createPngDataTable();
			for (let n = 0; n < buf.length; n++) {
				c = pngDataTable[(c ^ buf[n]) & 0xFF] ^ (c >>> 8);
			}
			return c ^ -1;
		}

		let pngDataTable;

		const PNG = 'image/png';
		const JPEG = 'image/jpeg';

		// those are 3 possible signature of the physBlock in base64.
		// the pHYs signature block is preceed by the 4 bytes of lenght. The length of
		// the block is always 9 bytes. So a phys block has always this signature:
		// 0 0 0 9 p H Y s.
		// However the data64 encoding aligns we will always find one of those 3 strings.
		// this allow us to find this particular occurence of the pHYs block without
		// converting from b64 back to string
		const b64PhysSignature1 = 'AAlwSFlz';
		const b64PhysSignature2 = 'AAAJcEhZ';
		const b64PhysSignature3 = 'AAAACXBI';

		const _P = 'p'.charCodeAt(0);
		const _H = 'H'.charCodeAt(0);
		const _Y = 'Y'.charCodeAt(0);
		const _S = 's'.charCodeAt(0);

		function changeDpiBlob(blob, dpi) {
			// 33 bytes are ok for pngs and jpegs
			// to contain the information.
			const headerChunk = blob.slice(0, 33);
			return new Promise((resolve, reject) => {
				const fileReader = new FileReader();
				fileReader.onload = () => {
				const dataArray = new Uint8Array(fileReader.result);
				const tail = blob.slice(33);
				const changedArray = changeDpiOnArray(dataArray, dpi, blob.type);
				resolve(new Blob([changedArray, tail], { type: blob.type }));
				};
				fileReader.readAsArrayBuffer(headerChunk);
			});
		}

		function changeDpiDataUrl(base64Image, dpi) {
			const dataSplitted = base64Image.split(',');
			const format = dataSplitted[0];
			const body = dataSplitted[1];
			let type;
			let headerLength;
			let overwritepHYs = false;
			if (format.indexOf(PNG) !== -1) {
				type = PNG;
				const b64Index = detectPhysChunkFromDataUrl(body);
				// 28 bytes in dataUrl are 21bytes, length of phys chunk with everything inside.
				if (b64Index >= 0) {
				headerLength = Math.ceil((b64Index + 28) / 3) * 4;
				overwritepHYs = true;
				} else {
				headerLength = 33 / 3 * 4;
				}
			}
			if (format.indexOf(JPEG) !== -1) {
				type = JPEG;
				headerLength = 18 / 3 * 4;
			}
			// 33 bytes are ok for pngs and jpegs
			// to contain the information.
			const stringHeader = body.substring(0, headerLength);
			const restOfData = body.substring(headerLength);
			const headerBytes = atob(stringHeader);
			const dataArray = new Uint8Array(headerBytes.length);
			for (let i = 0; i < dataArray.length; i++) {
				dataArray[i] = headerBytes.charCodeAt(i);
			}
			const finalArray = changeDpiOnArray(dataArray, dpi, type, overwritepHYs);
			const base64Header = btoa(String.fromCharCode(...finalArray));
			return [format, ',', base64Header, restOfData].join('');
		}

		function detectPhysChunkFromDataUrl(data) {
			let b64index = data.indexOf(b64PhysSignature1);
			if (b64index === -1) {
				b64index = data.indexOf(b64PhysSignature2);
			}
			if (b64index === -1) {
				b64index = data.indexOf(b64PhysSignature3);
			}
			// if b64index === -1 chunk is not found
			return b64index;
		}

		function searchStartOfPhys(data) {
			const length = data.length - 1;
			// we check from the end since we cut the string in proximity of the header
			// the header is within 21 bytes from the end.
			for (let i = length; i >= 4; i--) {
				if (data[i - 4] === 9 && data[i - 3] === _P &&
				data[i - 2] === _H && data[i - 1] === _Y &&
				data[i] === _S) {
					return i - 3;
				}
			}
		}

		function changeDpiOnArray(dataArray, dpi, format, overwritepHYs) {
			if (format === JPEG) {
				dataArray[13] = 1; // 1 pixel per inch or 2 pixel per cm
				dataArray[14] = dpi >> 8; // dpiX high byte
				dataArray[15] = dpi & 0xff; // dpiX low byte
				dataArray[16] = dpi >> 8; // dpiY high byte
				dataArray[17] = dpi & 0xff; // dpiY low byte
				return dataArray;
			}
			if (format === PNG) {
				const physChunk = new Uint8Array(13);
				// chunk header pHYs
				// 9 bytes of data
				// 4 bytes of crc
				// this multiplication is because the standard is dpi per meter.
				dpi *= 39.3701;
				physChunk[0] = _P;
				physChunk[1] = _H;
				physChunk[2] = _Y;
				physChunk[3] = _S;
				physChunk[4] = dpi >>> 24; // dpiX highest byte
				physChunk[5] = dpi >>> 16; // dpiX veryhigh byte
				physChunk[6] = dpi >>> 8; // dpiX high byte
				physChunk[7] = dpi & 0xff; // dpiX low byte
				physChunk[8] = physChunk[4]; // dpiY highest byte
				physChunk[9] = physChunk[5]; // dpiY veryhigh byte
				physChunk[10] = physChunk[6]; // dpiY high byte
				physChunk[11] = physChunk[7]; // dpiY low byte
				physChunk[12] = 1; // dot per meter....

				const crc = calcCrc(physChunk);

				const crcChunk = new Uint8Array(4);
				crcChunk[0] = crc >>> 24;
				crcChunk[1] = crc >>> 16;
				crcChunk[2] = crc >>> 8;
				crcChunk[3] = crc & 0xff;

				if (overwritepHYs) {
				const startingIndex = searchStartOfPhys(dataArray);
				dataArray.set(physChunk, startingIndex);
				dataArray.set(crcChunk, startingIndex + 13);
				return dataArray;
				} else {
				// i need to give back an array of data that is divisible by 3 so that
				// dataurl encoding gives me integers, for luck this chunk is 17 + 4 = 21
				// if it was we could add a text chunk contaning some info, untill desired
				// length is met.

				// chunk structur 4 bytes for length is 9
				const chunkLength = new Uint8Array(4);
				chunkLength[0] = 0;
				chunkLength[1] = 0;
				chunkLength[2] = 0;
				chunkLength[3] = 9;

				const finalHeader = new Uint8Array(54);
				finalHeader.set(dataArray, 0);
				finalHeader.set(chunkLength, 33);
				finalHeader.set(physChunk, 37);
				finalHeader.set(crcChunk, 50);
				return finalHeader;
				}
			}
		}
		
		var getPercentWidth = function(elem){
			var elemName = elem.attr("id");
			var width = elem.width();
			var parentWidth = elem.offsetParent().width();
			var percent = Math.round(100*width/parentWidth);
			return percent;
		}
		var getPercentHeight = function(elem){
			var elemName = elem.attr("id");
			var height = elem.height();
			var parentHeight = elem.offsetParent().height();
			var percent = Math.round(100*height/parentHeight);
			return percent;
		}
		var getPercentTop = function(elem){
			var elemName = elem.attr("id");
			var percent = Math.round($(elem).position().top / $(elem).parent().height() * 100);
			return percent;
		}
		
	</script>
	<script>
		window.onload = function() {
			$('.slider_menu_fade').fadeOut(10);
			window.DBase = $('.id_base').val();
			window.setStartedParams();
			$('.input_data_fade').fadeIn(10);
			$('.filter_choose_fade').fadeOut(10);
			$('.sorter_choose_fade').fadeOut(10);
			$('.filter_settings_fade').fadeOut(10);
			$('.right_arrow').show();
			$('.left_arrow').hide();
			window.changeEndDate();
			window.sortDisableSelection();
		};

		function imageToBase64(img) {
			var canvas, ctx, dataURL, base64;
			canvas = document.createElement("canvas");
			ctx = canvas.getContext("2d");
			canvas.width = img.width;
			canvas.height = img.height;
			ctx.drawImage(img, 0, 0);
			dataURL = canvas.toDataURL("image/png");
			var dataURL150 = changeDpiDataUrl(dataURL, 150);
			base64 = dataURL150.replace(/^data:image\/png;base64,/, "");
			return base64;
		}

		window.AddChart2Slide = function(pres,canvas,x,w,y,h,sld=false) {
			var slide;
			if(sld==false) {
				slide = pres.addSlide();
			} else {
				slide = sld;
			}
			var url = canvas.toBase64Image();

			//var dataURL = canvas.toDataURL('image/jpeg', 1.0);
		//	var dataURL = canvas.toDataURL("image/png");
		//	var dataURL150 = changeDpiDataUrl(dataURL, 150);
		//	var url = dataURL150.replace(/^data:image\/png;base64,/, "");
			
			slide.addImage({ data: url, x:x, y:y, w:w, h:h });
			return slide;
		};

		window.AddImg2Slide = function(sel, slide1, deferred, x, w, y, h, bg=0, scale=8) {

			var wd = $(sel).innerWidth()+10;

			html2canvas(document.querySelector(sel), {
				scale:window.devicePixelRatio*scale, 
				backgroundColor: bg, 
				width: wd,
				imageTimeout: 0,
				//x: 0,
				//y: 0, 
				scrollY: -window.scrollY
				}).then(canvas =>{

				var context = canvas.getContext('2d');

				context.webkitImageSmoothingEnabled = false;
				context.mozImageSmoothingEnabled = false;
				context.imageSmoothingEnabled = false;

				var img_ = canvas.toDataURL('image/jpeg', 1.0);
				var img = changeDpiDataUrl(img_, 150);
				//window.location.href=img;

				slide1.addImage({ data: img, x:x, y:y, w:w, h:h });
				deferred.resolve();
			});
		};

		window.setMainSelect = function() {
			$.ajax({
				url: 'http://betp.website/wp-admin/admin-ajax.php',
				type: 'POST',
				data: {
					action: 'ajax',
					task: 'getMain',
					dbase: window.DBase
				},
				beforeSend: function() {},
				success: function(response) {
					response = JSON.parse(response);
					let id_ms_select_content = '';
					if (response.hasOwnProperty('error')) {
						$().message_err('Ошибка при получении данных из БД: ' + response.error);
						return false;
					} else if (response.result > 0) {
						if ((typeof(window.Main) != "undefined") && (window.Main !== null)) {
							for (let i = 0; i < response.data.length; i++) {
								if (window.Main == response.data[i].id) {
									id_ms_select_content += '<option selected="selected" id="' + response.data[i].id + '" value="' + response.data[i].id + '">' + response.data[i].value + '</option>';
								} else {
									id_ms_select_content += '<option id="' + response.data[i].id + '" value="' + response.data[i].id + '">' + response.data[i].value + '</option>';
								}
							}
						} else {
							for (let i = 0; i < response.data.length; i++) {
								if (response.defaultId == response.data[i].id) {
									id_ms_select_content += '<option selected="selected" id="' + response.data[i].id + '" value="' + response.data[i].id + '">' + response.data[i].value + '</option>';
								} else {
									id_ms_select_content += '<option id="' + response.data[i].id + '" value="' + response.data[i].id + '">' + response.data[i].value + '</option>';
								}
							}
						}
					}
					$('.id_main')[0].innerHTML = id_ms_select_content;
					window.Main = $('.id_main').val();
				}
			});
		};

		window.setStartedParams = function() {
			window.sC = 0;
			window.fC = 0;
			window.pres = 0;
			window.cP = -1;
			window.sD = [];
			window.maxPrice = 0;
			$.ajax({
				url: 'http://betp.website/wp-admin/admin-ajax.php',
				type: 'POST',
				data: {
					action: 'ajax',
					task: 'maxStart',
					dbase: window.DBase
				},
				beforeSend: function() {},
				success: function(response) {
					response = JSON.parse(response);
					if (response.hasOwnProperty('error')) {
						$().message_err('Ошибка при получении данных из БД: ' + response.error);
						return false;
					} else if (response.result > 0) {
						window.maxPrice = response.max_price;
						$('.ndp_input_max').attr("max", window.maxPrice);
						$('.ndp_input_max').attr("value", window.maxPrice);
						$('.ndp_input_min').attr("max", window.maxPrice);
			window.pSD = {
				'sT': 'Новый слайд',
				'gT': 0,
				'sI': 60,
				'iI': 0,
				'sP': 0,
				'sU': 0,
				'sM': 0,
				'fC': 0,
				'sCs': 7,
				'fP': 0,
				'fN': '',
				'fI': '',
				'fD': [],
				'ssC': [],
				'osC': [],
				'dC': 5,
				'cD': 5,
				'dM': 1000,
				"dD": [
					{
						"sS": 0,
						"eS": 499999,
						"clr": 'rgba(239, 121, 9, 1)'
					},
					{
						"sS": 500000,
						"eS": 999999,
						"clr": 'rgba(255, 128, 0, 0.4)'
					},
					{
						"sS": 1000000,
						"eS": 4999999,
						"clr": 'rgba(71, 67, 137, 1)'
					},
					{
						"sS": 5000000,
						"eS": 9999999,
						"clr": 'rgba(66, 170, 255, 1)'
					},
					{
						"sS": 10000000,
						"eS": window.maxPrice,
						"clr": 'rgba(138, 123, 21, 0.75)'
					},
				],
			};
					}
				}
			});

			window.slidesListDraw();
			window.setMainSelect();
			$('.sp_sb_cs_up_status')[0].innerHTML = '<p>Слайды: ' + (window.cP>=0?window.cP+1:0) + ' из '+window.sC+'</p>';
			
			window.BrandsData = {};
			$.ajax({
				url: 'http://betp.website/wp-admin/admin-ajax.php',
				type: 'POST',
				async : false,
				data: {
					action: 'ajax',
					task: 'getName2Colors',
					dbase: window.DBase
				},
				beforeSend: function() {},
				success: function(response) {
					response = JSON.parse(response);
					if (response.hasOwnProperty('error')) {
						$().message_err('Ошибка при получении данных из БД: ' + response.error);
						return false;
					} else if (response.result > 0) {
						var brands = response.data;
						var newBrands = {};
						var re = /(rgb)\(([0-9]+),\s+([0-9]+),\s+([0-9]+)/;
						var subst = 'rgba($2, $3, $4, 1';
						Object.keys(brands).forEach(function( key ) {
							var element = {};
							element.name = brands[key].name;
							element.color = brands[key].color.replace(re, subst);
							newBrands[key] = element;
						});
						window.BrandsData = newBrands;
					}
				}
			});
			//console.log(JSON.stringify(window.BrandsData));

			var fs, ww=window.screen.width;
			console.log('WW: '+ww);
			//console.log('devicePixelRatio: '+window.devicePixelRatio);
			
			if(ww <= 1440) {
				fs = "13px";
				$('body').css({'font-size':13});
				$('input').css({'font-size' : 13, 'padding':'5px'});

				$('input.left_arrow').css({'width':'30px', 'height':'30px', 'border-radius':'20px'});
				$('input.right_arrow').css({'width':'30px', 'height':'30px', 'border-radius':'20px'});
				console.log('left wid: '+$('.left_arrow').width());
				console.log('rigth wid: '+$('.right_arrow').width());
				console.log('rigth css wid: '+$('.right_arrow').css('width'));
			
			}
			/*if(width >= 2000) fs = "30px";
			if(width >= 1500) fs = "25px";
			if(width >= 1000) fs = "20px";
			if(width <= 1000) fs = "15px";
			document.body.style.fontSize = fs;
			
			var resizeFonts = function() {
			  var item = document.getElementById("div1");
			  if (window.screen.width <= 1100) {
				  item.style.fontSize = "25px";
				  item.innerHTML = "String";
			  }
			  // Otherwise set a larger font
			  else item.style.fontSize = "30px";
			};

			window.onload = resizeFonts;
			window.onresize = resizeFonts;

			*/
			
		};

		window.changeEndDate = function() {
			let selectedDate = document.getElementById("id_date").value.split('-');
			window.controlDate = 'Выбранная контрольная дата: ' + selectedDate[2] + '.' + selectedDate[1] + '.' + selectedDate[0];
			$('.sm_date_value')[0].innerHTML = '<p>Выбранная контрольная дата: ' + selectedDate[2] + '.' + selectedDate[1] + '.' + selectedDate[0] + '</p>';
		};

		window.changeSlideBeginDate = function() {
			document.getElementById("ss_sdd_date_input_2").min = document.getElementById("ss_sdd_date_input_1").value;
		};

		window.changeSlideEndDate = function() {
			document.getElementById("ss_sdd_date_input_1").max = document.getElementById("ss_sdd_date_input_2").value;
		};

		window.changeFile = function(evt) {
			window.setStartedParams();
			let selectedFile = evt.target.files[0];
			let regex = /(.xls|.xlsx)$/;
			if (selectedFile) {
				if (regex.test(selectedFile.name.toLowerCase())) {
					if (typeof(FileReader) != "undefined") {
						let reader = new FileReader();
						if (reader.readAsBinaryString) {
							reader.onload = function(e) {
								let workbook = XLSX.read(e.target.result, {
									type: 'binary'
								});
								$.ajax({
									url: 'http://betp.website/wp-admin/admin-ajax.php',
									type: 'POST',
									data: {
										action: 'ajax',
										task: 'loadNewFile',
										data: XLSX.utils.sheet_to_json(workbook.Sheets[workbook.SheetNames[0]]),
										dbase: window.DBase
									},
									beforeSend: function() {
										$('.id_button').val('Загрузка, ...');
										$('.id_button').prop('disabled', true);
									},
									success: function(response) {
										$('.id_button').val('Старт');
										$('.id_button').prop('disabled', false);
									}
								});
							};
							reader.readAsBinaryString(selectedFile);
						} else {
							reader.onload = function(e) {
								let data = "";
								let bytes = new Uint8Array(e.target.result);
								for (let i = 0; i < bytes.byteLength; i++) {
									data += String.fromCharCode(bytes[i]);
								};
								let workbook = XLSX.read(data, {
									type: 'binary'
								});
								$.ajax({
									url: 'http://betp.website/wp-admin/admin-ajax.php',
									type: 'POST',
									data: {
										action: 'ajax',
										task: 'loadNewFile',
										data: XLSX.utils.sheet_to_json(workbook.Sheets[workbook.SheetNames[0]]),
										dbase: window.DBase
									},
									beforeSend: function() {
										$('.id_button').val('Загрузка, ...');
										$('.id_button').prop('disabled', true);
									},
									success: function(response) {
										$('.id_button').val('Старт');
										$('.id_button').prop('disabled', false);
									}
								});
							};
							reader.readAsArrayBuffer(selectedFile);
						};
						$('.id_button').prop('disabled', false);
						$('.sm_file_value')[0].innerHTML = '<p>Выбранный файл: ' + selectedFile.name + '</p>';

					} else {
						alert("This browser does not support HTML5.");
					};
				} else {
					$().message_err("Please upload a valid Excel file.");
				};
			};
		};

		window.slideSettingsDraw = function() {
			$.ajax({
				url: 'http://betp.website/wp-admin/admin-ajax.php',
				type: 'POST',
				data: {
					action: 'ajax',
					task: 'getColumns',
					data: window.sC,
					dbase: window.DBase
				},
				beforeSend: function() {},
				success: function(response) {
					response = JSON.parse(response);
					let ss_so_select_content = '';
					let sd_button_edit_prop = 'hidden';
					if (0 === Number(window.cSD.sI)) {
						ss_so_select_content = '<option selected="selected" id="0" value="0">Ценовые диапазоны</option>';
						sd_button_edit_prop = '';
					} else {
						ss_so_select_content = '<option id="0" value="0">Ценовые диапазоны</option>';
					}
					if (response.hasOwnProperty('error')) {
						$().message_err('Ошибка при получении данных из БД: ' + response.error);
						return false;
					} else if (response.result > 0) {
						console.log('window.cSD.sI: '+window.cSD.sI);
						if ((typeof(window.cSD.sI) != "undefined") && (window.cSD.sI !== null)) {
							for (let i = 0; i < response.data.length; i++) {
								if (response.data[i].id === Number(window.cSD.sI)) {
									ss_so_select_content += '<option selected="selected" id="' + response.data[i].id + '" value="' + response.data[i].id + '">' + response.data[i].value + '</option>';
								} else {
									ss_so_select_content += '<option id="' + response.data[i].id + '" value="' + response.data[i].id + '">' + response.data[i].value + '</option>';
								}
							}
						} /*else if ((typeof(window.oGsI) != "undefined") && (window.oGsI !== null)) {
							for (let i = 0; i < response.data.length; i++) {
								if (response.data[i].id === Number(window.oGsI)) {
									ss_so_select_content += '<option selected="selected" id="' + response.data[i].id + '" value="' + response.data[i].id + '">' + response.data[i].value + '</option>';
								} else {
									ss_so_select_content += '<option id="' + response.data[i].id + '" value="' + response.data[i].id + '">' + response.data[i].value + '</option>';
								}
							}
						} */else {
							for (let i = 0; i < response.data.length; i++) {
								if (response.data[i].id == 60) {
									ss_so_select_content += '<option selected="selected" id="' + response.data[i].id + '" value="' + response.data[i].id + '">' + response.data[i].value + '</option>';
								} else {
									ss_so_select_content += '<option id="' + response.data[i].id + '" value="' + response.data[i].id + '">' + response.data[i].value + '</option>';
								}
							}
						}
					}

					let options_names = ['Слайд тип "Страница1"','Слайд тип "Страница2"','Слайд тип "Страница3"','Слайд тип "Страница4 YTD"','Слайд тип "Страница5 MAT"','Слайд тип "Страница6 Круговая YTD"','Слайд тип "Страница7 Круговая MAT"','Слайд тип "Страница8 YTD 2 стб"','Слайд тип "Страница9 MAT 2 стб"','Слайд тип "Страница10 DIAP 2 стб"','Слайд тип "Страница11 YTD 3 стб"','Слайд тип "Страница12 MAT 3 стб"','Слайд тип "Страница13 DIAP 3 стб"','Слайд тип "Страница14"','Слайд тип "Страница15 4стб"','Слайд тип "Страница16 `горизонтальные тройки`"'];
					let ss_st_select_content = '';
					if ((typeof(window.cSD.gT) != "undefined") && (window.cSD.gT !== null)) {
						for (let i = 0; i < 16; i++) {
							if (i === Number(window.cSD.gT)) {
								ss_st_select_content += '<option selected="selected" id="' + i + '" value="' + i + '">' + options_names[i] + '</option>';
							} else {
								ss_st_select_content += '<option id="' + i + '" value="' + i + '">' + options_names[i] + '</option>';
							}
						}
					} else {
						for (let i = 0; i < 16; i++) {
							ss_st_select_content += '<option id="' + i + '" value="' + i + '">' + options_names[i] + '</option>';
						}
					}
					let ss_sd_select_content;
					if ((typeof(window.cSD.iI) != "undefined") && (window.cSD.iI !== null)) {
						if (Number(window.cSD.iI) === 1) {
							ss_sd_select_content = '<option id="0" value="0">Разбивка по кварталам</option><option selected="selected" id="1" value="1">Разбивка по месяцам</option><option id="2" value="2">Разбивка по годам</option>';
						} else if (Number(window.cSD.iI) === 2) {
							ss_sd_select_content = '<option id="0" value="0">Разбивка по кварталам</option><option id="1" value="1">Разбивка по месяцам</option><option selected="selected" id="2" value="2">Разбивка по годам</option>';
						} else {
							ss_sd_select_content = '<option selected="selected" id="0" value="0">Разбивка по кварталам</option><option id="1" value="1">Разбивка по месяцам</option><option id="2" value="2">Разбивка по годам</option>';
						}
					} else {
						ss_sd_select_content = '<option id="0" value="0">Разбивка по кварталам</option><option id="1" value="1">Разбивка по месяцам</option><option id="2" value="2">Разбивка по годам</option>';
					}
					let ss_sp_select_content;
					if ((typeof(window.cSD.sP) != "undefined") && (window.cSD.sP !== null)) {
						if (Number(window.cSD.sP) === 1) {
							ss_sp_select_content = '<option id="0" value="0">Анализ по суммам в РУБ</option><option selected="selected" id="1" value="1">Анализ по суммам в EUR</option><option id="2" value="2">Анализ по количествам</option>';
						} else if (Number(window.cSD.sP) === 2) {
							ss_sp_select_content = '<option id="0" value="0">Анализ по суммам в РУБ</option><option id="1" value="1">Анализ по суммам в EUR</option><option selected="selected" id="2" value="2">Анализ по количествам</option>';
						} else {
							ss_sp_select_content = '<option selected="selected" id="0" value="0">Анализ по суммам в РУБ</option><option id="1" value="1">Анализ по суммам в EUR</option><option id="2" value="2">Анализ по количествам</option>';
						}
					} else {
						ss_sp_select_content = '<option id="0" value="0">Анализ по суммам в РУБ</option><option id="1" value="1">Анализ по суммам в EUR</option><option id="2" value="2">Анализ по количествам</option>';
					}
					let ss_su_select_content;
					if ((typeof(window.cSD.sU) != "undefined") && (window.cSD.sU !== null)) {
						if (Number(window.cSD.sU) === 1) {
							ss_su_select_content = '<option id="0" value="0">включить в список</option><option selected="selected" id="1" value="1">включить в other</option>';
						} else {
							ss_su_select_content = '<option selected="selected" id="0" value="0">включить в список</option><option id="1" value="1">включить в other</option>';
						}
					} else {
						ss_su_select_content = '<option id="0" value="0">включить в список</option><option id="1" value="1">включить в other</option>';
					}
					let ss_sm_select_content;
					if ((typeof(window.cSD.sM) != "undefined") && (window.cSD.sM !== null)) {
						if (Number(window.cSD.sM) === 1) {
							ss_sm_select_content = '<option id="0" value="0">Миллиарды</option><option selected="selected" id="1" value="1">Миллионы</option><option id="2" value="2">Тысячи</option>';
						} else if (Number(window.cSD.sM) === 2) {
							ss_sm_select_content = '<option id="0" value="0">Миллиарды</option><option id="1" value="1">Миллионы</option><option selected="selected" id="2" value="2">Тысячи</option>';
						} else {
							ss_sm_select_content = '<option selected="selected" id="0" value="0">Миллиарды</option><option id="1" value="1">Миллионы</option><option id="2" value="2">Тысячи</option>';
						}
					} else {
						ss_sm_select_content = '<option id="0" value="0">Миллиарды</option><option id="1" value="1">Миллионы</option><option id="2" value="2">Тысячи</option>';
					}
					let ss_sc_select_content;
					if ((typeof(window.cSD.sCs) != "undefined") && (window.cSD.sCs !== null)) {
						for (let i = 2; i < 21; i++) {
							if (window.cSD.sCs == i) {
								ss_sc_select_content += '<option selected="selected" id="' + i + '" value="' + i + '">' + i + '</option>';
							} else {
								ss_sc_select_content += '<option id="' + i + '" value="' + i + '">' + i + '</option>';
							}
						}
					} else {
						for (let i = 2; i < 21; i++) {
							if (7 == i) {
								ss_sc_select_content += '<option selected="selected" id="' + i + '" value="' + i + '">' + i + '</option>';
							} else {
								ss_sc_select_content += '<option id="' + i + '" value="' + i + '">' + i + '</option>';
							}
						}
					}
					let ssd_params = [];
					if (window.pres) {
						ssd_params['ss_title'] = 'Задайте параметры слайда "по-умолчанию":';
						ssd_params['butons'] = '<input type="button" class="ss_button_ok" name="ss_button_ok" id="ss_button_ok" value="Сохранить"/><input type="button" class="ss_button_exit" name="ss_button_exit" id="ss_button_exit" value="Отменить"/>';
					} else {
						ssd_params['ss_title'] = 'Задайте параметры слайда:';
						ssd_params['butons'] = '<input type="button" class="ss_button_order" name="ss_button_order" id="ss_button_order" value="Упорядочить"/><input type="button" class="ss_button_ok" name="ss_button_ok" id="ss_button_ok" value="Ок"/><input type="button" class="ss_button_exit" name="ss_button_exit" id="ss_button_exit" value="Отменить"/>';
					}
					$('.slide_settings')[0].innerHTML = '<div class="ss_title"><p>' + ssd_params['ss_title'] + '</p></div><div class="ss_slide_params"><div class="ss_slide_name"><div class="ss_sn_label"><p>Наименование слайда</p></div><input type="text" class="ss_sn_input" name="ss_sn_input" id="ss_sn_input" value="' + window.cSD.sT + '"/></div><div class="ss_slide_type"><div class="ss_st_label"><p>Тип слайда</p></div><select class="ss_st_select" name="ss_st_select" id="ss_st_select">' + ss_st_select_content + '</select></div></div><div class="ss_slide_params_1"><div class="ss_slide_object_1"><div class="ss_so_label"><p>Корпорации U.M. </p></div><select class="ss_su_select" name="ss_su_select" id="ss_su_select">' + ss_su_select_content + '</select></div><div class="ss_slide_object_2"><div class="ss_so_label"><p>Масштаб сумм</p></div><select class="ss_sm_select" name="ss_sm_select" id="ss_sm_select">' + ss_sm_select_content + '</select></div><div class="ss_slide_diapazons"><div class="ss_sd_label"><p>Параметр анализа (руб/eur/кол-во)</p></div><select class="ss_sp_select" name="ss_sp_select" id="ss_sp_select">' + ss_sp_select_content + '</select></div></div><div class="ss_slide_params_2"><div class="ss_slide_object_1"><div class="ss_so_label"><p>Анализируемый элемент слайда</p></div><select class="ss_so_select" name="ss_so_select" id="ss_so_select">' + ss_so_select_content + '</select></div><div class="ss_slide_object_2"><div class="ss_so_label"><p>Количество категорий</p></div><select class="ss_sc_select" name="ss_sc_select" id="ss_sc_select">' + ss_sc_select_content + '</select></div><div class="ss_slide_diapazons"><div class="ss_sd_label"><p>Анализируемый диапазон (кварталы/месяцы)</p></div><select class="ss_sd_select" name="ss_sd_select" id="ss_sd_select">' + ss_sd_select_content + '</select></div></div><div class="ss_slide_params_3"><div class="ss_slide_object_1"><input type="button" class="sd_button_edit" name="sd_button_edit" id="sd_button_edit" value="Редактировать ценовые диапазоны" ' + sd_button_edit_prop + '/></div><div class="ss_slide_dates_diapazon"><div class="ss_sdd_label_1"><p>Установите интересующий диапазон анализа с</p></div><input type="date" class="ss_sdd_date_input_1" name="ss_sdd_date_input_1" id="ss_sdd_date_input_1" value="2021-01-01" min="2000-01-01" max="' + document.getElementById("id_date").value + '" onchange="window.changeSlideBeginDate();" /><div class="ss_sdd_label_2"><p>по</p></div><input type="date" class="ss_sdd_date_input_2" name="ss_sdd_date_input_2" id="ss_sdd_date_input_2" value="' + document.getElementById("id_date").value + '" min="2021-01-01" max="2022-12-31" onchange="window.changeSlideEndDate();" /></div></div><div class="ss_filters_block"><div class="ss_fb_label"><p>Список фильтров слайда:</p></div><input type="button" class="ss_fb_button" name="ss_fb_button" id="ss_fb_button" value="Добавить фильтр"/><div class="ss_fb_filters_list"></div></div>' + ssd_params['butons'];
					if ((($('.ss_st_select').val() > 6) && ($('.ss_st_select').val() < 13)) || ($('.ss_st_select').val() == 14)) {
						$('.ss_sdd_date_input_1').prop('disabled', false);
						$('.ss_sd_select').prop('disabled', true);
					} else {
						$('.ss_sdd_date_input_1').prop('disabled', true);
						$('.ss_sd_select').prop('disabled', false);
					}
					if ((typeof(window.cSD.sDt) != "undefined") && (window.cSD.sDt !== null)) {
						document.getElementById("ss_sdd_date_input_1").value = window.cSD.sDt;
					}
					if ((typeof(window.cSD.eDt) != "undefined") && (window.cSD.eDt !== null)) {
						document.getElementById("ss_sdd_date_input_2").value = window.cSD.eDt;
					}
					window.filtersListDraw();
					$('.slide_settings_fade').fadeIn(10);
				}
			});
		}

		window.diapasonsSettingsDraw = function() {
			$('.diapasons_settings')[0].innerHTML = '<div class="ds_title"><p>Задать параметры ценовых диапазонов</p></div><div class="ds_diapasons_block"><div class="ds_db_label"><p>Список параметров ценовых диапазонов:</p></div><input type="button" class="ds_db_button" name="ds_db_button" id="ds_db_button" value="Добавить диапазон"/><div class="ds_db_diapasons_list"></div></div><input type="button" class="ds_button_ok" name="ds_button_ok" id="ds_button_ok" value="Сохранить"/><input type="button" class="ds_button_exit" name="ds_button_exit" id="ds_button_exit" value="Отменить"/>';
			window.diapasonsListDraw();
			$('.diapasons_settings_fade').fadeIn(10);
		}

		window.filterChooseDraw = function() {
			$.ajax({
				url: 'http://betp.website/wp-admin/admin-ajax.php',
				type: 'POST',
				data: {
					action: 'ajax',
					task: 'getColumns',
					dbase: window.DBase
				},
				beforeSend: function() {},
				success: function(response) {
					response = JSON.parse(response);
					let fc_select_content = '';
					if (response.hasOwnProperty('error')) {
						$().message_err('Ошибка при получении данных из БД: ' + response.error);
						return false;
					} else if (response.result > 0) {
						if ((typeof(window.cSD.fD[window.cSD.cF]) != "undefined") && (window.cSD.fD[window.cSD.cF] !== null)) {
							for (let i = 0; i < response.data.length; i++) {
								if (response.data[i].id != $('.ss_so_select').val()) {
									if (response.data[i].id === Number(window.cSD.fD[window.cSD.cF].cI)) {
										fc_select_content += '<option selected="selected" id="' + response.data[i].id + '" value="' + response.data[i].id + '">' + response.data[i].value + '</option>';
									} else {
										fc_select_content += '<option id="' + response.data[i].id + '" value="' + response.data[i].id + '">' + response.data[i].value + '</option>';
									}
								}
							}
						} else {
							for (let i = 0; i < response.data.length; i++) {
								if (response.data[i].id != $('.ss_so_select').val()) {
									fc_select_content += '<option id="' + response.data[i].id + '" value="' + response.data[i].id + '">' + response.data[i].value + '</option>';
								}
							}
						}

					}
					$('.filter_choose')[0].innerHTML = '<div class="fc_label"><p>Выберите параметр фильтрации</p></div><select class="fc_select" name="fc_select" id="fc_select">' + fc_select_content + '</select><input type="button" class="fc_button_ok" name="fc_button_ok" id="fc_button_ok" value="Ок"/><input type="button" class="fc_button_exit" name="fc_button_exit" id="fc_button_exit" value="Отменить"/>';
					if (typeof window.cSD.fD[window.cSD.cF] === 'undefined') {
						window.cSD.fDold = window.cSD.fD[window.cSD.cF];
					} else {
						window.cSD.fDold = JSON.parse(JSON.stringify(window.cSD.fD[window.cSD.cF]));
					}
					$('.filter_choose_fade').fadeIn(10);
				}
			});
		}

		window.diapasonChooseDraw = function() {
			let minVal = 0;
			let maxVal = window.maxPrice;
			if ((typeof(window.cSD.dD[window.cSD.cD]) != "undefined") && (window.cSD.dD[window.cSD.cD] !== null)) {
				minVal = window.cSD.dD[window.cSD.cD].sS;
				maxVal = window.cSD.dD[window.cSD.cD].eS;
			}
			$('.new_diapason_settings')[0].innerHTML = '<div class="nd_label"><p>Задайте пераметры диапазона цен</p></div><div class="nd_price_min"><div class="ndp_label"><p>Цена min</p></div><input type="number" class="ndp_input_min" name="ndp_input_min" id="ndp_input_min" value="' + minVal + '" min="0" max="' + maxVal + '" step="0.01"></div><div class="nd_price_max"><div class="ndp_label"><p>Цена max</p></div><input type="number" class="ndp_input_max" name="ndp_input_max" id="ndp_input_max" value="' + maxVal + '" min="' + minVal + '" max="' + window.maxPrice + '" step="0.01"></div><input type="button" class="nd_button_ok" name="nd_button_ok" id="nd_button_ok" value="Ок" /><input type="button" class="nd_button_exit" name="nd_button_exit" id="nd_button_exit" value="Отменить" />';
			/*if (typeof window.cSD.fD[window.cSD.cF] === 'undefined') {
				window.cSD.fDold = window.cSD.fD[window.cSD.cF];
			} else {
				window.cSD.fDold = JSON.parse(JSON.stringify(window.cSD.fD[window.cSD.cF]));
			}*/
			$('.new_diapason_settings_fade').fadeIn(10);
		}

		window.sorterChooseDraw = function() {
			$.ajax({
				url: 'http://betp.website/wp-admin/admin-ajax.php',
				type: 'POST',
				data: {
					action: 'ajax',
					task: 'getColumns',
					dbase: window.DBase
				},
				beforeSend: function() {},
				success: function(response) {
					response = JSON.parse(response);
					let sc_select_content = '';
					if (response.hasOwnProperty('error')) {
						$().message_err('Ошибка при получении данных из БД: ' + response.error);
						return false;
					} else if (response.result > 0) {
						if ((typeof(window.cSD.sCD) != "undefined") && (window.cSD.sCD !== null)) {
							for (let i = 0; i < response.data.length; i++) {
								if (response.data[i].id != $('.ss_so_select').val()) {
									if (response.data[i].id === Number(window.cSD.sCD)) {
										sc_select_content += '<option selected="selected" id="' + response.data[i].id + '" value="' + response.data[i].id + '">' + response.data[i].value + '</option>';
									} else {
										sc_select_content += '<option id="' + response.data[i].id + '" value="' + response.data[i].id + '">' + response.data[i].value + '</option>';
									}
								}
							}
						} else {
							for (let i = 0; i < response.data.length; i++) {
								if (response.data[i].id != $('.ss_so_select').val()) {
									sc_select_content += '<option id="' + response.data[i].id + '" value="' + response.data[i].id + '">' + response.data[i].value + '</option>';
								}
							}
						}

					}
					$('.sorter_choose')[0].innerHTML = '<div class="sc_label"><p>Выберите параметр сортировки</p></div><select class="sc_select" name="sc_select" id="sc_select">' + sc_select_content + '</select><input type="button" class="sc_button_ok" name="sc_button_ok" id="sc_button_ok" value="Ок"/><input type="button" class="sc_button_exit" name="sc_button_exit" id="sc_button_exit" value="Отменить"/>';
					$('.sorter_choose_fade').fadeIn(10);
				}
			});
		}

		window.filterSettingsDraw = function() {
			$.ajax({
				url: 'http://betp.website/wp-admin/admin-ajax.php',
				type: 'POST',
				data: {
					action: 'ajax',
					task: 'getColValues',
					column: $('.fc_select').val(),
					params: window.cSD,
					filternum: window.cSD.cF,
					sdate: document.getElementById("ss_sdd_date_input_1").value,
					edate: document.getElementById("ss_sdd_date_input_2").value,
					dbase: window.DBase
				},
				beforeSend: function() {},
				success: function(response) {
					response = JSON.parse(response);
					let fs_sb_options_list_content = '';
					let fs_sb_selected_options_content = '';
					if (response.hasOwnProperty('error')) {
						$().message_err('Ошибка при получении данных из БД: ' + response.error);
						return false;
					} else if (response.result > 0) {
						window.getColValuesresponse = response;
						let selectedCount = -1;
						console.log('filterSettingsDraw: window.getColValuesresponse: '+JSON.stringify(window.getColValuesresponse));
						if ((typeof(window.cSD.fD[window.cSD.cF]) != "undefined") && (window.cSD.fD[window.cSD.cF] !== null)) {
						let vI = window.cSD.fD[window.cSD.cF].vI.map((i) => Number(i));
						console.log('vI: '+JSON.stringify(vI));
						window.cSD.fD[window.cSD.cF].vI = vI;
						console.log('filterSettingsDraw: window.cSD.fD[window.cSD.cF]: '+JSON.stringify(window.cSD.fD[window.cSD.cF]));
							for (let i = 0; i < window.getColValuesresponse.data.length; i++) {
							console.log('filterSettingsDraw: window.getColValuesresponse.data[i].id: '+window.getColValuesresponse.data[i].id);
								if (window.cSD.fD[window.cSD.cF].vI.indexOf(window.getColValuesresponse.data[i].id) > -1) {
									console.log('filterSettingsDraw: > - 1');
									selectedCount++;
									fs_sb_options_list_content += '<div class="fs_sb_ol_option_block _' + window.getColValuesresponse.data[i].id + '" style="top: calc(5px + ' + (5 * i) + '%);"><input type="checkbox" onchange="window.fs_sb_ol_ob_checkbox_change(' + window.getColValuesresponse.data[i].id + ", '" + window.getColValuesresponse.data[i].value + "'" + ');" class="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" value="' + window.getColValuesresponse.data[i].id + '" checked><label class="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '">' + window.getColValuesresponse.data[i].value + '</label></div>';
									fs_sb_selected_options_content += '<div class="fs_sb_so_option_block _' + window.getColValuesresponse.data[i].id + '" style="top: calc(5px + ' + (5 * selectedCount) + '%);"><input type="checkbox" onchange="window.fs_sb_ol_ob_checkbox_change(' + window.getColValuesresponse.data[i].id + ", '" + window.getColValuesresponse.data[i].value + "'" + ');" class="fs_sb_so_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_so_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_so_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" value="' + window.getColValuesresponse.data[i].id + '" checked><label class="fs_sb_so_ob_label _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_so_ob_label _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_so_ob_label _' + window.getColValuesresponse.data[i].id + '">' + window.getColValuesresponse.data[i].value + '</label></div>';
								} else {
									console.log('filterSettingsDraw: Not In');
									fs_sb_options_list_content += '<div class="fs_sb_ol_option_block _' + window.getColValuesresponse.data[i].id + '" style="top: calc(5px + ' + (5 * i) + '%);"><input type="checkbox" onchange="window.fs_sb_ol_ob_checkbox_change(' + window.getColValuesresponse.data[i].id + ", '" + window.getColValuesresponse.data[i].value + "'" + ');" class="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" value="' + window.getColValuesresponse.data[i].id + '"><label class="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '">' + window.getColValuesresponse.data[i].value + '</label></div>';
								}
							}
						} else {
							for (let i = 0; i < window.getColValuesresponse.data.length; i++) {
								fs_sb_options_list_content += '<div class="fs_sb_ol_option_block _' + window.getColValuesresponse.data[i].id + '" style="top: calc(5px + ' + (5 * i) + '%);"><input type="checkbox" onchange="window.fs_sb_ol_ob_checkbox_change(' + window.getColValuesresponse.data[i].id + ", '" + window.getColValuesresponse.data[i].value + "'" + ');" class="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" value="' + window.getColValuesresponse.data[i].id + '"><label class="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '">' + window.getColValuesresponse.data[i].value + '</label></div>';
							}
						}
					}
					$('.fs_sb_options_list')[0].innerHTML = fs_sb_options_list_content;
					$('.fs_sb_selected_options')[0].innerHTML = fs_sb_selected_options_content;
					if (typeof window.cSD.fD[window.cSD.cF] === 'undefined') {
						window.cSD.fDoldV = window.cSD.fD[window.cSD.cF];
					} else {
						window.cSD.fDoldV = JSON.parse(JSON.stringify(window.cSD.fD[window.cSD.cF]));
					}
					$('.filter_settings_fade').fadeIn(10);
				}
			});
		}

		window.fs_sb_ol_ob_checkbox_change = function(checkedID, checkedVal) {
			let searched = fs_sb_search_string.value;
			let fs_sb_options_list_content = '';
			let fs_sb_selected_options_content = '';
			let currIndex = window.cSD.fD[window.cSD.cF].vI.indexOf(checkedID);
			let selectedCount = -1;
			if (currIndex < 0) {
				window.cSD.fD[window.cSD.cF].vI.push(checkedID);
				window.cSD.fD[window.cSD.cF].vV['i' + checkedID] = checkedVal;
			} else {
				delete window.cSD.fD[window.cSD.cF].vV['i' + window.cSD.fD[window.cSD.cF].vI[currIndex]];
				window.cSD.fD[window.cSD.cF].vI.splice(currIndex, 1);
			}
			if (searched == '') {
				if ((typeof(window.cSD.fD[window.cSD.cF]) != "undefined") && (window.cSD.fD[window.cSD.cF] !== null)) {
					for (let i = 0; i < window.getColValuesresponse.data.length; i++) {
						if (window.cSD.fD[window.cSD.cF].vI.indexOf(window.getColValuesresponse.data[i].id) > -1) {
							selectedCount++;
							fs_sb_options_list_content += '<div class="fs_sb_ol_option_block _' + window.getColValuesresponse.data[i].id + '" style="top: calc(5px + ' + (5 * i) + '%);"><input type="checkbox" onchange="window.fs_sb_ol_ob_checkbox_change(' + window.getColValuesresponse.data[i].id + ", '" + window.getColValuesresponse.data[i].value + "'" + ');" class="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" value="' + window.getColValuesresponse.data[i].id + '" checked><label class="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '">' + window.getColValuesresponse.data[i].value + '</label></div>';
							fs_sb_selected_options_content += '<div class="fs_sb_so_option_block _' + window.getColValuesresponse.data[i].id + '" style="top: calc(5px + ' + (5 * selectedCount) + '%);"><input type="checkbox" onchange="window.fs_sb_ol_ob_checkbox_change(' + window.getColValuesresponse.data[i].id + ", '" + window.getColValuesresponse.data[i].value + "'" + ');" class="fs_sb_so_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_so_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_so_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" value="' + window.getColValuesresponse.data[i].id + '" checked><label class="fs_sb_so_ob_label _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_so_ob_label _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_so_ob_label _' + window.getColValuesresponse.data[i].id + '">' + window.getColValuesresponse.data[i].value + '</label></div>';
						} else {
							fs_sb_options_list_content += '<div class="fs_sb_ol_option_block _' + window.getColValuesresponse.data[i].id + '" style="top: calc(5px + ' + (5 * i) + '%);"><input type="checkbox" onchange="window.fs_sb_ol_ob_checkbox_change(' + window.getColValuesresponse.data[i].id + ", '" + window.getColValuesresponse.data[i].value + "'" + ');" class="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" value="' + window.getColValuesresponse.data[i].id + '"><label class="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '">' + window.getColValuesresponse.data[i].value + '</label></div>';
						}
					}
				} else {
					for (let i = 0; i < window.getColValuesresponse.data.length; i++) {
						fs_sb_options_list_content += '<div class="fs_sb_ol_option_block _' + window.getColValuesresponse.data[i].id + '" style="top: calc(5px + ' + (5 * i) + '%);"><input type="checkbox" onchange="window.fs_sb_ol_ob_checkbox_change(' + window.getColValuesresponse.data[i].id + ", '" + window.getColValuesresponse.data[i].value + "'" + ');" class="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" value="' + window.getColValuesresponse.data[i].id + '"><label class="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '">' + window.getColValuesresponse.data[i].value + '</label></div>';
					}
				}
			} else {
				let searchedCount = -1;
				if ((typeof(window.cSD.fD[window.cSD.cF]) != "undefined") && (window.cSD.fD[window.cSD.cF] !== null)) {
					for (let i = 0; i < window.getColValuesresponse.data.length; i++) {
						let searchedPos = window.getColValuesresponse.data[i].value.toUpperCase().indexOf(searched.toUpperCase());
						if (searchedPos != '-1') {
							searchedCount++;
							if (window.cSD.fD[window.cSD.cF].vI.indexOf(window.getColValuesresponse.data[i].id) > -1) {
								fs_sb_options_list_content += '<div class="fs_sb_ol_option_block _' + window.getColValuesresponse.data[i].id + '" style="top: calc(5px + ' + (5 * searchedCount) + '%)"><input type="checkbox" onchange="window.fs_sb_ol_ob_checkbox_change(' + window.getColValuesresponse.data[i].id + ", '" + window.getColValuesresponse.data[i].value + "'" + ');" class="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" value="' + window.getColValuesresponse.data[i].id + '" checked><label class="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '">' + window.getColValuesresponse.data[i].value.substring(0, searchedPos) + '<font color="#b6e026">' + window.getColValuesresponse.data[i].value.substr(searchedPos, searched.length) + '</font>' + window.getColValuesresponse.data[i].value.slice(searchedPos + searched.length) + '</label></div>';
							} else {
								fs_sb_options_list_content += '<div class="fs_sb_ol_option_block _' + window.getColValuesresponse.data[i].id + '" style="top: calc(5px + ' + (5 * searchedCount) + '%);"><input type="checkbox" onchange="window.fs_sb_ol_ob_checkbox_change(' + window.getColValuesresponse.data[i].id + ", '" + window.getColValuesresponse.data[i].value + "'" + ');" class="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" value="' + window.getColValuesresponse.data[i].id + '"><label class="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '">' + window.getColValuesresponse.data[i].value.substring(0, searchedPos) + '<font color="#b6e026">' + window.getColValuesresponse.data[i].value.substr(searchedPos, searched.length) + '</font>' + window.getColValuesresponse.data[i].value.slice(searchedPos + searched.length) + '</label></div>';
							}
						}
						if (window.cSD.fD[window.cSD.cF].vI.indexOf(window.getColValuesresponse.data[i].id) > -1) {
							selectedCount++;
							fs_sb_selected_options_content += '<div class="fs_sb_so_option_block _' + window.getColValuesresponse.data[i].id + '" style="top: calc(5px + ' + (5 * selectedCount) + '%);"><input type="checkbox" onchange="window.fs_sb_ol_ob_checkbox_change(' + window.getColValuesresponse.data[i].id + ", '" + window.getColValuesresponse.data[i].value + "'" + ');" class="fs_sb_so_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_so_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_so_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" value="' + window.getColValuesresponse.data[i].id + '" checked><label class="fs_sb_so_ob_label _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_so_ob_label _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_so_ob_label _' + window.getColValuesresponse.data[i].id + '">' + window.getColValuesresponse.data[i].value + '</label></div>';
						}
					}
				} else {
					for (let i = 0; i < window.getColValuesresponse.data.length; i++) {
						let searchedPos = window.getColValuesresponse.data[i].value.toUpperCase().indexOf(searched.toUpperCase());
						if (searchedPos != '-1') {
							searchedCount++;
							fs_sb_options_list_content += '<div class="fs_sb_ol_option_block _' + window.getColValuesresponse.data[i].id + '" style="top: calc(5px + ' + (5 * searchedCount) + '%);"><input type="checkbox" onchange="window.fs_sb_ol_ob_checkbox_change(' + window.getColValuesresponse.data[i].id + ", '" + window.getColValuesresponse.data[i].value + "'" + ');" class="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" value="' + window.getColValuesresponse.data[i].id + '"><label class="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '">' + window.getColValuesresponse.data[i].value.substring(0, searchedPos) + '<font color="#b6e026">' + window.getColValuesresponse.data[i].value.substr(searchedPos, searched.length) + '</font>' + window.getColValuesresponse.data[i].value.slice(searchedPos + searched.length) + '</label></div>';
						}
					}
				}
			}
			$('.fs_sb_options_list')[0].innerHTML = fs_sb_options_list_content;
			$('.fs_sb_selected_options')[0].innerHTML = fs_sb_selected_options_content;
			return false;
		}

		fs_sb_search_string.addEventListener('input', change_fs_sb_search_string);

		function change_fs_sb_search_string(e) {
			let searched = e.target.value;
			let fs_sb_options_list_content = '';
			if (searched == '') {
				if ((typeof(window.cSD.fD[window.cSD.cF]) != "undefined") && (window.cSD.fD[window.cSD.cF] !== null)) {
					for (let i = 0; i < window.getColValuesresponse.data.length; i++) {
						if (window.cSD.fD[window.cSD.cF].vI.indexOf(window.getColValuesresponse.data[i].id) > -1) {
							fs_sb_options_list_content += '<div class="fs_sb_ol_option_block _' + window.getColValuesresponse.data[i].id + '" style="top: calc(5px + ' + (5 * i) + '%)"><input type="checkbox" onchange="window.fs_sb_ol_ob_checkbox_change(' + window.getColValuesresponse.data[i].id + ", '" + window.getColValuesresponse.data[i].value + "'" + ');" class="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" value="' + window.getColValuesresponse.data[i].id + '" checked><label class="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '">' + window.getColValuesresponse.data[i].value + '</label></div>';
						} else {
							fs_sb_options_list_content += '<div class="fs_sb_ol_option_block _' + window.getColValuesresponse.data[i].id + '" style="top: calc(5px + ' + (5 * i) + '%);"><input type="checkbox" onchange="window.fs_sb_ol_ob_checkbox_change(' + window.getColValuesresponse.data[i].id + ", '" + window.getColValuesresponse.data[i].value + "'" + ');" class="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" value="' + window.getColValuesresponse.data[i].id + '"><label class="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '">' + window.getColValuesresponse.data[i].value + '</label></div>';
						}
					}
				} else {
					for (let i = 0; i < window.getColValuesresponse.data.length; i++) {
						fs_sb_options_list_content += '<div class="fs_sb_ol_option_block _' + window.getColValuesresponse.data[i].id + '" style="top: calc(5px + ' + (5 * i) + '%);"><input type="checkbox" onchange="window.fs_sb_ol_ob_checkbox_change(' + window.getColValuesresponse.data[i].id + ", '" + window.getColValuesresponse.data[i].value + "'" + ');" class="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" value="' + window.getColValuesresponse.data[i].id + '"><label class="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '">' + window.getColValuesresponse.data[i].value + '</label></div>';
					}
				}
			} else {
				let searchedCount = -1;
				if ((typeof(window.cSD.fD[window.cSD.cF]) != "undefined") && (window.cSD.fD[window.cSD.cF] !== null)) {
					for (let i = 0; i < window.getColValuesresponse.data.length; i++) {
						let searchedPos = window.getColValuesresponse.data[i].value.toUpperCase().indexOf(searched.toUpperCase());
						if (searchedPos != '-1') {
							searchedCount++;
							if (window.cSD.fD[window.cSD.cF].vI.indexOf(window.getColValuesresponse.data[i].id) > -1) {
								fs_sb_options_list_content += '<div class="fs_sb_ol_option_block _' + window.getColValuesresponse.data[i].id + '" style="top: calc(5px + ' + (5 * searchedCount) + '%)"><input type="checkbox" onchange="window.fs_sb_ol_ob_checkbox_change(' + window.getColValuesresponse.data[i].id + ", '" + window.getColValuesresponse.data[i].value + "'" + ');" class="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" value="' + window.getColValuesresponse.data[i].id + '" checked><label class="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '">' + window.getColValuesresponse.data[i].value.substring(0, searchedPos) + '<font color="#b6e026">' + window.getColValuesresponse.data[i].value.substr(searchedPos, searched.length) + '</font>' + window.getColValuesresponse.data[i].value.slice(searchedPos + searched.length) + '</label></div>';
							} else {
								fs_sb_options_list_content += '<div class="fs_sb_ol_option_block _' + window.getColValuesresponse.data[i].id + '" style="top: calc(5px + ' + (5 * searchedCount) + '%);"><input type="checkbox" onchange="window.fs_sb_ol_ob_checkbox_change(' + window.getColValuesresponse.data[i].id + ", '" + window.getColValuesresponse.data[i].value + "'" + ');" class="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" value="' + window.getColValuesresponse.data[i].id + '"><label class="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '">' + window.getColValuesresponse.data[i].value.substring(0, searchedPos) + '<font color="#b6e026">' + window.getColValuesresponse.data[i].value.substr(searchedPos, searched.length) + '</font>' + window.getColValuesresponse.data[i].value.slice(searchedPos + searched.length) + '</label></div>';
							}
						}
					}
				} else {
					for (let i = 0; i < window.getColValuesresponse.data.length; i++) {
						let searchedPos = window.getColValuesresponse.data[i].value.toUpperCase().indexOf(searched.toUpperCase());
						if (searchedPos != '-1') {
							searchedCount++;
							fs_sb_options_list_content += '<div class="fs_sb_ol_option_block _' + window.getColValuesresponse.data[i].id + '" style="top: calc(5px + ' + (5 * searchedCount) + '%);"><input type="checkbox" onchange="window.fs_sb_ol_ob_checkbox_change(' + window.getColValuesresponse.data[i].id + ", '" + window.getColValuesresponse.data[i].value + "'" + ');" class="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" value="' + window.getColValuesresponse.data[i].id + '"><label class="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '">' + window.getColValuesresponse.data[i].value.substring(0, searchedPos) + '<font color="#b6e026">' + window.getColValuesresponse.data[i].value.substr(searchedPos, searched.length) + '</font>' + window.getColValuesresponse.data[i].value.slice(searchedPos + searched.length) + '</label></div>';
						}
					}
				}
			}
			$('.fs_sb_options_list')[0].innerHTML = fs_sb_options_list_content;
			return false;
		}

		window.sorterSettingsDraw = function() {
			$.ajax({
				url: 'http://betp.website/wp-admin/admin-ajax.php',
				type: 'POST',
				data: {
					action: 'ajax',
					task: 'getColValues',
					column: window.cSD.sCD,
					params: window.cSD,
					filternum: -1,
					sdate: document.getElementById("ss_sdd_date_input_1").value,
					edate: document.getElementById("ss_sdd_date_input_2").value,
					dbase: window.DBase
				},
				beforeSend: function() {},
				success: function(response) {
					response = JSON.parse(response);
					let ss_select_content1 = '';
					let ss_select_content2 = '';
					if (response.hasOwnProperty('error')) {
						$().message_err('Ошибка при получении данных из БД: ' + response.error);
						return false;
					} else if (response.result > 0) {
			console.log('sorterSettingsDraw window.cSD.ssC: '+JSON.stringify(response));
						ss_select_content1 = '<ul id="sortable1" class="connectedSortable">';
						ss_select_content2 = '<ul id="sortable2" class="connectedSortable_1">';
						let tmp_vals = [];
						let new_ssC = window.cSD.ssC.map((i) => Number(i));
			console.log('sorterSettingsDraw window.cSD.ssC: '+JSON.stringify(new_ssC));
						window.cSD.ssC = new_ssC;
						for (let i = 0; i < response.data.length; i++) {
							if (window.cSD.ssC.includes(response.data[i].id)) {
								for (let j = 0; j < window.cSD.ssC.length; j++) {
									if (response.data[i].id == window.cSD.ssC[j]) {
										tmp_vals[j] = response.data[i].value;
									}
								}
							} else {
								ss_select_content1 += '<li class="ss_ms_case_row" ondblclick="window.ss_ms_case_row_dbl_click(' + response.data[i].id + ', 0);" value="' + response.data[i].id + '">' + response.data[i].value + '</li>';
							}
						}
						for (let i = 0; i < window.cSD.ssC.length; i++) {
							ss_select_content2 += '<li class="ss_ms_case_row" ondblclick="window.ss_ms_case_row_dbl_click(' + window.cSD.ssC[i] + ', 0);" value="' + window.cSD.ssC[i] + '">' + tmp_vals[i] + '</li>';
						}
						ss_select_content1 += '</ul>' + ss_select_content2 + '</ul>';
					}
			console.log('sorterSettingsDraw window.cSD.ssC: '+JSON.stringify(ss_select_content1));
			console.log('sorterSettingsDraw window.cSD.ssC: '+JSON.stringify(ss_select_content2));
					let sos_select_content = '';
					let options_names = ['Сортировать по порядку набора','Сортировать по суммам первых столбцов','Сортировать по суммам вторых столбцов','Сортировать по суммам третьих столбцов','Сортировать по общим суммам столбцов'];
					if ($('.ss_st_select').val() != 14) {
						if ((typeof(window.cSD.sos) != "undefined") && (window.cSD.sos !== null)) {
							for (let i = 0; i < 5; i++) {
								if (i === Number(window.cSD.sos)) {
									sos_select_content += '<option selected="selected" id="' + i + '" value="' + i + '">' + options_names[i] + '</option>';
								} else {
									sos_select_content += '<option id="' + i + '" value="' + i + '">' + options_names[i] + '</option>';
								}
							}
						} else {
							for (let i = 0; i < 5; i++) {
								sos_select_content += '<option id="' + i + '" value="' + i + '">' + options_names[i] + '</option>';
							}
						}
					} else {
						options_names = ['Тотал по всем','Тотал по выбранным'];
						if ((typeof(window.cSD.sos) != "undefined") && (window.cSD.sos !== null)) {
							for (let i = 0; i < 2; i++) {
								if (i === Number(window.cSD.sos)) {
									sos_select_content += '<option selected="selected" id="' + i + '" value="' + i + '">' + options_names[i] + '</option>';
								} else {
									sos_select_content += '<option id="' + i + '" value="' + i + '">' + options_names[i] + '</option>';
								}
							}
						} else {
							for (let i = 0; i < 2; i++) {
								sos_select_content += '<option id="' + i + '" value="' + i + '">' + options_names[i] + '</option>';
							}
						}
					}
					if (window.cSD.page8Flag) {
						$('.sorter_settings')[0].innerHTML = '<div class="ss_label"><p>Выберите значения для построения графика</p></div><div class="sos_title"><div class="sos_title_left">Перетащите необходимые элементы</div><div class="sos_title_right">сюда</div></div><div class="ss_multiselect">' + ss_select_content1 + '</div><input title="Добавить все" type="button" class="sortableButtonAddAll" name="sortableButtonAddAll" id="sortableButtonAddAll" value=">>" /><input type="button" class="sos_button_ok" name="sos_button_ok" id="sos_button_ok" value="Ок" /><input type="button" class="sos_button_exit_2" name="sos_button_exit_2" id="sos_button_exit_2" value="Отменить" />';
					} else {
						$('.sorter_settings')[0].innerHTML = '<div class="ss_label"><p>Выберите значения для построения графика</p></div><div class="sos_title"><div class="sos_title_left">Перетащите необходимые элементы</div><div class="sos_title_right">сюда</div></div><div class="ss_multiselect">' + ss_select_content1 + '</div><select class="sos_select" name="sos_select" id="sos_select">' + sos_select_content + '</select><input title="Добавить все" type="button" class="sortableButtonAddAll" name="sortableButtonAddAll" id="sortableButtonAddAll" value=">>" /><input type="button" class="sos_button_ok" name="sos_button_ok" id="sos_button_ok" value="Ок" /><input type="button" class="sos_button_exit_1" name="sos_button_exit_1" id="sos_button_exit_1" value="Отменить" />';
					}
					window.sortDisableSelection();
					$('.sorter_settings_fade').fadeIn(10);
				}
			});
		}

		window.ordererSettingsDraw = function() {
			$.ajax({
				url: 'http://betp.website/wp-admin/admin-ajax.php',
				type: 'POST',
				data: {
					action: 'ajax',
					task: 'getSorterValues',
					column: window.cSD.sI,
					params: window.cSD,
					filternum: -1,
					sdate: document.getElementById("ss_sdd_date_input_1").value,
					edate: document.getElementById("ss_sdd_date_input_2").value,
					dbase: window.DBase,
					mainCorp: window.Main
				},
				beforeSend: function() {},
				success: function(response) {
					response = JSON.parse(response);
					let ss_select_content1 = '';
					let ss_select_content2 = '';
					if (response.hasOwnProperty('error')) {
						$().message_err('Ошибка при получении данных из БД: ' + response.error);
						return false;
					} else if (response.result > 0) {
						ss_select_content1 = '<ul id="orderable1" class="connectedOrderable">';
						ss_select_content2 = '<ul id="orderable2" class="connectedOrderable_1">';
						let tmp_vals = [];
						let new_osC = window.cSD.osC.map((i) => Number(i));
						window.cSD.osC = new_osC;
						for (let i = 0; i < response.data.length; i++) {
							if (window.cSD.osC.includes(response.data[i].id)) {
								for (let j = 0; j < window.cSD.osC.length; j++) {
									if (response.data[i].id == window.cSD.osC[j]) {
										tmp_vals[j] = response.data[i].value;
									}
								}
							} else {
								ss_select_content1 += '<li class="ss_ms_case_row" ondblclick="window.ss_ms_case_row_dbl_click(' + response.data[i].id + ', 1);" value="' + response.data[i].id + '">' + response.data[i].value + '</li>';
							}
						}
						for (let i = 0; i < window.cSD.osC.length; i++) {
							ss_select_content2 += '<li class="ss_ms_case_row" ondblclick="window.ss_ms_case_row_dbl_click(' + window.cSD.osC[i] + ', 1);" value="' + window.cSD.osC[i] + '">' + tmp_vals[i] + '</li>';
						}
						ss_select_content1 += '</ul>' + ss_select_content2 + '</ul>';
					}
					let oos_select_content = '';
					let options_names = ['Other не отображать','Other отображать'];
					if ((typeof(window.cSD.oos) != "undefined") && (window.cSD.oos !== null)) {
						for (let i = 0; i < 2; i++) {
							if (i === Number(window.cSD.oos)) {
								oos_select_content += '<option selected="selected" id="' + i + '" value="' + i + '">' + options_names[i] + '</option>';
							} else {
								oos_select_content += '<option id="' + i + '" value="' + i + '">' + options_names[i] + '</option>';
							}
						}
					} else {
						for (let i = 0; i < 2; i++) {
							oos_select_content += '<option id="' + i + '" value="' + i + '">' + options_names[i] + '</option>';
						}
					}
					$('.orderer_settings')[0].innerHTML = '<div class="ss_label"><p>Выберите значения для построения графика</p></div><div class="sos_title"><div class="sos_title_left">Перетащите необходимые элементы</div><div class="sos_title_right">сюда</div></div><div class="os_multiselect">' + ss_select_content1 + '</div><select class="oos_select" name="oos_select" id="oos_select">' + oos_select_content + '</select><input title="Добавить все" type="button" class="orderableButtonAddAll" name="orderableButtonAddAll" id="orderableButtonAddAll" value=">>" /><input type="button" class="ors_button_ok" name="ors_button_ok" id="ors_button_ok" value="Ок" /><input type="button" class="ors_button_exit" name="ors_button_exit" id="ors_button_exit" value="Отменить" />';
					window.sortDisableSelection();
					$('.orderer_settings_fade').fadeIn(10);
				}
			});
		}

		window.filtersListDraw = function() {
			let textvalue = '';
			for (let i = 0; i < window.cSD.fD.length; i++) {
				let filterValues = [];
				for (let j = 0; j < window.cSD.fD[i].vI.length; j++) {
					filterValues.push(window.cSD.fD[i].vV['i' + window.cSD.fD[i].vI[j]]);
				}
				textvalue += '<div class="ss_fb_fl_filter_row _' + i + '" style="top: ' + (10 * i) + '%;"><div class="ss_fb_fl_fr_name _' + i + '">' + window.cSD.fD[i].cV + '</div><div class="ss_fb_fl_fr_value _' + i + '">' + filterValues.join(', ') + '</div><input type="button" class="ss_fb_fl_fr_edit _' + i + '" value="✎ Изменить"/><input type="button" class="ss_fb_fl_fr_delete _' + i + '" value="🛇 Удалить"/></div>';
			}
			if (textvalue === '') {
				textvalue = '<p>Плашки фильтров добавляются/удаляются динамически</p>';
			}
			$('.ss_fb_filters_list')[0].innerHTML = textvalue;
		}

		window.diapasonsListDraw = function() {
			let textvalue = '';
			for (let i = 0; i < window.cSD.dD.length; i++) {
				textvalue += '<div class="ds_db_dl_diapason_row _' + i + '" style="top: ' + (10 * i) + '%;"><div class="ds_db_dl_dr_min_val _' + i + '">' + window.cSD.dD[i].sS.toLocaleString() + '</div><div class="ds_db_dl_dr_max_val _' + i + '">' + window.cSD.dD[i].eS.toLocaleString() + '</div><div class="ds_db_dl_dr_color _' + i + '" style="background-color: ' + window.cSD.dD[i].clr + ';"></div><input type="button" class="ds_db_dl_dr_edit _' + i + '" value="✎ Изменить"/><input type="button" class="ds_db_dl_dr_delete _' + i + '" value="🛇 Удалить"/></div>';
			}
			if (textvalue === '') {
				textvalue = '<p>Плашки диапазонов добавляются/удаляются динамически</p>';
			}
			$('.ds_db_diapasons_list')[0].innerHTML = textvalue;
		}

		function hexToBase64(str) {
			return btoa(String.fromCharCode.apply(null, str.replace(/\r|\n/g, "").replace(/([\da-fA-F]{2}) ?/g, "0x$1 ").replace(/ +$/, "").split(" ")));
		}

		window.thumbnailDraw = function() {
			var deferreds = [];
			var deferred = $.Deferred();
			
			var deferred2 = $.Deferred();
			
			deferreds.push(deferred.promise());

			deferreds_global2.push(deferred2.promise());
			
			var thmb;
			var sel = '#sp_sb_cs_slide_body';

			html2canvas(document.querySelector(sel), {
				//scale:window.devicePixelRatio*0.17,
				scale:window.devicePixelRatio*0.2,
				backgroundColor: 0, 
				width: $(sel).innerWidth(),
				imageTimeout: 0,
				//x: 0,
				//y: 0, 
				scrollY: -window.scrollY
				}).then(canvas =>{
				var context = canvas.getContext('2d');
				context.webkitImageSmoothingEnabled = false;
				context.mozImageSmoothingEnabled = false;
				context.imageSmoothingEnabled = false;
				
				context.scale(0.1,0.1);
				
				var img_ = canvas.toDataURL('image/png');
				thmb = changeDpiDataUrl(img_, 1200);
				//window.location.href=thmb;
				//thmb = stepped_scale(img_, 50, 0.5);
				//thmb = resizeImage(thmb, 0.5);
								
				deferred.resolve();
			});
			
			$.when.apply($, deferreds, window.sD).then(function () {
				window.sD[window.cP].iM = thmb;
				deferred2.resolve();
			});
		}

		window.slidesListDraw = function() {
			let textvalue = '';
			for (let i = 0; i < window.sD.length; i++) {
			var thmb;
				if(typeof(window.sD[i].iM) != "undefined") {
					thmb = window.sD[i].iM;
				} else { 
					thmb = '/wp-content/themes/twentysixteen/img/spravka.png';
				}
		
				let name = window.sD[i].sT;
				let nameshort = (name.length >= 35) ? name.substring(0,35) : name;
				
				textvalue += '<li class="ui-state-default" id="'+i+'"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span> <div class="sm_sl_slide_row1 _' + i + '"><div class="sm_sl_sr_name1" title="' + name + '">' + nameshort + '</div><div class="sm_sl_sr"><img title="Перейти к ' + name + '" class="sm_sl_sr_img" src="' + thmb + '"></div><img class="sm_sl_sr_img_edit" src="/wp-content/themes/twentysixteen/img/pencil.png" title="✎ Изменить" /><img class="sm_sl_sr_img_delete" src="/wp-content/themes/twentysixteen/img/del.png" title="🛇 Удалить" /></div></li>';
				  
			}
			if (textvalue === '') {
				textvalue = '<p>Плашки слайдов добавляются/удаляются динамически</p>';
			}
			$('#sortable_ppt')[0].innerHTML = textvalue;
		}

		window.currentSlideDraw = function() {
			$('.sp_sb_cs_up_status')[0].innerHTML = '<p>Слайды: ' + (window.cP>=0?window.cP+1:0) + ' из '+window.sC+'</p>';
			if (window.sC > 0) {
				$('#uploader, #uploader_cur').prop('disabled', false);
				$('#del_slide').prop('disabled', false);
				$('#edit_slide').prop('disabled', false);
				$('#copy_slide').prop('disabled', false);
				$('#new_tmpl').prop('disabled', false);
			} else {
				$('#uploader, #uploader_cur').prop('disabled', true);
				$('#del_slide').prop('disabled', true);
				$('#edit_slide').prop('disabled', true);
				$('#copy_slide').prop('disabled', true);
				$('#new_tmpl').prop('disabled', true);
			}
			$('.sp_sb_cs_sb_slide_title')[0].innerHTML = '';
			$('.sp_sb_cs_sb_graph_block')[0].innerHTML = '';
			$('.sp_sb_cs_sb_data_block')[0].innerHTML = '';

			var res_coef = 2;
			var BrandsTypes = ['14', '60'];

			var ww=window.screen.width;
			var MainFontSize = 13;
			var InFontSize = 14;
			var decrFont = 4;
				if(ww <= 1440) {
					MainFontSize = 11;
					InFontSize = 10;
					decrFont = 1.5;
				}
			
			if (window.cP > -1) {
				$('.sp_sb_cs_sb_slide_title')[0].innerHTML = window.sD[window.cP].sT;
				
				//console.log('window.sD[window.cP].gT: '+Number(window.sD[window.cP].gT));
				//console.log('window.sD[window.cP]: '+JSON.stringify(window.sD[window.cP]));

				var deferred = $.Deferred();
				
				$.ajax({
					url: 'http://betp.website/wp-admin/admin-ajax.php',
					type: 'POST',
					data: {
						action: 'ajax',
						task: 'getSlideData',
						params: window.sD[window.cP],
						sdate: document.getElementById("ss_sdd_date_input_1") != null ? document.getElementById("ss_sdd_date_input_1").value : '2021-01-01',
						edate: document.getElementById("ss_sdd_date_input_2") != null ? document.getElementById("ss_sdd_date_input_2").value : '2021-12-31',
						page: Number(window.sD[window.cP].gT),
						dbase: window.DBase,
						mainCorp: window.Main,
						//async : false
					},
					beforeSend: function() {
						window.deferreds_global.push(deferred.promise());
					},
					success: function(response) {
						response = JSON.parse(response);
						if (response.hasOwnProperty('error')) {
							$().message_err('Ошибка при получении данных из БД: ' + response.error);
							return false;
						} else if (response.result > 0) {
							$('.sp_sb_cs_sb_down')[0].innerHTML = window.controlDate + ' ' + response.slide_legend;
							if ((window.sD[window.cP].gT === '0') || (window.sD[window.cP].gT === '1') || (window.sD[window.cP].gT === '2')) {
								
								let colw;
								let colwidth;
								let adj;
								if (Number(window.cSD.iI) === 1) {
									colw = 4.83;
									colwidth = colw;
									adj= 0.05;
								} else if (Number(window.cSD.iI) === 2) {	// flaii
									colw = 8;
									colwidth = (100-(colw*4+2+11)) / (response.data.lUs.length - 5);
									adj = 0;
								} else {
									colw = 8;
									colwidth = (100-(colw*4+2+11)) / (response.data.lUs.length - 5);
									adj = 0;
								}
								//console.log('colw: '+colw);
								
								let textvalue = '';
								let textvalueUp = '';
								$('div.sp_sb_cs_sb_user_text')[0].innerHTML = '';
								
								textvalue = '<b><div class="sp_sb_cs_db_data_col _0" style="left: 0%; width: '+colw+'%; max-width: '+colw+'%; min-width: '+colw+'%">'+
								'<p class="paragraph_1">' + response.data.lUs[0] + '<br><font color="' + ((response.data.lCs[0] === "Green") ? '008000">+' : 'c00000">') + 
								response.data.lDs[0] + '%</font></p></div><div class="sp_sb_cs_db_data_col _1" style="left: '+colw+'%; width: '+colw+'%; max-width: '+colw+'%; min-width: '+colw+'%">'+
								'<p class="paragraph_1">' + response.data.lUs[1] + '<br><font color="' + ((response.data.lCs[1] === "Green") ? '008000">+' : 'c00000">') + 
								response.data.lDs[1] + '%</font></p></div><div class="sp_sb_cs_db_data_col _2" style="left: '+(colw*2+2)+'%; width: '+colw+'%; max-width: '+colw+'%; min-width: '+colw+'%">'+
								'<p class="paragraph_1">' + response.data.lUs[2] + '<br><font color="' + ((response.data.lCs[2] === "Green") ? '008000">+' : 'c00000">') + 
								response.data.lDs[2] + '%</font></p></div><div class="sp_sb_cs_db_data_col _3" style="left: '+(colw*3+2)+'%; width: '+colw+'%; max-width: '+colw+'%; min-width: '+colw+'%">'+
								'<p class="paragraph_1">' + response.data.lUs[3] + '<br><font color="' + ((response.data.lCs[3] === "Green") ? '008000">+' : 'c00000">') + 
								response.data.lDs[3] + '%</font></p></div><div class="sp_sb_cs_db_data_col _4" style="left: '+(colw*4+2)+'%; width: 11%; max-width: 11%; min-width: 11%">'+
								'<p class="paragraph_1">' + response.data.lUs[4] + '<br>' + response.data.lDs[4] + '</p></div>';
								
								//console.log('len: '+response.data.lUs.length);
								//console.log('colwidth: '+colwidth);

								for (let i = 5; i < response.data.lUs.length; i++) {
									textvalue += '<div class="sp_sb_cs_db_data_col _' + i + '" style="left: ' + ((colw*4+2+11) + colwidth * (i - 5) - adj*(i - 5)) + '%; width: ' + colwidth + '%; max-width: ' + colwidth + '%; min-width: ' + colwidth + '%">'+
									'<p class="paragraph_1">' + response.data.lUs[i] + '<br>'+
									'<font color="' + ((response.data.lCs[i] === "Green") ? '008000">+' : 'c00000">') + 
										response.data.lDs[i] + '%'+
								'</font>'+
								'</p></div>';
								}
								//console.log(textvalue);
								$('.sp_sb_cs_sb_data_block')[0].innerHTML = textvalue + '</b>';

								if(ww <= 1440) {
									$('div.sp_sb_cs_sb_data_block').css({'font-size':'0.87em'});
								}
								
								textvalue= '';
								//let colors = ['rgba(0, 114, 218, 1)', 'rgba(0, 156, 73, 1)', 'rgba(240, 66, 121, 1)', 'rgba(255, 127, 0, 1)', 'rgba(167, 122, 215, 1)', 'rgba(255, 255, 0, 1)', 'rgba(255, 000, 128, 0.4)', 'rgba(255, 99, 132, 0.9)', 'rgba(197, 255, 10, 1)', 'rgba(127, 127, 127, 1)'];
								let colors = ['rgba(0, 114, 218, 1)', 'rgba(0, 156, 73, 1)', 'rgba(240, 66, 121, 1)', 'rgba(255, 127, 0, 1)', 'rgba(167, 122, 215, 1)', 'rgba(255, 255, 0, 1)', 'rgba(153, 0, 102, 1)', 'rgba(48, 213, 200, 1)', 'rgba(188, 152, 126, 1)', 'rgba(140, 203, 94, 1)', 'rgba(176, 0, 0, 1)', 'rgba(0, 191, 255, 1)', 'rgba(255, 36, 0, 1)', 'rgba(153, 255, 153, 1)', 'rgba(253, 234, 168, 1)'];

								let names = response.data.sNs;
								if(BrandsTypes.includes(window.cSD.sI)) {
									for (let i = 0; i < names.length; i++) {
										let brand = names[i].trim();
										if (brand in window.BrandsData) {
											colors[i] = window.BrandsData[brand].color;
											names[i] = window.BrandsData[brand].name;
											//console.log('L: '+i+': '+names[i]+' | '+colors[i]);
										}
									}
								}
								if (window.cSD.sI == 0) {
									for (let i = 0; i < names.length; i++) {
										colors[i] = window.cSD.dD[i].clr;
										names[i] = window.cSD.dD[i].sS.toLocaleString() + ' - ' + window.cSD.dD[i].eS.toLocaleString();
									}
								}
								textvalue = '<div class="sp_sb_cs_gb_graph_col_0"><canvas id="gr_canvas_0" style="width: 100%; height: 100%"></canvas></div>' + 
									'<div class="sp_sb_cs_gb_graph_col_1"></div>' + 
									'<div class="sp_sb_cs_gb_graph_col_2"><canvas id="gr_canvas_1" style="width: 100%; height: 100%"></canvas></div>' + 
									'<div class="sp_sb_cs_gb_graph_col_3">' + 
										'<div class="sp_sb_cs_gb_gc3_series_list">' + 
											'<div class="sp_sb_cs_gb_gc3_sl_in">';
								for (let i = 0; i < response.data.sNs.length; i++) {
									textvalue += '<div class="sp_sb_cs_gb_gc3_sl_series_row _' + i + '" style="top: ' + (4 + 14.28 * i) + '%;"><div class="sp_sb_cs_gb_gc3_sl_sr_color_box" style="background-color: ' + colors[i] + ';"></div><div class="sp_sb_cs_gb_gc3_sl_sr_space"></div><p class="sp_sb_cs_gb_gc3_sl_sr_item_name">' + names[i] + '</p></div>';
								}
								$('.sp_sb_cs_sb_graph_block')[0].innerHTML = textvalue + '</div></div></div><div class="sp_sb_cs_gb_graph_col_4"><canvas id="gr_canvas_2" style="width: 100%; height: 100%"></canvas></div>';

								Chart.defaults.global.defaultFontSize = MainFontSize;
								Chart.defaults.global.defaultFontStyle = 'bold';
								Chart.defaults.global.defaultFontColor = 'black';
								Chart.defaults.global.defaultFontFamily = 'Verdana';
								Chart.defaults.global.elements.rectangle.borderColor = 'rgba(25, 25, 25, 1)';
								Chart.defaults.global.elements.rectangle.borderWidth = 0.7;

								//Chart.defaults.global.elements.rectangle.backgroundColor = 'rgba(0, 0 0, 0)';
								//Chart.defaults.global.defaultColor = 'rgba(0, 0 0, 0)';
								
								if(Number(window.cSD.iI) === 1) {
									response.data.lMs.forEach(function(e, i, a) {
										   a[i] = e.replace(/\-/,'-\n');
									});
									response.data.lYs.forEach(function(e, i, a) {
										   a[i] = e.replace(/\-/,'-\n');
									});
								}
								//console.log('response.data.lMs: '+response.data.lMs);
								//console.log('response.data.lYs: '+response.data.lYs);
								
								let data_0 = {
									labels: response.data.lMs,
									datasets: [],
								};
								let data_1 = {
									labels: response.data.lYs,
									datasets: [],
								};
								let data_2 = {
									labels: [],
									datasets: [],
								};
								for (let i = 0; i < response.data.rs[0].iD.length; i++) {
									if(Number(window.cSD.iI) === 1) {
										response.data.rs[0].iD[i].idn = '\n'+response.data.rs[0].iD[i].idn
									}
									data_2.labels.unshift(response.data.rs[0].iD[i].idn);
								}
								let ctx_0 = document.getElementById('gr_canvas_0').getContext('2d');
								let ctx_1 = document.getElementById('gr_canvas_1').getContext('2d');
								let ctx_2 = document.getElementById('gr_canvas_2').getContext('2d');


								let legend_col_height = getPercentHeight($('div.sp_sb_cs_gb_gc3_sl_in'));
								console.log('legend col height: '+legend_col_height);

									if(response.data.sNs.length <=7) {
										let col_height = 30;
										let col_top_aj = 10;
										if(ww<1440) {
											col_height = 40;
											col_top_aj = 12;
										}
										//console.log('legend col_height: '+col_height);
										$('div.sp_sb_cs_gb_gc3_sl_in').css({'min-height':col_height+'%'});
										$('div.sp_sb_cs_gb_gc3_sl_in').css({'max-height':col_height+'%'});
										$('div.sp_sb_cs_gb_gc3_sl_in').css({height:col_height+'%'});
										$('div.sp_sb_cs_gb_gc3_sl_in').css({'top':'calc(30% + 2.2vh)'});

										let col_top = 0;
										$("div.sp_sb_cs_gb_gc3_sl_series_row").each(function(index){
											let legend_col_top = getPercentTop($(this));
											//console.log('legend top: '+index+': '+legend_col_top);
											//$(this).css({'top':+(legend_col_top-4)+'%'});
											col_top = (index > 0) ? col_top+col_top_aj : col_top+col_top_aj/2;
											$(this).css({'top':+col_top+'%'});
											$(this).css({'height':'7%'});
											$(this).css({'min-height':'7%'});
											$(this).css({'max-height':'7%'});
										});
									} else
									if(response.data.sNs.length > 7 && response.data.sNs.length < 15) {
										let col_height_ajust = (response.data.sNs.length)*2.5;
										let col_top_aj = 5.8;
										let top_margin = '15% + 2.0vh';
										if(ww<1440) {
											col_top_aj = 7;
											col_height_ajust = (response.data.sNs.length)*2.8;
											top_margin = '7% + 1.0vh';
										}
										//console.log('legend col_height_ajust: '+col_height_ajust);
										$('div.sp_sb_cs_gb_gc3_sl_in').css({'min-height':(legend_col_height+col_height_ajust)+'%'});
										$('div.sp_sb_cs_gb_gc3_sl_in').css({'max-height':(legend_col_height+col_height_ajust)+'%'});
										$('div.sp_sb_cs_gb_gc3_sl_in').css({height:(legend_col_height+col_height_ajust)+'%'});
										$('div.sp_sb_cs_gb_gc3_sl_in').css({'top':'calc('+top_margin+')'});

										let col_top = 0;
										$("div.sp_sb_cs_gb_gc3_sl_series_row").each(function(index){
											let legend_col_top = getPercentTop($(this));
											//console.log('legend top: '+index+': '+legend_col_top);
											//$(this).css({'top':+(legend_col_top-4)+'%'});
											col_top = (index > 0) ? col_top+col_top_aj : col_top+col_top_aj/2;
											$(this).css({'top':+col_top+'%'});
											});
									} else
									if(response.data.sNs.length >= 15 && response.data.sNs.length < 18) {
										let col_height = (response.data.sNs.length)*4.5;
										let prc_ajs = 30 - response.data.sNs.length;
										let col_top_aj = 5;
										//let top_margin = '15% + 2.0vh';
										if(ww<1440) {
											col_height = (response.data.sNs.length)*6;
											prc_ajs = 25 - response.data.sNs.length;
											col_top_aj = 5.1;
										}
										console.log('legend col_height 15: '+col_height);
										$('div.sp_sb_cs_gb_gc3_sl_in').css({'min-height':col_height+'%'});
										$('div.sp_sb_cs_gb_gc3_sl_in').css({'max-height':col_height+'%'});
										$('div.sp_sb_cs_gb_gc3_sl_in').css({height:col_height+'%'});
										$('div.sp_sb_cs_gb_gc3_sl_in').css({'top':'calc('+prc_ajs+'% + 1.3vh)'});

										let col_top = 0;
										$("div.sp_sb_cs_gb_gc3_sl_series_row").each(function(index){
											let legend_col_top = getPercentTop($(this));
											//console.log('legend top: '+index+': '+legend_col_top);
											//$(this).css({'top':+(legend_col_top-4)+'%'});
											col_top = (index > 0) ? col_top+col_top_aj : col_top+col_top_aj/2;
											$(this).css({'top':+col_top+'%'});
										});
									} else
									if(response.data.sNs.length >= 18) {
										let col_height = (response.data.sNs.length)*3.8;
										let prc_ajs = 30 - response.data.sNs.length*0.8;
										let col_top_aj = 4.6;
										if(ww<1440) {
											col_height = (response.data.sNs.length)*5.2;
											prc_ajs = 15 - response.data.sNs.length*0.8;
											col_top_aj = 4.7;
										}
										//console.log('legend col_height 20: '+col_height);
										$('div.sp_sb_cs_gb_gc3_sl_in').css({'min-height':col_height+'%'});
										$('div.sp_sb_cs_gb_gc3_sl_in').css({'max-height':col_height+'%'});
										$('div.sp_sb_cs_gb_gc3_sl_in').css({height:col_height+'%'});
										$('div.sp_sb_cs_gb_gc3_sl_in').css({'top':'calc('+prc_ajs+'% + 0.7vh)'});

										let col_top = 0;
										$("div.sp_sb_cs_gb_gc3_sl_series_row").each(function(index){
											let legend_col_top = getPercentTop($(this));
											//console.log('legend top: '+index+': '+legend_col_top);
											//$(this).css({'top':+(legend_col_top-4)+'%'});
											col_top = (index > 0) ? col_top+col_top_aj : col_top+col_top_aj/2;
											$(this).css({'top':+col_top+'%'});
										});
									}
								
								
								if (window.sD[window.cP].gT === '0') {
									for (let i = 0; i < response.data.sNs.length; i++) {
										data_0.datasets.unshift({label: response.data.sNs[i], backgroundColor: colors[i], data: [response.data.rs[i].mtmpp, response.data.rs[i].mtmcp], tooltipsValues: [response.data.rs[i].mtmpv, response.data.rs[i].mtmcv]});
										data_1.datasets.unshift({label: response.data.sNs[i], backgroundColor: colors[i], data: [response.data.rs[i].ytdpp, response.data.rs[i].ytdcp], tooltipsValues: [response.data.rs[i].ytdpv, response.data.rs[i].ytdcv]});
										data_2.datasets.unshift({label: response.data.sNs[i], backgroundColor: colors[i], data: [], tooltipsValues: []});
										for (let j = 0; j < response.data.rs[i].iD.length; j++) {
											data_2.datasets[0].data.unshift(response.data.rs[i].iD[j].idp);
											data_2.datasets[0].tooltipsValues.unshift(response.data.rs[i].iD[j].idv);
										}
									}
									
									if (Number(window.cSD.iI) === 1) {
										$('div.sp_sb_cs_gb_graph_col_0').css({'min-width':'9.66%'});
										$('div.sp_sb_cs_gb_graph_col_0').css({width:'9.66%'});

										$('div.sp_sb_cs_gb_graph_col_1').css({left:'9.66%'});

										$('div.sp_sb_cs_gb_graph_col_2').css({left:'11.66%'});
										$('div.sp_sb_cs_gb_graph_col_2').css({'min-width':'9.66%'});
										$('div.sp_sb_cs_gb_graph_col_2').css({width:'9.66%'});

										$('div.sp_sb_cs_gb_graph_col_3').css({left:'21.32%'});

										$('div.sp_sb_cs_gb_graph_col_4').css({left:'32.32%'});
										$('div.sp_sb_cs_gb_graph_col_4').css({'min-width':'67.62%'});
										$('div.sp_sb_cs_gb_graph_col_4').css({'max-width':'67.62%'});
										$('div.sp_sb_cs_gb_graph_col_4').css({width:'67.62%'});
									} 
									
									window.chart_0 = new Chart(ctx_0, {
										type: 'bar',
										data: data_0,
										options: {
											devicePixelRatio: window.devicePixelRatio*res_coef,
											hover: {
												mode: null,
												events: [],
											},
											legend: {
												display: false,
											},
											tooltips: {
												mode: 'index',
												backgroundColor: 'rgba(255, 255, 255, 0.75)',
												titleFontSize: InFontSize,
												titleFontColor: 'rgba(0, 0, 0, 1)',
												bodyFontSize: InFontSize,
												bodyFontColor: 'rgba(0, 0, 0, 1)',
												bodyFontStyle: 'normal',
												intersect: false,
												callbacks: {
													label: function(tooltipItem, data) {
														return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].tooltipsValues[tooltipItem.index];
													},
												},
											},
											responsive: true,
											scales: {
												xAxes: [{
													barPercentage: 1.0,
		   											categoryPercentage: 0.9,
													gridLineHeight: 20,
													gridLines: {
														display: false,
														drawBorder: false
													},
													stacked: true,
													position: 'top',
												}],
												yAxes: [{
													display: false,
													gridLines: {
														display: false,
														drawBorder: false
													},
													stacked: true,
													ticks: {
														max: 100,
														display: false,
													},
												}],
											},
											plugins: {												
												datalabels: {
													//anchor: 'end',
													//align: 'top',
													textAlign: 'center',
													color: function(context) {
															  var index = context.dataIndex;
															  var color = context.dataset.backgroundColor;
															  var result_rgb = rgbaToRgb(color);
															  var cc = contrastingColor(result_rgb);
															  return '#'+cc;
															},
													font: function(context) {
															  //var avgSize = Math.round((context.chart.height + context.chart.width) / 2);
															  //var size = Math.round(avgSize / 32);
															  var width = context.chart.width;
																//console.log('ChartWidth: '+width);
															  var index = context.dataIndex;
															  var value = context.dataset.data[index];
															  var size;
																switch(true) {
																	case value < 5 && value > 3: size = InFontSize; break;
																	case value <= 3: size = InFontSize - decrFont; break;
																	default: size = InFontSize+6;
																}
															 //console.log('value: '+value+' | size: '+size+' | weight: '+wgt);
															  return {
																 style:	(value < 5) ? 'normal' : 'bold',
																 "size": size
															};
														  },
													formatter: function(value, ctx_0) {
														if (Number(value) > 1.99) {
															if ((Number(value) * 10) % 10) {
																return value;
															} else {
																return value + '.0';
															}
														} else {
															return '';
														}
													},
												},
											},
										},
										plugins: [{
											  beforeInit: function(chart) {
												  chart.data.labels.forEach(function(e, i, a) {
													  if (/\n/.test(e)) {
														  if(Number(window.cSD.iI) === 1) {
															  a[i] = e.split(/\n/);
														  } else {
															a[i] = e.replace(/\n/, '');
														  }
													  }
												  });
											  }
										   }]
									});
									window.chart_1 = new Chart(ctx_1, {
										type: 'bar',
										data: data_1,
										options: {
											devicePixelRatio: window.devicePixelRatio*res_coef,
											hover: {
												mode: null,
												events: [],
											},
											legend: {
												display: false,
											},
											tooltips: {
												mode: 'index',
												backgroundColor: 'rgba(255, 255, 255, 0.75)',
												titleFontSize: InFontSize,
												titleFontColor: 'rgba(0, 0, 0, 1)',
												bodyFontSize: InFontSize,
												bodyFontColor: 'rgba(0, 0, 0, 1)',
												bodyFontStyle: 'normal',
												intersect: false,
												callbacks: {
													label: function(tooltipItem, data) {
														return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].tooltipsValues[tooltipItem.index];
													},
												},
											},
											responsive: true,
											scales: {
												xAxes: [{
													barPercentage: 1.0,
		   											categoryPercentage: 0.9,
													gridLineHeight: 20,
													gridLines: {
														display: false,
														drawBorder: false
													},
													stacked: true,
													position: 'top',
													//display:  (Number(window.cSD.iI) === 1)?false:true
												}],
												yAxes: [{
													display: false,
													gridLines: {
														display: false,
														drawBorder: false
													},
													stacked: true,
													ticks: {
														max: 100,
														display: false,
													},
												}],
											},
											plugins: {
												datalabels: {
													textAlign: 'center',
													color: function(context) {
															  var index = context.dataIndex;
															  var color = context.dataset.backgroundColor;
															  var result_rgb = rgbaToRgb(color);
															  var cc = contrastingColor(result_rgb);
															  return '#'+cc;
															},
													font: function(context) {
															  //var avgSize = Math.round((context.chart.height + context.chart.width) / 2);
															  //var size = Math.round(avgSize / 32);
															  var width = context.chart.width;
																//console.log('ChartWidth: '+width);
															  var index = context.dataIndex;
															  var value = context.dataset.data[index];
															  var size;
																switch(true) {
																	case value < 5 && value > 3: size = InFontSize; break;
																	case value <= 3: size = InFontSize - decrFont; break;
																	default: size = InFontSize+6;
																}
															 //console.log('value: '+value+' | size: '+size+' | weight: '+wgt);
															  return {
																 style:	(value < 5) ? 'normal' : 'bold',
																 "size": size
															};
														  },
													formatter: function(value, ctx_1) {
														if (Number(value) > 1.99) {
															if ((Number(value) * 10) % 10) {
																return value;
															} else {
																return value + '.0';
															}
														} else {
															return '';
														}
													},
												},
											},
										},
										plugins: [{
											  beforeInit: function(chart) {
												  chart.data.labels.forEach(function(e, i, a) {
													  if (/\n/.test(e)) {
														  if(Number(window.cSD.iI) === 1) {
															  a[i] = e.split(/\n/);
														  } else {
															a[i] = e.replace(/\n/, '');
														  }
													  }
												  });
											  }
										   }]
									});
									window.chart_2 = new Chart(ctx_2, {
										type: 'bar',
										data: data_2,
										options: {
											devicePixelRatio: window.devicePixelRatio*res_coef,
											hover: {
												mode: null,
												events: [],
											},
											legend: {
												display: false,
											},
											tooltips: {
												mode: 'index',
												backgroundColor: 'rgba(255, 255, 255, 0.75)',
												titleFontSize: InFontSize,
												titleFontColor: 'rgba(0, 0, 0, 1)',
												bodyFontSize: InFontSize,
												bodyFontColor: 'rgba(0, 0, 0, 1)',
												bodyFontStyle: 'normal',
												intersect: false,
												callbacks: {
													label: function(tooltipItem, data) {
														return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].tooltipsValues[tooltipItem.index];
													},
												},
											},
											responsive: true,
											scales: {
												xAxes: [{
													barPercentage: 1.0,
		   											categoryPercentage: 0.9,
													gridLineHeight: 20,
													gridLines: {
														display: false,
														drawBorder: false
													},
													stacked: true,
													position: 'top'
												}],
												yAxes: [{
													display: false,
													gridLines: {
														display: false,
														drawBorder: false
													},
													stacked: true,
													ticks: {
														max: 100,
														display: false,
													},
												}],
											},
											plugins: {
												datalabels: {
													textAlign: 'center',
													color: function(context) {
															  var index = context.dataIndex;
															  var color = context.dataset.backgroundColor;
															  var result_rgb = rgbaToRgb(color);
															  var cc = contrastingColor(result_rgb);
															  return '#'+cc;
															},
													font: function(context) {
															  //var avgSize = Math.round((context.chart.height + context.chart.width) / 2);
															  //var size = Math.round(avgSize / 32);
															  var width = context.chart.width;
																//console.log('ChartWidth: '+width);
															  var index = context.dataIndex;
															  var value = context.dataset.data[index];
															  var size;
																switch(true) {
																	case value < 5 && value > 3: size = InFontSize; break;
																	case value <= 3: size = InFontSize - decrFont; break;
																	default: size = InFontSize+6;
																}
															 //console.log('value: '+value+' | size: '+size+' | weight: '+wgt);
															  return {
																 style:	(value < 5) ? 'normal' : 'bold',
																 "size": size
															};
														  },
													formatter: function(value, ctx_2) {
														if (Number(value) > 1.99) {
															if ((Number(value) * 10) % 10) {
																return value;
															} else {
																return value + '.0';
															}
														} else {
															return '';
														}
													},
												},
											},
										},
										plugins: [{
											  beforeInit: function(chart) {
												  chart.data.labels.forEach(function(e, i, a) {
													  if (/\n/.test(e)) {
														  if(Number(window.cSD.iI) === 1) {
															  a[i] = e.split(/\n/);
														  } else {
															a[i] = e.replace(/\n/, '');
														  }
													  }
												  });
											  }
										   }]
									});
								} else if (window.sD[window.cP].gT === '1') {

								//console.log('DATA: '+JSON.stringify(response.data));

									let sum_0_0 = 0;
									let sum_0_1 = 0;
									let sum_1_0 = 0;
									let sum_1_1 = 0;
									let sums_2 = [];
									for (let i = 0; i < response.data.rs[0].iD.length; i++) {
										sums_2.unshift(0);
									}
																		
									for (let i = 0; i < response.data.sNs.length; i++) {
										sum_0_0 += Number(response.data.rs[i].mtmpv);
										sum_0_1 += Number(response.data.rs[i].mtmcv);
										data_0.datasets.unshift({label: response.data.sNs[i], backgroundColor: colors[i], data: [response.data.rs[i].mtmpv, response.data.rs[i].mtmcv], tooltipsValues: [response.data.rs[i].mtmpv, response.data.rs[i].mtmcv], formatterValues: [response.data.rs[i].mtmpp, response.data.rs[i].mtmcp]});
										sum_1_0 += Number(response.data.rs[i].ytdpv);
										sum_1_1 += Number(response.data.rs[i].ytdcv);
										data_1.datasets.unshift({label: response.data.sNs[i], backgroundColor: colors[i], data: [response.data.rs[i].ytdpv, response.data.rs[i].ytdcv], tooltipsValues: [response.data.rs[i].ytdpv, response.data.rs[i].ytdcv], formatterValues: [response.data.rs[i].ytdpp, response.data.rs[i].ytdcp]});
										data_2.datasets.unshift({label: response.data.sNs[i], backgroundColor: colors[i], data: [], tooltipsValues: [], formatterValues: []});
										for (let j = 0; j < response.data.rs[i].iD.length; j++) {
											data_2.datasets[0].data.unshift(response.data.rs[i].iD[j].idv);
											data_2.datasets[0].tooltipsValues.unshift(response.data.rs[i].iD[j].idv);
											data_2.datasets[0].formatterValues.unshift(response.data.rs[i].iD[j].idp);
											sums_2[j] += Number(response.data.rs[i].iD[j].idv);
										}
									}

									let max_0 = Math.max.apply(null, [sum_0_0, sum_0_1]);
									let max_1 = Math.max.apply(null, [sum_1_0, sum_1_1]);
									let max_2 = Math.max.apply(null, sums_2);
																		
									//console.log('DATA_2: '+JSON.stringify(data_2));
									
									if (Number(window.cSD.iI) === 1) {
										$('div.sp_sb_cs_gb_graph_col_0').css({'min-width':'9.66%'});
										$('div.sp_sb_cs_gb_graph_col_0').css({width:'9.66%'});

										$('div.sp_sb_cs_gb_graph_col_1').css({left:'9.66%'});

										$('div.sp_sb_cs_gb_graph_col_2').css({left:'11.66%'});
										$('div.sp_sb_cs_gb_graph_col_2').css({'min-width':'9.66%'});
										$('div.sp_sb_cs_gb_graph_col_2').css({width:'9.66%'});

										$('div.sp_sb_cs_gb_graph_col_3').css({left:'21.32%'});

										$('div.sp_sb_cs_gb_graph_col_4').css({left:'32.32%'});
										$('div.sp_sb_cs_gb_graph_col_4').css({'min-width':'67.62%'});
										$('div.sp_sb_cs_gb_graph_col_4').css({'max-width':'67.62%'});
										$('div.sp_sb_cs_gb_graph_col_4').css({width:'67.62%'});
									} 
									
									window.chart_0 = new Chart(ctx_0, {
										type: 'bar',
										data: data_0,
										options: {
											devicePixelRatio: window.devicePixelRatio*res_coef,
											hover: {
												mode: null,
												events: [],
											},
											legend: {
												display: false,
											},
											tooltips: {
												mode: 'index',
												backgroundColor: 'rgba(255, 255, 255, 0.75)',
												titleFontSize: 14,
												titleFontColor: 'rgba(0, 0, 0, 1)',
												bodyFontSize: 14,
												bodyFontColor: 'rgba(0, 0, 0, 1)',
												bodyFontStyle: 'normal',
												intersect: false,
												callbacks: {
													label: function(tooltipItem, data) {
														return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].tooltipsValues[tooltipItem.index];
													},
												},
											},
											responsive: true,
											scales: {
												xAxes: [{
													gridLines: {
														display: false,
														drawBorder: false
													},
													stacked: true,
													barPercentage: 1.0,
													categoryPercentage: 0.9,
													position: 'top',
													gridLineHeight: 20,
													backgroundColor: 'rgba(255, 255, 255, 0.75)',
													display: true,
													/*ticks: {
														backdropPadding: 0,
														backdropColor: 'rgba(255, 255, 255, 0.75)',
														padding: 0,
														showLabelBackdrop: false,
														textStrokeColor: 'rgba(255, 255, 255, 0.75)',
														z: 1
													}*/
												}],
												yAxes: [{
													display: false,
													gridLines: {
														display: false,
														drawBorder: false
													},
													stacked: true,
													ticks: {
														display: false,
														max: max_0
													},
												}],
											},
											plugins: {
												datalabels: {
													//anchor: 'end',
													//align: 'top',
													textAlign: 'center',
													//display: 'auto',
													display: function(context) {
														var index = context.dataIndex;
														var value = context.dataset.data[index];
														var prc = value*100/max_0;
														return (prc > 3) ? true : false;
													},
													color: function(context) {
															  var index = context.dataIndex;
															  var color = context.dataset.backgroundColor;
															  var result_rgb = rgbaToRgb(color);
															  var cc = contrastingColor(result_rgb);
															  return '#'+cc;
															},
													font: function(context) {
															  //var avgSize = Math.round((context.chart.height + context.chart.width) / 2);
															  //var size = Math.round(avgSize / 32);
															  var width = context.chart.width;
															  var index = context.dataIndex;
															  var value = context.dataset.data[index];
															  var prc = value*100/max_0;
															  var size = prc < 5 ? 10 : 15;
															  var wgt = prc < 5 ? 'normal' : 'bold';
															  //console.log('value: '+value+' | size: '+size+' | weight: '+wgt);
															  return {
																 style: wgt,
																 "size": size
															};
														  },
													formatter: function(value, ctx_0) {
														if (ctx_0.dataset.formatterValues[ctx_0.dataIndex] > 1.99) {
															return String(ctx_0.dataset.formatterValues[ctx_0.dataIndex]);
														} else {
															return '';
														}
													},
												},
											},
										},
										plugins: [{
											  beforeInit: function(chart) {
												  chart.data.labels.forEach(function(e, i, a) {
													  if (/\n/.test(e)) {
														  if(Number(window.cSD.iI) === 1) {
															  a[i] = e.split(/\n/);
														  } else {
															a[i] = e.replace(/\n/, '');
														  }
													  }
												  });
											  }
										   }]
									});
									
									window.chart_1 = new Chart(ctx_1, {
										type: 'bar',
										data: data_1,
										options: {
											devicePixelRatio: window.devicePixelRatio*res_coef,
											hover: {
												mode: null,
												events: [],
											},
											legend: {
												display: false,
											},
											tooltips: {
												mode: 'index',
												backgroundColor: 'rgba(255, 255, 255, 0.75)',
												titleFontSize: 14,
												titleFontColor: 'rgba(0, 0, 0, 1)',
												bodyFontSize: 14,
												bodyFontColor: 'rgba(0, 0, 0, 1)',
												bodyFontStyle: 'normal',
												intersect: false,
												callbacks: {
													label: function(tooltipItem, data) {
														return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].tooltipsValues[tooltipItem.index];
													},
												},
											},
											responsive: true,
											scales: {
												xAxes: [{
													gridLines: {
														display: false,
														drawBorder: false
													},
													stacked: true,
													barPercentage: 1.0,
													categoryPercentage: 0.9,
													position: 'top',
													gridLineHeight: 20,
												}],
												yAxes: [{
													display: false,
													gridLines: {
														display: false,
														drawBorder: false
													},
													stacked: true,
													ticks: {
														display: false,
														max: max_1
													},
												}],
											},
											plugins: {
												datalabels: {
													//anchor: 'end',
													//align: 'top',
													textAlign: 'center',
													//display: 'auto',
													display: function(context) {
														var index = context.dataIndex;
														var value = context.dataset.data[index];
														var prc = value*100/max_1;
														return (prc > 3) ? true : false;
													},
													color: function(context) {
															  var index = context.dataIndex;
															  var color = context.dataset.backgroundColor;
															  var result_rgb = rgbaToRgb(color);
															  var cc = contrastingColor(result_rgb);
															  return '#'+cc;
															},
													font: function(context) {
															  //var avgSize = Math.round((context.chart.height + context.chart.width) / 2);
															  //var size = Math.round(avgSize / 32);
															  var width = context.chart.width;
															  var index = context.dataIndex;
															  var value = context.dataset.data[index];
															  var prc = value*100/max_1;
															  var size = prc < 5 ? 10 : 15;
															  var wgt = prc < 5 ? 'normal' : 'bold';
															  //console.log('value: '+value+' | size: '+size+' | weight: '+wgt);
															  return {
																 style: wgt,
																 "size": size
															};
														  },
													formatter: function(value, ctx_1) {
														if (ctx_1.dataset.formatterValues[ctx_1.dataIndex] > 1.99) {
															return String(ctx_1.dataset.formatterValues[ctx_1.dataIndex]);
														} else {
															return '';
														}
													},
												},
											},
										},
										plugins: [{
											  beforeInit: function(chart) {
												  chart.data.labels.forEach(function(e, i, a) {
													  if (/\n/.test(e)) {
														  if(Number(window.cSD.iI) === 1) {
															  a[i] = e.split(/\n/);
														  } else {
															a[i] = e.replace(/\n/, '');
														  }
													  }
												  });
											  }
										   }]
									});
									window.chart_2 = new Chart(ctx_2, {
										type: 'bar',
										data: data_2,
										options: {
											devicePixelRatio: window.devicePixelRatio*res_coef,
											hover: {
												mode: null,
												events: [],
											},
											legend: {
												display: false,
											},
											tooltips: {
												mode: 'index',
												backgroundColor: 'rgba(255, 255, 255, 0.75)',
												titleFontSize: 14,
												titleFontColor: 'rgba(0, 0, 0, 1)',
												bodyFontSize: 14,
												bodyFontColor: 'rgba(0, 0, 0, 1)',
												bodyFontStyle: 'normal',
												intersect: false,
												callbacks: {
													label: function(tooltipItem, data) {
														return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].tooltipsValues[tooltipItem.index];
													},
												},
											},
											responsive: true,
											scales: {
												xAxes: [{
													gridLines: {
														display: false,
														drawBorder: false
													},
													stacked: true,
													barPercentage: 1.0,
													categoryPercentage: 0.9,
													position: 'top',
													gridLineHeight: 20,
												}],
												yAxes: [{
													display: false,
													gridLines: {
														display: false,
														drawBorder: false
													},
													stacked: true,
													ticks: {
														display: false,
														max: max_2
													},
												}],
											},
											plugins: {
												datalabels: {
													//anchor: 'end',
													//align: 'top',
													textAlign: 'center',
													//display: 'auto',
													display: function(context) {
														var index = context.dataIndex;
														var value = context.dataset.data[index];
														var prc = value*100/max_2;
														return (prc > 3) ? true : false;
													},
													color: function(context) {
															  var index = context.dataIndex;
															  var color = context.dataset.backgroundColor;
															  var result_rgb = rgbaToRgb(color);
															  var cc = contrastingColor(result_rgb);
															  return '#'+cc;
															},
													font: function(context) {
															  //var avgSize = Math.round((context.chart.height + context.chart.width) / 2);
															  //var size = Math.round(avgSize / 32);
															  var width = context.chart.width;
															  var index = context.dataIndex;
															  var value = context.dataset.data[index];
															  var prc = value*100/max_2;
															  var size = prc < 5 ? 10 : 15;
															  var wgt = prc < 5 ? 'normal' : 'bold';
															  //console.log('value: '+value+' | size: '+size+' | weight: '+wgt);
															  return {
																 style: wgt,
																 "size": size
															};
														  },
													formatter: function(value, ctx_2) {
														if (ctx_2.dataset.formatterValues[ctx_2.dataIndex] > 1.99) {
															return String(ctx_2.dataset.formatterValues[ctx_2.dataIndex]);
														} else {
															return '';
														}
													},
												},
											},
										},
										plugins: [{
											  beforeInit: function(chart) {
												  chart.data.labels.forEach(function(e, i, a) {
													  if (/\n/.test(e)) {
														  if(Number(window.cSD.iI) === 1) {
															  a[i] = e.split(/\n/);
														  } else {
															a[i] = e.replace(/\n/, '');
														  }
													  }
												  });
											  }
										   }]
									});
											
								} else {
									let sum_0_0 = 0;
									let sum_0_1 = 0;
									let sum_1_0 = 0;
									let sum_1_1 = 0;
									let sums_2 = [];
									for (let i = 0; i < response.data.rs[0].iD.length; i++) {
										sums_2.unshift(0);
									}
									for (let i = 0; i < response.data.sNs.length; i++) {
										sum_0_0 += Number(response.data.rs[i].mtmpppt3);
										sum_0_1 += Number(response.data.rs[i].mtmcppt3);
										data_0.datasets.unshift({label: response.data.sNs[i], backgroundColor: colors[i], data: [response.data.rs[i].mtmpppt3, response.data.rs[i].mtmcppt3], tooltipsValues: [response.data.rs[i].mtmpv, response.data.rs[i].mtmcv]});
										sum_1_0 += Number(response.data.rs[i].ytdpppt3);
										sum_1_1 += Number(response.data.rs[i].ytdcppt3);
										data_1.datasets.unshift({label: response.data.sNs[i], backgroundColor: colors[i], data: [response.data.rs[i].ytdpppt3, response.data.rs[i].ytdcppt3], tooltipsValues: [response.data.rs[i].ytdpv, response.data.rs[i].ytdcv]});
										data_2.datasets.unshift({label: response.data.sNs[i], backgroundColor: colors[i], data: [], tooltipsValues: []});
										for (let j = 0; j < response.data.rs[i].iD.length; j++) {
											data_2.datasets[0].data.unshift(response.data.rs[i].iD[j].idppt3);
											data_2.datasets[0].tooltipsValues.unshift(response.data.rs[i].iD[j].idv);
											sums_2[j] += Number(response.data.rs[i].iD[j].idppt3);
										}
									}
									
									let max_0 = ((sum_0_0 > sum_0_1) ? sum_0_0 : sum_0_1) * 1.05;
									let max_1 = ((sum_1_0 > sum_1_1) ? sum_1_0 : sum_1_1) * 1.05;
									let max_2 = Math.max.apply(null, sums_2) * 1.05

									if (Number(window.cSD.iI) === 1) {
										$('div.sp_sb_cs_gb_graph_col_0').css({'min-width':'9.66%'});
										$('div.sp_sb_cs_gb_graph_col_0').css({width:'9.66%'});

										$('div.sp_sb_cs_gb_graph_col_1').css({left:'9.66%'});

										$('div.sp_sb_cs_gb_graph_col_2').css({left:'11.66%'});
										$('div.sp_sb_cs_gb_graph_col_2').css({'min-width':'9.66%'});
										$('div.sp_sb_cs_gb_graph_col_2').css({width:'9.66%'});

										$('div.sp_sb_cs_gb_graph_col_3').css({left:'21.32%'});

										$('div.sp_sb_cs_gb_graph_col_4').css({left:'32.32%'});
										$('div.sp_sb_cs_gb_graph_col_4').css({'min-width':'67.62%'});
										$('div.sp_sb_cs_gb_graph_col_4').css({'max-width':'67.62%'});
										$('div.sp_sb_cs_gb_graph_col_4').css({width:'67.62%'});
									} 									
									
									window.chart_0 = new Chart(ctx_0, {
										type: 'bar',
										data: data_0,
										options: {
											devicePixelRatio: window.devicePixelRatio*res_coef,
											hover: {
												mode: null,
												events: [],
												animationDuration: 0,
											},
											legend: {
												display: false,
											},
											tooltips: {
												mode: 'index',
												backgroundColor: 'rgba(255, 255, 255, 0.75)',
												titleFontSize: 14,
												titleFontColor: 'rgba(0, 0, 0, 1)',
												bodyFontSize: 14,
												bodyFontColor: 'rgba(0, 0, 0, 1)',
												bodyFontStyle: 'normal',
												intersect: false,
												callbacks: {
													label: function(tooltipItem, data) {
														return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].tooltipsValues[tooltipItem.index];
													},
												},
											},
											responsive: true,
											scales: {
												xAxes: [{
													gridLineHeight: 20,
													gridLines: {
														display: false,
													},
													stacked: true,
													barPercentage: 1.0,
													categoryPercentage: 0.9,
													position: 'top',
												}],
												yAxes: [{
													display: false,
													gridLines: {
														display: false,
													},
													stacked: true,
													ticks: {
														max: max_0,
														display: false,
													},
												}],
											},
											plugins: {
												datalabels: {
													textAlign: 'center',
													display: function(context) {
														var index = context.dataIndex;
														var value = context.dataset.data[index];
														var prc = value*100/max_0;
														return (prc > 3) ? true : false;
													},
													color: function(context) {
															  var index = context.dataIndex;
															  var color = context.dataset.backgroundColor;
															  var result_rgb = rgbaToRgb(color);
															  var cc = contrastingColor(result_rgb);
															  return '#'+cc;
															},
													font: function(context) {
															  //var avgSize = Math.round((context.chart.height + context.chart.width) / 2);
															  //var size = Math.round(avgSize / 32);
															  var width = context.chart.width;
															  var index = context.dataIndex;
															  var value = context.dataset.data[index];
															  var prc = value*100/max_0;
															  var size = prc < 5 ? 10 : 15;
															  var wgt = prc < 5 ? 'normal' : 'bold';
															  //console.log('value: '+value+' | size: '+size+' | weight: '+wgt);
															  return {
																 style: wgt,
																 "size": size
															  };
														  },
													formatter: function(value, ctx_0) {
														if (ctx_0.dataset.label === 'All') {
															return String(Math.floor(Number(value) * 50) / 10);
														} else if (Number(value) > 0.49) {
															if ((Number(value) * 10) % 10) {
																return value;
															} else {
																return value + '.0';
															}
														} else {
															return '';
														}
													},
												},
											},
											animation: {
												duration: 1,
												onComplete: function() {
													let chartInstance = this.chart,
														ctxthis = chartInstance.ctx;
													ctxthis.font = Chart.helpers.fontString(12, 'bold', Chart.defaults.global.defaultFontFamily);
													ctxthis.textAlign = 'center';
													ctxthis.textBaseline = 'bottom';
													let datas = this.data;
													let maxYScales = 0;
													this.data.labels.forEach(function(label, label_index) {
														let colsumm = 0;
														datas.datasets.forEach(function(dataset, dataset_index) {
															let meta = chartInstance.controller.getDatasetMeta(dataset_index);
															colsumm += Number(dataset.data[label_index]);
															if (dataset_index == datas.datasets.length - 1) {
																ctxthis.fillText(String(Math.floor(Number(colsumm) * 10) / 10) + '%', meta.data[label_index]._model.x, meta.data[label_index]._model.y - 5);
															}
														});
													});
												},
											},
										},
										plugins: [{
											  beforeInit: function(chart) {
												  chart.data.labels.forEach(function(e, i, a) {
													  if (/\n/.test(e)) {
														  if(Number(window.cSD.iI) === 1) {
															  a[i] = e.split(/\n/);
														  } else {
															a[i] = e.replace(/\n/, '');
														  }
													  }
												  });
											  }
										   }]
									});
									window.chart_1 = new Chart(ctx_1, {
										type: 'bar',
										data: data_1,
										options: {
											devicePixelRatio: window.devicePixelRatio*res_coef,
											hover: {
												mode: null,
												events: [],
												animationDuration: 0,
											},
											legend: {
												display: false,
											},
											tooltips: {
												mode: 'index',
												backgroundColor: 'rgba(255, 255, 255, 0.75)',
												titleFontSize: 14,
												titleFontColor: 'rgba(0, 0, 0, 1)',
												bodyFontSize: 14,
												bodyFontColor: 'rgba(0, 0, 0, 1)',
												bodyFontStyle: 'normal',
												intersect: false,
												callbacks: {
													label: function(tooltipItem, data) {
														return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].tooltipsValues[tooltipItem.index];
													},
												},
											},
											responsive: true,
											scales: {
												xAxes: [{
													gridLineHeight: 20,
													gridLines: {
														display: false,
													},
													stacked: true,
													barPercentage: 1.0,
													categoryPercentage: 0.9,
													position: 'top',
												}],
												yAxes: [{
													display: false,
													gridLines: {
														display: false,
													},
													stacked: true,
													ticks: {
														max: max_1,
														display: false,
													},
												}],
											},
											plugins: {
												datalabels: {
													textAlign: 'center',
													display: function(context) {
														var index = context.dataIndex;
														var value = context.dataset.data[index];
														var prc = value*100/max_1;
														return (prc > 3) ? true : false;
													},
													color: function(context) {
															  var index = context.dataIndex;
															  var color = context.dataset.backgroundColor;
															  var result_rgb = rgbaToRgb(color);
															  var cc = contrastingColor(result_rgb);
															  return '#'+cc;
															},
													font: function(context) {
															  //var avgSize = Math.round((context.chart.height + context.chart.width) / 2);
															  //var size = Math.round(avgSize / 32);
															  var width = context.chart.width;
															  var index = context.dataIndex;
															  var value = context.dataset.data[index];
															  var prc = value*100/max_1;
															  var size = prc < 5 ? 10 : 15;
															  var wgt = prc < 5 ? 'normal' : 'bold';
															  //console.log('value: '+value+' | size: '+size+' | weight: '+wgt);
															  return {
																 style: wgt,
																 "size": size
															  };  },
													formatter: function(value, ctx_1) {
														if (ctx_1.dataset.label === 'All') {
															return String(Math.floor(Number(value) * 50) / 10);
														} else if (Number(value) > 0.49) {
															if ((Number(value) * 10) % 10) {
																return value;
															} else {
																return value + '.0';
															}
														} else {
															return '';
														}
													},
												},
											},
											animation: {
												duration: 1,
												onComplete: function() {
													let chartInstance = this.chart,
														ctxthis = chartInstance.ctx;
													ctxthis.font = Chart.helpers.fontString(12, 'bold', Chart.defaults.global.defaultFontFamily);
													ctxthis.textAlign = 'center';
													ctxthis.textBaseline = 'bottom';
													let datas = this.data;
													let maxYScales = 0;
													this.data.labels.forEach(function(label, label_index) {
														let colsumm = 0;
														datas.datasets.forEach(function(dataset, dataset_index) {
															let meta = chartInstance.controller.getDatasetMeta(dataset_index);
															colsumm += Number(dataset.data[label_index]);
															if (dataset_index == datas.datasets.length - 1) {
																ctxthis.fillText(String(Math.floor(Number(colsumm) * 10) / 10) + '%', meta.data[label_index]._model.x, meta.data[label_index]._model.y - 5);
															}
														});
													});
												},
											},
										},
										plugins: [{
											  beforeInit: function(chart) {
												  chart.data.labels.forEach(function(e, i, a) {
													  if (/\n/.test(e)) {
														  if(Number(window.cSD.iI) === 1) {
															  a[i] = e.split(/\n/);
														  } else {
															a[i] = e.replace(/\n/, '');
														  }
													  }
												  });
											  }
										   }]
									});
									window.chart_2 = new Chart(ctx_2, {
										type: 'bar',
										data: data_2,
										options: {
											devicePixelRatio: window.devicePixelRatio*res_coef,
											hover: {
												mode: null,
												events: [],
												animationDuration: 0,
											},
											legend: {
												display: false,
											},
											tooltips: {
												mode: 'index',
												backgroundColor: 'rgba(255, 255, 255, 0.75)',
												titleFontSize: 14,
												titleFontColor: 'rgba(0, 0, 0, 1)',
												bodyFontSize: 14,
												bodyFontColor: 'rgba(0, 0, 0, 1)',
												bodyFontStyle: 'normal',
												intersect: false,
												callbacks: {
													label: function(tooltipItem, data) {
														return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].tooltipsValues[tooltipItem.index];
													},
												},
											},
											responsive: true,
											scales: {
												xAxes: [{
													barPercentage: 1.0,
		   											categoryPercentage: 0.9,
													gridLineHeight: 20,
													gridLines: {
														display: false,
													},
													stacked: true,
													position: 'top',
												}],
												yAxes: [{
													display: false,
													gridLines: {
														display: false,
													},
													stacked: true,
													ticks: {
														max: max_2,
														display: false,
													},
												}],
											},
											plugins: {
												datalabels: {
													textAlign: 'center',
													display: function(context) {
														var index = context.dataIndex;
														var value = context.dataset.data[index];
														var prc = value*100/max_2;
														return (prc > 3) ? true : false;
													},
													color: function(context) {
															  var index = context.dataIndex;
															  var color = context.dataset.backgroundColor;
															  var result_rgb = rgbaToRgb(color);
															  var cc = contrastingColor(result_rgb);
															  return '#'+cc;
															},
													font: function(context) {
															  //var avgSize = Math.round((context.chart.height + context.chart.width) / 2);
															  //var size = Math.round(avgSize / 32);
															  var width = context.chart.width;
															  var index = context.dataIndex;
															  var value = context.dataset.data[index];
															  var prc = value*100/max_2;
															  var size = prc < 5 ? 10 : 15;
															  var wgt = prc < 5 ? 'normal' : 'bold';
															  //console.log('value: '+value+' | size: '+size+' | weight: '+wgt);
															  return {
																 style: wgt,
																 "size": size
															  };  },
													formatter: function(value, ctx_2) {
														if (ctx_2.dataset.label === 'All') {
															return String(Math.floor(Number(value) * 50) / 10);
														} else if (Number(value) > 0.49) {
															if ((Number(value) * 10) % 10) {
																return value;
															} else {
																return value + '.0';
															}
														} else {
															return '';
														}
													},
												},
											},
											animation: {
												duration: 1,
												onComplete: function() {
													let chartInstance = this.chart,
														ctxthis = chartInstance.ctx;
													ctxthis.font = Chart.helpers.fontString(12, 'bold', Chart.defaults.global.defaultFontFamily);
													ctxthis.textAlign = 'center';
													ctxthis.textBaseline = 'bottom';
													let datas = this.data;
													let maxYScales = 0;
													this.data.labels.forEach(function(label, label_index) {
														let colsumm = 0;
														datas.datasets.forEach(function(dataset, dataset_index) {
															let meta = chartInstance.controller.getDatasetMeta(dataset_index);
															colsumm += Number(dataset.data[label_index]);
															if (dataset_index == datas.datasets.length - 1) {
																ctxthis.fillText(String(Math.floor(Number(colsumm) * 10) / 10) + '%', meta.data[label_index]._model.x, meta.data[label_index]._model.y - 5);
															}
														});
													});
												},
											},
										},
										plugins: [{
											  beforeInit: function(chart) {
												  chart.data.labels.forEach(function(e, i, a) {
													  if (/\n/.test(e)) {
														  if(Number(window.cSD.iI) === 1) {
															  a[i] = e.split(/\n/);
														  } else {
															a[i] = e.replace(/\n/, '');
														  }
													  }
												  });
											  }
										   }]
									});
								}
							}
								else if (window.sD[window.cP].gT === '3') {
									let textvalue = '';
									textvalue = '<div class="sp_sb_cs_gb_graph3_col_0">' + 
													'<div class="sp_sb_cs_gb_g3c0_series_list">' + 
														'<div class="sp_sb_cs_gb_g3c0_sl_in">';
									for (let i = 0; i < response.data.sNs.length; i++) {
										textvalue += '<div class="sp_sb_cs_gb_g3c0_sl_series_row _' + i + '" style="top: ' + (6 + 94 / response.data.sNs.length * i) + '%; height: ' + (94 / response.data.sNs.length - 0.3) + '%"><div class="sp_sb_cs_gb_g3c0_sl_sr_item_name">' + response.data.sNs[i] + '</div></div>';
									}
									$('.sp_sb_cs_sb_graph_block')[0].innerHTML = textvalue + '</div></div></div>' + 
										'<div class="sp_sb_cs_gb_graph3_col_1"><div class="sp_sb_cs_gb_graph3_col_in"><canvas id="gr_canvas_0" style="width: 100%; height: 100%"></canvas></div></div>' + 
										'<div class="sp_sb_cs_gb_graph3_col_2"><div class="sp_sb_cs_gb_graph3_col_in"><canvas id="gr_canvas_1" style="width: 100%; height: 100%"></canvas></div></div>' + 
										'<div class="sp_sb_cs_gb_graph3_col_3"><div class="sp_sb_cs_gb_graph3_col_in"><canvas id="gr_canvas_2" style="width: 100%; height: 100%"></canvas></div></div>';
									Chart.defaults.global.defaultFontSize = 14;
									Chart.defaults.global.defaultFontStyle = 'bold';
									Chart.defaults.global.defaultFontColor = 'black';
									Chart.defaults.global.defaultFontFamily = 'Verdana';
									Chart.defaults.global.elements.rectangle.borderColor = 'rgba(25, 25, 25, 1)';
									Chart.defaults.global.elements.rectangle.borderWidth = 0.7;
									Chart.defaults.global.scaleBeginAtZero = true;

									let maxval;
									let minval;

									//console.log('111 datasets_0: '+JSON.stringify(response.data.datasets_0));

									maxval = Math.max.apply(null, response.data.datasets_0[0].data);
									minval = Math.abs(Math.min.apply(null, response.data.datasets_0[1].data));
									//console.log('minval: '+minval);
									//console.log('maxval: '+maxval);
									let maxval_0 = Math.max.apply(null, [maxval,minval]);
									//console.log('maxval_0: '+maxval_0);
									
									let data_0 = {
										labels: response.data.ss,
										datasets: response.data.datasets_0,
									};

									maxval = Math.max.apply(null, response.data.datasets_1[0].data);
									minval = Math.abs(Math.min.apply(null, response.data.datasets_1[1].data));
									let maxval_1 = Math.max.apply(null, [maxval,minval]);
									//console.log('maxval_1: '+maxval_1);

									console.log('111 datasets_1: '+JSON.stringify(response.data.datasets_1));
									console.log('minval: '+minval);
									console.log('maxval: '+maxval);
									console.log('maxval_1: '+maxval_1);
									
									let data_1 = {
										labels: response.data.ss,
										datasets: response.data.datasets_1,
									};

									maxval = Math.max.apply(null, response.data.datasets_2[0].data);
									minval = Math.abs(Math.min.apply(null, response.data.datasets_2[1].data));
									let maxval_2 = Math.max.apply(null, [maxval,minval]);
									//console.log('maxval_2: '+maxval_2);

									let data_2 = {
										labels: response.data.ss,
										datasets: response.data.datasets_2,
									};
									
									let ctx_0 = document.getElementById('gr_canvas_0').getContext('2d');
									let ctx_1 = document.getElementById('gr_canvas_1').getContext('2d');
									let ctx_2 = document.getElementById('gr_canvas_2').getContext('2d');

									let max = Math.max.apply(null, response.data.datasets_0[0].data);
									let min = Math.abs(Math.min.apply(null, response.data.datasets_0[1].data));
									let max_0 = Math.max.apply(null, [max,min]) * 1.1;
									max = Math.max.apply(null, response.data.datasets_1[0].data);
									min = Math.abs(Math.min.apply(null, response.data.datasets_1[1].data));
									let max_1 = Math.max.apply(null, [max,min]) * 1.1;
									max = Math.max.apply(null, response.data.datasets_2[0].data);
									min = Math.abs(Math.min.apply(null, response.data.datasets_2[1].data));
									let max_2 = Math.max.apply(null, [max,min]) * 1.1;

									let lim_mrg = 50;
									
									window.chart_0 = new Chart(ctx_0, {
										type: 'horizontalBar',
										data: data_0,
										options: {
											layout: {
											  padding: {
												right: 0,
												left: 0
											  }
											},
											devicePixelRatio: window.devicePixelRatio*res_coef,
											title: {
												display: true,
												text: 'Sales Units GROWTH%',
											},
											legend: {
												display: false,
											},
											tooltips: {
												enabled: false,
											},
											responsive: true,
											scales: {
												xAxes: [{
													display: false,
													gridLines: {
														display: false,
													},
													stacked: true,
													ticks: {
														min: -max_0,
														max: max_0,
														display: false,
													},
												}],
												yAxes: [{
													barPercentage: 1,
		   											categoryPercentage: 0.80,
													display: false,
													gridLines: {
														display: false,
													},
													stacked: true,
													//ticks: {
													//	display: false,
													//	beginAtZero : true
													//},
													id: 'y-axis-0',
													  gridLines: {
														display: true,
														lineWidth: 1,
														color: "rgba(127,127,127,0.30)"
													  },
													  ticks: {
														beginAtZero:true,
														mirror:false,
														//padding: 40,
														suggestedMin: 0,
														suggestedMax: 500,
														 /*backdropPadding: {
															  x: 0,
															  y: 0
														  }*/
													  },
													  afterBuildTicks: function(chart) {
													  }
												}],
											},
											plugins: {
												datalabels: {
													textAlign: 'center',
													font: {
														size: 14,
													},
													clamp: true,
													display: true,
													clip: true,
													padding: { left: 1 },
													color: function(context) {
															var index = context.dataIndex;
															let prclim = Math.abs(context.dataset.data[index])*100/maxval_0;
																if(prclim > lim_mrg) {														
																  var color = context.dataset.backgroundColor;
																  var result_rgb = rgbaToRgb(color);
																  var cc = contrastingColor(result_rgb);
																  return '#'+cc;
																} else { return 'black'; }
															},
													anchor: function(context) {
														let prclim = Math.abs(context.dataset.data[context.dataIndex])*100/maxval_0;
														//console.log('prclim0: '+prclim);
														if(context.dataset.data[context.dataIndex] < 0 && prclim < lim_mrg) return 'start';
														else if(context.dataset.data[context.dataIndex] > 0 && prclim < lim_mrg) return 'end';
														else return 'center';
													},
													align: function(context) {
														let prclim = Math.abs(context.dataset.data[context.dataIndex])*100/maxval_0;
														if(context.dataset.data[context.dataIndex] < 0 && prclim < lim_mrg) return 'start';
														else if(context.dataset.data[context.dataIndex] > 0 && prclim < lim_mrg) return 'end';
														else return 'center';
													},
													formatter: function(value, ctx_0) {
														if (ctx_0.dataset.data[ctx_0.dataIndex] != 0) {
															if (Math.abs(ctx_0.dataset.data[ctx_0.dataIndex]) > 1) {
																ctx_0.dataset.data[ctx_0.dataIndex] = Math.round(ctx_0.dataset.data[ctx_0.dataIndex]);
															}
															let tmpstr = String(ctx_0.dataset.data[ctx_0.dataIndex]);
															if (ctx_0.dataset.data[ctx_0.dataIndex] > 0) {
																tmpstr = '+' + tmpstr;
															}
															return tmpstr;
														} else {
															return '';
														}
													},
												},
											},
										},
									});
									window.chart_1 = new Chart(ctx_1, {
										type: 'horizontalBar',
										data: data_1,
										options: {
											devicePixelRatio: window.devicePixelRatio*res_coef,
											title: {
												display: true,
												text: 'Sales Value RUB GROWTH%',
											},
											legend: {
												display: false,
											},
											tooltips: {
												enabled: false,
											},
											scales: {
												xAxes: [{
													display: false,
													gridLines: {
														display: false,
													},
													stacked: true,
													ticks: {
														min: -max_1,
														max: max_1,
														display: false,
													},
												}],
												yAxes: [{
													barPercentage: 1,
		   											categoryPercentage: 0.80,
													display: false,
													gridLines: {
														display: false,
													},
													stacked: true,
													ticks: {
														display: false,
													},
												}],
											},
											plugins: {
												datalabels: {
													textAlign: 'center',
													font: {
														size: 14,
													},
													clamp: true,
													display: true,
													clip: true,
													color: function(context) {
															var index = context.dataIndex;
															let prclim = Math.abs(context.dataset.data[index])*100/maxval_1;
																if(prclim > lim_mrg) {														
																  var color = context.dataset.backgroundColor;
																  var result_rgb = rgbaToRgb(color);
																  var cc = contrastingColor(result_rgb);
																  return '#'+cc;
																} else { return 'black'; }
															},
													anchor: function(context) {
														let prclim = Math.abs(context.dataset.data[context.dataIndex])*100/maxval_1;
														console.log('prclim1: '+prclim);
														console.log('context.dataset.data[context.dataIndex]: '+context.dataset.data[context.dataIndex]);
														if(context.dataset.data[context.dataIndex] < 0 && prclim < lim_mrg) return 'start';
														else if(context.dataset.data[context.dataIndex] > 0 && prclim < lim_mrg) return 'end';
														else return 'center';
													},
													align: function(context) {
														let prclim = Math.abs(context.dataset.data[context.dataIndex])*100/maxval_1;
														if(context.dataset.data[context.dataIndex] < 0 && prclim < lim_mrg) return 'start';
														else if(context.dataset.data[context.dataIndex] > 0 && prclim < lim_mrg) return 'end';
														else return 'center';
													},
													formatter: function(value, ctx_1) {
														if (ctx_1.dataset.data[ctx_1.dataIndex] != 0) {
															if (Math.abs(ctx_1.dataset.data[ctx_1.dataIndex]) > 1) {
																ctx_1.dataset.data[ctx_1.dataIndex] = Math.round(ctx_1.dataset.data[ctx_1.dataIndex]);
															}
															let tmpstr = String(ctx_1.dataset.data[ctx_1.dataIndex]);
															if (ctx_1.dataset.data[ctx_1.dataIndex] > 0) {
																tmpstr = '+' + tmpstr;
															}
															return tmpstr;
														} else {
															return '';
														}
													},
												},
											},
										},
									});
									window.chart_2 = new Chart(ctx_2, {
										type: 'horizontalBar',
										data: data_2,
										options: {
											devicePixelRatio: window.devicePixelRatio*res_coef,
											title: {
												display: true,
												text: 'Price RUB GROWTH%',
											},
											legend: {
												display: false,
											},
											tooltips: {
												enabled: false,
											},
											scales: {
												xAxes: [{
													//barPercentage: 1,
		   											//categoryPercentage: 1,
													display: false,
													gridLines: {
														display: false,
													},
													stacked: true,
													ticks: {
														min: -max_2,
														max: max_2,
														display: false,
													},
												}],
												yAxes: [{
													barPercentage: 1,
		   											categoryPercentage: 0.80,
													display: false,
													gridLines: {
														display: false,
													},
													stacked: true,
													ticks: {
														display: false,
													},
												}],
											},
											plugins: {
												datalabels: {
													textAlign: 'center',
													font: {
														size: 14,
													},
													clamp: true,
													display: true,
													clip: true,
													color: function(context) {
															var index = context.dataIndex;
															let prclim = Math.abs(context.dataset.data[index])*100/maxval_2;
																if(prclim > lim_mrg) {														
																  var color = context.dataset.backgroundColor;
																  var result_rgb = rgbaToRgb(color);
																  var cc = contrastingColor(result_rgb);
																  return '#'+cc;
																} else { return 'black'; }
															},
													anchor: function(context) {
														let prclim = Math.abs(context.dataset.data[context.dataIndex])*100/maxval_2;
														console.log('context.dataset.data[context.dataIndex]: '+context.dataset.data[context.dataIndex]);
														console.log('prclim2: '+prclim);
														if(context.dataset.data[context.dataIndex] < 0 && prclim < lim_mrg) return 'start';
														else if(context.dataset.data[context.dataIndex] > 0 && prclim < lim_mrg) return 'end';
														else return 'center';
													},
													align: function(context) {
														let prclim = Math.abs(context.dataset.data[context.dataIndex])*100/maxval_2;
														if(context.dataset.data[context.dataIndex] < 0 && prclim < lim_mrg) return 'start';
														else if(context.dataset.data[context.dataIndex] > 0 && prclim < lim_mrg) return 'end';
														else return 'center';
													},
													formatter: function(value, ctx_2) {
														if (ctx_2.dataset.data[ctx_2.dataIndex] != 0) {
															if (Math.abs(ctx_2.dataset.data[ctx_2.dataIndex]) > 1) {
																ctx_2.dataset.data[ctx_2.dataIndex] = Math.round(ctx_2.dataset.data[ctx_2.dataIndex]);
															}
															let tmpstr = String(ctx_2.dataset.data[ctx_2.dataIndex]);
															if (ctx_2.dataset.data[ctx_2.dataIndex] > 0) {
																tmpstr = '+' + tmpstr;
															}
															return tmpstr;
														} else {
															return '';
														}
													},
												},
											},
										},
									});
									
								}
								else if (window.sD[window.cP].gT === '4') {
									let textvalue = '';
									textvalue = '<div class="sp_sb_cs_gb_graph3_col_0">' + 
													'<div class="sp_sb_cs_gb_g3c0_series_list">' + 
														'<div class="sp_sb_cs_gb_g3c0_sl_in">';
									for (let i = 0; i < response.data.sNs.length; i++) {
										textvalue += '<div class="sp_sb_cs_gb_g3c0_sl_series_row _' + i + '" style="top: ' + (6 + 94 / response.data.sNs.length * i) + '%; height: ' + (94 / response.data.sNs.length - 0.3) + '%"><div class="sp_sb_cs_gb_g3c0_sl_sr_item_name">' + response.data.sNs[i] + '</div></div>';
									}
									$('.sp_sb_cs_sb_graph_block')[0].innerHTML = textvalue + '</div></div></div>' + 
										'<div class="sp_sb_cs_gb_graph3_col_1"><div class="sp_sb_cs_gb_graph3_col_in"><canvas id="gr_canvas_0" style="width: 100%; height: 100%"></canvas></div></div>' + 
										'<div class="sp_sb_cs_gb_graph3_col_2"><div class="sp_sb_cs_gb_graph3_col_in"><canvas id="gr_canvas_1" style="width: 100%; height: 100%"></canvas></div></div>' + 
										'<div class="sp_sb_cs_gb_graph3_col_3"><div class="sp_sb_cs_gb_graph3_col_in"><canvas id="gr_canvas_2" style="width: 100%; height: 100%"></canvas></div></div>';
									Chart.defaults.global.defaultFontSize = 14;
									Chart.defaults.global.defaultFontStyle = 'bold';
									Chart.defaults.global.defaultFontColor = 'black';
									Chart.defaults.global.defaultFontFamily = 'Verdana';
									Chart.defaults.global.elements.rectangle.borderColor = 'rgba(25, 25, 25, 1)';
									Chart.defaults.global.elements.rectangle.borderWidth = 0.7;
									Chart.defaults.global.scaleBeginAtZero = true;

									let maxval;
									let minval;
									
									maxval = Math.max.apply(null, response.data.datasets_0[0].data);
									minval = Math.abs(Math.min.apply(null, response.data.datasets_0[1].data));
									let maxval_0 = Math.max.apply(null, [maxval,minval]);
									//console.log('maxval_0: '+maxval_0);
									
									let data_0 = {
										labels: response.data.ss,
										datasets: response.data.datasets_0,
									};

									maxval = Math.max.apply(null, response.data.datasets_1[0].data);
									minval = Math.abs(Math.min.apply(null, response.data.datasets_1[1].data));
									let maxval_1 = Math.max.apply(null, [maxval,minval]);
									//console.log('maxval_1: '+maxval_1);

									let data_1 = {
										labels: response.data.ss,
										datasets: response.data.datasets_1,
									};

									maxval = Math.max.apply(null, response.data.datasets_2[0].data);
									minval = Math.abs(Math.min.apply(null, response.data.datasets_2[1].data));
									let maxval_2 = Math.max.apply(null, [maxval,minval]);
									//console.log('maxval_2: '+maxval_2);

									let data_2 = {
										labels: response.data.ss,
										datasets: response.data.datasets_2,
									};
									
									let ctx_0 = document.getElementById('gr_canvas_0').getContext('2d');
									let ctx_1 = document.getElementById('gr_canvas_1').getContext('2d');
									let ctx_2 = document.getElementById('gr_canvas_2').getContext('2d');

									let max = Math.max.apply(null, response.data.datasets_0[0].data);
									let min = Math.abs(Math.min.apply(null, response.data.datasets_0[1].data));
									let max_0 = Math.max.apply(null, [max,min]) * 1.1;
									max = Math.max.apply(null, response.data.datasets_1[0].data);
									min = Math.abs(Math.min.apply(null, response.data.datasets_1[1].data));
									let max_1 = Math.max.apply(null, [max,min]) * 1.1;
									max = Math.max.apply(null, response.data.datasets_2[0].data);
									min = Math.abs(Math.min.apply(null, response.data.datasets_2[1].data));
									let max_2 = Math.max.apply(null, [max,min]) * 1.1;

									let lim_mrg = 50;
									
									window.chart_0 = new Chart(ctx_0, {
										type: 'horizontalBar',
										data: data_0,
										options: {
											layout: {
											  padding: {
												right: 0,
												left: 0
											  }
											},
											devicePixelRatio: window.devicePixelRatio*res_coef,
											title: {
												display: true,
												text: 'Sales Units GROWTH%',
											},
											legend: {
												display: false,
											},
											tooltips: {
												enabled: false,
											},
											responsive: true,
											scales: {
												xAxes: [{
													display: false,
													gridLines: {
														display: false,
													},
													stacked: true,
													ticks: {
														min: -max_0,
														max: max_0,
														display: false,
													},
												}],
												yAxes: [{
													barPercentage: 1,
		   											categoryPercentage: 0.80,
													display: false,
													gridLines: {
														display: false,
													},
													stacked: true,
													//ticks: {
													//	display: false,
													//	beginAtZero : true
													//},
													id: 'y-axis-0',
													  gridLines: {
														display: true,
														lineWidth: 1,
														color: "rgba(127,127,127,0.30)"
													  },
													  ticks: {
														beginAtZero:true,
														mirror:false,
														//padding: 40,
														suggestedMin: 0,
														suggestedMax: 500,
														 /*backdropPadding: {
															  x: 0,
															  y: 0
														  }*/
													  },
													  afterBuildTicks: function(chart) {
													  }
												}],
											},
											plugins: {
												datalabels: {
													textAlign: 'center',
													font: {
														size: 14,
													},
													clamp: true,
													display: true,
													clip: true,
													padding: { left: 1 },
													color: function(context) {
															var index = context.dataIndex;
															let prclim = Math.abs(context.dataset.data[index])*100/maxval_0;
																if(prclim > lim_mrg) {
																  var color = context.dataset.backgroundColor;
																  var result_rgb = rgbaToRgb(color);
																  var cc = contrastingColor(result_rgb);
																  return '#'+cc;
																} else { return 'black'; }
															},
													anchor: function(context) {
														let prclim = Math.abs(context.dataset.data[context.dataIndex])*100/maxval_0;
														//console.log('prclim0: '+prclim);
														if(context.dataset.data[context.dataIndex] < 0 && prclim < lim_mrg) return 'start';
														else if(context.dataset.data[context.dataIndex] > 0 && prclim < lim_mrg) return 'end';
														else return 'center';
													},
													align: function(context) {
														let prclim = Math.abs(context.dataset.data[context.dataIndex])*100/maxval_0;
														if(context.dataset.data[context.dataIndex] < 0 && prclim < lim_mrg) return 'start';
														else if(context.dataset.data[context.dataIndex] > 0 && prclim < lim_mrg) return 'end';
														else return 'center';
													},
													formatter: function(value, ctx_0) {
														if (ctx_0.dataset.data[ctx_0.dataIndex] != 0) {
															if (Math.abs(ctx_0.dataset.data[ctx_0.dataIndex]) > 1) {
																ctx_0.dataset.data[ctx_0.dataIndex] = Math.round(ctx_0.dataset.data[ctx_0.dataIndex]);
															}
															let tmpstr = String(ctx_0.dataset.data[ctx_0.dataIndex]);
															if (ctx_0.dataset.data[ctx_0.dataIndex] > 0) {
																tmpstr = '+' + tmpstr;
															}
															return tmpstr;
														} else {
															return '';
														}
													},
												},
											},
										},
									});
									window.chart_1 = new Chart(ctx_1, {
										type: 'horizontalBar',
										data: data_1,
										options: {
											devicePixelRatio: window.devicePixelRatio*res_coef,
											title: {
												display: true,
												text: 'Sales Value RUB GROWTH%',
											},
											legend: {
												display: false,
											},
											tooltips: {
												enabled: false,
											},
											scales: {
												xAxes: [{
													display: false,
													gridLines: {
														display: false,
													},
													stacked: true,
													ticks: {
														min: -max_1,
														max: max_1,
														display: false,
													},
												}],
												yAxes: [{
													barPercentage: 1,
		   											categoryPercentage: 0.80,
													display: false,
													gridLines: {
														display: false,
													},
													stacked: true,
													ticks: {
														display: false,
													},
												}],
											},
											plugins: {
												datalabels: {
													textAlign: 'center',
													font: {
														size: 14,
													},
													clamp: true,
													display: true,
													clip: true,
													color: function(context) {
															var index = context.dataIndex;
															let prclim = Math.abs(context.dataset.data[index])*100/maxval_0;
																if(prclim > lim_mrg) {
																  var color = context.dataset.backgroundColor;
																  var result_rgb = rgbaToRgb(color);
																  var cc = contrastingColor(result_rgb);
																  return '#'+cc;
																} else { return 'black'; }
															},
													anchor: function(context) {
														let prclim = Math.abs(context.dataset.data[context.dataIndex])*100/maxval_1;
														console.log('prclim1: '+prclim);
														console.log('context.dataset.data[context.dataIndex]: '+context.dataset.data[context.dataIndex]);
														if(context.dataset.data[context.dataIndex] < 0 && prclim < lim_mrg) return 'start';
														else if(context.dataset.data[context.dataIndex] > 0 && prclim < lim_mrg) return 'end';
														else return 'center';
													},
													align: function(context) {
														let prclim = Math.abs(context.dataset.data[context.dataIndex])*100/maxval_1;
														if(context.dataset.data[context.dataIndex] < 0 && prclim < lim_mrg) return 'start';
														else if(context.dataset.data[context.dataIndex] > 0 && prclim < lim_mrg) return 'end';
														else return 'center';
													},
													formatter: function(value, ctx_1) {
														if (ctx_1.dataset.data[ctx_1.dataIndex] != 0) {
															if (Math.abs(ctx_1.dataset.data[ctx_1.dataIndex]) > 1) {
																ctx_1.dataset.data[ctx_1.dataIndex] = Math.round(ctx_1.dataset.data[ctx_1.dataIndex]);
															}
															let tmpstr = String(ctx_1.dataset.data[ctx_1.dataIndex]);
															if (ctx_1.dataset.data[ctx_1.dataIndex] > 0) {
																tmpstr = '+' + tmpstr;
															}
															return tmpstr;
														} else {
															return '';
														}
													},
												},
											},
										},
									});
									window.chart_2 = new Chart(ctx_2, {
										type: 'horizontalBar',
										data: data_2,
										options: {
											devicePixelRatio: window.devicePixelRatio*res_coef,
											title: {
												display: true,
												text: 'Price RUB GROWTH%',
											},
											legend: {
												display: false,
											},
											tooltips: {
												enabled: false,
											},
											scales: {
												xAxes: [{
													//barPercentage: 1,
		   											//categoryPercentage: 1,
													display: false,
													gridLines: {
														display: false,
													},
													stacked: true,
													ticks: {
														min: -max_2,
														max: max_2,
														display: false,
													},
												}],
												yAxes: [{
													barPercentage: 1,
		   											categoryPercentage: 0.80,
													display: false,
													gridLines: {
														display: false,
													},
													stacked: true,
													ticks: {
														display: false,
													},
												}],
											},
											plugins: {
												datalabels: {
													textAlign: 'center',
													font: {
														size: 14,
													},
													clamp: true,
													display: true,
													clip: true,
													color: function(context) {
															var index = context.dataIndex;
															let prclim = Math.abs(context.dataset.data[index])*100/maxval_0;
																if(prclim > lim_mrg) {
																  var color = context.dataset.backgroundColor;
																  var result_rgb = rgbaToRgb(color);
																  var cc = contrastingColor(result_rgb);
																  return '#'+cc;
																} else { return 'black'; }
															},
													anchor: function(context) {
														let prclim = Math.abs(context.dataset.data[context.dataIndex])*100/maxval_2;
														console.log('context.dataset.data[context.dataIndex]: '+context.dataset.data[context.dataIndex]);
														console.log('prclim2: '+prclim);
														if(context.dataset.data[context.dataIndex] < 0 && prclim < lim_mrg) return 'start';
														else if(context.dataset.data[context.dataIndex] > 0 && prclim < lim_mrg) return 'end';
														else return 'center';
													},
													align: function(context) {
														let prclim = Math.abs(context.dataset.data[context.dataIndex])*100/maxval_2;
														if(context.dataset.data[context.dataIndex] < 0 && prclim < lim_mrg) return 'start';
														else if(context.dataset.data[context.dataIndex] > 0 && prclim < lim_mrg) return 'end';
														else return 'center';
													},
													formatter: function(value, ctx_2) {
														if (ctx_2.dataset.data[ctx_2.dataIndex] != 0) {
															if (Math.abs(ctx_2.dataset.data[ctx_2.dataIndex]) > 1) {
																ctx_2.dataset.data[ctx_2.dataIndex] = Math.round(ctx_2.dataset.data[ctx_2.dataIndex]);
															}
															let tmpstr = String(ctx_2.dataset.data[ctx_2.dataIndex]);
															if (ctx_2.dataset.data[ctx_2.dataIndex] > 0) {
																tmpstr = '+' + tmpstr;
															}
															return tmpstr;
														} else {
															return '';
														}
													},
												},
											},
										},
									});
									
								}
								else if (window.sD[window.cP].gT === '5') {
										
									$('.sp_sb_cs_sb_data_block')[0].innerHTML = '';

										textvalue= '';
										//let colors = ['rgba(0, 114, 218, 1)', 'rgba(0, 156, 73, 1)', 'rgba(240, 66, 121, 1)', 'rgba(255, 127, 0, 1)', 'rgba(167, 122, 215, 1)', 'rgba(255, 255, 0, 1)', 'rgba(255, 000, 128, 0.4)', 'rgba(255, 99, 132, 0.9)', 'rgba(197, 255, 10, 1)', 'rgba(127, 127, 127, 1)'];
										let colors = ['rgba(0, 114, 218, 1)', 'rgba(0, 156, 73, 1)', 'rgba(240, 66, 121, 1)', 'rgba(255, 127, 0, 1)', 'rgba(167, 122, 215, 1)', 'rgba(255, 255, 0, 1)', 'rgba(127, 127, 127, 1)'];

										let names = response.data.sNs;
										if(BrandsTypes.includes(window.cSD.sI)) {
											for (let i = 0; i < names.length; i++) {
												let brand = names[i].trim();
												if (brand in window.BrandsData) {
													colors[i] = window.BrandsData[brand].color;
													names[i] = window.BrandsData[brand].name;
												}
											}
										}
										if (window.cSD.sI == 0) {
											for (let i = 0; i < names.length; i++) {
												colors[i] = window.cSD.dD[i].clr;
												names[i] = window.cSD.dD[i].sS.toLocaleString() + ' - ' + window.cSD.dD[i].eS.toLocaleString();
											}
										}
										textvalue = '<div class="sp_sb_cs_gb_graph4_col_2"><canvas id="gr_canvas_0" style="width: 100%; height: 100%"></canvas></div><div class="sp_sb_cs_gb_graph4_col_3">' + 
												'<div class="sp_sb_cs_gb_gc3_series_list">' + 
													'<div class="sp_sb_cs_gb_gc3_sl_in">';
										for (let i = 0; i < response.data.sNs.length; i++) {
											textvalue += '<div class="sp_sb_cs_gb_gc3_sl_series_row _' + i + '" style="top: ' + (4 + 14.28 * i) + '%;"><div class="sp_sb_cs_gb_gc3_sl_sr_color_box" style="background-color: ' + colors[i] + ';"></div><div class="sp_sb_cs_gb_gc3_sl_sr_space"></div><p class="sp_sb_cs_gb_gc3_sl_sr_item_name">' + names[i] + '</p></div>';
										}
										$('.sp_sb_cs_sb_graph_block')[0].innerHTML = textvalue + '</div></div></div><div class="sp_sb_cs_gb_graph4_col_4"><canvas id="gr_canvas_1" style="width: 100%; height: 100%"></canvas></div>';


								let legend_col_height = getPercentHeight($('div.sp_sb_cs_gb_gc3_sl_in'));
								console.log('legend col height: '+legend_col_height);

									if(response.data.sNs.length <=7) {
										let col_height = 35;
										//console.log('legend col_height: '+col_height);
										$('div.sp_sb_cs_gb_gc3_sl_in').css({'min-height':col_height+'%'});
										$('div.sp_sb_cs_gb_gc3_sl_in').css({'max-height':col_height+'%'});
										$('div.sp_sb_cs_gb_gc3_sl_in').css({height:col_height+'%'});
										$('div.sp_sb_cs_gb_gc3_sl_in').css({'top':'calc(25%)'});

										let col_top = 0;
										$("div.sp_sb_cs_gb_gc3_sl_series_row").each(function(index){
											let legend_col_top = getPercentTop($(this));
											//console.log('legend top: '+index+': '+legend_col_top);
											//$(this).css({'top':+(legend_col_top-4)+'%'});
											col_top = col_top+10;
											$(this).css({'top':+col_top+'%'});
											$(this).css({'height':'7%'});
											$(this).css({'min-height':'7%'});
											$(this).css({'max-height':'7%'});
										});
									} else
									if(response.data.sNs.length > 7 && response.data.sNs.length < 15) {
										let col_height_ajust = (response.data.sNs.length)*2.5;
										//console.log('legend col_height_ajust: '+col_height_ajust);
										$('div.sp_sb_cs_gb_gc3_sl_in').css({'min-height':(legend_col_height+col_height_ajust)+'%'});
										$('div.sp_sb_cs_gb_gc3_sl_in').css({'max-height':(legend_col_height+col_height_ajust)+'%'});
										$('div.sp_sb_cs_gb_gc3_sl_in').css({height:(legend_col_height+col_height_ajust)+'%'});
										$('div.sp_sb_cs_gb_gc3_sl_in').css({'top':'calc(15% + 2.2vh)'});

										let col_top = 0;
										$("div.sp_sb_cs_gb_gc3_sl_series_row").each(function(index){
											let legend_col_top = getPercentTop($(this));
											//console.log('legend top: '+index+': '+legend_col_top);
											//$(this).css({'top':+(legend_col_top-4)+'%'});
											col_top = col_top+5.8;
											$(this).css({'top':+col_top+'%'});
											});
									} else
									if(response.data.sNs.length >= 15 && response.data.sNs.length < 18) {
										let col_height = (response.data.sNs.length)*4.5;
										console.log('legend col_height 15: '+col_height);
										let prc_ajs = 30 - response.data.sNs.length;
										$('div.sp_sb_cs_gb_gc3_sl_in').css({'min-height':col_height+'%'});
										$('div.sp_sb_cs_gb_gc3_sl_in').css({'max-height':col_height+'%'});
										$('div.sp_sb_cs_gb_gc3_sl_in').css({height:col_height+'%'});
										$('div.sp_sb_cs_gb_gc3_sl_in').css({'top':'calc('+prc_ajs+'% + 1.3vh)'});

										let col_top = 0;
										$("div.sp_sb_cs_gb_gc3_sl_series_row").each(function(index){
											let legend_col_top = getPercentTop($(this));
											//console.log('legend top: '+index+': '+legend_col_top);
											//$(this).css({'top':+(legend_col_top-4)+'%'});
											col_top = col_top+5;
											$(this).css({'top':+col_top+'%'});
										});
									} else
									if(response.data.sNs.length >= 18) {
										let col_height = (response.data.sNs.length)*3.8;
										//console.log('legend col_height 20: '+col_height);
										let prc_ajs = 30 - response.data.sNs.length*0.8;
										$('div.sp_sb_cs_gb_gc3_sl_in').css({'min-height':col_height+'%'});
										$('div.sp_sb_cs_gb_gc3_sl_in').css({'max-height':col_height+'%'});
										$('div.sp_sb_cs_gb_gc3_sl_in').css({height:col_height+'%'});
										$('div.sp_sb_cs_gb_gc3_sl_in').css({'top':'calc('+prc_ajs+'% + 0.7vh)'});

										let col_top = 0;
										$("div.sp_sb_cs_gb_gc3_sl_series_row").each(function(index){
											let legend_col_top = getPercentTop($(this));
											//console.log('legend top: '+index+': '+legend_col_top);
											//$(this).css({'top':+(legend_col_top-4)+'%'});
											col_top = col_top+4.6;
											$(this).css({'top':+col_top+'%'});
										});
									}
										
										
										Chart.defaults.global.defaultFontSize = 13;// до слияния 8
										Chart.defaults.global.defaultFontStyle = 'bold';
										Chart.defaults.global.defaultFontColor = 'black';
										Chart.defaults.global.defaultFontFamily = 'Verdana';

										let data_0 = {};
										let ctx_0 = document.getElementById('gr_canvas_0').getContext('2d');
										let data_1 = {};
										let ctx_1 = document.getElementById('gr_canvas_1').getContext('2d');
										let dataArr0 = [];
										let ttVal0 = [];
										let dataArr1 = [];
										let ttVal1 = [];
										let title0 = response.data.titleytd0;
										let title1 = response.data.titleytd1;
										for (let i = 0; i < response.data.sNs.length; i++) {
											dataArr0.push(response.data.rs[i].ytdpp);
											ttVal0.push(response.data.rs[i].ytdpv);
										}
										for (let i = 0; i < response.data.sNs.length; i++) {
											dataArr1.push(response.data.rs[i].ytdcp);
											ttVal1.push(response.data.rs[i].ytdcv);
										}
										data_0 = {
											labels: response.data.sNs,
											datasets: [
												{
													data: dataArr0,
													backgroundColor: colors,
													tooltipsValues: ttVal0
												}
											]
										};
										data_1 = {
											labels: response.data.sNs,
											datasets: [
												{
													data: dataArr1,
													backgroundColor: colors,
													tooltipsValues: ttVal1
												}
											]
										};
										window.chart_0 = new Chart(ctx_0, {
											type: 'doughnut',
											data: data_0,
											options: {
												devicePixelRatio: window.devicePixelRatio*res_coef,
												title: {
													display: true,
													text: title0,
													position: "bottom",
													fontSize: 25,
													padding: 23
												},
												legend: {
													display: false,
												},
												hover: {
													mode: null,
													events: [],
												},
												tooltips: {
													mode: 'index',
													backgroundColor: 'rgba(255, 255, 255, 0.75)',
													titleFontSize: 14,
													titleFontColor: 'rgba(0, 0, 0, 1)',
													bodyFontSize: 14,
													bodyFontColor: 'rgba(0, 0, 0, 1)',
													bodyFontStyle: 'normal',
													intersect: false,
													callbacks: {
														label: function(tooltipItem, data) {
															return data.labels[tooltipItem.index] + ': ' + data.datasets[tooltipItem.datasetIndex].tooltipsValues[tooltipItem.index];
														},
													},
												},
												responsive: true,
												plugins: {
													legend: {
														display: false,
													},
													datalabels: {
														textAlign: 'center',
														color: function(context) {
															var index = context.dataIndex;
															var color = context.dataset.backgroundColor[index];
															var result_rgb = rgbaToRgb(color);
															var cc = contrastingColor(result_rgb);
															return '#'+cc;
														},														
														font: function(context) {
															var width = context.chart.width;
															var index = context.dataIndex;
															var value = context.dataset.data[index];
															var size;
															switch(true) {
																case value < 5 && value > 3: size = InFontSize; break;
																case value <= 3: size = InFontSize - decrFont; break;
																default: size = InFontSize+6;
															}
															//console.log('value: '+value+' | size: '+size+' | weight: '+wgt);
															return {
																style:	(value < 5) ? 'normal' : 'bold',
																"size": size
															};
														},
														formatter: function(value, ctx_0) {
															if (Number(value) > 1.99) {
																if ((Number(value) * 10) % 10) {
																	return value;
																} else {
																	return value + '.0';
																}
															} else {
																return '';
															}
														},
													},
												},
											},
											plugins: [{
											  beforeInit: function(chart) {
												  chart.data.labels.forEach(function(e, i, a) {
													  if (/\n/.test(e)) {
														  a[i] = e;
													  }
												  });
											  }
										   }]
										});
										window.chart_1 = new Chart(ctx_1, {
											type: 'doughnut',
											data: data_1,
											options: {
												devicePixelRatio: window.devicePixelRatio*res_coef,
												title: {
													display: true,
													text: title1,
													position: "bottom",
													fontSize: 25,
													padding: 23
												},
												legend: {
													display: false,
												},
												hover: {
													mode: null,
													events: [],
												},
												tooltips: {
													mode: 'index',
													backgroundColor: 'rgba(255, 255, 255, 0.75)',
													titleFontSize: 14,
													titleFontColor: 'rgba(0, 0, 0, 1)',
													bodyFontSize: 14,
													bodyFontColor: 'rgba(0, 0, 0, 1)',
													bodyFontStyle: 'normal',
													intersect: false,
													callbacks: {
														label: function(tooltipItem, data) {
															return data.labels[tooltipItem.index] + ': ' + data.datasets[tooltipItem.datasetIndex].tooltipsValues[tooltipItem.index];
														},
													},
												},
												responsive: true,
												plugins: {
													legend: {
														display: false,
													},
													datalabels: {
														textAlign: 'center',
														color: function(context) {
															var index = context.dataIndex;
															var color = context.dataset.backgroundColor[index];
															var result_rgb = rgbaToRgb(color);
															var cc = contrastingColor(result_rgb);
															return '#'+cc;
														},														
														font: function(context) {
															var width = context.chart.width;
															var index = context.dataIndex;
															var value = context.dataset.data[index];
															var size;
															switch(true) {
																case value < 5 && value > 3: size = InFontSize; break;
																case value <= 3: size = InFontSize - decrFont; break;
																default: size = InFontSize+6;
															}
															//console.log('value: '+value+' | size: '+size+' | weight: '+wgt);
															return {
																style:	(value < 5) ? 'normal' : 'bold',
																"size": size
															};
														},														
														formatter: function(value, ctx_1) {
															if (Number(value) > 1.99) {
																if ((Number(value) * 10) % 10) {
																	return value;
																} else {
																	return value + '.0';
																}
															} else {
																return '';
															}
														},
													},
												},
											},
											plugins: [{
											  beforeInit: function(chart) {
												  chart.data.labels.forEach(function(e, i, a) {
													  if (/\n/.test(e)) {
														  a[i] = e;
													  }
												  });
											  }
										   }]
										});
									}
									else if (window.sD[window.cP].gT === '6') {
									
									$('.sp_sb_cs_sb_data_block')[0].innerHTML = '';

										textvalue= '';
										//let colors = ['rgba(0, 114, 218, 1)', 'rgba(0, 156, 73, 1)', 'rgba(240, 66, 121, 1)', 'rgba(255, 127, 0, 1)', 'rgba(167, 122, 215, 1)', 'rgba(255, 255, 0, 1)', 'rgba(255, 000, 128, 0.4)', 'rgba(255, 99, 132, 0.9)', 'rgba(197, 255, 10, 1)', 'rgba(127, 127, 127, 1)'];
										let colors = ['rgba(0, 114, 218, 1)', 'rgba(0, 156, 73, 1)', 'rgba(240, 66, 121, 1)', 'rgba(255, 127, 0, 1)', 'rgba(167, 122, 215, 1)', 'rgba(255, 255, 0, 1)', 'rgba(127, 127, 127, 1)'];

										let names = response.data.sNs;
										if(BrandsTypes.includes(window.cSD.sI)) {
											for (let i = 0; i < names.length; i++) {
												let brand = names[i].trim();
												if (brand in window.BrandsData) {
													colors[i] = window.BrandsData[brand].color;
													names[i] = window.BrandsData[brand].name;
												}
											}
										}
										if (window.cSD.sI == 0) {
											for (let i = 0; i < names.length; i++) {
												colors[i] = window.cSD.dD[i].clr;
												names[i] = window.cSD.dD[i].sS.toLocaleString() + ' - ' + window.cSD.dD[i].eS.toLocaleString();
											}
										}
										textvalue = '<div class="sp_sb_cs_gb_graph4_col_2"><canvas id="gr_canvas_0" style="width: 100%; height: 100%"></canvas></div><div class="sp_sb_cs_gb_graph4_col_3">' + 
												'<div class="sp_sb_cs_gb_gc3_series_list">' + 
													'<div class="sp_sb_cs_gb_gc3_sl_in">';
										for (let i = 0; i < response.data.sNs.length; i++) {
											textvalue += '<div class="sp_sb_cs_gb_gc3_sl_series_row _' + i + '" style="top: ' + (4 + 14.28 * i) + '%;"><div class="sp_sb_cs_gb_gc3_sl_sr_color_box" style="background-color: ' + colors[i] + ';"></div><div class="sp_sb_cs_gb_gc3_sl_sr_space"></div><p class="sp_sb_cs_gb_gc3_sl_sr_item_name">' + names[i] + '</p></div>';
										}
										$('.sp_sb_cs_sb_graph_block')[0].innerHTML = textvalue + '</div></div></div><div class="sp_sb_cs_gb_graph4_col_4"><canvas id="gr_canvas_1" style="width: 100%; height: 100%"></canvas></div>';


								let legend_col_height = getPercentHeight($('div.sp_sb_cs_gb_gc3_sl_in'));
								console.log('legend col height: '+legend_col_height);

									if(response.data.sNs.length <=7) {
										let col_height = 35;
										//console.log('legend col_height: '+col_height);
										$('div.sp_sb_cs_gb_gc3_sl_in').css({'min-height':col_height+'%'});
										$('div.sp_sb_cs_gb_gc3_sl_in').css({'max-height':col_height+'%'});
										$('div.sp_sb_cs_gb_gc3_sl_in').css({height:col_height+'%'});
										$('div.sp_sb_cs_gb_gc3_sl_in').css({'top':'calc(25%)'});

										let col_top = 0;
										$("div.sp_sb_cs_gb_gc3_sl_series_row").each(function(index){
											let legend_col_top = getPercentTop($(this));
											//console.log('legend top: '+index+': '+legend_col_top);
											//$(this).css({'top':+(legend_col_top-4)+'%'});
											col_top = col_top+10;
											$(this).css({'top':+col_top+'%'});
											$(this).css({'height':'7%'});
											$(this).css({'min-height':'7%'});
											$(this).css({'max-height':'7%'});
										});
									} else
									if(response.data.sNs.length > 7 && response.data.sNs.length < 15) {
										let col_height_ajust = (response.data.sNs.length)*2.5;
										//console.log('legend col_height_ajust: '+col_height_ajust);
										$('div.sp_sb_cs_gb_gc3_sl_in').css({'min-height':(legend_col_height+col_height_ajust)+'%'});
										$('div.sp_sb_cs_gb_gc3_sl_in').css({'max-height':(legend_col_height+col_height_ajust)+'%'});
										$('div.sp_sb_cs_gb_gc3_sl_in').css({height:(legend_col_height+col_height_ajust)+'%'});
										$('div.sp_sb_cs_gb_gc3_sl_in').css({'top':'calc(15% + 2.2vh)'});

										let col_top = 0;
										$("div.sp_sb_cs_gb_gc3_sl_series_row").each(function(index){
											let legend_col_top = getPercentTop($(this));
											//console.log('legend top: '+index+': '+legend_col_top);
											//$(this).css({'top':+(legend_col_top-4)+'%'});
											col_top = col_top+5.8;
											$(this).css({'top':+col_top+'%'});
											});
									} else
									if(response.data.sNs.length >= 15 && response.data.sNs.length < 18) {
										let col_height = (response.data.sNs.length)*4.5;
										console.log('legend col_height 15: '+col_height);
										let prc_ajs = 30 - response.data.sNs.length;
										$('div.sp_sb_cs_gb_gc3_sl_in').css({'min-height':col_height+'%'});
										$('div.sp_sb_cs_gb_gc3_sl_in').css({'max-height':col_height+'%'});
										$('div.sp_sb_cs_gb_gc3_sl_in').css({height:col_height+'%'});
										$('div.sp_sb_cs_gb_gc3_sl_in').css({'top':'calc('+prc_ajs+'% + 1.3vh)'});

										let col_top = 0;
										$("div.sp_sb_cs_gb_gc3_sl_series_row").each(function(index){
											let legend_col_top = getPercentTop($(this));
											//console.log('legend top: '+index+': '+legend_col_top);
											//$(this).css({'top':+(legend_col_top-4)+'%'});
											col_top = col_top+5;
											$(this).css({'top':+col_top+'%'});
										});
									} else
									if(response.data.sNs.length >= 18) {
										let col_height = (response.data.sNs.length)*3.8;
										//console.log('legend col_height 20: '+col_height);
										let prc_ajs = 30 - response.data.sNs.length*0.8;
										$('div.sp_sb_cs_gb_gc3_sl_in').css({'min-height':col_height+'%'});
										$('div.sp_sb_cs_gb_gc3_sl_in').css({'max-height':col_height+'%'});
										$('div.sp_sb_cs_gb_gc3_sl_in').css({height:col_height+'%'});
										$('div.sp_sb_cs_gb_gc3_sl_in').css({'top':'calc('+prc_ajs+'% + 0.7vh)'});

										let col_top = 0;
										$("div.sp_sb_cs_gb_gc3_sl_series_row").each(function(index){
											let legend_col_top = getPercentTop($(this));
											//console.log('legend top: '+index+': '+legend_col_top);
											//$(this).css({'top':+(legend_col_top-4)+'%'});
											col_top = col_top+4.6;
											$(this).css({'top':+col_top+'%'});
										});
									}
										
										
										Chart.defaults.global.defaultFontSize = 13;// до слияния 8
										Chart.defaults.global.defaultFontStyle = 'bold';
										Chart.defaults.global.defaultFontColor = 'black';
										Chart.defaults.global.defaultFontFamily = 'Verdana';

										let data_0 = {};
										let ctx_0 = document.getElementById('gr_canvas_0').getContext('2d');
										let data_1 = {};
										let ctx_1 = document.getElementById('gr_canvas_1').getContext('2d');
										let dataArr0 = [];
										let ttVal0 = [];
										let dataArr1 = [];
										let ttVal1 = [];
										let title0 = response.data.titlemtm0;
										let title1 = response.data.titlemtm1;
										for (let i = 0; i < response.data.sNs.length; i++) {
											dataArr0.push(response.data.rs[i].mtmpp);
											ttVal0.push(response.data.rs[i].mtmpv);
										}
										for (let i = 0; i < response.data.sNs.length; i++) {
											dataArr1.push(response.data.rs[i].mtmcp);
											ttVal1.push(response.data.rs[i].mtmcv);
										}
										data_0 = {
											labels: response.data.sNs,
											datasets: [
												{
													data: dataArr0,
													backgroundColor: colors,
													tooltipsValues: ttVal0
												}
											]
										};
										data_1 = {
											labels: response.data.sNs,
											datasets: [
												{
													data: dataArr1,
													backgroundColor: colors,
													tooltipsValues: ttVal1
												}
											]
										};
										window.chart_0 = new Chart(ctx_0, {
											type: 'doughnut',
											data: data_0,
											options: {
												devicePixelRatio: window.devicePixelRatio*res_coef,
												title: {
													display: true,
													text: title0,
													position: "bottom",
													fontSize: 25,
													padding: 23
												},
												legend: {
													display: false,
												},
												hover: {
													mode: null,
													events: [],
												},
												tooltips: {
													mode: 'index',
													backgroundColor: 'rgba(255, 255, 255, 0.75)',
													titleFontSize: 14,
													titleFontColor: 'rgba(0, 0, 0, 1)',
													bodyFontSize: 14,
													bodyFontColor: 'rgba(0, 0, 0, 1)',
													bodyFontStyle: 'normal',
													intersect: false,
													callbacks: {
														label: function(tooltipItem, data) {
															return data.labels[tooltipItem.index] + ': ' + data.datasets[tooltipItem.datasetIndex].tooltipsValues[tooltipItem.index];
														},
													},
												},
												responsive: true,
												plugins: {
													legend: {
														display: false,
													},
													datalabels: {
														textAlign: 'center',
														color: function(context) {
															var index = context.dataIndex;
															var color = context.dataset.backgroundColor[index];
															var result_rgb = rgbaToRgb(color);
															var cc = contrastingColor(result_rgb);
															return '#'+cc;
														},														
														font: function(context) {
															var width = context.chart.width;
															var index = context.dataIndex;
															var value = context.dataset.data[index];
															var size;
															switch(true) {
																case value < 5 && value > 3: size = InFontSize; break;
																case value <= 3: size = InFontSize - decrFont; break;
																default: size = InFontSize+6;
															}
															//console.log('value: '+value+' | size: '+size+' | weight: '+wgt);
															return {
																style:	(value < 5) ? 'normal' : 'bold',
																"size": size
															};
														},
														formatter: function(value, ctx_0) {
															if (Number(value) > 1.99) {
																if ((Number(value) * 10) % 10) {
																	return value;
																} else {
																	return value + '.0';
																}
															} else {
																return '';
															}
														},
													},
												},
											},
											plugins: [{
											  beforeInit: function(chart) {
												  chart.data.labels.forEach(function(e, i, a) {
													  if (/\n/.test(e)) {
														  a[i] = e;
													  }
												  });
											  }
										   }]
										});
										window.chart_1 = new Chart(ctx_1, {
											type: 'doughnut',
											data: data_1,
											options: {
												devicePixelRatio: window.devicePixelRatio*res_coef,
												title: {
													display: true,
													text: title1,
													position: "bottom",
													fontSize: 25,
													padding: 23
												},
												legend: {
													display: false,
												},
												hover: {
													mode: null,
													events: [],
												},
												tooltips: {
													mode: 'index',
													backgroundColor: 'rgba(255, 255, 255, 0.75)',
													titleFontSize: 14,
													titleFontColor: 'rgba(0, 0, 0, 1)',
													bodyFontSize: 14,
													bodyFontColor: 'rgba(0, 0, 0, 1)',
													bodyFontStyle: 'normal',
													intersect: false,
													callbacks: {
														label: function(tooltipItem, data) {
															return data.labels[tooltipItem.index] + ': ' + data.datasets[tooltipItem.datasetIndex].tooltipsValues[tooltipItem.index];
														},
													},
												},
												responsive: true,
												plugins: {
													legend: {
														display: false,
													},
													datalabels: {
														textAlign: 'center',
														color: function(context) {
															var index = context.dataIndex;
															var color = context.dataset.backgroundColor[index];
															var result_rgb = rgbaToRgb(color);
															var cc = contrastingColor(result_rgb);
															return '#'+cc;
														},														
														font: function(context) {
															var width = context.chart.width;
															var index = context.dataIndex;
															var value = context.dataset.data[index];
															var size;
															switch(true) {
																case value < 5 && value > 3: size = InFontSize; break;
																case value <= 3: size = InFontSize - decrFont; break;
																default: size = InFontSize+6;
															}
															//console.log('value: '+value+' | size: '+size+' | weight: '+wgt);
															return {
																style:	(value < 5) ? 'normal' : 'bold',
																"size": size
															};
														},
														formatter: function(value, ctx_1) {
															if (Number(value) > 1.99) {
																if ((Number(value) * 10) % 10) {
																	return value;
																} else {
																	return value + '.0';
																}
															} else {
																return '';
															}
														},
													},
												},
											},
											plugins: [{
											  beforeInit: function(chart) {
												  chart.data.labels.forEach(function(e, i, a) {
													  if (/\n/.test(e)) {
														  a[i] = e;
													  }
												  });
											  }
										   }]
										});
									}
									else if ((window.sD[window.cP].gT === '7') || (window.sD[window.cP].gT === '8') || (window.sD[window.cP].gT === '9')) {
									//console.log(response);
											let textvalue = '';
											
											let colwidth = 89 / response.data.lUs.length;
											//console.log('colwidth: '+colwidth);
											
											let percents = ['100%', '100%', ''];
											for (let i = 3; i < response.data.lUs.length - 1; i++) {
												if (Number(response.data.lUs[0]) != 0) {
													percents[i] = String(Math.round(Number(response.data.lUs[i]) / Number(response.data.lUs[0]) * 100)) + '%';
												} else {
													percents[i] = '100%';
												}
												i++;
												if (Number(response.data.lUs[1]) != 0) {
													percents[i] = String(Math.round(Number(response.data.lUs[i]) / Number(response.data.lUs[1]) * 100)) + '%';
												} else {
													percents[i] = '100%';
												}
											}

											let colors = ['rgba(0, 114, 218, 1)', 'rgba(0, 156, 73, 1)', 'rgba(240, 66, 121, 1)', 'rgba(255, 127, 0, 1)', 'rgba(167, 122, 215, 1)', 'rgba(255, 255, 0, 1)', 'rgba(255, 000, 128, 0.4)', 'rgba(255, 99, 132, 0.9)', 'rgba(197, 255, 10, 1)', 'rgba(127, 127, 127, 1)'];

											let names = response.data.sNs;
											if(BrandsTypes.includes(window.cSD.sI)) {
												for (let i = 0; i < names.length; i++) {
													let brand = names[i].trim();
													if (brand in window.BrandsData) {
														colors[i] = window.BrandsData[brand].color;
														names[i] = window.BrandsData[brand].name;
													}
												}
											}
											if (window.cSD.sI == 0) {
												for (let i = 0; i < names.length; i++) {
													colors[i] = window.cSD.dD[i].clr;
													names[i] = window.cSD.dD[i].sS.toLocaleString() + ' - ' + window.cSD.dD[i].eS.toLocaleString();
												}
											}
											textvalue = '<div class="sp_sb_cs_gb_graph5_col_0"><canvas id="gr_canvas_0" style="width: 100%; height: 100%"></canvas></div>' + 
												'<div class="sp_sb_cs_gb_graph5_col_1">' + 
													'<div class="sp_sb_cs_gb_g5c1_series_list">' + 
														'<div class="sp_sb_cs_gb_g5c1_sl_in">';
											for (let i = 0; i < response.data.sNs.length; i++) {
												textvalue += '<div class="sp_sb_cs_gb_g5c1_sl_series_row _' + i + '" style="top: ' + (4 + 7 * i) + '%;"><div class="sp_sb_cs_gb_g5c1_sl_sr_color_box" style="background-color: ' + colors[i] + ';"></div><div class="sp_sb_cs_gb_g5c1_sl_sr_space"></div><p class="sp_sb_cs_gb_g5c1_sl_sr_item_name">' + names[i] + '</p></div>';
											}
											$('.sp_sb_cs_sb_graph_block')[0].innerHTML = textvalue + '</div></div></div><div class="sp_sb_cs_gb_graph5_col_2"><canvas id="gr_canvas_1" style="width: 100%; height: 100%"></canvas></div>';

											//console.log('sBNs length: '+response.data.sBNs.length);
 											let legend_col_width = getPercentWidth($('div.sp_sb_cs_gb_graph5_col_1'));
											let left_col_width = 100/response.data.sBNs.length*(100-legend_col_width)/100;
											let right_col_width = (100-100/response.data.sBNs.length)*(100-legend_col_width)/100;
											//console.log('left col width: '+left_col_width);
											//console.log('right col width: '+right_col_width);
											//console.log('legend col width: '+legend_col_width);
											$('div.sp_sb_cs_gb_graph5_col_0').css({'min-width':left_col_width+'%'});
											$('div.sp_sb_cs_gb_graph5_col_0').css({'max-width':left_col_width+'%'});
											$('div.sp_sb_cs_gb_graph5_col_0').css({width:left_col_width+'%'});
											$('div.sp_sb_cs_gb_graph5_col_1').css({left:left_col_width+'%'});
											$('div.sp_sb_cs_gb_graph5_col_2').css({left:(left_col_width+legend_col_width)+'%'});
											$('div.sp_sb_cs_gb_graph5_col_2').css({'min-width':right_col_width+'%'});
											$('div.sp_sb_cs_gb_graph5_col_2').css({'max-width':right_col_width+'%'});
											$('div.sp_sb_cs_gb_graph5_col_2').css({width:right_col_width+'%'});

											let legend_col_height = getPercentHeight($('div.sp_sb_cs_gb_g5c1_sl_in'));
											//console.log('legend col height: '+legend_col_height);
											
											/*if(response.data.sNs.length > 7) {
												let col_height = (response.data.sNs.length)*4;
												console.log('legend col_height: '+col_height);
												$('div.sp_sb_cs_gb_g5c1_sl_in').css({'min-height':col_height+'%'});
												$('div.sp_sb_cs_gb_g5c1_sl_in').css({'max-height':col_height+'%'});
												$('div.sp_sb_cs_gb_g5c1_sl_in').css({height:col_height+'%'});
												$('div.sp_sb_cs_gb_g5c1_sl_in').css({'top':'calc(15% + 0.7vh)'});
												let col_top = 0;
												$("div.sp_sb_cs_gb_g5c1_sl_series_row").each(function(index){
													let legend_col_top = getPercentTop($(this));
													console.log('legend top: '+index+': '+legend_col_top);
													//$(this).css({'top':+(legend_col_top-4)+'%'});
													col_top = col_top+5;
													$(this).css({'top':+col_top+'%'});
												});
											}*/

											if(response.data.sNs.length <=7) {
												let col_height = 35;
												//console.log('legend col_height: '+col_height);
												$('div.sp_sb_cs_gb_g5c1_sl_in').css({'min-height':col_height+'%'});
												$('div.sp_sb_cs_gb_g5c1_sl_in').css({'max-height':col_height+'%'});
												$('div.sp_sb_cs_gb_g5c1_sl_in').css({height:col_height+'%'});
												//$('div.sp_sb_cs_gb_g5c1_sl_in').css({'top':'calc(20% + 2.2vh)'});
												$('div.sp_sb_cs_gb_g5c1_sl_in').css({'top':''});
												$('div.sp_sb_cs_gb_g5c1_sl_in').css({'vertical-align':'middle'});

												let col_top = 0;
												$("div.sp_sb_cs_gb_g5c1_sl_series_row").each(function(index){
													let legend_col_top = getPercentTop($(this));
													//console.log('legend top: '+index+': '+legend_col_top);
													//$(this).css({'top':+(legend_col_top-4)+'%'});
													col_top = col_top+10;
													$(this).css({'top':+col_top+'%'});
													$(this).css({'height':'7%'});
													$(this).css({'min-height':'7%'});
													$(this).css({'max-height':'7%'});
												});
											} else
											if(response.data.sNs.length > 7 && response.data.sNs.length < 15) {
												let col_height_ajust = (response.data.sNs.length)*2.5;
												//console.log('legend col_height_ajust: '+col_height_ajust);
												$('div.sp_sb_cs_gb_g5c1_sl_in').css({'min-height':(legend_col_height+col_height_ajust)+'%'});
												$('div.sp_sb_cs_gb_g5c1_sl_in').css({'max-height':(legend_col_height+col_height_ajust)+'%'});
												$('div.sp_sb_cs_gb_g5c1_sl_in').css({height:(legend_col_height+col_height_ajust)+'%'});
												//$('div.sp_sb_cs_gb_g5c1_sl_in').css({'top':'calc(20% + 2.2vh)'});
												$('div.sp_sb_cs_gb_g5c1_sl_in').css({'top':''});
												$('div.sp_sb_cs_gb_g5c1_sl_in').css({'vertical-align':'middle'});

												let col_top = 0;
												$("div.sp_sb_cs_gb_g5c1_sl_series_row").each(function(index){
													let legend_col_top = getPercentTop($(this));
													//console.log('legend top: '+index+': '+legend_col_top);
													//$(this).css({'top':+(legend_col_top-4)+'%'});
													col_top = col_top+6;
													$(this).css({'top':+col_top+'%'});
												});
											} else
											if(response.data.sNs.length >= 15 &&response.data.sNs.length < 20) {
												let col_height = (response.data.sNs.length)*4;
												console.log('legend col_height 15: '+col_height);
												$('div.sp_sb_cs_gb_g5c1_sl_in').css({'min-height':col_height+'%'});
												$('div.sp_sb_cs_gb_g5c1_sl_in').css({'max-height':col_height+'%'});
												$('div.sp_sb_cs_gb_g5c1_sl_in').css({height:col_height+'%'});
												//$('div.sp_sb_cs_gb_g5c1_sl_in').css({'top':'calc(20% + 1.3vh)'});
												$('div.sp_sb_cs_gb_g5c1_sl_in').css({'top':''});
												$('div.sp_sb_cs_gb_g5c1_sl_in').css({'vertical-align':'middle'});
												
												let col_top = 0;
												$("div.sp_sb_cs_gb_g5c1_sl_series_row").each(function(index){
													let legend_col_top = getPercentTop($(this));
													//console.log('legend top: '+index+': '+legend_col_top);
													//$(this).css({'top':+(legend_col_top-4)+'%'});
													col_top = col_top+5;
													$(this).css({'top':+col_top+'%'});
												});
											} else
											if(response.data.sNs.length >= 20) {
												let col_height = (response.data.sNs.length)*3.5;
												//console.log('legend col_height 20: '+col_height);
												$('div.sp_sb_cs_gb_g5c1_sl_in').css({'min-height':col_height+'%'});
												$('div.sp_sb_cs_gb_g5c1_sl_in').css({'max-height':col_height+'%'});
												$('div.sp_sb_cs_gb_g5c1_sl_in').css({height:col_height+'%'});
												//$('div.sp_sb_cs_gb_g5c1_sl_in').css({'top':'calc(15% + 0.7vh)'});
												$('div.sp_sb_cs_gb_g5c1_sl_in').css({'top':''});
												$('div.sp_sb_cs_gb_g5c1_sl_in').css({'vertical-align':'middle'});

												let col_top = 0;
												$("div.sp_sb_cs_gb_g5c1_sl_series_row").each(function(index){
													let legend_col_top = getPercentTop($(this));
													//console.log('legend top: '+index+': '+legend_col_top);
													//$(this).css({'top':+(legend_col_top-4)+'%'});
													col_top = col_top+4.5;
													$(this).css({'top':+col_top+'%'});
												});
											}
											let $cnt_string = 'LC';
											let $msscht = 10000000;
											let $mst_string = 'Bln.' + $cnt_string;
											switch (window.cSD.sP) {
												case '0':
													$cnt_string = 'LC';
													switch (window.cSD.sM) {
														case '0':
															$msscht = 10000000;
															$mst_string = 'Bln.' + $cnt_string;
															break;
														case '1':
															$msscht = 10000;
															$mst_string = 'Mln.' + $cnt_string;
															break;
														case '2':
															$msscht = 10;
															$mst_string = 'Tsd.' + $cnt_string;
															break;
														default:
															$msscht = 10000000;
															$mst_string = 'Bln.' + $cnt_string;
													}
													break;
												case '1':
													$cnt_string = 'Euro';
													switch (window.cSD.sM) {
														case '0':
															$msscht = 10000000;
															$mst_string = 'Bln.' + $cnt_string;
															break;
														case '1':
															$msscht = 10000;
															$mst_string = 'Mln.' + $cnt_string;
															break;
														case '2':
															$msscht = 10;
															$mst_string = 'Tsd.' + $cnt_string;
															break;
														default:
															$msscht = 10000000;
															$mst_string = 'Bln.' + $cnt_string;
													}
													break;
												case '2':
													$msscht = 1;//	tmp
													$mst_string = 'units';
													break;
												default:
													$cnt_string = 'LC';
													switch (window.cSD.sM) {
														case '0':
															$msscht = 10000000;
															$mst_string = 'Bln.' + $cnt_string;
															break;
														case '1':
															$msscht = 10000;
															$mst_string = 'Mln.' + $cnt_string;
															break;
														case '2':
															$msscht = 10;
															$mst_string = 'Tsd.' + $cnt_string;
															break;
														default:
															$msscht = 10000000;
															$mst_string = 'Bln.' + $cnt_string;
													}
											}
											for (let i = 0; i < response.data.lUs.length; i++) {
												 response.data.lUs[i] = Math.round(response.data.lUs[i] / $msscht) / 100;
											}

											var lg_fnt_sz;
											switch(true) {
												case response.data.lUs.length > 30: lg_fnt_sz = 0.75; break;
												case response.data.lUs.length > 27: lg_fnt_sz = 0.8; break;
												case response.data.lUs.length > 24: lg_fnt_sz = 0.9; break;
												default: lg_fnt_sz = 1;
											}
											console.log('lg_fnt_sz: '+lg_fnt_sz);
											$('div.sp_sb_cs_sb_data_block').css({'font-size':lg_fnt_sz+'em'});

											textvalue= '';
											colwidth = left_col_width / 2;
											for (let i = 0; i < 2; i++) {
												let coef2ajust = 0;
												if(i%2) { coef2ajust = colwidth/10; }
												textvalue += '<div class="sp_sb_cs_db_data_col _' + i + '" style="left: ' + (colwidth * i - coef2ajust) + '%; width: ' + colwidth + '%; max-width: ' + colwidth + '%; min-width: ' + colwidth + '%"><p class="paragraph_1">' + response.data.lUs[i] + '<br><font color="' + ((response.data.lCs[i] === "Green") ? '008000">+' : 'c00000">') + ((response.data.lDs[i] != '') ? (response.data.lDs[i] + '%') : '') + '</font>' + '<br>' + percents[i] + '</p></div>';
											}
											
											textvalue += '<div class="sp_sb_cs_db_data_col _' + 2 + '" style="left: ' + (colwidth * 2) + '%; width: ' + legend_col_width + '%; max-width: ' + legend_col_width + '%; min-width: ' + legend_col_width + '%"><p class="paragraph_1">' + $mst_string + '<br>+/- % PY<br>Horiz.% out. GT</p></div>';
											
											for (let i = 3; i < response.data.lUs.length; i++) {
												let coef2ajust = colwidth/15;
												if(i%2 == 0) { coef2ajust = -coef2ajust; }
												//console.log('coef2ajust: '+coef2ajust);
												textvalue += '<div class="sp_sb_cs_db_data_col _' + i + '" style="left: ' + (colwidth * (i-1) + legend_col_width + coef2ajust - 0.3) + '%; width: ' + colwidth + '%; max-width: ' + colwidth + '%; min-width: ' + colwidth + '%"><p class="paragraph_1">' + response.data.lUs[i] + '<br><font color="' + ((response.data.lCs[i] === "Green") ? '008000">+' : 'c00000">') + ((response.data.lDs[i] != '') ? (response.data.lDs[i] + '%') : '') + '</font>' + '<br>' + percents[i] + '</p></div>'
											}

											$('.sp_sb_cs_sb_data_block')[0].innerHTML = '<b>' + textvalue + '</b>';
											
											Chart.defaults.global.defaultFontSize = 12;
											Chart.defaults.global.defaultFontStyle = 'bold';
											Chart.defaults.global.defaultFontColor = 'black';
											Chart.defaults.global.defaultFontFamily = 'Verdana';
											Chart.defaults.global.elements.rectangle.borderColor = 'rgba(25, 25, 25, 1)';
											Chart.defaults.global.elements.rectangle.borderWidth = 0.7;

											//console.log('response.data.lYs: '+response.data.lYs);
											let tmpArr = [response.data.lYs[0], response.data.lYs[1]];
											let data_0 = {
												labels: ['', ''],//[response.data.lYs[0], response.data.lYs[1]],
												datasets: [],
											};
											response.data.lYs.splice(0, 3);
											let data_1 = {
												labels: [],
												datasets: [],
											};

											let num = 0;
											for (let i = 0; i < response.data.lYs.length; i++) {
												if (i % 2) {
													num++;
													let txt = ' ';
													if(response.data.sBNs.length > 4) { 
														//num += '\n';
														txt += '\n';
													}
													//data_1.labels.push(txt);
													data_1.labels.push('');
												}
											}
											let ctx_0 = document.getElementById('gr_canvas_0').getContext('2d');
											let ctx_1 = document.getElementById('gr_canvas_1').getContext('2d');
											let sums0 = [];
											let sums1 = [];
											let sums2 = [];
											for (let i = 0; i < response.data.rs[0].iD.length; i++) {
												sums0.unshift(0);
												sums1.unshift(0);
												sums2.unshift(0);
											}
											for (let i = 0; i < response.data.sNs.length; i++) {
												data_0.datasets.unshift({label: response.data.sNs[i], backgroundColor: colors[i], data: [], tooltipsValues: [], lbl: [], lbl2: []});
												for (let j = 0; j < 2; j++) {
													data_0.datasets[0].data.push(response.data.rs[i].iD[j].idppt3);
													data_0.datasets[0].tooltipsValues.push(response.data.rs[i].iD[j].idv);
													data_0.datasets[0].lbl.push(response.data.rs[i].iD[j].lbl);
													data_0.datasets[0].lbl2.push(tmpArr[j]);
													sums0[j] += Number(response.data.rs[i].iD[j].idppt3);
												}
											}
											for (let i = 0; i < response.data.sNs.length; i++) {
												data_1.datasets.unshift({label: response.data.sNs[i], backgroundColor: colors[i], data: [], tooltipsValues: [], lbl: [], lbl2: [], stack: 'Stack 0'});
												data_1.datasets.unshift({label: response.data.sNs[i], backgroundColor: colors[i], data: [], tooltipsValues: [], lbl: [], lbl2: [], stack: 'Stack 1'});
												for (let j = 3; j < response.data.rs[i].iD.length; j++) {
													if (j % 2) {
														data_1.datasets[0].data.push(response.data.rs[i].iD[j].idppt3);
														data_1.datasets[0].tooltipsValues.push(response.data.rs[i].iD[j].idv);
														data_1.datasets[0].lbl.push(response.data.rs[i].iD[j].lbl);
														data_1.datasets[0].lbl2.push([tmpArr[1], tmpArr[0]]);
														sums1[j] += Number(response.data.rs[i].iD[j].idppt3);
													} else {
														data_1.datasets[1].data.push(response.data.rs[i].iD[j].idppt3);
														data_1.datasets[1].tooltipsValues.push(response.data.rs[i].iD[j].idv);
														data_1.datasets[1].lbl.push(response.data.rs[i].iD[j].lbl);
														data_1.datasets[1].lbl2.push([tmpArr[1], tmpArr[0]]);
														sums2[j] += Number(response.data.rs[i].iD[j].idppt3);
													}
												}
											}
											let max_0 = Math.max.apply(null, sums0) * 1.05;
											let max_1 = Math.max.apply(null, sums1) * 1.05;
											let max_2 = Math.max.apply(null, sums2) * 1.05;
											if (max_2 > max_1) {
												max_1 = max_2;
											}
													
											//console.log('DATA_1: '+JSON.stringify(data_1));

											switch(true) {
												case window.cSD.ssC.length > 7: InFontSize -= 4; decrFont = -1;  break;
												case window.cSD.ssC.length <= 7 && window.cSD.ssC.length > 4: InFontSize -= 2; decrFont = 0; break;
												default: break;
											}
											
											window.chart_0 = new Chart(ctx_0, {
												type: 'bar',
												data: data_0,
												options: {
													devicePixelRatio: window.devicePixelRatio*res_coef,
													hover: {
														mode: null,
														events: [],
														animationDuration: 0,
													},
													legend: {
														display: false,
													},
													tooltips: {
														mode: 'index',
														backgroundColor: 'rgba(255, 255, 255, 0.75)',
														titleFontSize: 14,
														titleFontColor: 'rgba(0, 0, 0, 1)',
														bodyFontSize: 14,
														bodyFontColor: 'rgba(0, 0, 0, 1)',
														bodyFontStyle: 'normal',
														intersect: false,
														callbacks: {
															label: function(tooltipItem, data) {
																return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].tooltipsValues[tooltipItem.index];
															},
														},
													},
													responsive: true,
													scales: {
														xAxes: [{
															gridLineHeight: 20,
															gridLines: {
																display: false,
																drawBorder: false
															},
															stacked: true,
															barPercentage: 1.0,
		   													categoryPercentage: 0.9,
															position: 'top',
														}],
														yAxes: [{
															display: false,
															gridLines: {
																display: false,
																drawBorder: false
															},
															stacked: true,
															ticks: {
																max: max_0,
																display: false,
															},
														}],
													},
													plugins: {
														datalabels: {
															textAlign: 'center',
																color: function(context) {
																  var index = context.dataIndex;
																  var color = context.dataset.backgroundColor;
																  var result_rgb = rgbaToRgb(color);
																  var cc = contrastingColor(result_rgb);
																  return '#'+cc;
																},
															font: function(context) {
																var width = context.chart.width;
																var index = context.dataIndex;
																var value = context.dataset.data[index];
																var size;
																switch(true) {
																	case value < 5 && value > 3: size = InFontSize; break;
																	case value <= 3: size = InFontSize - decrFont; break;
																	default: size = InFontSize+6;
																}
																//console.log('value: '+value+' | size: '+size+' | weight: '+wgt);
																return {
																	style:	(value < 5) ? 'normal' : 'bold',
																	"size": size
																};
															},
															formatter: function(value, ctx_0) {
																if (ctx_0.dataset.label === 'All') {
																	return String(Math.floor(Number(value) * 50) / 10);
																} else if (Number(value) > 1.99) {
																	if ((Number(value) * 10) % 10) {
																		return value;
																	} else if(Number(value) == 100) {
																		return 100;
																	} else {
																		return value + '.0';
																	}
																} else {
																	return '';
																}
															},
														},
													},
													animation: {
														duration: 1,
														onComplete: function() {
															let chartInstance = this.chart,
																ctxthis = chartInstance.ctx;

															ctxthis.textAlign = 'center';
															ctxthis.textBaseline = 'bottom';
															let datas = this.data;
															let maxYScales = 0;
															this.data.labels.forEach(function(label, label_index) {
																let colsumm = 0;
																datas.datasets.forEach(function(dataset, dataset_index) {
																	let meta = chartInstance.controller.getDatasetMeta(dataset_index);
																	colsumm += Number(dataset.data[label_index]);
																	if (dataset_index == datas.datasets.length - 1) {

																			if(response.data.sBNs.length > 6) {
																				ctxthis.font = Chart.helpers.fontString(11, 'normal', Chart.defaults.global.defaultFontFamily);
																			} else {
																				ctxthis.font = Chart.helpers.fontString(14, 'normal', Chart.defaults.global.defaultFontFamily);
																			}
																			let tmpY = 20;
																			if(response.data.sBNs.length > 8) {
																				tmpY = 35;
																				let tmpA = dataset.lbl2[label_index].split(/\-/);
																				ctxthis.fillText((tmpA[0] + '-'), meta.data[label_index]._model.x, meta.data[label_index]._model.y - 20);
																				ctxthis.fillText(tmpA[1], meta.data[label_index]._model.x, meta.data[label_index]._model.y - 5);
																			} else {
																				ctxthis.fillText(dataset.lbl2[label_index], meta.data[label_index]._model.x, meta.data[label_index]._model.y - 5);
																			}
																		
																		ctxthis.font = Chart.helpers.fontString(17, 'bold', Chart.defaults.global.defaultFontFamily);
																		
																		if (label_index < 2) {
																			if (label_index % 2) {
																				ctxthis.fillText(dataset.lbl[label_index], (meta.data[label_index - 1]._model.x + meta.data[label_index]._model.x) / 2, meta.data[label_index]._model.y - tmpY);
																			}
																		} else {
																			if (!(label_index % 2)) {
																				ctxthis.fillText(dataset.lbl[label_index], (meta.data[label_index - 1]._model.x + meta.data[label_index]._model.x) / 2, meta.data[label_index]._model.y - tmpY);
																			}
																		}
																	}
																});
															});
														},
													},
												},
												plugins: [{
												  beforeInit: function(chart) {
													  chart.data.labels.forEach(function(e, i, a) {
														  if (/\n/.test(e)) {
															  if(response.data.sBNs.length > 8) {
																  a[i] = e.split(/\n/);
															  } else {
																a[i] = e.replace(/\n/, '');
															  }
														  }
													  });
												  }
											   }]
											});
											window.chart_1 = new Chart(ctx_1, {
												type: 'bar',
												data: data_1,
												options: {
													devicePixelRatio: window.devicePixelRatio*res_coef,
													hover: {
														mode: null,
														events: [],
														animationDuration: 0,
													},
													legend: {
														display: false,
													},
													tooltips: {
														mode: 'index',
														backgroundColor: 'rgba(255, 255, 255, 0.75)',
														titleFontSize: 14,
														titleFontColor: 'rgba(0, 0, 0, 1)',
														bodyFontSize: 14,
														bodyFontColor: 'rgba(0, 0, 0, 1)',
														bodyFontStyle: 'normal',
														intersect: false,
														callbacks: {
															label: function(tooltipItem, data) {
																return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].tooltipsValues[tooltipItem.index];
															},
														},
													},
													responsive: true,
													scales: {
														xAxes: [{
															gridLineHeight: 20,
															gridLines: {
																display: false,
																drawBorder: false
															},
															stacked: true,
															barPercentage: 0.9,
		   													categoryPercentage: 0.9,
															position: 'top'
														}],
														yAxes: [{
															display: false,
															gridLines: {
																display: false,
																drawBorder: false
															},
															stacked: true,
															ticks: {
																max: max_1,
																display: false,
															},
														}],
													},
													plugins: {
														datalabels: {
															textAlign: 'center',
																color: function(context) {
																  var index = context.dataIndex;
																  var color = context.dataset.backgroundColor;
																  var result_rgb = rgbaToRgb(color);
																  var cc = contrastingColor(result_rgb);
																  return '#'+cc;
																},
															font: function(context) {
																var width = context.chart.width;
																var index = context.dataIndex;
																var value = context.dataset.data[index];
																var size;
																switch(true) {
																	case value < 5 && value > 3: size = InFontSize; break;
																	case value <= 3: size = InFontSize - decrFont; break;
																	default: size = InFontSize+6;
																}
																//console.log('value: '+value+' | size: '+size+' | weight: '+wgt);
																return {
																	style:	(value < 5) ? 'normal' : 'bold',
																	"size": size
																};
															},
															formatter: function(value, ctx_1) {
																if (ctx_1.dataset.label === 'All') {
																	return String(Math.floor(Number(value) * 50) / 10);
																} else if (Number(value) > 1.99) {
																	if ((Number(value) * 10) % 10) {
																		return value;
																	} else if(Number(value) == 100) {
																		return 100;
																	} else {
																		return value + '.0';
																	}
																} else {
																	return '';
																}
															},
														},
													},
													animation: {
														duration: 1,
														onComplete: function() {
															let chartInstance = this.chart,
																ctxthis = chartInstance.ctx;
															ctxthis.textAlign = 'center';
															ctxthis.textBaseline = 'bottom';
															let datas = this.data;
															let maxYScales = 0;
															this.data.labels.forEach(function(label, label_index) {
																let colsumm = 0;
																			let tmpY = 15;
																			if(response.data.sBNs.length > 8) {
																				tmpY = 30;
																			}
																datas.datasets.forEach(function(dataset, dataset_index) {
																	colsumm += Number(dataset.data[label_index]);
																	if (dataset_index == datas.datasets.length - 1) {
																		let meta1 = chartInstance.controller.getDatasetMeta(dataset_index);
																		let meta2 = chartInstance.controller.getDatasetMeta(dataset_index - 1);
																		let maxY = ((meta1.data[label_index]._model.y < meta2.data[label_index]._model.y) ? meta1.data[label_index]._model.y : meta2.data[label_index]._model.y) - 5;
																		ctxthis.font = Chart.helpers.fontString(17, 'bold', Chart.defaults.global.defaultFontFamily);
																		ctxthis.fillText(dataset.lbl[label_index], (meta1.data[label_index]._model.x + meta2.data[label_index]._model.x) / 2, maxY - tmpY);
																			if(response.data.sBNs.length > 6) {
																				ctxthis.font = Chart.helpers.fontString(11, 'normal', Chart.defaults.global.defaultFontFamily);
																			} else {
																				ctxthis.font = Chart.helpers.fontString(14, 'normal', Chart.defaults.global.defaultFontFamily);
																			}
																			if(response.data.sBNs.length > 8) {
																				let tmpA = dataset.lbl2[label_index][0].split(/\-/);
																				ctxthis.fillText((tmpA[0] + '-'), meta1.data[label_index]._model.x, maxY - 15);
																				ctxthis.fillText(tmpA[1], meta1.data[label_index]._model.x, maxY);
																				tmpA =dataset.lbl2[label_index][1].split(/\-/);
																				ctxthis.fillText((tmpA[0] + '-'), meta2.data[label_index]._model.x, maxY - 15);
																				ctxthis.fillText(tmpA[1], meta2.data[label_index]._model.x, maxY);
																			} else {
																				ctxthis.fillText(dataset.lbl2[label_index][0], meta1.data[label_index]._model.x, maxY);
																				ctxthis.fillText(dataset.lbl2[label_index][1], meta2.data[label_index]._model.x, maxY);
																			}
																	}
																});
															});
															let newh = $('#gr_canvas_1').css('height');
															//console.log('newh: '+newh);
															$('#gr_canvas_0').css({'height':newh});
														},
													},
												},
												plugins: [{
												  beforeInit: function(chart) {
													  chart.data.labels.forEach(function(e, i, a) {
														  if (/\n/.test(e)) {
															  if(response.data.sBNs.length > 8) {
																  a[i] = e.split(/\n/);
															  } else {
																a[i] = e.replace(/\n/, '');
															  }
														  }
													  });
												  }
											   }]
											});
										}
										else if ((window.sD[window.cP].gT === '10') || (window.sD[window.cP].gT === '11') || (window.sD[window.cP].gT === '12')) {
												
												let textvalue = '';
												
												let colwidth = 89 / response.data.lUs.length;
												//console.log('colwidth: '+colwidth);
												
												let percents = ['100%', '100%', '100%', ''];
												for (let i = 4; i < response.data.lUs.length - 1; i++) {
													if (Number(response.data.lUs[0]) != 0) {
														percents[i] = String(Math.round(Number(response.data.lUs[i]) / Number(response.data.lUs[0]) * 100)) + '%';
													} else {
														percents[i] = '100%';
													}
													i++;
													if (Number(response.data.lUs[1]) != 0) {
														percents[i] = String(Math.round(Number(response.data.lUs[i]) / Number(response.data.lUs[1]) * 100)) + '%';
													} else {
														percents[i] = '100%';
													}
													i++;
													if (Number(response.data.lUs[2]) != 0) {
														percents[i] = String(Math.round(Number(response.data.lUs[i]) / Number(response.data.lUs[2]) * 100)) + '%';
													} else {
														percents[i] = '100%';
													}
												}

												let colors = ['rgba(0, 114, 218, 1)', 'rgba(0, 156, 73, 1)', 'rgba(240, 66, 121, 1)', 'rgba(255, 127, 0, 1)', 'rgba(167, 122, 215, 1)', 'rgba(255, 255, 0, 1)', 'rgba(255, 000, 128, 0.4)', 'rgba(255, 99, 132, 0.9)', 'rgba(197, 255, 10, 1)', 'rgba(127, 127, 127, 1)'];
								
												let names = response.data.sNs;
												if(BrandsTypes.includes(window.cSD.sI)) {
													for (let i = 0; i < names.length; i++) {
														let brand = names[i].trim();
														if (brand in window.BrandsData) {
															colors[i] = window.BrandsData[brand].color;
															names[i] = window.BrandsData[brand].name;
														}
													}
												}
												if (window.cSD.sI == 0) {
													for (let i = 0; i < names.length; i++) {
														colors[i] = window.cSD.dD[i].clr;
														names[i] = window.cSD.dD[i].sS.toLocaleString() + ' - ' + window.cSD.dD[i].eS.toLocaleString();
													}
												}
												textvalue = '<div class="sp_sb_cs_gb_graph5_col_0"><canvas id="gr_canvas_0" style="width: 100%; height: 100%"></canvas></div>' + 
													'<div class="sp_sb_cs_gb_graph5_col_1">' + 
														'<div class="sp_sb_cs_gb_g5c1_series_list">' + 
															'<div class="sp_sb_cs_gb_g5c1_sl_in">';
												for (let i = 0; i < response.data.sNs.length; i++) {
													textvalue += '<div class="sp_sb_cs_gb_g5c1_sl_series_row _' + i + '" style="top: ' + (4 + 7 * i) + '%;"><div class="sp_sb_cs_gb_g5c1_sl_sr_color_box" style="background-color: ' + colors[i] + ';"></div><div class="sp_sb_cs_gb_g5c1_sl_sr_space"></div><p class="sp_sb_cs_gb_g5c1_sl_sr_item_name">' + names[i] + '</p></div>';
												}
												$('.sp_sb_cs_sb_graph_block')[0].innerHTML = textvalue + '</div></div></div><div class="sp_sb_cs_gb_graph5_col_2"><canvas id="gr_canvas_1" style="width: 100%; height: 100%"></canvas></div>';

												//console.log('sBNs length: '+response.data.sBNs.length);
												let legend_col_width = getPercentWidth($('div.sp_sb_cs_gb_graph5_col_1'));
												let left_col_width = 100/response.data.sBNs.length*(100-legend_col_width)/100;
												let right_col_width = (100-100/response.data.sBNs.length)*(100-legend_col_width)/100;
												//console.log('left col width: '+left_col_width);
												//console.log('right col width: '+right_col_width);
												//console.log('legend col width: '+legend_col_width);
												$('div.sp_sb_cs_gb_graph5_col_0').css({'min-width':left_col_width+'%'});
												$('div.sp_sb_cs_gb_graph5_col_0').css({'max-width':left_col_width+'%'});
												$('div.sp_sb_cs_gb_graph5_col_0').css({width:left_col_width+'%'});
												$('div.sp_sb_cs_gb_graph5_col_1').css({left:left_col_width+'%'});
												$('div.sp_sb_cs_gb_graph5_col_2').css({left:(left_col_width+legend_col_width)+'%'});
												$('div.sp_sb_cs_gb_graph5_col_2').css({'min-width':right_col_width+'%'});
												$('div.sp_sb_cs_gb_graph5_col_2').css({'max-width':right_col_width+'%'});
												$('div.sp_sb_cs_gb_graph5_col_2').css({width:right_col_width+'%'});

												let legend_col_height = getPercentHeight($('div.sp_sb_cs_gb_g5c1_sl_in'));
												//console.log('legend col height: '+legend_col_height);
												
												/*if(response.data.sNs.length > 7) {
													let col_height = (response.data.sNs.length)*4;
													console.log('legend col_height: '+col_height);
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({'min-height':col_height+'%'});
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({'max-height':col_height+'%'});
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({height:col_height+'%'});
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({'top':'calc(15% + 0.7vh)'});
													let col_top = 0;
													$("div.sp_sb_cs_gb_g5c1_sl_series_row").each(function(index){
														let legend_col_top = getPercentTop($(this));
														console.log('legend top: '+index+': '+legend_col_top);
														//$(this).css({'top':+(legend_col_top-4)+'%'});
														col_top = col_top+5;
														$(this).css({'top':+col_top+'%'});
													});
												}*/

												if(response.data.sNs.length <=7) {
													let col_height = 35;
													//console.log('legend col_height: '+col_height);
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({'min-height':col_height+'%'});
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({'max-height':col_height+'%'});
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({height:col_height+'%'});
													//$('div.sp_sb_cs_gb_g5c1_sl_in').css({'top':'calc(20% + 2.2vh)'});
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({'top':''});
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({'vertical-align':'middle'});

													let col_top = 0;
													$("div.sp_sb_cs_gb_g5c1_sl_series_row").each(function(index){
														let legend_col_top = getPercentTop($(this));
														//console.log('legend top: '+index+': '+legend_col_top);
														//$(this).css({'top':+(legend_col_top-4)+'%'});
														col_top = col_top+10;
														$(this).css({'top':+col_top+'%'});
														$(this).css({'height':'7%'});
														$(this).css({'min-height':'7%'});
														$(this).css({'max-height':'7%'});
													});
												} else
												if(response.data.sNs.length > 7 && response.data.sNs.length < 15) {
													let col_height_ajust = (response.data.sNs.length)*2.5;
													//console.log('legend col_height_ajust: '+col_height_ajust);
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({'min-height':(legend_col_height+col_height_ajust)+'%'});
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({'max-height':(legend_col_height+col_height_ajust)+'%'});
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({height:(legend_col_height+col_height_ajust)+'%'});
													//$('div.sp_sb_cs_gb_g5c1_sl_in').css({'top':'calc(20% + 2.2vh)'});
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({'top':''});
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({'vertical-align':'middle'});

													let col_top = 0;
													$("div.sp_sb_cs_gb_g5c1_sl_series_row").each(function(index){
														let legend_col_top = getPercentTop($(this));
														//console.log('legend top: '+index+': '+legend_col_top);
														//$(this).css({'top':+(legend_col_top-4)+'%'});
														col_top = col_top+6;
														$(this).css({'top':+col_top+'%'});
													});
												} else
												if(response.data.sNs.length >= 15 &&response.data.sNs.length < 20) {
													let col_height = (response.data.sNs.length)*4;
													console.log('legend col_height 15: '+col_height);
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({'min-height':col_height+'%'});
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({'max-height':col_height+'%'});
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({height:col_height+'%'});
													//$('div.sp_sb_cs_gb_g5c1_sl_in').css({'top':'calc(20% + 1.3vh)'});
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({'top':''});
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({'vertical-align':'middle'});
													
													let col_top = 0;
													$("div.sp_sb_cs_gb_g5c1_sl_series_row").each(function(index){
														let legend_col_top = getPercentTop($(this));
														//console.log('legend top: '+index+': '+legend_col_top);
														//$(this).css({'top':+(legend_col_top-4)+'%'});
														col_top = col_top+5;
														$(this).css({'top':+col_top+'%'});
													});
												} else
												if(response.data.sNs.length >= 20) {
													let col_height = (response.data.sNs.length)*3.5;
													//console.log('legend col_height 20: '+col_height);
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({'min-height':col_height+'%'});
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({'max-height':col_height+'%'});
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({height:col_height+'%'});
													//$('div.sp_sb_cs_gb_g5c1_sl_in').css({'top':'calc(15% + 0.7vh)'});
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({'top':''});
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({'vertical-align':'middle'});

													let col_top = 0;
													$("div.sp_sb_cs_gb_g5c1_sl_series_row").each(function(index){
														let legend_col_top = getPercentTop($(this));
														//console.log('legend top: '+index+': '+legend_col_top);
														//$(this).css({'top':+(legend_col_top-4)+'%'});
														col_top = col_top+4.5;
														$(this).css({'top':+col_top+'%'});
													});
												}
												let $cnt_string = 'LC';
												let $msscht = 10000000;
												let $mst_string = 'Bln.' + $cnt_string;
												switch (window.cSD.sP) {
													case '0':
														$cnt_string = 'LC';
														switch (window.cSD.sM) {
															case '0':
																$msscht = 10000000;
																$mst_string = 'Bln.' + $cnt_string;
																break;
															case '1':
																$msscht = 10000;
																$mst_string = 'Mln.' + $cnt_string;
																break;
															case '2':
																$msscht = 10;
																$mst_string = 'Tsd.' + $cnt_string;
																break;
															default:
																$msscht = 10000000;
																$mst_string = 'Bln.' + $cnt_string;
														}
														break;
													case '1':
														$cnt_string = 'Euro';
														switch (window.cSD.sM) {
															case '0':
																$msscht = 10000000;
																$mst_string = 'Bln.' + $cnt_string;
																break;
															case '1':
																$msscht = 10000;
																$mst_string = 'Mln.' + $cnt_string;
																break;
															case '2':
																$msscht = 10;
																$mst_string = 'Tsd.' + $cnt_string;
																break;
															default:
																$msscht = 10000000;
																$mst_string = 'Bln.' + $cnt_string;
														}
														break;
													case '2':
														$msscht = 1;//	tmp
														$mst_string = 'units';
														break;
													default:
														$cnt_string = 'LC';
														switch (window.cSD.sM) {
															case '0':
																$msscht = 10000000;
																$mst_string = 'Bln.' + $cnt_string;
																break;
															case '1':
																$msscht = 10000;
																$mst_string = 'Mln.' + $cnt_string;
																break;
															case '2':
																$msscht = 10;
																$mst_string = 'Tsd.' + $cnt_string;
																break;
															default:
																$msscht = 10000000;
																$mst_string = 'Bln.' + $cnt_string;
														}
												}
												for (let i = 0; i < response.data.lUs.length; i++) {
													 response.data.lUs[i] = Math.round(response.data.lUs[i] / $msscht) / 100;
												}
												
												// legend font size ajustment
												//console.log('response.data.lUs.length: '+response.data.lUs.length);
												var lg_fnt_sz;
												switch(true) {
													case response.data.lUs.length > 30: lg_fnt_sz = 0.75; break;
													case response.data.lUs.length > 27: lg_fnt_sz = 0.8; break;
													case response.data.lUs.length > 24: lg_fnt_sz = 0.9; break;
													default: lg_fnt_sz = 1;
												}
												console.log('lg_fnt_sz: '+lg_fnt_sz);
												$('div.sp_sb_cs_sb_data_block').css({'font-size':lg_fnt_sz+'em'});
												
												textvalue= '';
												colwidth = left_col_width / 3;
												for (let i = 0; i < 3; i++) {
													let coef2ajust = 0;//	!!!
													if(i%3 == 1) { coef2ajust = colwidth/30; }
													if(i%3 == 2) { coef2ajust = -colwidth/20; }
													textvalue += '<div class="sp_sb_cs_db_data_col _' + i + '" style="left: ' + (colwidth * i + coef2ajust) + '%; width: ' + colwidth + '%; max-width: ' + colwidth + '%; min-width: ' + colwidth + '%"><p class="paragraph_1">' + response.data.lUs[i] + '<br><font color="' + ((response.data.lCs[i] === "Green") ? '008000">+' : 'c00000">') + ((response.data.lDs[i] != '') ? (response.data.lDs[i] + '%') : '') + '</font>' + '<br>' + percents[i] + '</p></div>';
												}
												
												textvalue += '<div class="sp_sb_cs_db_data_col _' + 2 + '" style="left: ' + (colwidth * 3) + '%; width: ' + legend_col_width + '%; max-width: ' + legend_col_width + '%; min-width: ' + legend_col_width + '%"><p class="paragraph_1">' + $mst_string + '<br>+/- % PY<br>Horiz.% out. GT</p></div>';
												
												for (let i = 4; i < response.data.lUs.length; i++) {
													let coef2ajust = colwidth/20;//	!!!
													if(i%3 == 1) { coef2ajust = coef2ajust*2; }
													if(i%3 == 2) { coef2ajust = coef2ajust; }
													if(i%3 == 0) { coef2ajust = -coef2ajust; }
													//console.log('coef2ajust: '+coef2ajust);
													textvalue += '<div class="sp_sb_cs_db_data_col _' + i + '" style="left: ' + (colwidth * (i-1) + legend_col_width + coef2ajust - 0.3) + '%; width: ' + colwidth + '%; max-width: ' + colwidth + '%; min-width: ' + colwidth + '%"><p class="paragraph_1">' + response.data.lUs[i] + '<br><font color="' + ((response.data.lCs[i] === "Green") ? '008000">+' : 'c00000">') + ((response.data.lDs[i] != '') ? (response.data.lDs[i] + '%') : '') + '</font>' + '<br>' + percents[i] + '</p></div>'
												}

												$('.sp_sb_cs_sb_data_block')[0].innerHTML = '<b>' + textvalue + '</b>';
												
												Chart.defaults.global.defaultFontSize = 12;
												Chart.defaults.global.defaultFontStyle = 'bold';
												Chart.defaults.global.defaultFontColor = 'black';
												Chart.defaults.global.defaultFontFamily = 'Verdana';
												Chart.defaults.global.elements.rectangle.borderColor = 'rgba(25, 25, 25, 1)';
												Chart.defaults.global.elements.rectangle.borderWidth = 0.7;

												//console.log('response.data.lYs: '+response.data.lYs);
												let tmpArr = [response.data.lYs[0], response.data.lYs[1], response.data.lYs[2]];
												let data_0 = {
													labels: ['', '', ''],//[response.data.lYs[0], response.data.lYs[1], response.data.lYs[2]],
													datasets: [],
												};
												response.data.lYs.splice(0, 4);
												let data_1 = {
													labels: [],
													datasets: [],
												};

												let num = 0;
												for (let i = 0; i < response.data.lYs.length; i++) {
													if (i % 3 == 1) {
														num++;
														let txt = ' ';
														if(response.data.sBNs.length > 4) { 
															//num += '\n';
															txt += '\n';
														}
														//data_1.labels.push(txt);
														data_1.labels.push('');
													}
												}
												let ctx_0 = document.getElementById('gr_canvas_0').getContext('2d');
												let ctx_1 = document.getElementById('gr_canvas_1').getContext('2d');
												let sums0 = [];
												let sums1 = [];
												let sums2 = [];
												let sums3 = [];
												for (let i = 0; i < response.data.rs[0].iD.length; i++) {
													sums0.unshift(0);
													sums1.unshift(0);
													sums2.unshift(0);
													sums3.unshift(0);
												}
												for (let i = 0; i < response.data.sNs.length; i++) {
													data_0.datasets.unshift({label: response.data.sNs[i], backgroundColor: colors[i], data: [], tooltipsValues: [], lbl: [], lbl2: []});
													for (let j = 0; j < 3; j++) {
														data_0.datasets[0].data.push(response.data.rs[i].iD[j].idppt3);
														data_0.datasets[0].tooltipsValues.push(response.data.rs[i].iD[j].idv);
														data_0.datasets[0].lbl.push(response.data.rs[i].iD[j].lbl);
														data_0.datasets[0].lbl2.push(tmpArr[j]);
														sums0[j] += Number(response.data.rs[i].iD[j].idppt3);
													}
												}
												for (let i = 0; i < response.data.sNs.length; i++) {
													data_1.datasets.unshift({label: response.data.sNs[i], backgroundColor: colors[i], data: [], tooltipsValues: [], lbl: [], lbl2: [], stack: 'Stack 0'});
													data_1.datasets.unshift({label: response.data.sNs[i], backgroundColor: colors[i], data: [], tooltipsValues: [], lbl: [], lbl2: [], stack: 'Stack 1'});
													data_1.datasets.unshift({label: response.data.sNs[i], backgroundColor: colors[i], data: [], tooltipsValues: [], lbl: [], lbl2: [], stack: 'Stack 2'});
													for (let j = 4; j < response.data.rs[i].iD.length; j++) {
														if (j % 3 == 1) {
															data_1.datasets[0].data.push(response.data.rs[i].iD[j].idppt3);
															data_1.datasets[0].tooltipsValues.push(response.data.rs[i].iD[j].idv);
															data_1.datasets[0].lbl.push(response.data.rs[i].iD[j].lbl);
															data_1.datasets[0].lbl2.push([tmpArr[2], tmpArr[1], tmpArr[0]]);
															sums1[j] += Number(response.data.rs[i].iD[j].idppt3);
														} else if (j % 3 == 2) {
															data_1.datasets[1].data.push(response.data.rs[i].iD[j].idppt3);
															data_1.datasets[1].tooltipsValues.push(response.data.rs[i].iD[j].idv);
															data_1.datasets[1].lbl.push(response.data.rs[i].iD[j].lbl);
															data_1.datasets[1].lbl2.push([tmpArr[2], tmpArr[1], tmpArr[0]]);
															sums2[j] += Number(response.data.rs[i].iD[j].idppt3);
														} else {
															data_1.datasets[2].data.push(response.data.rs[i].iD[j].idppt3);
															data_1.datasets[2].tooltipsValues.push(response.data.rs[i].iD[j].idv);
															data_1.datasets[2].lbl.push(response.data.rs[i].iD[j].lbl);
															data_1.datasets[2].lbl2.push([tmpArr[2], tmpArr[1], tmpArr[0]]);
															sums3[j] += Number(response.data.rs[i].iD[j].idppt3);
														}
													}
												}
												let max_0 = Math.max.apply(null, sums0) * 1.05;
												let max_1 = Math.max.apply(null, sums1) * 1.05;
												let max_2 = Math.max.apply(null, sums2) * 1.05;
												let max_3 = Math.max.apply(null, sums3) * 1.05;
												if (max_2 > max_1) {
													max_1 = max_2;
												}
												if (max_3 > max_1) {
													max_1 = max_3;
												}
														
												//console.log('DATA_1: '+JSON.stringify(data_1));

												switch(true) {
													case window.cSD.ssC.length > 7: InFontSize -= 4; decrFont = -1;  break;
													case window.cSD.ssC.length <= 7 && window.cSD.ssC.length > 4: InFontSize -= 2; decrFont = 0; break;
													default: break;
												}
												
												window.chart_0 = new Chart(ctx_0, {
													type: 'bar',
													data: data_0,
													options: {
														devicePixelRatio: window.devicePixelRatio*res_coef,
														hover: {
															mode: null,
															events: [],
															animationDuration: 0,
														},
														legend: {
															display: false,
														},
														tooltips: {
															mode: 'index',
															backgroundColor: 'rgba(255, 255, 255, 0.75)',
															titleFontSize: 14,
															titleFontColor: 'rgba(0, 0, 0, 1)',
															bodyFontSize: 14,
															bodyFontColor: 'rgba(0, 0, 0, 1)',
															bodyFontStyle: 'normal',
															intersect: false,
															callbacks: {
																label: function(tooltipItem, data) {
																	return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].tooltipsValues[tooltipItem.index];
																},
															},
														},
														responsive: true,
														scales: {
															xAxes: [{
																gridLineHeight: 20,
																gridLines: {
																	display: false,
																	drawBorder: false
																},
																stacked: true,
																barPercentage: 1.0,
																categoryPercentage: 0.9,
																position: 'top',
															}],
															yAxes: [{
																display: false,
																gridLines: {
																	display: false,
																	drawBorder: false
																},
																stacked: true,
																ticks: {
																	max: max_0,
																	display: false,
																},
															}],
														},
														plugins: {
															datalabels: {
																textAlign: 'center',
																color: function(context) {
																  var index = context.dataIndex;
																  var color = context.dataset.backgroundColor;
																  var result_rgb = rgbaToRgb(color);
																  var cc = contrastingColor(result_rgb);
																  return '#'+cc;
																},
																font: function(context) {
																		  var width = context.chart.width;
																		  var index = context.dataIndex;
																		  var value = context.dataset.data[index];
																		  var size;
																			switch(true) {
																				case value < 5 && value > 3: size = InFontSize; break;
																				case value <= 3: size = InFontSize - decrFont; break;
																				default: size = InFontSize+6;
																			}
																		 //console.log('value: '+value+' | size: '+size+' | weight: '+wgt);
																		  return {
																			 style:	(value < 5) ? 'normal' : 'bold',
																			 "size": size
																		};
																	  },
																formatter: function(value, ctx_0) {
																	if (ctx_0.dataset.label === 'All') {
																		return String(Math.floor(Number(value) * 50) / 10);
																	} else if (Number(value) > 1.99) {
																		if ((Number(value) * 10) % 10) {
																			return value;
																		} else if(Number(value) == 100) {
																			return 100;
																		} else {
																			return value + '.0';
																		}
																	} else {
																		return '';
																	}
																},
															},
														},
														animation: {
															duration: 1,
															onComplete: function() {
																let chartInstance = this.chart,
																	ctxthis = chartInstance.ctx;

																ctxthis.textAlign = 'center';
																ctxthis.textBaseline = 'bottom';
																let datas = this.data;
																let maxYScales = 0;
																this.data.labels.forEach(function(label, label_index) {
																	let colsumm = 0;
																	datas.datasets.forEach(function(dataset, dataset_index) {
																		let meta = chartInstance.controller.getDatasetMeta(dataset_index);
																		colsumm += Number(dataset.data[label_index]);
																		if (dataset_index == datas.datasets.length - 1) {

																				if(response.data.sBNs.length > 6) {
																					ctxthis.font = Chart.helpers.fontString(11, 'normal', Chart.defaults.global.defaultFontFamily);
																				} else {
																					ctxthis.font = Chart.helpers.fontString(14, 'normal', Chart.defaults.global.defaultFontFamily);
																				}
																				let tmpY = 20;
																				if(response.data.sBNs.length > 4) {
																					tmpY = 35;
																					let tmpA = dataset.lbl2[label_index].split(/\-/);
																					ctxthis.fillText((tmpA[0] + '-'), meta.data[label_index]._model.x, meta.data[label_index]._model.y - 20);
																					ctxthis.fillText(tmpA[1], meta.data[label_index]._model.x, meta.data[label_index]._model.y - 5);
																				} else {
																					ctxthis.fillText(dataset.lbl2[label_index], meta.data[label_index]._model.x, meta.data[label_index]._model.y - 5);
																				}
																			
																			ctxthis.font = Chart.helpers.fontString(17, 'bold', Chart.defaults.global.defaultFontFamily);
																			
																			if (label_index < 3) {
																				if (label_index % 3 == 1) {
																					ctxthis.fillText(dataset.lbl[label_index], meta.data[label_index]._model.x, meta.data[label_index]._model.y - tmpY);
																				}
																			} else {
																				if (!(label_index % 3 == 2)) {
																					ctxthis.fillText(dataset.lbl[label_index], meta.data[label_index]._model.x, meta.data[label_index]._model.y - tmpY);
																				}
																			}
																		}
																	});
																});
															},
														},
													},
													plugins: [{
													  beforeInit: function(chart) {
														  chart.data.labels.forEach(function(e, i, a) {
															  if (/\n/.test(e)) {
																  if(response.data.sBNs.length > 4) {
																	  a[i] = e.split(/\n/);
																  } else {
																	a[i] = e.replace(/\n/, '');
																  }
															  }
														  });
													  }
												   }]
												});
												window.chart_1 = new Chart(ctx_1, {
													type: 'bar',
													data: data_1,
													options: {
														devicePixelRatio: window.devicePixelRatio*res_coef,
														hover: {
															mode: null,
															events: [],
															animationDuration: 0,
														},
														legend: {
															display: false,
														},
														tooltips: {
															mode: 'index',
															backgroundColor: 'rgba(255, 255, 255, 0.75)',
															titleFontSize: 14,
															titleFontColor: 'rgba(0, 0, 0, 1)',
															bodyFontSize: 14,
															bodyFontColor: 'rgba(0, 0, 0, 1)',
															bodyFontStyle: 'normal',
															intersect: false,
															callbacks: {
																label: function(tooltipItem, data) {
																	return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].tooltipsValues[tooltipItem.index];
																},
															},
														},
														responsive: true,
														scales: {
															xAxes: [{
																gridLineHeight: 20,
																gridLines: {
																	display: false,
																	drawBorder: false
																},
																stacked: true,
																barPercentage: 0.9,
																categoryPercentage: 0.9,
																position: 'top'
															}],
															yAxes: [{
																display: false,
																gridLines: {
																	display: false,
																	drawBorder: false
																},
																stacked: true,
																ticks: {
																	max: max_1,
																	display: false,
																},
															}],
														},
														plugins: {
															datalabels: {
																textAlign: 'center',
																color: function(context) {
																  var index = context.dataIndex;
																  var color = context.dataset.backgroundColor;
																  var result_rgb = rgbaToRgb(color);
																  var cc = contrastingColor(result_rgb);
																  return '#'+cc;
																},
																font: function(context) {
																		  var width = context.chart.width;
																		  var index = context.dataIndex;
																		  var value = context.dataset.data[index];
																		  var size;
																			switch(true) {
																				case value < 5 && value > 3: size = InFontSize; break;
																				case value <= 3: size = InFontSize - decrFont; break;
																				default: size = InFontSize+6;
																			}
																		 //console.log('value: '+value+' | size: '+size+' | weight: '+wgt);
																		  return {
																			 style:	(value < 5) ? 'normal' : 'bold',
																			 "size": size
																		};
																	  },
																formatter: function(value, ctx_1) {
																	if (ctx_1.dataset.label === 'All') {
																		return String(Math.floor(Number(value) * 50) / 10);
																	} else if (Number(value) > 1.99) {
																		if ((Number(value) * 10) % 10) {
																			return value;
																		} else if(Number(value) == 100) {
																			return 100;
																		} else {
																			return value + '.0';
																		}
																	} else {
																		return '';
																	}
																},
															},
														},
														animation: {
															duration: 1,
															onComplete: function() {
																let chartInstance = this.chart,
																	ctxthis = chartInstance.ctx;
																ctxthis.textAlign = 'center';
																ctxthis.textBaseline = 'bottom';
																let datas = this.data;
																let maxYScales = 0;
																this.data.labels.forEach(function(label, label_index) {
																	let colsumm = 0;
																				let tmpY = 15;
																				if(response.data.sBNs.length > 4) {
																					tmpY = 30;
																				}
																	datas.datasets.forEach(function(dataset, dataset_index) {
																		colsumm += Number(dataset.data[label_index]);
																		if (dataset_index == datas.datasets.length - 1) {
																			let meta1 = chartInstance.controller.getDatasetMeta(dataset_index);
																			let meta2 = chartInstance.controller.getDatasetMeta(dataset_index - 1);
																			let meta3 = chartInstance.controller.getDatasetMeta(dataset_index - 2);
																			let maxY = ((meta1.data[label_index]._model.y < meta2.data[label_index]._model.y) ? meta1.data[label_index]._model.y : meta2.data[label_index]._model.y);
																			maxY = ((meta3.data[label_index]._model.y < maxY) ? meta3.data[label_index]._model.y : maxY) - 5;
																			ctxthis.font = Chart.helpers.fontString(17, 'bold', Chart.defaults.global.defaultFontFamily);
																			ctxthis.fillText(dataset.lbl[label_index], meta2.data[label_index]._model.x, maxY - tmpY);
																				if(response.data.sBNs.length > 6) {
																					ctxthis.font = Chart.helpers.fontString(11, 'normal', Chart.defaults.global.defaultFontFamily);
																				} else {
																					ctxthis.font = Chart.helpers.fontString(14, 'normal', Chart.defaults.global.defaultFontFamily);
																				}
																				if(response.data.sBNs.length > 4) {
																					let tmpA = dataset.lbl2[label_index][0].split(/\-/);
																					ctxthis.fillText((tmpA[0] + '-'), meta1.data[label_index]._model.x, maxY - 15);
																					ctxthis.fillText(tmpA[1], meta1.data[label_index]._model.x, maxY);
																					tmpA =dataset.lbl2[label_index][1].split(/\-/);
																					ctxthis.fillText((tmpA[0] + '-'), meta2.data[label_index]._model.x, maxY - 15);
																					ctxthis.fillText(tmpA[1], meta2.data[label_index]._model.x, maxY);
																					tmpA =dataset.lbl2[label_index][2].split(/\-/);
																					ctxthis.fillText((tmpA[0] + '-'), meta3.data[label_index]._model.x, maxY - 15);
																					ctxthis.fillText(tmpA[1], meta3.data[label_index]._model.x, maxY);
																				} else {
																					ctxthis.fillText(dataset.lbl2[label_index][0], meta1.data[label_index]._model.x, maxY);
																					ctxthis.fillText(dataset.lbl2[label_index][1], meta2.data[label_index]._model.x, maxY);
																					ctxthis.fillText(dataset.lbl2[label_index][2], meta3.data[label_index]._model.x, maxY);
																				}
																		}
																	});
																});
															let newh = $('#gr_canvas_1').css('height');
															//console.log('newh: '+newh);
															$('#gr_canvas_0').css({'height':newh});
															},
														},
													},
													plugins: [{
													  beforeInit: function(chart) {
														  chart.data.labels.forEach(function(e, i, a) {
															  if (/\n/.test(e)) {
																  if(response.data.sBNs.length > 4) {
																	  a[i] = e.split(/\n/);
																  } else {
																	a[i] = e.replace(/\n/, '');
																  }
															  }
														  });
													  }
												   }]
												});
											}
											else if (window.sD[window.cP].gT === '14') {
												
												let textvalue = '';
												
												let colwidth = 89 / response.data.lUs.length;
												//console.log('colwidth: '+colwidth);
												
												let percents = ['100%', '100%', '100%', '100%', ''];
												for (let i = 5; i < response.data.lUs.length - 1; i++) {
													if (Number(response.data.lUs[0]) != 0) {
														percents[i] = String(Math.round(Number(response.data.lUs[i]) / Number(response.data.lUs[0]) * 100)) + '%';
													} else {
														percents[i] = '100%';
													}
													i++;
													if (Number(response.data.lUs[1]) != 0) {
														percents[i] = String(Math.round(Number(response.data.lUs[i]) / Number(response.data.lUs[1]) * 100)) + '%';
													} else {
														percents[i] = '100%';
													}
													i++;
													if (Number(response.data.lUs[2]) != 0) {
														percents[i] = String(Math.round(Number(response.data.lUs[i]) / Number(response.data.lUs[2]) * 100)) + '%';
													} else {
														percents[i] = '100%';
													}
													i++;
													if (Number(response.data.lUs[3]) != 0) {
														percents[i] = String(Math.round(Number(response.data.lUs[i]) / Number(response.data.lUs[3]) * 100)) + '%';
													} else {
														percents[i] = '100%';
													}
												}

												let colors = ['rgba(0, 114, 218, 1)', 'rgba(0, 156, 73, 1)', 'rgba(240, 66, 121, 1)', 'rgba(255, 127, 0, 1)', 'rgba(167, 122, 215, 1)', 'rgba(255, 255, 0, 1)', 'rgba(255, 000, 128, 0.4)', 'rgba(255, 99, 132, 0.9)', 'rgba(197, 255, 10, 1)', 'rgba(127, 127, 127, 1)'];
								
												let names = response.data.sNs;
												if(BrandsTypes.includes(window.cSD.sI)) {
													for (let i = 0; i < names.length; i++) {
														let brand = names[i].trim();
														if (brand in window.BrandsData) {
															colors[i] = window.BrandsData[brand].color;
															names[i] = window.BrandsData[brand].name;
														}
													}
												}
												if (window.cSD.sI == 0) {
													for (let i = 0; i < names.length; i++) {
														colors[i] = window.cSD.dD[i].clr;
														names[i] = window.cSD.dD[i].sS.toLocaleString() + ' - ' + window.cSD.dD[i].eS.toLocaleString();
													}
												}
												textvalue = '<div class="sp_sb_cs_gb_graph5_col_0"><canvas id="gr_canvas_0" style="width: 100%; height: 100%"></canvas></div>' + 
													'<div class="sp_sb_cs_gb_graph5_col_1">' + 
														'<div class="sp_sb_cs_gb_g5c1_series_list">' + 
															'<div class="sp_sb_cs_gb_g5c1_sl_in">';
												for (let i = 0; i < response.data.sNs.length; i++) {
													textvalue += '<div class="sp_sb_cs_gb_g5c1_sl_series_row _' + i + '" style="top: ' + (4 + 7 * i) + '%;"><div class="sp_sb_cs_gb_g5c1_sl_sr_color_box" style="background-color: ' + colors[i] + ';"></div><div class="sp_sb_cs_gb_g5c1_sl_sr_space"></div><p class="sp_sb_cs_gb_g5c1_sl_sr_item_name">' + names[i] + '</p></div>';
												}
												$('.sp_sb_cs_sb_graph_block')[0].innerHTML = textvalue + '</div></div></div><div class="sp_sb_cs_gb_graph5_col_2"><canvas id="gr_canvas_1" style="width: 100%; height: 100%"></canvas></div>';

												//console.log('sBNs length: '+response.data.sBNs.length);
												let legend_col_width = getPercentWidth($('div.sp_sb_cs_gb_graph5_col_1'));
												let left_col_width = 100/response.data.sBNs.length*(100-legend_col_width)/100;
												let right_col_width = (100-100/response.data.sBNs.length)*(100-legend_col_width)/100;
												//console.log('left col width: '+left_col_width);
												//console.log('right col width: '+right_col_width);
												//console.log('legend col width: '+legend_col_width);
												$('div.sp_sb_cs_gb_graph5_col_0').css({'min-width':left_col_width+'%'});
												$('div.sp_sb_cs_gb_graph5_col_0').css({'max-width':left_col_width+'%'});
												$('div.sp_sb_cs_gb_graph5_col_0').css({width:left_col_width+'%'});
												$('div.sp_sb_cs_gb_graph5_col_1').css({left:left_col_width+'%'});
												$('div.sp_sb_cs_gb_graph5_col_2').css({left:(left_col_width+legend_col_width)+'%'});
												$('div.sp_sb_cs_gb_graph5_col_2').css({'min-width':right_col_width+'%'});
												$('div.sp_sb_cs_gb_graph5_col_2').css({'max-width':right_col_width+'%'});
												$('div.sp_sb_cs_gb_graph5_col_2').css({width:right_col_width+'%'});

												let legend_col_height = getPercentHeight($('div.sp_sb_cs_gb_g5c1_sl_in'));
												//console.log('legend col height: '+legend_col_height);
												
												/*if(response.data.sNs.length > 7) {
													let col_height = (response.data.sNs.length)*4;
													console.log('legend col_height: '+col_height);
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({'min-height':col_height+'%'});
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({'max-height':col_height+'%'});
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({height:col_height+'%'});
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({'top':'calc(15% + 0.7vh)'});
													let col_top = 0;
													$("div.sp_sb_cs_gb_g5c1_sl_series_row").each(function(index){
														let legend_col_top = getPercentTop($(this));
														console.log('legend top: '+index+': '+legend_col_top);
														//$(this).css({'top':+(legend_col_top-4)+'%'});
														col_top = col_top+5;
														$(this).css({'top':+col_top+'%'});
													});
												}*/

												if(response.data.sNs.length <=7) {
													let col_height = 35;
													//console.log('legend col_height: '+col_height);
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({'min-height':col_height+'%'});
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({'max-height':col_height+'%'});
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({height:col_height+'%'});
													//$('div.sp_sb_cs_gb_g5c1_sl_in').css({'top':'calc(20% + 2.2vh)'});
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({'top':''});
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({'vertical-align':'middle'});

													let col_top = 0;
													$("div.sp_sb_cs_gb_g5c1_sl_series_row").each(function(index){
														let legend_col_top = getPercentTop($(this));
														//console.log('legend top: '+index+': '+legend_col_top);
														//$(this).css({'top':+(legend_col_top-4)+'%'});
														col_top = col_top+10;
														$(this).css({'top':+col_top+'%'});
														$(this).css({'height':'7%'});
														$(this).css({'min-height':'7%'});
														$(this).css({'max-height':'7%'});
													});
												} else
												if(response.data.sNs.length > 7 && response.data.sNs.length < 15) {
													let col_height_ajust = (response.data.sNs.length)*2.5;
													//console.log('legend col_height_ajust: '+col_height_ajust);
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({'min-height':(legend_col_height+col_height_ajust)+'%'});
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({'max-height':(legend_col_height+col_height_ajust)+'%'});
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({height:(legend_col_height+col_height_ajust)+'%'});
													//$('div.sp_sb_cs_gb_g5c1_sl_in').css({'top':'calc(20% + 2.2vh)'});
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({'top':''});
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({'vertical-align':'middle'});

													let col_top = 0;
													$("div.sp_sb_cs_gb_g5c1_sl_series_row").each(function(index){
														let legend_col_top = getPercentTop($(this));
														//console.log('legend top: '+index+': '+legend_col_top);
														//$(this).css({'top':+(legend_col_top-4)+'%'});
														col_top = col_top+6;
														$(this).css({'top':+col_top+'%'});
													});
												} else
												if(response.data.sNs.length >= 15 &&response.data.sNs.length < 20) {
													let col_height = (response.data.sNs.length)*4;
													console.log('legend col_height 15: '+col_height);
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({'min-height':col_height+'%'});
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({'max-height':col_height+'%'});
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({height:col_height+'%'});
													//$('div.sp_sb_cs_gb_g5c1_sl_in').css({'top':'calc(20% + 1.3vh)'});
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({'top':''});
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({'vertical-align':'middle'});
													
													let col_top = 0;
													$("div.sp_sb_cs_gb_g5c1_sl_series_row").each(function(index){
														let legend_col_top = getPercentTop($(this));
														//console.log('legend top: '+index+': '+legend_col_top);
														//$(this).css({'top':+(legend_col_top-4)+'%'});
														col_top = col_top+5;
														$(this).css({'top':+col_top+'%'});
													});
												} else
												if(response.data.sNs.length >= 20) {
													let col_height = (response.data.sNs.length)*3.5;
													//console.log('legend col_height 20: '+col_height);
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({'min-height':col_height+'%'});
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({'max-height':col_height+'%'});
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({height:col_height+'%'});
													//$('div.sp_sb_cs_gb_g5c1_sl_in').css({'top':'calc(15% + 0.7vh)'});
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({'top':''});
													$('div.sp_sb_cs_gb_g5c1_sl_in').css({'vertical-align':'middle'});

													let col_top = 0;
													$("div.sp_sb_cs_gb_g5c1_sl_series_row").each(function(index){
														let legend_col_top = getPercentTop($(this));
														//console.log('legend top: '+index+': '+legend_col_top);
														//$(this).css({'top':+(legend_col_top-4)+'%'});
														col_top = col_top+4.5;
														$(this).css({'top':+col_top+'%'});
													});
												}
												let $cnt_string = 'LC';
												let $msscht = 10000000;
												let $mst_string = 'Bln.' + $cnt_string;
												switch (window.cSD.sP) {
													case '0':
														$cnt_string = 'LC';
														switch (window.cSD.sM) {
															case '0':
																$msscht = 10000000;
																$mst_string = 'Bln.' + $cnt_string;
																break;
															case '1':
																$msscht = 10000;
																$mst_string = 'Mln.' + $cnt_string;
																break;
															case '2':
																$msscht = 10;
																$mst_string = 'Tsd.' + $cnt_string;
																break;
															default:
																$msscht = 10000000;
																$mst_string = 'Bln.' + $cnt_string;
														}
														break;
													case '1':
														$cnt_string = 'Euro';
														switch (window.cSD.sM) {
															case '0':
																$msscht = 10000000;
																$mst_string = 'Bln.' + $cnt_string;
																break;
															case '1':
																$msscht = 10000;
																$mst_string = 'Mln.' + $cnt_string;
																break;
															case '2':
																$msscht = 10;
																$mst_string = 'Tsd.' + $cnt_string;
																break;
															default:
																$msscht = 10000000;
																$mst_string = 'Bln.' + $cnt_string;
														}
														break;
													case '2':
														$msscht = 1;//	tmp
														$mst_string = 'units';
														break;
													default:
														$cnt_string = 'LC';
														switch (window.cSD.sM) {
															case '0':
																$msscht = 10000000;
																$mst_string = 'Bln.' + $cnt_string;
																break;
															case '1':
																$msscht = 10000;
																$mst_string = 'Mln.' + $cnt_string;
																break;
															case '2':
																$msscht = 10;
																$mst_string = 'Tsd.' + $cnt_string;
																break;
															default:
																$msscht = 10000000;
																$mst_string = 'Bln.' + $cnt_string;
														}
												}
												for (let i = 0; i < response.data.lUs.length; i++) {
													 response.data.lUs[i] = Math.round(response.data.lUs[i] / $msscht) / 100;
												}
												
												// legend font size ajustment
												//console.log('response.data.lUs.length: '+response.data.lUs.length);
												var lg_fnt_sz;
												switch(true) {
													case response.data.lUs.length > 30: lg_fnt_sz = 0.7; break;
													case response.data.lUs.length > 27: lg_fnt_sz = 0.8; break;
													case response.data.lUs.length > 24: lg_fnt_sz = 0.9; break;
													default: lg_fnt_sz = 1;
												}
												//console.log('lg_fnt_sz: '+lg_fnt_sz);
												$('div.sp_sb_cs_sb_data_block').css({'font-size':lg_fnt_sz+'em'});
												
												textvalue= '';
												colwidth = left_col_width / 4;
												for (let i = 0; i < 4; i++) {
													let coef2ajust = 0;//	!!!
													if(i%4 == 1) { coef2ajust = colwidth/30; }
													if(i%4 == 2) { coef2ajust = -colwidth/20; }
													if(i%4 == 3) { coef2ajust = -colwidth/20; }
													textvalue += '<div class="sp_sb_cs_db_data_col _' + i + '" style="left: ' + (colwidth * i + coef2ajust) + '%; width: ' + colwidth + '%; max-width: ' + colwidth + '%; min-width: ' + colwidth + '%"><p class="paragraph_1">' + response.data.lUs[i] + '<br><font color="' + ((response.data.lCs[i] === "Green") ? '008000">+' : 'c00000">') + ((response.data.lDs[i] != '') ? (response.data.lDs[i] + '%') : '') + '</font>' + '<br>' + percents[i] + '</p></div>';
												}
												
												textvalue += '<div class="sp_sb_cs_db_data_col _' + 2 + '" style="left: ' + (colwidth * 4) + '%; width: ' + legend_col_width + '%; max-width: ' + legend_col_width + '%; min-width: ' + legend_col_width + '%"><p class="paragraph_1">' + $mst_string + '<br>+/- % PY<br>Horiz.% out. GT</p></div>';
												
												for (let i = 5; i < response.data.lUs.length; i++) {
													let coef2ajust = colwidth/20;//	!!!
													if(i%4 == 1) { coef2ajust = coef2ajust*2; }
													if(i%4 == 2) { coef2ajust = coef2ajust; }
													if(i%4 == 0) { coef2ajust = -coef2ajust; }
													if(i%4 == 3) { coef2ajust = -coef2ajust; }
													//console.log('coef2ajust: '+coef2ajust);
													textvalue += '<div class="sp_sb_cs_db_data_col _' + i + '" style="left: ' + (colwidth * (i-1) + legend_col_width + coef2ajust - 0.3) + '%; width: ' + colwidth + '%; max-width: ' + colwidth + '%; min-width: ' + colwidth + '%"><p class="paragraph_1">' + response.data.lUs[i] + '<br><font color="' + ((response.data.lCs[i] === "Green") ? '008000">+' : 'c00000">') + ((response.data.lDs[i] != '') ? (response.data.lDs[i] + '%') : '') + '</font>' + '<br>' + percents[i] + '</p></div>'
												}

												$('.sp_sb_cs_sb_data_block')[0].innerHTML = '<b>' + textvalue + '</b>';
												
												Chart.defaults.global.defaultFontSize = 12;
												Chart.defaults.global.defaultFontStyle = 'bold';
												Chart.defaults.global.defaultFontColor = 'black';
												Chart.defaults.global.defaultFontFamily = 'Verdana';
												Chart.defaults.global.elements.rectangle.borderColor = 'rgba(25, 25, 25, 1)';
												Chart.defaults.global.elements.rectangle.borderWidth = 0.7;

												//console.log('response.data.lYs: '+response.data.lYs);
												let tmpArr = [response.data.lYs[0], response.data.lYs[1], response.data.lYs[2], response.data.lYs[3]];
												let data_0 = {
													labels: ['', '', '', ''],//[response.data.lYs[0], response.data.lYs[1], response.data.lYs[2]],
													datasets: [],
												};
												response.data.lYs.splice(0, 5);
												let data_1 = {
													labels: [],
													datasets: [],
												};

												let num = 0;
												for (let i = 0; i < response.data.lYs.length; i++) {
													if (i % 4 == 1) {
														num++;
														let txt = ' ';
														if(response.data.sBNs.length > 3) { 
															//num += '\n';
															txt += '\n';
														}
														//data_1.labels.push(txt);
														data_1.labels.push('');
													}
												}
												let ctx_0 = document.getElementById('gr_canvas_0').getContext('2d');
												let ctx_1 = document.getElementById('gr_canvas_1').getContext('2d');
												let sums0 = [];
												let sums1 = [];
												let sums2 = [];
												let sums3 = [];
												let sums4 = [];
												for (let i = 0; i < response.data.rs[0].iD.length; i++) {
													sums0.unshift(0);
													sums1.unshift(0);
													sums2.unshift(0);
													sums3.unshift(0);
													sums4.unshift(0);
												}
												for (let i = 0; i < response.data.sNs.length; i++) {
													data_0.datasets.unshift({label: response.data.sNs[i], backgroundColor: colors[i], data: [], tooltipsValues: [], lbl: [], lbl2: []});
													for (let j = 0; j < 4; j++) {
														data_0.datasets[0].data.push(response.data.rs[i].iD[j].idppt3);
														data_0.datasets[0].tooltipsValues.push(response.data.rs[i].iD[j].idv);
														data_0.datasets[0].lbl.push(response.data.rs[i].iD[j].lbl);
														data_0.datasets[0].lbl2.push(tmpArr[j]);
														sums0[j] += Number(response.data.rs[i].iD[j].idppt3);
													}
												}
												for (let i = 0; i < response.data.sNs.length; i++) {
													data_1.datasets.unshift({label: response.data.sNs[i], backgroundColor: colors[i], data: [], tooltipsValues: [], lbl: [], lbl2: [], stack: 'Stack 0'});
													data_1.datasets.unshift({label: response.data.sNs[i], backgroundColor: colors[i], data: [], tooltipsValues: [], lbl: [], lbl2: [], stack: 'Stack 1'});
													data_1.datasets.unshift({label: response.data.sNs[i], backgroundColor: colors[i], data: [], tooltipsValues: [], lbl: [], lbl2: [], stack: 'Stack 2'});
													data_1.datasets.unshift({label: response.data.sNs[i], backgroundColor: colors[i], data: [], tooltipsValues: [], lbl: [], lbl2: [], stack: 'Stack 3'});
													for (let j = 5; j < response.data.rs[i].iD.length; j++) {
														if (j % 4 == 1) {
															data_1.datasets[0].data.push(response.data.rs[i].iD[j].idppt3);
															data_1.datasets[0].tooltipsValues.push(response.data.rs[i].iD[j].idv);
															data_1.datasets[0].lbl.push(response.data.rs[i].iD[j].lbl);
															data_1.datasets[0].lbl2.push([tmpArr[3], tmpArr[2], tmpArr[1], tmpArr[0]]);
															sums1[j] += Number(response.data.rs[i].iD[j].idppt3);
														} else if (j % 4 == 2) {
															data_1.datasets[1].data.push(response.data.rs[i].iD[j].idppt3);
															data_1.datasets[1].tooltipsValues.push(response.data.rs[i].iD[j].idv);
															data_1.datasets[1].lbl.push(response.data.rs[i].iD[j].lbl);
															data_1.datasets[1].lbl2.push([tmpArr[3], tmpArr[2], tmpArr[1], tmpArr[0]]);
															sums2[j] += Number(response.data.rs[i].iD[j].idppt3);
														} else if (j % 4 == 3) {
															data_1.datasets[2].data.push(response.data.rs[i].iD[j].idppt3);
															data_1.datasets[2].tooltipsValues.push(response.data.rs[i].iD[j].idv);
															data_1.datasets[2].lbl.push(response.data.rs[i].iD[j].lbl);
															data_1.datasets[2].lbl2.push([tmpArr[3], tmpArr[2], tmpArr[1], tmpArr[0]]);
															sums3[j] += Number(response.data.rs[i].iD[j].idppt3);
														} else {
															data_1.datasets[3].data.push(response.data.rs[i].iD[j].idppt3);
															data_1.datasets[3].tooltipsValues.push(response.data.rs[i].iD[j].idv);
															data_1.datasets[3].lbl.push(response.data.rs[i].iD[j].lbl);
															data_1.datasets[3].lbl2.push([tmpArr[3], tmpArr[2], tmpArr[1], tmpArr[0]]);
															sums4[j] += Number(response.data.rs[i].iD[j].idppt3);
														}
													}
												}
												let max_0 = Math.max.apply(null, sums0) * 1.05;
												let max_1 = Math.max.apply(null, sums1) * 1.05;
												let max_2 = Math.max.apply(null, sums2) * 1.05;
												let max_3 = Math.max.apply(null, sums3) * 1.05;
												let max_4 = Math.max.apply(null, sums4) * 1.05;
												if (max_2 > max_1) {
													max_1 = max_2;
												}
												if (max_3 > max_1) {
													max_1 = max_3;
												}
												if (max_4 > max_1) {
													max_1 = max_4;
												}
														
												//console.log('DATA_1: '+JSON.stringify(data_1));

												switch(true) {
													case window.cSD.ssC.length > 7: InFontSize -= 4; decrFont = -1;  break;
													case window.cSD.ssC.length <= 7 && window.cSD.ssC.length > 4: InFontSize -= 2; decrFont = 0; break;
													default: break;
												}
												
												window.chart_0 = new Chart(ctx_0, {
													type: 'bar',
													data: data_0,
													options: {
														devicePixelRatio: window.devicePixelRatio*res_coef,
														hover: {
															mode: null,
															events: [],
															animationDuration: 0,
														},
														legend: {
															display: false,
														},
														tooltips: {
															mode: 'index',
															backgroundColor: 'rgba(255, 255, 255, 0.75)',
															titleFontSize: 14,
															titleFontColor: 'rgba(0, 0, 0, 1)',
															bodyFontSize: 14,
															bodyFontColor: 'rgba(0, 0, 0, 1)',
															bodyFontStyle: 'normal',
															intersect: false,
															callbacks: {
																label: function(tooltipItem, data) {
																	return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].tooltipsValues[tooltipItem.index];
																},
															},
														},
														responsive: true,
														scales: {
															xAxes: [{
																gridLineHeight: 20,
																gridLines: {
																	display: false,
																	drawBorder: false
																},
																stacked: true,
																barPercentage: 1.0,
																categoryPercentage: 0.9,
																position: 'top',
															}],
															yAxes: [{
																display: false,
																gridLines: {
																	display: false,
																	drawBorder: false
																},
																stacked: true,
																ticks: {
																	max: max_0,
																	display: false,
																},
															}],
														},
														plugins: {
															datalabels: {
																textAlign: 'center',
																color: function(context) {
																  var index = context.dataIndex;
																  var color = context.dataset.backgroundColor;
																  var result_rgb = rgbaToRgb(color);
																  var cc = contrastingColor(result_rgb);
																  return '#'+cc;
																},
																font: function(context) {
																		  var width = context.chart.width;
																		  var index = context.dataIndex;
																		  var value = context.dataset.data[index];
																		  var size;
																			switch(true) {
																				case value < 5 && value > 3: size = InFontSize; break;
																				case value <= 3: size = InFontSize - decrFont; break;
																				default: size = InFontSize+6;
																			}
																		 //console.log('value: '+value+' | size: '+size+' | weight: '+wgt);
																		  return {
																			 style:	(value < 5) ? 'normal' : 'bold',
																			 "size": size
																		};
																	  },
																formatter: function(value, ctx_0) {
																	if (ctx_0.dataset.label === 'All') {
																		return String(Math.floor(Number(value) * 50) / 10);
																	} else if (Number(value) > 1.99) {
																		if ((Number(value) * 10) % 10) {
																			return value;
																		} else if(Number(value) == 100) {
																			return 100;
																		} else {
																			return value + '.0';
																		}
																	} else {
																		return '';
																	}
																},
															},
														},
														animation: {
															duration: 1,
															onComplete: function() {
																let chartInstance = this.chart,
																	ctxthis = chartInstance.ctx;

																ctxthis.textAlign = 'center';
																ctxthis.textBaseline = 'bottom';
																let datas = this.data;
																let maxYScales = 0;
																this.data.labels.forEach(function(label, label_index) {
																	let colsumm = 0;
																	datas.datasets.forEach(function(dataset, dataset_index) {
																		let meta = chartInstance.controller.getDatasetMeta(dataset_index);
																		colsumm += Number(dataset.data[label_index]);
																		if (dataset_index == datas.datasets.length - 1) {

																				if(response.data.sBNs.length > 6) {
																					ctxthis.font = Chart.helpers.fontString(11, 'normal', Chart.defaults.global.defaultFontFamily);
																				} else {
																					ctxthis.font = Chart.helpers.fontString(14, 'normal', Chart.defaults.global.defaultFontFamily);
																				}
																				let tmpY = 20;
																				ctxthis.fillText(dataset.lbl2[label_index], meta.data[label_index]._model.x, meta.data[label_index]._model.y - 5);
																			
																			ctxthis.font = Chart.helpers.fontString(17, 'bold', Chart.defaults.global.defaultFontFamily);
																			
																			if (label_index < 4) {
																				if (label_index % 4 == 2) {
							ctxthis.fillText(dataset.lbl[label_index], (meta.data[label_index - 1]._model.x + meta.data[label_index]._model.x) / 2, meta.data[label_index]._model.y - tmpY);
																				}
																			} else {
																				if (!(label_index % 4 == 3)) {
							ctxthis.fillText(dataset.lbl[label_index], (meta.data[label_index - 1]._model.x + meta.data[label_index]._model.x) / 2, meta.data[label_index]._model.y - tmpY);
																				}
																			}
																		}
																	});
																});
															},
														},
													},
													plugins: [{
													  beforeInit: function(chart) {
														  chart.data.labels.forEach(function(e, i, a) {
															  if (/\n/.test(e)) {
																  if(response.data.sBNs.length > 3) {
																	  a[i] = e.split(/\n/);
																  } else {
																	a[i] = e.replace(/\n/, '');
																  }
															  }
														  });
													  }
												   }]
												});
												window.chart_1 = new Chart(ctx_1, {
													type: 'bar',
													data: data_1,
													options: {
														devicePixelRatio: window.devicePixelRatio*res_coef,
														hover: {
															mode: null,
															events: [],
															animationDuration: 0,
														},
														legend: {
															display: false,
														},
														tooltips: {
															mode: 'index',
															backgroundColor: 'rgba(255, 255, 255, 0.75)',
															titleFontSize: 14,
															titleFontColor: 'rgba(0, 0, 0, 1)',
															bodyFontSize: 14,
															bodyFontColor: 'rgba(0, 0, 0, 1)',
															bodyFontStyle: 'normal',
															intersect: false,
															callbacks: {
																label: function(tooltipItem, data) {
																	return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].tooltipsValues[tooltipItem.index];
																},
															},
														},
														responsive: true,
														scales: {
															xAxes: [{
																gridLineHeight: 20,
																gridLines: {
																	display: false,
																	drawBorder: false
																},
																stacked: true,
																barPercentage: 0.9,
																categoryPercentage: 0.9,
																position: 'top'
															}],
															yAxes: [{
																display: false,
																gridLines: {
																	display: false,
																	drawBorder: false
																},
																stacked: true,
																ticks: {
																	max: max_1,
																	display: false,
																},
															}],
														},
														plugins: {
															datalabels: {
																textAlign: 'center',
																color: function(context) {
																  var index = context.dataIndex;
																  var color = context.dataset.backgroundColor;
																  var result_rgb = rgbaToRgb(color);
																  var cc = contrastingColor(result_rgb);
																  return '#'+cc;
																},
																font: function(context) {
																		  var width = context.chart.width;
																		  var index = context.dataIndex;
																		  var value = context.dataset.data[index];
																		  var size;
																			switch(true) {
																				case value < 5 && value > 3: size = InFontSize; break;
																				case value <= 3: size = InFontSize - decrFont; break;
																				default: size = InFontSize+6;
																			}
																		 //console.log('value: '+value+' | size: '+size+' | weight: '+wgt);
																		  return {
																			 style:	(value < 5) ? 'normal' : 'bold',
																			 "size": size
																		};
																	  },
																formatter: function(value, ctx_1) {
																	if (ctx_1.dataset.label === 'All') {
																		return String(Math.floor(Number(value) * 50) / 10);
																	} else if (Number(value) > 1.99) {
																		if ((Number(value) * 10) % 10) {
																			return value;
																		} else if(Number(value) == 100) {
																			return 100;
																		} else {
																			return value + '.0';
																		}
																	} else {
																		return '';
																	}
																},
															},
														},
														animation: {
															duration: 1,
															onComplete: function() {
																let chartInstance = this.chart,
																	ctxthis = chartInstance.ctx;
																ctxthis.textAlign = 'center';
																ctxthis.textBaseline = 'bottom';
																let datas = this.data;
																let maxYScales = 0;
																this.data.labels.forEach(function(label, label_index) {
																	let colsumm = 0;
																				let tmpY = 15;
																	datas.datasets.forEach(function(dataset, dataset_index) {
																		colsumm += Number(dataset.data[label_index]);
																		if (dataset_index == datas.datasets.length - 1) {
																			let meta1 = chartInstance.controller.getDatasetMeta(dataset_index);
																			let meta2 = chartInstance.controller.getDatasetMeta(dataset_index - 1);
																			let meta3 = chartInstance.controller.getDatasetMeta(dataset_index - 2);
																			let meta4 = chartInstance.controller.getDatasetMeta(dataset_index - 3);
																			let maxY = ((meta1.data[label_index]._model.y < meta2.data[label_index]._model.y) ? meta1.data[label_index]._model.y : meta2.data[label_index]._model.y);
																			maxY = ((meta3.data[label_index]._model.y < maxY) ? meta3.data[label_index]._model.y : maxY);
																			maxY = ((meta4.data[label_index]._model.y < maxY) ? meta4.data[label_index]._model.y : maxY) - 5;
																			ctxthis.font = Chart.helpers.fontString(17, 'bold', Chart.defaults.global.defaultFontFamily);
											ctxthis.fillText(dataset.lbl[label_index], (meta2.data[label_index]._model.x + meta3.data[label_index]._model.x) / 2, maxY - tmpY);
																				if(response.data.sBNs.length > 6) {
																					ctxthis.font = Chart.helpers.fontString(11, 'normal', Chart.defaults.global.defaultFontFamily);
																				} else {
																					ctxthis.font = Chart.helpers.fontString(14, 'normal', Chart.defaults.global.defaultFontFamily);
																				}
				ctxthis.fillText(dataset.lbl2[label_index][0], meta1.data[label_index]._model.x, maxY);
				ctxthis.fillText(dataset.lbl2[label_index][1], meta2.data[label_index]._model.x, maxY);
				ctxthis.fillText(dataset.lbl2[label_index][2], meta3.data[label_index]._model.x, maxY);
				ctxthis.fillText(dataset.lbl2[label_index][3], meta4.data[label_index]._model.x, maxY);
																		}
																	});
																});
															},
														},
													},
													plugins: [{
													  beforeInit: function(chart) {
														  chart.data.labels.forEach(function(e, i, a) {
															  if (/\n/.test(e)) {
																  if(response.data.sBNs.length > 3) {
																	  a[i] = e.split(/\n/);
																  } else {
																	a[i] = e.replace(/\n/, '');
																  }
															  }
														  });
													  }
												   }]
												});
											}
											else if (window.sD[window.cP].gT === '15') {

											let colwl;
											let colwr;
											let adjl;
											let adjr;
											let shiftl;
											if (Number(window.cSD.iI) === 1) { // month
												colwl = 4.9;
												colwr = 4.78;
												adjl = 4.6;
												adjr = 4.65;
												shiftl = 1.5;
											} else if (Number(window.cSD.iI) === 2) {	// years
												colwl = 8.3;
												colwr = 8.3;
												adjl = 1;
												adjr = 1;
												shiftl = 1.5;
											} else { // quart
												colwl = 9;
												colwr = 9.1;
												adjl = 1;
												adjr = 0.2;
												shiftl = 1.5;
											}
								
										console.log('Number(window.cSD.iI): '+Number(window.cSD.iI));

										let textvalue = '';
										let textvalueUp = '';

										textvalue = '<b><div class="sp_sb_cs_db_data_col _0" style="text-align:left; left:'+shiftl+'%; width: '+colwl+'%; max-width: '+colwl+'%; min-width: '+colwl+'%">' +
										'<p class="paragraph_1">' + response.data.lUs[0] + '<br>' + response.data.lMs[0] + '<br>' + response.data.lDs[0] + 
										'</p></div><div class="sp_sb_cs_db_data_col _1" style="left: '+(colwl+adjl)+'%; width: '+colwl+'%; max-width: '+colwl+'%; min-width: '+colwl+'%">' +
										'<p class="paragraph_1">' + response.data.lUs[1] + '<br>' + response.data.lMs[1] + '<br>' + response.data.lDs[1] + 
										'</p></div><div class="sp_sb_cs_db_data_col _2" style="left: '+(colwl*2+adjl)+'%; width: '+colwl+'%; max-width: '+colwl+'%; min-width: '+colwl+'%">' +
										'<p class="paragraph_1">' + response.data.lUs[2] + '<br>' + response.data.lMs[2] + '<br>' + response.data.lDs[2] + 
										'</p></div><div class="sp_sb_cs_db_data_col _3" style="left: '+(colwl*3+adjl)+'%; width: '+colwl+'%; max-width: '+colwl+'%; min-width: '+colwl+'%">' +
										'<p class="paragraph_1">' + response.data.lUs[3] + '<br>' + response.data.lMs[3] + '<br>' + response.data.lDs[3] + 
										'</p></div><div class="sp_sb_cs_db_data_col _4" style="left: '+(colwl*4+adjl)+'%; width:  '+colwl+'%; max-width:  '+colwl+'%; min-width:  '+colwl+'%">' +
										'<p class="paragraph_1">' + response.data.lUs[4] + '<br>' + response.data.lMs[4] + '<br>' + response.data.lDs[4] + 
										'</p></div><div class="sp_sb_cs_db_data_col _5" style="left: '+(colwl*5+adjl)+'%; width:  '+colwl+'%; max-width:  '+colwl+'%; min-width:  '+colwl+'%">' +
										'<p class="paragraph_1">' + response.data.lUs[5] + '<br>' + response.data.lMs[5] + '<br>' + response.data.lDs[5] + '</p></div>';

										textvalueUp = '<b><div class="sp_sb_cs_db_data_col _0" style="left: 0%; width: '+colwl+'%; max-width: '+colwl+'%; min-width: '+colwl+'%">' + '<p class="paragraph_1">' + response.data.sNs[0] + 
										'</p></div><div class="sp_sb_cs_db_data_col _1" style="left: '+(colwl + adjl)+'%; width: '+colwl+'%; max-width: '+colwl+'%; min-width: '+colwl+'%">' + '<p class="paragraph_1">' + response.data.sNs[1] + 
										'</p></div><div class="sp_sb_cs_db_data_col _2" style="left: '+(colwl*2 + adjl)+'%; width: '+colwl+'%; max-width: '+colwl+'%; min-width: '+colwl+'%">' + '<p class="paragraph_1">' + response.data.sNs[2] + 
										'</p></div><div class="sp_sb_cs_db_data_col _3" style="left: '+(colwl*3 + adjl)+'%; width: '+colwl+'%; max-width: '+colwl+'%; min-width: '+colwl+'%">' + '<p class="paragraph_1">' + response.data.sNs[3] + 
										'</p></div><div class="sp_sb_cs_db_data_col _4" style="left: '+(colwl*4 + adjl)+'%; width: '+colwl+'%; max-width: '+colwl+'%; min-width: '+colwl+'%">' + '<p class="paragraph_1">' + response.data.sNs[4] + 
										'</p></div><div class="sp_sb_cs_db_data_col _5" style="left: '+(colwl*5 + adjl)+'%; width: '+colwl+'%; max-width: '+colwl+'%; min-width: '+colwl+'%">' + '<p class="paragraph_1">' + response.data.sNs[5] + '</p></div>';
								

										$('div.sp_sb_cs_sb_user_text').css({'top':'14%'});
										$('div.sp_sb_cs_sb_slide_title').css({'top':'5%'});

										//console.log('textvalueUp: '+textvalueUp);
										//console.log('len: '+response.data.lUs.length);
										//console.log('colwidth: '+colwidth);

											for (let i = 6; i < response.data.lUs.length; i++) {

												textvalue += '<div class="sp_sb_cs_db_data_col _' + i + '" style="left: ' + (colwr*i + adjr) + '%; width: ' + colwr + '%; max-width: ' + colwr + '%; min-width: ' + colwr + '%">'+
												'<p class="paragraph_1">' + response.data.lUs[i] + '<br>'+ response.data.lMs[i] + '<br>'+ response.data.lDs[i] + '</p></div>';

												textvalueUp += '<div class="sp_sb_cs_db_data_col _' + i + '" style="left: ' + (colwr*i + adjr) + '%; width: ' + colwr + '%; max-width: ' + colwr + '%; min-width: ' + colwr + '%">'+
												'<p class="paragraph_1">' + response.data.sNs[i] + '</p></div>';
											}

										//console.log(textvalue);
										$('.sp_sb_cs_sb_data_block')[0].innerHTML = textvalue + '</b>';
										$('.sp_sb_cs_sb_user_text')[0].innerHTML = textvalueUp + '</b>';

										$('.sp_sb_cs_sb_graph_block')[0].innerHTML = 
											'<div class="sp_sb_cs_gb_graph15_col_0"><b><div class="sp_sb_cs_gb_graph15_c0_row_1"><div class="sp_sb_cs_gb_graph15_c0_row_in">Sales Value ' + response.currency + '</div></div>' + 
											'<div class="sp_sb_cs_gb_graph15_c0_row_2"><div class="sp_sb_cs_gb_graph15_c0_row_in">Units Count</div></div>' + 
											'<div class="sp_sb_cs_gb_graph15_c0_row_3"><div class="sp_sb_cs_gb_graph15_c0_row_in">Price ' + response.currency + '</div></div></b></div><div class="sp_sb_cs_gb_graph15_col_1"><div class="sp_sb_cs_gb_graph15_c1_row_1"><div class="sp_sb_cs_gb_graph15_row_in"><canvas id="gr_canvas_0" style="width: 100%; height: 100%"></canvas></div></div>' + 
											'<div class="sp_sb_cs_gb_graph15_c1_row_2"><div class="sp_sb_cs_gb_graph15_row_in"><canvas id="gr_canvas_1" style="width: 100%; height: 100%"></canvas></div></div>' + 
											'<div class="sp_sb_cs_gb_graph15_c1_row_3"><div class="sp_sb_cs_gb_graph15_row_in"><canvas id="gr_canvas_2" style="width: 100%; height: 100%"></canvas></div></div></div>';
											Chart.defaults.global.defaultFontSize = 14;
											Chart.defaults.global.defaultFontStyle = 'bold';
											Chart.defaults.global.defaultFontColor = 'black';
											Chart.defaults.global.defaultFontFamily = 'Verdana';
											Chart.defaults.global.elements.rectangle.borderColor = 'rgba(25, 25, 25, 1)';
											Chart.defaults.global.elements.rectangle.borderWidth = 0.7;
											Chart.defaults.global.scaleBeginAtZero = true;

											let maxval;
											let minval;

											//console.log('111 datasets_0: '+JSON.stringify(response.data.datasets_0));

												maxval = Math.max.apply(null, response.data.datasets_0[0].data);
												minval = Math.abs(Math.min.apply(null, response.data.datasets_0[1].data));
												//console.log('minval: '+minval);
												//console.log('maxval: '+maxval);
												let maxval_0 = Math.max.apply(null, [maxval,minval]);
												//console.log('maxval_0: '+maxval_0);

												let data_0 = {
													labels: response.data.ss,
													datasets: response.data.datasets_0,
												};

												maxval = Math.max.apply(null, response.data.datasets_1[0].data);
												minval = Math.abs(Math.min.apply(null, response.data.datasets_1[1].data));
												let maxval_1 = Math.max.apply(null, [maxval,minval]);
												//console.log('maxval_1: '+maxval_1);

												console.log('111 datasets_1: '+JSON.stringify(response.data.datasets_1));
												console.log('minval: '+minval);
												console.log('maxval: '+maxval);
												console.log('maxval_1: '+maxval_1);

												let data_1 = {
													labels: response.data.ss,
													datasets: response.data.datasets_1,
												};

												maxval = Math.max.apply(null, response.data.datasets_2[0].data);
												minval = Math.abs(Math.min.apply(null, response.data.datasets_2[1].data));
												let maxval_2 = Math.max.apply(null, [maxval,minval]);
												//console.log('maxval_2: '+maxval_2);

												let data_2 = {
													labels: response.data.ss,
													datasets: response.data.datasets_2,
												};
													
												let ctx_0 = document.getElementById('gr_canvas_0').getContext('2d');
												let ctx_1 = document.getElementById('gr_canvas_1').getContext('2d');
												let ctx_2 = document.getElementById('gr_canvas_2').getContext('2d');

												let max = Math.max.apply(null, response.data.datasets_0[0].data);
												let min = Math.abs(Math.min.apply(null, response.data.datasets_0[1].data));
												let max_0 = Math.max.apply(null, [max,min]) * 1.1;
												max = Math.max.apply(null, response.data.datasets_1[0].data);
												min = Math.abs(Math.min.apply(null, response.data.datasets_1[1].data));
												let max_1 = Math.max.apply(null, [max,min]) * 1.1;
												max = Math.max.apply(null, response.data.datasets_2[0].data);
												min = Math.abs(Math.min.apply(null, response.data.datasets_2[1].data));
												let max_2 = Math.max.apply(null, [max,min]) * 1.1;

												let lim_mrg = 50;
													
													window.chart_0 = new Chart(ctx_0, {
														type: 'bar',
														data: data_0,
														options: {
															layout: {
															  padding: {
																right: 0,
																left: 0
															  }
															},
															devicePixelRatio: window.devicePixelRatio*res_coef,
															title: {
																display: false,
																text: 'Sales Value RUB',
																position: 'left',
															},
															legend: {
																display: false,
															},
															tooltips: {
																enabled: false,
															},
															responsive: true,
															scales: {
																yAxes: [{
																	display: false,
																	gridLines: {
																		display: false,
																	},
																	stacked: true,
																	ticks: {
																		min: -max_0,
																		max: max_0,
																		display: false,
																	},
																}],
																xAxes: [{
																	barPercentage: 1,
																	categoryPercentage: 0.80,
																	display: false,
																	gridLines: {
																		display: false,
																	},
																	stacked: true,
																	//ticks: {
																	//	display: false,
																	//	beginAtZero : true
																	//},
																	id: 'x-axis-0',
																	  gridLines: {
																		display: true,
																		lineWidth: 1,
																		color: "rgba(127,127,127,0.30)"
																	  },
																	  ticks: {
																		beginAtZero:true,
																		mirror:false,
																		//padding: 40,
																		suggestedMin: 0,
																		suggestedMax: 500,
																		 /*backdropPadding: {
																			  x: 0,
																			  y: 0
																		  }*/
																	  },
																	  afterBuildTicks: function(chart) {
																	  }
																}],
															},
															plugins: {
																datalabels: {
																	textAlign: 'center',
																	font: {
																		size: 14,
																	},
																	clamp: true,
																	display: true,
																	clip: true,
																	padding: { left: 1 },
																	color: function(context) {
																			var index = context.dataIndex;
																			let prclim = Math.abs(context.dataset.data[index])*100/maxval_0;
																				if(prclim > lim_mrg) {
																				  var color = context.dataset.backgroundColor;
																				  var result_rgb = rgbaToRgb(color);
																				  var cc = contrastingColor(result_rgb);
																				  return '#'+cc;
																				} else { return 'black'; }
																			},
																	anchor: function(context) {
																		let prclim = Math.abs(context.dataset.data[context.dataIndex])*100/maxval_0;
																		//console.log('prclim0: '+prclim);
																		if(context.dataset.data[context.dataIndex] < 0 && prclim < lim_mrg) return 'start';
																		else if(context.dataset.data[context.dataIndex] > 0 && prclim < lim_mrg) return 'end';
																		else return 'center';
																	},
																	align: function(context) {
																		let prclim = Math.abs(context.dataset.data[context.dataIndex])*100/maxval_0;
																		if(context.dataset.data[context.dataIndex] < 0 && prclim < lim_mrg) return 'start';
																		else if(context.dataset.data[context.dataIndex] > 0 && prclim < lim_mrg) return 'end';
																		else return 'center';
																	},
																	formatter: function(value, ctx_0) {
																		if (ctx_0.dataset.data[ctx_0.dataIndex] != 0) {
																			if (Math.abs(ctx_0.dataset.data[ctx_0.dataIndex]) > 1) {
																				ctx_0.dataset.data[ctx_0.dataIndex] = Math.round(ctx_0.dataset.data[ctx_0.dataIndex]);
																			}
																			let tmpstr = String(ctx_0.dataset.data[ctx_0.dataIndex]);
																			if (ctx_0.dataset.data[ctx_0.dataIndex] > 0) {
																				tmpstr = '+' + tmpstr;
																			}
																			return tmpstr;
																		} else {
																			return '';
																		}
																	},
																},
															},
														},
													});
													window.chart_1 = new Chart(ctx_1, {
														type: 'bar',
														data: data_1,
														options: {
															devicePixelRatio: window.devicePixelRatio*res_coef,
															title: {
																display: false,
																text: 'Sales Piece/Litre/Kilos',
																position: 'left',
															},
															legend: {
																display: false,
															},
															tooltips: {
																enabled: false,
															},
															scales: {
																yAxes: [{
																	display: false,
																	gridLines: {
																		display: false,
																	},
																	stacked: true,
																	ticks: {
																		min: -max_1,
																		max: max_1,
																		display: false,
																	},
																}],
																xAxes: [{
																	barPercentage: 1,
																	categoryPercentage: 0.80,
																	display: false,
																	gridLines: {
																		display: false,
																	},
																	stacked: true,
																	ticks: {
																		display: false,
																	},
																}],
															},
															plugins: {
																datalabels: {
																	textAlign: 'center',
																	font: {
																		size: 14,
																	},
																	clamp: true,
																	display: true,
																	clip: true,
																	color: function(context) {
																			var index = context.dataIndex;
																			let prclim = Math.abs(context.dataset.data[index])*100/maxval_1;
																				if(prclim > lim_mrg) {
																				  var color = context.dataset.backgroundColor;
																				  var result_rgb = rgbaToRgb(color);
																				  var cc = contrastingColor(result_rgb);
																				  return '#'+cc;
																				} else { return 'black'; }
																			},
																	anchor: function(context) {
																		let prclim = Math.abs(context.dataset.data[context.dataIndex])*100/maxval_1;
																		console.log('prclim1: '+prclim);
																		console.log('context.dataset.data[context.dataIndex]: '+context.dataset.data[context.dataIndex]);
																		if(context.dataset.data[context.dataIndex] < 0 && prclim < lim_mrg) return 'start';
																		else if(context.dataset.data[context.dataIndex] > 0 && prclim < lim_mrg) return 'end';
																		else return 'center';
																	},
																	align: function(context) {
																		let prclim = Math.abs(context.dataset.data[context.dataIndex])*100/maxval_1;
																		if(context.dataset.data[context.dataIndex] < 0 && prclim < lim_mrg) return 'start';
																		else if(context.dataset.data[context.dataIndex] > 0 && prclim < lim_mrg) return 'end';
																		else return 'center';
																	},
																	formatter: function(value, ctx_1) {
																		if (ctx_1.dataset.data[ctx_1.dataIndex] != 0) {
																			if (Math.abs(ctx_1.dataset.data[ctx_1.dataIndex]) > 1) {
																				ctx_1.dataset.data[ctx_1.dataIndex] = Math.round(ctx_1.dataset.data[ctx_1.dataIndex]);
																			}
																			let tmpstr = String(ctx_1.dataset.data[ctx_1.dataIndex]);
																			if (ctx_1.dataset.data[ctx_1.dataIndex] > 0) {
																				tmpstr = '+' + tmpstr;
																			}
																			return tmpstr;
																		} else {
																			return '';
																		}
																	},
																},
															},
														},
													});
													window.chart_2 = new Chart(ctx_2, {
														type: 'bar',
														data: data_2,
														options: {
															devicePixelRatio: window.devicePixelRatio*res_coef,
															title: {
																display: false,
																text: 'Price RUB',
																position: 'left',
															},
															legend: {
																display: false,
															},
															tooltips: {
																enabled: false,
															},
															scales: {
																yAxes: [{
																	//barPercentage: 1,
																	//categoryPercentage: 1,
																	display: false,
																	gridLines: {
																		display: false,
																	},
																	stacked: true,
																	ticks: {
																		min: -max_2,
																		max: max_2,
																		display: false,
																	},
																}],
																xAxes: [{
																	barPercentage: 1,
																	categoryPercentage: 0.80,
																	display: false,
																	gridLines: {
																		display: false,
																	},
																	stacked: true,
																	ticks: {
																		display: false,
																	},
																}],
															},
															plugins: {
																datalabels: {
																	textAlign: 'center',
																	font: {
																		size: 14,
																	},
																	clamp: true,
																	display: true,
																	clip: true,
																	color: function(context) {
																			var index = context.dataIndex;
																			let prclim = Math.abs(context.dataset.data[index])*100/maxval_2;
																				if(prclim > lim_mrg) {
																				  var color = context.dataset.backgroundColor;
																				  var result_rgb = rgbaToRgb(color);
																				  var cc = contrastingColor(result_rgb);
																				  return '#'+cc;
																				} else { return 'black'; }
																			},
																	anchor: function(context) {
																		let prclim = Math.abs(context.dataset.data[context.dataIndex])*100/maxval_2;
																		console.log('context.dataset.data[context.dataIndex]: '+context.dataset.data[context.dataIndex]);
																		console.log('prclim2: '+prclim);
																		if(context.dataset.data[context.dataIndex] < 0 && prclim < lim_mrg) return 'start';
																		else if(context.dataset.data[context.dataIndex] > 0 && prclim < lim_mrg) return 'end';
																		else return 'center';
																	},
																	align: function(context) {
																		let prclim = Math.abs(context.dataset.data[context.dataIndex])*100/maxval_2;
																		if(context.dataset.data[context.dataIndex] < 0 && prclim < lim_mrg) return 'start';
																		else if(context.dataset.data[context.dataIndex] > 0 && prclim < lim_mrg) return 'end';
																		else return 'center';
																	},
																	formatter: function(value, ctx_2) {
																		if (ctx_2.dataset.data[ctx_2.dataIndex] != 0) {
																			if (Math.abs(ctx_2.dataset.data[ctx_2.dataIndex]) > 1) {
																				ctx_2.dataset.data[ctx_2.dataIndex] = Math.round(ctx_2.dataset.data[ctx_2.dataIndex]);
																			}
																			let tmpstr = String(ctx_2.dataset.data[ctx_2.dataIndex]);
																			if (ctx_2.dataset.data[ctx_2.dataIndex] > 0) {
																				tmpstr = '+' + tmpstr;
																			}
																			return tmpstr;
																		} else {
																			return '';
																		}
																	},
																},
															},
														},
													});
												}
												else {
													textvalue= '';
													let colors = ['rgba(0, 114, 218, 1)', 'rgba(0, 156, 73, 1)', 'rgba(240, 66, 121, 1)', 'rgba(255, 127, 0, 1)', 'rgba(167, 122, 215, 1)', 'rgba(255, 255, 0, 1)', 'rgba(153, 0, 102, 1)', 'rgba(48, 213, 200, 1)', 'rgba(188, 152, 126, 1)', 'rgba(140, 203, 94, 1)', 'rgba(176, 0, 0, 1)', 'rgba(0, 191, 255, 1)', 'rgba(255, 36, 0, 1)', 'rgba(153, 255, 153, 1)', 'rgba(253, 234, 168, 1)'];

													let names = response.data.sNs;
													if(BrandsTypes.includes(window.cSD.sI)) {
														for (let i = 0; i < names.length; i++) {
															let brand = names[i].trim();
															if (brand in window.BrandsData) {
																colors[i] = window.BrandsData[brand].color;
																names[i] = window.BrandsData[brand].name;
															}
														}
													}
													if (window.cSD.sI == 0) {
														for (let i = 0; i < names.length; i++) {
															colors[i] = window.cSD.dD[i].clr;
															names[i] = window.cSD.dD[i].sS.toLocaleString() + ' - ' + window.cSD.dD[i].eS.toLocaleString();
														}
													}
													textvalue = '<div class="sp_sb_cs_gb_graph_col_0"><canvas id="gr_canvas_0" style="width: 100%; height: 100%"></canvas></div>' + 
														'<div class="sp_sb_cs_gb_graph_col_1"></div>' + 
														'<div class="sp_sb_cs_gb_graph_col_2"><canvas id="gr_canvas_1" style="width: 100%; height: 100%"></canvas></div>' + 
														'<div class="sp_sb_cs_gb_graph_col_3">' + 
															'<div class="sp_sb_cs_gb_gc3_series_list">' + 
																'<div class="sp_sb_cs_gb_gc3_sl_in">';
													for (let i = 0; i < response.data.sNs.length; i++) {
														textvalue += '<div class="sp_sb_cs_gb_gc3_sl_series_row _' + i + '" style="top: ' + (4 + 14.28 * i) + '%;"><div class="sp_sb_cs_gb_gc3_sl_sr_color_box" style="background-color: ' + colors[i] + ';"></div><div class="sp_sb_cs_gb_gc3_sl_sr_space"></div><p class="sp_sb_cs_gb_gc3_sl_sr_item_name">' + names[i] + '</p></div>';
													}
													$('.sp_sb_cs_sb_graph_block')[0].innerHTML = textvalue + '</div></div></div><div class="sp_sb_cs_gb_graph_col_4"><canvas id="gr_canvas_2" style="width: 100%; height: 100%"></canvas></div>';

													Chart.defaults.global.defaultFontSize = 13;
													Chart.defaults.global.defaultFontStyle = 'bold';
													Chart.defaults.global.defaultFontColor = 'black';
													Chart.defaults.global.defaultFontFamily = 'Verdana';
													Chart.defaults.global.elements.rectangle.borderColor = 'rgba(25, 25, 25, 1)';
													Chart.defaults.global.elements.rectangle.borderWidth = 0.7;

													if(Number(window.cSD.iI) === 1) {
														response.data.lMs.forEach(function(e, i, a) {
															   a[i] = e.replace(/\-/,'-\n');
														});
														response.data.lYs.forEach(function(e, i, a) {
															   a[i] = e.replace(/\-/,'-\n');
														});
													}
													
													let data_0 = {
														labels: response.data.lMs,
														fill: false,
														datasets: [],
													};
													let data_1 = {
														labels: response.data.lYs,
														fill: false,
														datasets: [],
													};
													let data_2 = {
														labels: [],
														fill: false,
														datasets: [],
													};
													for (let i = 0; i < response.data.rs[0].iD.length; i++) {
														if(Number(window.cSD.iI) === 1) {
															response.data.rs[0].iD[i].idn = '\n'+response.data.rs[0].iD[i].idn
														}
														data_2.labels.unshift(response.data.rs[0].iD[i].idn);
													}
													let ctx_0 = document.getElementById('gr_canvas_0').getContext('2d');
													let ctx_1 = document.getElementById('gr_canvas_1').getContext('2d');
													let ctx_2 = document.getElementById('gr_canvas_2').getContext('2d');

													let legend_col_height = getPercentHeight($('div.sp_sb_cs_gb_gc3_sl_in'));

													if(response.data.sNs.length <=7) {
														let col_height = 35;
														$('div.sp_sb_cs_gb_gc3_sl_in').css({'min-height':col_height+'%'});
														$('div.sp_sb_cs_gb_gc3_sl_in').css({'max-height':col_height+'%'});
														$('div.sp_sb_cs_gb_gc3_sl_in').css({height:col_height+'%'});
														$('div.sp_sb_cs_gb_gc3_sl_in').css({'top':'calc(30% + 2.2vh)'});

														let col_top = 0;
														$("div.sp_sb_cs_gb_gc3_sl_series_row").each(function(index){
															let legend_col_top = getPercentTop($(this));
															col_top = col_top+10;
															$(this).css({'top':+col_top+'%'});
															$(this).css({'height':'7%'});
															$(this).css({'min-height':'7%'});
															$(this).css({'max-height':'7%'});
														});
													} else
													if(response.data.sNs.length > 7 && response.data.sNs.length < 15) {
														let col_height_ajust = (response.data.sNs.length)*2.5;
														$('div.sp_sb_cs_gb_gc3_sl_in').css({'min-height':(legend_col_height+col_height_ajust)+'%'});
														$('div.sp_sb_cs_gb_gc3_sl_in').css({'max-height':(legend_col_height+col_height_ajust)+'%'});
														$('div.sp_sb_cs_gb_gc3_sl_in').css({height:(legend_col_height+col_height_ajust)+'%'});
														$('div.sp_sb_cs_gb_gc3_sl_in').css({'top':'calc(20% + 2.2vh)'});

														let col_top = 0;
														$("div.sp_sb_cs_gb_gc3_sl_series_row").each(function(index){
															let legend_col_top = getPercentTop($(this));
															col_top = col_top+5.8;
															$(this).css({'top':+col_top+'%'});
															});
													} else
													if(response.data.sNs.length >= 15 && response.data.sNs.length < 18) {
														let col_height = (response.data.sNs.length)*4.5;
														let prc_ajs = 30 - response.data.sNs.length;
														$('div.sp_sb_cs_gb_gc3_sl_in').css({'min-height':col_height+'%'});
														$('div.sp_sb_cs_gb_gc3_sl_in').css({'max-height':col_height+'%'});
														$('div.sp_sb_cs_gb_gc3_sl_in').css({height:col_height+'%'});
														$('div.sp_sb_cs_gb_gc3_sl_in').css({'top':'calc('+prc_ajs+'% + 1.3vh)'});

														let col_top = 0;
														$("div.sp_sb_cs_gb_gc3_sl_series_row").each(function(index){
															let legend_col_top = getPercentTop($(this));
															col_top = col_top+5;
															$(this).css({'top':+col_top+'%'});
														});
													} else
													if(response.data.sNs.length >= 18) {
														let col_height = (response.data.sNs.length)*3.8;
														let prc_ajs = 30 - response.data.sNs.length*0.8;
														$('div.sp_sb_cs_gb_gc3_sl_in').css({'min-height':col_height+'%'});
														$('div.sp_sb_cs_gb_gc3_sl_in').css({'max-height':col_height+'%'});
														$('div.sp_sb_cs_gb_gc3_sl_in').css({height:col_height+'%'});
														$('div.sp_sb_cs_gb_gc3_sl_in').css({'top':'calc('+prc_ajs+'% + 0.7vh)'});

														let col_top = 0;
														$("div.sp_sb_cs_gb_gc3_sl_series_row").each(function(index){
															let legend_col_top = getPercentTop($(this));
															col_top = col_top+4.6;
															$(this).css({'top':+col_top+'%'});
														});
													}
													
													let sum_0_0 = 0;
													let sum_0_1 = 0;
													let sum_1_0 = 0;
													let sum_1_1 = 0;
													let sums_2 = [];
													for (let i = 0; i < response.data.rs[0].iD.length; i++) {
														sums_2.unshift(0);
													}
													for (let i = 0; i < response.data.sNs.length; i++) {
														if (sum_0_0 < Number(response.data.rs[i].mtmpp)) {
															sum_0_0 = Number(response.data.rs[i].mtmpp);
														}
														if (sum_0_1 < Number(response.data.rs[i].mtmcp)) {
															sum_0_1 = Number(response.data.rs[i].mtmcp);
														}
														data_0.datasets.unshift({label: response.data.sNs[i], fill: false, pointRadius: 5, pointStyle: 'rectRounded', borderColor: colors[i], backgroundColor: colors[i], data: [response.data.rs[i].mtmpp, response.data.rs[i].mtmcp], tooltipsValues: [response.data.rs[i].mtmpv, response.data.rs[i].mtmcv]});
														if (sum_1_0 < Number(response.data.rs[i].ytdpp)) {
															sum_1_0 = Number(response.data.rs[i].ytdpp);
														}
														if (sum_1_1 < Number(response.data.rs[i].ytdcp)) {
															sum_1_1 = Number(response.data.rs[i].ytdcp);
														}
														data_1.datasets.unshift({label: response.data.sNs[i], fill: false, pointRadius: 5, pointStyle: 'rectRounded', borderColor: colors[i], backgroundColor: colors[i], data: [response.data.rs[i].ytdpp, response.data.rs[i].ytdcp], tooltipsValues: [response.data.rs[i].ytdpv, response.data.rs[i].ytdcv]});
														data_2.datasets.unshift({label: response.data.sNs[i], fill: false, pointRadius: 5, pointStyle: 'rectRounded', borderColor: colors[i], backgroundColor: colors[i], data: [], tooltipsValues: []});
														for (let j = 0; j < response.data.rs[i].iD.length; j++) {
															data_2.datasets[0].data.unshift(response.data.rs[i].iD[j].idp);
															data_2.datasets[0].tooltipsValues.unshift(response.data.rs[i].iD[j].idv);
															if (sums_2[j] < Number(response.data.rs[i].iD[j].idp)) {
																sums_2[j] = Number(response.data.rs[i].iD[j].idp);
															}
														}
													}
														
													let max_0 = ((sum_0_0 > sum_0_1) ? sum_0_0 : sum_0_1) * 1.05;
													let max_1 = ((sum_1_0 > sum_1_1) ? sum_1_0 : sum_1_1) * 1.05;
													let max_2 = Math.max.apply(null, sums_2) * 1.05
													
													let min_0 = -((sum_0_0 > sum_0_1) ? sum_0_0 : sum_0_1) * 0.05;
													let min_1 = -((sum_1_0 > sum_1_1) ? sum_1_0 : sum_1_1) * 0.05;
													let min_2 = -Math.max.apply(null, sums_2) * 0.05

													let abs_max = Math.ceil(Math.max(max_0, max_1, max_2));
													console.log('abs_max: '+abs_max);
													
													console.log('data_0: '+JSON.stringify(data_2));
													
													if (Number(window.cSD.iI) === 1) {
														$('div.sp_sb_cs_gb_graph_col_0').css({'min-width':'9.66%'});
														$('div.sp_sb_cs_gb_graph_col_0').css({width:'9.66%'});

														$('div.sp_sb_cs_gb_graph_col_1').css({left:'9.66%'});

														$('div.sp_sb_cs_gb_graph_col_2').css({left:'11.66%'});
														$('div.sp_sb_cs_gb_graph_col_2').css({'min-width':'9.66%'});
														$('div.sp_sb_cs_gb_graph_col_2').css({width:'9.66%'});

														$('div.sp_sb_cs_gb_graph_col_3').css({left:'21.32%'});

														$('div.sp_sb_cs_gb_graph_col_4').css({left:'32.32%'});
														$('div.sp_sb_cs_gb_graph_col_4').css({'min-width':'67.62%'});
														$('div.sp_sb_cs_gb_graph_col_4').css({'max-width':'67.62%'});
														$('div.sp_sb_cs_gb_graph_col_4').css({width:'67.62%'});
													} 
													
													var vals = {};
													window.chart_0 = new Chart(ctx_0, {
														type: 'line',
														data: data_0,
														fill: false,
														options: {
															elements: {
																line: {
																	tension: 0 // disables bezier curves
																}
															},
															devicePixelRatio: window.devicePixelRatio*res_coef,
															hover: {
																mode: null,
																events: [],
																animationDuration: 0,
															},
															legend: {
																display: false,
															},
															tooltips: {
																mode: 'index',
																backgroundColor: 'rgba(255, 255, 255, 0.75)',
																titleFontSize: 14,
																titleFontColor: 'rgba(0, 0, 0, 1)',
																bodyFontSize: 14,
																bodyFontColor: 'rgba(0, 0, 0, 1)',
																bodyFontStyle: 'normal',
																intersect: false,
																callbacks: {
																	label: function(tooltipItem, data) {
																		return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].tooltipsValues[tooltipItem.index];
																	},
																},
															},
															responsive: true,
															scales: {
																xAxes: [{
																	offset: true,
																	barPercentage: 1.0,
																	categoryPercentage: 0.9,
																	gridLineHeight: 20,
																	gridLines: {
																		display: true,
																		drawBorder: true,
																		drawOnChartArea: false
																	},
																	stacked: false,
																	position: 'top',
																	display: true
																}],
																yAxes: [{
																	display: true,
																	gridLines: {
																		display: true,
																		drawBorder: true,
																		drawOnChartArea: false
																	},
																	stacked: false,
																	ticks: {
																		max: abs_max, // max_0,
																		min: 0, //min_0,
																		display: true,
																		callback: function(value, context) {
																		let msscht;
																			switch (window.cSD.sM) {
																				case '0':
																					msscht = 10000000;
																					break;
																				case '1':
																					msscht = 10000;
																					break;
																				case '2':
																					msscht = 10;
																					break;
																				default:
																					msscht = 10000000;
																			}
																		let val = value/msscht/100;
																		return Math.ceil(val.round2(1));
																		}
																	},
																	afterFit: (scaleInstance) => {
																		scaleInstance.height = 60;
																	},
																}],
															},
															plugins: {
																datalabels: {
																	offset: 8,
																	padding: 3,
																	textAlign: 'center',
																	backgroundColor: function(context) {
																		return context.dataset.backgroundColor;
																	},
																	borderColor: function(context) {
																	  return context.dataset.backgroundColor;
																	},
																	borderWidth: 0,
																	color: function(context) {
																		  var index = context.dataIndex;
																		  var color = context.dataset.backgroundColor;
																		  var result_rgb = rgbaToRgb(color);
																		  var cc = contrastingColor(result_rgb);
																		  return '#'+cc;
																	},
																	font: {
																		size: 11,
																		weight: 'normal'
																	},
																	display: true,
																	align: function(context) {
																		let idx = context.dataIndex;
																		if(typeof(vals[idx]) == "undefined") {
																			vals[idx] = [];
																		}
																		vals[idx].push(context.dataset.data[idx]);
																		let len = vals[idx].length;
																		let prc;
																		if(len > 1) {
																			for(let i=0; i<=len-2; i++) {
																				prc = Math.round(Math.abs(vals[idx][i] - vals[idx][len-1])*100/vals[idx][i]);
																				if(prc < 5) {
																					return (vals[idx][i] > vals[idx][len-1]) ? 'start' : 'end';
																				}
																			}
																		}
																		return context.active ? 'start' : 'center';
																	},
																	formatter: function(value, context) {
																		let msscht;
																		switch (window.cSD.sM) {
																			case '0':
																				msscht = 10000000;
																				break;
																			case '1':
																				msscht = 10000;
																				break;
																			case '2':
																				msscht = 10;
																				break;
																			default:
																				msscht = 10000000;
																		}
																		let val = value/msscht/100;
																		return val.round2(1);
																	},
																},
															},
															animation: {
																duration: 1,
																onComplete: function() {																	
																	let chartInstance = this.chart,
																	ctxthis = chartInstance.ctx;
																	
																	/*ctxthis.save();
																	ctxthis.globalCompositeOperation='destination-over';

																	
																	ctxthis.font = Chart.helpers.fontString(11, 'normal', Chart.defaults.global.defaultFontFamily);
																	ctxthis.fillStyle = 'rgba(0, 0, 0, 1)';
																	ctxthis.textAlign = 'center';
																	ctxthis.textBaseline = 'bottom';
																																		
																	ctxthis.fillStyle = 'red';
																	//context.dataset.backgroundColor;
																	
																	let datas = this.data;
																	this.data.labels.forEach(function(label, label_index) {
																		console.log('label:'+label);
																		console.log('label index:'+label_index);
																		datas.datasets.forEach(function(dataset, dataset_index) {
																			console.log('dataset:'+dataset);
																			console.log('dataset index:'+dataset_index);
																			let meta = chartInstance.controller.getDatasetMeta(dataset_index);
																			//console.log('meta: '+JSON.stringify(meta.data[label_index]));
																			ctxthis.fillText(meta.controller._data[label_index], meta.data[label_index]._model.x, meta.data[label_index]._model.y - 5);
																			console.log('Value: '+meta.controller._data[label_index]);
																		});
																	});
																	ctxthis.restore();*/
																},
															},
														},
														plugins: [{
															beforeInit: function(chart) {
																chart.data.labels.forEach(function(e, i, a) {
																	if (/\n/.test(e)) {
																		if(Number(window.cSD.iI) === 1) {
																			a[i] = e.split(/\n/);
																		} else {
																			a[i] = e.replace(/\n/, '');
																		}
																	}
																});
															}
														}]
													});
													var vals = {};
													window.chart_1 = new Chart(ctx_1, {
														type: 'line',
														data: data_1,
														fill: false,
														options: {
															elements: {
																line: {
																	tension: 0 // disables bezier curves
																}
															},
															devicePixelRatio: window.devicePixelRatio*res_coef,
															hover: {
																mode: null,
																events: [],
																animationDuration: 0,
															},
															legend: {
																display: false,
															},
															tooltips: {
																mode: 'index',
																backgroundColor: 'rgba(255, 255, 255, 0.75)',
																titleFontSize: 14,
																titleFontColor: 'rgba(0, 0, 0, 1)',
																bodyFontSize: 14,
																bodyFontColor: 'rgba(0, 0, 0, 1)',
																bodyFontStyle: 'normal',
																intersect: false,
																callbacks: {
																	label: function(tooltipItem, data) {
																		return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].tooltipsValues[tooltipItem.index];
																	},
																},
															},
															responsive: true,
															scales: {
																xAxes: [{
																	offset: true,
																	barPercentage: 1.0,
																	categoryPercentage: 0.9,
																	gridLineHeight: 20,
																	gridLines: {
																		display: true,
																		drawBorder: true,
																		drawOnChartArea: false
																	},
																	stacked: false,
																	position: 'top',
																	display: true
																}],
																yAxes: [{
																	display: false,
																	gridLines: {
																		display: true,
																		drawBorder: true,
																		drawOnChartArea: false
																	},
																	stacked: false,
																	ticks: {
																		max: abs_max, // max_1,
																		min: 0, // min_1,
																		display: true,
																		callback: function(value, context) {
																		let msscht;
																			switch (window.cSD.sM) {
																				case '0':
																					msscht = 10000000;
																					break;
																				case '1':
																					msscht = 10000;
																					break;
																				case '2':
																					msscht = 10;
																					break;
																				default:
																					msscht = 10000000;
																			}
																		let val = value/msscht/100;
																		return val.round2(1);
																		}
																	},
																	afterFit: (scaleInstance) => {
																		scaleInstance.height = 60;
																	},
																}],
															},
															plugins: {
																datalabels: {
																	offset: 8,
																	padding: 3,
																	textAlign: 'center',
																	backgroundColor: function(context) {
																		return context.dataset.backgroundColor;
																	},
																	borderColor: function(context) {
																	  return context.dataset.backgroundColor;
																	},
																	borderWidth: 0,
																	color: function(context) {
																		  var index = context.dataIndex;
																		  var color = context.dataset.backgroundColor;
																		  var result_rgb = rgbaToRgb(color);
																		  var cc = contrastingColor(result_rgb);
																		  return '#'+cc;
																	},
																	font: {
																		size: 11,
																		weight: 'normal'
																	},
																	display: true,
																	align: function(context) {
																		let idx = context.dataIndex;
																		if(typeof(vals[idx]) == "undefined") {
																			vals[idx] = [];
																		}
																		vals[idx].push(context.dataset.data[idx]);
																		let len = vals[idx].length;
																		let prc;
																		if(len > 1) {
																			for(let i=0; i<=len-2; i++) {
																				prc = Math.round(Math.abs(vals[idx][i] - vals[idx][len-1])*100/vals[idx][i]);
																				if(prc < 5) {
																					return (vals[idx][i] > vals[idx][len-1]) ? 'start' : 'end';
																				}
																			}
																		}
																		return context.active ? 'start' : 'center';
																	},
																	formatter: function(value, context) {
																		let msscht;
																		switch (window.cSD.sM) {
																			case '0':
																				msscht = 10000000;
																				break;
																			case '1':
																				msscht = 10000;
																				break;
																			case '2':
																				msscht = 10;
																				break;
																			default:
																				msscht = 10000000;
																		}
																		let val = value/msscht/100;
																		return val.round2(1);
																	},
																},
															},
															animation: {
																duration: 1,
																onComplete: function() {
																	let chartInstance = this.chart,
																		ctxthis = chartInstance.ctx;
																	/*ctxthis.font = Chart.helpers.fontString(11, 'normal', Chart.defaults.global.defaultFontFamily);
																	ctxthis.fillStyle = 'rgba(0, 0, 0, 1)';
																	ctxthis.textAlign = 'center';
																	ctxthis.textBaseline = 'bottom';
																	let datas = this.data;
																	this.data.labels.forEach(function(label, label_index) {
																		datas.datasets.forEach(function(dataset, dataset_index) {
																			let meta = chartInstance.controller.getDatasetMeta(dataset_index);
																			ctxthis.fillText(meta.controller._data[label_index], meta.data[label_index]._model.x, meta.data[label_index]._model.y - 5);
																		});
																	});
																	*/
																},
															},
														},
														plugins: [{
															  beforeInit: function(chart) {
																  chart.data.labels.forEach(function(e, i, a) {
																	  if (/\n/.test(e)) {
																		  if(Number(window.cSD.iI) === 1) {
																			  a[i] = e.split(/\n/);
																		  } else {
																			a[i] = e.replace(/\n/, '');
																		  }
																	  }
																  });
															  }
														   }]
													});
													var vals = {};
													window.chart_2 = new Chart(ctx_2, {
														type: 'line',
														data: data_2,
														fill: false,
														options: {
															elements: {
																line: {
																	tension: 0 // disables bezier curves
																}
															},
															devicePixelRatio: window.devicePixelRatio*res_coef,
															hover: {
																mode: null,
																events: [],
																animationDuration: 0,
															},
															legend: {
																display: false,
															},
															tooltips: {
																mode: 'index',
																backgroundColor: 'rgba(255, 255, 255, 0.75)',
																titleFontSize: 14,
																titleFontColor: 'rgba(0, 0, 0, 1)',
																bodyFontSize: 14,
																bodyFontColor: 'rgba(0, 0, 0, 1)',
																bodyFontStyle: 'normal',
																intersect: false,
																callbacks: {
																	label: function(tooltipItem, data) {
																		return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].tooltipsValues[tooltipItem.index];
																	},
																},
															},
															responsive: true,
															scales: {
																xAxes: [{
																	offset: true,
																	barPercentage: 1.0,
																	categoryPercentage: 0.9,
																	gridLineHeight: 20,
																	gridLines: {
																		display: true,
																		drawBorder: true,
																		drawOnChartArea: false
																	},
																	stacked: false,
																	position: 'top',
																	display: true
																}],
																yAxes: [{
																	display: false,
																	gridLines: {
																		display: true,
																		drawBorder: true,
																		drawOnChartArea: false
																	},
																	stacked: false,
																	ticks: {
																		max: abs_max, // max_2,
																		min: min_2,
																		display: true,
																		padding: 50,
																		callback: function(value, context) {
																		let msscht;
																			switch (window.cSD.sM) {
																				case '0':
																					msscht = 10000000;
																					break;
																				case '1':
																					msscht = 10000;
																					break;
																				case '2':
																					msscht = 10;
																					break;
																				default:
																					msscht = 10000000;
																			}
																		let val = value/msscht/100;
																		return val.round2(1);
																		}
																	},
																	afterFit: (scaleInstance) => {
																		scaleInstance.height = 60;
																	},
																}],
															},
															plugins: {
																datalabels: {
																	offset: 8,
																	padding: 3,
																	textAlign: 'center',
																	backgroundColor: function(context) {
																		return context.dataset.backgroundColor;
																	},
																	borderColor: function(context) {
																	  return context.dataset.backgroundColor;
																	},
																	borderWidth: 0,
																	color: function(context) {																	  
																		  var index = context.dataIndex;
																		  var color = context.dataset.backgroundColor;
																		  var result_rgb = rgbaToRgb(color);
																		  var cc = contrastingColor(result_rgb);
																		  return '#'+cc;
																	},
																	font: {
																		size: 11,
																		weight: 'normal'
																	},
																	display: true,
																	align: function(context) {
																		let idx = context.dataIndex;
																		if(typeof(vals[idx]) == "undefined") {
																			vals[idx] = [];
																		}
																		vals[idx].push(context.dataset.data[idx]);
																		let len = vals[idx].length;
																		let prc;
																		if(len > 1) {
																			for(let i=0; i<=len-2; i++) {
																				prc = Math.round(Math.abs(vals[idx][i] - vals[idx][len-1])*100/vals[idx][i]);
																				if(prc < 5) {
																					return (vals[idx][i] > vals[idx][len-1]) ? 'start' : 'end';
																				}
																			}
																		}
																		return context.active ? 'start' : 'center';
																	},
																	formatter: function(value, context) {
																		let msscht;
																		switch (window.cSD.sM) {
																			case '0':
																				msscht = 10000000;
																				break;
																			case '1':
																				msscht = 10000;
																				break;
																			case '2':
																				msscht = 10;
																				break;
																			default:
																				msscht = 10000000;
																		}
																		let val = value/msscht/100;
																		return val.round2(1);
																	},
																},
															},
															animation: {
																duration: 1,
																onComplete: function() {
																	let chartInstance = this.chart,
																	ctxthis = chartInstance.ctx;
																	/*ctxthis.font = Chart.helpers.fontString(11, 'normal', Chart.defaults.global.defaultFontFamily);
																	ctxthis.fillStyle = 'rgba(0, 0, 0, 1)';
																	ctxthis.textAlign = 'center';
																	ctxthis.textBaseline = 'bottom';
																	let datas = this.data;
																	this.data.labels.forEach(function(label, label_index) {
																		datas.datasets.forEach(function(dataset, dataset_index) {
																			let meta = chartInstance.controller.getDatasetMeta(dataset_index);
																			ctxthis.fillText(meta.controller._data[label_index], meta.data[label_index]._model.x, meta.data[label_index]._model.y - 5);
																		});
																	});
																	*/
																},
															},
														},
														plugins: [{
															  beforeInit: function(chart) {
																  chart.data.labels.forEach(function(e, i, a) {
																	  if (/\n/.test(e)) {
																		  if(Number(window.cSD.iI) === 1) {
																			  a[i] = e.split(/\n/);
																		  } else {
																			a[i] = e.replace(/\n/, '');
																		  }
																	  }
																  });
															  }
														   }],
													});
												}
						} else {
							console.log(response);
						}
					deferred.resolve();
					}
				});
			}
			
			return false;
		}

		function resizeImage(imgToResize, resizingFactor = 0.5) {
		  const canvas = document.createElement("canvas");
		  const context = canvas.getContext("2d");

		  const originalWidth = imgToResize.width;
		  const originalHeight = imgToResize.height;

		  const canvasWidth = originalWidth * resizingFactor;
		  const canvasHeight = originalHeight * resizingFactor;

		  canvas.width = canvasWidth;
		  canvas.height = canvasHeight;

		  context.drawImage(
			imgToResize,
			0,
			0,
			originalWidth * resizingFactor,
			originalHeight * resizingFactor
		  );

		  return canvas.toDataURL();
		}

		function stepped_scale(img, width, step) {
		  var canvas = document.createElement('canvas'),
			ctx = canvas.getContext("2d"),
			oc = document.createElement('canvas'),
			octx = oc.getContext('2d');

		  canvas.width = width; // destination canvas size
		  canvas.height = canvas.width * img.height / img.width;

					console.log('img width: '+img.width);

		  if (img.width * step > width) { // For performance avoid unnecessary drawing
			var mul = 1 / step;
			var cur = {
			  width: Math.floor(img.width * step),
			  height: Math.floor(img.height * step)
			}
			  
			oc.width = cur.width;
			oc.height = cur.height;

			octx.drawImage(img, 0, 0, cur.width, cur.height);

			while (cur.width * step > width) {
			  cur = {
				width: Math.floor(cur.width * step),
				height: Math.floor(cur.height * step)
			  };
			  octx.drawImage(oc, 0, 0, cur.width * mul, cur.height * mul, 0, 0, cur.width, cur.height);
			}

			ctx.drawImage(oc, 0, 0, cur.width, cur.height, 0, 0, canvas.width, canvas.height);
		  } else {
			ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
		  }

		  return canvas.toDataURL('image/png');
		}

		$(document).ready(function() {

			window.deferreds_global = [];
			window.deferreds_global2 = [];

			$("#sortable_ppt").sortable({
				//handle:		 '.handle'
				//revert	: true,
				connectWith  : ".sortable_ppt",
				placeholder:	'sort-placeholder',
				forcePlaceholderSize: true,
				start: function( e, ui ) {
					var prevPagesOrder = $(this).sortable('toArray', {attribute: 'id'});
					ui.item.data( 'prevPagesOrder', prevPagesOrder );
					
					var initSD = window.sD;
					ui.item.data( 'initSD', initSD );
					
					ui.item.data( 'start-pos', ui.item.index()+1 );
				},
				change: function( e, ui ) {
					  var seq,
					  startPos = ui.item.data('start-pos'),
					  $index,
					  correction;
					// if startPos < placeholder pos, we go from top to bottom
					  // else startPos > placeholder pos, we go from bottom to top and we need to correct the index with +1
					  //					  
					  correction = startPos <= ui.placeholder.index() ? 0 : 1;
					  ui.item.parent().find( 'li.ui-state-default').each( function( idx, el )
					  {
						var $this = $( el ), $index = $this.index();
						// correction 0 means moving top to bottom, correction 1 means bottom to top
						console.log('$index+1: '+($index+1)+' | startPos: '+ startPos + ' | correction: '+correction);
						  
						if ( ( $index+1 >= startPos && correction === 0) || ($index+1 <= startPos && correction === 1 ) )
						{
						  $index = $index + correction;
						  //$this.find( '.ordinal-position').text( $index );
						  $this.find( 'div.sm_sl_slide_row1').attr("class", 'sm_sl_slide_row1 _'+($index-1));
						}
					  });
					  // handle dragged item separatelly
										
					  seq = ui.item.parent().find( 'li.sort-placeholder').index() + correction;
					  //ui.item.find( '.ordinal-position' ).text( seq );
					  ui.item.find( 'div.sm_sl_slide_row1').attr("class", 'sm_sl_slide_row1 _'+(seq-1));
					  console.log('seq-1: '+(seq-1)+ 'startPos-1: '+(startPos-1) );
				  },
				  update: function( e, ui ) {
					var prevPagesOrder = ui.item.data('prevPagesOrder');
					var currentOrder = $(this).sortable('toArray');
					console.log('currentOrder: '+currentOrder);
					var initSD = ui.item.data('initSD');
					var neWsD = [];
					prevPagesOrder.forEach(function(item) {
					//for(let i=0; i<currentOrder.length; i++) {
					  	if(typeof("item") != "undefined" && item.length > 0) {
							//console.log(i + ' -> ' + currentOrder[i]);
							console.log(item + ' -> ' + currentOrder[item]);
							neWsD[item] = initSD[currentOrder[item]];
							//neWsD[i] = window.sD[currentOrder[i]];
						}
					}
				  	);
					prevPagesOrder = currentOrder;
					console.log('SD : '+JSON.stringify(initSD));
					window.sD = neWsD;
					console.log('SD : '+JSON.stringify(window.sD));
				  }
				} );

			if (window.sC < 1) {
				$('#uploader, #uploader_cur').prop('disabled', true);
				$('#del_slide').prop('disabled', true);
				$('#edit_slide').prop('disabled', true);
				$('#copy_slide').prop('disabled', true);
				$('#new_tmpl').prop('disabled', true);
				$('#save_tmpl').prop('disabled', true);
			}

			$('#add_slide').prop('disabled', false);

			$('#load_tmpl').prop('disabled', false);

			$(document).on('click', '#edit_slide', function() {
				window.pres = 0;
				window.cS = window.cP;
				window.cSD = JSON.parse(JSON.stringify(window.sD[window.cS]));
				window.slideSettingsDraw();
			});

			$(document).on('click', '#del_slide', function() {

				$.confirm({
					title: 'Подтвеждение действия',
					content: 'Удалить слайд?',
					type: 'red',
					typeAnimated: true,
					buttons: {
						yes: {
							text: 'Удалить',
							btnClass: 'btn-red',
							action: function(){

								window.cS = window.cP;

								console.log('sm_sl_sr_delete: window.cS: '+window.cS);
								console.log('sm_sl_sr_delete: window.sD[window.cS].fP: '+window.sD[window.cS].fP);
								console.log('sm_sl_sr_delete: window.fC: '+window.fC);
								console.log('sm_sl_sr_delete: window.cP: '+window.cP);

								if (window.sD[window.cS].fP) {
									window.fC--;
								}
								window.sD.splice(window.cS, 1);
								window.slidesListDraw();
								window.sC--;
								if (window.sC < 1) {
									$('#uploader, #uploader_cur').prop('disabled', true);
									$('#del_slide').prop('disabled', true);
									$('#edit_slide').prop('disabled', true);
									$('#copy_slide').prop('disabled', true);
									$('#new_tmpl').prop('disabled', true);
								}
								if (window.cS < window.cP) {
									window.cP--;
								} else if (window.cS == window.cP) {
									if ((window.cP > 0) || (window.sC == 0)) {
										window.cP--;
									}
									window.currentSlideDraw();
								}
							}
						},
						close: function () {
						}
					}
				});
				
			});

			$(document).on('click', '#copy_slide', function() {

				let tmp_cSD = JSON.parse(JSON.stringify(window.sD[window.cP]));
				window.cP++;
				window.cS++;
				window.sC++;
				window.sD.splice(window.cP, 0, tmp_cSD);
				//window.cSD = JSON.parse(JSON.stringify(window.sD[window.cP]));
				window.cSD = window.sD[window.cP];
				window.cSD.sT += ' [Копия]';
				window.slidesListDraw();
				window.currentSlideDraw();
				window.pres = 0;
				//window.slideSettingsDraw();
			});

			$(document).on('click', '#load_tmpl', function() {

				$.ajax({
					url: 'http://betp.website/wp-admin/admin-ajax.php',
					type: 'POST',
					data: {
						action: 'ajax',
						task: 'getUserReqs',
						dbase: window.DBase
					},
					beforeSend: function() {},
					success: function(response) {
						response = JSON.parse(response);
						let ur_select_content = '';
						if (response.hasOwnProperty('error')) {
							$().message_err('Ошибка при получении данных из БД: ' + response.error);
							return false;
						} else if (response.result > 0) {
							console.log('window.gTmpl: '+window.gTmpl);
							console.log('response.data.length: '+response.data.length);
							console.log('response.data: '+JSON.stringify(response.data));

							if ((typeof(window.gTmpl) != "undefined") && (window.gTmpl !== null)) {
								for (let i = 0; i < response.data.length; i++) {
									if (response.data[i].id === Number(window.gTmpl)) {
										ur_select_content += '<option selected="selected" id="' + response.data[i].id + '" value="' + response.data[i].id + '">' + response.data[i].name + '</option>';
									} else {
										ur_select_content += '<option id="' + response.data[i].id + '" value="' + response.data[i].id + '">' + response.data[i].name + '</option>';
									}
								}
							} else {
								for (let i = 0; i < response.data.length; i++) {
									ur_select_content += '<option id="' + response.data[i].id + '" value="' + response.data[i].id + '">' + response.data[i].name + '</option>';
								}
							}
						} else {
							//$().message_err('null result!');
						}
						
						$('#save_tmpl').prop('disabled', false);
						
						$('.tmpl_choose')[0].innerHTML = '<div class="fc_label"><p>Выберите шаблон</p></div><select class="ur_select" name="ur_select" id="ur_select">' + ur_select_content + '</select><input type="button" class="ur_button_load" name="ur_button_load" id="ur_button_load" value="Загрузить"/><input type="button" class="ur_button_del" name="ur_button_del" id="ur_button_del" value="Удалить"/><input type="button" class="ur_button_choose_exit" name="ur_button_choose_exit" id="ur_button_choose_exit" value="Отменить"/>';
						/*$('.tmpl_choose')[0].innerHTML = '<div class="fc_label"><p>Выберите шаблон</p></div><div id="tmpl_input"><select class="ur_select" name="ur_select" id="ur_select">' + ur_select_content + '</select></div><img id="tmpl_input_edit" src="/wp-content/themes/twentysixteen/img/pencil.png"><img id="tmpl_input_add" src="/wp-content/themes/twentysixteen/img/plus.png"><input type="button" class="ur_button_ok" name="ur_button_ok" id="ur_button_ok" value="Загрузить"/><input type="button" class="ur_button_del" name="ur_button_del" id="ur_button_del" value="Удалить"/><input type="button" class="ur_button_exit" name="ur_button_exit" id="ur_button_exit" value="Отменить"/>';*/
						$('.tmpl_choose_fade').fadeIn(10);
					}
				});
				
			});

			$(document).on('click', '#tmpl_input_edit', function() {
				return false;
			});

			$(document).on('click', '#tmpl_input_add', function() {
				return false;
			});

			$(document).on('click', '#new_tmpl', function() {
				$('.tmpl_add')[0].innerHTML = '<div class="fc_label"><p>Новый шаблон</p></div><input class="ur_add" name="ur_add" id="ur_add"><input type="button" class="ur_button_add" name="ur_button_add" id="ur_button_add" value="Сохранить"/><input type="button" class="ur_button_add_exit" name="ur_button_add_exit" id="ur_button_add_exit" value="Отменить"/>';
				$('.tmpl_add_fade').fadeIn(10);
			});

			$(document).on('click', '#save_tmpl', function() {
				if(typeof (window.curReqID) === "undefined" ) {
					$().message_err('Error: Template not loaded!');
				}
				
				$.ajax({
					url: 'http://betp.website/wp-admin/admin-ajax.php',
					type: 'POST',
					data: {
						action: 'ajax',
						task: 'saveUserReqs',
						req_id: window.curReqID,
						req_data: window.sD,
						dbase: window.DBase
					},
					beforeSend: function() {},
					success: function(response) {
						response = JSON.parse(response);
						if (response.hasOwnProperty('error')) {
							$().message_err('Ошибка: ' + response.error);
						} else if (response.result > 0) {
							$().message(response.message);
						}
					}
				});
			});
			
			$(document).on('click', '#uploader, #uploader_cur', async function() {
				
				var pres = new PptxGenJS();
				console.log('make pres '+window.sC);

				var selector = $(this).attr('id');
				console.log('selector: '+selector);

				let start_sld = 0;
				let finish_sld = window.sC;
					if(selector == 'uploader_cur') {
						start_sld = window.cP;
						finish_sld = window.cP+1;
					}
				if (window.sC > 0) {
				var deferreds = [];

					for (let i = start_sld; i < finish_sld; i++) {
						console.log('i: '+i);

						console.log('cP: '+window.cP);

						window.cP = i;
						console.log('cP new: '+window.cP);

						window.cSD = JSON.parse(JSON.stringify(window.sD[window.cP]));

							//if (window.cP > 0) {
								window.currentSlideDraw();
								await new Promise(r => setTimeout(r, 3000));
							//}
						
						var gtypes1 = [0,1,2];
						var gtypes2 = [3,4];
						var gtypes3 = [5,6];
						var gtypes4 = [7,8,9,10,11,12,14];
						var gtypes5 = [13];
						var gtypes6 = [15];

						var res_coef = 2;
						
						var coef = 0.01041666666667/1.94;
						
						var ww=window.screen.width;
							/*if(ww < 1920) {
								res_coef = 1920/ww;
							}*/
									
							console.log('add slide '+i);

							let h1 = document.getElementById('gr_canvas_0').height*coef+0.2;
							let absh = '14.5%';
							let txth = 86; // in percents
						
							console.log('gtype: '+parseInt(window.sD[window.cP].gT));

							var deferred = $.Deferred();
							deferreds.push(deferred.promise());
						
								if (gtypes1.includes(parseInt(window.sD[window.cP].gT))) {
									let mrg = 0.1;
									let h1_ = h1/res_coef;
									var legends_class = document.getElementsByClassName("sp_sb_cs_gb_gc3_series_list");
									var w1 = document.getElementById('gr_canvas_0').width*coef/res_coef;
									var w2 = $("div.sp_sb_cs_gb_graph_col_1").css("width").replace(/[^\d\.]/g,'')*coef;
									var w3 = document.getElementById('gr_canvas_1').width*coef/res_coef;
									var w4 = $("div.sp_sb_cs_gb_gc3_series_list").css("width").replace(/[^\d\.]/g,'')*coef;
									var w5 = document.getElementById('gr_canvas_2').width*coef/res_coef;
									sl = AddChart2Slide(pres,window.chart_0,mrg, w1, absh, h1_);
									AddChart2Slide(pres,window.chart_1, w1+w2+mrg, w3, absh, h1_, sl);
									//if(ww <= 1440) {									
									//	AddChart2Slide(pres,window.chart_2, w1+w2+w3+w4+mrg+0.25, w5, absh, h1_, sl);
									//} else {
										AddChart2Slide(pres,window.chart_2, w1+w2+w3+w4+mrg, w5, absh, h1_, sl);
									//}
									legends_class[0].setAttribute('id','leg1');
									//if(ww <= 1440) {
									//	AddImg2Slide('#leg1', sl, deferred, w1+w2+w3+mrg-0.04, w4+0.32, absh, h1_, 0, res_coef);
									//} else {
										AddImg2Slide('#leg1', sl, deferred, w1+w2+w3+mrg-0.04, w4+0.07, absh, h1_, 0, res_coef);
									//}
									var legtxt = document.getElementsByClassName("sp_sb_cs_sb_data_block");
									legtxt[0].setAttribute('id','leg2');
									let leg2h = $("div.sp_sb_cs_sb_data_block").css("height").replace(/[^\d\.]/g,'')*coef;
									let leg2w = $("div.sp_sb_cs_sb_data_block").css("width").replace(/[^\d\.]/g,'')*coef;
										/*if(ww <= 1440) { 
											leg2h = leg2h*res_coef;
											leg2w = leg2w*res_coef;
										}*/
									//console.log('leg2h: '+leg2h);
									//console.log('leg2w: '+leg2w);

									AddImg2Slide('#leg2', sl, deferred, mrg, leg2w, '82%', leg2h, '#ffffff', res_coef);

									var fsize = 8;
										if($("div.sp_sb_cs_sb_data_block").find("div[class^='sp_sb_cs_db_data_col']").length > 8) {
											fsize=6;
										}

									$("div.sp_sb_cs_sb_data_block").find("div[class^='sp_sb_cs_db_data_col']").each(function (i,el) {
										//console.log(i + ': '+$(el).html() + ' | '+$(el).css("left")+ ' | '+$(el).css("width"));
										
										var legends = $(el).find("p.paragraph_1").html().replace('%', '\%').split("<br>");
										//console.log(legends[0] +' | '+legends[1]);
										
										//console.log(i + ': '+legends[0] + ' | '+$(legends[1]).text() + ' | '+$(legends[1]).attr("color"));
										var x1;
										if(i==0) {
											x1 = mrg;
										} else {
											x1 = $(el).css("left").replace(/[^\d\.]/g,'')*coef+mrg;
										}

										let w1 = $(el).css("width").replace(/[^\d\.]/g,'')*coef;
										//console.log('x1: '+x1+' w1: '+w1);

										var colorr;
										var text;
										if(i!=4) {
											colorr = $(legends[1]).attr("color");
											text = $(legends[1]).text();
										} else {
											colorr = '000000';
											text = '+/- % SPLY';
										}
										
										if(ww <= 1440) { 
											//x1 = x1*res_coef;
											//console.log('New x1: '+x1);
										}
										sl.addText(legends[0], { w: w1, x: x1, y: txth+'%', align: 'center', color: "000000", fontFace: "Arial", fontSize: fsize, autoFit:'true', margin: 0 });
										sl.addText(text, { w: w1, x: x1, y: (txth+2)+'%', align: 'center', fontFace: "Arial", fontSize: fsize, color: colorr, autoFit:'true', margin: 0 });
									});
									
									var flth = (txth+7);
								} else if(gtypes2.includes(parseInt(window.sD[window.cP].gT))) {
									let incr_coef = 1.2;
									let h1_ = h1/res_coef*incr_coef;
									var legends_class = document.getElementsByClassName("sp_sb_cs_gb_g3c0_series_list");
									var w1 = $("div.sp_sb_cs_gb_g3c0_series_list").css("width").replace(/[^\d\.]/g,'')*coef*incr_coef;
									var w2 = document.getElementById('gr_canvas_0').width*coef/res_coef*incr_coef;
									var w3 = document.getElementById('gr_canvas_1').width*coef/res_coef*incr_coef;
									var w4 = document.getElementById('gr_canvas_2').width*coef/res_coef*incr_coef;
									sl = AddChart2Slide(pres,window.chart_0, w1*1.5, w2, absh, h1_);
									AddChart2Slide(pres,window.chart_1, w1*1.5+w2*2, w3, absh, h1_, sl);
									AddChart2Slide(pres,window.chart_2, w1*1.5+w2*2+w3*2, w4, absh, h1_, sl);
									legends_class[0].setAttribute('id','leg1');
									AddImg2Slide('#leg1', sl, deferred, 0, w1, absh, h1_, 0, res_coef);
									await new Promise(r => setTimeout(r, 2000));
									var flth = (txth+7)
								} else if (gtypes3.includes(parseInt(window.sD[window.cP].gT))) {
									let h1_ = h1/res_coef;
									let lft_shift = 0.2;
									var legends_class = document.getElementsByClassName("sp_sb_cs_gb_gc3_series_list");
									var w1 = document.getElementById('gr_canvas_0').width*coef/res_coef;
									var w2 = $("div.sp_sb_cs_gb_gc3_series_list").css("width").replace(/[^\d\.]/g,'')*coef;
									var w3 = document.getElementById('gr_canvas_1').width*coef/res_coef;
									//await new Promise(r => setTimeout(r, 1000));
									sl = AddChart2Slide(pres,window.chart_0, lft_shift, w1, absh, h1_);
									AddChart2Slide(pres,window.chart_1, w1+w2+lft_shift, w3, absh, h1_, sl);
									legends_class[0].setAttribute('id','leg1');
									AddImg2Slide('#leg1', sl, deferred, w1+lft_shift, w2, absh, h1_, 0, res_coef);
									await new Promise(r => setTimeout(r, 2000));
									var flth = (txth+7);
								} else if (gtypes4.includes(parseInt(window.sD[window.cP].gT))) {
									let mrg = 0;
									let left_mrg = 0.12;
									let h1_ = h1/res_coef;
									var legends_class = document.getElementsByClassName("sp_sb_cs_gb_g5c1_series_list");
									var w1 = document.getElementById('gr_canvas_0').width*coef/res_coef;
									//console.log('6 type: '+w1);
									var w2 = $("div.sp_sb_cs_gb_g5c1_series_list").css("width").replace(/[^\d\.]/g,'')*coef;
									var w3 = document.getElementById('gr_canvas_1').width*coef/res_coef;

									sl = AddChart2Slide(pres,window.chart_0, left_mrg, w1, absh, h1_);
									AddChart2Slide(pres,window.chart_1, left_mrg+w1+w2, w3, absh, h1_, sl);
									
									legends_class[0].setAttribute('id','leg1');
									AddImg2Slide('#leg1', sl, deferred, left_mrg+w1-0.012, w2+0.12, absh, h1_, 0, res_coef);

									var legtxt = document.getElementsByClassName("sp_sb_cs_sb_data_block");
									legtxt[0].setAttribute('id','leg2');
									let leg2h = $("div.sp_sb_cs_sb_data_block").css("height").replace(/[^\d\.]/g,'')*coef;
									let leg2w = $("div.sp_sb_cs_sb_data_block").css("width").replace(/[^\d\.]/g,'')*coef;

									//console.log('leg2h: '+leg2h);
									//console.log('leg2w: '+leg2w);

									AddImg2Slide('#leg2', sl, deferred, left_mrg-0.03, leg2w, '75.5%', leg2h, '#ffffff', 2);

									await new Promise(r => setTimeout(r, 3000));

									var fsize = 8;
										if($("div.sp_sb_cs_sb_data_block").find("div[class^='sp_sb_cs_db_data_col']").length > 6) {
											fsize=6;
											await new Promise(r => setTimeout(r, 2000));
										}

									$("div.sp_sb_cs_sb_data_block").find("div[class^='sp_sb_cs_db_data_col']").each(function (i,el) {
										//console.log(i + ': '+$(el).html() + ' | '+$(el).css("left")+ ' | '+$(el).css("width"));
										var legends = $(el).find("p.paragraph_1").html().replace('%', '\%').split("<br>");
										//console.log(i + ': '+legends[0] + ' | '+$(legends[1]).text() + ' | '+$(legends[1]).attr("color"));
										//console.log(i + ': '+legends[2]);
										
										/*var x1;
										if(i==0) {
											x1 = left_mrg;
										} else {
											x1 = left_mrg+$(el).css("left").replace(/[^\d\.]/g,'')*coef;
										}*/

										let wt1 = $(el).css("width").replace(/[^\d\.]/g,'')*coef;
										//console.log('x1: '+x1+' wt1: '+wt1);

										/*var colorr;
										var text1;
										var text2
										if(i!=4) {
											colorr = $(legends[1]).attr("color");
											text = $(legends[1]).text();
											text2 = legends[2];
										} else {
											colorr = '000000';
											text1 = '+/- % PY';
											text2 = 'Horiz.% out. GT';
										}*/

										//sl.addText(legends[0], { w: wt1, x: x1, y: txth+'%', align: 'center', color: "000000", fontFace: "Arial", fontSize: fsize, autoFit:'true', margin: 0 });
										//sl.addText(text1, { w: wt1, x: x1, y: (txth+2)+'%', align: 'center', fontFace: "Arial", fontSize: fsize, color: colorr, autoFit:'true', margin: 0 });
										//sl.addText(text2, { w: wt1, x: x1, y: (txth+4)+'%', align: 'center', fontFace: "Arial", fontSize: fsize, autoFit:'true', margin: 0 });
									});
									var flth = (txth+5);
								} else if (gtypes5.includes(parseInt(window.sD[window.cP].gT))) {
									let mrg = 0.1;
									let h1_ = h1/res_coef;
									var legends_class = document.getElementsByClassName("sp_sb_cs_gb_gc3_series_list");
									var w1 = document.getElementById('gr_canvas_0').width*coef/res_coef;
									var w2 = $("div.sp_sb_cs_gb_graph_col_1").css("width").replace(/[^\d\.]/g,'')*coef;
									var w3 = document.getElementById('gr_canvas_1').width*coef/res_coef;
									var w4 = $("div.sp_sb_cs_gb_gc3_series_list").css("width").replace(/[^\d\.]/g,'')*coef;
									var w5 = document.getElementById('gr_canvas_2').width*coef/res_coef;
									sl = AddChart2Slide(pres,window.chart_0, mrg, w1, absh, h1_);
									AddChart2Slide(pres,window.chart_1, w1+w2+mrg, w3, absh, h1_, sl);
									AddChart2Slide(pres,window.chart_2, w1+w2+w3+w4+mrg, w5, absh, h1_, sl);
									legends_class[0].setAttribute('id','leg1');
									AddImg2Slide('#leg1', sl, deferred, w1+w2+w3+mrg-0.04, w4+0.07, absh, h1_, 0, res_coef);
									
									var flth = (txth+7);
								} else if (gtypes6.includes(parseInt(window.sD[window.cP].gT))) {
									//$().message_err('New graph');
									let mrg = 0.2;
									let mrgh = 0.7;
									let h1_ = h1/res_coef;
									
									var w1 = $("div.sp_sb_cs_gb_graph15_col_0").css("width").replace(/[^\d\.]/g,'')*coef; 
									var w2 = document.getElementById('gr_canvas_0').width*coef/res_coef;

									var h2 = $("div.sp_sb_cs_sb_user_text").css("height").replace(/[^\d\.]/g,'')*coef;
																			
									sl = AddChart2Slide(pres, window.chart_0, w1+mrg, w2, mrgh+h2, h1_);									
									AddChart2Slide(pres, window.chart_1, w1+mrg, w2, mrgh+h2+h1_, h1_, sl);
									AddChart2Slide(pres, window.chart_2, w1+mrg, w2, mrgh+h2+h1_*2, h1_, sl);
									
									var w3 = $("div.sp_sb_cs_sb_user_text").css("width").replace(/[^\d\.]/g,'')*coef; 
									var h3 = $("div.sp_sb_cs_sb_user_text").css("height").replace(/[^\d\.]/g,'')*coef;
									var legends_class1 = document.getElementsByClassName("sp_sb_cs_sb_user_text");
									legends_class1[0].setAttribute('id','leg1');									
									AddImg2Slide('#leg1', sl, deferred, 0, w3, absh, h3, 0, res_coef);

									var w4 = $("div.sp_sb_cs_gb_graph15_col_0").css("width").replace(/[^\d\.]/g,'')*coef; 
									var h4 = $("div.sp_sb_cs_gb_graph15_col_0").css("height").replace(/[^\d\.]/g,'')*coef;
									var legends_class2 = document.getElementsByClassName("sp_sb_cs_gb_graph15_col_0");
									legends_class2[0].setAttribute('id','leg2');																		
									AddImg2Slide('#leg2', sl, deferred, mrg-0.05, w4, mrgh+h3, h4, 0, res_coef);
									
									var w5 = $("div.sp_sb_cs_sb_data_block").css("width").replace(/[^\d\.]/g,'')*coef; 
									var h5 = $("div.sp_sb_cs_sb_data_block").css("height").replace(/[^\d\.]/g,'')*coef;									
									var legends_class3 = document.getElementsByClassName("sp_sb_cs_sb_data_block");
									legends_class3[0].setAttribute('id','leg3');
									AddImg2Slide('#leg3', sl, deferred, 0, w5, mrgh+h3+h4, h5, 0, res_coef);

									var flth = (txth+7);
									
								} else {
									$().message_err('Error: Unknown page type.');
									return false;
								}

							var text = $("div.sp_sb_cs_sb_slide_title").text();
							sl.addText(text, { w: '95%', x:0, y: '8%', align: 'center', fontFace: "Arial", fontSize: 12, color: '000000' });

							let fltleg = $("div.sp_sb_cs_sb_down").html().replace(/<br>/g,' ');
							//console.log('fltleg: '+fltleg);
							sl.addText(fltleg, { w: '90%', x: '5%', y: flth+'%', align: 'center', fontFace: "Arial", fontSize: 7, color: '000000', autoFit:'true', margin: 0 });

					}
				}
				
				$.when.apply($, deferreds, pres).then(function () { // executes after adding all images
					pres.writeFile({ fileName: 'test.pptx' });
				});
				$('#uploader').prop('disabled', false);
				return false;
			});

			$(document).on('change', '.id_file', function(evt) {
				window.changeFile(evt);
			});

			$(document).on('change', '.ndp_input_min', function(evt) {
				checkValueMinMax(evt);
				$('.ndp_input_max').attr("min", evt.target.value);
				$('.ndp_input_min').attr("value", evt.target.value);
			});

			$(document).on('change', '.ndp_input_max', function(evt) {
				checkValueMinMax(evt);
				$('.ndp_input_min').attr("max", evt.target.value);
				$('.ndp_input_max').attr("value", evt.target.value);
			});

			function checkValueMinMax(obj) {
				let value= +obj.target.value||0;
				let min = +obj.target.min;
				let max = +obj.target.max;
				obj.target.value = Math.min(+obj.target.max, Math.max(+obj.target.min, +obj.target.value));
			}

			$(document).on('change', '.id_base', function(evt) {
				window.DBase = $('.id_base').val();
				window.setMainSelect();
			});

			$(document).on('change', '.id_main', function(evt) {
				window.Main = $('.id_main').val();
			});

			$(document).on('change', '.sos_select', function(evt) {
				window.cSD.sos = $('.sos_select').val();
			});

			$(document).on('change', '.oos_select', function(evt) {
				window.cSD.oos = $('.oos_select').val();
			});

			$(document).on('click', '.nd_button_ok', function() {
				if (window.cSD.cD == window.cSD.dC) {
					window.cSD.dC++;
					window.cSD.dD[window.cSD.cD] = [];
					window.cSD.dD[window.cSD.cD].clr = 'rgba(127, 127, 127, 1)';
				}
				window.cSD.dD[window.cSD.cD].sS = $('.ndp_input_min').val();
				window.cSD.dD[window.cSD.cD].eS = $('.ndp_input_max').val();
				window.diapasonsListDraw();
				$('.new_diapason_settings_fade').fadeOut(10);
			});

			$(document).on('click', '.nd_button_exit', function() {
				$('.new_diapason_settings_fade').fadeOut(10);
			});

			$(document).on('click', '.ds_db_dl_dr_edit', function() {
				window.cSD.cD = $(this).attr('class').replace('ds_db_dl_dr_edit _', '');
				window.diapasonChooseDraw();
			});

			$(document).on('click', '.ds_db_dl_dr_delete', function() {
				window.cSD.cD = $(this).attr('class').replace('ds_db_dl_dr_delete _', '');
				window.cSD.dD.splice(window.cSD.cD, 1);
				window.diapasonsListDraw();
				window.cSD.dC--;
			});

			$(document).on('click', '.id_button', function() {
				$('.sm_file_value')[0].innerHTML = '<p>Выбранный файл: /В разработке/</p>';
				$('.input_data_fade').fadeOut(10);
			});

			$(document).on('click', '.fs_sb_add_all', function() {
				let searched = fs_sb_search_string.value;
				let fs_sb_options_list_content = '';
				let fs_sb_selected_options_content = '';
				let selectedCount = -1;
				if (searched == '') {
					window.cSD.fD[window.cSD.cF].vI = [];
					window.cSD.fD[window.cSD.cF].vV = [];
					for (let i = 0; i < window.getColValuesresponse.data.length; i++) {
						window.cSD.fD[window.cSD.cF].vI.push(window.getColValuesresponse.data[i].id);
						window.cSD.fD[window.cSD.cF].vV['i' + window.getColValuesresponse.data[i].id] = window.getColValuesresponse.data[i].value;
						fs_sb_options_list_content += '<div class="fs_sb_ol_option_block _' + window.getColValuesresponse.data[i].id + '" style="top: calc(5px + ' + (5 * i) + '%);"><input type="checkbox" onchange="window.fs_sb_ol_ob_checkbox_change(' + window.getColValuesresponse.data[i].id + ", '" + window.getColValuesresponse.data[i].value + "'" + ');" class="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" value="' + window.getColValuesresponse.data[i].id + '" checked><label class="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '">' + window.getColValuesresponse.data[i].value + '</label></div>';
						fs_sb_selected_options_content += '<div class="fs_sb_so_option_block _' + window.getColValuesresponse.data[i].id + '" style="top: calc(5px + ' + (5 * i) + '%);"><input type="checkbox" onchange="window.fs_sb_ol_ob_checkbox_change(' + window.getColValuesresponse.data[i].id + ", '" + window.getColValuesresponse.data[i].value + "'" + ');" class="fs_sb_so_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_so_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_so_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" value="' + window.getColValuesresponse.data[i].id + '" checked><label class="fs_sb_so_ob_label _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_so_ob_label _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_so_ob_label _' + window.getColValuesresponse.data[i].id + '">' + window.getColValuesresponse.data[i].value + '</label></div>';
					}
				} else {
					let searchedCount = -1;
					if ((typeof(window.cSD.fD[window.cSD.cF]) != "undefined") && (window.cSD.fD[window.cSD.cF] !== null)) {
						for (let i = 0; i < window.getColValuesresponse.data.length; i++) {
							let searchedPos = window.getColValuesresponse.data[i].value.toUpperCase().indexOf(searched.toUpperCase());
							if (searchedPos != '-1') {
								searchedCount++;
								fs_sb_options_list_content += '<div class="fs_sb_ol_option_block _' + window.getColValuesresponse.data[i].id + '" style="top: calc(5px + ' + (5 * searchedCount) + '%)"><input type="checkbox" onchange="window.fs_sb_ol_ob_checkbox_change(' + window.getColValuesresponse.data[i].id + ", '" + window.getColValuesresponse.data[i].value + "'" + ');" class="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" value="' + window.getColValuesresponse.data[i].id + '" checked><label class="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '">' + window.getColValuesresponse.data[i].value.substring(0, searchedPos) + '<font color="#b6e026">' + window.getColValuesresponse.data[i].value.substr(searchedPos, searched.length) + '</font>' + window.getColValuesresponse.data[i].value.slice(searchedPos + searched.length) + '</label></div>';
								if (window.cSD.fD[window.cSD.cF].vI.indexOf(window.getColValuesresponse.data[i].id) > -1) {
								} else {
									window.cSD.fD[window.cSD.cF].vI.push(window.getColValuesresponse.data[i].id);
								}
							}
							if (window.cSD.fD[window.cSD.cF].vI.indexOf(window.getColValuesresponse.data[i].id) > -1) {
								selectedCount++;
								fs_sb_selected_options_content += '<div class="fs_sb_so_option_block _' + window.getColValuesresponse.data[i].id + '" style="top: calc(5px + ' + (5 * selectedCount) + '%);"><input type="checkbox" onchange="window.fs_sb_ol_ob_checkbox_change(' + window.getColValuesresponse.data[i].id + ", '" + window.getColValuesresponse.data[i].value + "'" + ');" class="fs_sb_so_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_so_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_so_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" value="' + window.getColValuesresponse.data[i].id + '" checked><label class="fs_sb_so_ob_label _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_so_ob_label _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_so_ob_label _' + window.getColValuesresponse.data[i].id + '">' + window.getColValuesresponse.data[i].value + '</label></div>';
							}
						}
					} else {
						for (let i = 0; i < window.getColValuesresponse.data.length; i++) {
							let searchedPos = window.getColValuesresponse.data[i].value.toUpperCase().indexOf(searched.toUpperCase());
							if (searchedPos != '-1') {
								searchedCount++;
								window.cSD.fD[window.cSD.cF].vI.push(window.getColValuesresponse.data[i].id);
								window.cSD.fD[window.cSD.cF].vV['i' + window.getColValuesresponse.data[i].id] = window.getColValuesresponse.data[i].value;
								fs_sb_options_list_content += '<div class="fs_sb_ol_option_block _' + window.getColValuesresponse.data[i].id + '" style="top: calc(5px + ' + (5 * searchedCount) + '%);"><input type="checkbox" onchange="window.fs_sb_ol_ob_checkbox_change(' + window.getColValuesresponse.data[i].id + ", '" + window.getColValuesresponse.data[i].value + "'" + ');" class="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" value="' + window.getColValuesresponse.data[i].id + '" checked><label class="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '">' + window.getColValuesresponse.data[i].value.substring(0, searchedPos) + '<font color="#b6e026">' + window.getColValuesresponse.data[i].value.substr(searchedPos, searched.length) + '</font>' + window.getColValuesresponse.data[i].value.slice(searchedPos + searched.length) + '</label></div>';
								fs_sb_selected_options_content += '<div class="fs_sb_so_option_block _' + window.getColValuesresponse.data[i].id + '" style="top: calc(5px + ' + (5 * searchedCount) + '%);"><input type="checkbox" onchange="window.fs_sb_ol_ob_checkbox_change(' + window.getColValuesresponse.data[i].id + ", '" + window.getColValuesresponse.data[i].value + "'" + ');" class="fs_sb_so_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_so_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_so_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" value="' + window.getColValuesresponse.data[i].id + '" checked><label class="fs_sb_so_ob_label _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_so_ob_label _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_so_ob_label _' + window.getColValuesresponse.data[i].id + '">' + window.getColValuesresponse.data[i].value + '</label></div>';

							}
						}
					}
				}
				$('.fs_sb_options_list')[0].innerHTML = fs_sb_options_list_content;
				$('.fs_sb_selected_options')[0].innerHTML = fs_sb_selected_options_content;
				return false;
			});

			$(document).on('click', '.fs_sb_del_all', function() {
				let searched = fs_sb_search_string.value;
				let fs_sb_options_list_content = '';
				let fs_sb_selected_options_content = '';
				for (let i = 0; i < window.cSD.fD[window.cSD.cF].vI.length; i++) {
					delete window.cSD.fD[window.cSD.cF].vV['i' + window.cSD.fD[window.cSD.cF].vI[i]];
				}
				window.cSD.fD[window.cSD.cF].vI = [];
				if (searched == '') {
					for (let i = 0; i < window.getColValuesresponse.data.length; i++) {
						fs_sb_options_list_content += '<div class="fs_sb_ol_option_block _' + window.getColValuesresponse.data[i].id + '" style="top: calc(5px + ' + (5 * i) + '%);"><input type="checkbox" onchange="window.fs_sb_ol_ob_checkbox_change(' + window.getColValuesresponse.data[i].id + ", '" + window.getColValuesresponse.data[i].value + "'" + ');" class="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" value="' + window.getColValuesresponse.data[i].id + '"><label class="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '">' + window.getColValuesresponse.data[i].value + '</label></div>';
					}
				} else {
					let searchedCount = -1;
					for (let i = 0; i < window.getColValuesresponse.data.length; i++) {
						let searchedPos = window.getColValuesresponse.data[i].value.toUpperCase().indexOf(searched.toUpperCase());
						if (searchedPos != '-1') {
							searchedCount++;
							fs_sb_options_list_content += '<div class="fs_sb_ol_option_block _' + window.getColValuesresponse.data[i].id + '" style="top: calc(5px + ' + (5 * searchedCount) + '%);"><input type="checkbox" onchange="window.fs_sb_ol_ob_checkbox_change(' + window.getColValuesresponse.data[i].id + ", '" + window.getColValuesresponse.data[i].value + "'" + ');" class="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_checkbox _' + window.getColValuesresponse.data[i].id + '" value="' + window.getColValuesresponse.data[i].id + '"><label class="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" name="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '" id="fs_sb_ol_ob_label _' + window.getColValuesresponse.data[i].id + '">' + window.getColValuesresponse.data[i].value.substring(0, searchedPos) + '<font color="#b6e026">' + window.getColValuesresponse.data[i].value.substr(searchedPos, searched.length) + '</font>' + window.getColValuesresponse.data[i].value.slice(searchedPos + searched.length) + '</label></div>';
						}
					}
				}
				$('.fs_sb_options_list')[0].innerHTML = fs_sb_options_list_content;
				$('.fs_sb_selected_options')[0].innerHTML = fs_sb_selected_options_content;
				return false;
			});

			$(document).on('click', '.right_arrow', function() {
				$('.slider_menu_fade').fadeIn(10);
				$('.left_arrow').show();
				$('.right_arrow').hide();
			});

			$(document).on('click', '.sm_button_edit', function() {
				let id_base_select_content;
				if ((typeof(window.DBase) != "undefined") && (window.DBase !== null)) {
					if (Number(window.DBase) === 1) {
						id_base_select_content = '<option id="0" value="0">Россия</option><option selected="selected" id="1" value="1">Беларусь</option><option id="2" value="2">Казахстан</option>';
					} else if (Number(window.DBase) === 2) {
						id_base_select_content = '<option id="0" value="0">Россия</option><option id="1" value="1">Беларусь</option><option selected="selected" id="2" value="2">Казахстан</option>';
					} else {
						id_base_select_content = '<option selected="selected" id="0" value="0">Россия</option><option id="1" value="1">Беларусь</option><option id="2" value="2">Казахстан</option>';
					}
				} else {
					id_base_select_content = '<option selected="selected" id="0" value="0">Россия</option><option id="1" value="1">Беларусь</option><option id="2" value="2">Казахстан</option>';
				}
				$('.id_base')[0].innerHTML = id_base_select_content;
				window.setMainSelect();
				$('.input_data_fade').fadeIn(10);
			});

			$(document).on('click', '.sm_button_add,#add_slide', function() {
				window.cS = window.sC;
				window.cSD = JSON.parse(JSON.stringify(window.pSD));
				window.pres = 0;
				window.slideSettingsDraw();
			});

			$(document).on('click', '.sd_button_edit', function() {
				window.diapasonsSettingsDraw();
			});

			$(document).on('click', '#presets', function() {
				window.cSD = JSON.parse(JSON.stringify(window.pSD));
				window.tmpSD = JSON.parse(JSON.stringify(window.pSD));
				window.pres = 1;
				window.slideSettingsDraw();
			});

			$(document).on('click', '.ss_button_exit', function() {
				if (window.pres) {
					window.pSD = JSON.parse(JSON.stringify(window.tmpSD));
				}
				$('.slide_settings_fade').fadeOut(10);
			});

			$(document).on('click', '.ds_button_exit', function() {
				$('.diapasons_settings_fade').fadeOut(10);
			});

			$(document).on('click', '.tmpl_button_exit', function() {
				$('.tmpl_settings_fade').fadeOut(10);
			});

			$(document).on('click', '.sm_sl_sr_edit, .sm_sl_sr_img_edit', function() {
				window.cS = $(this).closest('div.sm_sl_slide_row1').attr('class').replace('sm_sl_slide_row1 _', '');
				window.cSD = JSON.parse(JSON.stringify(window.sD[window.cS]));
				window.cP = window.cS;
				window.pres = 0;
				window.slideSettingsDraw();
				$('.right_arrow').show();
				$('.left_arrow').hide();
				$('.slider_menu_fade').fadeOut(10);
				window.currentSlideDraw();
			});

			$(document).on('click', '.ss_fb_button', function() {
				window.cSD.cF = window.cSD.fC;
				window.filterChooseDraw();
			});

			$(document).on('click', '.ds_db_button', function() {
				window.cSD.cD = window.cSD.dC;
				window.diapasonChooseDraw();
			});

			$(document).on('focus', '.ss_st_select', function() {
				window.old_ss_st_select = $('.ss_st_select').val();
			}).on('change', '.ss_st_select', function() {
				window.cSD.page8Flag = 0;
				window.tmp_ss_st_select = $('.ss_st_select').val();
				$('.ss_sdd_date_input_1').prop('disabled', true);
				$('.ss_sd_select').prop('disabled', false);
				let options_names = ['Анализ по суммам в РУБ','Анализ по суммам в EUR','Анализ по количествам'];
				if ($('.ss_st_select').val() == 15) {
					options_names = ['Анализ по суммам в РУБ','Анализ по суммам в EUR'];
				}
				if ($('.ss_st_select').val() == 13) {
					window.cSD.sCD = $('.ss_so_select').val();
					window.cSD.page8Flag = 1;
					window.sorterSettingsDraw();
				} else if (($('.ss_st_select').val() > 6) && ($('.ss_st_select').val() < 15)) {
					window.sorterChooseDraw();
					$('.ss_sdd_date_input_1').prop('disabled', false);
					$('.ss_sd_select').prop('disabled', true);
				} else {
					window.old_ss_st_select = window.tmp_ss_st_select;
				}
				let ss_sp_select_content = '';
				if ((typeof(window.cSD.gT) != "undefined") && (window.cSD.gT !== null)) {
					for (let i = 0; i < options_names.length; i++) {
						if (i === Number(window.cSD.sP)) {
							ss_sp_select_content += '<option selected="selected" id="' + i + '" value="' + i + '">' + options_names[i] + '</option>';
						} else {
							ss_sp_select_content += '<option id="' + i + '" value="' + i + '">' + options_names[i] + '</option>';
						}
					}
				} else {
					for (let i = 0; i < options_names.length; i++) {
						ss_sp_select_content += '<option id="' + i + '" value="' + i + '">' + options_names[i] + '</option>';
					}
				}
				$('.ss_sp_select')[0].innerHTML = ss_sp_select_content;
			});

			$(document).on('change', '.ss_so_select', function() {
				if ($('.ss_so_select').val() === '0') {
					$('.sd_button_edit').prop('hidden', false);
				} else {
					$('.sd_button_edit').prop('hidden', true);
				}
			});

			$(document).on('change', '.ss_sp_select', function(evt) {
				window.cSD.sP = $('.ss_sp_select').val();
			});

			$(document).on('click', '.ss_fb_fl_fr_edit', function() {
				window.cSD.cF = $(this).attr('class').replace('ss_fb_fl_fr_edit _', '');
				window.filterChooseDraw();
			});

			$(document).on('click', '.fc_button_exit', function() {
				if (typeof window.cSD.fDold === 'undefined') {
					window.cSD.fD[window.cSD.cF] = window.cSD.fDold;
				} else {
					window.cSD.fD[window.cSD.cF] = JSON.parse(JSON.stringify(window.cSD.fDold));
				}
				$('.filter_choose_fade').fadeOut(10);
			});

			$(document).on('click', '.ur_button_load', function() {
				$.ajax({
					url: 'http://betp.website/wp-admin/admin-ajax.php',
					type: 'POST',
					data: {
						action: 'ajax',
						task: 'loadUserReqs',
						req_id: $('#ur_select').val(),
						dbase: window.DBase
					},
					beforeSend: function() {},
					success: function(response) {
						response = JSON.parse(response);
						if (response.hasOwnProperty('error')) {
							$().message_err('Ошибка: ' + response.error);
							return false;
						} else if (response.result > 0) {
						window.curReqID = $('#ur_select').val();
						//console.log('response.data.length: '+response.data.length);
						//console.log('response.data: '+JSON.stringify(response.data));

						window.cSD = JSON.parse(JSON.stringify(window.pSD));
							
						window.sD = [];
							
						window.cS = response.data.length;
						window.cP = 0;
						window.sC = window.cS;

							for(let i=0; i<window.cS; i++) {
								window.sD[i] = response.data[i];
								console.log('window.sD['+i+']: '+JSON.stringify(window.sD[i]));
								if (typeof window.sD[i].fD === "undefined") {
								 	window.sD[i].fD = [];
								}
								if (typeof window.sD[i].ssC === "undefined") {
									window.sD[i].ssC = [];
								} else {
									let tmp_ssC = window.sD[i].ssC.map((i) => Number(i));
									window.sD[i].ssC = tmp_ssC;
								}
								if (typeof window.sD[i].osC === "undefined") {
									window.sD[i].osC = [];
								} else {
									let tmp_osC = window.sD[i].osC.map((i) => Number(i));
									window.sD[i].osC = tmp_osC;
								}
							}

						window.cSD.sDt = window.sD[0].sDt;
						window.cSD.eDt = window.sD[0].eDt;
						window.cSD.sT = window.sD[0].sT;
						window.cSD.gT = window.sD[0].gT;
						window.cSD.sI = window.sD[0].sI;
						window.cSD.iI = window.sD[0].iI;
						window.cSD.sP = window.sD[0].sP;
						window.cSD.sU = window.sD[0].sU;
						window.cSD.sM = window.sD[0].sM;
						window.cSD.fC = window.sD[0].fC;
						window.cSD.fP = window.sD[0].fP;
						window.cSD.fN = window.sD[0].fN;
						window.cSD.fI = window.sD[0].fI;
						window.cSD.fD = window.sD[0].fD;
						window.cSD.ors = window.sD[0].ors;
						window.cSD.ssC = window.sD[0].ssC.map((i) => Number(i));
						window.cSD.osC = window.sD[0].osC.map((i) => Number(i));
							
						console.log('window.sD: '+JSON.stringify(window.sD));
						console.log('window.sD[window.cP]: '+JSON.stringify(window.sD[window.cP]));
						console.log('window.cSD: '+JSON.stringify(window.cSD));

						console.log('window.cS: '+window.cS);
						console.log('window.cP: '+window.cP);

						console.log('1 window.cSD.fD: '+window.cSD.fD);

						window.slidesListDraw();
						window.currentSlideDraw();

						console.log('2 window.cSD.fD: '+window.cSD.fD);
							
						$('.tmpl_choose_fade').fadeOut(10);
						}
					}
				});
			});

			$(document).on('click', '.ur_button_del', function() {
				$.ajax({
					url: 'http://betp.website/wp-admin/admin-ajax.php',
					type: 'POST',
					data: {
						action: 'ajax',
						task: 'delUserReqs',
						req_id: $('#ur_select').val(),
						dbase: window.DBase
					},
					beforeSend: function() {},
					success: function(response) {
						response = JSON.parse(response);
						if (response.hasOwnProperty('error')) {
							$().message_err('Ошибка: ' + response.error);
							return false;
						} else if (response.result > 0) {
							$().message(response.message);
							$('.tmpl_choose_fade').fadeOut(10);
						}
					}
				});
			});

			$(document).on('click', '.ur_button_add', function() {
			console.log('window.sD: '+JSON.stringify(window.sD));
			//console.log('Req name: '+$("input[id=ur_add]").val());
				$.ajax({
					url: 'http://betp.website/wp-admin/admin-ajax.php',
					type: 'POST',
					data: {
						action: 'ajax',
						task: 'addUserReqs',
						req_name: $("input[id=ur_add]").val(), 
						req_data: window.sD,
						dbase: window.DBase
					},
					beforeSend: function() {},
					success: function(response) {
						response = JSON.parse(response);
						if (response.hasOwnProperty('error')) {
							$().message_err('Ошибка: ' + response.error);
							return false;
						} else if (response.result > 0) {
							$().message(response.message);
							$('.tmpl_add_fade').fadeOut(10);
						}
					}
				});
			});

			$(document).on('click', '.ur_button_choose_exit', function() {
				$('.tmpl_choose_fade').fadeOut(10);
			});

			$(document).on('click', '.ur_button_add_exit', function() {
				$('.tmpl_add_fade').fadeOut(10);
			});

			$(document).on('click', '.fc_button_ok', function() {
				console.log('fc_button_ok: window.cSD.fD[window.cSD.cF]: '+JSON.stringify(window.cSD.fD[window.cSD.cF]));
				if (typeof window.cSD.fD[window.cSD.cF] === "undefined") {
					window.cSD.fD[window.cSD.cF] = {
						'cI': $('.fc_select').val(),
						'cV': $('.fc_select option:selected').text(),
						'vI': [],
						'vV': {}
					};
				} else if ($('.fc_select').val() != window.cSD.fD[window.cSD.cF].cI) {
				console.log('fc_button_ok: window.cSD.fD[window.cSD.cF].cI: '+window.cSD.fD[window.cSD.cF].cI)
					window.cSD.fD[window.cSD.cF] = {
						'cI': $('.fc_select').val(),
						'cV': $('.fc_select option:selected').text(),
						'vI': [],
						'vV': {}
					};
				}
				window.filterSettingsDraw();
			});

			$(document).on('click', '.fs_tb_button_exit', function() {
				if (typeof window.cSD.fDoldV.vI === 'undefined') {
					window.cSD.fD[window.cSD.cF].vI = window.cSD.fDoldV.vI;
				} else {
					window.cSD.fD[window.cSD.cF].vI = JSON.parse(JSON.stringify(window.cSD.fDoldV.vI));
				}
				if (typeof window.cSD.fDoldV.vV === 'undefined') {
					window.cSD.fD[window.cSD.cF].vV = window.cSD.fDoldV.vV;
				} else {
					window.cSD.fD[window.cSD.cF].vV = JSON.parse(JSON.stringify(window.cSD.fDoldV.vV));
				}
				$('.filter_settings_fade').fadeOut(10);
			});

			$(document).on('click', '.sos_button_exit_1', function() {
				$('.sorter_settings_fade').fadeOut(10);
				$('.sorter_choose_fade').fadeIn(10);
			});

			$(document).on('click', '.ors_button_exit', function() {
				$('.orderer_settings_fade').fadeOut(10);
			});

			$(document).on('click', '.sos_button_exit_2', function() {
				let options_names = ['Слайд тип "Страница1"','Слайд тип "Страница2"','Слайд тип "Страница3"','Слайд тип "Страница4 YTD"','Слайд тип "Страница5 MAT"','Слайд тип "Страница6 Круговая YTD"','Слайд тип "Страница7 Круговая MAT"','Слайд тип "Страница8 YTD 2 стб"','Слайд тип "Страница9 MAT 2 стб"','Слайд тип "Страница10 DIAP 2 стб"','Слайд тип "Страница11 YTD 3 стб"','Слайд тип "Страница12 MAT 3 стб"','Слайд тип "Страница13 DIAP 3 стб"','Слайд тип "Страница14"','Слайд тип "Страница15 4стб"','Слайд тип "Страница16 `горизонтальные тройки`"'];
				let ss_st_select_content = '';
				for (let i = 0; i < 16; i++) {
					if (i === Number(window.old_ss_st_select)) {
						ss_st_select_content += '<option selected="selected" id="' + i + '" value="' + i + '">' + options_names[i] + '</option>';
					} else {
						ss_st_select_content += '<option id="' + i + '" value="' + i + '">' + options_names[i] + '</option>';
					}
				}
				$('.ss_st_select')[0].innerHTML = ss_st_select_content;
				if ((($('.ss_st_select').val() > 6) && ($('.ss_st_select').val() < 13)) || ($('.ss_st_select').val() == 14)) {
					$('.ss_sdd_date_input_1').prop('disabled', false);
					$('.ss_sd_select').prop('disabled', true);
				} else {
					$('.ss_sdd_date_input_1').prop('disabled', true);
					$('.ss_sd_select').prop('disabled', false);
				}
				if ((typeof(window.cSD.sDt) != "undefined") && (window.cSD.sDt !== null)) {
					document.getElementById("ss_sdd_date_input_1").value = window.cSD.sDt;
				}
				if ((typeof(window.cSD.eDt) != "undefined") && (window.cSD.eDt !== null)) {
					document.getElementById("ss_sdd_date_input_2").value = window.cSD.eDt;
				}
				$('.sorter_settings_fade').fadeOut(10);
			});

			$(document).on('click', '.fs_tb_button_ok', function() {
				if (window.cSD.fD[window.cSD.cF].vI.length < 1) {
					$().message_err('Должно быть выбрано хотя бы одно значение!');
					return;
				}
				if (window.cSD.fC == window.cSD.cF) {
					window.cSD.fC++;
				}
				window.filtersListDraw();
				$('.filter_settings_fade').fadeOut(10);
				$('.filter_choose_fade').fadeOut(10);
			});

			$(document).on('click', '.sos_button_ok', function() {
				console.log('sos_button_ok window.cSD.ssC: '+JSON.stringify(window.cSD.ssC));
				let cases = document.getElementById('sortable2').getElementsByClassName('ss_ms_case_row');
				let count = 0;
				let tmp_arr = [];
				for (let item of cases) {
				console.log('sos_button_ok '+count+' item: '+item.value);
					tmp_arr[count] = item.value;
					count++;
				}
				window.cSD.ssC = tmp_arr;
				window.cSD.sos = $('.sos_select').val();
				console.log('sos_button_ok window.cSD.ssC: '+JSON.stringify(window.cSD.ssC));

				if (count) {
					window.old_ss_st_select = window.tmp_ss_st_select;
					$('.sorter_settings_fade').fadeOut(10);
				} else {
					window.cSD.ssC = [];
					$().message_err('Должен быть выбран хотя бы 1 пункт!');
				}
			});

			window.ss_ms_case_row_dbl_click = function(clickedID, clickedType) {
				let typesL = ['sort', 'order'];
				let typesU = ['Sort', 'Order'];
				let cases = document.getElementById(typesL[clickedType] + 'able1').getElementsByClassName('ss_ms_case_row');
				let count_l = 0;
				let tmp_val_l = [];
				let tmp_txt_l = [];
				let count_r = 0;
				let tmp_val_r = [];
				let tmp_txt_r = [];
				for (let item of cases) {
					if (item.value == clickedID) {
						tmp_val_r[count_r] = item.value;
						tmp_txt_r[count_r] = item.textContent;
						count_r++;
					} else {
						tmp_val_l[count_l] = item.value;
						tmp_txt_l[count_l] = item.textContent;
						count_l++;
					}
				}
				cases = document.getElementById(typesL[clickedType] + 'able2').getElementsByClassName('ss_ms_case_row');
				for (let item of cases) {
					if (item.value == clickedID) {
						tmp_val_l[count_l] = item.value;
						tmp_txt_l[count_l] = item.textContent;
						count_l++;
					} else {
						tmp_val_r[count_r] = item.value;
						tmp_txt_r[count_r] = item.textContent;
						count_r++;
					}
				}
				let ss_select_content = '<ul id="' + typesL[clickedType] + 'able1" class="connected' + typesU[clickedType] + 'able">';
				for (let i = 0; i < count_l; i++) {
					ss_select_content += '<li class="ss_ms_case_row" ondblclick="window.ss_ms_case_row_dbl_click(' + tmp_val_l[i] + ', 0);" value="' + tmp_val_l[i] + '">' + tmp_txt_l[i] + '</li>';
				}
				ss_select_content += '</ul><ul id="' + typesL[clickedType] + 'able2" class="connected' + typesU[clickedType] + 'able_1">';
				for (let i = 0; i < count_r; i++) {
					ss_select_content += '<li class="ss_ms_case_row" ondblclick="window.ss_ms_case_row_dbl_click(' + tmp_val_r[i] + ', 0);" value="' + tmp_val_r[i] + '">' + tmp_txt_r[i] + '</li>';
				}
				ss_select_content += '</ul>';
				if (clickedType) {
					let oos_select_content = '';
					let options_names = ['Other не отображать','Other отображать'];
					if ((typeof(window.cSD.oos) != "undefined") && (window.cSD.oos !== null)) {
						for (let i = 0; i < 2; i++) {
							if (i === Number(window.cSD.oos)) {
								oos_select_content += '<option selected="selected" id="' + i + '" value="' + i + '">' + options_names[i] + '</option>';
							} else {
								oos_select_content += '<option id="' + i + '" value="' + i + '">' + options_names[i] + '</option>';
							}
						}
					} else {
						for (let i = 0; i < 2; i++) {
							oos_select_content += '<option id="' + i + '" value="' + i + '">' + options_names[i] + '</option>';
						}
					}
					$('.orderer_settings')[0].innerHTML = '<div class="ss_label"><p>Выберите значения для построения графика</p></div><div class="sos_title"><div class="sos_title_left">Перетащите необходимые элементы</div><div class="sos_title_right">сюда</div></div><div class="os_multiselect">' + ss_select_content + '</div><select class="oos_select" name="oos_select" id="oos_select">' + oos_select_content + '</select><input title="Добавить все" type="button" class="orderableButtonAddAll" name="orderableButtonAddAll" id="orderableButtonAddAll" value=">>" /><input type="button" class="ors_button_ok" name="ors_button_ok" id="ors_button_ok" value="Ок" /><input type="button" class="ors_button_exit" name="ors_button_exit" id="ors_button_exit" value="Отменить" />';
				} else {
					let sos_select_content = '';
					let options_names = ['Сортировать по порядку набора','Сортировать по суммам первых столбцов','Сортировать по суммам вторых столбцов','Сортировать по суммам третьих столбцов','Сортировать по общим суммам столбцов'];
					if ($('.ss_st_select').val() != 14) {
						if ((typeof(window.cSD.sos) != "undefined") && (window.cSD.sos !== null)) {
							for (let i = 0; i < 5; i++) {
								if (i === Number(window.cSD.sos)) {
									sos_select_content += '<option selected="selected" id="' + i + '" value="' + i + '">' + options_names[i] + '</option>';
								} else {
									sos_select_content += '<option id="' + i + '" value="' + i + '">' + options_names[i] + '</option>';
								}
							}
						} else {
							for (let i = 0; i < 5; i++) {
								sos_select_content += '<option id="' + i + '" value="' + i + '">' + options_names[i] + '</option>';
							}
						}
					} else {
						options_names = ['Тотал по всем','Тотал по выбранным'];
						if ((typeof(window.cSD.sos) != "undefined") && (window.cSD.sos !== null)) {
							for (let i = 0; i < 2; i++) {
								if (i === Number(window.cSD.sos)) {
									sos_select_content += '<option selected="selected" id="' + i + '" value="' + i + '">' + options_names[i] + '</option>';
								} else {
									sos_select_content += '<option id="' + i + '" value="' + i + '">' + options_names[i] + '</option>';
								}
							}
						} else {
							for (let i = 0; i < 2; i++) {
								sos_select_content += '<option id="' + i + '" value="' + i + '">' + options_names[i] + '</option>';
							}
						}
					}
					if (window.cSD.page8Flag) {
						$('.sorter_settings')[0].innerHTML = '<div class="ss_label"><p>Выберите значения для построения графика</p></div><div class="sos_title"><div class="sos_title_left">Перетащите необходимые элементы</div><div class="sos_title_right">сюда</div></div><div class="ss_multiselect">' + ss_select_content + '</div><input title="Добавить все" type="button" class="sortableButtonAddAll" name="sortableButtonAddAll" id="sortableButtonAddAll" value=">>" /><input type="button" class="sos_button_ok" name="sos_button_ok" id="sos_button_ok" value="Ок" /><input type="button" class="sos_button_exit_2" name="sos_button_exit_2" id="sos_button_exit_2" value="Отменить" />';
					} else {
						$('.sorter_settings')[0].innerHTML = '<div class="ss_label"><p>Выберите значения для построения графика</p></div><div class="sos_title"><div class="sos_title_left">Перетащите необходимые элементы</div><div class="sos_title_right">сюда</div></div><div class="ss_multiselect">' + ss_select_content + '</div><select class="sos_select" name="sos_select" id="sos_select">' + sos_select_content + '</select><input title="Добавить все" type="button" class="sortableButtonAddAll" name="sortableButtonAddAll" id="sortableButtonAddAll" value=">>" /><input type="button" class="sos_button_ok" name="sos_button_ok" id="sos_button_ok" value="Ок" /><input type="button" class="sos_button_exit_1" name="sos_button_exit_1" id="sos_button_exit_1" value="Отменить" />';
					}
				}
				window.sortDisableSelection();
			}

			$(document).on('click', '.sortableButtonAddAll', function() {
				let cases = document.getElementById('sortable2').getElementsByClassName('ss_ms_case_row');
				let count = 0;
				let tmp_val = [];
				let tmp_txt = [];
				for (let item of cases) {
					tmp_val[count] = item.value;
					tmp_txt[count] = item.textContent;
					count++;
				}
				cases = document.getElementById('sortable1').getElementsByClassName('ss_ms_case_row');
				for (let item of cases) {
					tmp_val[count] = item.value;
					tmp_txt[count] = item.textContent;
					count++;
				}
				let ss_select_content = '<ul id="sortable2" class="connectedSortable_1">';
				for (let i = 0; i < count; i++) {
					ss_select_content += '<li class="ss_ms_case_row" ondblclick="window.ss_ms_case_row_dbl_click(' + tmp_val[i] + ', 0);" value="' + tmp_val[i] + '">' + tmp_txt[i] + '</li>';
				}
				ss_select_content = '<ul id="sortable1" class="connectedSortable"></ul>' + ss_select_content + '</ul>';
				let sos_select_content = '';
					let options_names = ['Сортировать по порядку набора','Сортировать по суммам первых столбцов','Сортировать по суммам вторых столбцов','Сортировать по суммам третьих столбцов','Сортировать по общим суммам столбцов'];
					if ($('.ss_st_select').val() != 14) {
						if ((typeof(window.cSD.sos) != "undefined") && (window.cSD.sos !== null)) {
							for (let i = 0; i < 5; i++) {
								if (i === Number(window.cSD.sos)) {
									sos_select_content += '<option selected="selected" id="' + i + '" value="' + i + '">' + options_names[i] + '</option>';
								} else {
									sos_select_content += '<option id="' + i + '" value="' + i + '">' + options_names[i] + '</option>';
								}
							}
						} else {
							for (let i = 0; i < 5; i++) {
								sos_select_content += '<option id="' + i + '" value="' + i + '">' + options_names[i] + '</option>';
							}
						}
					} else {
						options_names = ['Тотал по всем','Тотал по выбранным'];
						if ((typeof(window.cSD.sos) != "undefined") && (window.cSD.sos !== null)) {
							for (let i = 0; i < 2; i++) {
								if (i === Number(window.cSD.sos)) {
									sos_select_content += '<option selected="selected" id="' + i + '" value="' + i + '">' + options_names[i] + '</option>';
								} else {
									sos_select_content += '<option id="' + i + '" value="' + i + '">' + options_names[i] + '</option>';
								}
							}
						} else {
							for (let i = 0; i < 2; i++) {
								sos_select_content += '<option id="' + i + '" value="' + i + '">' + options_names[i] + '</option>';
							}
						}
					}
				if (window.cSD.page8Flag) {
					$('.sorter_settings')[0].innerHTML = '<div class="ss_label"><p>Выберите значения для построения графика</p></div><div class="sos_title"><div class="sos_title_left">Перетащите необходимые элементы</div><div class="sos_title_right">сюда</div></div><div class="ss_multiselect">' + ss_select_content + '</div><input title="Добавить все" type="button" class="sortableButtonAddAll" name="sortableButtonAddAll" id="sortableButtonAddAll" value=">>" /><input type="button" class="sos_button_ok" name="sos_button_ok" id="sos_button_ok" value="Ок" /><input type="button" class="sos_button_exit_2" name="sos_button_exit_2" id="sos_button_exit_2" value="Отменить" />';
				} else {
					$('.sorter_settings')[0].innerHTML = '<div class="ss_label"><p>Выберите значения для построения графика</p></div><div class="sos_title"><div class="sos_title_left">Перетащите необходимые элементы</div><div class="sos_title_right">сюда</div></div><div class="ss_multiselect">' + ss_select_content + '</div><select class="sos_select" name="sos_select" id="sos_select">' + sos_select_content + '</select><input title="Добавить все" type="button" class="sortableButtonAddAll" name="sortableButtonAddAll" id="sortableButtonAddAll" value=">>" /><input type="button" class="sos_button_ok" name="sos_button_ok" id="sos_button_ok" value="Ок" /><input type="button" class="sos_button_exit_1" name="sos_button_exit_1" id="sos_button_exit_1" value="Отменить" />';
				}
				window.sortDisableSelection();
			});

			$(document).on('click', '.orderableButtonAddAll', function() {
				let cases = document.getElementById('orderable2').getElementsByClassName('ss_ms_case_row');
				let count = 0;
				let tmp_val = [];
				let tmp_txt = [];
				for (let item of cases) {
					tmp_val[count] = item.value;
					tmp_txt[count] = item.textContent;
					count++;
				}
				cases = document.getElementById('orderable1').getElementsByClassName('ss_ms_case_row');
				for (let item of cases) {
					tmp_val[count] = item.value;
					tmp_txt[count] = item.textContent;
					count++;
				}
				let ss_select_content = '<ul id="orderable2" class="connectedOrderable_1">';
				for (let i = 0; i < count; i++) {
					ss_select_content += '<li class="ss_ms_case_row" ondblclick="window.ss_ms_case_row_dbl_click(' + tmp_val[i] + ', 1);" value="' + tmp_val[i] + '">' + tmp_txt[i] + '</li>';
				}
				ss_select_content = '<ul id="orderable1" class="connectedOrderable"></ul>' + ss_select_content + '</ul>';
				let oos_select_content = '';
				let options_names = ['Other не отображать','Other отображать'];
				if ((typeof(window.cSD.oos) != "undefined") && (window.cSD.oos !== null)) {
					for (let i = 0; i < 2; i++) {
						if (i === Number(window.cSD.oos)) {
							oos_select_content += '<option selected="selected" id="' + i + '" value="' + i + '">' + options_names[i] + '</option>';
						} else {
							oos_select_content += '<option id="' + i + '" value="' + i + '">' + options_names[i] + '</option>';
						}
					}
				} else {
					for (let i = 0; i < 2; i++) {
						oos_select_content += '<option id="' + i + '" value="' + i + '">' + options_names[i] + '</option>';
					}
				}
				$('.orderer_settings')[0].innerHTML = '<div class="ss_label"><p>Выберите значения для построения графика</p></div><div class="sos_title"><div class="sos_title_left">Перетащите необходимые элементы</div><div class="sos_title_right">сюда</div></div><div class="os_multiselect">' + ss_select_content + '</div><select class="oos_select" name="oos_select" id="oos_select">' + oos_select_content + '</select><input title="Добавить все" type="button" class="orderableButtonAddAll" name="orderableButtonAddAll" id="orderableButtonAddAll" value=">>" /><input type="button" class="ors_button_ok" name="ors_button_ok" id="ors_button_ok" value="Ок" /><input type="button" class="ors_button_exit" name="ors_button_exit" id="ors_button_exit" value="Отменить" />';
				window.sortDisableSelection();
			});

			$(document).on('click', '.sc_button_ok', function() {
				window.cSD.sCD = $('.sc_select').val();
				$('.sorter_choose_fade').fadeOut(10);
				window.sorterSettingsDraw();
			});

			$(document).on('click', '.sc_button_exit', function() {
				let options_names = ['Слайд тип "Страница1"','Слайд тип "Страница2"','Слайд тип "Страница3"','Слайд тип "Страница4 YTD"','Слайд тип "Страница5 MAT"','Слайд тип "Страница6 Круговая YTD"','Слайд тип "Страница7 Круговая MAT"','Слайд тип "Страница8 YTD 2 стб"','Слайд тип "Страница9 MAT 2 стб"','Слайд тип "Страница10 DIAP 2 стб"','Слайд тип "Страница11 YTD 3 стб"','Слайд тип "Страница12 MAT 3 стб"','Слайд тип "Страница13 DIAP 3 стб"','Слайд тип "Страница14"','Слайд тип "Страница15 4стб"','Слайд тип "Страница16 `горизонтальные тройки`"'];
				let ss_st_select_content = '';
				for (let i = 0; i < 16; i++) {
					if (i === Number(window.old_ss_st_select)) {
						ss_st_select_content += '<option selected="selected" id="' + i + '" value="' + i + '">' + options_names[i] + '</option>';
					} else {
						ss_st_select_content += '<option id="' + i + '" value="' + i + '">' + options_names[i] + '</option>';
					}
				}
				$('.ss_st_select')[0].innerHTML = ss_st_select_content;
				if ((($('.ss_st_select').val() > 6) && ($('.ss_st_select').val() < 13)) || ($('.ss_st_select').val() == 14)) {
					$('.ss_sdd_date_input_1').prop('disabled', false);
					$('.ss_sd_select').prop('disabled', true);
				} else {
					$('.ss_sdd_date_input_1').prop('disabled', true);
					$('.ss_sd_select').prop('disabled', false);
				}
				if ((typeof(window.cSD.sDt) != "undefined") && (window.cSD.sDt !== null)) {
					document.getElementById("ss_sdd_date_input_1").value = window.cSD.sDt;
				}
				if ((typeof(window.cSD.eDt) != "undefined") && (window.cSD.eDt !== null)) {
					document.getElementById("ss_sdd_date_input_2").value = window.cSD.eDt;
				}
				$('.sorter_choose_fade').fadeOut(10);
			});

			$(document).on('click', '.ss_fb_fl_fr_delete', function() {
				window.cSD.cF = $(this).attr('class').replace('ss_fb_fl_fr_delete _', '');
				window.cSD.fD.splice(window.cSD.cF, 1);
				window.filtersListDraw();
				window.cSD.fC--;
			});

			$(document).on('click', '.ss_button_ok', async function() {
				window.cSD.sDt = $('.ss_sdd_date_input_1').val();
				window.cSD.eDt = $('.ss_sdd_date_input_2').val();
				window.cSD.sT = $('.ss_sn_input').val();
				window.cSD.gT = $('.ss_st_select').val();
				window.cSD.sI = $('.ss_so_select').val();
				window.cSD.iI = $('.ss_sd_select').val();
				window.cSD.sP = $('.ss_sp_select').val();
				window.cSD.sU = $('.ss_su_select').val();
				window.cSD.sM = $('.ss_sm_select').val();
				window.cSD.sCs = $('.ss_sc_select').val();
				window.cSD.ors = 0;
				$('.slide_settings_fade').fadeOut(10);
				if (window.pres) {
					window.pSD = JSON.parse(JSON.stringify(window.cSD));
				} else {
					if (window.sC === window.cS) {
						window.cP = window.sC;
						window.sC++;
					}
					window.sD[window.cS] = JSON.parse(JSON.stringify(window.cSD));
					window.currentSlideDraw();
					await new Promise(r => setTimeout(r, 3000));
					$.when.apply($, window.deferreds_global, window).then(async function () {
						window.thumbnailDraw();
						//await new Promise(r => setTimeout(r, 1000));
						$.when.apply($, window.deferreds_global2, window).then(async function () {
							window.slidesListDraw();
						});
					});
				}
			});

			$(document).on('click', '.ds_button_ok', async function() {
				/*window.cSD.sDt = $('.ss_sdd_date_input_1').val();
				window.cSD.eDt = $('.ss_sdd_date_input_2').val();
				window.cSD.sT = $('.ss_sn_input').val();
				window.cSD.gT = $('.ss_st_select').val();
				window.cSD.sI = $('.ss_so_select').val();
				window.cSD.iI = $('.ss_sd_select').val();
				window.cSD.sP = $('.ss_sp_select').val();
				window.cSD.sU = $('.ss_su_select').val();
				window.cSD.sM = $('.ss_sm_select').val();
				window.cSD.sCs = $('.ss_sc_select').val();
				window.cSD.ors = 0;*/
				$('.diapasons_settings_fade').fadeOut(10);
				/*if (window.pres) {
					window.pSD = JSON.parse(JSON.stringify(window.cSD));
				} else {
					if (window.sC === window.cS) {
						window.cP = window.sC;
						window.sC++;
					}
					window.sD[window.cS] = JSON.parse(JSON.stringify(window.cSD));
					window.currentSlideDraw();
					await new Promise(r => setTimeout(r, 3000));
					$.when.apply($, window.deferreds_global, window).then(async function () {
						window.thumbnailDraw();
						//await new Promise(r => setTimeout(r, 1000));
						$.when.apply($, window.deferreds_global2, window).then(async function () {
							window.slidesListDraw();
						});
					});
				}*/
			});

			$(document).on('click', '.ss_button_order', function() {
				window.cSD.sI = $('.ss_so_select').val();
				window.cSD.sP = $('.ss_sp_select').val();
				window.cSD.sU = $('.ss_su_select').val();
				window.cSD.sCs = $('.ss_sc_select').val();
				window.ordererSettingsDraw();
			});

			$(document).on('click', '.ors_button_ok', function() {

				let cases = document.getElementById('orderable2').getElementsByClassName('ss_ms_case_row');
				let count = 0;
				let tmp_arr = [];
				for (let item of cases) {
					tmp_arr[count] = item.value;
					count++;
				}
				window.cSD.osC = tmp_arr;
				if (count) {
					$('.orderer_settings_fade').fadeOut(10);

					if (window.sC === window.cS) {
						window.cP = window.sC;
						window.sC++;
					}
					window.cSD.oos = $('.oos_select').val();
					window.cSD.sT = $('.ss_sn_input').val();
					window.cSD.gT = $('.ss_st_select').val();
					window.cSD.sI = $('.ss_so_select').val();
					window.cSD.iI = $('.ss_sd_select').val();
					window.cSD.sP = $('.ss_sp_select').val();
					window.cSD.sU = $('.ss_su_select').val();
					window.cSD.sM = $('.ss_sm_select').val();
					window.cSD.sCs = $('.ss_sc_select').val();
					window.cSD.ors = 1;
					window.sD[window.cS] = JSON.parse(JSON.stringify(window.cSD));

					//window.oGsI = $('.ss_so_select').val();

					window.slidesListDraw();
					window.currentSlideDraw();
					$('.slide_settings_fade').fadeOut(10);
				} else {
					window.cSD.osC = [];
					$().message_err('Должен быть выбран хотя бы 1 пункт!');
				}
			});

			$(document).on('click', '.tmpl_button_ok', function() {
				$().message('Template saved...');
				$('.tmpl_settings_fade').fadeOut(10);
			});

			$(document).on('click', '.sm_sl_sr_delete, .sm_sl_sr_img_delete', function() {
				window.cS = $(this).closest('div.sm_sl_slide_row1').attr('class').replace('sm_sl_slide_row1 _', '');
				
				if (window.sD[window.cS].fP) {
					window.fC--;
				}
				window.sD.splice(window.cS, 1);
				window.slidesListDraw();
				window.sC--;
				if (window.sC < 1) {
					$('#uploader, #uploader_cur').prop('disabled', true);
					$('#del_slide').prop('disabled', true);
					$('#edit_slide').prop('disabled', true);
					$('#copy_slide').prop('disabled', true);
					$('#new_tmpl').prop('disabled', true);
				}
				if (window.cS < window.cP) {
					window.cP--;
				} else if (window.cS == window.cP) {
					if ((window.cP > 0) || (window.sC == 0)) {
						window.cP--;
					}
					window.currentSlideDraw();
				}
			});

			$(document).on('click', '.left_arrow', function() {
				$('.right_arrow').show();
				$('.left_arrow').hide();
				$('.slider_menu_fade').fadeOut(10);
			});

			$(document).on('click', '.sp_left_button', function() {
				if (window.sC > 1) {
					if (window.cP == 0) {
						window.cP = window.sC;
					}
					window.cP--;
					window.cSD = JSON.parse(JSON.stringify(window.sD[window.cP]));
					window.currentSlideDraw();
				}
			});

			$(document).on('click', '.sp_right_button', function() {

				if (window.sC > 1) {
					window.cP++;
					if (window.cP == window.sC) {
						window.cP = 0;
					}
					window.cSD = JSON.parse(JSON.stringify(window.sD[window.cP]));

					window.currentSlideDraw();
				}
			});

			$(document).on('click', '.sm_sl_sr_img', function() {
				window.cS = $(this).closest('div.sm_sl_slide_row1').attr('class').replace('sm_sl_slide_row1 _', '');
				window.cSD = JSON.parse(JSON.stringify(window.sD[window.cS]));
				window.cP = window.cS;
				$('.right_arrow').show();
				$('.left_arrow').hide();
				$('.slider_menu_fade').fadeOut(10);
				window.currentSlideDraw();
			});

		});
	</script>
	<script>
		/*! jQuery v3.5.1 | (c) JS Foundation and other contributors | jquery.org/license */ ! function(e, t) {
			"use strict";
			"object" == typeof module && "object" == typeof module.exports ? module.exports = e.document ? t(e, !0) : function(e) {
				if (!e.document) throw new Error("jQuery requires a window with a document");
				return t(e)
			} : t(e)
		}("undefined" != typeof window ? window : this, function(C, e) {
			"use strict";
			var t = [],
				r = Object.getPrototypeOf,
				s = t.slice,
				g = t.flat ? function(e) {
					return t.flat.call(e)
				} : function(e) {
					return t.concat.apply([], e)
				},
				u = t.push,
				i = t.indexOf,
				n = {},
				o = n.toString,
				v = n.hasOwnProperty,
				a = v.toString,
				l = a.call(Object),
				y = {},
				m = function(e) {
					return "function" == typeof e && "number" != typeof e.nodeType
				},
				x = function(e) {
					return null != e && e === e.window
				},
				E = C.document,
				c = {
					type: !0,
					src: !0,
					nonce: !0,
					noModule: !0
				};

			function b(e, t, n) {
				var r, i, o = (n = n || E).createElement("script");
				if (o.text = e, t)
					for (r in c)(i = t[r] || t.getAttribute && t.getAttribute(r)) && o.setAttribute(r, i);
				n.head.appendChild(o).parentNode.removeChild(o)
			}

			function w(e) {
				return null == e ? e + "" : "object" == typeof e || "function" == typeof e ? n[o.call(e)] || "object" : typeof e
			}
			var f = "3.5.1",
				S = function(e, t) {
					return new S.fn.init(e, t)
				};

			function p(e) {
				var t = !!e && "length" in e && e.length,
					n = w(e);
				return !m(e) && !x(e) && ("array" === n || 0 === t || "number" == typeof t && 0 < t && t - 1 in e)
			}
			S.fn = S.prototype = {
				jquery: f,
				constructor: S,
				length: 0,
				toArray: function() {
					return s.call(this)
				},
				get: function(e) {
					return null == e ? s.call(this) : e < 0 ? this[e + this.length] : this[e]
				},
				pushStack: function(e) {
					var t = S.merge(this.constructor(), e);
					return t.prevObject = this, t
				},
				each: function(e) {
					return S.each(this, e)
				},
				map: function(n) {
					return this.pushStack(S.map(this, function(e, t) {
						return n.call(e, t, e)
					}))
				},
				slice: function() {
					return this.pushStack(s.apply(this, arguments))
				},
				first: function() {
					return this.eq(0)
				},
				last: function() {
					return this.eq(-1)
				},
				even: function() {
					return this.pushStack(S.grep(this, function(e, t) {
						return (t + 1) % 2
					}))
				},
				odd: function() {
					return this.pushStack(S.grep(this, function(e, t) {
						return t % 2
					}))
				},
				eq: function(e) {
					var t = this.length,
						n = +e + (e < 0 ? t : 0);
					return this.pushStack(0 <= n && n < t ? [this[n]] : [])
				},
				end: function() {
					return this.prevObject || this.constructor()
				},
				push: u,
				sort: t.sort,
				splice: t.splice
			}, S.extend = S.fn.extend = function() {
				var e, t, n, r, i, o, a = arguments[0] || {},
					s = 1,
					u = arguments.length,
					l = !1;
				for ("boolean" == typeof a && (l = a, a = arguments[s] || {}, s++), "object" == typeof a || m(a) || (a = {}), s === u && (a = this, s--); s < u; s++)
					if (null != (e = arguments[s]))
						for (t in e) r = e[t], "__proto__" !== t && a !== r && (l && r && (S.isPlainObject(r) || (i = Array.isArray(r))) ? (n = a[t], o = i && !Array.isArray(n) ? [] : i || S.isPlainObject(n) ? n : {}, i = !1, a[t] = S.extend(l, o, r)) : void 0 !== r && (a[t] = r));
				return a
			}, S.extend({
				expando: "jQuery" + (f + Math.random()).replace(/\D/g, ""),
				isReady: !0,
				error: function(e) {
					throw new Error(e)
				},
				noop: function() {},
				isPlainObject: function(e) {
					var t, n;
					return !(!e || "[object Object]" !== o.call(e)) && (!(t = r(e)) || "function" == typeof(n = v.call(t, "constructor") && t.constructor) && a.call(n) === l)
				},
				isEmptyObject: function(e) {
					var t;
					for (t in e) return !1;
					return !0
				},
				globalEval: function(e, t, n) {
					b(e, {
						nonce: t && t.nonce
					}, n)
				},
				each: function(e, t) {
					var n, r = 0;
					if (p(e)) {
						for (n = e.length; r < n; r++)
							if (!1 === t.call(e[r], r, e[r])) break
					} else
						for (r in e)
							if (!1 === t.call(e[r], r, e[r])) break; return e
				},
				makeArray: function(e, t) {
					var n = t || [];
					return null != e && (p(Object(e)) ? S.merge(n, "string" == typeof e ? [e] : e) : u.call(n, e)), n
				},
				inArray: function(e, t, n) {
					return null == t ? -1 : i.call(t, e, n)
				},
				merge: function(e, t) {
					for (var n = +t.length, r = 0, i = e.length; r < n; r++) e[i++] = t[r];
					return e.length = i, e
				},
				grep: function(e, t, n) {
					for (var r = [], i = 0, o = e.length, a = !n; i < o; i++) !t(e[i], i) !== a && r.push(e[i]);
					return r
				},
				map: function(e, t, n) {
					var r, i, o = 0,
						a = [];
					if (p(e))
						for (r = e.length; o < r; o++) null != (i = t(e[o], o, n)) && a.push(i);
					else
						for (o in e) null != (i = t(e[o], o, n)) && a.push(i);
					return g(a)
				},
				guid: 1,
				support: y
			}), "function" == typeof Symbol && (S.fn[Symbol.iterator] = t[Symbol.iterator]), S.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function(e, t) {
				n["[object " + t + "]"] = t.toLowerCase()
			});
			var d = function(n) {
				var e, d, b, o, i, h, f, g, w, u, l, T, C, a, E, v, s, c, y, S = "sizzle" + 1 * new Date,
					p = n.document,
					k = 0,
					r = 0,
					m = ue(),
					x = ue(),
					A = ue(),
					N = ue(),
					D = function(e, t) {
						return e === t && (l = !0), 0
					},
					j = {}.hasOwnProperty,
					t = [],
					q = t.pop,
					L = t.push,
					H = t.push,
					O = t.slice,
					P = function(e, t) {
						for (var n = 0, r = e.length; n < r; n++)
							if (e[n] === t) return n;
						return -1
					},
					R = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
					M = "[\\x20\\t\\r\\n\\f]",
					I = "(?:\\\\[\\da-fA-F]{1,6}" + M + "?|\\\\[^\\r\\n\\f]|[\\w-]|[^\0-\\x7f])+",
					W = "\\[" + M + "*(" + I + ")(?:" + M + "*([*^$|!~]?=)" + M + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + I + "))|)" + M + "*\\]",
					F = ":(" + I + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + W + ")*)|.*)\\)|)",
					B = new RegExp(M + "+", "g"),
					$ = new RegExp("^" + M + "+|((?:^|[^\\\\])(?:\\\\.)*)" + M + "+$", "g"),
					_ = new RegExp("^" + M + "*," + M + "*"),
					z = new RegExp("^" + M + "*([>+~]|" + M + ")" + M + "*"),
					U = new RegExp(M + "|>"),
					X = new RegExp(F),
					V = new RegExp("^" + I + "$"),
					G = {
						ID: new RegExp("^#(" + I + ")"),
						CLASS: new RegExp("^\\.(" + I + ")"),
						TAG: new RegExp("^(" + I + "|[*])"),
						ATTR: new RegExp("^" + W),
						PSEUDO: new RegExp("^" + F),
						CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + M + "*(even|odd|(([+-]|)(\\d*)n|)" + M + "*(?:([+-]|)" + M + "*(\\d+)|))" + M + "*\\)|)", "i"),
						bool: new RegExp("^(?:" + R + ")$", "i"),
						needsContext: new RegExp("^" + M + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + M + "*((?:-\\d)?\\d*)" + M + "*\\)|)(?=[^-]|$)", "i")
					},
					Y = /HTML$/i,
					Q = /^(?:input|select|textarea|button)$/i,
					J = /^h\d$/i,
					K = /^[^{]+\{\s*\[native \w/,
					Z = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
					ee = /[+~]/,
					te = new RegExp("\\\\[\\da-fA-F]{1,6}" + M + "?|\\\\([^\\r\\n\\f])", "g"),
					ne = function(e, t) {
						var n = "0x" + e.slice(1) - 65536;
						return t || (n < 0 ? String.fromCharCode(n + 65536) : String.fromCharCode(n >> 10 | 55296, 1023 & n | 56320))
					},
					re = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
					ie = function(e, t) {
						return t ? "\0" === e ? "\ufffd" : e.slice(0, -1) + "\\" + e.charCodeAt(e.length - 1).toString(16) + " " : "\\" + e
					},
					oe = function() {
						T()
					},
					ae = be(function(e) {
						return !0 === e.disabled && "fieldset" === e.nodeName.toLowerCase()
					}, {
						dir: "parentNode",
						next: "legend"
					});
				try {
					H.apply(t = O.call(p.childNodes), p.childNodes), t[p.childNodes.length].nodeType
				} catch (e) {
					H = {
						apply: t.length ? function(e, t) {
							L.apply(e, O.call(t))
						} : function(e, t) {
							var n = e.length,
								r = 0;
							while (e[n++] = t[r++]);
							e.length = n - 1
						}
					}
				}

				function se(t, e, n, r) {
					var i, o, a, s, u, l, c, f = e && e.ownerDocument,
						p = e ? e.nodeType : 9;
					if (n = n || [], "string" != typeof t || !t || 1 !== p && 9 !== p && 11 !== p) return n;
					if (!r && (T(e), e = e || C, E)) {
						if (11 !== p && (u = Z.exec(t)))
							if (i = u[1]) {
								if (9 === p) {
									if (!(a = e.getElementById(i))) return n;
									if (a.id === i) return n.push(a), n
								} else if (f && (a = f.getElementById(i)) && y(e, a) && a.id === i) return n.push(a), n
							} else {
								if (u[2]) return H.apply(n, e.getElementsByTagName(t)), n;
								if ((i = u[3]) && d.getElementsByClassName && e.getElementsByClassName) return H.apply(n, e.getElementsByClassName(i)), n
							}
						if (d.qsa && !N[t + " "] && (!v || !v.test(t)) && (1 !== p || "object" !== e.nodeName.toLowerCase())) {
							if (c = t, f = e, 1 === p && (U.test(t) || z.test(t))) {
								(f = ee.test(t) && ye(e.parentNode) || e) === e && d.scope || ((s = e.getAttribute("id")) ? s = s.replace(re, ie) : e.setAttribute("id", s = S)), o = (l = h(t)).length;
								while (o--) l[o] = (s ? "#" + s : ":scope") + " " + xe(l[o]);
								c = l.join(",")
							}
							try {
								return H.apply(n, f.querySelectorAll(c)), n
							} catch (e) {
								N(t, !0)
							} finally {
								s === S && e.removeAttribute("id")
							}
						}
					}
					return g(t.replace($, "$1"), e, n, r)
				}

				function ue() {
					var r = [];
					return function e(t, n) {
						return r.push(t + " ") > b.cacheLength && delete e[r.shift()], e[t + " "] = n
					}
				}

				function le(e) {
					return e[S] = !0, e
				}

				function ce(e) {
					var t = C.createElement("fieldset");
					try {
						return !!e(t)
					} catch (e) {
						return !1
					} finally {
						t.parentNode && t.parentNode.removeChild(t), t = null
					}
				}

				function fe(e, t) {
					var n = e.split("|"),
						r = n.length;
					while (r--) b.attrHandle[n[r]] = t
				}

				function pe(e, t) {
					var n = t && e,
						r = n && 1 === e.nodeType && 1 === t.nodeType && e.sourceIndex - t.sourceIndex;
					if (r) return r;
					if (n)
						while (n = n.nextSibling)
							if (n === t) return -1;
					return e ? 1 : -1
				}

				function de(t) {
					return function(e) {
						return "input" === e.nodeName.toLowerCase() && e.type === t
					}
				}

				function he(n) {
					return function(e) {
						var t = e.nodeName.toLowerCase();
						return ("input" === t || "button" === t) && e.type === n
					}
				}

				function ge(t) {
					return function(e) {
						return "form" in e ? e.parentNode && !1 === e.disabled ? "label" in e ? "label" in e.parentNode ? e.parentNode.disabled === t : e.disabled === t : e.isDisabled === t || e.isDisabled !== !t && ae(e) === t : e.disabled === t : "label" in e && e.disabled === t
					}
				}

				function ve(a) {
					return le(function(o) {
						return o = +o, le(function(e, t) {
							var n, r = a([], e.length, o),
								i = r.length;
							while (i--) e[n = r[i]] && (e[n] = !(t[n] = e[n]))
						})
					})
				}

				function ye(e) {
					return e && "undefined" != typeof e.getElementsByTagName && e
				}
				for (e in d = se.support = {}, i = se.isXML = function(e) {
						var t = e.namespaceURI,
							n = (e.ownerDocument || e).documentElement;
						return !Y.test(t || n && n.nodeName || "HTML")
					}, T = se.setDocument = function(e) {
						var t, n, r = e ? e.ownerDocument || e : p;
						return r != C && 9 === r.nodeType && r.documentElement && (a = (C = r).documentElement, E = !i(C), p != C && (n = C.defaultView) && n.top !== n && (n.addEventListener ? n.addEventListener("unload", oe, !1) : n.attachEvent && n.attachEvent("onunload", oe)), d.scope = ce(function(e) {
							return a.appendChild(e).appendChild(C.createElement("div")), "undefined" != typeof e.querySelectorAll && !e.querySelectorAll(":scope fieldset div").length
						}), d.attributes = ce(function(e) {
							return e.className = "i", !e.getAttribute("className")
						}), d.getElementsByTagName = ce(function(e) {
							return e.appendChild(C.createComment("")), !e.getElementsByTagName("*").length
						}), d.getElementsByClassName = K.test(C.getElementsByClassName), d.getById = ce(function(e) {
							return a.appendChild(e).id = S, !C.getElementsByName || !C.getElementsByName(S).length
						}), d.getById ? (b.filter.ID = function(e) {
							var t = e.replace(te, ne);
							return function(e) {
								return e.getAttribute("id") === t
							}
						}, b.find.ID = function(e, t) {
							if ("undefined" != typeof t.getElementById && E) {
								var n = t.getElementById(e);
								return n ? [n] : []
							}
						}) : (b.filter.ID = function(e) {
							var n = e.replace(te, ne);
							return function(e) {
								var t = "undefined" != typeof e.getAttributeNode && e.getAttributeNode("id");
								return t && t.value === n
							}
						}, b.find.ID = function(e, t) {
							if ("undefined" != typeof t.getElementById && E) {
								var n, r, i, o = t.getElementById(e);
								if (o) {
									if ((n = o.getAttributeNode("id")) && n.value === e) return [o];
									i = t.getElementsByName(e), r = 0;
									while (o = i[r++])
										if ((n = o.getAttributeNode("id")) && n.value === e) return [o]
								}
								return []
							}
						}), b.find.TAG = d.getElementsByTagName ? function(e, t) {
							return "undefined" != typeof t.getElementsByTagName ? t.getElementsByTagName(e) : d.qsa ? t.querySelectorAll(e) : void 0
						} : function(e, t) {
							var n, r = [],
								i = 0,
								o = t.getElementsByTagName(e);
							if ("*" === e) {
								while (n = o[i++]) 1 === n.nodeType && r.push(n);
								return r
							}
							return o
						}, b.find.CLASS = d.getElementsByClassName && function(e, t) {
							if ("undefined" != typeof t.getElementsByClassName && E) return t.getElementsByClassName(e)
						}, s = [], v = [], (d.qsa = K.test(C.querySelectorAll)) && (ce(function(e) {
							var t;
							a.appendChild(e).innerHTML = "<a id='" + S + "'></a><select id='" + S + "-\r\\' msallowcapture=''><option selected=''></option></select>", e.querySelectorAll("[msallowcapture^='']").length && v.push("[*^$]=" + M + "*(?:''|\"\")"), e.querySelectorAll("[selected]").length || v.push("\\[" + M + "*(?:value|" + R + ")"), e.querySelectorAll("[id~=" + S + "-]").length || v.push("~="), (t = C.createElement("input")).setAttribute("name", ""), e.appendChild(t), e.querySelectorAll("[name='']").length || v.push("\\[" + M + "*name" + M + "*=" + M + "*(?:''|\"\")"), e.querySelectorAll(":checked").length || v.push(":checked"), e.querySelectorAll("a#" + S + "+*").length || v.push(".#.+[+~]"), e.querySelectorAll("\\\f"), v.push("[\\r\\n\\f]")
						}), ce(function(e) {
							e.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
							var t = C.createElement("input");
							t.setAttribute("type", "hidden"), e.appendChild(t).setAttribute("name", "D"), e.querySelectorAll("[name=d]").length && v.push("name" + M + "*[*^$|!~]?="), 2 !== e.querySelectorAll(":enabled").length && v.push(":enabled", ":disabled"), a.appendChild(e).disabled = !0, 2 !== e.querySelectorAll(":disabled").length && v.push(":enabled", ":disabled"), e.querySelectorAll("*,:x"), v.push(",.*:")
						})), (d.matchesSelector = K.test(c = a.matches || a.webkitMatchesSelector || a.mozMatchesSelector || a.oMatchesSelector || a.msMatchesSelector)) && ce(function(e) {
							d.disconnectedMatch = c.call(e, "*"), c.call(e, "[s!='']:x"), s.push("!=", F)
						}), v = v.length && new RegExp(v.join("|")), s = s.length && new RegExp(s.join("|")), t = K.test(a.compareDocumentPosition), y = t || K.test(a.contains) ? function(e, t) {
							var n = 9 === e.nodeType ? e.documentElement : e,
								r = t && t.parentNode;
							return e === r || !(!r || 1 !== r.nodeType || !(n.contains ? n.contains(r) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(r)))
						} : function(e, t) {
							if (t)
								while (t = t.parentNode)
									if (t === e) return !0;
							return !1
						}, D = t ? function(e, t) {
							if (e === t) return l = !0, 0;
							var n = !e.compareDocumentPosition - !t.compareDocumentPosition;
							return n || (1 & (n = (e.ownerDocument || e) == (t.ownerDocument || t) ? e.compareDocumentPosition(t) : 1) || !d.sortDetached && t.compareDocumentPosition(e) === n ? e == C || e.ownerDocument == p && y(p, e) ? -1 : t == C || t.ownerDocument == p && y(p, t) ? 1 : u ? P(u, e) - P(u, t) : 0 : 4 & n ? -1 : 1)
						} : function(e, t) {
							if (e === t) return l = !0, 0;
							var n, r = 0,
								i = e.parentNode,
								o = t.parentNode,
								a = [e],
								s = [t];
							if (!i || !o) return e == C ? -1 : t == C ? 1 : i ? -1 : o ? 1 : u ? P(u, e) - P(u, t) : 0;
							if (i === o) return pe(e, t);
							n = e;
							while (n = n.parentNode) a.unshift(n);
							n = t;
							while (n = n.parentNode) s.unshift(n);
							while (a[r] === s[r]) r++;
							return r ? pe(a[r], s[r]) : a[r] == p ? -1 : s[r] == p ? 1 : 0
						}), C
					}, se.matches = function(e, t) {
						return se(e, null, null, t)
					}, se.matchesSelector = function(e, t) {
						if (T(e), d.matchesSelector && E && !N[t + " "] && (!s || !s.test(t)) && (!v || !v.test(t))) try {
							var n = c.call(e, t);
							if (n || d.disconnectedMatch || e.document && 11 !== e.document.nodeType) return n
						} catch (e) {
							N(t, !0)
						}
						return 0 < se(t, C, null, [e]).length
					}, se.contains = function(e, t) {
						return (e.ownerDocument || e) != C && T(e), y(e, t)
					}, se.attr = function(e, t) {
						(e.ownerDocument || e) != C && T(e);
						var n = b.attrHandle[t.toLowerCase()],
							r = n && j.call(b.attrHandle, t.toLowerCase()) ? n(e, t, !E) : void 0;
						return void 0 !== r ? r : d.attributes || !E ? e.getAttribute(t) : (r = e.getAttributeNode(t)) && r.specified ? r.value : null
					}, se.escape = function(e) {
						return (e + "").replace(re, ie)
					}, se.error = function(e) {
						throw new Error("Syntax error, unrecognized expression: " + e)
					}, se.uniqueSort = function(e) {
						var t, n = [],
							r = 0,
							i = 0;
						if (l = !d.detectDuplicates, u = !d.sortStable && e.slice(0), e.sort(D), l) {
							while (t = e[i++]) t === e[i] && (r = n.push(i));
							while (r--) e.splice(n[r], 1)
						}
						return u = null, e
					}, o = se.getText = function(e) {
						var t, n = "",
							r = 0,
							i = e.nodeType;
						if (i) {
							if (1 === i || 9 === i || 11 === i) {
								if ("string" == typeof e.textContent) return e.textContent;
								for (e = e.firstChild; e; e = e.nextSibling) n += o(e)
							} else if (3 === i || 4 === i) return e.nodeValue
						} else
							while (t = e[r++]) n += o(t);
						return n
					}, (b = se.selectors = {
						cacheLength: 50,
						createPseudo: le,
						match: G,
						attrHandle: {},
						find: {},
						relative: {
							">": {
								dir: "parentNode",
								first: !0
							},
							" ": {
								dir: "parentNode"
							},
							"+": {
								dir: "previousSibling",
								first: !0
							},
							"~": {
								dir: "previousSibling"
							}
						},
						preFilter: {
							ATTR: function(e) {
								return e[1] = e[1].replace(te, ne), e[3] = (e[3] || e[4] || e[5] || "").replace(te, ne), "~=" === e[2] && (e[3] = " " + e[3] + " "), e.slice(0, 4)
							},
							CHILD: function(e) {
								return e[1] = e[1].toLowerCase(), "nth" === e[1].slice(0, 3) ? (e[3] || se.error(e[0]), e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])), e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && se.error(e[0]), e
							},
							PSEUDO: function(e) {
								var t, n = !e[6] && e[2];
								return G.CHILD.test(e[0]) ? null : (e[3] ? e[2] = e[4] || e[5] || "" : n && X.test(n) && (t = h(n, !0)) && (t = n.indexOf(")", n.length - t) - n.length) && (e[0] = e[0].slice(0, t), e[2] = n.slice(0, t)), e.slice(0, 3))
							}
						},
						filter: {
							TAG: function(e) {
								var t = e.replace(te, ne).toLowerCase();
								return "*" === e ? function() {
									return !0
								} : function(e) {
									return e.nodeName && e.nodeName.toLowerCase() === t
								}
							},
							CLASS: function(e) {
								var t = m[e + " "];
								return t || (t = new RegExp("(^|" + M + ")" + e + "(" + M + "|$)")) && m(e, function(e) {
									return t.test("string" == typeof e.className && e.className || "undefined" != typeof e.getAttribute && e.getAttribute("class") || "")
								})
							},
							ATTR: function(n, r, i) {
								return function(e) {
									var t = se.attr(e, n);
									return null == t ? "!=" === r : !r || (t += "", "=" === r ? t === i : "!=" === r ? t !== i : "^=" === r ? i && 0 === t.indexOf(i) : "*=" === r ? i && -1 < t.indexOf(i) : "$=" === r ? i && t.slice(-i.length) === i : "~=" === r ? -1 < (" " + t.replace(B, " ") + " ").indexOf(i) : "|=" === r && (t === i || t.slice(0, i.length + 1) === i + "-"))
								}
							},
							CHILD: function(h, e, t, g, v) {
								var y = "nth" !== h.slice(0, 3),
									m = "last" !== h.slice(-4),
									x = "of-type" === e;
								return 1 === g && 0 === v ? function(e) {
									return !!e.parentNode
								} : function(e, t, n) {
									var r, i, o, a, s, u, l = y !== m ? "nextSibling" : "previousSibling",
										c = e.parentNode,
										f = x && e.nodeName.toLowerCase(),
										p = !n && !x,
										d = !1;
									if (c) {
										if (y) {
											while (l) {
												a = e;
												while (a = a[l])
													if (x ? a.nodeName.toLowerCase() === f : 1 === a.nodeType) return !1;
												u = l = "only" === h && !u && "nextSibling"
											}
											return !0
										}
										if (u = [m ? c.firstChild : c.lastChild], m && p) {
											d = (s = (r = (i = (o = (a = c)[S] || (a[S] = {}))[a.uniqueID] || (o[a.uniqueID] = {}))[h] || [])[0] === k && r[1]) && r[2], a = s && c.childNodes[s];
											while (a = ++s && a && a[l] || (d = s = 0) || u.pop())
												if (1 === a.nodeType && ++d && a === e) {
													i[h] = [k, s, d];
													break
												}
										} else if (p && (d = s = (r = (i = (o = (a = e)[S] || (a[S] = {}))[a.uniqueID] || (o[a.uniqueID] = {}))[h] || [])[0] === k && r[1]), !1 === d)
											while (a = ++s && a && a[l] || (d = s = 0) || u.pop())
												if ((x ? a.nodeName.toLowerCase() === f : 1 === a.nodeType) && ++d && (p && ((i = (o = a[S] || (a[S] = {}))[a.uniqueID] || (o[a.uniqueID] = {}))[h] = [k, d]), a === e)) break;
										return (d -= v) === g || d % g == 0 && 0 <= d / g
									}
								}
							},
							PSEUDO: function(e, o) {
								var t, a = b.pseudos[e] || b.setFilters[e.toLowerCase()] || se.error("unsupported pseudo: " + e);
								return a[S] ? a(o) : 1 < a.length ? (t = [e, e, "", o], b.setFilters.hasOwnProperty(e.toLowerCase()) ? le(function(e, t) {
									var n, r = a(e, o),
										i = r.length;
									while (i--) e[n = P(e, r[i])] = !(t[n] = r[i])
								}) : function(e) {
									return a(e, 0, t)
								}) : a
							}
						},
						pseudos: {
							not: le(function(e) {
								var r = [],
									i = [],
									s = f(e.replace($, "$1"));
								return s[S] ? le(function(e, t, n, r) {
									var i, o = s(e, null, r, []),
										a = e.length;
									while (a--)(i = o[a]) && (e[a] = !(t[a] = i))
								}) : function(e, t, n) {
									return r[0] = e, s(r, null, n, i), r[0] = null, !i.pop()
								}
							}),
							has: le(function(t) {
								return function(e) {
									return 0 < se(t, e).length
								}
							}),
							contains: le(function(t) {
								return t = t.replace(te, ne),
									function(e) {
										return -1 < (e.textContent || o(e)).indexOf(t)
									}
							}),
							lang: le(function(n) {
								return V.test(n || "") || se.error("unsupported lang: " + n), n = n.replace(te, ne).toLowerCase(),
									function(e) {
										var t;
										do {
											if (t = E ? e.lang : e.getAttribute("xml:lang") || e.getAttribute("lang")) return (t = t.toLowerCase()) === n || 0 === t.indexOf(n + "-")
										} while ((e = e.parentNode) && 1 === e.nodeType);
										return !1
									}
							}),
							target: function(e) {
								var t = n.location && n.location.hash;
								return t && t.slice(1) === e.id
							},
							root: function(e) {
								return e === a
							},
							focus: function(e) {
								return e === C.activeElement && (!C.hasFocus || C.hasFocus()) && !!(e.type || e.href || ~e.tabIndex)
							},
							enabled: ge(!1),
							disabled: ge(!0),
							checked: function(e) {
								var t = e.nodeName.toLowerCase();
								return "input" === t && !!e.checked || "option" === t && !!e.selected
							},
							selected: function(e) {
								return e.parentNode && e.parentNode.selectedIndex, !0 === e.selected
							},
							empty: function(e) {
								for (e = e.firstChild; e; e = e.nextSibling)
									if (e.nodeType < 6) return !1;
								return !0
							},
							parent: function(e) {
								return !b.pseudos.empty(e)
							},
							header: function(e) {
								return J.test(e.nodeName)
							},
							input: function(e) {
								return Q.test(e.nodeName)
							},
							button: function(e) {
								var t = e.nodeName.toLowerCase();
								return "input" === t && "button" === e.type || "button" === t
							},
							text: function(e) {
								var t;
								return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || "text" === t.toLowerCase())
							},
							first: ve(function() {
								return [0]
							}),
							last: ve(function(e, t) {
								return [t - 1]
							}),
							eq: ve(function(e, t, n) {
								return [n < 0 ? n + t : n]
							}),
							even: ve(function(e, t) {
								for (var n = 0; n < t; n += 2) e.push(n);
								return e
							}),
							odd: ve(function(e, t) {
								for (var n = 1; n < t; n += 2) e.push(n);
								return e
							}),
							lt: ve(function(e, t, n) {
								for (var r = n < 0 ? n + t : t < n ? t : n; 0 <= --r;) e.push(r);
								return e
							}),
							gt: ve(function(e, t, n) {
								for (var r = n < 0 ? n + t : n; ++r < t;) e.push(r);
								return e
							})
						}
					}).pseudos.nth = b.pseudos.eq, {
						radio: !0,
						checkbox: !0,
						file: !0,
						password: !0,
						image: !0
					}) b.pseudos[e] = de(e);
				for (e in {
						submit: !0,
						reset: !0
					}) b.pseudos[e] = he(e);

				function me() {}

				function xe(e) {
					for (var t = 0, n = e.length, r = ""; t < n; t++) r += e[t].value;
					return r
				}

				function be(s, e, t) {
					var u = e.dir,
						l = e.next,
						c = l || u,
						f = t && "parentNode" === c,
						p = r++;
					return e.first ? function(e, t, n) {
						while (e = e[u])
							if (1 === e.nodeType || f) return s(e, t, n);
						return !1
					} : function(e, t, n) {
						var r, i, o, a = [k, p];
						if (n) {
							while (e = e[u])
								if ((1 === e.nodeType || f) && s(e, t, n)) return !0
						} else
							while (e = e[u])
								if (1 === e.nodeType || f)
									if (i = (o = e[S] || (e[S] = {}))[e.uniqueID] || (o[e.uniqueID] = {}), l && l === e.nodeName.toLowerCase()) e = e[u] || e;
									else {
										if ((r = i[c]) && r[0] === k && r[1] === p) return a[2] = r[2];
										if ((i[c] = a)[2] = s(e, t, n)) return !0
									} return !1
					}
				}

				function we(i) {
					return 1 < i.length ? function(e, t, n) {
						var r = i.length;
						while (r--)
							if (!i[r](e, t, n)) return !1;
						return !0
					} : i[0]
				}

				function Te(e, t, n, r, i) {
					for (var o, a = [], s = 0, u = e.length, l = null != t; s < u; s++)(o = e[s]) && (n && !n(o, r, i) || (a.push(o), l && t.push(s)));
					return a
				}

				function Ce(d, h, g, v, y, e) {
					return v && !v[S] && (v = Ce(v)), y && !y[S] && (y = Ce(y, e)), le(function(e, t, n, r) {
						var i, o, a, s = [],
							u = [],
							l = t.length,
							c = e || function(e, t, n) {
								for (var r = 0, i = t.length; r < i; r++) se(e, t[r], n);
								return n
							}(h || "*", n.nodeType ? [n] : n, []),
							f = !d || !e && h ? c : Te(c, s, d, n, r),
							p = g ? y || (e ? d : l || v) ? [] : t : f;
						if (g && g(f, p, n, r), v) {
							i = Te(p, u), v(i, [], n, r), o = i.length;
							while (o--)(a = i[o]) && (p[u[o]] = !(f[u[o]] = a))
						}
						if (e) {
							if (y || d) {
								if (y) {
									i = [], o = p.length;
									while (o--)(a = p[o]) && i.push(f[o] = a);
									y(null, p = [], i, r)
								}
								o = p.length;
								while (o--)(a = p[o]) && -1 < (i = y ? P(e, a) : s[o]) && (e[i] = !(t[i] = a))
							}
						} else p = Te(p === t ? p.splice(l, p.length) : p), y ? y(null, t, p, r) : H.apply(t, p)
					})
				}

				function Ee(e) {
					for (var i, t, n, r = e.length, o = b.relative[e[0].type], a = o || b.relative[" "], s = o ? 1 : 0, u = be(function(e) {
							return e === i
						}, a, !0), l = be(function(e) {
							return -1 < P(i, e)
						}, a, !0), c = [function(e, t, n) {
							var r = !o && (n || t !== w) || ((i = t).nodeType ? u(e, t, n) : l(e, t, n));
							return i = null, r
						}]; s < r; s++)
						if (t = b.relative[e[s].type]) c = [be(we(c), t)];
						else {
							if ((t = b.filter[e[s].type].apply(null, e[s].matches))[S]) {
								for (n = ++s; n < r; n++)
									if (b.relative[e[n].type]) break;
								return Ce(1 < s && we(c), 1 < s && xe(e.slice(0, s - 1).concat({
									value: " " === e[s - 2].type ? "*" : ""
								})).replace($, "$1"), t, s < n && Ee(e.slice(s, n)), n < r && Ee(e = e.slice(n)), n < r && xe(e))
							}
							c.push(t)
						}
					return we(c)
				}
				return me.prototype = b.filters = b.pseudos, b.setFilters = new me, h = se.tokenize = function(e, t) {
					var n, r, i, o, a, s, u, l = x[e + " "];
					if (l) return t ? 0 : l.slice(0);
					a = e, s = [], u = b.preFilter;
					while (a) {
						for (o in n && !(r = _.exec(a)) || (r && (a = a.slice(r[0].length) || a), s.push(i = [])), n = !1, (r = z.exec(a)) && (n = r.shift(), i.push({
								value: n,
								type: r[0].replace($, " ")
							}), a = a.slice(n.length)), b.filter) !(r = G[o].exec(a)) || u[o] && !(r = u[o](r)) || (n = r.shift(), i.push({
							value: n,
							type: o,
							matches: r
						}), a = a.slice(n.length));
						if (!n) break
					}
					return t ? a.length : a ? se.error(e) : x(e, s).slice(0)
				}, f = se.compile = function(e, t) {
					var n, v, y, m, x, r, i = [],
						o = [],
						a = A[e + " "];
					if (!a) {
						t || (t = h(e)), n = t.length;
						while (n--)(a = Ee(t[n]))[S] ? i.push(a) : o.push(a);
						(a = A(e, (v = o, m = 0 < (y = i).length, x = 0 < v.length, r = function(e, t, n, r, i) {
							var o, a, s, u = 0,
								l = "0",
								c = e && [],
								f = [],
								p = w,
								d = e || x && b.find.TAG("*", i),
								h = k += null == p ? 1 : Math.random() || .1,
								g = d.length;
							for (i && (w = t == C || t || i); l !== g && null != (o = d[l]); l++) {
								if (x && o) {
									a = 0, t || o.ownerDocument == C || (T(o), n = !E);
									while (s = v[a++])
										if (s(o, t || C, n)) {
											r.push(o);
											break
										}
									i && (k = h)
								}
								m && ((o = !s && o) && u--, e && c.push(o))
							}
							if (u += l, m && l !== u) {
								a = 0;
								while (s = y[a++]) s(c, f, t, n);
								if (e) {
									if (0 < u)
										while (l--) c[l] || f[l] || (f[l] = q.call(r));
									f = Te(f)
								}
								H.apply(r, f), i && !e && 0 < f.length && 1 < u + y.length && se.uniqueSort(r)
							}
							return i && (k = h, w = p), c
						}, m ? le(r) : r))).selector = e
					}
					return a
				}, g = se.select = function(e, t, n, r) {
					var i, o, a, s, u, l = "function" == typeof e && e,
						c = !r && h(e = l.selector || e);
					if (n = n || [], 1 === c.length) {
						if (2 < (o = c[0] = c[0].slice(0)).length && "ID" === (a = o[0]).type && 9 === t.nodeType && E && b.relative[o[1].type]) {
							if (!(t = (b.find.ID(a.matches[0].replace(te, ne), t) || [])[0])) return n;
							l && (t = t.parentNode), e = e.slice(o.shift().value.length)
						}
						i = G.needsContext.test(e) ? 0 : o.length;
						while (i--) {
							if (a = o[i], b.relative[s = a.type]) break;
							if ((u = b.find[s]) && (r = u(a.matches[0].replace(te, ne), ee.test(o[0].type) && ye(t.parentNode) || t))) {
								if (o.splice(i, 1), !(e = r.length && xe(o))) return H.apply(n, r), n;
								break
							}
						}
					}
					return (l || f(e, c))(r, t, !E, n, !t || ee.test(e) && ye(t.parentNode) || t), n
				}, d.sortStable = S.split("").sort(D).join("") === S, d.detectDuplicates = !!l, T(), d.sortDetached = ce(function(e) {
					return 1 & e.compareDocumentPosition(C.createElement("fieldset"))
				}), ce(function(e) {
					return e.innerHTML = "<a href='#'></a>", "#" === e.firstChild.getAttribute("href")
				}) || fe("type|href|height|width", function(e, t, n) {
					if (!n) return e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2)
				}), d.attributes && ce(function(e) {
					return e.innerHTML = "<input/>", e.firstChild.setAttribute("value", ""), "" === e.firstChild.getAttribute("value")
				}) || fe("value", function(e, t, n) {
					if (!n && "input" === e.nodeName.toLowerCase()) return e.defaultValue
				}), ce(function(e) {
					return null == e.getAttribute("disabled")
				}) || fe(R, function(e, t, n) {
					var r;
					if (!n) return !0 === e[t] ? t.toLowerCase() : (r = e.getAttributeNode(t)) && r.specified ? r.value : null
				}), se
			}(C);
			S.find = d, S.expr = d.selectors, S.expr[":"] = S.expr.pseudos, S.uniqueSort = S.unique = d.uniqueSort, S.text = d.getText, S.isXMLDoc = d.isXML, S.contains = d.contains, S.escapeSelector = d.escape;
			var h = function(e, t, n) {
					var r = [],
						i = void 0 !== n;
					while ((e = e[t]) && 9 !== e.nodeType)
						if (1 === e.nodeType) {
							if (i && S(e).is(n)) break;
							r.push(e)
						}
					return r
				},
				T = function(e, t) {
					for (var n = []; e; e = e.nextSibling) 1 === e.nodeType && e !== t && n.push(e);
					return n
				},
				k = S.expr.match.needsContext;

			function A(e, t) {
				return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase()
			}
			var N = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i;

			function D(e, n, r) {
				return m(n) ? S.grep(e, function(e, t) {
					return !!n.call(e, t, e) !== r
				}) : n.nodeType ? S.grep(e, function(e) {
					return e === n !== r
				}) : "string" != typeof n ? S.grep(e, function(e) {
					return -1 < i.call(n, e) !== r
				}) : S.filter(n, e, r)
			}
			S.filter = function(e, t, n) {
				var r = t[0];
				return n && (e = ":not(" + e + ")"), 1 === t.length && 1 === r.nodeType ? S.find.matchesSelector(r, e) ? [r] : [] : S.find.matches(e, S.grep(t, function(e) {
					return 1 === e.nodeType
				}))
			}, S.fn.extend({
				find: function(e) {
					var t, n, r = this.length,
						i = this;
					if ("string" != typeof e) return this.pushStack(S(e).filter(function() {
						for (t = 0; t < r; t++)
							if (S.contains(i[t], this)) return !0
					}));
					for (n = this.pushStack([]), t = 0; t < r; t++) S.find(e, i[t], n);
					return 1 < r ? S.uniqueSort(n) : n
				},
				filter: function(e) {
					return this.pushStack(D(this, e || [], !1))
				},
				not: function(e) {
					return this.pushStack(D(this, e || [], !0))
				},
				is: function(e) {
					return !!D(this, "string" == typeof e && k.test(e) ? S(e) : e || [], !1).length
				}
			});
			var j, q = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;
			(S.fn.init = function(e, t, n) {
				var r, i;
				if (!e) return this;
				if (n = n || j, "string" == typeof e) {
					if (!(r = "<" === e[0] && ">" === e[e.length - 1] && 3 <= e.length ? [null, e, null] : q.exec(e)) || !r[1] && t) return !t || t.jquery ? (t || n).find(e) : this.constructor(t).find(e);
					if (r[1]) {
						if (t = t instanceof S ? t[0] : t, S.merge(this, S.parseHTML(r[1], t && t.nodeType ? t.ownerDocument || t : E, !0)), N.test(r[1]) && S.isPlainObject(t))
							for (r in t) m(this[r]) ? this[r](t[r]) : this.attr(r, t[r]);
						return this
					}
					return (i = E.getElementById(r[2])) && (this[0] = i, this.length = 1), this
				}
				return e.nodeType ? (this[0] = e, this.length = 1, this) : m(e) ? void 0 !== n.ready ? n.ready(e) : e(S) : S.makeArray(e, this)
			}).prototype = S.fn, j = S(E);
			var L = /^(?:parents|prev(?:Until|All))/,
				H = {
					children: !0,
					contents: !0,
					next: !0,
					prev: !0
				};

			function O(e, t) {
				while ((e = e[t]) && 1 !== e.nodeType);
				return e
			}
			S.fn.extend({
				has: function(e) {
					var t = S(e, this),
						n = t.length;
					return this.filter(function() {
						for (var e = 0; e < n; e++)
							if (S.contains(this, t[e])) return !0
					})
				},
				closest: function(e, t) {
					var n, r = 0,
						i = this.length,
						o = [],
						a = "string" != typeof e && S(e);
					if (!k.test(e))
						for (; r < i; r++)
							for (n = this[r]; n && n !== t; n = n.parentNode)
								if (n.nodeType < 11 && (a ? -1 < a.index(n) : 1 === n.nodeType && S.find.matchesSelector(n, e))) {
									o.push(n);
									break
								}
					return this.pushStack(1 < o.length ? S.uniqueSort(o) : o)
				},
				index: function(e) {
					return e ? "string" == typeof e ? i.call(S(e), this[0]) : i.call(this, e.jquery ? e[0] : e) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
				},
				add: function(e, t) {
					return this.pushStack(S.uniqueSort(S.merge(this.get(), S(e, t))))
				},
				addBack: function(e) {
					return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
				}
			}), S.each({
				parent: function(e) {
					var t = e.parentNode;
					return t && 11 !== t.nodeType ? t : null
				},
				parents: function(e) {
					return h(e, "parentNode")
				},
				parentsUntil: function(e, t, n) {
					return h(e, "parentNode", n)
				},
				next: function(e) {
					return O(e, "nextSibling")
				},
				prev: function(e) {
					return O(e, "previousSibling")
				},
				nextAll: function(e) {
					return h(e, "nextSibling")
				},
				prevAll: function(e) {
					return h(e, "previousSibling")
				},
				nextUntil: function(e, t, n) {
					return h(e, "nextSibling", n)
				},
				prevUntil: function(e, t, n) {
					return h(e, "previousSibling", n)
				},
				siblings: function(e) {
					return T((e.parentNode || {}).firstChild, e)
				},
				children: function(e) {
					return T(e.firstChild)
				},
				contents: function(e) {
					return null != e.contentDocument && r(e.contentDocument) ? e.contentDocument : (A(e, "template") && (e = e.content || e), S.merge([], e.childNodes))
				}
			}, function(r, i) {
				S.fn[r] = function(e, t) {
					var n = S.map(this, i, e);
					return "Until" !== r.slice(-5) && (t = e), t && "string" == typeof t && (n = S.filter(t, n)), 1 < this.length && (H[r] || S.uniqueSort(n), L.test(r) && n.reverse()), this.pushStack(n)
				}
			});
			var P = /[^\x20\t\r\n\f]+/g;

			function R(e) {
				return e
			}

			function M(e) {
				throw e
			}

			function I(e, t, n, r) {
				var i;
				try {
					e && m(i = e.promise) ? i.call(e).done(t).fail(n) : e && m(i = e.then) ? i.call(e, t, n) : t.apply(void 0, [e].slice(r))
				} catch (e) {
					n.apply(void 0, [e])
				}
			}
			S.Callbacks = function(r) {
				var e, n;
				r = "string" == typeof r ? (e = r, n = {}, S.each(e.match(P) || [], function(e, t) {
					n[t] = !0
				}), n) : S.extend({}, r);
				var i, t, o, a, s = [],
					u = [],
					l = -1,
					c = function() {
						for (a = a || r.once, o = i = !0; u.length; l = -1) {
							t = u.shift();
							while (++l < s.length) !1 === s[l].apply(t[0], t[1]) && r.stopOnFalse && (l = s.length, t = !1)
						}
						r.memory || (t = !1), i = !1, a && (s = t ? [] : "")
					},
					f = {
						add: function() {
							return s && (t && !i && (l = s.length - 1, u.push(t)), function n(e) {
								S.each(e, function(e, t) {
									m(t) ? r.unique && f.has(t) || s.push(t) : t && t.length && "string" !== w(t) && n(t)
								})
							}(arguments), t && !i && c()), this
						},
						remove: function() {
							return S.each(arguments, function(e, t) {
								var n;
								while (-1 < (n = S.inArray(t, s, n))) s.splice(n, 1), n <= l && l--
							}), this
						},
						has: function(e) {
							return e ? -1 < S.inArray(e, s) : 0 < s.length
						},
						empty: function() {
							return s && (s = []), this
						},
						disable: function() {
							return a = u = [], s = t = "", this
						},
						disabled: function() {
							return !s
						},
						lock: function() {
							return a = u = [], t || i || (s = t = ""), this
						},
						locked: function() {
							return !!a
						},
						fireWith: function(e, t) {
							return a || (t = [e, (t = t || []).slice ? t.slice() : t], u.push(t), i || c()), this
						},
						fire: function() {
							return f.fireWith(this, arguments), this
						},
						fired: function() {
							return !!o
						}
					};
				return f
			}, S.extend({
				Deferred: function(e) {
					var o = [
							["notify", "progress", S.Callbacks("memory"), S.Callbacks("memory"), 2],
							["resolve", "done", S.Callbacks("once memory"), S.Callbacks("once memory"), 0, "resolved"],
							["reject", "fail", S.Callbacks("once memory"), S.Callbacks("once memory"), 1, "rejected"]
						],
						i = "pending",
						a = {
							state: function() {
								return i
							},
							always: function() {
								return s.done(arguments).fail(arguments), this
							},
							"catch": function(e) {
								return a.then(null, e)
							},
							pipe: function() {
								var i = arguments;
								return S.Deferred(function(r) {
									S.each(o, function(e, t) {
										var n = m(i[t[4]]) && i[t[4]];
										s[t[1]](function() {
											var e = n && n.apply(this, arguments);
											e && m(e.promise) ? e.promise().progress(r.notify).done(r.resolve).fail(r.reject) : r[t[0] + "With"](this, n ? [e] : arguments)
										})
									}), i = null
								}).promise()
							},
							then: function(t, n, r) {
								var u = 0;

								function l(i, o, a, s) {
									return function() {
										var n = this,
											r = arguments,
											e = function() {
												var e, t;
												if (!(i < u)) {
													if ((e = a.apply(n, r)) === o.promise()) throw new TypeError("Thenable self-resolution");
													t = e && ("object" == typeof e || "function" == typeof e) && e.then, m(t) ? s ? t.call(e, l(u, o, R, s), l(u, o, M, s)) : (u++, t.call(e, l(u, o, R, s), l(u, o, M, s), l(u, o, R, o.notifyWith))) : (a !== R && (n = void 0, r = [e]), (s || o.resolveWith)(n, r))
												}
											},
											t = s ? e : function() {
												try {
													e()
												} catch (e) {
													S.Deferred.exceptionHook && S.Deferred.exceptionHook(e, t.stackTrace), u <= i + 1 && (a !== M && (n = void 0, r = [e]), o.rejectWith(n, r))
												}
											};
										i ? t() : (S.Deferred.getStackHook && (t.stackTrace = S.Deferred.getStackHook()), C.setTimeout(t))
									}
								}
								return S.Deferred(function(e) {
									o[0][3].add(l(0, e, m(r) ? r : R, e.notifyWith)), o[1][3].add(l(0, e, m(t) ? t : R)), o[2][3].add(l(0, e, m(n) ? n : M))
								}).promise()
							},
							promise: function(e) {
								return null != e ? S.extend(e, a) : a
							}
						},
						s = {};
					return S.each(o, function(e, t) {
						var n = t[2],
							r = t[5];
						a[t[1]] = n.add, r && n.add(function() {
							i = r
						}, o[3 - e][2].disable, o[3 - e][3].disable, o[0][2].lock, o[0][3].lock), n.add(t[3].fire), s[t[0]] = function() {
							return s[t[0] + "With"](this === s ? void 0 : this, arguments), this
						}, s[t[0] + "With"] = n.fireWith
					}), a.promise(s), e && e.call(s, s), s
				},
				when: function(e) {
					var n = arguments.length,
						t = n,
						r = Array(t),
						i = s.call(arguments),
						o = S.Deferred(),
						a = function(t) {
							return function(e) {
								r[t] = this, i[t] = 1 < arguments.length ? s.call(arguments) : e, --n || o.resolveWith(r, i)
							}
						};
					if (n <= 1 && (I(e, o.done(a(t)).resolve, o.reject, !n), "pending" === o.state() || m(i[t] && i[t].then))) return o.then();
					while (t--) I(i[t], a(t), o.reject);
					return o.promise()
				}
			});
			var W = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
			S.Deferred.exceptionHook = function(e, t) {
				C.console && C.console.warn && e && W.test(e.name) && C.console.warn("jQuery.Deferred exception: " + e.message, e.stack, t)
			}, S.readyException = function(e) {
				C.setTimeout(function() {
					throw e
				})
			};
			var F = S.Deferred();

			function B() {
				E.removeEventListener("DOMContentLoaded", B), C.removeEventListener("load", B), S.ready()
			}
			S.fn.ready = function(e) {
				return F.then(e)["catch"](function(e) {
					S.readyException(e)
				}), this
			}, S.extend({
				isReady: !1,
				readyWait: 1,
				ready: function(e) {
					(!0 === e ? --S.readyWait : S.isReady) || (S.isReady = !0) !== e && 0 < --S.readyWait || F.resolveWith(E, [S])
				}
			}), S.ready.then = F.then, "complete" === E.readyState || "loading" !== E.readyState && !E.documentElement.doScroll ? C.setTimeout(S.ready) : (E.addEventListener("DOMContentLoaded", B), C.addEventListener("load", B));
			var $ = function(e, t, n, r, i, o, a) {
					var s = 0,
						u = e.length,
						l = null == n;
					if ("object" === w(n))
						for (s in i = !0, n) $(e, t, s, n[s], !0, o, a);
					else if (void 0 !== r && (i = !0, m(r) || (a = !0), l && (a ? (t.call(e, r), t = null) : (l = t, t = function(e, t, n) {
							return l.call(S(e), n)
						})), t))
						for (; s < u; s++) t(e[s], n, a ? r : r.call(e[s], s, t(e[s], n)));
					return i ? e : l ? t.call(e) : u ? t(e[0], n) : o
				},
				_ = /^-ms-/,
				z = /-([a-z])/g;

			function U(e, t) {
				return t.toUpperCase()
			}

			function X(e) {
				return e.replace(_, "ms-").replace(z, U)
			}
			var V = function(e) {
				return 1 === e.nodeType || 9 === e.nodeType || !+e.nodeType
			};

			function G() {
				this.expando = S.expando + G.uid++
			}
			G.uid = 1, G.prototype = {
				cache: function(e) {
					var t = e[this.expando];
					return t || (t = {}, V(e) && (e.nodeType ? e[this.expando] = t : Object.defineProperty(e, this.expando, {
						value: t,
						configurable: !0
					}))), t
				},
				set: function(e, t, n) {
					var r, i = this.cache(e);
					if ("string" == typeof t) i[X(t)] = n;
					else
						for (r in t) i[X(r)] = t[r];
					return i
				},
				get: function(e, t) {
					return void 0 === t ? this.cache(e) : e[this.expando] && e[this.expando][X(t)]
				},
				access: function(e, t, n) {
					return void 0 === t || t && "string" == typeof t && void 0 === n ? this.get(e, t) : (this.set(e, t, n), void 0 !== n ? n : t)
				},
				remove: function(e, t) {
					var n, r = e[this.expando];
					if (void 0 !== r) {
						if (void 0 !== t) {
							n = (t = Array.isArray(t) ? t.map(X) : (t = X(t)) in r ? [t] : t.match(P) || []).length;
							while (n--) delete r[t[n]]
						}(void 0 === t || S.isEmptyObject(r)) && (e.nodeType ? e[this.expando] = void 0 : delete e[this.expando])
					}
				},
				hasData: function(e) {
					var t = e[this.expando];
					return void 0 !== t && !S.isEmptyObject(t)
				}
			};
			var Y = new G,
				Q = new G,
				J = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
				K = /[A-Z]/g;

			function Z(e, t, n) {
				var r, i;
				if (void 0 === n && 1 === e.nodeType)
					if (r = "data-" + t.replace(K, "-$&").toLowerCase(), "string" == typeof(n = e.getAttribute(r))) {
						try {
							n = "true" === (i = n) || "false" !== i && ("null" === i ? null : i === +i + "" ? +i : J.test(i) ? JSON.parse(i) : i)
						} catch (e) {}
						Q.set(e, t, n)
					} else n = void 0;
				return n
			}
			S.extend({
				hasData: function(e) {
					return Q.hasData(e) || Y.hasData(e)
				},
				data: function(e, t, n) {
					return Q.access(e, t, n)
				},
				removeData: function(e, t) {
					Q.remove(e, t)
				},
				_data: function(e, t, n) {
					return Y.access(e, t, n)
				},
				_removeData: function(e, t) {
					Y.remove(e, t)
				}
			}), S.fn.extend({
				data: function(n, e) {
					var t, r, i, o = this[0],
						a = o && o.attributes;
					if (void 0 === n) {
						if (this.length && (i = Q.get(o), 1 === o.nodeType && !Y.get(o, "hasDataAttrs"))) {
							t = a.length;
							while (t--) a[t] && 0 === (r = a[t].name).indexOf("data-") && (r = X(r.slice(5)), Z(o, r, i[r]));
							Y.set(o, "hasDataAttrs", !0)
						}
						return i
					}
					return "object" == typeof n ? this.each(function() {
						Q.set(this, n)
					}) : $(this, function(e) {
						var t;
						if (o && void 0 === e) return void 0 !== (t = Q.get(o, n)) ? t : void 0 !== (t = Z(o, n)) ? t : void 0;
						this.each(function() {
							Q.set(this, n, e)
						})
					}, null, e, 1 < arguments.length, null, !0)
				},
				removeData: function(e) {
					return this.each(function() {
						Q.remove(this, e)
					})
				}
			}), S.extend({
				queue: function(e, t, n) {
					var r;
					if (e) return t = (t || "fx") + "queue", r = Y.get(e, t), n && (!r || Array.isArray(n) ? r = Y.access(e, t, S.makeArray(n)) : r.push(n)), r || []
				},
				dequeue: function(e, t) {
					t = t || "fx";
					var n = S.queue(e, t),
						r = n.length,
						i = n.shift(),
						o = S._queueHooks(e, t);
					"inprogress" === i && (i = n.shift(), r--), i && ("fx" === t && n.unshift("inprogress"), delete o.stop, i.call(e, function() {
						S.dequeue(e, t)
					}, o)), !r && o && o.empty.fire()
				},
				_queueHooks: function(e, t) {
					var n = t + "queueHooks";
					return Y.get(e, n) || Y.access(e, n, {
						empty: S.Callbacks("once memory").add(function() {
							Y.remove(e, [t + "queue", n])
						})
					})
				}
			}), S.fn.extend({
				queue: function(t, n) {
					var e = 2;
					return "string" != typeof t && (n = t, t = "fx", e--), arguments.length < e ? S.queue(this[0], t) : void 0 === n ? this : this.each(function() {
						var e = S.queue(this, t, n);
						S._queueHooks(this, t), "fx" === t && "inprogress" !== e[0] && S.dequeue(this, t)
					})
				},
				dequeue: function(e) {
					return this.each(function() {
						S.dequeue(this, e)
					})
				},
				clearQueue: function(e) {
					return this.queue(e || "fx", [])
				},
				promise: function(e, t) {
					var n, r = 1,
						i = S.Deferred(),
						o = this,
						a = this.length,
						s = function() {
							--r || i.resolveWith(o, [o])
						};
					"string" != typeof e && (t = e, e = void 0), e = e || "fx";
					while (a--)(n = Y.get(o[a], e + "queueHooks")) && n.empty && (r++, n.empty.add(s));
					return s(), i.promise(t)
				}
			});
			var ee = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
				te = new RegExp("^(?:([+-])=|)(" + ee + ")([a-z%]*)$", "i"),
				ne = ["Top", "Right", "Bottom", "Left"],
				re = E.documentElement,
				ie = function(e) {
					return S.contains(e.ownerDocument, e)
				},
				oe = {
					composed: !0
				};
			re.getRootNode && (ie = function(e) {
				return S.contains(e.ownerDocument, e) || e.getRootNode(oe) === e.ownerDocument
			});
			var ae = function(e, t) {
				return "none" === (e = t || e).style.display || "" === e.style.display && ie(e) && "none" === S.css(e, "display")
			};

			function se(e, t, n, r) {
				var i, o, a = 20,
					s = r ? function() {
						return r.cur()
					} : function() {
						return S.css(e, t, "")
					},
					u = s(),
					l = n && n[3] || (S.cssNumber[t] ? "" : "px"),
					c = e.nodeType && (S.cssNumber[t] || "px" !== l && +u) && te.exec(S.css(e, t));
				if (c && c[3] !== l) {
					u /= 2, l = l || c[3], c = +u || 1;
					while (a--) S.style(e, t, c + l), (1 - o) * (1 - (o = s() / u || .5)) <= 0 && (a = 0), c /= o;
					c *= 2, S.style(e, t, c + l), n = n || []
				}
				return n && (c = +c || +u || 0, i = n[1] ? c + (n[1] + 1) * n[2] : +n[2], r && (r.unit = l, r.start = c, r.end = i)), i
			}
			var ue = {};

			function le(e, t) {
				for (var n, r, i, o, a, s, u, l = [], c = 0, f = e.length; c < f; c++)(r = e[c]).style && (n = r.style.display, t ? ("none" === n && (l[c] = Y.get(r, "display") || null, l[c] || (r.style.display = "")), "" === r.style.display && ae(r) && (l[c] = (u = a = o = void 0, a = (i = r).ownerDocument, s = i.nodeName, (u = ue[s]) || (o = a.body.appendChild(a.createElement(s)), u = S.css(o, "display"), o.parentNode.removeChild(o), "none" === u && (u = "block"), ue[s] = u)))) : "none" !== n && (l[c] = "none", Y.set(r, "display", n)));
				for (c = 0; c < f; c++) null != l[c] && (e[c].style.display = l[c]);
				return e
			}
			S.fn.extend({
				show: function() {
					return le(this, !0)
				},
				hide: function() {
					return le(this)
				},
				toggle: function(e) {
					return "boolean" == typeof e ? e ? this.show() : this.hide() : this.each(function() {
						ae(this) ? S(this).show() : S(this).hide()
					})
				}
			});
			var ce, fe, pe = /^(?:checkbox|radio)$/i,
				de = /<([a-z][^\/\0>\x20\t\r\n\f]*)/i,
				he = /^$|^module$|\/(?:java|ecma)script/i;
			ce = E.createDocumentFragment().appendChild(E.createElement("div")), (fe = E.createElement("input")).setAttribute("type", "radio"), fe.setAttribute("checked", "checked"), fe.setAttribute("name", "t"), ce.appendChild(fe), y.checkClone = ce.cloneNode(!0).cloneNode(!0).lastChild.checked, ce.innerHTML = "<textarea>x</textarea>", y.noCloneChecked = !!ce.cloneNode(!0).lastChild.defaultValue, ce.innerHTML = "<option></option>", y.option = !!ce.lastChild;
			var ge = {
				thead: [1, "<table>", "</table>"],
				col: [2, "<table><colgroup>", "</colgroup></table>"],
				tr: [2, "<table><tbody>", "</tbody></table>"],
				td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
				_default: [0, "", ""]
			};

			function ve(e, t) {
				var n;
				return n = "undefined" != typeof e.getElementsByTagName ? e.getElementsByTagName(t || "*") : "undefined" != typeof e.querySelectorAll ? e.querySelectorAll(t || "*") : [], void 0 === t || t && A(e, t) ? S.merge([e], n) : n
			}

			function ye(e, t) {
				for (var n = 0, r = e.length; n < r; n++) Y.set(e[n], "globalEval", !t || Y.get(t[n], "globalEval"))
			}
			ge.tbody = ge.tfoot = ge.colgroup = ge.caption = ge.thead, ge.th = ge.td, y.option || (ge.optgroup = ge.option = [1, "<select multiple='multiple'>", "</select>"]);
			var me = /<|&#?\w+;/;

			function xe(e, t, n, r, i) {
				for (var o, a, s, u, l, c, f = t.createDocumentFragment(), p = [], d = 0, h = e.length; d < h; d++)
					if ((o = e[d]) || 0 === o)
						if ("object" === w(o)) S.merge(p, o.nodeType ? [o] : o);
						else if (me.test(o)) {
					a = a || f.appendChild(t.createElement("div")), s = (de.exec(o) || ["", ""])[1].toLowerCase(), u = ge[s] || ge._default, a.innerHTML = u[1] + S.htmlPrefilter(o) + u[2], c = u[0];
					while (c--) a = a.lastChild;
					S.merge(p, a.childNodes), (a = f.firstChild).textContent = ""
				} else p.push(t.createTextNode(o));
				f.textContent = "", d = 0;
				while (o = p[d++])
					if (r && -1 < S.inArray(o, r)) i && i.push(o);
					else if (l = ie(o), a = ve(f.appendChild(o), "script"), l && ye(a), n) {
					c = 0;
					while (o = a[c++]) he.test(o.type || "") && n.push(o)
				}
				return f
			}
			var be = /^key/,
				we = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
				Te = /^([^.]*)(?:\.(.+)|)/;

			function Ce() {
				return !0
			}

			function Ee() {
				return !1
			}

			function Se(e, t) {
				return e === function() {
					try {
						return E.activeElement
					} catch (e) {}
				}() == ("focus" === t)
			}

			function ke(e, t, n, r, i, o) {
				var a, s;
				if ("object" == typeof t) {
					for (s in "string" != typeof n && (r = r || n, n = void 0), t) ke(e, s, n, r, t[s], o);
					return e
				}
				if (null == r && null == i ? (i = n, r = n = void 0) : null == i && ("string" == typeof n ? (i = r, r = void 0) : (i = r, r = n, n = void 0)), !1 === i) i = Ee;
				else if (!i) return e;
				return 1 === o && (a = i, (i = function(e) {
					return S().off(e), a.apply(this, arguments)
				}).guid = a.guid || (a.guid = S.guid++)), e.each(function() {
					S.event.add(this, t, i, r, n)
				})
			}

			function Ae(e, i, o) {
				o ? (Y.set(e, i, !1), S.event.add(e, i, {
					namespace: !1,
					handler: function(e) {
						var t, n, r = Y.get(this, i);
						if (1 & e.isTrigger && this[i]) {
							if (r.length)(S.event.special[i] || {}).delegateType && e.stopPropagation();
							else if (r = s.call(arguments), Y.set(this, i, r), t = o(this, i), this[i](), r !== (n = Y.get(this, i)) || t ? Y.set(this, i, !1) : n = {}, r !== n) return e.stopImmediatePropagation(), e.preventDefault(), n.value
						} else r.length && (Y.set(this, i, {
							value: S.event.trigger(S.extend(r[0], S.Event.prototype), r.slice(1), this)
						}), e.stopImmediatePropagation())
					}
				})) : void 0 === Y.get(e, i) && S.event.add(e, i, Ce)
			}
			S.event = {
				global: {},
				add: function(t, e, n, r, i) {
					var o, a, s, u, l, c, f, p, d, h, g, v = Y.get(t);
					if (V(t)) {
						n.handler && (n = (o = n).handler, i = o.selector), i && S.find.matchesSelector(re, i), n.guid || (n.guid = S.guid++), (u = v.events) || (u = v.events = Object.create(null)), (a = v.handle) || (a = v.handle = function(e) {
							return "undefined" != typeof S && S.event.triggered !== e.type ? S.event.dispatch.apply(t, arguments) : void 0
						}), l = (e = (e || "").match(P) || [""]).length;
						while (l--) d = g = (s = Te.exec(e[l]) || [])[1], h = (s[2] || "").split(".").sort(), d && (f = S.event.special[d] || {}, d = (i ? f.delegateType : f.bindType) || d, f = S.event.special[d] || {}, c = S.extend({
							type: d,
							origType: g,
							data: r,
							handler: n,
							guid: n.guid,
							selector: i,
							needsContext: i && S.expr.match.needsContext.test(i),
							namespace: h.join(".")
						}, o), (p = u[d]) || ((p = u[d] = []).delegateCount = 0, f.setup && !1 !== f.setup.call(t, r, h, a) || t.addEventListener && t.addEventListener(d, a)), f.add && (f.add.call(t, c), c.handler.guid || (c.handler.guid = n.guid)), i ? p.splice(p.delegateCount++, 0, c) : p.push(c), S.event.global[d] = !0)
					}
				},
				remove: function(e, t, n, r, i) {
					var o, a, s, u, l, c, f, p, d, h, g, v = Y.hasData(e) && Y.get(e);
					if (v && (u = v.events)) {
						l = (t = (t || "").match(P) || [""]).length;
						while (l--)
							if (d = g = (s = Te.exec(t[l]) || [])[1], h = (s[2] || "").split(".").sort(), d) {
								f = S.event.special[d] || {}, p = u[d = (r ? f.delegateType : f.bindType) || d] || [], s = s[2] && new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)"), a = o = p.length;
								while (o--) c = p[o], !i && g !== c.origType || n && n.guid !== c.guid || s && !s.test(c.namespace) || r && r !== c.selector && ("**" !== r || !c.selector) || (p.splice(o, 1), c.selector && p.delegateCount--, f.remove && f.remove.call(e, c));
								a && !p.length && (f.teardown && !1 !== f.teardown.call(e, h, v.handle) || S.removeEvent(e, d, v.handle), delete u[d])
							} else
								for (d in u) S.event.remove(e, d + t[l], n, r, !0);
						S.isEmptyObject(u) && Y.remove(e, "handle events")
					}
				},
				dispatch: function(e) {
					var t, n, r, i, o, a, s = new Array(arguments.length),
						u = S.event.fix(e),
						l = (Y.get(this, "events") || Object.create(null))[u.type] || [],
						c = S.event.special[u.type] || {};
					for (s[0] = u, t = 1; t < arguments.length; t++) s[t] = arguments[t];
					if (u.delegateTarget = this, !c.preDispatch || !1 !== c.preDispatch.call(this, u)) {
						a = S.event.handlers.call(this, u, l), t = 0;
						while ((i = a[t++]) && !u.isPropagationStopped()) {
							u.currentTarget = i.elem, n = 0;
							while ((o = i.handlers[n++]) && !u.isImmediatePropagationStopped()) u.rnamespace && !1 !== o.namespace && !u.rnamespace.test(o.namespace) || (u.handleObj = o, u.data = o.data, void 0 !== (r = ((S.event.special[o.origType] || {}).handle || o.handler).apply(i.elem, s)) && !1 === (u.result = r) && (u.preventDefault(), u.stopPropagation()))
						}
						return c.postDispatch && c.postDispatch.call(this, u), u.result
					}
				},
				handlers: function(e, t) {
					var n, r, i, o, a, s = [],
						u = t.delegateCount,
						l = e.target;
					if (u && l.nodeType && !("click" === e.type && 1 <= e.button))
						for (; l !== this; l = l.parentNode || this)
							if (1 === l.nodeType && ("click" !== e.type || !0 !== l.disabled)) {
								for (o = [], a = {}, n = 0; n < u; n++) void 0 === a[i = (r = t[n]).selector + " "] && (a[i] = r.needsContext ? -1 < S(i, this).index(l) : S.find(i, this, null, [l]).length), a[i] && o.push(r);
								o.length && s.push({
									elem: l,
									handlers: o
								})
							}
					return l = this, u < t.length && s.push({
						elem: l,
						handlers: t.slice(u)
					}), s
				},
				addProp: function(t, e) {
					Object.defineProperty(S.Event.prototype, t, {
						enumerable: !0,
						configurable: !0,
						get: m(e) ? function() {
							if (this.originalEvent) return e(this.originalEvent)
						} : function() {
							if (this.originalEvent) return this.originalEvent[t]
						},
						set: function(e) {
							Object.defineProperty(this, t, {
								enumerable: !0,
								configurable: !0,
								writable: !0,
								value: e
							})
						}
					})
				},
				fix: function(e) {
					return e[S.expando] ? e : new S.Event(e)
				},
				special: {
					load: {
						noBubble: !0
					},
					click: {
						setup: function(e) {
							var t = this || e;
							return pe.test(t.type) && t.click && A(t, "input") && Ae(t, "click", Ce), !1
						},
						trigger: function(e) {
							var t = this || e;
							return pe.test(t.type) && t.click && A(t, "input") && Ae(t, "click"), !0
						},
						_default: function(e) {
							var t = e.target;
							return pe.test(t.type) && t.click && A(t, "input") && Y.get(t, "click") || A(t, "a")
						}
					},
					beforeunload: {
						postDispatch: function(e) {
							void 0 !== e.result && e.originalEvent && (e.originalEvent.returnValue = e.result)
						}
					}
				}
			}, S.removeEvent = function(e, t, n) {
				e.removeEventListener && e.removeEventListener(t, n)
			}, S.Event = function(e, t) {
				if (!(this instanceof S.Event)) return new S.Event(e, t);
				e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || void 0 === e.defaultPrevented && !1 === e.returnValue ? Ce : Ee, this.target = e.target && 3 === e.target.nodeType ? e.target.parentNode : e.target, this.currentTarget = e.currentTarget, this.relatedTarget = e.relatedTarget) : this.type = e, t && S.extend(this, t), this.timeStamp = e && e.timeStamp || Date.now(), this[S.expando] = !0
			}, S.Event.prototype = {
				constructor: S.Event,
				isDefaultPrevented: Ee,
				isPropagationStopped: Ee,
				isImmediatePropagationStopped: Ee,
				isSimulated: !1,
				preventDefault: function() {
					var e = this.originalEvent;
					this.isDefaultPrevented = Ce, e && !this.isSimulated && e.preventDefault()
				},
				stopPropagation: function() {
					var e = this.originalEvent;
					this.isPropagationStopped = Ce, e && !this.isSimulated && e.stopPropagation()
				},
				stopImmediatePropagation: function() {
					var e = this.originalEvent;
					this.isImmediatePropagationStopped = Ce, e && !this.isSimulated && e.stopImmediatePropagation(), this.stopPropagation()
				}
			}, S.each({
				altKey: !0,
				bubbles: !0,
				cancelable: !0,
				changedTouches: !0,
				ctrlKey: !0,
				detail: !0,
				eventPhase: !0,
				metaKey: !0,
				pageX: !0,
				pageY: !0,
				shiftKey: !0,
				view: !0,
				"char": !0,
				code: !0,
				charCode: !0,
				key: !0,
				keyCode: !0,
				button: !0,
				buttons: !0,
				clientX: !0,
				clientY: !0,
				offsetX: !0,
				offsetY: !0,
				pointerId: !0,
				pointerType: !0,
				screenX: !0,
				screenY: !0,
				targetTouches: !0,
				toElement: !0,
				touches: !0,
				which: function(e) {
					var t = e.button;
					return null == e.which && be.test(e.type) ? null != e.charCode ? e.charCode : e.keyCode : !e.which && void 0 !== t && we.test(e.type) ? 1 & t ? 1 : 2 & t ? 3 : 4 & t ? 2 : 0 : e.which
				}
			}, S.event.addProp), S.each({
				focus: "focusin",
				blur: "focusout"
			}, function(e, t) {
				S.event.special[e] = {
					setup: function() {
						return Ae(this, e, Se), !1
					},
					trigger: function() {
						return Ae(this, e), !0
					},
					delegateType: t
				}
			}), S.each({
				mouseenter: "mouseover",
				mouseleave: "mouseout",
				pointerenter: "pointerover",
				pointerleave: "pointerout"
			}, function(e, i) {
				S.event.special[e] = {
					delegateType: i,
					bindType: i,
					handle: function(e) {
						var t, n = e.relatedTarget,
							r = e.handleObj;
						return n && (n === this || S.contains(this, n)) || (e.type = r.origType, t = r.handler.apply(this, arguments), e.type = i), t
					}
				}
			}), S.fn.extend({
				on: function(e, t, n, r) {
					return ke(this, e, t, n, r)
				},
				one: function(e, t, n, r) {
					return ke(this, e, t, n, r, 1)
				},
				off: function(e, t, n) {
					var r, i;
					if (e && e.preventDefault && e.handleObj) return r = e.handleObj, S(e.delegateTarget).off(r.namespace ? r.origType + "." + r.namespace : r.origType, r.selector, r.handler), this;
					if ("object" == typeof e) {
						for (i in e) this.off(i, t, e[i]);
						return this
					}
					return !1 !== t && "function" != typeof t || (n = t, t = void 0), !1 === n && (n = Ee), this.each(function() {
						S.event.remove(this, e, n, t)
					})
				}
			});
			var Ne = /<script|<style|<link/i,
				De = /checked\s*(?:[^=]|=\s*.checked.)/i,
				je = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;

			function qe(e, t) {
				return A(e, "table") && A(11 !== t.nodeType ? t : t.firstChild, "tr") && S(e).children("tbody")[0] || e
			}

			function Le(e) {
				return e.type = (null !== e.getAttribute("type")) + "/" + e.type, e
			}

			function He(e) {
				return "true/" === (e.type || "").slice(0, 5) ? e.type = e.type.slice(5) : e.removeAttribute("type"), e
			}

			function Oe(e, t) {
				var n, r, i, o, a, s;
				if (1 === t.nodeType) {
					if (Y.hasData(e) && (s = Y.get(e).events))
						for (i in Y.remove(t, "handle events"), s)
							for (n = 0, r = s[i].length; n < r; n++) S.event.add(t, i, s[i][n]);
					Q.hasData(e) && (o = Q.access(e), a = S.extend({}, o), Q.set(t, a))
				}
			}

			function Pe(n, r, i, o) {
				r = g(r);
				var e, t, a, s, u, l, c = 0,
					f = n.length,
					p = f - 1,
					d = r[0],
					h = m(d);
				if (h || 1 < f && "string" == typeof d && !y.checkClone && De.test(d)) return n.each(function(e) {
					var t = n.eq(e);
					h && (r[0] = d.call(this, e, t.html())), Pe(t, r, i, o)
				});
				if (f && (t = (e = xe(r, n[0].ownerDocument, !1, n, o)).firstChild, 1 === e.childNodes.length && (e = t), t || o)) {
					for (s = (a = S.map(ve(e, "script"), Le)).length; c < f; c++) u = e, c !== p && (u = S.clone(u, !0, !0), s && S.merge(a, ve(u, "script"))), i.call(n[c], u, c);
					if (s)
						for (l = a[a.length - 1].ownerDocument, S.map(a, He), c = 0; c < s; c++) u = a[c], he.test(u.type || "") && !Y.access(u, "globalEval") && S.contains(l, u) && (u.src && "module" !== (u.type || "").toLowerCase() ? S._evalUrl && !u.noModule && S._evalUrl(u.src, {
							nonce: u.nonce || u.getAttribute("nonce")
						}, l) : b(u.textContent.replace(je, ""), u, l))
				}
				return n
			}

			function Re(e, t, n) {
				for (var r, i = t ? S.filter(t, e) : e, o = 0; null != (r = i[o]); o++) n || 1 !== r.nodeType || S.cleanData(ve(r)), r.parentNode && (n && ie(r) && ye(ve(r, "script")), r.parentNode.removeChild(r));
				return e
			}
			S.extend({
				htmlPrefilter: function(e) {
					return e
				},
				clone: function(e, t, n) {
					var r, i, o, a, s, u, l, c = e.cloneNode(!0),
						f = ie(e);
					if (!(y.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || S.isXMLDoc(e)))
						for (a = ve(c), r = 0, i = (o = ve(e)).length; r < i; r++) s = o[r], u = a[r], void 0, "input" === (l = u.nodeName.toLowerCase()) && pe.test(s.type) ? u.checked = s.checked : "input" !== l && "textarea" !== l || (u.defaultValue = s.defaultValue);
					if (t)
						if (n)
							for (o = o || ve(e), a = a || ve(c), r = 0, i = o.length; r < i; r++) Oe(o[r], a[r]);
						else Oe(e, c);
					return 0 < (a = ve(c, "script")).length && ye(a, !f && ve(e, "script")), c
				},
				cleanData: function(e) {
					for (var t, n, r, i = S.event.special, o = 0; void 0 !== (n = e[o]); o++)
						if (V(n)) {
							if (t = n[Y.expando]) {
								if (t.events)
									for (r in t.events) i[r] ? S.event.remove(n, r) : S.removeEvent(n, r, t.handle);
								n[Y.expando] = void 0
							}
							n[Q.expando] && (n[Q.expando] = void 0)
						}
				}
			}), S.fn.extend({
				detach: function(e) {
					return Re(this, e, !0)
				},
				remove: function(e) {
					return Re(this, e)
				},
				text: function(e) {
					return $(this, function(e) {
						return void 0 === e ? S.text(this) : this.empty().each(function() {
							1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = e)
						})
					}, null, e, arguments.length)
				},
				append: function() {
					return Pe(this, arguments, function(e) {
						1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || qe(this, e).appendChild(e)
					})
				},
				prepend: function() {
					return Pe(this, arguments, function(e) {
						if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
							var t = qe(this, e);
							t.insertBefore(e, t.firstChild)
						}
					})
				},
				before: function() {
					return Pe(this, arguments, function(e) {
						this.parentNode && this.parentNode.insertBefore(e, this)
					})
				},
				after: function() {
					return Pe(this, arguments, function(e) {
						this.parentNode && this.parentNode.insertBefore(e, this.nextSibling)
					})
				},
				empty: function() {
					for (var e, t = 0; null != (e = this[t]); t++) 1 === e.nodeType && (S.cleanData(ve(e, !1)), e.textContent = "");
					return this
				},
				clone: function(e, t) {
					return e = null != e && e, t = null == t ? e : t, this.map(function() {
						return S.clone(this, e, t)
					})
				},
				html: function(e) {
					return $(this, function(e) {
						var t = this[0] || {},
							n = 0,
							r = this.length;
						if (void 0 === e && 1 === t.nodeType) return t.innerHTML;
						if ("string" == typeof e && !Ne.test(e) && !ge[(de.exec(e) || ["", ""])[1].toLowerCase()]) {
							e = S.htmlPrefilter(e);
							try {
								for (; n < r; n++) 1 === (t = this[n] || {}).nodeType && (S.cleanData(ve(t, !1)), t.innerHTML = e);
								t = 0
							} catch (e) {}
						}
						t && this.empty().append(e)
					}, null, e, arguments.length)
				},
				replaceWith: function() {
					var n = [];
					return Pe(this, arguments, function(e) {
						var t = this.parentNode;
						S.inArray(this, n) < 0 && (S.cleanData(ve(this)), t && t.replaceChild(e, this))
					}, n)
				}
			}), S.each({
				appendTo: "append",
				prependTo: "prepend",
				insertBefore: "before",
				insertAfter: "after",
				replaceAll: "replaceWith"
			}, function(e, a) {
				S.fn[e] = function(e) {
					for (var t, n = [], r = S(e), i = r.length - 1, o = 0; o <= i; o++) t = o === i ? this : this.clone(!0), S(r[o])[a](t), u.apply(n, t.get());
					return this.pushStack(n)
				}
			});
			var Me = new RegExp("^(" + ee + ")(?!px)[a-z%]+$", "i"),
				Ie = function(e) {
					var t = e.ownerDocument.defaultView;
					return t && t.opener || (t = C), t.getComputedStyle(e)
				},
				We = function(e, t, n) {
					var r, i, o = {};
					for (i in t) o[i] = e.style[i], e.style[i] = t[i];
					for (i in r = n.call(e), t) e.style[i] = o[i];
					return r
				},
				Fe = new RegExp(ne.join("|"), "i");

			function Be(e, t, n) {
				var r, i, o, a, s = e.style;
				return (n = n || Ie(e)) && ("" !== (a = n.getPropertyValue(t) || n[t]) || ie(e) || (a = S.style(e, t)), !y.pixelBoxStyles() && Me.test(a) && Fe.test(t) && (r = s.width, i = s.minWidth, o = s.maxWidth, s.minWidth = s.maxWidth = s.width = a, a = n.width, s.width = r, s.minWidth = i, s.maxWidth = o)), void 0 !== a ? a + "" : a
			}

			function $e(e, t) {
				return {
					get: function() {
						if (!e()) return (this.get = t).apply(this, arguments);
						delete this.get
					}
				}
			}! function() {
				function e() {
					if (l) {
						u.style.cssText = "position:absolute;left:-11111px;width:60px;margin-top:1px;padding:0;border:0", l.style.cssText = "position:relative;display:block;box-sizing:border-box;overflow:scroll;margin:auto;border:1px;padding:1px;width:60%;top:1%", re.appendChild(u).appendChild(l);
						var e = C.getComputedStyle(l);
						n = "1%" !== e.top, s = 12 === t(e.marginLeft), l.style.right = "60%", o = 36 === t(e.right), r = 36 === t(e.width), l.style.position = "absolute", i = 12 === t(l.offsetWidth / 3), re.removeChild(u), l = null
					}
				}

				function t(e) {
					return Math.round(parseFloat(e))
				}
				var n, r, i, o, a, s, u = E.createElement("div"),
					l = E.createElement("div");
				l.style && (l.style.backgroundClip = "content-box", l.cloneNode(!0).style.backgroundClip = "", y.clearCloneStyle = "content-box" === l.style.backgroundClip, S.extend(y, {
					boxSizingReliable: function() {
						return e(), r
					},
					pixelBoxStyles: function() {
						return e(), o
					},
					pixelPosition: function() {
						return e(), n
					},
					reliableMarginLeft: function() {
						return e(), s
					},
					scrollboxSize: function() {
						return e(), i
					},
					reliableTrDimensions: function() {
						var e, t, n, r;
						return null == a && (e = E.createElement("table"), t = E.createElement("tr"), n = E.createElement("div"), e.style.cssText = "position:absolute;left:-11111px", t.style.height = "1px", n.style.height = "9px", re.appendChild(e).appendChild(t).appendChild(n), r = C.getComputedStyle(t), a = 3 < parseInt(r.height), re.removeChild(e)), a
					}
				}))
			}();
			var _e = ["Webkit", "Moz", "ms"],
				ze = E.createElement("div").style,
				Ue = {};

			function Xe(e) {
				var t = S.cssProps[e] || Ue[e];
				return t || (e in ze ? e : Ue[e] = function(e) {
					var t = e[0].toUpperCase() + e.slice(1),
						n = _e.length;
					while (n--)
						if ((e = _e[n] + t) in ze) return e
				}(e) || e)
			}
			var Ve = /^(none|table(?!-c[ea]).+)/,
				Ge = /^--/,
				Ye = {
					position: "absolute",
					visibility: "hidden",
					display: "block"
				},
				Qe = {
					letterSpacing: "0",
					fontWeight: "400"
				};

			function Je(e, t, n) {
				var r = te.exec(t);
				return r ? Math.max(0, r[2] - (n || 0)) + (r[3] || "px") : t
			}

			function Ke(e, t, n, r, i, o) {
				var a = "width" === t ? 1 : 0,
					s = 0,
					u = 0;
				if (n === (r ? "border" : "content")) return 0;
				for (; a < 4; a += 2) "margin" === n && (u += S.css(e, n + ne[a], !0, i)), r ? ("content" === n && (u -= S.css(e, "padding" + ne[a], !0, i)), "margin" !== n && (u -= S.css(e, "border" + ne[a] + "Width", !0, i))) : (u += S.css(e, "padding" + ne[a], !0, i), "padding" !== n ? u += S.css(e, "border" + ne[a] + "Width", !0, i) : s += S.css(e, "border" + ne[a] + "Width", !0, i));
				return !r && 0 <= o && (u += Math.max(0, Math.ceil(e["offset" + t[0].toUpperCase() + t.slice(1)] - o - u - s - .5)) || 0), u
			}

			function Ze(e, t, n) {
				var r = Ie(e),
					i = (!y.boxSizingReliable() || n) && "border-box" === S.css(e, "boxSizing", !1, r),
					o = i,
					a = Be(e, t, r),
					s = "offset" + t[0].toUpperCase() + t.slice(1);
				if (Me.test(a)) {
					if (!n) return a;
					a = "auto"
				}
				return (!y.boxSizingReliable() && i || !y.reliableTrDimensions() && A(e, "tr") || "auto" === a || !parseFloat(a) && "inline" === S.css(e, "display", !1, r)) && e.getClientRects().length && (i = "border-box" === S.css(e, "boxSizing", !1, r), (o = s in e) && (a = e[s])), (a = parseFloat(a) || 0) + Ke(e, t, n || (i ? "border" : "content"), o, r, a) + "px"
			}

			function et(e, t, n, r, i) {
				return new et.prototype.init(e, t, n, r, i)
			}
			S.extend({
				cssHooks: {
					opacity: {
						get: function(e, t) {
							if (t) {
								var n = Be(e, "opacity");
								return "" === n ? "1" : n
							}
						}
					}
				},
				cssNumber: {
					animationIterationCount: !0,
					columnCount: !0,
					fillOpacity: !0,
					flexGrow: !0,
					flexShrink: !0,
					fontWeight: !0,
					gridArea: !0,
					gridColumn: !0,
					gridColumnEnd: !0,
					gridColumnStart: !0,
					gridRow: !0,
					gridRowEnd: !0,
					gridRowStart: !0,
					lineHeight: !0,
					opacity: !0,
					order: !0,
					orphans: !0,
					widows: !0,
					zIndex: !0,
					zoom: !0
				},
				cssProps: {},
				style: function(e, t, n, r) {
					if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
						var i, o, a, s = X(t),
							u = Ge.test(t),
							l = e.style;
						if (u || (t = Xe(s)), a = S.cssHooks[t] || S.cssHooks[s], void 0 === n) return a && "get" in a && void 0 !== (i = a.get(e, !1, r)) ? i : l[t];
						"string" === (o = typeof n) && (i = te.exec(n)) && i[1] && (n = se(e, t, i), o = "number"), null != n && n == n && ("number" !== o || u || (n += i && i[3] || (S.cssNumber[s] ? "" : "px")), y.clearCloneStyle || "" !== n || 0 !== t.indexOf("background") || (l[t] = "inherit"), a && "set" in a && void 0 === (n = a.set(e, n, r)) || (u ? l.setProperty(t, n) : l[t] = n))
					}
				},
				css: function(e, t, n, r) {
					var i, o, a, s = X(t);
					return Ge.test(t) || (t = Xe(s)), (a = S.cssHooks[t] || S.cssHooks[s]) && "get" in a && (i = a.get(e, !0, n)), void 0 === i && (i = Be(e, t, r)), "normal" === i && t in Qe && (i = Qe[t]), "" === n || n ? (o = parseFloat(i), !0 === n || isFinite(o) ? o || 0 : i) : i
				}
			}), S.each(["height", "width"], function(e, u) {
				S.cssHooks[u] = {
					get: function(e, t, n) {
						if (t) return !Ve.test(S.css(e, "display")) || e.getClientRects().length && e.getBoundingClientRect().width ? Ze(e, u, n) : We(e, Ye, function() {
							return Ze(e, u, n)
						})
					},
					set: function(e, t, n) {
						var r, i = Ie(e),
							o = !y.scrollboxSize() && "absolute" === i.position,
							a = (o || n) && "border-box" === S.css(e, "boxSizing", !1, i),
							s = n ? Ke(e, u, n, a, i) : 0;
						return a && o && (s -= Math.ceil(e["offset" + u[0].toUpperCase() + u.slice(1)] - parseFloat(i[u]) - Ke(e, u, "border", !1, i) - .5)), s && (r = te.exec(t)) && "px" !== (r[3] || "px") && (e.style[u] = t, t = S.css(e, u)), Je(0, t, s)
					}
				}
			}), S.cssHooks.marginLeft = $e(y.reliableMarginLeft, function(e, t) {
				if (t) return (parseFloat(Be(e, "marginLeft")) || e.getBoundingClientRect().left - We(e, {
					marginLeft: 0
				}, function() {
					return e.getBoundingClientRect().left
				})) + "px"
			}), S.each({
				margin: "",
				padding: "",
				border: "Width"
			}, function(i, o) {
				S.cssHooks[i + o] = {
					expand: function(e) {
						for (var t = 0, n = {}, r = "string" == typeof e ? e.split(" ") : [e]; t < 4; t++) n[i + ne[t] + o] = r[t] || r[t - 2] || r[0];
						return n
					}
				}, "margin" !== i && (S.cssHooks[i + o].set = Je)
			}), S.fn.extend({
				css: function(e, t) {
					return $(this, function(e, t, n) {
						var r, i, o = {},
							a = 0;
						if (Array.isArray(t)) {
							for (r = Ie(e), i = t.length; a < i; a++) o[t[a]] = S.css(e, t[a], !1, r);
							return o
						}
						return void 0 !== n ? S.style(e, t, n) : S.css(e, t)
					}, e, t, 1 < arguments.length)
				}
			}), ((S.Tween = et).prototype = {
				constructor: et,
				init: function(e, t, n, r, i, o) {
					this.elem = e, this.prop = n, this.easing = i || S.easing._default, this.options = t, this.start = this.now = this.cur(), this.end = r, this.unit = o || (S.cssNumber[n] ? "" : "px")
				},
				cur: function() {
					var e = et.propHooks[this.prop];
					return e && e.get ? e.get(this) : et.propHooks._default.get(this)
				},
				run: function(e) {
					var t, n = et.propHooks[this.prop];
					return this.options.duration ? this.pos = t = S.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : this.pos = t = e, this.now = (this.end - this.start) * t + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : et.propHooks._default.set(this), this
				}
			}).init.prototype = et.prototype, (et.propHooks = {
				_default: {
					get: function(e) {
						var t;
						return 1 !== e.elem.nodeType || null != e.elem[e.prop] && null == e.elem.style[e.prop] ? e.elem[e.prop] : (t = S.css(e.elem, e.prop, "")) && "auto" !== t ? t : 0
					},
					set: function(e) {
						S.fx.step[e.prop] ? S.fx.step[e.prop](e) : 1 !== e.elem.nodeType || !S.cssHooks[e.prop] && null == e.elem.style[Xe(e.prop)] ? e.elem[e.prop] = e.now : S.style(e.elem, e.prop, e.now + e.unit)
					}
				}
			}).scrollTop = et.propHooks.scrollLeft = {
				set: function(e) {
					e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now)
				}
			}, S.easing = {
				linear: function(e) {
					return e
				},
				swing: function(e) {
					return .5 - Math.cos(e * Math.PI) / 2
				},
				_default: "swing"
			}, S.fx = et.prototype.init, S.fx.step = {};
			var tt, nt, rt, it, ot = /^(?:toggle|show|hide)$/,
				at = /queueHooks$/;

			function st() {
				nt && (!1 === E.hidden && C.requestAnimationFrame ? C.requestAnimationFrame(st) : C.setTimeout(st, S.fx.interval), S.fx.tick())
			}

			function ut() {
				return C.setTimeout(function() {
					tt = void 0
				}), tt = Date.now()
			}

			function lt(e, t) {
				var n, r = 0,
					i = {
						height: e
					};
				for (t = t ? 1 : 0; r < 4; r += 2 - t) i["margin" + (n = ne[r])] = i["padding" + n] = e;
				return t && (i.opacity = i.width = e), i
			}

			function ct(e, t, n) {
				for (var r, i = (ft.tweeners[t] || []).concat(ft.tweeners["*"]), o = 0, a = i.length; o < a; o++)
					if (r = i[o].call(n, t, e)) return r
			}

			function ft(o, e, t) {
				var n, a, r = 0,
					i = ft.prefilters.length,
					s = S.Deferred().always(function() {
						delete u.elem
					}),
					u = function() {
						if (a) return !1;
						for (var e = tt || ut(), t = Math.max(0, l.startTime + l.duration - e), n = 1 - (t / l.duration || 0), r = 0, i = l.tweens.length; r < i; r++) l.tweens[r].run(n);
						return s.notifyWith(o, [l, n, t]), n < 1 && i ? t : (i || s.notifyWith(o, [l, 1, 0]), s.resolveWith(o, [l]), !1)
					},
					l = s.promise({
						elem: o,
						props: S.extend({}, e),
						opts: S.extend(!0, {
							specialEasing: {},
							easing: S.easing._default
						}, t),
						originalProperties: e,
						originalOptions: t,
						startTime: tt || ut(),
						duration: t.duration,
						tweens: [],
						createTween: function(e, t) {
							var n = S.Tween(o, l.opts, e, t, l.opts.specialEasing[e] || l.opts.easing);
							return l.tweens.push(n), n
						},
						stop: function(e) {
							var t = 0,
								n = e ? l.tweens.length : 0;
							if (a) return this;
							for (a = !0; t < n; t++) l.tweens[t].run(1);
							return e ? (s.notifyWith(o, [l, 1, 0]), s.resolveWith(o, [l, e])) : s.rejectWith(o, [l, e]), this
						}
					}),
					c = l.props;
				for (! function(e, t) {
						var n, r, i, o, a;
						for (n in e)
							if (i = t[r = X(n)], o = e[n], Array.isArray(o) && (i = o[1], o = e[n] = o[0]), n !== r && (e[r] = o, delete e[n]), (a = S.cssHooks[r]) && "expand" in a)
								for (n in o = a.expand(o), delete e[r], o) n in e || (e[n] = o[n], t[n] = i);
							else t[r] = i
					}(c, l.opts.specialEasing); r < i; r++)
					if (n = ft.prefilters[r].call(l, o, c, l.opts)) return m(n.stop) && (S._queueHooks(l.elem, l.opts.queue).stop = n.stop.bind(n)), n;
				return S.map(c, ct, l), m(l.opts.start) && l.opts.start.call(o, l), l.progress(l.opts.progress).done(l.opts.done, l.opts.complete).fail(l.opts.fail).always(l.opts.always), S.fx.timer(S.extend(u, {
					elem: o,
					anim: l,
					queue: l.opts.queue
				})), l
			}
			S.Animation = S.extend(ft, {
				tweeners: {
					"*": [function(e, t) {
						var n = this.createTween(e, t);
						return se(n.elem, e, te.exec(t), n), n
					}]
				},
				tweener: function(e, t) {
					m(e) ? (t = e, e = ["*"]) : e = e.match(P);
					for (var n, r = 0, i = e.length; r < i; r++) n = e[r], ft.tweeners[n] = ft.tweeners[n] || [], ft.tweeners[n].unshift(t)
				},
				prefilters: [function(e, t, n) {
					var r, i, o, a, s, u, l, c, f = "width" in t || "height" in t,
						p = this,
						d = {},
						h = e.style,
						g = e.nodeType && ae(e),
						v = Y.get(e, "fxshow");
					for (r in n.queue || (null == (a = S._queueHooks(e, "fx")).unqueued && (a.unqueued = 0, s = a.empty.fire, a.empty.fire = function() {
							a.unqueued || s()
						}), a.unqueued++, p.always(function() {
							p.always(function() {
								a.unqueued--, S.queue(e, "fx").length || a.empty.fire()
							})
						})), t)
						if (i = t[r], ot.test(i)) {
							if (delete t[r], o = o || "toggle" === i, i === (g ? "hide" : "show")) {
								if ("show" !== i || !v || void 0 === v[r]) continue;
								g = !0
							}
							d[r] = v && v[r] || S.style(e, r)
						}
					if ((u = !S.isEmptyObject(t)) || !S.isEmptyObject(d))
						for (r in f && 1 === e.nodeType && (n.overflow = [h.overflow, h.overflowX, h.overflowY], null == (l = v && v.display) && (l = Y.get(e, "display")), "none" === (c = S.css(e, "display")) && (l ? c = l : (le([e], !0), l = e.style.display || l, c = S.css(e, "display"), le([e]))), ("inline" === c || "inline-block" === c && null != l) && "none" === S.css(e, "float") && (u || (p.done(function() {
								h.display = l
							}), null == l && (c = h.display, l = "none" === c ? "" : c)), h.display = "inline-block")), n.overflow && (h.overflow = "hidden", p.always(function() {
								h.overflow = n.overflow[0], h.overflowX = n.overflow[1], h.overflowY = n.overflow[2]
							})), u = !1, d) u || (v ? "hidden" in v && (g = v.hidden) : v = Y.access(e, "fxshow", {
							display: l
						}), o && (v.hidden = !g), g && le([e], !0), p.done(function() {
							for (r in g || le([e]), Y.remove(e, "fxshow"), d) S.style(e, r, d[r])
						})), u = ct(g ? v[r] : 0, r, p), r in v || (v[r] = u.start, g && (u.end = u.start, u.start = 0))
				}],
				prefilter: function(e, t) {
					t ? ft.prefilters.unshift(e) : ft.prefilters.push(e)
				}
			}), S.speed = function(e, t, n) {
				var r = e && "object" == typeof e ? S.extend({}, e) : {
					complete: n || !n && t || m(e) && e,
					duration: e,
					easing: n && t || t && !m(t) && t
				};
				return S.fx.off ? r.duration = 0 : "number" != typeof r.duration && (r.duration in S.fx.speeds ? r.duration = S.fx.speeds[r.duration] : r.duration = S.fx.speeds._default), null != r.queue && !0 !== r.queue || (r.queue = "fx"), r.old = r.complete, r.complete = function() {
					m(r.old) && r.old.call(this), r.queue && S.dequeue(this, r.queue)
				}, r
			}, S.fn.extend({
				fadeTo: function(e, t, n, r) {
					return this.filter(ae).css("opacity", 0).show().end().animate({
						opacity: t
					}, e, n, r)
				},
				animate: function(t, e, n, r) {
					var i = S.isEmptyObject(t),
						o = S.speed(e, n, r),
						a = function() {
							var e = ft(this, S.extend({}, t), o);
							(i || Y.get(this, "finish")) && e.stop(!0)
						};
					return a.finish = a, i || !1 === o.queue ? this.each(a) : this.queue(o.queue, a)
				},
				stop: function(i, e, o) {
					var a = function(e) {
						var t = e.stop;
						delete e.stop, t(o)
					};
					return "string" != typeof i && (o = e, e = i, i = void 0), e && this.queue(i || "fx", []), this.each(function() {
						var e = !0,
							t = null != i && i + "queueHooks",
							n = S.timers,
							r = Y.get(this);
						if (t) r[t] && r[t].stop && a(r[t]);
						else
							for (t in r) r[t] && r[t].stop && at.test(t) && a(r[t]);
						for (t = n.length; t--;) n[t].elem !== this || null != i && n[t].queue !== i || (n[t].anim.stop(o), e = !1, n.splice(t, 1));
						!e && o || S.dequeue(this, i)
					})
				},
				finish: function(a) {
					return !1 !== a && (a = a || "fx"), this.each(function() {
						var e, t = Y.get(this),
							n = t[a + "queue"],
							r = t[a + "queueHooks"],
							i = S.timers,
							o = n ? n.length : 0;
						for (t.finish = !0, S.queue(this, a, []), r && r.stop && r.stop.call(this, !0), e = i.length; e--;) i[e].elem === this && i[e].queue === a && (i[e].anim.stop(!0), i.splice(e, 1));
						for (e = 0; e < o; e++) n[e] && n[e].finish && n[e].finish.call(this);
						delete t.finish
					})
				}
			}), S.each(["toggle", "show", "hide"], function(e, r) {
				var i = S.fn[r];
				S.fn[r] = function(e, t, n) {
					return null == e || "boolean" == typeof e ? i.apply(this, arguments) : this.animate(lt(r, !0), e, t, n)
				}
			}), S.each({
				slideDown: lt("show"),
				slideUp: lt("hide"),
				slideToggle: lt("toggle"),
				fadeIn: {
					opacity: "show"
				},
				fadeOut: {
					opacity: "hide"
				},
				fadeToggle: {
					opacity: "toggle"
				}
			}, function(e, r) {
				S.fn[e] = function(e, t, n) {
					return this.animate(r, e, t, n)
				}
			}), S.timers = [], S.fx.tick = function() {
				var e, t = 0,
					n = S.timers;
				for (tt = Date.now(); t < n.length; t++)(e = n[t])() || n[t] !== e || n.splice(t--, 1);
				n.length || S.fx.stop(), tt = void 0
			}, S.fx.timer = function(e) {
				S.timers.push(e), S.fx.start()
			}, S.fx.interval = 13, S.fx.start = function() {
				nt || (nt = !0, st())
			}, S.fx.stop = function() {
				nt = null
			}, S.fx.speeds = {
				slow: 600,
				fast: 200,
				_default: 400
			}, S.fn.delay = function(r, e) {
				return r = S.fx && S.fx.speeds[r] || r, e = e || "fx", this.queue(e, function(e, t) {
					var n = C.setTimeout(e, r);
					t.stop = function() {
						C.clearTimeout(n)
					}
				})
			}, rt = E.createElement("input"), it = E.createElement("select").appendChild(E.createElement("option")), rt.type = "checkbox", y.checkOn = "" !== rt.value, y.optSelected = it.selected, (rt = E.createElement("input")).value = "t", rt.type = "radio", y.radioValue = "t" === rt.value;
			var pt, dt = S.expr.attrHandle;
			S.fn.extend({
				attr: function(e, t) {
					return $(this, S.attr, e, t, 1 < arguments.length)
				},
				removeAttr: function(e) {
					return this.each(function() {
						S.removeAttr(this, e)
					})
				}
			}), S.extend({
				attr: function(e, t, n) {
					var r, i, o = e.nodeType;
					if (3 !== o && 8 !== o && 2 !== o) return "undefined" == typeof e.getAttribute ? S.prop(e, t, n) : (1 === o && S.isXMLDoc(e) || (i = S.attrHooks[t.toLowerCase()] || (S.expr.match.bool.test(t) ? pt : void 0)), void 0 !== n ? null === n ? void S.removeAttr(e, t) : i && "set" in i && void 0 !== (r = i.set(e, n, t)) ? r : (e.setAttribute(t, n + ""), n) : i && "get" in i && null !== (r = i.get(e, t)) ? r : null == (r = S.find.attr(e, t)) ? void 0 : r)
				},
				attrHooks: {
					type: {
						set: function(e, t) {
							if (!y.radioValue && "radio" === t && A(e, "input")) {
								var n = e.value;
								return e.setAttribute("type", t), n && (e.value = n), t
							}
						}
					}
				},
				removeAttr: function(e, t) {
					var n, r = 0,
						i = t && t.match(P);
					if (i && 1 === e.nodeType)
						while (n = i[r++]) e.removeAttribute(n)
				}
			}), pt = {
				set: function(e, t, n) {
					return !1 === t ? S.removeAttr(e, n) : e.setAttribute(n, n), n
				}
			}, S.each(S.expr.match.bool.source.match(/\w+/g), function(e, t) {
				var a = dt[t] || S.find.attr;
				dt[t] = function(e, t, n) {
					var r, i, o = t.toLowerCase();
					return n || (i = dt[o], dt[o] = r, r = null != a(e, t, n) ? o : null, dt[o] = i), r
				}
			});
			var ht = /^(?:input|select|textarea|button)$/i,
				gt = /^(?:a|area)$/i;

			function vt(e) {
				return (e.match(P) || []).join(" ")
			}

			function yt(e) {
				return e.getAttribute && e.getAttribute("class") || ""
			}

			function mt(e) {
				return Array.isArray(e) ? e : "string" == typeof e && e.match(P) || []
			}
			S.fn.extend({
				prop: function(e, t) {
					return $(this, S.prop, e, t, 1 < arguments.length)
				},
				removeProp: function(e) {
					return this.each(function() {
						delete this[S.propFix[e] || e]
					})
				}
			}), S.extend({
				prop: function(e, t, n) {
					var r, i, o = e.nodeType;
					if (3 !== o && 8 !== o && 2 !== o) return 1 === o && S.isXMLDoc(e) || (t = S.propFix[t] || t, i = S.propHooks[t]), void 0 !== n ? i && "set" in i && void 0 !== (r = i.set(e, n, t)) ? r : e[t] = n : i && "get" in i && null !== (r = i.get(e, t)) ? r : e[t]
				},
				propHooks: {
					tabIndex: {
						get: function(e) {
							var t = S.find.attr(e, "tabindex");
							return t ? parseInt(t, 10) : ht.test(e.nodeName) || gt.test(e.nodeName) && e.href ? 0 : -1
						}
					}
				},
				propFix: {
					"for": "htmlFor",
					"class": "className"
				}
			}), y.optSelected || (S.propHooks.selected = {
				get: function(e) {
					var t = e.parentNode;
					return t && t.parentNode && t.parentNode.selectedIndex, null
				},
				set: function(e) {
					var t = e.parentNode;
					t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex)
				}
			}), S.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function() {
				S.propFix[this.toLowerCase()] = this
			}), S.fn.extend({
				addClass: function(t) {
					var e, n, r, i, o, a, s, u = 0;
					if (m(t)) return this.each(function(e) {
						S(this).addClass(t.call(this, e, yt(this)))
					});
					if ((e = mt(t)).length)
						while (n = this[u++])
							if (i = yt(n), r = 1 === n.nodeType && " " + vt(i) + " ") {
								a = 0;
								while (o = e[a++]) r.indexOf(" " + o + " ") < 0 && (r += o + " ");
								i !== (s = vt(r)) && n.setAttribute("class", s)
							}
					return this
				},
				removeClass: function(t) {
					var e, n, r, i, o, a, s, u = 0;
					if (m(t)) return this.each(function(e) {
						S(this).removeClass(t.call(this, e, yt(this)))
					});
					if (!arguments.length) return this.attr("class", "");
					if ((e = mt(t)).length)
						while (n = this[u++])
							if (i = yt(n), r = 1 === n.nodeType && " " + vt(i) + " ") {
								a = 0;
								while (o = e[a++])
									while (-1 < r.indexOf(" " + o + " ")) r = r.replace(" " + o + " ", " ");
								i !== (s = vt(r)) && n.setAttribute("class", s)
							}
					return this
				},
				toggleClass: function(i, t) {
					var o = typeof i,
						a = "string" === o || Array.isArray(i);
					return "boolean" == typeof t && a ? t ? this.addClass(i) : this.removeClass(i) : m(i) ? this.each(function(e) {
						S(this).toggleClass(i.call(this, e, yt(this), t), t)
					}) : this.each(function() {
						var e, t, n, r;
						if (a) {
							t = 0, n = S(this), r = mt(i);
							while (e = r[t++]) n.hasClass(e) ? n.removeClass(e) : n.addClass(e)
						} else void 0 !== i && "boolean" !== o || ((e = yt(this)) && Y.set(this, "__className__", e), this.setAttribute && this.setAttribute("class", e || !1 === i ? "" : Y.get(this, "__className__") || ""))
					})
				},
				hasClass: function(e) {
					var t, n, r = 0;
					t = " " + e + " ";
					while (n = this[r++])
						if (1 === n.nodeType && -1 < (" " + vt(yt(n)) + " ").indexOf(t)) return !0;
					return !1
				}
			});
			var xt = /\r/g;
			S.fn.extend({
				val: function(n) {
					var r, e, i, t = this[0];
					return arguments.length ? (i = m(n), this.each(function(e) {
						var t;
						1 === this.nodeType && (null == (t = i ? n.call(this, e, S(this).val()) : n) ? t = "" : "number" == typeof t ? t += "" : Array.isArray(t) && (t = S.map(t, function(e) {
							return null == e ? "" : e + ""
						})), (r = S.valHooks[this.type] || S.valHooks[this.nodeName.toLowerCase()]) && "set" in r && void 0 !== r.set(this, t, "value") || (this.value = t))
					})) : t ? (r = S.valHooks[t.type] || S.valHooks[t.nodeName.toLowerCase()]) && "get" in r && void 0 !== (e = r.get(t, "value")) ? e : "string" == typeof(e = t.value) ? e.replace(xt, "") : null == e ? "" : e : void 0
				}
			}), S.extend({
				valHooks: {
					option: {
						get: function(e) {
							var t = S.find.attr(e, "value");
							return null != t ? t : vt(S.text(e))
						}
					},
					select: {
						get: function(e) {
							var t, n, r, i = e.options,
								o = e.selectedIndex,
								a = "select-one" === e.type,
								s = a ? null : [],
								u = a ? o + 1 : i.length;
							for (r = o < 0 ? u : a ? o : 0; r < u; r++)
								if (((n = i[r]).selected || r === o) && !n.disabled && (!n.parentNode.disabled || !A(n.parentNode, "optgroup"))) {
									if (t = S(n).val(), a) return t;
									s.push(t)
								}
							return s
						},
						set: function(e, t) {
							var n, r, i = e.options,
								o = S.makeArray(t),
								a = i.length;
							while (a--)((r = i[a]).selected = -1 < S.inArray(S.valHooks.option.get(r), o)) && (n = !0);
							return n || (e.selectedIndex = -1), o
						}
					}
				}
			}), S.each(["radio", "checkbox"], function() {
				S.valHooks[this] = {
					set: function(e, t) {
						if (Array.isArray(t)) return e.checked = -1 < S.inArray(S(e).val(), t)
					}
				}, y.checkOn || (S.valHooks[this].get = function(e) {
					return null === e.getAttribute("value") ? "on" : e.value
				})
			}), y.focusin = "onfocusin" in C;
			var bt = /^(?:focusinfocus|focusoutblur)$/,
				wt = function(e) {
					e.stopPropagation()
				};
			S.extend(S.event, {
				trigger: function(e, t, n, r) {
					var i, o, a, s, u, l, c, f, p = [n || E],
						d = v.call(e, "type") ? e.type : e,
						h = v.call(e, "namespace") ? e.namespace.split(".") : [];
					if (o = f = a = n = n || E, 3 !== n.nodeType && 8 !== n.nodeType && !bt.test(d + S.event.triggered) && (-1 < d.indexOf(".") && (d = (h = d.split(".")).shift(), h.sort()), u = d.indexOf(":") < 0 && "on" + d, (e = e[S.expando] ? e : new S.Event(d, "object" == typeof e && e)).isTrigger = r ? 2 : 3, e.namespace = h.join("."), e.rnamespace = e.namespace ? new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, e.result = void 0, e.target || (e.target = n), t = null == t ? [e] : S.makeArray(t, [e]), c = S.event.special[d] || {}, r || !c.trigger || !1 !== c.trigger.apply(n, t))) {
						if (!r && !c.noBubble && !x(n)) {
							for (s = c.delegateType || d, bt.test(s + d) || (o = o.parentNode); o; o = o.parentNode) p.push(o), a = o;
							a === (n.ownerDocument || E) && p.push(a.defaultView || a.parentWindow || C)
						}
						i = 0;
						while ((o = p[i++]) && !e.isPropagationStopped()) f = o, e.type = 1 < i ? s : c.bindType || d, (l = (Y.get(o, "events") || Object.create(null))[e.type] && Y.get(o, "handle")) && l.apply(o, t), (l = u && o[u]) && l.apply && V(o) && (e.result = l.apply(o, t), !1 === e.result && e.preventDefault());
						return e.type = d, r || e.isDefaultPrevented() || c._default && !1 !== c._default.apply(p.pop(), t) || !V(n) || u && m(n[d]) && !x(n) && ((a = n[u]) && (n[u] = null), S.event.triggered = d, e.isPropagationStopped() && f.addEventListener(d, wt), n[d](), e.isPropagationStopped() && f.removeEventListener(d, wt), S.event.triggered = void 0, a && (n[u] = a)), e.result
					}
				},
				simulate: function(e, t, n) {
					var r = S.extend(new S.Event, n, {
						type: e,
						isSimulated: !0
					});
					S.event.trigger(r, null, t)
				}
			}), S.fn.extend({
				trigger: function(e, t) {
					return this.each(function() {
						S.event.trigger(e, t, this)
					})
				},
				triggerHandler: function(e, t) {
					var n = this[0];
					if (n) return S.event.trigger(e, t, n, !0)
				}
			}), y.focusin || S.each({
				focus: "focusin",
				blur: "focusout"
			}, function(n, r) {
				var i = function(e) {
					S.event.simulate(r, e.target, S.event.fix(e))
				};
				S.event.special[r] = {
					setup: function() {
						var e = this.ownerDocument || this.document || this,
							t = Y.access(e, r);
						t || e.addEventListener(n, i, !0), Y.access(e, r, (t || 0) + 1)
					},
					teardown: function() {
						var e = this.ownerDocument || this.document || this,
							t = Y.access(e, r) - 1;
						t ? Y.access(e, r, t) : (e.removeEventListener(n, i, !0), Y.remove(e, r))
					}
				}
			});
			var Tt = C.location,
				Ct = {
					guid: Date.now()
				},
				Et = /\?/;
			S.parseXML = function(e) {
				var t;
				if (!e || "string" != typeof e) return null;
				try {
					t = (new C.DOMParser).parseFromString(e, "text/xml")
				} catch (e) {
					t = void 0
				}
				return t && !t.getElementsByTagName("parsererror").length || S.error("Invalid XML: " + e), t
			};
			var St = /\[\]$/,
				kt = /\r?\n/g,
				At = /^(?:submit|button|image|reset|file)$/i,
				Nt = /^(?:input|select|textarea|keygen)/i;

			function Dt(n, e, r, i) {
				var t;
				if (Array.isArray(e)) S.each(e, function(e, t) {
					r || St.test(n) ? i(n, t) : Dt(n + "[" + ("object" == typeof t && null != t ? e : "") + "]", t, r, i)
				});
				else if (r || "object" !== w(e)) i(n, e);
				else
					for (t in e) Dt(n + "[" + t + "]", e[t], r, i)
			}
			S.param = function(e, t) {
				var n, r = [],
					i = function(e, t) {
						var n = m(t) ? t() : t;
						r[r.length] = encodeURIComponent(e) + "=" + encodeURIComponent(null == n ? "" : n)
					};
				if (null == e) return "";
				if (Array.isArray(e) || e.jquery && !S.isPlainObject(e)) S.each(e, function() {
					i(this.name, this.value)
				});
				else
					for (n in e) Dt(n, e[n], t, i);
				return r.join("&")
			}, S.fn.extend({
				serialize: function() {
					return S.param(this.serializeArray())
				},
				serializeArray: function() {
					return this.map(function() {
						var e = S.prop(this, "elements");
						return e ? S.makeArray(e) : this
					}).filter(function() {
						var e = this.type;
						return this.name && !S(this).is(":disabled") && Nt.test(this.nodeName) && !At.test(e) && (this.checked || !pe.test(e))
					}).map(function(e, t) {
						var n = S(this).val();
						return null == n ? null : Array.isArray(n) ? S.map(n, function(e) {
							return {
								name: t.name,
								value: e.replace(kt, "\r\n")
							}
						}) : {
							name: t.name,
							value: n.replace(kt, "\r\n")
						}
					}).get()
				}
			});
			var jt = /%20/g,
				qt = /#.*$/,
				Lt = /([?&])_=[^&]*/,
				Ht = /^(.*?):[ \t]*([^\r\n]*)$/gm,
				Ot = /^(?:GET|HEAD)$/,
				Pt = /^\/\//,
				Rt = {},
				Mt = {},
				It = "*/".concat("*"),
				Wt = E.createElement("a");

			function Ft(o) {
				return function(e, t) {
					"string" != typeof e && (t = e, e = "*");
					var n, r = 0,
						i = e.toLowerCase().match(P) || [];
					if (m(t))
						while (n = i[r++]) "+" === n[0] ? (n = n.slice(1) || "*", (o[n] = o[n] || []).unshift(t)) : (o[n] = o[n] || []).push(t)
				}
			}

			function Bt(t, i, o, a) {
				var s = {},
					u = t === Mt;

				function l(e) {
					var r;
					return s[e] = !0, S.each(t[e] || [], function(e, t) {
						var n = t(i, o, a);
						return "string" != typeof n || u || s[n] ? u ? !(r = n) : void 0 : (i.dataTypes.unshift(n), l(n), !1)
					}), r
				}
				return l(i.dataTypes[0]) || !s["*"] && l("*")
			}

			function $t(e, t) {
				var n, r, i = S.ajaxSettings.flatOptions || {};
				for (n in t) void 0 !== t[n] && ((i[n] ? e : r || (r = {}))[n] = t[n]);
				return r && S.extend(!0, e, r), e
			}
			Wt.href = Tt.href, S.extend({
				active: 0,
				lastModified: {},
				etag: {},
				ajaxSettings: {
					url: Tt.href,
					type: "GET",
					isLocal: /^(?:about|app|app-storage|.+-extension|file|res|widget):$/.test(Tt.protocol),
					global: !0,
					processData: !0,
					async: !0,
					contentType: "application/x-www-form-urlencoded; charset=UTF-8",
					accepts: {
						"*": It,
						text: "text/plain",
						html: "text/html",
						xml: "application/xml, text/xml",
						json: "application/json, text/javascript"
					},
					contents: {
						xml: /\bxml\b/,
						html: /\bhtml/,
						json: /\bjson\b/
					},
					responseFields: {
						xml: "responseXML",
						text: "responseText",
						json: "responseJSON"
					},
					converters: {
						"* text": String,
						"text html": !0,
						"text json": JSON.parse,
						"text xml": S.parseXML
					},
					flatOptions: {
						url: !0,
						context: !0
					}
				},
				ajaxSetup: function(e, t) {
					return t ? $t($t(e, S.ajaxSettings), t) : $t(S.ajaxSettings, e)
				},
				ajaxPrefilter: Ft(Rt),
				ajaxTransport: Ft(Mt),
				ajax: function(e, t) {
					"object" == typeof e && (t = e, e = void 0), t = t || {};
					var c, f, p, n, d, r, h, g, i, o, v = S.ajaxSetup({}, t),
						y = v.context || v,
						m = v.context && (y.nodeType || y.jquery) ? S(y) : S.event,
						x = S.Deferred(),
						b = S.Callbacks("once memory"),
						w = v.statusCode || {},
						a = {},
						s = {},
						u = "canceled",
						T = {
							readyState: 0,
							getResponseHeader: function(e) {
								var t;
								if (h) {
									if (!n) {
										n = {};
										while (t = Ht.exec(p)) n[t[1].toLowerCase() + " "] = (n[t[1].toLowerCase() + " "] || []).concat(t[2])
									}
									t = n[e.toLowerCase() + " "]
								}
								return null == t ? null : t.join(", ")
							},
							getAllResponseHeaders: function() {
								return h ? p : null
							},
							setRequestHeader: function(e, t) {
								return null == h && (e = s[e.toLowerCase()] = s[e.toLowerCase()] || e, a[e] = t), this
							},
							overrideMimeType: function(e) {
								return null == h && (v.mimeType = e), this
							},
							statusCode: function(e) {
								var t;
								if (e)
									if (h) T.always(e[T.status]);
									else
										for (t in e) w[t] = [w[t], e[t]];
								return this
							},
							abort: function(e) {
								var t = e || u;
								return c && c.abort(t), l(0, t), this
							}
						};
					if (x.promise(T), v.url = ((e || v.url || Tt.href) + "").replace(Pt, Tt.protocol + "//"), v.type = t.method || t.type || v.method || v.type, v.dataTypes = (v.dataType || "*").toLowerCase().match(P) || [""], null == v.crossDomain) {
						r = E.createElement("a");
						try {
							r.href = v.url, r.href = r.href, v.crossDomain = Wt.protocol + "//" + Wt.host != r.protocol + "//" + r.host
						} catch (e) {
							v.crossDomain = !0
						}
					}
					if (v.data && v.processData && "string" != typeof v.data && (v.data = S.param(v.data, v.traditional)), Bt(Rt, v, t, T), h) return T;
					for (i in (g = S.event && v.global) && 0 == S.active++ && S.event.trigger("ajaxStart"), v.type = v.type.toUpperCase(), v.hasContent = !Ot.test(v.type), f = v.url.replace(qt, ""), v.hasContent ? v.data && v.processData && 0 === (v.contentType || "").indexOf("application/x-www-form-urlencoded") && (v.data = v.data.replace(jt, "+")) : (o = v.url.slice(f.length), v.data && (v.processData || "string" == typeof v.data) && (f += (Et.test(f) ? "&" : "?") + v.data, delete v.data), !1 === v.cache && (f = f.replace(Lt, "$1"), o = (Et.test(f) ? "&" : "?") + "_=" + Ct.guid++ + o), v.url = f + o), v.ifModified && (S.lastModified[f] && T.setRequestHeader("If-Modified-Since", S.lastModified[f]), S.etag[f] && T.setRequestHeader("If-None-Match", S.etag[f])), (v.data && v.hasContent && !1 !== v.contentType || t.contentType) && T.setRequestHeader("Content-Type", v.contentType), T.setRequestHeader("Accept", v.dataTypes[0] && v.accepts[v.dataTypes[0]] ? v.accepts[v.dataTypes[0]] + ("*" !== v.dataTypes[0] ? ", " + It + "; q=0.01" : "") : v.accepts["*"]), v.headers) T.setRequestHeader(i, v.headers[i]);
					if (v.beforeSend && (!1 === v.beforeSend.call(y, T, v) || h)) return T.abort();
					if (u = "abort", b.add(v.complete), T.done(v.success), T.fail(v.error), c = Bt(Mt, v, t, T)) {
						if (T.readyState = 1, g && m.trigger("ajaxSend", [T, v]), h) return T;
						v.async && 0 < v.timeout && (d = C.setTimeout(function() {
							T.abort("timeout")
						}, v.timeout));
						try {
							h = !1, c.send(a, l)
						} catch (e) {
							if (h) throw e;
							l(-1, e)
						}
					} else l(-1, "No Transport");

					function l(e, t, n, r) {
						var i, o, a, s, u, l = t;
						h || (h = !0, d && C.clearTimeout(d), c = void 0, p = r || "", T.readyState = 0 < e ? 4 : 0, i = 200 <= e && e < 300 || 304 === e, n && (s = function(e, t, n) {
							var r, i, o, a, s = e.contents,
								u = e.dataTypes;
							while ("*" === u[0]) u.shift(), void 0 === r && (r = e.mimeType || t.getResponseHeader("Content-Type"));
							if (r)
								for (i in s)
									if (s[i] && s[i].test(r)) {
										u.unshift(i);
										break
									}
							if (u[0] in n) o = u[0];
							else {
								for (i in n) {
									if (!u[0] || e.converters[i + " " + u[0]]) {
										o = i;
										break
									}
									a || (a = i)
								}
								o = o || a
							}
							if (o) return o !== u[0] && u.unshift(o), n[o]
						}(v, T, n)), !i && -1 < S.inArray("script", v.dataTypes) && (v.converters["text script"] = function() {}), s = function(e, t, n, r) {
							var i, o, a, s, u, l = {},
								c = e.dataTypes.slice();
							if (c[1])
								for (a in e.converters) l[a.toLowerCase()] = e.converters[a];
							o = c.shift();
							while (o)
								if (e.responseFields[o] && (n[e.responseFields[o]] = t), !u && r && e.dataFilter && (t = e.dataFilter(t, e.dataType)), u = o, o = c.shift())
									if ("*" === o) o = u;
									else if ("*" !== u && u !== o) {
								if (!(a = l[u + " " + o] || l["* " + o]))
									for (i in l)
										if ((s = i.split(" "))[1] === o && (a = l[u + " " + s[0]] || l["* " + s[0]])) {
											!0 === a ? a = l[i] : !0 !== l[i] && (o = s[0], c.unshift(s[1]));
											break
										}
								if (!0 !== a)
									if (a && e["throws"]) t = a(t);
									else try {
										t = a(t)
									} catch (e) {
										return {
											state: "parsererror",
											error: a ? e : "No conversion from " + u + " to " + o
										}
									}
							}
							return {
								state: "success",
								data: t
							}
						}(v, s, T, i), i ? (v.ifModified && ((u = T.getResponseHeader("Last-Modified")) && (S.lastModified[f] = u), (u = T.getResponseHeader("etag")) && (S.etag[f] = u)), 204 === e || "HEAD" === v.type ? l = "nocontent" : 304 === e ? l = "notmodified" : (l = s.state, o = s.data, i = !(a = s.error))) : (a = l, !e && l || (l = "error", e < 0 && (e = 0))), T.status = e, T.statusText = (t || l) + "", i ? x.resolveWith(y, [o, l, T]) : x.rejectWith(y, [T, l, a]), T.statusCode(w), w = void 0, g && m.trigger(i ? "ajaxSuccess" : "ajaxError", [T, v, i ? o : a]), b.fireWith(y, [T, l]), g && (m.trigger("ajaxComplete", [T, v]), --S.active || S.event.trigger("ajaxStop")))
					}
					return T
				},
				getJSON: function(e, t, n) {
					return S.get(e, t, n, "json")
				},
				getScript: function(e, t) {
					return S.get(e, void 0, t, "script")
				}
			}), S.each(["get", "post"], function(e, i) {
				S[i] = function(e, t, n, r) {
					return m(t) && (r = r || n, n = t, t = void 0), S.ajax(S.extend({
						url: e,
						type: i,
						dataType: r,
						data: t,
						success: n
					}, S.isPlainObject(e) && e))
				}
			}), S.ajaxPrefilter(function(e) {
				var t;
				for (t in e.headers) "content-type" === t.toLowerCase() && (e.contentType = e.headers[t] || "")
			}), S._evalUrl = function(e, t, n) {
				return S.ajax({
					url: e,
					type: "GET",
					dataType: "script",
					cache: !0,
					async: !1,
					global: !1,
					converters: {
						"text script": function() {}
					},
					dataFilter: function(e) {
						S.globalEval(e, t, n)
					}
				})
			}, S.fn.extend({
				wrapAll: function(e) {
					var t;
					return this[0] && (m(e) && (e = e.call(this[0])), t = S(e, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && t.insertBefore(this[0]), t.map(function() {
						var e = this;
						while (e.firstElementChild) e = e.firstElementChild;
						return e
					}).append(this)), this
				},
				wrapInner: function(n) {
					return m(n) ? this.each(function(e) {
						S(this).wrapInner(n.call(this, e))
					}) : this.each(function() {
						var e = S(this),
							t = e.contents();
						t.length ? t.wrapAll(n) : e.append(n)
					})
				},
				wrap: function(t) {
					var n = m(t);
					return this.each(function(e) {
						S(this).wrapAll(n ? t.call(this, e) : t)
					})
				},
				unwrap: function(e) {
					return this.parent(e).not("body").each(function() {
						S(this).replaceWith(this.childNodes)
					}), this
				}
			}), S.expr.pseudos.hidden = function(e) {
				return !S.expr.pseudos.visible(e)
			}, S.expr.pseudos.visible = function(e) {
				return !!(e.offsetWidth || e.offsetHeight || e.getClientRects().length)
			}, S.ajaxSettings.xhr = function() {
				try {
					return new C.XMLHttpRequest
				} catch (e) {}
			};
			var _t = {
					0: 200,
					1223: 204
				},
				zt = S.ajaxSettings.xhr();
			y.cors = !!zt && "withCredentials" in zt, y.ajax = zt = !!zt, S.ajaxTransport(function(i) {
				var o, a;
				if (y.cors || zt && !i.crossDomain) return {
					send: function(e, t) {
						var n, r = i.xhr();
						if (r.open(i.type, i.url, i.async, i.username, i.password), i.xhrFields)
							for (n in i.xhrFields) r[n] = i.xhrFields[n];
						for (n in i.mimeType && r.overrideMimeType && r.overrideMimeType(i.mimeType), i.crossDomain || e["X-Requested-With"] || (e["X-Requested-With"] = "XMLHttpRequest"), e) r.setRequestHeader(n, e[n]);
						o = function(e) {
							return function() {
								o && (o = a = r.onload = r.onerror = r.onabort = r.ontimeout = r.onreadystatechange = null, "abort" === e ? r.abort() : "error" === e ? "number" != typeof r.status ? t(0, "error") : t(r.status, r.statusText) : t(_t[r.status] || r.status, r.statusText, "text" !== (r.responseType || "text") || "string" != typeof r.responseText ? {
									binary: r.response
								} : {
									text: r.responseText
								}, r.getAllResponseHeaders()))
							}
						}, r.onload = o(), a = r.onerror = r.ontimeout = o("error"), void 0 !== r.onabort ? r.onabort = a : r.onreadystatechange = function() {
							4 === r.readyState && C.setTimeout(function() {
								o && a()
							})
						}, o = o("abort");
						try {
							r.send(i.hasContent && i.data || null)
						} catch (e) {
							if (o) throw e
						}
					},
					abort: function() {
						o && o()
					}
				}
			}), S.ajaxPrefilter(function(e) {
				e.crossDomain && (e.contents.script = !1)
			}), S.ajaxSetup({
				accepts: {
					script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
				},
				contents: {
					script: /\b(?:java|ecma)script\b/
				},
				converters: {
					"text script": function(e) {
						return S.globalEval(e), e
					}
				}
			}), S.ajaxPrefilter("script", function(e) {
				void 0 === e.cache && (e.cache = !1), e.crossDomain && (e.type = "GET")
			}), S.ajaxTransport("script", function(n) {
				var r, i;
				if (n.crossDomain || n.scriptAttrs) return {
					send: function(e, t) {
						r = S("<script>").attr(n.scriptAttrs || {}).prop({
							charset: n.scriptCharset,
							src: n.url
						}).on("load error", i = function(e) {
							r.remove(), i = null, e && t("error" === e.type ? 404 : 200, e.type)
						}), E.head.appendChild(r[0])
					},
					abort: function() {
						i && i()
					}
				}
			});
			var Ut, Xt = [],
				Vt = /(=)\?(?=&|$)|\?\?/;
			S.ajaxSetup({
				jsonp: "callback",
				jsonpCallback: function() {
					var e = Xt.pop() || S.expando + "_" + Ct.guid++;
					return this[e] = !0, e
				}
			}), S.ajaxPrefilter("json jsonp", function(e, t, n) {
				var r, i, o, a = !1 !== e.jsonp && (Vt.test(e.url) ? "url" : "string" == typeof e.data && 0 === (e.contentType || "").indexOf("application/x-www-form-urlencoded") && Vt.test(e.data) && "data");
				if (a || "jsonp" === e.dataTypes[0]) return r = e.jsonpCallback = m(e.jsonpCallback) ? e.jsonpCallback() : e.jsonpCallback, a ? e[a] = e[a].replace(Vt, "$1" + r) : !1 !== e.jsonp && (e.url += (Et.test(e.url) ? "&" : "?") + e.jsonp + "=" + r), e.converters["script json"] = function() {
					return o || S.error(r + " was not called"), o[0]
				}, e.dataTypes[0] = "json", i = C[r], C[r] = function() {
					o = arguments
				}, n.always(function() {
					void 0 === i ? S(C).removeProp(r) : C[r] = i, e[r] && (e.jsonpCallback = t.jsonpCallback, Xt.push(r)), o && m(i) && i(o[0]), o = i = void 0
				}), "script"
			}), y.createHTMLDocument = ((Ut = E.implementation.createHTMLDocument("").body).innerHTML = "<form></form><form></form>", 2 === Ut.childNodes.length), S.parseHTML = function(e, t, n) {
				return "string" != typeof e ? [] : ("boolean" == typeof t && (n = t, t = !1), t || (y.createHTMLDocument ? ((r = (t = E.implementation.createHTMLDocument("")).createElement("base")).href = E.location.href, t.head.appendChild(r)) : t = E), o = !n && [], (i = N.exec(e)) ? [t.createElement(i[1])] : (i = xe([e], t, o), o && o.length && S(o).remove(), S.merge([], i.childNodes)));
				var r, i, o
			}, S.fn.load = function(e, t, n) {
				var r, i, o, a = this,
					s = e.indexOf(" ");
				return -1 < s && (r = vt(e.slice(s)), e = e.slice(0, s)), m(t) ? (n = t, t = void 0) : t && "object" == typeof t && (i = "POST"), 0 < a.length && S.ajax({
					url: e,
					type: i || "GET",
					dataType: "html",
					data: t
				}).done(function(e) {
					o = arguments, a.html(r ? S("<div>").append(S.parseHTML(e)).find(r) : e)
				}).always(n && function(e, t) {
					a.each(function() {
						n.apply(this, o || [e.responseText, t, e])
					})
				}), this
			}, S.expr.pseudos.animated = function(t) {
				return S.grep(S.timers, function(e) {
					return t === e.elem
				}).length
			}, S.offset = {
				setOffset: function(e, t, n) {
					var r, i, o, a, s, u, l = S.css(e, "position"),
						c = S(e),
						f = {};
					"static" === l && (e.style.position = "relative"), s = c.offset(), o = S.css(e, "top"), u = S.css(e, "left"), ("absolute" === l || "fixed" === l) && -1 < (o + u).indexOf("auto") ? (a = (r = c.position()).top, i = r.left) : (a = parseFloat(o) || 0, i = parseFloat(u) || 0), m(t) && (t = t.call(e, n, S.extend({}, s))), null != t.top && (f.top = t.top - s.top + a), null != t.left && (f.left = t.left - s.left + i), "using" in t ? t.using.call(e, f) : ("number" == typeof f.top && (f.top += "px"), "number" == typeof f.left && (f.left += "px"), c.css(f))
				}
			}, S.fn.extend({
				offset: function(t) {
					if (arguments.length) return void 0 === t ? this : this.each(function(e) {
						S.offset.setOffset(this, t, e)
					});
					var e, n, r = this[0];
					return r ? r.getClientRects().length ? (e = r.getBoundingClientRect(), n = r.ownerDocument.defaultView, {
						top: e.top + n.pageYOffset,
						left: e.left + n.pageXOffset
					}) : {
						top: 0,
						left: 0
					} : void 0
				},
				position: function() {
					if (this[0]) {
						var e, t, n, r = this[0],
							i = {
								top: 0,
								left: 0
							};
						if ("fixed" === S.css(r, "position")) t = r.getBoundingClientRect();
						else {
							t = this.offset(), n = r.ownerDocument, e = r.offsetParent || n.documentElement;
							while (e && (e === n.body || e === n.documentElement) && "static" === S.css(e, "position")) e = e.parentNode;
							e && e !== r && 1 === e.nodeType && ((i = S(e).offset()).top += S.css(e, "borderTopWidth", !0), i.left += S.css(e, "borderLeftWidth", !0))
						}
						return {
							top: t.top - i.top - S.css(r, "marginTop", !0),
							left: t.left - i.left - S.css(r, "marginLeft", !0)
						}
					}
				},
				offsetParent: function() {
					return this.map(function() {
						var e = this.offsetParent;
						while (e && "static" === S.css(e, "position")) e = e.offsetParent;
						return e || re
					})
				}
			}), S.each({
				scrollLeft: "pageXOffset",
				scrollTop: "pageYOffset"
			}, function(t, i) {
				var o = "pageYOffset" === i;
				S.fn[t] = function(e) {
					return $(this, function(e, t, n) {
						var r;
						if (x(e) ? r = e : 9 === e.nodeType && (r = e.defaultView), void 0 === n) return r ? r[i] : e[t];
						r ? r.scrollTo(o ? r.pageXOffset : n, o ? n : r.pageYOffset) : e[t] = n
					}, t, e, arguments.length)
				}
			}), S.each(["top", "left"], function(e, n) {
				S.cssHooks[n] = $e(y.pixelPosition, function(e, t) {
					if (t) return t = Be(e, n), Me.test(t) ? S(e).position()[n] + "px" : t
				})
			}), S.each({
				Height: "height",
				Width: "width"
			}, function(a, s) {
				S.each({
					padding: "inner" + a,
					content: s,
					"": "outer" + a
				}, function(r, o) {
					S.fn[o] = function(e, t) {
						var n = arguments.length && (r || "boolean" != typeof e),
							i = r || (!0 === e || !0 === t ? "margin" : "border");
						return $(this, function(e, t, n) {
							var r;
							return x(e) ? 0 === o.indexOf("outer") ? e["inner" + a] : e.document.documentElement["client" + a] : 9 === e.nodeType ? (r = e.documentElement, Math.max(e.body["scroll" + a], r["scroll" + a], e.body["offset" + a], r["offset" + a], r["client" + a])) : void 0 === n ? S.css(e, t, i) : S.style(e, t, n, i)
						}, s, n ? e : void 0, n)
					}
				})
			}), S.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function(e, t) {
				S.fn[t] = function(e) {
					return this.on(t, e)
				}
			}), S.fn.extend({
				bind: function(e, t, n) {
					return this.on(e, null, t, n)
				},
				unbind: function(e, t) {
					return this.off(e, null, t)
				},
				delegate: function(e, t, n, r) {
					return this.on(t, e, n, r)
				},
				undelegate: function(e, t, n) {
					return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", n)
				},
				hover: function(e, t) {
					return this.mouseenter(e).mouseleave(t || e)
				}
			}), S.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "), function(e, n) {
				S.fn[n] = function(e, t) {
					return 0 < arguments.length ? this.on(n, null, e, t) : this.trigger(n)
				}
			});
			var Gt = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
			S.proxy = function(e, t) {
				var n, r, i;
				if ("string" == typeof t && (n = e[t], t = e, e = n), m(e)) return r = s.call(arguments, 2), (i = function() {
					return e.apply(t || this, r.concat(s.call(arguments)))
				}).guid = e.guid = e.guid || S.guid++, i
			}, S.holdReady = function(e) {
				e ? S.readyWait++ : S.ready(!0)
			}, S.isArray = Array.isArray, S.parseJSON = JSON.parse, S.nodeName = A, S.isFunction = m, S.isWindow = x, S.camelCase = X, S.type = w, S.now = Date.now, S.isNumeric = function(e) {
				var t = S.type(e);
				return ("number" === t || "string" === t) && !isNaN(e - parseFloat(e))
			}, S.trim = function(e) {
				return null == e ? "" : (e + "").replace(Gt, "")
			}, "function" == typeof define && define.amd && define("jquery", [], function() {
				return S
			});
			var Yt = C.jQuery,
				Qt = C.$;
			return S.noConflict = function(e) {
				return C.$ === S && (C.$ = Qt), e && C.jQuery === S && (C.jQuery = Yt), S
			}, "undefined" == typeof e && (C.jQuery = C.$ = S), S
		});
	</script>
	<script>
		/*!
		 * Webflow: Front-end site library
		 * @license MIT
		 * Inline scripts may access the api using an async handler:
		 *   var Webflow = Webflow || [];
		 *   Webflow.push(readyFunction);
		 */
		! function(t) {
			var e = {};

			function n(i) {
				if (e[i]) return e[i].exports;
				var r = e[i] = {
					i: i,
					l: !1,
					exports: {}
				};
				return t[i].call(r.exports, r, r.exports, n), r.l = !0, r.exports
			}
			n.m = t, n.c = e, n.d = function(t, e, i) {
				n.o(t, e) || Object.defineProperty(t, e, {
					enumerable: !0,
					get: i
				})
			}, n.r = function(t) {
				"undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, {
					value: "Module"
				}), Object.defineProperty(t, "__esModule", {
					value: !0
				})
			}, n.t = function(t, e) {
				if (1 & e && (t = n(t)), 8 & e) return t;
				if (4 & e && "object" == typeof t && t && t.__esModule) return t;
				var i = Object.create(null);
				if (n.r(i), Object.defineProperty(i, "default", {
						enumerable: !0,
						value: t
					}), 2 & e && "string" != typeof t)
					for (var r in t) n.d(i, r, function(e) {
						return t[e]
					}.bind(null, r));
				return i
			}, n.n = function(t) {
				var e = t && t.__esModule ? function() {
					return t.default
				} : function() {
					return t
				};
				return n.d(e, "a", e), e
			}, n.o = function(t, e) {
				return Object.prototype.hasOwnProperty.call(t, e)
			}, n.p = "", n(n.s = 2)
		}([function(t, e, n) {
				"use strict";
				var i = {},
					r = {},
					o = [],
					a = window.Webflow || [],
					s = window.jQuery,
					u = s(window),
					c = s(document),
					l = s.isFunction,
					h = i._ = n(4),
					f = i.tram = n(1) && s.tram,
					d = !1,
					p = !1;

				function v(t) {
					i.env() && (l(t.design) && u.on("__wf_design", t.design), l(t.preview) && u.on("__wf_preview", t.preview)), l(t.destroy) && u.on("__wf_destroy", t.destroy), t.ready && l(t.ready) && function(t) {
						if (d) return void t.ready();
						if (h.contains(o, t.ready)) return;
						o.push(t.ready)
					}(t)
				}

				function m(t) {
					l(t.design) && u.off("__wf_design", t.design), l(t.preview) && u.off("__wf_preview", t.preview), l(t.destroy) && u.off("__wf_destroy", t.destroy), t.ready && l(t.ready) && function(t) {
						o = h.filter(o, function(e) {
							return e !== t.ready
						})
					}(t)
				}
				f.config.hideBackface = !1, f.config.keepInherited = !0, i.define = function(t, e, n) {
					r[t] && m(r[t]);
					var i = r[t] = e(s, h, n) || {};
					return v(i), i
				}, i.require = function(t) {
					return r[t]
				}, i.push = function(t) {
					d ? l(t) && t() : a.push(t)
				}, i.env = function(t) {
					var e = window.__wf_design,
						n = void 0 !== e;
					return t ? "design" === t ? n && e : "preview" === t ? n && !e : "slug" === t ? n && window.__wf_slug : "editor" === t ? window.WebflowEditor : "test" === t ? window.__wf_test : "frame" === t ? window !== window.top : void 0 : n
				};
				var w, b = navigator.userAgent.toLowerCase(),
					g = i.env.touch = "ontouchstart" in window || window.DocumentTouch && document instanceof window.DocumentTouch,
					y = i.env.chrome = /chrome/.test(b) && /Google/.test(navigator.vendor) && parseInt(b.match(/chrome\/(\d+)\./)[1], 10),
					_ = i.env.ios = /(ipod|iphone|ipad)/.test(b);
				i.env.safari = /safari/.test(b) && !y && !_, g && c.on("touchstart mousedown", function(t) {
					w = t.target
				}), i.validClick = g ? function(t) {
					return t === w || s.contains(t, w)
				} : function() {
					return !0
				};
				var x, k = "resize.webflow orientationchange.webflow load.webflow";

				function S(t, e) {
					var n = [],
						i = {};
					return i.up = h.throttle(function(t) {
						h.each(n, function(e) {
							e(t)
						})
					}), t && e && t.on(e, i.up), i.on = function(t) {
						"function" == typeof t && (h.contains(n, t) || n.push(t))
					}, i.off = function(t) {
						n = arguments.length ? h.filter(n, function(e) {
							return e !== t
						}) : []
					}, i
				}

				function z(t) {
					l(t) && t()
				}

				function E() {
					x && (x.reject(), u.off("load", x.resolve)), x = new s.Deferred, u.on("load", x.resolve)
				}
				i.resize = S(u, k), i.scroll = S(u, "scroll.webflow resize.webflow orientationchange.webflow load.webflow"), i.redraw = S(), i.location = function(t) {
					window.location = t
				}, i.env() && (i.location = function() {}), i.ready = function() {
					d = !0, p ? (p = !1, h.each(r, v)) : h.each(o, z), h.each(a, z), i.resize.up()
				}, i.load = function(t) {
					x.then(t)
				}, i.destroy = function(t) {
					t = t || {}, p = !0, u.triggerHandler("__wf_destroy"), null != t.domready && (d = t.domready), h.each(r, m), i.resize.off(), i.scroll.off(), i.redraw.off(), o = [], a = [], "pending" === x.state() && E()
				}, s(i.ready), E(), t.exports = window.Webflow = i
			}, function(t, e, n) {
				"use strict";
				var i = n(5)(n(6));
				window.tram = function(t) {
					function e(t, e) {
						return (new I.Bare).init(t, e)
					}

					function n(t) {
						return t.replace(/[A-Z]/g, function(t) {
							return "-" + t.toLowerCase()
						})
					}

					function r(t) {
						var e = parseInt(t.slice(1), 16);
						return [e >> 16 & 255, e >> 8 & 255, 255 & e]
					}

					function o(t, e, n) {
						return "#" + (1 << 24 | t << 16 | e << 8 | n).toString(16).slice(1)
					}

					function a() {}

					function s(t, e, n) {
						c("Units do not match [" + t + "]: " + e + ", " + n)
					}

					function u(t, e, n) {
						if (void 0 !== e && (n = e), void 0 === t) return n;
						var i = n;
						return Q.test(t) || !J.test(t) ? i = parseInt(t, 10) : J.test(t) && (i = 1e3 * parseFloat(t)), 0 > i && (i = 0), i == i ? i : n
					}

					function c(t) {
						N.debug && window && window.console.warn(t)
					}
					var l = function(t, e, n) {
							function r(t) {
								return "object" == (0, i.default)(t)
							}

							function o(t) {
								return "function" == typeof t
							}

							function a() {}
							return function i(s, u) {
								function c() {
									var t = new l;
									return o(t.init) && t.init.apply(t, arguments), t
								}

								function l() {}
								u === n && (u = s, s = Object), c.Bare = l;
								var h, f = a[t] = s[t],
									d = l[t] = c[t] = new a;
								return d.constructor = c, c.mixin = function(e) {
									return l[t] = c[t] = i(c, e)[t], c
								}, c.open = function(t) {
									if (h = {}, o(t) ? h = t.call(c, d, f, c, s) : r(t) && (h = t), r(h))
										for (var n in h) e.call(h, n) && (d[n] = h[n]);
									return o(d.init) || (d.init = s), c
								}, c.open(u)
							}
						}("prototype", {}.hasOwnProperty),
						h = {
							ease: ["ease", function(t, e, n, i) {
								var r = (t /= i) * t,
									o = r * t;
								return e + n * (-2.75 * o * r + 11 * r * r + -15.5 * o + 8 * r + .25 * t)
							}],
							"ease-in": ["ease-in", function(t, e, n, i) {
								var r = (t /= i) * t,
									o = r * t;
								return e + n * (-1 * o * r + 3 * r * r + -3 * o + 2 * r)
							}],
							"ease-out": ["ease-out", function(t, e, n, i) {
								var r = (t /= i) * t,
									o = r * t;
								return e + n * (.3 * o * r + -1.6 * r * r + 2.2 * o + -1.8 * r + 1.9 * t)
							}],
							"ease-in-out": ["ease-in-out", function(t, e, n, i) {
								var r = (t /= i) * t,
									o = r * t;
								return e + n * (2 * o * r + -5 * r * r + 2 * o + 2 * r)
							}],
							linear: ["linear", function(t, e, n, i) {
								return n * t / i + e
							}],
							"ease-in-quad": ["cubic-bezier(0.550, 0.085, 0.680, 0.530)", function(t, e, n, i) {
								return n * (t /= i) * t + e
							}],
							"ease-out-quad": ["cubic-bezier(0.250, 0.460, 0.450, 0.940)", function(t, e, n, i) {
								return -n * (t /= i) * (t - 2) + e
							}],
							"ease-in-out-quad": ["cubic-bezier(0.455, 0.030, 0.515, 0.955)", function(t, e, n, i) {
								return (t /= i / 2) < 1 ? n / 2 * t * t + e : -n / 2 * (--t * (t - 2) - 1) + e
							}],
							"ease-in-cubic": ["cubic-bezier(0.550, 0.055, 0.675, 0.190)", function(t, e, n, i) {
								return n * (t /= i) * t * t + e
							}],
							"ease-out-cubic": ["cubic-bezier(0.215, 0.610, 0.355, 1)", function(t, e, n, i) {
								return n * ((t = t / i - 1) * t * t + 1) + e
							}],
							"ease-in-out-cubic": ["cubic-bezier(0.645, 0.045, 0.355, 1)", function(t, e, n, i) {
								return (t /= i / 2) < 1 ? n / 2 * t * t * t + e : n / 2 * ((t -= 2) * t * t + 2) + e
							}],
							"ease-in-quart": ["cubic-bezier(0.895, 0.030, 0.685, 0.220)", function(t, e, n, i) {
								return n * (t /= i) * t * t * t + e
							}],
							"ease-out-quart": ["cubic-bezier(0.165, 0.840, 0.440, 1)", function(t, e, n, i) {
								return -n * ((t = t / i - 1) * t * t * t - 1) + e
							}],
							"ease-in-out-quart": ["cubic-bezier(0.770, 0, 0.175, 1)", function(t, e, n, i) {
								return (t /= i / 2) < 1 ? n / 2 * t * t * t * t + e : -n / 2 * ((t -= 2) * t * t * t - 2) + e
							}],
							"ease-in-quint": ["cubic-bezier(0.755, 0.050, 0.855, 0.060)", function(t, e, n, i) {
								return n * (t /= i) * t * t * t * t + e
							}],
							"ease-out-quint": ["cubic-bezier(0.230, 1, 0.320, 1)", function(t, e, n, i) {
								return n * ((t = t / i - 1) * t * t * t * t + 1) + e
							}],
							"ease-in-out-quint": ["cubic-bezier(0.860, 0, 0.070, 1)", function(t, e, n, i) {
								return (t /= i / 2) < 1 ? n / 2 * t * t * t * t * t + e : n / 2 * ((t -= 2) * t * t * t * t + 2) + e
							}],
							"ease-in-sine": ["cubic-bezier(0.470, 0, 0.745, 0.715)", function(t, e, n, i) {
								return -n * Math.cos(t / i * (Math.PI / 2)) + n + e
							}],
							"ease-out-sine": ["cubic-bezier(0.390, 0.575, 0.565, 1)", function(t, e, n, i) {
								return n * Math.sin(t / i * (Math.PI / 2)) + e
							}],
							"ease-in-out-sine": ["cubic-bezier(0.445, 0.050, 0.550, 0.950)", function(t, e, n, i) {
								return -n / 2 * (Math.cos(Math.PI * t / i) - 1) + e
							}],
							"ease-in-expo": ["cubic-bezier(0.950, 0.050, 0.795, 0.035)", function(t, e, n, i) {
								return 0 === t ? e : n * Math.pow(2, 10 * (t / i - 1)) + e
							}],
							"ease-out-expo": ["cubic-bezier(0.190, 1, 0.220, 1)", function(t, e, n, i) {
								return t === i ? e + n : n * (1 - Math.pow(2, -10 * t / i)) + e
							}],
							"ease-in-out-expo": ["cubic-bezier(1, 0, 0, 1)", function(t, e, n, i) {
								return 0 === t ? e : t === i ? e + n : (t /= i / 2) < 1 ? n / 2 * Math.pow(2, 10 * (t - 1)) + e : n / 2 * (2 - Math.pow(2, -10 * --t)) + e
							}],
							"ease-in-circ": ["cubic-bezier(0.600, 0.040, 0.980, 0.335)", function(t, e, n, i) {
								return -n * (Math.sqrt(1 - (t /= i) * t) - 1) + e
							}],
							"ease-out-circ": ["cubic-bezier(0.075, 0.820, 0.165, 1)", function(t, e, n, i) {
								return n * Math.sqrt(1 - (t = t / i - 1) * t) + e
							}],
							"ease-in-out-circ": ["cubic-bezier(0.785, 0.135, 0.150, 0.860)", function(t, e, n, i) {
								return (t /= i / 2) < 1 ? -n / 2 * (Math.sqrt(1 - t * t) - 1) + e : n / 2 * (Math.sqrt(1 - (t -= 2) * t) + 1) + e
							}],
							"ease-in-back": ["cubic-bezier(0.600, -0.280, 0.735, 0.045)", function(t, e, n, i, r) {
								return void 0 === r && (r = 1.70158), n * (t /= i) * t * ((r + 1) * t - r) + e
							}],
							"ease-out-back": ["cubic-bezier(0.175, 0.885, 0.320, 1.275)", function(t, e, n, i, r) {
								return void 0 === r && (r = 1.70158), n * ((t = t / i - 1) * t * ((r + 1) * t + r) + 1) + e
							}],
							"ease-in-out-back": ["cubic-bezier(0.680, -0.550, 0.265, 1.550)", function(t, e, n, i, r) {
								return void 0 === r && (r = 1.70158), (t /= i / 2) < 1 ? n / 2 * t * t * ((1 + (r *= 1.525)) * t - r) + e : n / 2 * ((t -= 2) * t * ((1 + (r *= 1.525)) * t + r) + 2) + e
							}]
						},
						f = {
							"ease-in-back": "cubic-bezier(0.600, 0, 0.735, 0.045)",
							"ease-out-back": "cubic-bezier(0.175, 0.885, 0.320, 1)",
							"ease-in-out-back": "cubic-bezier(0.680, 0, 0.265, 1)"
						},
						d = document,
						p = window,
						v = "bkwld-tram",
						m = /[\-\.0-9]/g,
						w = /[A-Z]/,
						b = "number",
						g = /^(rgb|#)/,
						y = /(em|cm|mm|in|pt|pc|px)$/,
						_ = /(em|cm|mm|in|pt|pc|px|%)$/,
						x = /(deg|rad|turn)$/,
						k = "unitless",
						S = /(all|none) 0s ease 0s/,
						z = /^(width|height)$/,
						E = " ",
						j = d.createElement("a"),
						M = ["Webkit", "Moz", "O", "ms"],
						$ = ["-webkit-", "-moz-", "-o-", "-ms-"],
						q = function(t) {
							if (t in j.style) return {
								dom: t,
								css: t
							};
							var e, n, i = "",
								r = t.split("-");
							for (e = 0; e < r.length; e++) i += r[e].charAt(0).toUpperCase() + r[e].slice(1);
							for (e = 0; e < M.length; e++)
								if ((n = M[e] + i) in j.style) return {
									dom: n,
									css: $[e] + t
								}
						},
						A = e.support = {
							bind: Function.prototype.bind,
							transform: q("transform"),
							transition: q("transition"),
							backface: q("backface-visibility"),
							timing: q("transition-timing-function")
						};
					if (A.transition) {
						var L = A.timing.dom;
						if (j.style[L] = h["ease-in-back"][0], !j.style[L])
							for (var F in f) h[F][0] = f[F]
					}
					var O = e.frame = function() {
							var t = p.requestAnimationFrame || p.webkitRequestAnimationFrame || p.mozRequestAnimationFrame || p.oRequestAnimationFrame || p.msRequestAnimationFrame;
							return t && A.bind ? t.bind(p) : function(t) {
								p.setTimeout(t, 16)
							}
						}(),
						T = e.now = function() {
							var t = p.performance,
								e = t && (t.now || t.webkitNow || t.msNow || t.mozNow);
							return e && A.bind ? e.bind(t) : Date.now || function() {
								return +new Date
							}
						}(),
						C = l(function(e) {
							function r(t, e) {
								var n = function(t) {
										for (var e = -1, n = t ? t.length : 0, i = []; ++e < n;) {
											var r = t[e];
											r && i.push(r)
										}
										return i
									}(("" + t).split(E)),
									i = n[0];
								e = e || {};
								var r = Y[i];
								if (!r) return c("Unsupported property: " + i);
								if (!e.weak || !this.props[i]) {
									var o = r[0],
										a = this.props[i];
									return a || (a = this.props[i] = new o.Bare), a.init(this.$el, n, r, e), a
								}
							}

							function o(t, e, n) {
								if (t) {
									var o = (0, i.default)(t);
									if (e || (this.timer && this.timer.destroy(), this.queue = [], this.active = !1), "number" == o && e) return this.timer = new H({
										duration: t,
										context: this,
										complete: a
									}), void(this.active = !0);
									if ("string" == o && e) {
										switch (t) {
											case "hide":
												l.call(this);
												break;
											case "stop":
												s.call(this);
												break;
											case "redraw":
												h.call(this);
												break;
											default:
												r.call(this, t, n && n[1])
										}
										return a.call(this)
									}
									if ("function" == o) return void t.call(this, this);
									if ("object" == o) {
										var c = 0;
										d.call(this, t, function(t, e) {
											t.span > c && (c = t.span), t.stop(), t.animate(e)
										}, function(t) {
											"wait" in t && (c = u(t.wait, 0))
										}), f.call(this), c > 0 && (this.timer = new H({
											duration: c,
											context: this
										}), this.active = !0, e && (this.timer.complete = a));
										var p = this,
											v = !1,
											m = {};
										O(function() {
											d.call(p, t, function(t) {
												t.active && (v = !0, m[t.name] = t.nextStyle)
											}), v && p.$el.css(m)
										})
									}
								}
							}

							function a() {
								if (this.timer && this.timer.destroy(), this.active = !1, this.queue.length) {
									var t = this.queue.shift();
									o.call(this, t.options, !0, t.args)
								}
							}

							function s(t) {
								var e;
								this.timer && this.timer.destroy(), this.queue = [], this.active = !1, "string" == typeof t ? (e = {})[t] = 1 : e = "object" == (0, i.default)(t) && null != t ? t : this.props, d.call(this, e, p), f.call(this)
							}

							function l() {
								s.call(this), this.el.style.display = "none"
							}

							function h() {
								this.el.offsetHeight
							}

							function f() {
								var t, e, n = [];
								for (t in this.upstream && n.push(this.upstream), this.props)(e = this.props[t]).active && n.push(e.string);
								n = n.join(","), this.style !== n && (this.style = n, this.el.style[A.transition.dom] = n)
							}

							function d(t, e, i) {
								var o, a, s, u, c = e !== p,
									l = {};
								for (o in t) s = t[o], o in K ? (l.transform || (l.transform = {}), l.transform[o] = s) : (w.test(o) && (o = n(o)), o in Y ? l[o] = s : (u || (u = {}), u[o] = s));
								for (o in l) {
									if (s = l[o], !(a = this.props[o])) {
										if (!c) continue;
										a = r.call(this, o)
									}
									e.call(this, a, s)
								}
								i && u && i.call(this, u)
							}

							function p(t) {
								t.stop()
							}

							function m(t, e) {
								t.set(e)
							}

							function b(t) {
								this.$el.css(t)
							}

							function g(t, n) {
								e[t] = function() {
									return this.children ? function(t, e) {
										var n, i = this.children.length;
										for (n = 0; i > n; n++) t.apply(this.children[n], e);
										return this
									}.call(this, n, arguments) : (this.el && n.apply(this, arguments), this)
								}
							}
							e.init = function(e) {
								if (this.$el = t(e), this.el = this.$el[0], this.props = {}, this.queue = [], this.style = "", this.active = !1, N.keepInherited && !N.fallback) {
									var n = Z(this.el, "transition");
									n && !S.test(n) && (this.upstream = n)
								}
								A.backface && N.hideBackface && X(this.el, A.backface.css, "hidden")
							}, g("add", r), g("start", o), g("wait", function(t) {
								t = u(t, 0), this.active ? this.queue.push({
									options: t
								}) : (this.timer = new H({
									duration: t,
									context: this,
									complete: a
								}), this.active = !0)
							}), g("then", function(t) {
								return this.active ? (this.queue.push({
									options: t,
									args: arguments
								}), void(this.timer.complete = a)) : c("No active transition timer. Use start() or wait() before then().")
							}), g("next", a), g("stop", s), g("set", function(t) {
								s.call(this, t), d.call(this, t, m, b)
							}), g("show", function(t) {
								"string" != typeof t && (t = "block"), this.el.style.display = t
							}), g("hide", l), g("redraw", h), g("destroy", function() {
								s.call(this), t.removeData(this.el, v), this.$el = this.el = null
							})
						}),
						I = l(C, function(e) {
							function n(e, n) {
								var i = t.data(e, v) || t.data(e, v, new C.Bare);
								return i.el || i.init(e), n ? i.start(n) : i
							}
							e.init = function(e, i) {
								var r = t(e);
								if (!r.length) return this;
								if (1 === r.length) return n(r[0], i);
								var o = [];
								return r.each(function(t, e) {
									o.push(n(e, i))
								}), this.children = o, this
							}
						}),
						P = l(function(t) {
							function e() {
								var t = this.get();
								this.update("auto");
								var e = this.get();
								return this.update(t), e
							}

							function n(t) {
								var e = /rgba?\((\d+),\s*(\d+),\s*(\d+)/.exec(t);
								return (e ? o(e[1], e[2], e[3]) : t).replace(/#(\w)(\w)(\w)$/, "#$1$1$2$2$3$3")
							}
							var r = 500,
								a = "ease",
								s = 0;
							t.init = function(t, e, n, i) {
								this.$el = t, this.el = t[0];
								var o = e[0];
								n[2] && (o = n[2]), U[o] && (o = U[o]), this.name = o, this.type = n[1], this.duration = u(e[1], this.duration, r), this.ease = function(t, e, n) {
									return void 0 !== e && (n = e), t in h ? t : n
								}(e[2], this.ease, a), this.delay = u(e[3], this.delay, s), this.span = this.duration + this.delay, this.active = !1, this.nextStyle = null, this.auto = z.test(this.name), this.unit = i.unit || this.unit || N.defaultUnit, this.angle = i.angle || this.angle || N.defaultAngle, N.fallback || i.fallback ? this.animate = this.fallback : (this.animate = this.transition, this.string = this.name + E + this.duration + "ms" + ("ease" != this.ease ? E + h[this.ease][0] : "") + (this.delay ? E + this.delay + "ms" : ""))
							}, t.set = function(t) {
								t = this.convert(t, this.type), this.update(t), this.redraw()
							}, t.transition = function(t) {
								this.active = !0, t = this.convert(t, this.type), this.auto && ("auto" == this.el.style[this.name] && (this.update(this.get()), this.redraw()), "auto" == t && (t = e.call(this))), this.nextStyle = t
							}, t.fallback = function(t) {
								var n = this.el.style[this.name] || this.convert(this.get(), this.type);
								t = this.convert(t, this.type), this.auto && ("auto" == n && (n = this.convert(this.get(), this.type)), "auto" == t && (t = e.call(this))), this.tween = new B({
									from: n,
									to: t,
									duration: this.duration,
									delay: this.delay,
									ease: this.ease,
									update: this.update,
									context: this
								})
							}, t.get = function() {
								return Z(this.el, this.name)
							}, t.update = function(t) {
								X(this.el, this.name, t)
							}, t.stop = function() {
								(this.active || this.nextStyle) && (this.active = !1, this.nextStyle = null, X(this.el, this.name, this.get()));
								var t = this.tween;
								t && t.context && t.destroy()
							}, t.convert = function(t, e) {
								if ("auto" == t && this.auto) return t;
								var r, o = "number" == typeof t,
									a = "string" == typeof t;
								switch (e) {
									case b:
										if (o) return t;
										if (a && "" === t.replace(m, "")) return +t;
										r = "number(unitless)";
										break;
									case g:
										if (a) {
											if ("" === t && this.original) return this.original;
											if (e.test(t)) return "#" == t.charAt(0) && 7 == t.length ? t : n(t)
										}
										r = "hex or rgb string";
										break;
									case y:
										if (o) return t + this.unit;
										if (a && e.test(t)) return t;
										r = "number(px) or string(unit)";
										break;
									case _:
										if (o) return t + this.unit;
										if (a && e.test(t)) return t;
										r = "number(px) or string(unit or %)";
										break;
									case x:
										if (o) return t + this.angle;
										if (a && e.test(t)) return t;
										r = "number(deg) or string(angle)";
										break;
									case k:
										if (o) return t;
										if (a && _.test(t)) return t;
										r = "number(unitless) or string(unit or %)"
								}
								return function(t, e) {
									c("Type warning: Expected: [" + t + "] Got: [" + (0, i.default)(e) + "] " + e)
								}(r, t), t
							}, t.redraw = function() {
								this.el.offsetHeight
							}
						}),
						R = l(P, function(t, e) {
							t.init = function() {
								e.init.apply(this, arguments), this.original || (this.original = this.convert(this.get(), g))
							}
						}),
						W = l(P, function(t, e) {
							t.init = function() {
								e.init.apply(this, arguments), this.animate = this.fallback
							}, t.get = function() {
								return this.$el[this.name]()
							}, t.update = function(t) {
								this.$el[this.name](t)
							}
						}),
						D = l(P, function(t, e) {
							function n(t, e) {
								var n, i, r, o, a;
								for (n in t) r = (o = K[n])[0], i = o[1] || n, a = this.convert(t[n], r), e.call(this, i, a, r)
							}
							t.init = function() {
								e.init.apply(this, arguments), this.current || (this.current = {}, K.perspective && N.perspective && (this.current.perspective = N.perspective, X(this.el, this.name, this.style(this.current)), this.redraw()))
							}, t.set = function(t) {
								n.call(this, t, function(t, e) {
									this.current[t] = e
								}), X(this.el, this.name, this.style(this.current)), this.redraw()
							}, t.transition = function(t) {
								var e = this.values(t);
								this.tween = new G({
									current: this.current,
									values: e,
									duration: this.duration,
									delay: this.delay,
									ease: this.ease
								});
								var n, i = {};
								for (n in this.current) i[n] = n in e ? e[n] : this.current[n];
								this.active = !0, this.nextStyle = this.style(i)
							}, t.fallback = function(t) {
								var e = this.values(t);
								this.tween = new G({
									current: this.current,
									values: e,
									duration: this.duration,
									delay: this.delay,
									ease: this.ease,
									update: this.update,
									context: this
								})
							}, t.update = function() {
								X(this.el, this.name, this.style(this.current))
							}, t.style = function(t) {
								var e, n = "";
								for (e in t) n += e + "(" + t[e] + ") ";
								return n
							}, t.values = function(t) {
								var e, i = {};
								return n.call(this, t, function(t, n, r) {
									i[t] = n, void 0 === this.current[t] && (e = 0, ~t.indexOf("scale") && (e = 1), this.current[t] = this.convert(e, r))
								}), i
							}
						}),
						B = l(function(e) {
							function n() {
								var t, e, i, r = u.length;
								if (r)
									for (O(n), e = T(), t = r; t--;)(i = u[t]) && i.render(e)
							}
							var i = {
								ease: h.ease[1],
								from: 0,
								to: 1
							};
							e.init = function(t) {
								this.duration = t.duration || 0, this.delay = t.delay || 0;
								var e = t.ease || i.ease;
								h[e] && (e = h[e][1]), "function" != typeof e && (e = i.ease), this.ease = e, this.update = t.update || a, this.complete = t.complete || a, this.context = t.context || this, this.name = t.name;
								var n = t.from,
									r = t.to;
								void 0 === n && (n = i.from), void 0 === r && (r = i.to), this.unit = t.unit || "", "number" == typeof n && "number" == typeof r ? (this.begin = n, this.change = r - n) : this.format(r, n), this.value = this.begin + this.unit, this.start = T(), !1 !== t.autoplay && this.play()
							}, e.play = function() {
								var t;
								this.active || (this.start || (this.start = T()), this.active = !0, t = this, 1 === u.push(t) && O(n))
							}, e.stop = function() {
								var e, n, i;
								this.active && (this.active = !1, e = this, (i = t.inArray(e, u)) >= 0 && (n = u.slice(i + 1), u.length = i, n.length && (u = u.concat(n))))
							}, e.render = function(t) {
								var e, n = t - this.start;
								if (this.delay) {
									if (n <= this.delay) return;
									n -= this.delay
								}
								if (n < this.duration) {
									var i = this.ease(n, 0, 1, this.duration);
									return e = this.startRGB ? function(t, e, n) {
										return o(t[0] + n * (e[0] - t[0]), t[1] + n * (e[1] - t[1]), t[2] + n * (e[2] - t[2]))
									}(this.startRGB, this.endRGB, i) : function(t) {
										return Math.round(t * c) / c
									}(this.begin + i * this.change), this.value = e + this.unit, void this.update.call(this.context, this.value)
								}
								e = this.endHex || this.begin + this.change, this.value = e + this.unit, this.update.call(this.context, this.value), this.complete.call(this.context), this.destroy()
							}, e.format = function(t, e) {
								if (e += "", "#" == (t += "").charAt(0)) return this.startRGB = r(e), this.endRGB = r(t), this.endHex = t, this.begin = 0, void(this.change = 1);
								if (!this.unit) {
									var n = e.replace(m, "");
									n !== t.replace(m, "") && s("tween", e, t), this.unit = n
								}
								e = parseFloat(e), t = parseFloat(t), this.begin = this.value = e, this.change = t - e
							}, e.destroy = function() {
								this.stop(), this.context = null, this.ease = this.update = this.complete = a
							};
							var u = [],
								c = 1e3
						}),
						H = l(B, function(t) {
							t.init = function(t) {
								this.duration = t.duration || 0, this.complete = t.complete || a, this.context = t.context, this.play()
							}, t.render = function(t) {
								t - this.start < this.duration || (this.complete.call(this.context), this.destroy())
							}
						}),
						G = l(B, function(t, e) {
							t.init = function(t) {
								var e, n;
								for (e in this.context = t.context, this.update = t.update, this.tweens = [], this.current = t.current, t.values) n = t.values[e], this.current[e] !== n && this.tweens.push(new B({
									name: e,
									from: this.current[e],
									to: n,
									duration: t.duration,
									delay: t.delay,
									ease: t.ease,
									autoplay: !1
								}));
								this.play()
							}, t.render = function(t) {
								var e, n, i = !1;
								for (e = this.tweens.length; e--;)(n = this.tweens[e]).context && (n.render(t), this.current[n.name] = n.value, i = !0);
								return i ? void(this.update && this.update.call(this.context)) : this.destroy()
							}, t.destroy = function() {
								if (e.destroy.call(this), this.tweens) {
									var t;
									for (t = this.tweens.length; t--;) this.tweens[t].destroy();
									this.tweens = null, this.current = null
								}
							}
						}),
						N = e.config = {
							debug: !1,
							defaultUnit: "px",
							defaultAngle: "deg",
							keepInherited: !1,
							hideBackface: !1,
							perspective: "",
							fallback: !A.transition,
							agentTests: []
						};
					e.fallback = function(t) {
						if (!A.transition) return N.fallback = !0;
						N.agentTests.push("(" + t + ")");
						var e = new RegExp(N.agentTests.join("|"), "i");
						N.fallback = e.test(navigator.userAgent)
					}, e.fallback("6.0.[2-5] Safari"), e.tween = function(t) {
						return new B(t)
					}, e.delay = function(t, e, n) {
						return new H({
							complete: e,
							duration: t,
							context: n
						})
					}, t.fn.tram = function(t) {
						return e.call(null, this, t)
					};
					var X = t.style,
						Z = t.css,
						U = {
							transform: A.transform && A.transform.css
						},
						Y = {
							color: [R, g],
							background: [R, g, "background-color"],
							"outline-color": [R, g],
							"border-color": [R, g],
							"border-top-color": [R, g],
							"border-right-color": [R, g],
							"border-bottom-color": [R, g],
							"border-left-color": [R, g],
							"border-width": [P, y],
							"border-top-width": [P, y],
							"border-right-width": [P, y],
							"border-bottom-width": [P, y],
							"border-left-width": [P, y],
							"border-spacing": [P, y],
							"letter-spacing": [P, y],
							margin: [P, y],
							"margin-top": [P, y],
							"margin-right": [P, y],
							"margin-bottom": [P, y],
							"margin-left": [P, y],
							padding: [P, y],
							"padding-top": [P, y],
							"padding-right": [P, y],
							"padding-bottom": [P, y],
							"padding-left": [P, y],
							"outline-width": [P, y],
							opacity: [P, b],
							top: [P, _],
							right: [P, _],
							bottom: [P, _],
							left: [P, _],
							"font-size": [P, _],
							"text-indent": [P, _],
							"word-spacing": [P, _],
							width: [P, _],
							"min-width": [P, _],
							"max-width": [P, _],
							height: [P, _],
							"min-height": [P, _],
							"max-height": [P, _],
							"line-height": [P, k],
							"scroll-top": [W, b, "scrollTop"],
							"scroll-left": [W, b, "scrollLeft"]
						},
						K = {};
					A.transform && (Y.transform = [D], K = {
						x: [_, "translateX"],
						y: [_, "translateY"],
						rotate: [x],
						rotateX: [x],
						rotateY: [x],
						scale: [b],
						scaleX: [b],
						scaleY: [b],
						skew: [x],
						skewX: [x],
						skewY: [x]
					}), A.transform && A.backface && (K.z = [_, "translateZ"], K.rotateZ = [x], K.scaleZ = [b], K.perspective = [y]);
					var Q = /ms/,
						J = /s|\./;
					return t.tram = e
				}(window.jQuery)
			}, function(t, e, n) {
				n(3), n(7), n(8), n(9), t.exports = n(10)
			}, function(t, e, n) {
				"use strict";
				var i = n(0);
				i.define("brand", t.exports = function(t) {
					var e, n = {},
						r = document,
						o = t("html"),
						a = t("body"),
						s = ".w-webflow-badge",
						u = window.location,
						c = /PhantomJS/i.test(navigator.userAgent),
						l = "fullscreenchange webkitfullscreenchange mozfullscreenchange msfullscreenchange";

					function h() {
						var n = r.fullScreen || r.mozFullScreen || r.webkitIsFullScreen || r.msFullscreenElement || Boolean(r.webkitFullscreenElement);
						t(e).attr("style", n ? "display: none !important;" : "")
					}

					function f() {
						var t = a.children(s),
							n = t.length && t.get(0) === e,
							r = i.env("editor");
						n ? r && t.remove() : (t.length && t.remove(), r || a.append(e))
					}
					return n.ready = function() {
						var n, i, a, s = o.attr("data-wf-status"),
							d = o.attr("data-wf-domain") || "";
						/\.webflow\.io$/i.test(d) && u.hostname !== d && (s = !0), s && !c && (e = e || (n = t('<a class="w-webflow-badge"></a>').attr("href", "https://webflow.com?utm_campaign=brandjs"), i = t("<img>").attr("src", "https://d3e54v103j8qbb.cloudfront.net/img/webflow-badge-icon.f67cd735e3.svg").attr("alt", "").css({
							marginRight: "8px",
							width: "16px"
						}), a = t("<img>").attr("src", "https://d1otoma47x30pg.cloudfront.net/img/webflow-badge-text.6faa6a38cd.svg").attr("alt", "Made in Webflow"), n.append(i, a), n[0]), f(), setTimeout(f, 500), t(r).off(l, h).on(l, h))
					}, n
				})
			}, function(t, e, n) {
				"use strict";
				var i = window.$,
					r = n(1) && i.tram;
				/*!
				 * Webflow._ (aka) Underscore.js 1.6.0 (custom build)
				 * _.each
				 * _.map
				 * _.find
				 * _.filter
				 * _.any
				 * _.contains
				 * _.delay
				 * _.defer
				 * _.throttle (webflow)
				 * _.debounce
				 * _.keys
				 * _.has
				 * _.now
				 *
				 * http://underscorejs.org
				 * (c) 2009-2013 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
				 * Underscore may be freely distributed under the MIT license.
				 * @license MIT
				 */
				t.exports = function() {
					var t = {
							VERSION: "1.6.0-Webflow"
						},
						e = {},
						n = Array.prototype,
						i = Object.prototype,
						o = Function.prototype,
						a = (n.push, n.slice),
						s = (n.concat, i.toString, i.hasOwnProperty),
						u = n.forEach,
						c = n.map,
						l = (n.reduce, n.reduceRight, n.filter),
						h = (n.every, n.some),
						f = n.indexOf,
						d = (n.lastIndexOf, Array.isArray, Object.keys),
						p = (o.bind, t.each = t.forEach = function(n, i, r) {
							if (null == n) return n;
							if (u && n.forEach === u) n.forEach(i, r);
							else if (n.length === +n.length) {
								for (var o = 0, a = n.length; o < a; o++)
									if (i.call(r, n[o], o, n) === e) return
							} else {
								var s = t.keys(n);
								for (o = 0, a = s.length; o < a; o++)
									if (i.call(r, n[s[o]], s[o], n) === e) return
							}
							return n
						});
					t.map = t.collect = function(t, e, n) {
						var i = [];
						return null == t ? i : c && t.map === c ? t.map(e, n) : (p(t, function(t, r, o) {
							i.push(e.call(n, t, r, o))
						}), i)
					}, t.find = t.detect = function(t, e, n) {
						var i;
						return v(t, function(t, r, o) {
							if (e.call(n, t, r, o)) return i = t, !0
						}), i
					}, t.filter = t.select = function(t, e, n) {
						var i = [];
						return null == t ? i : l && t.filter === l ? t.filter(e, n) : (p(t, function(t, r, o) {
							e.call(n, t, r, o) && i.push(t)
						}), i)
					};
					var v = t.some = t.any = function(n, i, r) {
						i || (i = t.identity);
						var o = !1;
						return null == n ? o : h && n.some === h ? n.some(i, r) : (p(n, function(t, n, a) {
							if (o || (o = i.call(r, t, n, a))) return e
						}), !!o)
					};
					t.contains = t.include = function(t, e) {
						return null != t && (f && t.indexOf === f ? -1 != t.indexOf(e) : v(t, function(t) {
							return t === e
						}))
					}, t.delay = function(t, e) {
						var n = a.call(arguments, 2);
						return setTimeout(function() {
							return t.apply(null, n)
						}, e)
					}, t.defer = function(e) {
						return t.delay.apply(t, [e, 1].concat(a.call(arguments, 1)))
					}, t.throttle = function(t) {
						var e, n, i;
						return function() {
							e || (e = !0, n = arguments, i = this, r.frame(function() {
								e = !1, t.apply(i, n)
							}))
						}
					}, t.debounce = function(e, n, i) {
						var r, o, a, s, u, c = function c() {
							var l = t.now() - s;
							l < n ? r = setTimeout(c, n - l) : (r = null, i || (u = e.apply(a, o), a = o = null))
						};
						return function() {
							a = this, o = arguments, s = t.now();
							var l = i && !r;
							return r || (r = setTimeout(c, n)), l && (u = e.apply(a, o), a = o = null), u
						}
					}, t.defaults = function(e) {
						if (!t.isObject(e)) return e;
						for (var n = 1, i = arguments.length; n < i; n++) {
							var r = arguments[n];
							for (var o in r) void 0 === e[o] && (e[o] = r[o])
						}
						return e
					}, t.keys = function(e) {
						if (!t.isObject(e)) return [];
						if (d) return d(e);
						var n = [];
						for (var i in e) t.has(e, i) && n.push(i);
						return n
					}, t.has = function(t, e) {
						return s.call(t, e)
					}, t.isObject = function(t) {
						return t === Object(t)
					}, t.now = Date.now || function() {
						return (new Date).getTime()
					}, t.templateSettings = {
						evaluate: /<%([\s\S]+?)%>/g,
						interpolate: /<%=([\s\S]+?)%>/g,
						escape: /<%-([\s\S]+?)%>/g
					};
					var m = /(.)^/,
						w = {
							"'": "'",
							"\\": "\\",
							"\r": "r",
							"\n": "n",
							"\u2028": "u2028",
							"\u2029": "u2029"
						},
						b = /\\|'|\r|\n|\u2028|\u2029/g,
						g = function(t) {
							return "\\" + w[t]
						};
					return t.template = function(e, n, i) {
						!n && i && (n = i), n = t.defaults({}, n, t.templateSettings);
						var r = RegExp([(n.escape || m).source, (n.interpolate || m).source, (n.evaluate || m).source].join("|") + "|$", "g"),
							o = 0,
							a = "__p+='";
						e.replace(r, function(t, n, i, r, s) {
							return a += e.slice(o, s).replace(b, g), o = s + t.length, n ? a += "'+\n((__t=(" + n + "))==null?'':_.escape(__t))+\n'" : i ? a += "'+\n((__t=(" + i + "))==null?'':__t)+\n'" : r && (a += "';\n" + r + "\n__p+='"), t
						}), a += "';\n", n.variable || (a = "with(obj||{}){\n" + a + "}\n"), a = "var __t,__p='',__j=Array.prototype.join,print=function(){__p+=__j.call(arguments,'');};\n" + a + "return __p;\n";
						try {
							var s = new Function(n.variable || "obj", "_", a)
						} catch (t) {
							throw t.source = a, t
						}
						var u = function(e) {
								return s.call(this, e, t)
							},
							c = n.variable || "obj";
						return u.source = "function(" + c + "){\n" + a + "}", u
					}, t
				}()
			}, function(t, e) {
				t.exports = function(t) {
					return t && t.__esModule ? t : {
						default: t
					}
				}
			}, function(t, e) {
				function n(t) {
					return (n = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(t) {
						return typeof t
					} : function(t) {
						return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t
					})(t)
				}

				function i(e) {
					return "function" == typeof Symbol && "symbol" === n(Symbol.iterator) ? t.exports = i = function(t) {
						return n(t)
					} : t.exports = i = function(t) {
						return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : n(t)
					}, i(e)
				}
				t.exports = i
			}, function(t, e, n) {
				"use strict";
				var i = n(0);
				i.define("edit", t.exports = function(t, e, n) {
					if (n = n || {}, (i.env("test") || i.env("frame")) && !n.fixture && ! function() {
							try {
								return window.top.__Cypress__
							} catch (t) {
								return !1
							}
						}()) return {
						exit: 1
					};
					var r, o = t(window),
						a = t(document.documentElement),
						s = document.location,
						u = "hashchange",
						c = n.load || function() {
							r = !0, window.WebflowEditor = !0, o.off(u, h),
								function(t) {
									var e = window.document.createElement("iframe");
									e.src = "https://webflow.com/site/third-party-cookie-check.html", e.style.display = "none", e.sandbox = "allow-scripts allow-same-origin";
									var n = function n(i) {
										"WF_third_party_cookies_unsupported" === i.data ? (w(e, n), t(!1)) : "WF_third_party_cookies_supported" === i.data && (w(e, n), t(!0))
									};
									e.onerror = function() {
										w(e, n), t(!1)
									}, window.addEventListener("message", n, !1), window.document.body.appendChild(e)
								}(function(e) {
									t.ajax({
										url: m("https://editor-api.webflow.com/api/editor/view"),
										data: {
											siteId: a.attr("data-wf-site")
										},
										xhrFields: {
											withCredentials: !0
										},
										dataType: "json",
										crossDomain: !0,
										success: f(e)
									})
								})
						},
						l = !1;
					try {
						l = localStorage && localStorage.getItem && localStorage.getItem("WebflowEditor")
					} catch (t) {}

					function h() {
						r || /\?edit/.test(s.hash) && c()
					}

					function f(t) {
						return function(e) {
							e ? (e.thirdPartyCookiesSupported = t, d(v(e.bugReporterScriptPath), function() {
								d(v(e.scriptPath), function() {
									window.WebflowEditor(e)
								})
							})) : console.error("Could not load editor data")
						}
					}

					function d(e, n) {
						t.ajax({
							type: "GET",
							url: e,
							dataType: "script",
							cache: !0
						}).then(n, p)
					}

					function p(t, e, n) {
						throw console.error("Could not load editor script: " + e), n
					}

					function v(t) {
						return t.indexOf("//") >= 0 ? t : m("https://editor-api.webflow.com" + t)
					}

					function m(t) {
						return t.replace(/([^:])\/\//g, "$1/")
					}

					function w(t, e) {
						window.removeEventListener("message", e, !1), t.remove()
					}
					return l ? c() : s.search ? (/[?&](edit)(?:[=&?]|$)/.test(s.search) || /\?edit$/.test(s.href)) && c() : o.on(u, h).triggerHandler(u), {}
				})
			}, function(t, e, n) {
				"use strict";
				var i = n(0);
				i.define("links", t.exports = function(t, e) {
					var n, r, o, a = {},
						s = t(window),
						u = i.env(),
						c = window.location,
						l = document.createElement("a"),
						h = "w--current",
						f = /index\.(html|php)$/,
						d = /\/$/;

					function p(e) {
						var i = n && e.getAttribute("href-disabled") || e.getAttribute("href");
						if (l.href = i, !(i.indexOf(":") >= 0)) {
							var a = t(e);
							if (l.hash.length > 1 && l.host + l.pathname === c.host + c.pathname) {
								if (!/^#[a-zA-Z0-9\-\_]+$/.test(l.hash)) return;
								var s = t(l.hash);
								s.length && r.push({
									link: a,
									sec: s,
									active: !1
								})
							} else if ("#" !== i && "" !== i) {
								var u = l.href === c.href || i === o || f.test(i) && d.test(o);
								m(a, h, u)
							}
						}
					}

					function v() {
						var t = s.scrollTop(),
							n = s.height();
						e.each(r, function(e) {
							var i = e.link,
								r = e.sec,
								o = r.offset().top,
								a = r.outerHeight(),
								s = .5 * n,
								u = r.is(":visible") && o + a - s >= t && o + s <= t + n;
							e.active !== u && (e.active = u, m(i, h, u))
						})
					}

					function m(t, e, n) {
						var i = t.hasClass(e);
						n && i || (n || i) && (n ? t.addClass(e) : t.removeClass(e))
					}
					return a.ready = a.design = a.preview = function() {
						n = u && i.env("design"), o = i.env("slug") || c.pathname || "", i.scroll.off(v), r = [];
						for (var t = document.links, e = 0; e < t.length; ++e) p(t[e]);
						r.length && (i.scroll.on(v), v())
					}, a
				})
			}, function(t, e, n) {
				"use strict";
				var i = n(0);
				i.define("scroll", t.exports = function(t) {
					var e = {
							WF_CLICK_EMPTY: "click.wf-empty-link",
							WF_CLICK_SCROLL: "click.wf-scroll"
						},
						n = window.location,
						r = function() {
							try {
								return Boolean(window.frameElement)
							} catch (t) {
								return !0
							}
						}() ? null : window.history,
						o = t(window),
						a = t(document),
						s = t(document.body),
						u = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || function(t) {
							window.setTimeout(t, 15)
						},
						c = i.env("editor") ? ".w-editor-body" : "body",
						l = "header, " + c + " > .header, " + c + " > .w-nav:not([data-no-scroll])",
						h = 'a[href="#"]',
						f = 'a[href*="#"]:not(.w-tab-link):not(' + h + ")";
					var d = /^#[a-zA-Z0-9][\w:.-]*$/;

					function p(e) {
						var a = e.currentTarget;
						if (!(i.env("design") || window.$.mobile && /(?:^|\s)ui-link(?:$|\s)/.test(a.className))) {
							var c, h = (c = a, d.test(c.hash) && c.host + c.pathname === n.host + n.pathname ? a.hash : "");
							if ("" !== h) {
								var f = t(h);
								f.length && (e && (e.preventDefault(), e.stopPropagation()), function(t) {
									if (n.hash !== t && r && r.pushState && (!i.env.chrome || "file:" !== n.protocol)) {
										var e = r.state && r.state.hash;
										e !== t && r.pushState({
											hash: t
										}, "", t)
									}
								}(h), window.setTimeout(function() {
									! function(e) {
										var n = o.scrollTop(),
											i = function(e) {
												var n = t(l),
													i = "fixed" === n.css("position") ? n.outerHeight() : 0,
													r = e.offset().top - i;
												if ("mid" === e.data("scroll")) {
													var a = o.height() - i,
														s = e.outerHeight();
													s < a && (r -= Math.round((a - s) / 2))
												}
												return r
											}(e);
										if (n === i) return;
										var r = function(t, e, n) {
												if (document.body.hasAttribute("data-wf-reduce-scroll-motion") && ("none" === document.body.getAttribute("data-wf-scroll-motion") || "function" == typeof window.matchMedia && window.matchMedia("(prefers-reduced-motion: reduce)").matches)) return 0;
												var i = 1;
												return s.add(t).each(function(t, e) {
													var n = parseFloat(e.getAttribute("data-scroll-time"));
													!isNaN(n) && n >= 0 && (i = n)
												}), (472.143 * Math.log(Math.abs(e - n) + 125) - 2e3) * i
											}(e, n, i),
											a = Date.now();
										u(function t() {
											var e = Date.now() - a;
											window.scroll(0, function(t, e, n, i) {
												return n > i ? e : t + (e - t) * ((r = n / i) < .5 ? 4 * r * r * r : (r - 1) * (2 * r - 2) * (2 * r - 2) + 1);
												var r
											}(n, i, e, r)), e <= r && u(t)
										})
									}(f)
								}, e ? 0 : 300))
							}
						}
					}
					return {
						ready: function() {
							var t = e.WF_CLICK_EMPTY,
								n = e.WF_CLICK_SCROLL;
							a.on(n, f, p), a.on(t, h, function(t) {
								t.preventDefault()
							})
						}
					}
				})
			}, function(t, e, n) {
				"use strict";
				n(0).define("touch", t.exports = function(t) {
					var e = {},
						n = window.getSelection;

					function i(e) {
						var i, r, o = !1,
							a = !1,
							s = Math.min(Math.round(.04 * window.innerWidth), 40);

						function u(t) {
							var e = t.touches;
							e && e.length > 1 || (o = !0, e ? (a = !0, i = e[0].clientX) : i = t.clientX, r = i)
						}

						function c(e) {
							if (o) {
								if (a && "mousemove" === e.type) return e.preventDefault(), void e.stopPropagation();
								var i = e.touches,
									u = i ? i[0].clientX : e.clientX,
									c = u - r;
								r = u, Math.abs(c) > s && n && "" === String(n()) && (! function(e, n, i) {
									var r = t.Event(e, {
										originalEvent: n
									});
									t(n.target).trigger(r, i)
								}("swipe", e, {
									direction: c > 0 ? "right" : "left"
								}), h())
							}
						}

						function l(t) {
							if (o) return o = !1, a && "mouseup" === t.type ? (t.preventDefault(), t.stopPropagation(), void(a = !1)) : void 0
						}

						function h() {
							o = !1
						}
						e.addEventListener("touchstart", u, !1), e.addEventListener("touchmove", c, !1), e.addEventListener("touchend", l, !1), e.addEventListener("touchcancel", h, !1), e.addEventListener("mousedown", u, !1), e.addEventListener("mousemove", c, !1), e.addEventListener("mouseup", l, !1), e.addEventListener("mouseout", h, !1), this.destroy = function() {
							e.removeEventListener("touchstart", u, !1), e.removeEventListener("touchmove", c, !1), e.removeEventListener("touchend", l, !1), e.removeEventListener("touchcancel", h, !1), e.removeEventListener("mousedown", u, !1), e.removeEventListener("mousemove", c, !1), e.removeEventListener("mouseup", l, !1), e.removeEventListener("mouseout", h, !1), e = null
						}
					}
					return t.event.special.tap = {
						bindType: "click",
						delegateType: "click"
					}, e.init = function(e) {
						return (e = "string" == typeof e ? t(e).get(0) : e) ? new i(e) : null
					}, e.instance = e.init(document), e
				})
			},
			function(t, e, n) {
				"use strict";
				var i = n(0),
					r = n(12),
					o = {
						ARROW_LEFT: 37,
						ARROW_UP: 38,
						ARROW_RIGHT: 39,
						ARROW_DOWN: 40,
						SPACE: 32,
						ENTER: 13,
						HOME: 36,
						END: 35
					},
					a = 'a[href], area[href], [role="button"], input, select, textarea, button, iframe, object, embed, *[tabindex], *[contenteditable]';
				i.define("slider", t.exports = function(t, e) {
					var n, s, u, c, l = {},
						f = t.tram,
						h = t(document),
						d = i.env(),
						p = ".w-slider",
						v = '<div class="w-slider-dot" data-wf-ignore />',
						m = '<div aria-live="off" aria-atomic="true" class="w-slider-aria-label" data-wf-ignore />',
						w = r.triggers;

					function g() {
						(n = h.find(p)).length && (n.each(x), c = null, u || (b(), i.resize.on(y), i.redraw.on(l.redraw)))
					}

					function b() {
						i.resize.off(y), i.redraw.off(l.redraw)
					}

					function y() {
						n.filter(":visible").each(L)
					}

					function x(e, n) {
						var i = t(n),
							r = t.data(n, p);
						r || (r = t.data(n, p, {
							index: 0,
							depth: 1,
							hasFocus: {
								keyboard: !1,
								mouse: !1
							},
							el: i,
							config: {}
						})), r.mask = i.children(".w-slider-mask"), r.left = i.children(".w-slider-arrow-left"), r.right = i.children(".w-slider-arrow-right"), r.nav = i.children(".w-slider-nav"), r.slides = r.mask.children(".w-slide"), r.slides.each(w.reset), c && (r.maskWidth = 0), void 0 === i.attr("role") && i.attr("role", "region"), void 0 === i.attr("aria-label") && i.attr("aria-label", "carousel");
						var o = r.mask.attr("id");
						if (o || (o = "w-slider-mask-" + e, r.mask.attr("id", o)), r.ariaLiveLabel = t(m).appendTo(r.mask), r.left.attr("role", "button"), r.left.attr("tabindex", "0"), r.left.attr("aria-controls", o), void 0 === r.left.attr("aria-label") && r.left.attr("aria-label", "previous slide"), r.right.attr("role", "button"), r.right.attr("tabindex", "0"), r.right.attr("aria-controls", o), void 0 === r.right.attr("aria-label") && r.right.attr("aria-label", "next slide"), !f.support.transform) return r.left.hide(), r.right.hide(), r.nav.hide(), void(u = !0);
						r.el.off(p), r.left.off(p), r.right.off(p), r.nav.off(p), _(r), s ? (r.el.on("setting" + p, R(r)), A(r), r.hasTimer = !1) : (r.el.on("swipe" + p, R(r)), r.left.on("click" + p, T(r)), r.right.on("click" + p, S(r)), r.left.on("keydown" + p, O(r, T)), r.right.on("keydown" + p, O(r, S)), r.nav.on("keydown" + p, "> div", R(r)), r.config.autoplay && !r.hasTimer && (r.hasTimer = !0, r.timerCount = 1, z(r)), r.el.on("mouseenter" + p, E(r, !0, "mouse")), r.el.on("focusin" + p, E(r, !0, "keyboard")), r.el.on("mouseleave" + p, E(r, !1, "mouse")), r.el.on("focusout" + p, E(r, !1, "keyboard"))), r.nav.on("click" + p, "> div", R(r)), d || r.mask.contents().filter(function() {
							return 3 === this.nodeType
						}).remove();
						var a = i.filter(":hidden");
						a.show();
						var l = i.parents(":hidden");
						l.show(), L(e, n), a.css("display", ""), l.css("display", "")
					}

					function _(t) {
						var e = {
							crossOver: 0
						};
						e.animation = t.el.attr("data-animation") || "slide", "outin" === e.animation && (e.animation = "cross", e.crossOver = .5), e.easing = t.el.attr("data-easing") || "ease";
						var n = t.el.attr("data-duration");
						if (e.duration = null != n ? parseInt(n, 10) : 500, k(t.el.attr("data-infinite")) && (e.infinite = !0), k(t.el.attr("data-disable-swipe")) && (e.disableSwipe = !0), k(t.el.attr("data-hide-arrows")) ? e.hideArrows = !0 : t.config.hideArrows && (t.left.show(), t.right.show()), k(t.el.attr("data-autoplay"))) {
							e.autoplay = !0, e.delay = parseInt(t.el.attr("data-delay"), 10) || 2e3, e.timerMax = parseInt(t.el.attr("data-autoplay-limit"), 10);
							var i = "mousedown" + p + " touchstart" + p;
							s || t.el.off(i).one(i, function() {
								A(t)
							})
						}
						var r = t.right.width();
						e.edge = r ? r + 40 : 100, t.config = e
					}

					function k(t) {
						return "1" === t || "true" === t
					}

					function E(e, n, i) {
						return function(r) {
							if (n) e.hasFocus[i] = n;
							else {
								if (t.contains(e.el.get(0), r.relatedTarget)) return;
								if (e.hasFocus[i] = n, e.hasFocus.mouse && "keyboard" === i || e.hasFocus.keyboard && "mouse" === i) return
							}
							n ? (e.ariaLiveLabel.attr("aria-live", "polite"), e.hasTimer && A(e)) : (e.ariaLiveLabel.attr("aria-live", "off"), e.hasTimer && z(e))
						}
					}

					function O(t, e) {
						return function(n) {
							switch (n.keyCode) {
								case o.SPACE:
								case o.ENTER:
									return e(t)(), n.preventDefault(), n.stopPropagation()
							}
						}
					}

					function T(t) {
						return function() {
							M(t, {
								index: t.index - 1,
								vector: -1
							})
						}
					}

					function S(t) {
						return function() {
							M(t, {
								index: t.index + 1,
								vector: 1
							})
						}
					}

					function z(t) {
						A(t);
						var e = t.config,
							n = e.timerMax;
						n && t.timerCount++ > n || (t.timerId = window.setTimeout(function() {
							null == t.timerId || s || (S(t)(), z(t))
						}, e.delay))
					}

					function A(t) {
						window.clearTimeout(t.timerId), t.timerId = null
					}

					function R(n) {
						return function(r, a) {
							a = a || {};
							var u = n.config;
							if (s && "setting" === r.type) {
								if ("prev" === a.select) return T(n)();
								if ("next" === a.select) return S(n)();
								if (_(n), j(n), null == a.select) return;
								! function(n, i) {
									var r = null;
									i === n.slides.length && (g(), j(n)), e.each(n.anchors, function(e, n) {
										t(e.els).each(function(e, o) {
											t(o).index() === i && (r = n)
										})
									}), null != r && M(n, {
										index: r,
										immediate: !0
									})
								}(n, a.select)
							} else {
								if ("swipe" === r.type) {
									if (u.disableSwipe) return;
									if (i.env("editor")) return;
									return "left" === a.direction ? S(n)() : "right" === a.direction ? T(n)() : void 0
								}
								if (n.nav.has(r.target).length) {
									var c = t(r.target).index();
									if ("click" === r.type && M(n, {
											index: c
										}), "keydown" === r.type) switch (r.keyCode) {
										case o.ENTER:
										case o.SPACE:
											M(n, {
												index: c
											}), r.preventDefault();
											break;
										case o.ARROW_LEFT:
										case o.ARROW_UP:
											C(n.nav, Math.max(c - 1, 0)), r.preventDefault();
											break;
										case o.ARROW_RIGHT:
										case o.ARROW_DOWN:
											C(n.nav, Math.min(c + 1, n.pages)), r.preventDefault();
											break;
										case o.HOME:
											C(n.nav, 0), r.preventDefault();
											break;
										case o.END:
											C(n.nav, n.pages), r.preventDefault();
											break;
										default:
											return
									}
								}
							}
						}
					}

					function C(t, e) {
						var n = t.children().eq(e).focus();
						t.children().not(n)
					}

					function M(e, n) {
						n = n || {};
						var i = e.config,
							r = e.anchors;
						e.previous = e.index;
						var o = n.index,
							u = {};
						o < 0 ? (o = r.length - 1, i.infinite && (u.x = -e.endX, u.from = 0, u.to = r[0].width)) : o >= r.length && (o = 0, i.infinite && (u.x = r[r.length - 1].width, u.from = -r[r.length - 1].x, u.to = u.from - u.x)), e.index = o;
						var l = e.nav.children().eq(o).addClass("w-active").attr("aria-selected", "true").attr("tabindex", "0");
						e.nav.children().not(l).removeClass("w-active").attr("aria-selected", "false").attr("tabindex", "-1"), i.hideArrows && (e.index === r.length - 1 ? e.right.hide() : e.right.show(), 0 === e.index ? e.left.hide() : e.left.show());
						var h = e.offsetX || 0,
							d = e.offsetX = -r[e.index].x,
							p = {
								x: d,
								opacity: 1,
								visibility: ""
							},
							v = t(r[e.index].els),
							m = t(r[e.previous] && r[e.previous].els),
							g = e.slides.not(v),
							b = i.animation,
							y = i.easing,
							x = Math.round(i.duration),
							_ = n.vector || (e.index > e.previous ? 1 : -1),
							k = "opacity " + x + "ms " + y,
							E = "transform " + x + "ms " + y;
						if (v.find(a).removeAttr("tabindex"), v.removeAttr("aria-hidden"), v.find("*").removeAttr("aria-hidden"), g.find(a).attr("tabindex", "-1"), g.attr("aria-hidden", "true"), g.find("*").attr("aria-hidden", "true"), s || (v.each(w.intro), g.each(w.outro)), n.immediate && !c) return f(v).set(p), void S();
						if (e.index !== e.previous) {
							if (e.ariaLiveLabel.text("Slide ".concat(o + 1, " of ").concat(r.length, ".")), "cross" === b) {
								var O = Math.round(x - x * i.crossOver),
									T = Math.round(x - O);
								return k = "opacity " + O + "ms " + y, f(m).set({
									visibility: ""
								}).add(k).start({
									opacity: 0
								}), void f(v).set({
									visibility: "",
									x: d,
									opacity: 0,
									zIndex: e.depth++
								}).add(k).wait(T).then({
									opacity: 1
								}).then(S)
							}
							if ("fade" === b) return f(m).set({
								visibility: ""
							}).stop(), void f(v).set({
								visibility: "",
								x: d,
								opacity: 0,
								zIndex: e.depth++
							}).add(k).start({
								opacity: 1
							}).then(S);
							if ("over" === b) return p = {
								x: e.endX
							}, f(m).set({
								visibility: ""
							}).stop(), void f(v).set({
								visibility: "",
								zIndex: e.depth++,
								x: d + r[e.index].width * _
							}).add(E).start({
								x: d
							}).then(S);
							i.infinite && u.x ? (f(e.slides.not(m)).set({
								visibility: "",
								x: u.x
							}).add(E).start({
								x: d
							}), f(m).set({
								visibility: "",
								x: u.from
							}).add(E).start({
								x: u.to
							}), e.shifted = m) : (i.infinite && e.shifted && (f(e.shifted).set({
								visibility: "",
								x: h
							}), e.shifted = null), f(e.slides).set({
								visibility: ""
							}).add(E).start({
								x: d
							}))
						}

						function S() {
							v = t(r[e.index].els), g = e.slides.not(v), "slide" !== b && (p.visibility = "hidden"), f(g).set(p)
						}
					}

					function L(e, n) {
						var i = t.data(n, p);
						if (i) return function(t) {
							var e = t.mask.width();
							if (t.maskWidth !== e) return t.maskWidth = e, !0;
							return !1
						}(i) ? j(i) : void(s && function(e) {
							var n = 0;
							if (e.slides.each(function(e, i) {
									n += t(i).outerWidth(!0)
								}), e.slidesWidth !== n) return e.slidesWidth = n, !0;
							return !1
						}(i) && j(i))
					}

					function j(e) {
						var n = 1,
							i = 0,
							r = 0,
							o = 0,
							a = e.maskWidth,
							u = a - e.config.edge;
						u < 0 && (u = 0), e.anchors = [{
							els: [],
							x: 0,
							width: 0
						}], e.slides.each(function(s, c) {
							r - i > u && (n++, i += a, e.anchors[n - 1] = {
								els: [],
								x: r,
								width: 0
							}), o = t(c).outerWidth(!0), r += o, e.anchors[n - 1].width += o, e.anchors[n - 1].els.push(c);
							var l = s + 1 + " of " + e.slides.length;
							t(c).attr("aria-label", l), t(c).attr("role", "group")
						}), e.endX = r, s && (e.pages = null), e.nav.length && e.pages !== n && (e.pages = n, function(e) {
							var n, i = [],
								r = e.el.attr("data-nav-spacing");
							r && (r = parseFloat(r) + "px");
							for (var o = 0, a = e.pages; o < a; o++)(n = t(v)).attr("aria-label", "Show slide " + (o + 1) + " of " + a).attr("aria-selected", "false").attr("role", "button").attr("tabindex", "-1"), e.nav.hasClass("w-num") && n.text(o + 1), null != r && n.css({
								"margin-left": r,
								"margin-right": r
							}), i.push(n);
							e.nav.empty().append(i)
						}(e));
						var c = e.index;
						c >= n && (c = n - 1), M(e, {
							immediate: !0,
							index: c
						})
					}
					return l.ready = function() {
						s = i.env("design"), g()
					}, l.design = function() {
						s = !0, g()
					}, l.preview = function() {
						s = !1, g()
					}, l.redraw = function() {
						c = !0, g()
					}, l.destroy = b, l
				})
			},
		]);
	</script>


	<script src="//ajax.aspnetcdn.com/ajax/jquery.ui/1.10.3/jquery-ui.min.js"></script>
	<script src="/wp-content/themes/twentysixteen/js/jquery.message.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
	
	<script>
		window.sortDisableSelection = function() {
			$( "#sortable1, #sortable2, #orderable1, #orderable2" ).sortable({connectWith:".connectedSortable, .connectedSortable_1, .connectedOrderable, .connectedOrderable_1"}).disableSelection(); // для отмены выделения текста на элементах
		};
	</script>
</body>

</html>