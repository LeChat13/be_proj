<?php
	/*
		Template Name: Result
	*/
?>
<?php if (current_user_can('level_10')){ ?>
<?php
	require_once 'db_work.php';
?>
<?
if (isset($_POST['parse_task'])) {
	if (isset($_POST['parse_start_date'])) {
		$parse_start_date = $_POST['parse_start_date'];
	}
		else {
			$parse_start_date = date('Y-m-d', time());
		}
	$parse_start_date .= ' 00:00:00';
	if (isset($_POST['parse_end_date'])) {
		$parse_end_date = $_POST['parse_end_date'];
	}
		else {
			$parse_end_date = date('Y-m-d', time()+604800);
		}
	$parse_end_date .= ' 00:00:00';
	$parse_status = isset($_POST['parse_status']) ? 'closed' : 'active';
	if (isset($_POST['parse_resource'])) {
		$parse_resource = $_POST['parse_resource'];
	}
		else {
			$parse_resource = 'avito';
		}
	if (isset($_POST['parse_region'])) {
		$parse_region = $_POST['parse_region'];
	}
		else {
			$parse_region = '01';
		}
	if (isset($_POST['parse_deal'])) {
		$parse_deal = $_POST['parse_deal'];
	}
		else {
			$parse_deal = 'buy';
		}
	if (isset($_POST['parse_object'])) {
		$parse_object = $_POST['parse_object'];
	}
		else {
			$parse_object = 'flat';
		}
	if ($stmt = mysqli_prepare ($link, 'SELECT id FROM parse_tasks WHERE site_code = ? AND region_code = ? AND deal_code = ? AND object_code = ? ORDER BY id ASC')) {
		mysqli_stmt_bind_param ($stmt, "ssss", $parse_resource, $parse_region, $parse_deal, $parse_object);
		mysqli_stmt_execute ($stmt);
		mysqli_stmt_store_result ($stmt);
		if (mysqli_stmt_num_rows ($stmt) > 0) {
			mysqli_stmt_bind_result ($stmt, $task_id);
			$f = 0;
			while (mysqli_stmt_fetch ($stmt)) {
				if ($f) {
					if ($substmt = mysqli_prepare ($link, "DELETE FROM parse_tasks WHERE id = ?")) {
						mysqli_stmt_bind_param ($substmt, "i", $task_id);
						mysqli_stmt_execute ($substmt);
						mysqli_stmt_close ($substmt);
					}
					mysqli_stmt_close ($substmt);
				}
					else {
						if ($substmt = mysqli_prepare ($link, "UPDATE parse_tasks SET start_date = ?, end_date = ?, `status` = ? WHERE id = ?")) {
							mysqli_stmt_bind_param ($substmt, "sssi", $parse_start_date, $parse_end_date, $parse_status, $task_id);
							mysqli_stmt_execute ($substmt);
							mysqli_stmt_close ($substmt);
						}
						mysqli_stmt_close ($substmt);
					}
				$f++;
			}
		}
			else {
				mysqli_stmt_close ($stmt);
				if ($stmt = mysqli_prepare ($link, "INSERT INTO parse_tasks (start_date, end_date, `status`, site_code, region_code, deal_code, object_code) VALUES (?, ?, ?, ?, ?, ?, ?)")) {
					mysqli_stmt_bind_param ($stmt, "sssssss", $parse_start_date, $parse_end_date, $parse_status, $parse_resource, $parse_region, $parse_deal, $parse_object);
					mysqli_stmt_execute ($stmt);
					mysqli_stmt_close ($stmt);
				}
			}
	}
	mysqli_stmt_close ($stmt);
	echo ("<div style=\"text-align: center; margin-top: 10px;\">
	<font color=\"green\">Данные задачи парсинга успешно сохранены!</font>
	<a href=\"http://1rlt.ru\">Вернуться назад</a></div>");
}
if (isset($_POST['post_task'])) {
	if (isset($_POST['post_start_date'])) {
		$post_start_date = $_POST['post_start_date'];
	}
		else {
			$post_start_date = date('Y-m-d', time());
		}
	$post_start_date .= ' 00:00:00';
	if (isset($_POST['post_end_date'])) {
		$post_end_date = $_POST['post_end_date'];
	}
		else {
			$post_end_date = date('Y-m-d', time()+604800);
		}
	$post_end_date .= ' 00:00:00';
	$post_status = isset($_POST['post_status']) ? 'closed' : 'active';
	if (isset($_POST['post_place'])) {
		$post_place = $_POST['post_place'];
	}
		else {
			$post_place = 'vk';
		}
	if (isset($_POST['post_region'])) {
		$post_region = $_POST['post_region'];
	}
		else {
			$post_region = '01';
		}
	if (isset($_POST['parse_deal'])) {
		$post_deal = $_POST['post_deal'];
	}
		else {
			$post_deal = 'buy';
		}
	if (isset($_POST['parse_object'])) {
		$post_object = $_POST['post_object'];
	}
		else {
			$post_object = 'flat';
		}
	if ($stmt = mysqli_prepare ($link, 'SELECT id FROM post_tasks WHERE place_code = ? AND region_code = ? AND deal_code = ? AND object_code = ? ORDER BY id ASC')) {
		mysqli_stmt_bind_param ($stmt, "ssss", $post_place, $post_region, $post_deal, $post_object);
		mysqli_stmt_execute ($stmt);
		mysqli_stmt_store_result ($stmt);
		if (mysqli_stmt_num_rows ($stmt) > 0) {
			mysqli_stmt_bind_result ($stmt, $task_id);
			$f = 0;
			while (mysqli_stmt_fetch ($stmt)) {
				if ($f) {
					if ($substmt = mysqli_prepare ($link, "DELETE FROM post_tasks WHERE id = ?")) {
						mysqli_stmt_bind_param ($substmt, "i", $task_id);
						mysqli_stmt_execute ($substmt);
						mysqli_stmt_close ($substmt);
					}
					mysqli_stmt_close ($substmt);
				}
					else {
						if ($substmt = mysqli_prepare ($link, "UPDATE post_tasks SET start_date = ?, end_date = ?, `status` = ? WHERE id = ?")) {
							mysqli_stmt_bind_param ($substmt, "sssi", $post_start_date, $post_end_date, $post_status, $task_id);
							mysqli_stmt_execute ($substmt);
							mysqli_stmt_close ($substmt);
						}
						mysqli_stmt_close ($substmt);
					}
				$f++;
			}
		}
			else {
				mysqli_stmt_close ($stmt);
				if ($stmt = mysqli_prepare ($link, "INSERT INTO post_tasks (start_date, end_date, `status`, place_code, region_code, deal_code, object_code) VALUES (?, ?, ?, ?, ?, ?, ?)")) {
					mysqli_stmt_bind_param ($stmt, "sssssss", $post_start_date, $post_end_date, $post_status, $post_place, $post_region, $post_deal, $post_object);
					mysqli_stmt_execute ($stmt);
					mysqli_stmt_close ($stmt);
				}
			}
	}
	mysqli_stmt_close ($stmt);
	echo ("<div style=\"text-align: center; margin-top: 10px;\">
	<font color=\"green\">Данные задачи постинга успешно сохранены!</font>
	<a href=\"http://1rlt.ru\">Вернуться назад</a></div>");
}
?>
<?php } ?> 

<?php if (!(current_user_can('level_10'))){ ?><br/>
	<?
		echo ("<div style=\"text-align: center; margin-top: 10px;\">
		<font color=\"red\">Необходимо авторизоваться!</font>
		<a href=\"http://1rlt.ru\">Войти</a></div>");
	?>
<?php } ?>

<?php
if (isset($_POST['submit0'])) {
$query =  file_get_contents("https://oauth.vk.com/authorize?client_id=6611877&scope=friends,photos,audio,video,wall,groups,messages,offline&redirect_uri=http://1rlt.ru/result&display=page&response_type=token");
$response_string = json_decode($query,true);
$_SESSION['token'] = $response_string['access_token'];
header('location: /');
echo 'error - '.$response_string['error']."\n";
echo 'error_description - '.$response_string['error_description']."\n";
echo 'access_token - '.$response_string['access_token']."\n";
echo 'expires_in - '.$response_string['expires_in']."\n";
echo 'user_id - '.$response_string['user_id']."\n";
}
?>

<script type="text/javascript">
	setTimeout('location.replace("http://1rlt.ru")', 1300);
</script>