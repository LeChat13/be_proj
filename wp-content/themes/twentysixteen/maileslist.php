<?php
	/*
		Template Name: maileslist
	*/
?>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<div>
<?
	global $user_ID, $user_identity, $user_email;
	get_currentuserinfo();
#	echo $user_email;
	if (!$user_ID):
?>
	<div>
		<?$temp = str_replace('Войти', 'Авторизация', $temp);?>
			<form name="loginform" id="autoriz" action="<?=get_settings('siteurl'); ?>/wp-login.php" method="post">
				<div class="left">
					<p>Логин:  <input type="text" name="log" placeholder="Логин" value="wpadm" id="login" /></p>
				</div>
				<div>
					<p>Пароль: <input type="password" name="pwd" placeholder="Пароль" value="" id="password" /></p>
					<input type="submit" name="submit" value="Войти" id="enter" /><br/>
				</div>
				<div>
					<input type="hidden" name="redirect_to" value="<?=$_SERVER['REQUEST_URI']; ?>" />
				</div>
			</form> 
	</div>
	<?elseif ($user_identity <> 'be_admin'):?>
	<div>
        <label style="color:red">Ваших прав доступа недостаточно для просмотра данной страницы</label>
	</div>
	<?else:?>
	<div>
 	    <?require_once 'db_mail.php';?>
        <a href="/" style="color:green">Назад на главную</a>
		<br/>
        <a href="/xlsxparser/" style="color:green">К заданиям парсинга XLSX</a>
		<br/>
		<br/>
			<form name="add" id="add" action="/mailadd" method="post">
				<input type="submit" name="added" value="Добавить" id="added"><br/>
			</form>
		<br/>
		<div>
			<label>Таблица адресов рассылки:</label>
		</div>
		<div>
			<table border="1" style="width: 100%;">
				<thead align="center">
					<tr>
						<td style="width: 3%;">
							№ пп
						</td>
						<td style="width: 30%;">
							Адрес E-Mail
						</td>
						<td style="width: 26%;">
							Обрабатывающий скрипт
						</td>
						<td style="width: 12%;">
							Метка группировки
						</td>
						<td style="width: 12%;">
							Флаг прикрепленного файла
						</td>
						<td style="width: 10%;">
							Изменить запись
						</td>
<!--						<td style="width: 3%;">
							Всего строк (номеров, закупок, и т.д.)
						</td>
						<td style="width: 3%;">
							Прошел строк (номеров, закупок, и т.д.)
						</td>
-->					</tr>
				</thead>
				<tbody>
					<?
					$num = 0;
					foreach ($mails_data as $dropd):
						$num++;
#						$file = fopen("/home/h807240122/betp.website/logs/task_id_".$dropd['id'].".tmp","r");
#						$dropd['all_str'] = chop(fgets($file));
#						$dropd['run_str'] = chop(fgets($file));
						fclose ($file);
					?>
						<tr>
							<td align="center">
								<?=$num?>
							</td>
							<td align="left">
								<?=$dropd['email']?>
							</td>
							<td align="left">
								<?=$dropd['script_info']?> - <?=$dropd['script_name']?>
							</td>
							<td align="left">
								<?=$dropd['group_flag']?>
							</td>
							<td align="left">
								<?=$dropd['attach_flag']?>
							</td>
							<td align="center">
								<form name="edit<?=$num?>" id="edit<?=$num?>" action="/mailedit" method="post">
									<div hidden>
										<input type="text" id="mail_id" name="mail_id" value="<?=$dropd['mail_id']?>">
									</div>
									<div hidden>
										<input type="text" id="script_id" name="script_id" value="<?=$dropd['script_id']?>">
									</div>
									<input type="submit" name="paste<?=$num?>" value="Изменить" id="paste<?=$num?>"><br/>
								</form>
							</td>
<!--							<td align="center">
								<?=$dropd['all_str']?>
							</td>
							<td align="center">
								<?=$dropd['run_str']?>
							</td>
-->						</tr>
					<?endforeach?>
				</tbody>
			</table>
		</div>
	</div>
	<?endif;?> 
</div>